Attribute VB_Name = "Module4"
'
'   PackedHighDensityExMetaVba [ 04 / 04 ] Self-extracting comment-block packed VBA source codes, which is generated automatically
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Count of packed modules:
'       18 / 393
'
'   Lack user-form sources:
'       UTestFormTextRClickClipBoard - CommonOfficeVBA\FormTools\UnitTest\UTestFormTextRClickClipBoard.frm
'       UInputPasswordBox - CommonOfficeVBA\FormTools\ManagePassword\UInputPasswordBox.frm
'       LogTextForm - CommonOfficeVBA\LoggingModules\LogTextForm\LogTextForm.frm
'       UProgressBarForm - CommonOfficeVBA\UserFormTools\UProgressBarForm.frm
'       UTestFormNoFormTop - CommonOfficeVBA\WindowControlModules\CommonControlHandlers\UnitTest\UTestFormNoFormTop.frm
'       UTestFormWithFormTop - CommonOfficeVBA\WindowControlModules\CommonControlHandlers\UnitTest\UTestFormWithFormTop.frm
'       UTestFormKeepStateReg - CommonOfficeVBA\RegistryAccess\UTestFormKeepStateReg.frm
'       CodePaneSwitchMultiLstForm - CommonOfficeVBA\DevelopmentAssist\SwitchCodePanes\CodePaneSwitchMultiLstForm.frm
'       CodePaneSwitchMultiLsvForm - CommonOfficeVBA\DevelopmentAssist\SwitchCodePanes\CodePaneSwitchMultiLsvForm.frm
'       CodePaneSwitchSingleLsvForm - CommonOfficeVBA\DevelopmentAssist\SwitchCodePanes\CodePaneSwitchSingleLsvForm.frm
'       USetAdoOleDbConStrForXlBook - CommonOfficeVBA\ADOModules\USetAdoOleDbConStrForXlBook.frm
'       USetAdoOdbcConStrForSqLite - CommonOfficeVBA\SQLiteConnector\USetAdoOdbcConStrForSqLite.frm
'       USetAdoOleDbConStrForAccdb - CommonOfficeVBA\ADOModules\MsAccessTool\USetAdoOleDbConStrForAccdb.frm
'       USetAdoOdbcConStrForPgSql - CommonOfficeVBA\PostgreSQLConnector\USetAdoOdbcConStrForPgSql.frm
'       USetAdoOdbcConStrForOracle - CommonOfficeVBA\OracleConnector\USetAdoOdbcConStrForOracle.frm
'       UErrorADOSQLForm - CommonOfficeVBA\ADOModules\AdoErrorHandling\UErrorADOSQLForm.frm
'
'   In this packed source files:
'       IExpandAdoRecordsetOnSheet.cls, AdoRSetSheetExpander.cls, ISqlRsetSheetExpander.cls, XlAdoSheetExpander.cls, UTfAdoConnectExcelSheet.bas
'       SqlUTfXlSheetExpander.bas, SqlUTfXlSheetExpanderByPassword.bas, CsvAdoSheetExpander.cls, SqlUTfCsvAdoSheetExpander.bas, UTfAdoConnectAccDbForXl.bas
'       AccDbAdoSheetExpander.cls, SqlUTfAccDbSheetExpander.bas, PgSqlOdbcSheetExpander.cls, SqlUTfPgSqlOdbcSheetExpander.bas, OracleOdbcSheetExpander.cls
'       SqLiteAdoSheetExpander.cls, SqlUTfSqLiteOdbcSheetExpander.bas, ExpandCodeOnWORDOut.bas
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Mon, 22/Jan/2024    Tarclanus-generator     Generated automatically
'
Option Explicit

'///////////////////////////////////////////////
'/// Directives
'///////////////////////////////////////////////
#Const HAS_REF = False

'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////

Private Const mstrThisExtractionModuleName As String = "Module4"

Private Const mstrALoadingDirectionVBProjectName As String = "ConvergenceProj"

'**---------------------------------------------
'** Block preserved keyword definitions
'**---------------------------------------------
Private Const mstrBlockDelimiterOfCodesBegin As String = "'''--Begin the comment out codes block--"

Private Const mstrBlockDelimiterOfASourceFileBeginLeft As String = "'''--VBA_Code_File--<"

Private Const mstrBlockDelimiterOfASourceFileBeginRight As String = ">--"

'**---------------------------------------------
'** Temporary files paths
'**---------------------------------------------
Private Const mstrTmpSubDir As String = "TmpCBSubDirModule4"


'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
''' Extract all VBA source files from this file comment block and load these into the specified VBA project name Macro book
'''
Public Sub ExtractVbaCodesFrom_PackedHighDensityExMetaVba04_InThisCommentBlock()

    Dim objBook As Excel.Workbook
    
    Set objBook = mfobjGetBookAfterLoadingVbaCodesFromThisVbaModuleCommentBlock()
End Sub


'''
'''
'''
Private Sub msubSanityTestToWriteSourceCodesFromThisVbaModuleCommentBlockToTemporaryDirectory()

    msubWriteSourceCodesFromThisVbaModuleCommentBlockToTemporaryDirectory
End Sub


'''
'''
'''
Private Sub msubSanityTestToGetFileNameToTextDicFromThisVbaModuleCommentBlock()

    Dim objDic As Scripting.Dictionary: Set objDic = mfobjGetFileNameToTextDicFromThisVbaModuleCommentBlock()

    'DebugDic objDic
End Sub

'''
'''
'''
Private Sub msubSanityTestToConfirmThisText()

    Debug.Print mfstrGetThisVbaModuleText()
End Sub



'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////

'''
'''
'''
Private Function mfobjGetBookAfterLoadingVbaCodesFromThisVbaModuleCommentBlock(Optional ByVal vstrTmpVbaCodesSubDir As String = mstrTmpSubDir) As Excel.Workbook

    Dim objBook As Excel.Workbook
    
    Dim strTmpDir As String
   
#If HAS_REF Then

    Dim objFS As Scripting.FileSystemObject, objDir As Scripting.Folder, objFile As Scripting.File
    
    Dim objVBProject As VBIDE.VBProject
    
    Set objFS = New Scripting.FileSystemObject
#Else
    Dim objFS As Object, objDir As Object, objFile As Object
    
    Dim objVBProject As Object
    
    Set objFS = CreateObject("Scripting.FileSystemObject")
#End If
  
    msubWriteSourceCodesFromThisVbaModuleCommentBlockToTemporaryDirectory vstrTmpVbaCodesSubDir


    Set objBook = mfobjGetTargetWorkbookOfSpecifiedVBProjectName()

    Set objVBProject = objBook.VBProject

    strTmpDir = ThisWorkbook.Path & "\" & vstrTmpVbaCodesSubDir

    Set objDir = objFS.GetFolder(strTmpDir)

    For Each objFile In objDir.Files
    
        objVBProject.VBComponents.Import objFile.Path
    Next

    Set mfobjGetBookAfterLoadingVbaCodesFromThisVbaModuleCommentBlock = objBook
End Function

'''
'''
'''
Private Function mfobjGetTargetWorkbookOfSpecifiedVBProjectName() As Excel.Workbook

    Dim objBook As Excel.Workbook, objTargetBook As Excel.Workbook
    
#If HAS_REF Then

    Dim objVBProject As VBIDE.VBProject
#Else
    Dim objVBProject As Object
#End If

    Set objTargetBook = Nothing

    For Each objBook In ThisWorkbook.Application.Workbooks
    
        Set objVBProject = objBook.VBProject
        
        If StrComp(objVBProject.Name, mstrALoadingDirectionVBProjectName) = 0 Then
        
            Set objTargetBook = objBook
            
            Exit For
        End If
    Next
    
    If objTargetBook Is Nothing Then
    
        Set objTargetBook = ThisWorkbook.Application.Workbooks.Add()
        
        objTargetBook.VBProject.Name = mstrALoadingDirectionVBProjectName
    End If

    Set mfobjGetTargetWorkbookOfSpecifiedVBProjectName = objTargetBook
End Function


'''
'''
'''
Private Sub msubWriteSourceCodesFromThisVbaModuleCommentBlockToTemporaryDirectory(Optional ByVal vstrTmpVbaCodesSubDir As String = mstrTmpSubDir)

#If HAS_REF Then
    Dim objSourceFileNameToCodesDic As Scripting.Dictionary
    
    Dim objFS As Scripting.FileSystemObject, objFile As Scripting.File
    
    Set objFS = New Scripting.FileSystemObject
#Else
    Dim objSourceFileNameToCodesDic As Object
    
    Dim objFS As Object, objFile As Object
    
    Set objFS = CreateObject("Scripting.FileSystemObject")
#End If

    Dim varSourceFileName As Variant, strSourceFilePath As String
    Dim strTmpDir As String
    
    
    strTmpDir = ThisWorkbook.Path & "\" & vstrTmpVbaCodesSubDir
    
    With objFS
    
        If Not .FolderExists(strTmpDir) Then
        
            .CreateFolder strTmpDir
        Else
            ' delete all
            
            For Each objFile In .GetFolder(strTmpDir).Files
        
                objFile.Delete
            Next
        End If
    End With
    
    
    Set objSourceFileNameToCodesDic = mfobjGetFileNameToTextDicFromThisVbaModuleCommentBlock()

    With objSourceFileNameToCodesDic

        For Each varSourceFileName In .Keys
        
            strSourceFilePath = strTmpDir & "\" & varSourceFileName
        
            With objFS
            
                With .CreateTextFile(strSourceFilePath, True)
                
                    .Write objSourceFileNameToCodesDic.Item(varSourceFileName)
                
                    .Close
                End With
            End With
        Next
    End With
End Sub


'''
'''
'''
#If HAS_REF Then
Private Function mfobjGetFileNameToTextDicFromThisVbaModuleCommentBlock() As Scripting.Dictionary

    Dim objDic As Scripting.Dictionary: Set objDic = New Scripting.Dictionary
#Else
Private Function mfobjGetFileNameToTextDicFromThisVbaModuleCommentBlock() As Object

    Dim objDic As Object: Set objDic = CreateObject("Scripting.Dictionary")
#End If
    Dim varLineText As Variant
    Dim blnFoundBlockOfCodesBegin As Boolean
    Dim blnInASourceFileBlock As Boolean, strCurrentSourceFileName As String
    Dim intLeftPoint As Long, intRightPoint As Long, intChildSourceLineNumber As Long
    Dim strCurrentChildSourceText As String
    
    
    blnFoundBlockOfCodesBegin = False
    
    blnInASourceFileBlock = False
    
    strCurrentChildSourceText = ""

    For Each varLineText In Split(mfstrGetThisVbaModuleText(), vbNewLine)

        If Not blnFoundBlockOfCodesBegin Then
        
            If InStr(1, varLineText, "'") = 1 Then
            
                If InStr(1, varLineText, mstrBlockDelimiterOfCodesBegin) > 0 Then
                
                    blnFoundBlockOfCodesBegin = True
                End If
            End If
        End If

        If blnFoundBlockOfCodesBegin Then
        
            If Not blnInASourceFileBlock Then
            
                intLeftPoint = InStr(1, varLineText, mstrBlockDelimiterOfASourceFileBeginLeft)
                
                If intLeftPoint > 0 Then
                
                    intRightPoint = InStrRev(varLineText, mstrBlockDelimiterOfASourceFileBeginRight)
                    
                    strCurrentSourceFileName = Mid(varLineText, intLeftPoint + Len(mstrBlockDelimiterOfASourceFileBeginLeft), intRightPoint - (intLeftPoint + Len(mstrBlockDelimiterOfASourceFileBeginLeft)))
                    
                    intChildSourceLineNumber = 0
                    
                    blnInASourceFileBlock = True
                End If
            End If
            
            
            If blnInASourceFileBlock Then
            
                If intChildSourceLineNumber > 0 Then
                
                    intLeftPoint = InStr(1, varLineText, mstrBlockDelimiterOfASourceFileBeginLeft)
                    
                    If intLeftPoint > 0 Then
                    
                        ' save cache the current source text
                        If strCurrentChildSourceText <> "" Then
                        
                            objDic.Add strCurrentSourceFileName, strCurrentChildSourceText
                        End If
                    
                    
                        ' Prepare the new file
                        intRightPoint = InStrRev(varLineText, mstrBlockDelimiterOfASourceFileBeginRight)
                    
                        strCurrentSourceFileName = Mid(varLineText, intLeftPoint + Len(mstrBlockDelimiterOfASourceFileBeginLeft), intRightPoint - (intLeftPoint + Len(mstrBlockDelimiterOfASourceFileBeginLeft)))
                    
                        intChildSourceLineNumber = 0
                    
                        strCurrentChildSourceText = ""
                        
                        blnInASourceFileBlock = True
                    End If
                End If
                
                If intChildSourceLineNumber > 0 Then
                
                    ' confirm the line head character is the line comment char
                    If InStr(1, varLineText, "'") = 1 Then
                    
                        If strCurrentChildSourceText <> "" Then
                        
                            strCurrentChildSourceText = strCurrentChildSourceText & vbNewLine
                        End If
                    
                        ' get a source code line
                    
                        strCurrentChildSourceText = strCurrentChildSourceText & Right(varLineText, Len(varLineText) - 1)
                    Else
                        ' save cache the current source text
                        If strCurrentChildSourceText <> "" Then
                        
                            objDic.Add strCurrentSourceFileName, strCurrentChildSourceText
                        End If
                        
                        intChildSourceLineNumber = 0
                    
                        strCurrentChildSourceText = ""
                        
                        blnInASourceFileBlock = False
                    End If
                End If
            
                intChildSourceLineNumber = intChildSourceLineNumber + 1
            End If
        End If
    Next

    Set mfobjGetFileNameToTextDicFromThisVbaModuleCommentBlock = objDic
End Function


'''
'''
'''
Private Function mfstrGetThisVbaModuleText() As String

#If HAS_REF Then
    Dim objVBProject As VBIDE.VBProject, objVBComponent As VBIDE.VBComponent
#Else
    Dim objVBProject As Object, objVBComponent As Object
#End If
    Dim strText As String
    
    Set objVBProject = ThisWorkbook.VBProject
    
    Set objVBComponent = objVBProject.VBComponents.Item(mstrThisExtractionModuleName)

    With objVBComponent.CodeModule
    
        strText = .Lines(1, .CountOfLines)
    End With

    mfstrGetThisVbaModuleText = strText
End Function


'''--Begin the comment out codes block--
'''--VBA_Code_File--<IExpandAdoRecordsetOnSheet.cls>--
'VERSION 1.0 CLASS
'BEGIN
'  MultiUse = -1  'True
'END
'Attribute VB_Name = "IExpandAdoRecordsetOnSheet"
'Attribute VB_GlobalNameSpace = False
'Attribute VB_Creatable = False
'Attribute VB_PredeclaredId = False
'Attribute VB_Exposed = False
''
''   Interface class - interfaces for expanding Recordset into a new Excel.Worksheet
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on Excel
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Tue, 16/May/2023    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Property interfaces
''///////////////////////////////////////////////
'
'Public CurrentSheet As Excel.Worksheet
'
'Public CacheOutputExcelBookPath As String
'
'Public OutputRSetExcelBookOpenModeProcess As OutputRSetExcelBookOpenModeProcessFlag
'
'Public IsCacheMemoriedOfOutputtedExcelBook As Boolean
'
''///////////////////////////////////////////////
''/// Operation interfaces
''///////////////////////////////////////////////
'
'Public Sub CloseCacheOutputBookOfOpenByPresetModeProcess(): End Sub
'
'''--VBA_Code_File--<AdoRSetSheetExpander.cls>--
'VERSION 1.0 CLASS
'BEGIN
'  MultiUse = -1  'True
'END
'Attribute VB_Name = "AdoRSetSheetExpander"
'Attribute VB_GlobalNameSpace = False
'Attribute VB_Creatable = False
'Attribute VB_PredeclaredId = False
'Attribute VB_Exposed = False
''
''   Outputting Excel sheet common operations for SQL result Recordset object
''   Collect the actual method implementations for serveing Excel books with creating its
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on both ADO and Excel
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Fri, 12/May/2023    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Interface implementations
''///////////////////////////////////////////////
'Implements IExpandAdoRecordsetOnSheet
'
''///////////////////////////////////////////////
''/// Declarations
''///////////////////////////////////////////////
'
'Private mitfAdoConnector As IADOConnector
'
'Private mitfOutputBookPath As IOutputBookPath
'
'Private mobjADOSheetFormatter As ADOSheetFormatter
'
''**---------------------------------------------
''** About cache for adding ListObject with naming
''**---------------------------------------------
'Private mobjDataTableSheetRangeAddresses As DataTableSheetRangeAddresses
'
''**---------------------------------------------
''** About cache for Unit-test mode Workbook preparetion
''**---------------------------------------------
'Private mobjCurrentSheet As Excel.Worksheet
'
'Private mstrCacheOutputExcelBookPath As String
'
'Private menmOutputRSetExcelBookOpenModeProcessFlag As OutputRSetExcelBookOpenModeProcessFlag
'
'Private mblnIsCacheMemoriedOfOutputtedExcelBook As Boolean
'
''///////////////////////////////////////////////
''/// Event Handlers
''///////////////////////////////////////////////
'Private Sub Class_Initialize()
'
'    Set mobjADOSheetFormatter = New ADOSheetFormatter
'
'
'    Set mobjCurrentSheet = Nothing
'
'    mstrCacheOutputExcelBookPath = ""
'
'    menmOutputRSetExcelBookOpenModeProcessFlag = NoProcessAboutOutputExcelBook
'
'    mblnIsCacheMemoriedOfOutputtedExcelBook = False
'End Sub
'
'Private Sub Class_Terminate()
'
'    IExpandAdoRecordsetOnSheet_CloseCacheOutputBookOfOpenByPresetModeProcess
'End Sub
'
''///////////////////////////////////////////////
''/// Properties
''///////////////////////////////////////////////
''''
''''
''''
'Public Property Get ExpandAdoRecordsetOnSheetInterface() As IExpandAdoRecordsetOnSheet
'
'    Set ExpandAdoRecordsetOnSheetInterface = Me
'End Property
'
''///////////////////////////////////////////////
''/// Implemented properties
''///////////////////////////////////////////////
'Public Property Set IExpandAdoRecordsetOnSheet_CurrentSheet(ByVal vobjSheet As Excel.Worksheet)
'
'    Set mobjCurrentSheet = vobjSheet
'End Property
'Public Property Get IExpandAdoRecordsetOnSheet_CurrentSheet() As Excel.Worksheet
'
'    Set IExpandAdoRecordsetOnSheet_CurrentSheet = mobjCurrentSheet
'End Property
'
''''
''''
''''
'Public Property Let IExpandAdoRecordsetOnSheet_CacheOutputExcelBookPath(ByVal vstrCacheOutputExcelBookPath As String)
'
'    mstrCacheOutputExcelBookPath = vstrCacheOutputExcelBookPath
'End Property
'Public Property Get IExpandAdoRecordsetOnSheet_CacheOutputExcelBookPath() As String
'
'    IExpandAdoRecordsetOnSheet_CacheOutputExcelBookPath = mstrCacheOutputExcelBookPath
'End Property
'
'Public Property Let IExpandAdoRecordsetOnSheet_OutputRSetExcelBookOpenModeProcess(ByVal venmOutputRSetExcelBookOpenModeProcessFlag As OutputRSetExcelBookOpenModeProcessFlag)
'
'    menmOutputRSetExcelBookOpenModeProcessFlag = venmOutputRSetExcelBookOpenModeProcessFlag
'End Property
'Public Property Get IExpandAdoRecordsetOnSheet_OutputRSetExcelBookOpenModeProcess() As OutputRSetExcelBookOpenModeProcessFlag
'
'    IExpandAdoRecordsetOnSheet_OutputRSetExcelBookOpenModeProcess = menmOutputRSetExcelBookOpenModeProcessFlag
'End Property
'
'Public Property Let IExpandAdoRecordsetOnSheet_IsCacheMemoriedOfOutputtedExcelBook(ByVal vblnIsCacheMemoriedOfOutputtedExcelBook As Boolean)
'
'    mblnIsCacheMemoriedOfOutputtedExcelBook = vblnIsCacheMemoriedOfOutputtedExcelBook
'End Property
'Public Property Get IExpandAdoRecordsetOnSheet_IsCacheMemoriedOfOutputtedExcelBook() As Boolean
'
'    IExpandAdoRecordsetOnSheet_IsCacheMemoriedOfOutputtedExcelBook = mblnIsCacheMemoriedOfOutputtedExcelBook
'End Property
'
'
''///////////////////////////////////////////////
''/// Properties
''///////////////////////////////////////////////
''''
'''' Reference of an interface
''''
'Public Property Get ADOConnectorInterface() As IADOConnector
'
'    Set ADOConnectorInterface = mitfAdoConnector
'End Property
'Public Property Set ADOConnectorInterface(ByVal vitfAdoConnector As IADOConnector)
'
'    Set mitfAdoConnector = vitfAdoConnector
'End Property
'
''''
'''' Reference of an interface
''''
'Public Property Get OutputBookPathInterface() As IOutputBookPath
'
'    Set OutputBookPathInterface = mitfOutputBookPath
'End Property
'Public Property Set OutputBookPathInterface(ByVal vitfOutputBookPath As IOutputBookPath)
'
'    Set mitfOutputBookPath = vitfOutputBookPath
'End Property
'
''''
'''' old: ADOSheetFormatting
''''
'Public Property Get DataTableSheetFormattingInterface() As IDataTableSheetFormatter
'
'    Set DataTableSheetFormattingInterface = mobjADOSheetFormatter.DataTableSheetFormattingInterface
'End Property
'
''///////////////////////////////////////////////
''/// Implemented operations
''///////////////////////////////////////////////
''''
''''
''''
'Private Sub IExpandAdoRecordsetOnSheet_CloseCacheOutputBookOfOpenByPresetModeProcess()
'
'    Dim objOutputBook As Excel.Workbook
'
'    If menmOutputRSetExcelBookOpenModeProcessFlag <> OutputRSetExcelBookOpenModeProcessFlag.NoProcessAboutOutputExcelBook Then
'
'        If Not mobjCurrentSheet Is Nothing Then
'
'            Set objOutputBook = mobjCurrentSheet.Parent
'
'            If Not objOutputBook Is Nothing Then
'
'                DoAfterProcessingToOutputBook objOutputBook, menmOutputRSetExcelBookOpenModeProcessFlag
'            End If
'
'            Set mobjCurrentSheet = Nothing
'        End If
'    End If
'
'    mblnIsCacheMemoriedOfOutputtedExcelBook = False
'End Sub
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''''
''''
''''
'Public Sub CloseCacheOutputBookOfOpenByPresetModeProcess()
'
'    IExpandAdoRecordsetOnSheet_CloseCacheOutputBookOfOpenByPresetModeProcess
'End Sub
'
'
''**---------------------------------------------
''** about query SQL and expand results into Excel.Worksheet
''**---------------------------------------------
''''
'''' query SQL and output result to Excel sheet
''''
'Public Sub OutputToSheetFrom(ByVal vstrSQL As String, _
'        ByVal vobjOutputSheet As Excel.Worksheet, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vobjHeaderInsertTexts As Collection = Nothing, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'    Dim objRSetAfterExpanded As ADODB.Recordset
'
'
'    OutputSqlQueryResultsToSheetFromByIAdoConnector objRSetAfterExpanded, mobjDataTableSheetRangeAddresses, mitfAdoConnector, mobjADOSheetFormatter, vstrSQL, vobjOutputSheet, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, vobjHeaderInsertTexts, vblnChangeVisibleExcelApplicationIfItIsImvisible
'
'    Set mobjDataTableSheetRangeAddresses = mobjDataTableSheetRangeAddresses
'
'    Set mobjCurrentSheet = vobjOutputSheet
'
'    ' Close ADODB.Recordset exactly
'
'    If Not objRSetAfterExpanded Is Nothing Then
'
'        With objRSetAfterExpanded
'
'            If .State <> ADODB.ObjectStateEnum.adStateClosed Then
'
'                ' For PostgreSQL ODBC driver, Oracle ODBC driver, Access OLE DB provider, the following is not needed. The ADODB.Recordset is closed automatically when the ADODB.Connection has been closed
'
'                ' But the SQLite3 ODBC driver needs the following at the 2023 Aug.
'
'                .Close
'            End If
'        End With
'    End If
'End Sub
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputToSpecifiedBook(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vobjHeaderInsertTexts As Collection = Nothing, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'    Dim objNewSheet As Excel.Worksheet
'
'    Set objNewSheet = GetNewWorksheetAfterAllExistedSheetsAndFindNewSheetName(vstrOutputBookPath, vstrNewSheetName)
'
'    ' Debug code
''    With New Scripting.FileSystemObject
'
''        CurrentProcessInfoFromThisApplication objNewSheet.Parent, True, vstrNewSheetName & "_", .GetParentFolderName(GetCurrentOfficeFileObject().FullName)
''    End With
'
'    Me.OutputToSheetFrom vstrSQL, objNewSheet, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, vobjHeaderInsertTexts, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
'''' query SQL and output result to the Existed Excel sheet
''''
'Public Sub OutputInExistedBook(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vobjHeaderInsertTexts As Collection = Nothing, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'    Me.OutputToSpecifiedBook vstrSQL, mitfOutputBookPath.GetPreservedOutputBookPath(), vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, vobjHeaderInsertTexts, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
'''' query SQL and output result to the already specified Excel sheet
''''
'Public Sub OutputToAlreadySpecifiedBook(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vobjInsertTexts As Collection = Nothing, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    SetupFirstCacheOfIAdoRecordsetSheetExpander Me
'
'    OutputToSpecifiedBook vstrSQL, Me.ExpandAdoRecordsetOnSheetInterface.CacheOutputExcelBookPath(), vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, vobjInsertTexts, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
''''
''''
'Public Sub OutputInAlreadyExistedBook(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vobjInsertTexts As Collection = Nothing, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    With Me.ExpandAdoRecordsetOnSheetInterface
'
'        .CacheOutputExcelBookPath = mitfOutputBookPath.GetPreservedOutputBookPath()
'    End With
'
'    OutputToAlreadySpecifiedBook vstrSQL, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, vobjInsertTexts, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
''''
''''
'Public Sub OutputToSpecifiedBookByUnitTestMode(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vobjInsertTexts As Collection = Nothing, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    With Me.ExpandAdoRecordsetOnSheetInterface
'
'        .OutputRSetExcelBookOpenModeProcess = OpenOutputBookByUnitTestMode
'
'        .CacheOutputExcelBookPath = vstrOutputBookPath
'    End With
'
'    Me.OutputToAlreadySpecifiedBook vstrSQL, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, vobjInsertTexts, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
''''
''''
'Public Sub OutputInExistedBookByUnitTestMode(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vobjInsertTexts As Collection = Nothing, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    With Me.ExpandAdoRecordsetOnSheetInterface
'
'        .OutputRSetExcelBookOpenModeProcess = OpenOutputBookByUnitTestMode
'    End With
'
'    Me.OutputInAlreadyExistedBook vstrSQL, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, vobjInsertTexts, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''**---------------------------------------------
''** about command SQL
''**---------------------------------------------
''''
'''' execute SQL command (UPDATE, INSERT, DELTE) and output result log to Excel sheet
''''
'Public Sub OutputCommandLogToSheetFrom(ByVal vstrSQL As String, _
'        ByVal vobjOutputSheet As Excel.Worksheet, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vobjHeaderInsertTexts As Collection = Nothing, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'    OutputSqlCommandLogToCellsOnSheetFromByIADOConnector mitfAdoConnector, mobjADOSheetFormatter, vstrSQL, vobjOutputSheet, venmShowUpAdoErrorOptionFlag, vobjHeaderInsertTexts, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputCommandLogToSpecifiedBook(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vobjHeaderInsertTexts As Collection = Nothing, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    With mobjADOSheetFormatter
'
'        If vstrNewSqlCommandLogSheetName <> "" Then
'
'            .ExecutedAdoSqlCommandLogPosition = AppendingSqlCommandLogsInSheetCellsOnSpecifiedSheet
'        Else
'            .ExecutedAdoSqlCommandLogPosition = AppendingSqlCommandLogsInSheetCellsOnFixedIndependentSheet
'        End If
'    End With
'
'    Me.OutputCommandLogToSheetFrom vstrSQL, GetRdbSqlCommandLogWorksheet(vstrOutputBookPath, vstrNewSqlCommandLogSheetName, mobjADOSheetFormatter), venmShowUpAdoErrorOptionFlag, vobjHeaderInsertTexts, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputCommandLogToAlreadySpecifiedBook(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vobjHeaderInsertTexts As Collection = Nothing, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    SetupFirstCacheOfIAdoRecordsetSheetExpander Me
'
'    OutputCommandLogToSpecifiedBook vstrSQL, Me.ExpandAdoRecordsetOnSheetInterface.CacheOutputExcelBookPath(), venmShowUpAdoErrorOptionFlag, vobjHeaderInsertTexts, vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputCommandLogInAlreadyExistedBook(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vobjHeaderInsertTexts As Collection = Nothing, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    With Me.ExpandAdoRecordsetOnSheetInterface
'
'        .CacheOutputExcelBookPath = mitfOutputBookPath.GetPreservedOutputBookPath()
'    End With
'
'    OutputCommandLogToAlreadySpecifiedBook vstrSQL, venmShowUpAdoErrorOptionFlag, vobjHeaderInsertTexts, vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputCommandLogToSpecifiedBookByUnitTestMode(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vobjHeaderInsertTexts As Collection = Nothing, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    With Me.ExpandAdoRecordsetOnSheetInterface
'
'        .OutputRSetExcelBookOpenModeProcess = OpenOutputBookByUnitTestMode
'
'        .CacheOutputExcelBookPath = vstrOutputBookPath
'    End With
'
'    OutputCommandLogToAlreadySpecifiedBook vstrSQL, venmShowUpAdoErrorOptionFlag, vobjHeaderInsertTexts, vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputCommandLogInExistedBookByUnitTestMode(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vobjHeaderInsertTexts As Collection = Nothing, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    With Me.ExpandAdoRecordsetOnSheetInterface
'
'        .OutputRSetExcelBookOpenModeProcess = OpenOutputBookByUnitTestMode
'    End With
'
'    OutputCommandLogInAlreadyExistedBook vstrSQL, venmShowUpAdoErrorOptionFlag, vobjHeaderInsertTexts, vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
'
''**---------------------------------------------
''** About outputting Excel book control
''**---------------------------------------------
''''
''''
''''
'Public Sub SetOutputExcelBookPath(ByVal vstrOutputBookPath As String, _
'        Optional ByVal venmOutputRSetExcelBookOpenModeProcessFlag As OutputRSetExcelBookOpenModeProcessFlag = OutputRSetExcelBookOpenModeProcessFlag.OpenOutputBookByUnitTestMode)
'
'    mstrCacheOutputExcelBookPath = vstrOutputBookPath
'
'    menmOutputRSetExcelBookOpenModeProcessFlag = venmOutputRSetExcelBookOpenModeProcessFlag
'End Sub
'
'''--VBA_Code_File--<ISqlRsetSheetExpander.cls>--
'VERSION 1.0 CLASS
'BEGIN
'  MultiUse = -1  'True
'END
'Attribute VB_Name = "ISqlRsetSheetExpander"
'Attribute VB_GlobalNameSpace = False
'Attribute VB_Creatable = False
'Attribute VB_PredeclaredId = False
'Attribute VB_Exposed = False
''
''   Interface class - interfaces for expanding Recordset into a new Excel.Worksheet
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on both ADO and Excel
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Fri,  7/Jul/2023    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Operation interfaces
''///////////////////////////////////////////////
''''
''''
''''
'Public Function OutputToSheetFrom(ByVal vstrSQL As String, _
'        ByVal vobjSheet As Excel.Worksheet, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable) As ADODB.Recordset
'End Function
'
''''
''''
''''
'Public Function OutputToSpecifiedBook(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable) As ADODB.Recordset
'End Function
'
'
'
'
'''--VBA_Code_File--<XlAdoSheetExpander.cls>--
'VERSION 1.0 CLASS
'BEGIN
'  MultiUse = -1  'True
'END
'Attribute VB_Name = "XlAdoSheetExpander"
'Attribute VB_GlobalNameSpace = False
'Attribute VB_Creatable = False
'Attribute VB_PredeclaredId = False
'Attribute VB_Exposed = False
''
''   output SQL result into Excel.Worksheet after connecting Excel.Workbook by ADO OLE DB interface
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on both ADO and Excel
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Thu,  9/Jun/2022    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Interface implementations
''///////////////////////////////////////////////
'Implements IOutputBookPath
'
''///////////////////////////////////////////////
''/// Declarations
''///////////////////////////////////////////////
'Private mobjConnector As XlAdoConnector: Private mitfConnector As IADOConnector
'
'Private mobjAdoRecordsetSheetExpander As AdoRSetSheetExpander
'
'
''///////////////////////////////////////////////
''/// Event Handlers
''///////////////////////////////////////////////
'Private Sub Class_Initialize()
'
'    Set mobjConnector = New XlAdoConnector
'
'    Set mitfConnector = mobjConnector
'
'    Set mobjAdoRecordsetSheetExpander = New AdoRSetSheetExpander
'
'    With mobjAdoRecordsetSheetExpander
'
'        Set .ADOConnectorInterface = mobjConnector
'
'        Set .OutputBookPathInterface = Me
'
'        .DataTableSheetFormattingInterface.FieldTitleInteriorType = ColumnNameInteriorOfExcelSheetAdoSQL
'    End With
'End Sub
'
''''
''''
''''
'Private Sub Class_Terminate()
'
'    mobjAdoRecordsetSheetExpander.CloseCacheOutputBookOfOpenByPresetModeProcess
'
'    Debug.Print "class XlAdoSheetExpander terminate -> " & mobjAdoRecordsetSheetExpander Is Nothing
'
'    Me.CloseConnection
'
'    Set mobjAdoRecordsetSheetExpander = Nothing
'
'    Set mobjConnector = Nothing
'End Sub
'
'
''///////////////////////////////////////////////
''/// Properties - connoted ADO connector
''///////////////////////////////////////////////
'Public Property Get SQLExecutionResult() As SQLResult
'
'    Set SQLExecutionResult = mobjConnector.SQLExecutionResult
'End Property
'
'Public Property Get IsSqlExecutionFailed() As Boolean
'
'    IsSqlExecutionFailed = mobjConnector.IsSqlExecutionFailed
'End Property
'
'Public Property Let AllowToRecordSQLLog(ByVal vblnAllowToRecord As Boolean)
'
'    mobjConnector.AllowToRecordSQLLog = vblnAllowToRecord
'End Property
'Public Property Get AllowToRecordSQLLog() As Boolean
'
'    AllowToRecordSQLLog = mobjConnector.AllowToRecordSQLLog
'End Property
'
'
'Public Property Get CurrentSheet() As Excel.Worksheet
'
'    Set CurrentSheet = mobjAdoRecordsetSheetExpander.IExpandAdoRecordsetOnSheet_CurrentSheet
'End Property
'
'
'Public Property Get CurrentBook() As Excel.Workbook
'
'    If Not Me.CurrentSheet Is Nothing Then
'
'        Set CurrentBook = Me.CurrentSheet.Parent
'    Else
'        Set CurrentBook = Nothing
'    End If
'End Property
'
'
''///////////////////////////////////////////////
''/// Properties
''///////////////////////////////////////////////
''''
''''
''''
'Public Property Get ADOConnectorInterface() As IADOConnector
'
'    Set ADOConnectorInterface = mitfConnector
'End Property
'
''''
''''
''''
'Public Property Get DataTableSheetFormattingInterface() As IDataTableSheetFormatter
'
'    Set DataTableSheetFormattingInterface = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface
'End Property
'
'
''**---------------------------------------------
''** connoted DataTableSheetFormatter
''**---------------------------------------------
'Public Property Get TopLeftRowIndex() As Long
'
'    TopLeftRowIndex = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.TopLeftRowIndex
'End Property
'Public Property Let TopLeftRowIndex(ByVal vintTopLeftRowIndex As Long)
'
'    mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.TopLeftRowIndex = vintTopLeftRowIndex
'End Property
'
'Public Property Get TopLeftColumnIndex() As Long
'
'    TopLeftColumnIndex = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.TopLeftColumnIndex
'End Property
'Public Property Let TopLeftColumnIndex(ByVal vintTopLeftColumnIndex As Long)
'
'    mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.TopLeftColumnIndex = vintTopLeftColumnIndex
'End Property
'
'
'Public Property Let AllowToShowFieldTitle(ByVal vblnAllowToShowFieldTitle As Boolean)
'
'    mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.AllowToShowFieldTitle = vblnAllowToShowFieldTitle
'End Property
'Public Property Get AllowToShowFieldTitle() As Boolean
'
'    AllowToShowFieldTitle = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.AllowToShowFieldTitle
'End Property
'
'
'
'Public Property Let AllowToSetAutoFilter(ByVal vblnAllowToSetAutoFilter As Boolean)
'
'    mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.AllowToSetAutoFilter = vblnAllowToSetAutoFilter
'End Property
'Public Property Get AllowToSetAutoFilter() As Boolean
'
'    AllowToSetAutoFilter = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.AllowToSetAutoFilter
'End Property
'
''''
'''' Dictionary(Of String(column title), Double(column-width))
''''
'Public Property Set FieldTitleToColumnWidthDic(ByVal vobjFieldTitleToColumnWidthDic As Scripting.Dictionary)
'
'    Set mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.FieldTitleToColumnWidthDic = vobjFieldTitleToColumnWidthDic
'End Property
'Public Property Get FieldTitleToColumnWidthDic() As Scripting.Dictionary
'
'    Set FieldTitleToColumnWidthDic = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.FieldTitleToColumnWidthDic
'End Property
'
''''
'''' Dictionary(Of String(column title order, such as 'Col1', 'Col07'), Double(column-width))
''''
'Public Property Set FieldTitleOrderToColumnWidthDic(ByVal vobjFieldTitleOrderToColumnWidthDic As Scripting.Dictionary)
'
'    Set mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.FieldTitleOrderToColumnWidthDic = vobjFieldTitleOrderToColumnWidthDic
'End Property
'Public Property Get FieldTitleOrderToColumnWidthDic() As Scripting.Dictionary
'
'    Set FieldTitleOrderToColumnWidthDic = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.FieldTitleOrderToColumnWidthDic
'End Property
'
'
'
''''
'''' When it is true, then ConvertDataForColumns procedure is used.
''''
'Public Property Get AllowToConvertDataForSpecifiedColumns() As Boolean
'
'    AllowToConvertDataForSpecifiedColumns = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.AllowToConvertDataForSpecifiedColumns
'End Property
'Public Property Let AllowToConvertDataForSpecifiedColumns(ByVal vblnAllowToConvertDataForSpecifiedColumns As Boolean)
'
'    mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.AllowToConvertDataForSpecifiedColumns = vblnAllowToConvertDataForSpecifiedColumns
'End Property
''''
'''' Dictionary(Of String(column title), ColumnDataConvertType(Enum definition))
''''
'Public Property Get FieldTitlesToColumnDataConvertTypeDic() As Scripting.Dictionary
'
'    Set FieldTitlesToColumnDataConvertTypeDic = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.FieldTitlesToColumnDataConvertTypeDic
'End Property
'Public Property Set FieldTitlesToColumnDataConvertTypeDic(ByVal vobjFieldTitlesToColumnDataConvertTypeDic As Scripting.Dictionary)
'
'    Set mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.FieldTitlesToColumnDataConvertTypeDic = vobjFieldTitlesToColumnDataConvertTypeDic
'End Property
'
''''
'''' ColumnsNumberFormatLocalParam
''''
'Public Property Set ColumnsNumberFormatLocal(ByVal vobjColumnsNumberFormatLocalParam As ColumnsNumberFormatLocalParam)
'
'    Set mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.ColumnsNumberFormatLocal = vobjColumnsNumberFormatLocalParam
'End Property
'Public Property Get ColumnsNumberFormatLocal() As ColumnsNumberFormatLocalParam
'
'    Set ColumnsNumberFormatLocal = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.ColumnsNumberFormatLocal
'End Property
'
''''
'''' set format-condition for each column
''''
'Public Property Set ColumnsFormatCondition(ByVal vobjColumnsFormatConditionParam As ColumnsFormatConditionParam)
'
'    Set mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.ColumnsFormatCondition = vobjColumnsFormatConditionParam
'End Property
'Public Property Get ColumnsFormatCondition() As ColumnsFormatConditionParam
'
'    Set ColumnsFormatCondition = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.ColumnsFormatCondition
'End Property
'
''''
'''' merge cells when the same values continue
''''
'Public Property Let AllowToMergeCellsByContinuousSameValues(ByVal vblnAllowToMergeCellsByContinuousSameValues As Boolean)
'
'    mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.AllowToMergeCellsByContinuousSameValues = vblnAllowToMergeCellsByContinuousSameValues
'End Property
'Public Property Get AllowToMergeCellsByContinuousSameValues() As Boolean
'
'    AllowToMergeCellsByContinuousSameValues = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.AllowToMergeCellsByContinuousSameValues
'End Property
'Public Property Let AllowToSetVerticalTopAlignmentTopOfMergeCells(ByVal vblnAllowToSetVerticalTopAlignmentTopOfMergeCells As Boolean)
'
'    mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.AllowToSetVerticalTopAlignmentTopOfMergeCells = vblnAllowToSetVerticalTopAlignmentTopOfMergeCells
'End Property
'Public Property Get AllowToSetVerticalTopAlignmentTopOfMergeCells() As Boolean
'
'    AllowToSetVerticalTopAlignmentTopOfMergeCells = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.AllowToSetVerticalTopAlignmentTopOfMergeCells
'End Property
'
'Public Property Set FieldTitlesToMergeCells(ByVal vobjFieldTitlesToMergeCells As Collection)
'
'    Set mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.FieldTitlesToMergeCells = vobjFieldTitlesToMergeCells
'End Property
'Public Property Get FieldTitlesToMergeCells()
'
'    Set FieldTitlesToMergeCells = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.FieldTitlesToMergeCells
'End Property
'
'Public Property Let FieldTitleInteriorType(ByVal venmFieldTitleInteriorType As FieldTitleInterior)
'
'    mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.FieldTitleInteriorType = venmFieldTitleInteriorType
'End Property
'Public Property Get FieldTitleInteriorType() As FieldTitleInterior
'
'    FieldTitleInteriorType = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.FieldTitleInteriorType
'End Property
'
'Public Property Let RecordBordersType(ByVal venmRecordCellsBordersType As RecordCellsBorders)
'
'    mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.RecordBordersType = venmRecordCellsBordersType
'End Property
'Public Property Get RecordBordersType() As RecordCellsBorders
'
'    RecordBordersType = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.RecordBordersType
'End Property
'
''///////////////////////////////////////////////
''/// Properties - connoted ADOConnectionSetting
''///////////////////////////////////////////////
'Public Property Get ConnectSetting() As ADOConnectionSetting
'
'    Set ConnectSetting = mobjConnector.ConnectSetting
'End Property
'
''''
'''' unit is second. [s]
''''
'Public Property Let CommandTimeout(ByVal vintTimeout As Long)
'
'    mobjConnector.CommandTimeout = vintTimeout
'End Property
'Public Property Get CommandTimeout() As Long
'
'    CommandTimeout = mobjConnector.CommandTimeout
'End Property
'
'
''///////////////////////////////////////////////
''/// Properties - Excel-book connection by ACCESS OLE-DB provider
''///////////////////////////////////////////////
'Public Property Get ConnectingFilePath() As String
'
'    ConnectingFilePath = mobjConnector.ConnectingFilePath
'End Property
'
''''
'''' when the load Excel sheet doesn't have the field header title text, this is False
''''
'Public Property Get HeaderFieldTitleExists() As Boolean
'
'    HeaderFieldTitleExists = mobjConnector.HeaderFieldTitleExists
'End Property
'
'Public Property Get BookReadOnly() As Boolean
'
'    BookReadOnly = mobjConnector.BookReadOnly
'End Property
'
'Public Property Get IMEX() As IMEXMode
'
'    IMEX = mobjConnector.IMEX
'End Property
'
''///////////////////////////////////////////////
''/// Implemented operations
''///////////////////////////////////////////////
''''
''''
''''
'Private Function IOutputBookPath_GetPreservedOutputBookPath() As String
'
'    IOutputBookPath_GetPreservedOutputBookPath = GetXlConnectingPrearrangedLogBookPath()
'End Function
'
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''**---------------------------------------------
''** set ADO connection string all parameters directly
''**---------------------------------------------
''''
'''' set input Excel book path
''''
'Public Sub SetInputExcelBookPath(ByVal vstrBookPath As String, _
'        Optional venmIMEXMode As IMEXMode = IMEXMode.NoUseIMEXMode, _
'        Optional vblnHeaderFieldTitleExists As Boolean = True, _
'        Optional vblnBookReadOnly As Boolean = False, _
'        Optional ByVal vstrLockBookPassword As String = "", _
'        Optional vblnIsConnectedToOffice95 As Boolean = False)
'
'    mobjConnector.SetInputExcelBookPath vstrBookPath, venmIMEXMode, vblnHeaderFieldTitleExists, vblnBookReadOnly, vstrLockBookPassword, vblnIsConnectedToOffice95
'End Sub
'
''**---------------------------------------------
''** set ADO connection string parameters by a User-form interface
''**---------------------------------------------
''''
''''
''''
'''' <Argument>vstrSettingKeyName: Input</Argument>
'''' <Argument>vstrBookPath: Input</Argument>
'''' <Argument>venmIMEXMode: Input</Argument>
'''' <Argument>vblnHeaderFieldTitleExists: Input</Argument>
'''' <Argument>vblnBookReadOnly: Input</Argument>
'''' <Argument>venmAccessOLEDBProviderType: Input</Argument>
'''' <Argument>vstrBookLockPassword: Input</Argument>
'''' <Return>Boolean</Return>
'Public Function IsAdoConnectionContinuedAfterAccessOleDbConnectionParametersByForm(ByVal vstrSettingKeyName As String, _
'        Optional ByVal vstrBookPath As String = "", _
'        Optional ByVal venmIMEXMode As IMEXMode = IMEXMode.NoUseIMEXMode, _
'        Optional ByVal vblnHeaderFieldTitleExists As Boolean = True, _
'        Optional ByVal vblnBookReadOnly As Boolean = False, _
'        Optional ByVal venmAccessOLEDBProviderType As AccessOLEDBProviderType = AccessOLEDBProviderType.AceOLEDB_12P0, _
'        Optional ByVal vstrBookLockPassword As String = "")
'
'
'    IsAdoConnectionContinuedAfterAccessOleDbConnectionParametersByForm = mobjConnector.IsAdoConnectionContinuedAfterAccessOleDbConnectionParametersByForm(vstrSettingKeyName, vstrBookPath, venmIMEXMode, vblnHeaderFieldTitleExists, vblnBookReadOnly, venmAccessOLEDBProviderType, vstrBookLockPassword)
'End Function
'
'
'
''''
'''' Close ADO connection
''''
'Public Sub CloseConnection()
'
'    mobjConnector.CloseConnection
'End Sub
'
''''
''''
''''
'Public Sub CloseCacheOutputBookOfOpenByPresetModeProcess()
'
'    mobjAdoRecordsetSheetExpander.CloseCacheOutputBookOfOpenByPresetModeProcess
'End Sub
'
''''
''''
''''
'Public Sub CloseAll()
'
'    CloseConnection
'
'    CloseCacheOutputBookOfOpenByPresetModeProcess
'End Sub
'
''**---------------------------------------------
''** Use SQL after the ADO connected
''**---------------------------------------------
''''
'''' get recordset object by the Select SQL
''''
'Public Function GetRecordset(ByVal vstrSQL As String) As ADODB.Recordset
'
'    Set GetRecordset = mobjConnector.GetRecordset(vstrSQL)
'End Function
'
''**---------------------------------------------
''** Output Recordset and logs to cells on sheet
''**---------------------------------------------
''''
'''' query SQL and output result to Excel sheet
''''
'Public Sub OutputToSheetFrom(ByVal vstrSQL As String, _
'        ByVal vobjOutputSheet As Excel.Worksheet, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'    mobjAdoRecordsetSheetExpander.OutputToSheetFrom vstrSQL, vobjOutputSheet, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedBookPathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
'''' query SQL and output result to the Existed Excel sheet
''''
'Public Sub OutputInExistedBook(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputInExistedBook vstrSQL, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedBookPathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
'Public Sub OutputInAlreadyExistedBook(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputInAlreadyExistedBook vstrSQL, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedBookPathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
''''
''''
'Public Sub OutputInExistedBookByUnitTestMode(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputInExistedBookByUnitTestMode vstrSQL, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedBookPathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputToSpecifiedBook(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputToSpecifiedBook vstrSQL, vstrOutputBookPath, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedBookPathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
'Public Sub OutputToSpecifiedBookByUnitTestMode(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'    mobjAdoRecordsetSheetExpander.OutputToSpecifiedBookByUnitTestMode vstrSQL, vstrOutputBookPath, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedBookPathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
'''' query SQL and output result to the already specified Excel sheet
''''
'Public Sub OutputToAlreadySpecifiedBook(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'    mobjAdoRecordsetSheetExpander.OutputToAlreadySpecifiedBook vstrSQL, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedBookPathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
''''
''''
'Public Sub SetOutputExcelBookPath(ByVal vstrOutputBookPath As String, _
'        Optional ByVal venmOutputRSetExcelBookOpenModeProcessFlag As OutputRSetExcelBookOpenModeProcessFlag = OutputRSetExcelBookOpenModeProcessFlag.OpenOutputBookByUnitTestMode, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag)
'
'    mobjAdoRecordsetSheetExpander.SetOutputExcelBookPath vstrOutputBookPath, venmOutputRSetExcelBookOpenModeProcessFlag
'End Sub
'
''**---------------------------------------------
''** Excecute SQL command (UPDATE, INSERT, DELETE)
''**---------------------------------------------
''''
'''' command SQL and output result-log to Excel sheet
''''
'Public Sub OutputCommandLogToSheetFrom(ByVal vstrSQL As String, _
'        ByVal vobjOutputSqlCommandLogSheet As Excel.Worksheet, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogToSheetFrom vstrSQL, vobjOutputSqlCommandLogSheet, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedBookPathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputCommandLogToSpecifiedBook(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogToSpecifiedBook vstrSQL, vstrOutputBookPath, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedBookPathInsertText(), vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputCommandLogToAlreadySpecifiedBook(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogToAlreadySpecifiedBook vstrSQL, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedBookPathInsertText(), vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputCommandLogInAlreadyExistedBook(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogInAlreadyExistedBook vstrSQL, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedBookPathInsertText(), vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputCommandLogToSpecifiedBookByUnitTestMode(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogToSpecifiedBookByUnitTestMode vstrSQL, vstrOutputBookPath, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedBookPathInsertText(), vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
'''' query SQL and output result to the prepared Excel sheet
''''
'Public Sub OutputCommandLogInExistedBookByUnitTestMode(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogInExistedBookByUnitTestMode vstrSQL, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedBookPathInsertText(), vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
''''
''''
''''
'Private Function mfobjGetConnectedBookPathInsertText() As Collection
'
'    Dim objInsertTexts As Collection
'
'    Set objInsertTexts = New Collection
'
'    With mobjConnector
'
'        objInsertTexts.Add "<File Name> " & GetFileNameFromPathByVbaDir(.ConnectingFilePath) & ", <Directory> " & GetParentDirectoryPathByVbaDir(.ConnectingFilePath)
'    End With
'
'    Set mfobjGetConnectedBookPathInsertText = objInsertTexts
'End Function
'
'
'''--VBA_Code_File--<UTfAdoConnectExcelSheet.bas>--
'Attribute VB_Name = "UTfAdoConnectExcelSheet"
''
''   Sanity tests for USetAdoOleDbConStrForXlBook.frm
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on both ADO and Excel
''       Dependent on UTfDataTableSecurity.bas
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Tue, 25/Jul/2023    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
'
''///////////////////////////////////////////////
''/// Constants
''///////////////////////////////////////////////
'
'Private Const mstrSheetName As String = "TestSheet"
'
'
''///////////////////////////////////////////////
''/// Sanity tests
''///////////////////////////////////////////////
''**---------------------------------------------
''** XlAdoSheetExpander tests, which need to open the password locked Excel book by Microsoft Excel specification
''**---------------------------------------------
''''
'''' Use a User-form
''''
'Private Sub msubSanityTestToXlAdoSheetExpanderWithForm()
'
'    Dim strBookPath As String, strSQL As String
'
'    strBookPath = mfstrPrepareTestBookWithLockPassword()
'
'    With New XlAdoConnector
'
'        ' input the password by a User-form
'
'        If .IsAdoConnectionContinuedAfterAccessOleDbConnectionParametersByForm("TestAccOleDbToExcelWithPasswordKey", strBookPath) Then
'
'            strSQL = "SELECT * FROM [" & mstrSheetName & "$]"
'
'            DebugCol GetTableCollectionFromRSet(.GetRecordset(strSQL))
'        End If
'    End With
'End Sub
'
'Private Sub msubSanityTestToXlAdoSheetExpander()
'
'    Dim strBookPath As String, strSQL As String
'
'    strBookPath = mfstrPrepareTestBookWithLockPassword()
'
'    With New XlAdoSheetExpander
'
'        ' In the case, input the password directly
'
'        .SetInputExcelBookPath strBookPath, vstrLockBookPassword:="1234"
'
'        strSQL = "SELECT * FROM [" & mstrSheetName & "$]"
'
'        .OutputInExistedBookByUnitTestMode strSQL, "TestSQLResults", SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable
'    End With
'End Sub
'
''**---------------------------------------------
''** USetAdoOleDbConStrForXlBook form tests
''**---------------------------------------------
''''
'''' delete testing SettingKeyName from the registory
''''
'Private Sub msubSanityTestToDeleteTestingSettingKeyNamesForConnectingToAccessOleDbWithExcelSheet()
'
'    DeleteWinRegKeyValuesOfAdoConnectionStringInfo "TestAccOleDbToExcelKey"
'
'    DeleteWinRegKeyValuesOfAdoConnectionStringInfo "TestAccOleDbToExcelWithPasswordKey"
'End Sub
'
''''
'''' Excel book, which is not locked by any password.
''''
'Private Sub msubSanityTestToSetOdbcConnectionParametersByAccessOleDbToExcelSheetForm()
'
'    Dim blnIsRequestedToCancelAdoConnection As Boolean, objADOConStrOfAceOledbXl As ADOConStrOfAceOleDbXl
'    Dim strSettingKeyName As String, strBookPath As String
'
'
'    strSettingKeyName = "TestAccOleDbToExcelKey"
'
'    Set objADOConStrOfAceOledbXl = New ADOConStrOfAceOleDbXl
'
'    strBookPath = mfstrPrepareTestBook()
'
'    SetOleDbConnectionParametersByAccessOleDbToExcelSheetForm blnIsRequestedToCancelAdoConnection, objADOConStrOfAceOledbXl, strSettingKeyName, strBookPath
'End Sub
'
''''
'''' Connect a password locked Excel book
''''
'Private Sub msubSanityTestToSetOdbcConnectionParametersByAccessOleDbToExcelSheetFormWithPasswordLock()
'
'    Dim blnIsRequestedToCancelAdoConnection As Boolean, objADOConStrOfAceOledbXl As ADOConStrOfAceOleDbXl
'    Dim strSettingKeyName As String, strBookPath As String
'
'
'    strSettingKeyName = "TestAccOleDbToExcelWithPasswordKey"
'
'    Set objADOConStrOfAceOledbXl = New ADOConStrOfAceOleDbXl
'
'    strBookPath = mfstrPrepareTestBookWithLockPassword()
'
'    SetOleDbConnectionParametersByAccessOleDbToExcelSheetForm blnIsRequestedToCancelAdoConnection, objADOConStrOfAceOledbXl, strSettingKeyName, strBookPath, vstrBookLockPassword:="1234"
'End Sub
'
''''
'''' Connect a password locked Excel book
''''
'Private Sub msubSanityTestToSetOdbcConnectionParametersByAccessOleDbToExcelSheetFormWithPasswordLockAndOpen()
'
'    Dim blnIsRequestedToCancelAdoConnection As Boolean, objADOConStrOfAceOledbXl As ADOConStrOfAceOleDbXl
'    Dim strSettingKeyName As String, strBookPath As String
'
'
'    strSettingKeyName = "TestAccOleDbToExcelWithPasswordKey"
'
'    Set objADOConStrOfAceOledbXl = New ADOConStrOfAceOleDbXl
'
'    strBookPath = mfstrPrepareTestBookWithLockPassword()
'
'    SetOleDbConnectionParametersByAccessOleDbToExcelSheetForm blnIsRequestedToCancelAdoConnection, objADOConStrOfAceOledbXl, strSettingKeyName, strBookPath
'End Sub
'
''''
'''' Connect a password locked Excel book using a User-form
''''
'Private Sub msubSanityTestToSetOdbcConnectionParametersByAccessOleDbToExcelSheetFormWithPasswordLockAndOpenFormIfItNeeded()
'
'    Dim blnIsRequestedToCancelAdoConnection As Boolean, objADOConStrOfAceOledbXl As ADOConStrOfAceOleDbXl
'    Dim strSettingKeyName As String, strBookPath As String
'
'
'    strSettingKeyName = "TestAccOleDbToExcelKey"
'
'    Set objADOConStrOfAceOledbXl = New ADOConStrOfAceOleDbXl
'
'    strBookPath = mfstrPrepareTestBookWithLockPassword()
'
'    SetOleDbConnectionParametersByAccessOleDbToExcelSheetForm blnIsRequestedToCancelAdoConnection, objADOConStrOfAceOledbXl, strSettingKeyName, strBookPath
'End Sub
'
'
'
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
''''
''''
''''
'Private Function mfstrPrepareTestBookWithLockPassword() As String
'
'    Dim strPath As String
'
'    strPath = mfstrGetTemporaryAdoConnectingTestExcelBookDir() & "\AdoConectionUITestWithLockPassword.xlsx"
'
'    PrepareTestBookWithLockingPasswordAndCreating strPath, mstrSheetName, "1234"
'
'    mfstrPrepareTestBookWithLockPassword = strPath
'End Function
'
''''
''''
''''
'Private Function mfstrPrepareTestBook() As String
'
'    Dim strPath As String
'
'    strPath = mfstrGetTemporaryAdoConnectingTestExcelBookDir() & "\AdoConectionUITest.xlsx"
'
'    PrepareTestBookWithLockingPasswordAndCreating strPath, mstrSheetName, ""
'
'    mfstrPrepareTestBook = strPath
'End Function
'
'
''''
''''
''''
'Private Function mfstrGetTemporaryAdoConnectingTestExcelBookDir() As String
'
'    Dim strDir As String
'
'    strDir = GetParentDirectoryPathOfCurrentOfficeFile() & "\TemporaryBooks\AdoConnectionUITests"
'
'    'strDir = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\AdoConnectionUITests"
'
'    ForceToCreateDirectory strDir
'
'    mfstrGetTemporaryAdoConnectingTestExcelBookDir = strDir
'End Function
'
'''--VBA_Code_File--<SqlUTfXlSheetExpander.bas>--
'Attribute VB_Name = "SqlUTfXlSheetExpander"
''
''   Sanity test to output SQL result into an output Excel sheet for a source Excel file sheets in a directory using ADO OLE DB
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on both ADO and Excel
''       Dependent on Microsoft Office Access Data Base Engine OLE DB Provider,
''       which should have been the installed when the Microsoft Office Excel software has been installed.
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Fri, 20/Jan/2023    Kalmclaeyd Tarclanus    Added the multi-language caption function and tested
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Constants
''///////////////////////////////////////////////
'Private Const mstrOutputBaseName As String = "SQLResultsOfUTfXlAdoSheetExpander"
'
'
''///////////////////////////////////////////////
''/// Sanity tests
''///////////////////////////////////////////////
''**---------------------------------------------
''** XlAdoSheetExpander
''**---------------------------------------------
''''
'''' Create an in-line table from the only SELECT sql inside
''''
'Private Sub msubSanityTestToConnectByVirtualTableXlConnector()
'
'    Dim strSQL As String
'
'    With New XlAdoSheetExpander
'
'        .SetInputExcelBookPath GetXlConnectingPrearrangedLogBookPath()
'
'        .AllowToRecordSQLLog = True
'
'        'strSQL = "SELECT 'ABC', 'DEF' UNION SELECT 'GHI', 'JKL'" ' Error occurs
'
'        'strSQL = "SELECT 1"
'
'        'strSQL = "SELECT 'ABC', 'DEF'"
'
'        strSQL = "SELECT 'ABC' AS A_Column, 'DEF' AS B_Column"
'
'        .OutputInExistedBookByUnitTestMode strSQL, "Test_ADO_VirtualTable"
'
'
'        .CloseCacheOutputBookOfOpenByPresetModeProcess
'    End With
'End Sub
'
''''
'''' Create an in-line table from the only SELECT sql inside
''''
'Private Sub msubSanityTestToConnectForExpandingOperations()
'
'    Dim objSheet As Excel.Worksheet, strOutputBookPath As String, objBook As Excel.Workbook
'    Dim strSQL As String
'
'    ForceToCreateDirectory GetTemporaryDevelopmentVBARootDir()
'
'    strOutputBookPath = GetTemporaryDevelopmentVBARootDir() & "\SanityTestToOutputOperationsOfAdoRecordsetSheetExpander.xlsx"
'
'    Set objBook = GetWorkbookAndPrepareForUnitTest(strOutputBookPath)
'
'    With New XlAdoSheetExpander
'
'        .SetInputExcelBookPath GetXlConnectingPrearrangedLogBookPath()
'
'        .AllowToRecordSQLLog = True
'
'
'
'        strSQL = "SELECT 'ABC' AS A_Column, 'DEF' AS B_Column"
'
'
'        .OutputInExistedBookByUnitTestMode strSQL, "Test_ADO_VirtualTable"
'
'        .OutputInAlreadyExistedBook strSQL, "VirtualTable02"
'
'        Set objSheet = GetNewWorksheetAfterAllExistedSheetsFromBook(objBook)
'
'        objSheet.Name = "VirtualTable03"
'
'        .OutputToSheetFrom strSQL, objSheet
'
'        .OutputInExistedBook strSQL, "VirtualTable04"
'
'        .OutputToSpecifiedBookByUnitTestMode strSQL, strOutputBookPath, "VirtualTable05"
'
'        .OutputToSpecifiedBook strSQL, strOutputBookPath, "VirtualTable06"
'
'
'        .CloseCacheOutputBookOfOpenByPresetModeProcess
'
'        DeleteDummySheetWhenItExists .CurrentSheet
'    End With
'End Sub
'
''''
'''' SQL logs will be appended on a specified sheet.
''''
'Private Sub msubSanityTestToConnectByVirtualTableXlConnectorForSQLLogAppending()
'
'    Dim strSQL As String
'
'    With New XlAdoSheetExpander
'
'        .SetInputExcelBookPath GetXlConnectingPrearrangedLogBookPath()
'
'        strSQL = "SELECT 'ABC' AS A_Column, 'DEF' AS B_Column"
'
'        .OutputInExistedBookByUnitTestMode strSQL, "Test_ADO_VirtualTable", AppendingSqlQueryLogsInSheetCellsOnIndependentSheet
'
'        strSQL = "SELECT 'EFG' AS C_Column, 'HIJ' AS D_Column"
'
'        .OutputInExistedBookByUnitTestMode strSQL, "Test_ADO_VirtualTable02", AppendingSqlQueryLogsInSheetCellsOnIndependentSheet
'
'        strSQL = "SELECT 'KLM' AS E_Column, 'OPQ' AS F_Column"
'
'        .OutputInExistedBookByUnitTestMode strSQL, "Test_ADO_VirtualTable03", AppendingSqlQueryLogsInSheetCellsOnIndependentSheet
'
'
'        DeleteDummySheetWhenItExists .CurrentBook
'
'        .CloseCacheOutputBookOfOpenByPresetModeProcess
'    End With
'End Sub
'
''''
'''' Confirm the auto shape log of SQL select
''''
'Private Sub msubSanityTestToConnectByVirtualTableXlConnectorWithAutoShapeLog()
'
'    Dim strSQL As String
'
'    With New XlAdoSheetExpander
'
'        .SetInputExcelBookPath GetXlConnectingPrearrangedLogBookPath()
'
'        strSQL = "SELECT 'ABC' AS A_Column, 'DEF' AS B_Column"
'
'        .OutputInExistedBookByUnitTestMode strSQL, "Test_ADO_VirtualTable", SqlQueryLogsByAutoShapesOnSheetWithOutputtingRecordsetDataTable
'
'        .CloseCacheOutputBookOfOpenByPresetModeProcess
'    End With
'End Sub
'
''''
'''' show UErrorADOSQLForm tests
''''
'Private Sub msubSanityTestToShowErrorTrapFormByIllegalSQL()
'
'    Dim strSQL As String, objRSet As ADODB.Recordset
'
'    With New XlAdoSheetExpander
'
'        .SetInputExcelBookPath GetXlConnectingPrearrangedLogBookPath()
'
'        strSQL = "(SELECT 'ABC', 'DEF') UNION ALL (SELECT 'GHI', 'JKL')" ' This causes a error. It seems that UNION ALL is not applicable....
'
'        'strSQL = "(SELECT 'ABC' AS A_Column, 'DEF' AS B_Column) UNION ALL (SELECT 'GHI', 'JKL')" ' Error occurs
'
'        .OutputInExistedBookByUnitTestMode strSQL, "Test_ADO_VirtualTable"
'    End With
'End Sub
'
'
'
''''
''''
''''
'Private Sub msubSanityTest01ToConnectExcelBookTestAfterCreateBook()
'
'    GetOutputExcelBookPathOfConnectingBookTestAfterCreateBook BasicSampleDT, 100, False
'End Sub
'
''''
''''
''''
'Private Sub msubSanityTest02ToConnectExcelBookTestAfterCreateBook()
'
'    GetOutputExcelBookPathOfConnectingBookTestAfterCreateBook IncludeFictionalProductionNumberSampleDT, 1000, True
'End Sub
'
'
''''
''''
''''
'Private Sub msubSanityTestToSQLJoinWithConnectingExcelBookAfterItIsCreated()
'
'    Dim strInputBookPath As String, strSheetName01 As String, strSheetName02 As String, strSQL As String
'
'    strInputBookPath = GetOutputBookPathAfterCreateALeastTestSheetSet()
'
'    With New XlAdoSheetExpander
'
'        .SetInputExcelBookPath strInputBookPath
'
'        strSheetName01 = "[" & GetSheetNameOfPreparedSampleDataTableExcelBookForTest(IncludeFictionalProductionNumberSampleDT) & "$]"
'
'        strSheetName02 = "[" & GetSheetNameOfPreparedSampleDataTableExcelBookForTest(FishTypeSampleMasterDT) & "$]"
'
'        strSQL = "SELECT * FROM " & strSheetName01
'
'        .OutputInExistedBookByUnitTestMode strSQL, "CreatedTable01"
'
'        strSQL = "SELECT * FROM " & strSheetName02
'
'        .OutputInExistedBookByUnitTestMode strSQL, "SampleMasterTable02"
'
'        strSQL = "SELECT A.RowNumber, A.RealNumber, B.FishTypeJpn, B.RandAlpha FROM " & strSheetName01 & " AS A LEFT JOIN " & strSheetName02 & " AS B ON A.FishType = B.FishTypeEng"
'
'        .OutputInExistedBookByUnitTestMode strSQL, "SQLResult"
'    End With
'
'End Sub
'
'
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''''
'''' This is also referenced from UTfReadFromShapeOnXlSheet.bas
''''
'Public Function GetOutputExcelBookPathOfConnectingBookTestAfterCreateBook(ByVal venmUnitTestPreparedSampleDataTable As UnitTestPreparedSampleDataTable, Optional ByVal vintCountOfRows As Long = 100, Optional ByVal vblnIncludesSQLLogToSheet As Boolean = True) As String
'
'    Dim strInputBookPath As String, strSQL As String, strLog As String, strSheetName As String
'    Dim strOutputBookPath As String
'
'    strInputBookPath = GetExcelBookPathAfterCheckFileExistenceOrCreateWhenItDoesntExist(strSheetName, venmUnitTestPreparedSampleDataTable, vintCountOfRows)
'
'    With New XlAdoSheetExpander
'
'        .SetInputExcelBookPath strInputBookPath
'
'        With .DataTableSheetFormattingInterface
'
'            Set .FieldTitleOrderToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("Col1,12,Col2,15")
'
'            Set .FieldTitleToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("FlowerTypeSample,19,RandomInteger,16")
'
'            .RecordBordersType = RecordCellsGrayAll
'        End With
'
'        strSQL = "SELECT * FROM [" & strSheetName & "$]"
'
'        .OutputInExistedBookByUnitTestMode strSQL, "Load_" & strSheetName, vblnIncludesSQLLogToSheet
'
'
'        strLog = GetLogTextOfPreparedSampleDataTableBaseName(venmUnitTestPreparedSampleDataTable)
'
'
'        AddAutoShapeGeneralMessage .CurrentSheet, strLog
'
'        strOutputBookPath = .CurrentBook.FullName
'
'        DeleteDummySheetWhenItExists .CurrentBook
'    End With
'
'    GetOutputExcelBookPathOfConnectingBookTestAfterCreateBook = strOutputBookPath
'End Function
'
'
'
''''
'''' XlAdoSheetExpander unit test
''''
'Public Sub SanityTestAboutXlSheetExpander()
'
'    Dim strInputPath As String, strOutputPath As String, strSheetName As String, objFieldTitleToColumnWidthDic As Scripting.Dictionary
'    Dim strSQL As String, strSQLTableName As String
'
'
'    GetBookPathAndSheetNameAndFieldTitleToColumnWidthDicFromUnitTestPreparedSampleDataTable strInputPath, strSheetName, objFieldTitleToColumnWidthDic, UnitTestPreparedSampleDataTable.BasicSampleDT
'
'    strOutputPath = GetCurrentBookOutputDir() & mstrOutputBaseName & ".xlsx"
'
'    With New XlAdoSheetExpander
'
'        .SetInputExcelBookPath strInputPath
'
'        .AllowToRecordSQLLog = True
'
'        Set .FieldTitleToColumnWidthDic = objFieldTitleToColumnWidthDic
'
'
'        strSQLTableName = "[" & strSheetName & "$]"
'
'        strSQL = "SELECT * FROM " & strSQLTableName
'
'        .OutputToSpecifiedBookByUnitTestMode strSQL, strOutputPath, "QueryTest", True
'    End With
'End Sub
'
'
'
'
'
'''--VBA_Code_File--<SqlUTfXlSheetExpanderByPassword.bas>--
'Attribute VB_Name = "SqlUTfXlSheetExpanderByPassword"
''
''   Sanity test to connect Excel sheet with a locked password for opening
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on both Excel and ADO
''       Dependent on UTfDataTableSecurity.bas, UTfDecorationSetterToXlSheet.bas
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Thu, 29/Jun/2023    Kalmclaeyd Tarclanus    Initial creation
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Constants
''///////////////////////////////////////////////
'
'Private Const mstrSheetName As String = "TestSheet"
'
'
''**---------------------------------------------
''** XlAdoConnector tests for password locked books
''**---------------------------------------------
''''
''''
''''
'Private Sub msubSanityTestToXlAdoConnectorForPasswordLockedBook()
'
'    Dim strBookPath As String, strSQL As String
'
'    strBookPath = GetBookSecurityTemporaryGeneratingDirectoryPath() & "\SamplePasswordLockedBook.xlsx"
'
'    PrepareTestBookWithLockingPasswordAndCreating strBookPath, mstrSheetName, "1234"
'
'    With New XlAdoConnector
'
'        .SetInputExcelBookPath strBookPath, vstrLockBookPassword:="1234"
'
'        strSQL = "SELECT * FROM [" & mstrSheetName & "$]"
'
'        DebugCol GetTableCollectionFromRSet(.GetRecordset(strSQL))
'    End With
'End Sub
'
''''
''''
''''
'Private Sub msubSanityTestToXlAdoExpanderForPasswordLockedBook()
'
'    Dim strBookPath As String, strSQL As String
'
'    strBookPath = GetBookSecurityTemporaryGeneratingDirectoryPath() & "\SamplePasswordLockedBook.xlsx"
'
'    PrepareTestBookWithLockingPasswordAndCreating strBookPath, mstrSheetName, "1234"
'
'    With New XlAdoSheetExpander
'
'        .SetInputExcelBookPath strBookPath, vstrLockBookPassword:="1234"
'
'        strSQL = "SELECT * FROM [" & mstrSheetName & "$]"
'
'        .OutputInExistedBookByUnitTestMode strSQL, "ATest", SqlQueryLogsByAutoShapesOnSheetWithOutputtingRecordsetDataTable
'
'        DeleteDummySheetWhenItExists .CurrentBook
'
'        .CloseAll
'    End With
'End Sub
'
'
''**---------------------------------------------
''** ADO connection tests
''**---------------------------------------------
''''
''''
''''
'Private Sub msubSanityTestToAdoConnectionToNoPasswordBook()
'
'    Dim strBookPath As String, objBook As Excel.Workbook
'    Dim objADOConStrOfAceOledbXl As ADOConStrOfAceOleDbXl, objConnection As ADODB.Connection
'
'
'    strBookPath = GetBookSecurityTemporaryGeneratingDirectoryPath() & "\SampleBookWithNoLocked.xlsx"
'
'    PrepareTestBookWithLockingPasswordAndCreating strBookPath, mstrSheetName, ""
'
'    Set objADOConStrOfAceOledbXl = New ADOConStrOfAceOleDbXl
'
'    With objADOConStrOfAceOledbXl
'
'        .SetExcelBookConnection strBookPath, NoUseIMEXMode
'    End With
'
'    Set objConnection = New ADODB.Connection
'
'    With objConnection
'
'        .ConnectionString = objADOConStrOfAceOledbXl.GetConnectionString
'
'        On Error Resume Next
'
'        .Open
'
'        If Err.Number = 0 Then
'
'            Debug.Print "Expected result to be connected"
'
'            .Close
'        End If
'
'        On Error GoTo 0
'    End With
'
'End Sub
'
''''
''''
''''
'Private Sub msubSanityTestToAdoConnectionToPasswordLockedBook()
'
'    Dim strBookPath As String, objBook As Excel.Workbook
'    Dim objADOConStrOfAceOledbXl As ADOConStrOfAceOleDbXl, objConnection As ADODB.Connection
'
'
'    strBookPath = GetBookSecurityTemporaryGeneratingDirectoryPath() & "\SamplePasswordLockedBook.xlsx"
'
'    PrepareTestBookWithLockingPasswordAndCreating strBookPath, mstrSheetName, "1234"
'
'    Set objADOConStrOfAceOledbXl = New ADOConStrOfAceOleDbXl
'
'    With objADOConStrOfAceOledbXl
'
'        .SetExcelBookConnection strBookPath, NoUseIMEXMode
'    End With
'
'    Set objConnection = New ADODB.Connection
'
'    With objConnection
'
'        .ConnectionString = objADOConStrOfAceOledbXl.GetConnectionString
'
'        On Error Resume Next
'
'        .Open
'
'        If Err.Number <> 0 Then
'
'            Debug.Print "Expected result to be failed connecting"
'        Else
'            .Close
'        End If
'
'        On Error GoTo 0
'    End With
'End Sub
'
'
''''
''''
''''
'Private Sub msubSanityTestToAdoConnectionToPasswordLockedBookWithOpening()
'
'    Dim strBookPath As String, objBook As Excel.Workbook
'    Dim objADOConStrOfAceOledbXl As ADOConStrOfAceOleDbXl, objConnection As ADODB.Connection
'
'
'    strBookPath = GetBookSecurityTemporaryGeneratingDirectoryPath() & "\SamplePasswordLockedBook.xlsx"
'
'    PrepareTestBookWithLockingPasswordAndCreating strBookPath, mstrSheetName, "1234"
'
'    Set objBook = GetWorkbook(strBookPath, False, vstrPassword:="1234")
'
'    Set objADOConStrOfAceOledbXl = New ADOConStrOfAceOleDbXl
'
'    With objADOConStrOfAceOledbXl
'
'        .SetExcelBookConnection strBookPath, NoUseIMEXMode
'    End With
'
'    Set objConnection = New ADODB.Connection
'
'    With objConnection
'
'        .ConnectionString = objADOConStrOfAceOledbXl.GetConnectionString
'
'        On Error Resume Next
'
'        .Open
'
'        If Err.Number = 0 Then
'
'            Debug.Print "Expected result to be connected, when the password locked Book is opened in this Excel"
'
'            .Close
'        End If
'
'        On Error GoTo 0
'    End With
'End Sub
'
'''--VBA_Code_File--<CsvAdoSheetExpander.cls>--
'VERSION 1.0 CLASS
'BEGIN
'  MultiUse = -1  'True
'END
'Attribute VB_Name = "CsvAdoSheetExpander"
'Attribute VB_GlobalNameSpace = False
'Attribute VB_Creatable = False
'Attribute VB_PredeclaredId = False
'Attribute VB_Exposed = False
''
''   output SQL result into Excel.Worksheet after connecting CSV file by ADO-DB interface
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on both ADO and Excel
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Fri, 10/Jun/2022    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
'
''///////////////////////////////////////////////
''/// Interface implementations
''///////////////////////////////////////////////
'Implements IOutputBookPath
'
''///////////////////////////////////////////////
''/// Declarations
''///////////////////////////////////////////////
'Private mobjConnector As CsvAdoConnector
'
'Private mobjAdoRecordsetSheetExpander As AdoRSetSheetExpander
'
'
''///////////////////////////////////////////////
''/// Event Handlers
''///////////////////////////////////////////////
'Private Sub Class_Initialize()
'
'    Set mobjConnector = New CsvAdoConnector
'
'    Set mobjAdoRecordsetSheetExpander = New AdoRSetSheetExpander
'
'    With mobjAdoRecordsetSheetExpander
'
'        Set .ADOConnectorInterface = mobjConnector
'
'        Set .OutputBookPathInterface = Me
'
'        .DataTableSheetFormattingInterface.FieldTitleInteriorType = ColumnNameInteriorOfCsvAdoSQL
'    End With
'End Sub
'
'Private Sub Class_Terminate()
'
'    mobjAdoRecordsetSheetExpander.CloseCacheOutputBookOfOpenByPresetModeProcess
'
'    Me.CloseConnection
'
'    Set mobjAdoRecordsetSheetExpander = Nothing
'
'    Set mobjConnector = Nothing
'End Sub
'
''///////////////////////////////////////////////
''/// Properties - connoted ADO connector
''///////////////////////////////////////////////
'Public Property Get SQLExecutionResult() As SQLResult
'
'    Set SQLExecutionResult = mobjConnector.SQLExecutionResult
'End Property
'
'Public Property Get IsSqlExecutionFailed() As Boolean
'
'    IsSqlExecutionFailed = mobjConnector.IsSqlExecutionFailed
'End Property
'
'Public Property Let AllowToRecordSQLLog(ByVal vblnAllowToRecord As Boolean)
'
'    mobjConnector.AllowToRecordSQLLog = vblnAllowToRecord
'End Property
'Public Property Get AllowToRecordSQLLog() As Boolean
'
'    AllowToRecordSQLLog = mobjConnector.AllowToRecordSQLLog
'End Property
'
'
'Public Property Get CurrentSheet() As Excel.Worksheet
'
'    Set CurrentSheet = mobjAdoRecordsetSheetExpander.IExpandAdoRecordsetOnSheet_CurrentSheet
'End Property
'
'
'Public Property Get CurrentBook() As Excel.Workbook
'
'    If Not Me.CurrentSheet Is Nothing Then
'
'        Set CurrentBook = Me.CurrentSheet.Parent
'    Else
'        Set CurrentBook = Nothing
'    End If
'End Property
'
''///////////////////////////////////////////////
''/// Properties
''///////////////////////////////////////////////
''''
''''
''''
'Public Property Get ADOConnectorInterface() As IADOConnector
'
'    Set ADOConnectorInterface = mitfConnector
'End Property
'
''''
''''
''''
'Public Property Get DataTableSheetFormattingInterface() As IDataTableSheetFormatter
'
'    Set DataTableSheetFormattingInterface = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface
'End Property
'
'
''**---------------------------------------------
''** connoted DataTableSheetFormatter
''**---------------------------------------------
'Public Property Get TopLeftRowIndex() As Long
'
'    TopLeftRowIndex = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.TopLeftRowIndex
'End Property
'Public Property Let TopLeftRowIndex(ByVal vintTopLeftRowIndex As Long)
'
'    mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.TopLeftRowIndex = vintTopLeftRowIndex
'End Property
'Public Property Get TopLeftColumnIndex() As Long
'
'    TopLeftColumnIndex = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.TopLeftColumnIndex
'End Property
'Public Property Let TopLeftColumnIndex(ByVal vintTopLeftColumnIndex As Long)
'
'    mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.TopLeftColumnIndex = vintTopLeftColumnIndex
'End Property
'
'
'Public Property Let AllowToShowFieldTitle(ByVal vblnAllowToShowFieldTitle As Boolean)
'
'    mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.AllowToShowFieldTitle = vblnAllowToShowFieldTitle
'End Property
'Public Property Get AllowToShowFieldTitle() As Boolean
'
'    AllowToShowFieldTitle = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.AllowToShowFieldTitle
'End Property
'
'
'
'Public Property Let AllowToSetAutoFilter(ByVal vblnAllowToSetAutoFilter As Boolean)
'
'    mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.AllowToSetAutoFilter = vblnAllowToSetAutoFilter
'End Property
'Public Property Get AllowToSetAutoFilter() As Boolean
'
'    AllowToSetAutoFilter = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.AllowToSetAutoFilter
'End Property
'
''''
'''' Dictionary(Of String(column title), Double(column-width))
''''
'Public Property Set FieldTitleToColumnWidthDic(ByVal vobjFieldTitleToColumnWidthDic As Scripting.Dictionary)
'
'    Set mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.FieldTitleToColumnWidthDic = vobjFieldTitleToColumnWidthDic
'End Property
'Public Property Get FieldTitleToColumnWidthDic() As Scripting.Dictionary
'
'    Set FieldTitleToColumnWidthDic = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.FieldTitleToColumnWidthDic
'End Property
'
''''
'''' Dictionary(Of String(column title order, such as 'Col1', 'Col07'), Double(column-width))
''''
'Public Property Set FieldTitleOrderToColumnWidthDic(ByVal vobjFieldTitleOrderToColumnWidthDic As Scripting.Dictionary)
'
'    Set mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.FieldTitleOrderToColumnWidthDic = vobjFieldTitleOrderToColumnWidthDic
'End Property
'Public Property Get FieldTitleOrderToColumnWidthDic() As Scripting.Dictionary
'
'    Set FieldTitleOrderToColumnWidthDic = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.FieldTitleOrderToColumnWidthDic
'End Property
'
'
''''
'''' When it is true, then ConvertDataForColumns procedure is used.
''''
'Public Property Get AllowToConvertDataForSpecifiedColumns() As Boolean
'
'    AllowToConvertDataForSpecifiedColumns = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.AllowToConvertDataForSpecifiedColumns
'End Property
'Public Property Let AllowToConvertDataForSpecifiedColumns(ByVal vblnAllowToConvertDataForSpecifiedColumns As Boolean)
'
'    mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.AllowToConvertDataForSpecifiedColumns = vblnAllowToConvertDataForSpecifiedColumns
'End Property
''''
'''' Dictionary(Of String(column title), ColumnDataConvertType(Enum definition))
''''
'Public Property Get FieldTitlesToColumnDataConvertTypeDic() As Scripting.Dictionary
'
'    Set FieldTitlesToColumnDataConvertTypeDic = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.FieldTitlesToColumnDataConvertTypeDic
'End Property
'Public Property Set FieldTitlesToColumnDataConvertTypeDic(ByVal vobjFieldTitlesToColumnDataConvertTypeDic As Scripting.Dictionary)
'
'    Set mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.FieldTitlesToColumnDataConvertTypeDic = vobjFieldTitlesToColumnDataConvertTypeDic
'End Property
'
''''
'''' ColumnsNumberFormatLocalParam
''''
'Public Property Set ColumnsNumberFormatLocal(ByVal vobjColumnsNumberFormatLocalParam As ColumnsNumberFormatLocalParam)
'
'    Set mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.ColumnsNumberFormatLocal = vobjColumnsNumberFormatLocalParam
'End Property
'Public Property Get ColumnsNumberFormatLocal() As ColumnsNumberFormatLocalParam
'
'    Set ColumnsNumberFormatLocal = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.ColumnsNumberFormatLocal
'End Property
'
''''
'''' set format-condition for each column
''''
'Public Property Set ColumnsFormatCondition(ByVal vobjColumnsFormatConditionParam As ColumnsFormatConditionParam)
'
'    Set mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.ColumnsFormatCondition = vobjColumnsFormatConditionParam
'End Property
'Public Property Get ColumnsFormatCondition() As ColumnsFormatConditionParam
'
'    Set ColumnsFormatCondition = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.ColumnsFormatCondition
'End Property
'
''''
'''' merge cells when the same values continue
''''
'Public Property Let AllowToMergeCellsByContinuousSameValues(ByVal vblnAllowToMergeCellsByContinuousSameValues As Boolean)
'
'    mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.AllowToMergeCellsByContinuousSameValues = vblnAllowToMergeCellsByContinuousSameValues
'End Property
'Public Property Get AllowToMergeCellsByContinuousSameValues() As Boolean
'
'    AllowToMergeCellsByContinuousSameValues = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.AllowToMergeCellsByContinuousSameValues
'End Property
'Public Property Let AllowToSetVerticalTopAlignmentTopOfMergeCells(ByVal vblnAllowToSetVerticalTopAlignmentTopOfMergeCells As Boolean)
'
'    mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.AllowToSetVerticalTopAlignmentTopOfMergeCells = vblnAllowToSetVerticalTopAlignmentTopOfMergeCells
'End Property
'Public Property Get AllowToSetVerticalTopAlignmentTopOfMergeCells() As Boolean
'
'    AllowToSetVerticalTopAlignmentTopOfMergeCells = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.AllowToSetVerticalTopAlignmentTopOfMergeCells
'End Property
'
'Public Property Set FieldTitlesToMergeCells(ByVal vobjFieldTitlesToMergeCells As Collection)
'
'    Set mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.FieldTitlesToMergeCells = vobjFieldTitlesToMergeCells
'End Property
'Public Property Get FieldTitlesToMergeCells()
'
'    Set FieldTitlesToMergeCells = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.FieldTitlesToMergeCells
'End Property
'
'Public Property Let FieldTitleInteriorType(ByVal venmFieldTitleInteriorType As FieldTitleInterior)
'
'    mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.FieldTitleInteriorType = venmFieldTitleInteriorType
'End Property
'Public Property Get FieldTitleInteriorType() As FieldTitleInterior
'
'    FieldTitleInteriorType = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.FieldTitleInteriorType
'End Property
'
'Public Property Let RecordBordersType(ByVal venmRecordCellsBordersType As RecordCellsBorders)
'
'    mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.RecordBordersType = venmRecordCellsBordersType
'End Property
'Public Property Get RecordBordersType() As RecordCellsBorders
'
'    RecordBordersType = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.RecordBordersType
'End Property
'
''///////////////////////////////////////////////
''/// Properties - connoted ADOConnectionSetting
''///////////////////////////////////////////////
'Public Property Get ConnectSetting() As ADOConnectionSetting
'
'    Set ConnectSetting = mobjConnector.ConnectSetting
'End Property
'
''''
'''' unit is second. [s]
''''
'Public Property Let CommandTimeout(ByVal vintTimeout As Long)
'
'    mobjConnector.CommandTimeout = vintTimeout
'End Property
'Public Property Get CommandTimeout() As Long
'
'    CommandTimeout = mobjConnector.CommandTimeout
'End Property
'
''///////////////////////////////////////////////
''/// Properties - CSV files connection by ACCESS OLE-DB provider
''///////////////////////////////////////////////
''''
'''' connecting CSV file path
''''
'Public Property Get ConnectingFilePath() As String
'
'    ConnectingFilePath = mobjConnector.ConnectingFilePath
'End Property
'
''''
'''' connecting directory path, which includes CSV files
''''
'Public Property Get ConnectingDirectoryPath() As String
'
'    ConnectingDirectoryPath = mobjConnector.ConnectingDirectoryPath
'End Property
'
''''
'''' when the load Excel sheet doesn't have the field header title text, this is False
''''
'Public Property Get HeaderFieldTitleExists() As Boolean
'
'    HeaderFieldTitleExists = mobjConnector.HeaderFieldTitleExists
'End Property
'
'
''///////////////////////////////////////////////
''/// Implemented operations
''///////////////////////////////////////////////
'Private Function IOutputBookPath_GetPreservedOutputBookPath() As String
'
'    IOutputBookPath_GetPreservedOutputBookPath = GetCsvAdoConnectingGeneralLogBookPath()
'End Function
'
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''**---------------------------------------------
''** preparation before SQL execution
''**---------------------------------------------
''''
'''' set input CSV file path
''''
'Public Sub SetInputCsvFileConnection(ByVal vstrCSVFilePath As String, _
'        Optional ByVal vblnHeaderFieldTitleExists As Boolean = True, _
'        Optional ByVal venmAccessOLEDBProviderType As AccessOLEDBProviderType = AccessOLEDBProviderType.AceOLEDB_12P0)
'
'    mobjConnector.SetInputCsvFileConnection vstrCSVFilePath, vblnHeaderFieldTitleExists, venmAccessOLEDBProviderType
'End Sub
'
''''
'''' set input directory path, which includes CSV files
''''
'Public Sub SetInputCsvDirectoryConnection(ByVal vstrCsvDirectoryPath As String, _
'        Optional ByVal vblnHeaderFieldTitleExists As Boolean = True, _
'        Optional ByVal venmAccessOLEDBProviderType As AccessOLEDBProviderType = AccessOLEDBProviderType.AceOLEDB_12P0)
'
'    mobjConnector.SetInputCsvDirectoryConnection vstrCsvDirectoryPath, vblnHeaderFieldTitleExists, venmAccessOLEDBProviderType
'End Sub
'
''''
'''' Close ADO connection
''''
'Public Sub CloseConnection()
'
'    mobjConnector.CloseConnection
'End Sub
'
''''
''''
''''
'Public Sub CloseCacheOutputBookOfOpenByPresetModeProcess()
'
'    mobjAdoRecordsetSheetExpander.CloseCacheOutputBookOfOpenByPresetModeProcess
'End Sub
'
''''
''''
''''
'Public Sub CloseAll()
'
'    CloseConnection
'
'    CloseCacheOutputBookOfOpenByPresetModeProcess
'End Sub
'
''''
'''' get recordset object by the Select SQL
''''
'Public Function GetRecordset(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag) As ADODB.Recordset
'
'    Set GetRecordset = mobjConnector.GetRecordset(vstrSQL, venmShowUpAdoErrorOptionFlag)
'End Function
'
'
''**---------------------------------------------
''** Output Recordset and logs to cells on sheet
''**---------------------------------------------
''''
'''' query SQL and output result to Excel sheet
''''
'Public Sub OutputToSheetFrom(ByVal vstrSQL As String, _
'        ByVal vobjOutputSheet As Excel.Worksheet, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'    mobjAdoRecordsetSheetExpander.OutputToSheetFrom vstrSQL, vobjOutputSheet, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedCSVPathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
'''' query SQL and output result to the Existed Excel sheet
''''
'Public Sub OutputInExistedBook(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputInExistedBook vstrSQL, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedCSVPathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
'Public Sub OutputInAlreadyExistedBook(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputInAlreadyExistedBook vstrSQL, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedCSVPathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
''''
''''
'Public Sub OutputInExistedBookByUnitTestMode(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputInExistedBookByUnitTestMode vstrSQL, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedCSVPathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputToSpecifiedBook(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputToSpecifiedBook vstrSQL, vstrOutputBookPath, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedCSVPathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
'Public Sub OutputToSpecifiedBookByUnitTestMode(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'    mobjAdoRecordsetSheetExpander.OutputToSpecifiedBookByUnitTestMode vstrSQL, vstrOutputBookPath, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedCSVPathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
'''' query SQL and output result to the already specified Excel sheet
''''
'Public Sub OutputToAlreadySpecifiedBook(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'    mobjAdoRecordsetSheetExpander.OutputToAlreadySpecifiedBook vstrSQL, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedCSVPathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
''''
''''
'Public Sub SetOutputExcelBookPath(ByVal vstrOutputBookPath As String, _
'        Optional ByVal venmOutputRSetExcelBookOpenModeProcessFlag As OutputRSetExcelBookOpenModeProcessFlag = OutputRSetExcelBookOpenModeProcessFlag.OpenOutputBookByUnitTestMode, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag)
'
'    mobjAdoRecordsetSheetExpander.SetOutputExcelBookPath vstrOutputBookPath, venmOutputRSetExcelBookOpenModeProcessFlag
'End Sub
'
''**---------------------------------------------
''** Excecute SQL command (UPDATE, INSERT, DELETE)
''**---------------------------------------------
''''
'''' command SQL and output result-log to Excel sheet
''''
'Public Sub OutputCommandLogToSheetFrom(ByVal vstrSQL As String, _
'        ByVal vobjOutputSqlCommandLogSheet As Excel.Worksheet, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogToSheetFrom vstrSQL, vobjOutputSqlCommandLogSheet, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedCSVPathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputCommandLogToSpecifiedBook(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogToSpecifiedBook vstrSQL, vstrOutputBookPath, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedCSVPathInsertText(), vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputCommandLogToAlreadySpecifiedBook(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogToAlreadySpecifiedBook vstrSQL, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedCSVPathInsertText(), vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputCommandLogInAlreadyExistedBook(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogInAlreadyExistedBook vstrSQL, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedCSVPathInsertText(), vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputCommandLogToSpecifiedBookByUnitTestMode(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogToSpecifiedBookByUnitTestMode vstrSQL, vstrOutputBookPath, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedCSVPathInsertText(), vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
'''' query SQL and output result to the prepared Excel sheet
''''
'Public Sub OutputCommandLogInExistedBookByUnitTestMode(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogInExistedBookByUnitTestMode vstrSQL, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedCSVPathInsertText(), vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
'Private Function mfobjGetConnectedCSVPathInsertText() As Collection
'
'    Dim objInsertTexts As Collection
'
'    Set objInsertTexts = New Collection
'
'    With Me
'        If FileExistsByVbaDir(.ConnectingFilePath) Then
'
'            objInsertTexts.Add "<File Name> " & GetFileNameFromPathByVbaDir(.ConnectingFilePath) & ", <CSV Directory> " & GetParentDirectoryPathByVbaDir(.ConnectingFilePath)
'        Else
'            objInsertTexts.Add "<CSV Directory> " & .ConnectingDirectoryPath
'        End If
'    End With
'
'    Set mfobjGetConnectedCSVPathInsertText = objInsertTexts
'End Function
'
'
'''--VBA_Code_File--<SqlUTfCsvAdoSheetExpander.bas>--
'Attribute VB_Name = "SqlUTfCsvAdoSheetExpander"
''
''   Sanity test to output SQL result into a Excel sheet for CSV files in a directory using ADO OLE DB
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on both ADO and Excel
''       Dependent on Microsoft Office Access Data Base Engine OLE DB Provider,
''       which should have been the installed when the Microsoft Office Excel software has been installed.
''       Dependent on UTfCreateDataTable.bas
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Wed, 26/Jul/2023    Kalmclaeyd Tarclanus    Redesigned
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Constants
''///////////////////////////////////////////////
'Private Const mstrTestCSVFileNamePrefix As String = "TestGenerated"
'
'
''///////////////////////////////////////////////
''/// Sanity tests
''///////////////////////////////////////////////
''**---------------------------------------------
''** CsvAdoSheetExpander test
''**---------------------------------------------
''''
'''' Test-connecting
''''
'Private Sub ConnectTestToCSVFileByADO()
'
'    Dim strSQL As String, strPath As String, blnCSVGenerated As Boolean
'
'
'    blnCSVGenerated = False
'
'    strPath = mfstrGetSimpleTestCSVFilePath
'
'    With New Scripting.FileSystemObject
'
'        If Not .FileExists(strPath) Then
'
'            msubGenerateTestingCSVFile
'
'            blnCSVGenerated = True
'        End If
'    End With
'
'
'    With New CsvAdoSheetExpander
'
'        .SetInputCsvFileConnection strPath
'
'        strSQL = "SELECT * FROM " & GetCSVTableNameLeteral(strPath)
'
'
'        With .DataTableSheetFormattingInterface
'
'            Set .FieldTitleToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("col3,13")
'
'            Set .ColumnsNumberFormatLocal = New ColumnsNumberFormatLocalParam
'
'            With .ColumnsNumberFormatLocal
'
'                .SetNumberFormatLocalAndApplyFieldTitles "ddd, yyyy/m/d", GetColFromLineDelimitedChar("col3")
'            End With
'        End With
'
'
'        .OutputInExistedBookByUnitTestMode strSQL, "Load_sample_csv"
'
'
'        If blnCSVGenerated Then
'
'            AddAutoShapeGeneralMessage .CurrentSheet, CreateObject("Scripting.FileSystemObject").GetFileName(strPath) & " is created now."
'        End If
'    End With
'End Sub
'
''**---------------------------------------------
''** CsvAdoConnector connecting by generated sample CSV
''**---------------------------------------------
''''
''''
''''
'Private Sub msubSanityTest01ToConnectCSVTestAfterCSVCreate()
'
'    GetOutputBookPathOfConnectingAdoCSVTestAfterCSVCreate BasicSampleDT, 100, False
'End Sub
'
'
'Private Sub msubSanityTest02ToConnectCSVTestAfterCSVCreate()
'
'    GetOutputBookPathOfConnectingAdoCSVTestAfterCSVCreate IncludeFictionalProductionNumberSampleDT, 10, True
'End Sub
'
'
'Private Sub msubSanityTest02ToConnectCSVTestAfterCSVSampleMasterTableOfFishType()
'
'    GetOutputBookPathOfConnectingAdoCSVTestAfterCSVCreate FishTypeSampleMasterDT, vblnIncludesSQLLogToSheet:=True
'End Sub
'
'Private Sub msubSanityTest02ToConnectCSVTestAfterCSVSampleMasterTableOfFlowerType()
'
'    GetOutputBookPathOfConnectingAdoCSVTestAfterCSVCreate FlowerTypeSampleMasterDT, vblnIncludesSQLLogToSheet:=True
'End Sub
'
'
''''
''''
''''
'Private Sub msubSanityTestToConectCSVWithJOINSQL()
'
'    Dim strInputCsvPath01 As String, strInputCsvPath02 As String
'    Dim strTableName01 As String, strTableName02 As String, strDir As String
'    Dim strSQL As String
'
'    strInputCsvPath01 = GetCSVPathAfterCreateSampleDataTable(IncludeFictionalProductionNumberSampleDT, 1000)
'
'    strInputCsvPath02 = GetCSVPathAfterCreateSampleDataTable(FishTypeSampleMasterDT)
'
'    With New Scripting.FileSystemObject
'
'        strDir = .GetParentFolderName(strInputCsvPath01)
'
'        strTableName01 = "[" & .GetFileName(strInputCsvPath01) & "]"
'
'        strTableName02 = "[" & .GetFileName(strInputCsvPath02) & "]"
'
'    End With
'
'    With New CsvAdoSheetExpander
'
'        .SetInputCsvDirectoryConnection strDir
'
'        .RecordBordersType = RecordCellsGrayAll
'
'        Set .FieldTitleToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("RowNumber,12,RealNumber,12,FishType,11,FictionalProductionSerialNumber,30,FishTypeJpn,11,RandAlpha,11")
'
'
'        strSQL = "SELECT * FROM " & strTableName01
'
'        .OutputInExistedBookByUnitTestMode strSQL, "1stTable"
'
'        strSQL = "SELECT * FROM " & strTableName02
'
'        .OutputInExistedBookByUnitTestMode strSQL, "2ndTable"
'
'        'strSQL = "SELECT A.RowNumber, A.RealNumber, A.FishType FROM " & strTableName01 & " AS A"
'
'        strSQL = "SELECT A.RowNumber, A.RealNumber, A.FishType, B.FishTypeJpn, B.RandAlpha FROM " & strTableName01 & " AS A LEFT JOIN " & strTableName02 & " AS B ON A.FishType = B.FishTypeEng"
'
'
'        .OutputInExistedBookByUnitTestMode strSQL, "SQLResult"
'
'        AddAutoShapeGeneralMessage .CurrentSheet, "The SQL 'JOIN' can be also used by connecting plural CSV files in a conntected directory"
'    End With
'End Sub
'
'
''**---------------------------------------------
''** CSV directory connection tests
''**---------------------------------------------
''''
''''
''''
'Private Sub msubSanityTestToTryToConnectCSVDirectory()
'
'    Dim strSQL As String, strPath As String, blnCSVGenerated As Boolean
'    Dim objCsvADOConnectStrGenerator As ADOConStrOfAceOleDbCsv
'
'    blnCSVGenerated = False
'
'    msubDeleteTestingCSVFile    ' If the directory path exists, the ADO connection is to be succeeded even though no CSV file exist.
'
'    strPath = mfstrGetSimpleTestCSVFilePath
'
'    Set objCsvADOConnectStrGenerator = New ADOConStrOfAceOleDbCsv
'
'    objCsvADOConnectStrGenerator.SetCsvFileConnection strPath
'
'    ConnectToCsvFileByAdo objCsvADOConnectStrGenerator.GetConnectionString
'
'    ' Since no CSV file exist, the SQL will be failed.
'End Sub
'
''''
'''' Confirm that an error occurs
''''
'Private Sub msubSanityTestToTryToConnectCSVDirectoryWhichDoesNotExist()
'
'    Dim strSQL As String, strPath As String, blnCSVGenerated As Boolean
'    Dim objCsvADOConnectStrGenerator As ADOConStrOfAceOleDbCsv
'
'    blnCSVGenerated = False
'
'    strPath = mfstrGetTempCSVDirectoryPath("TemporaryCSF02", False)
'
'    Set objCsvADOConnectStrGenerator = New ADOConStrOfAceOleDbCsv
'
'    objCsvADOConnectStrGenerator.SetCsvDirectoryConnection strPath
'
'    ConnectToCsvFileByAdo objCsvADOConnectStrGenerator.GetConnectionString
'End Sub
'
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''''
''''
''''
'Public Function GetOutputBookPathOfConnectingAdoCSVTestAfterCSVCreate(ByVal venmUnitTestPreparedSampleDataTable As UnitTestPreparedSampleDataTable, Optional ByVal vintCountOfRows As Long = 100, Optional ByVal vblnIncludesSQLLogToSheet As Boolean = True) As String
'
'    Dim strInputCSVPath As String, strSQL As String, strLog As String
'    Dim strOutputBookPath As String
'
'    strInputCSVPath = GetCSVPathAfterCheckFileExistenceOrCreateWhenItDoesntExist(venmUnitTestPreparedSampleDataTable, vintCountOfRows)
'
'    With New CsvAdoSheetExpander
'
'        .SetInputCsvFileConnection strInputCSVPath
'
'        .DataTableSheetFormattingInterface.RecordBordersType = RecordCellsGrayAll
'
'        strSQL = "SELECT * FROM " & GetCSVTableNameLeteral(strInputCSVPath)
'
'        .OutputInExistedBookByUnitTestMode strSQL, "Load_sample_csv", vblnIncludesSQLLogToSheet
'
'
'        strLog = GetLogTextOfPreparedSampleDataTableBaseName(venmUnitTestPreparedSampleDataTable)
'
'
'        AddAutoShapeGeneralMessage .CurrentSheet, strLog
'
'        strOutputBookPath = .CurrentBook.FullName
'
'        .CloseAll
'    End With
'
'    GetOutputBookPathOfConnectingAdoCSVTestAfterCSVCreate = strOutputBookPath
'End Function
'
'
'
'
''''
'''' CSV directory connection test
''''
'Public Sub ConnectToCsvFileByAdo(ByVal vstrConnectionString As String)
'
'    Dim objCon As ADODB.Connection
'
'    Set objCon = New ADODB.Connection
'
'    With objCon
'
'        .ConnectionString = vstrConnectionString
'
'        On Error GoTo ErrHandler
'
'        .Open   ' If the directory path exists, the ADO connection is to be succeeded even though no CSV file exist.
'
'        .Close
'
'ErrHandler:
'        If Err.Number <> 0 Then
'
'            Debug.Print "Failed to connect directory of CSV files: " & vstrConnectionString
'
'            MsgBox "Failed to connect directory of CSV files: " & vbNewLine & vstrConnectionString, vbCritical Or vbOKOnly
'        Else
'            Debug.Print "Connected to the directory of CSV files"
'        End If
'
'        On Error GoTo 0
'    End With
'End Sub
'
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
''**---------------------------------------------
''** test CSV file generation
''**---------------------------------------------
''''
''''
''''
'Private Sub msubGenerateTestingCSVFile()
'
'    Dim strPath As String
'
'    strPath = mfstrGetSimpleTestCSVFilePath
'
'    With New Scripting.FileSystemObject
'
'        With .CreateTextFile(strPath, True)
'
'            ' field title
'            .WriteLine "co1,col2,col3"
'
'            ' data
'            .WriteLine "abc,123," & Format(DateAdd("d", -1, Now()), "yyyy/m/d") '  2021/6/28"
'
'            .WriteLine "def,456," & Format(DateAdd("d", -2, Now()), "yyyy/m/d") '  2021/6/29"
'
'            .WriteLine "ghi,789," & Format(DateAdd("d", -3, Now()), "yyyy/m/d") '  2021/6/30"
'
'            .Close
'        End With
'    End With
'End Sub
'
''''
'''' delete a generated sample CSV file
''''
'Private Sub msubDeleteTestingCSVFile()
'
'    Dim strPath As String
'
'    strPath = mfstrGetSimpleTestCSVFilePath
'
'    With New Scripting.FileSystemObject
'
'        If .FileExists(strPath) Then
'
'            .DeleteFile strPath, True
'        End If
'    End With
'End Sub
'
''''
''''
''''
'Private Function mfstrGetSimpleTestCSVFilePath()
'
'    Dim strDir As String, strFileName As String, strPath As String
'
'
'    strDir = mfstrGetTempCSVDirectoryPath
'
'    strFileName = mstrTestCSVFileNamePrefix & ".csv"
'
'    strPath = strDir & "\" & strFileName
'
'    mfstrGetSimpleTestCSVFilePath = strPath
'End Function
'
'
''''
''''
''''
'Private Function mfstrGetTempCSVDirectoryPath(Optional ByVal vstrChildDirectoryName As String = "TemporaryCSVFiles", Optional ByVal vblnAllowToCreateDirectory As Boolean = True) As String
'    Dim strDir As String
'
'    With New Scripting.FileSystemObject
'
'        strDir = GetCurrentOfficeFileObject().Path & "\" & vstrChildDirectoryName
'
'        If vblnAllowToCreateDirectory Then
'
'            ForceToCreateDirectory strDir
'        End If
'    End With
'
'    mfstrGetTempCSVDirectoryPath = strDir
'End Function
'
'
'
'
'
'
'''--VBA_Code_File--<UTfAdoConnectAccDbForXl.bas>--
'Attribute VB_Name = "UTfAdoConnectAccDbForXl"
''
''   Serving Microsoft Access db serving tests by ADO without Access application
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on both ADO and Excel
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Tue, 16/May/2023    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
'
''///////////////////////////////////////////////
''/// Sanity tests
''///////////////////////////////////////////////
''''
''''
''''
'Private Sub msubSanityTestToLoadAccDbTableToExcelSheetWithUsingQueryTable()
'
'    Dim strDbPath As String, strDbName As String, strSQL As String, strConnectionString As String
'    Dim strBookName As String
'    Dim strOutputBookPath As String, objBook As Excel.Workbook, objSheet As Excel.Worksheet
'    Dim objQueryTable As Excel.QueryTable
'
'
'    strDbName = "TestFromVba01.accdb"
'
'    strDbPath = GetPathAfterCreateSampleAccDb(strDbName)
'
'
'
'    strBookName = Replace(strDbName, ".", "_") & ".xlsx"
'
'    ' prepare a AccDb
'    strOutputBookPath = mfstrGetTemporaryBookDir() & "\" & strBookName
'
'    Set objSheet = GetWorkbookAndAnWorksheetPrepareForUnitTest(strOutputBookPath, "FromAccDb")
'
'
'    'strSQL = "select * from TestTable;"
'
'    strSQL = "select * from TestTable"
'
'    strConnectionString = "ODBC;DSN=MS Access Database;DBQ=" & strDbPath
'
'
'    Set objQueryTable = objSheet.QueryTables.Add(strConnectionString, objSheet.Range("A1"), strSQL)
'
'    With objQueryTable
'
'        .Connection
'
'        .Name = "A_Query"
'
'        '.BackgroundQuery = False  ' prevent the back-ground processing
'
'        .Refresh
'
'        '.Delete  ' delete query-table
'    End With
'End Sub
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
''''
''''
''''
'Private Function mfstrGetTemporaryBookDir() As String
'
'    Dim strDir As String
'
'    strDir = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\LoadedFromAccDb"
'
'    ForceToCreateDirectory strDir
'
'    mfstrGetTemporaryBookDir = strDir
'End Function
'
'''--VBA_Code_File--<AccDbAdoSheetExpander.cls>--
'VERSION 1.0 CLASS
'BEGIN
'  MultiUse = -1  'True
'END
'Attribute VB_Name = "AccDbAdoSheetExpander"
'Attribute VB_GlobalNameSpace = False
'Attribute VB_Creatable = False
'Attribute VB_PredeclaredId = False
'Attribute VB_Exposed = False
''
''   output SQL result into Excel.Worksheet after connecting Access database by ADO OLE DB interface
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on both ADO and Excel
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Wed, 26/Jul/2023    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Interface implementations
''///////////////////////////////////////////////
'Implements IOutputBookPath
'
''///////////////////////////////////////////////
''/// Declarations
''///////////////////////////////////////////////
'Private mobjConnector As AccDbAdoConnector
'
'Private mobjAdoRecordsetSheetExpander As AdoRSetSheetExpander
'
'
''///////////////////////////////////////////////
''/// Event Handlers
''///////////////////////////////////////////////
'Private Sub Class_Initialize()
'
'    Set mobjConnector = New AccDbAdoConnector
'
'    Set mobjAdoRecordsetSheetExpander = New AdoRSetSheetExpander
'
'    With mobjAdoRecordsetSheetExpander
'
'        Set .ADOConnectorInterface = mobjConnector
'
'        Set .OutputBookPathInterface = Me
'
'        .DataTableSheetFormattingInterface.FieldTitleInteriorType = ColumnNameInteriorOrMSAccessRDBSQL
'    End With
'End Sub
'
''''
''''
''''
'Private Sub Class_Terminate()
'
'    mobjAdoRecordsetSheetExpander.CloseCacheOutputBookOfOpenByPresetModeProcess
'
'    Debug.Print "class AccDbAdoSheetExpander terminate -> " & mobjAdoRecordsetSheetExpander Is Nothing
'
'    Me.CloseConnection
'
'    Set mobjAdoRecordsetSheetExpander = Nothing
'
'    Set mobjConnector = Nothing
'End Sub
'
'
''///////////////////////////////////////////////
''/// Properties - connoted ADO connector
''///////////////////////////////////////////////
'Public Property Get SQLExecutionResult() As SQLResult
'
'    Set SQLExecutionResult = mobjConnector.SQLExecutionResult
'End Property
'
'Public Property Get IsSqlExecutionFailed() As Boolean
'
'    IsSqlExecutionFailed = mobjConnector.IsSqlExecutionFailed
'End Property
'
'Public Property Let AllowToRecordSQLLog(ByVal vblnAllowToRecord As Boolean)
'
'    mobjConnector.AllowToRecordSQLLog = vblnAllowToRecord
'End Property
'Public Property Get AllowToRecordSQLLog() As Boolean
'
'    AllowToRecordSQLLog = mobjConnector.AllowToRecordSQLLog
'End Property
'
'
'Public Property Get CurrentSheet() As Excel.Worksheet
'
'    Set CurrentSheet = mobjAdoRecordsetSheetExpander.IExpandAdoRecordsetOnSheet_CurrentSheet
'End Property
'
'
'Public Property Get CurrentBook() As Excel.Workbook
'
'    If Not Me.CurrentSheet Is Nothing Then
'
'        Set CurrentBook = Me.CurrentSheet.Parent
'    Else
'        Set CurrentBook = Nothing
'    End If
'End Property
'
'
''///////////////////////////////////////////////
''/// Properties
''///////////////////////////////////////////////
''''
''''
''''
'Public Property Get ADOConnectorInterface() As IADOConnector
'
'    Set ADOConnectorInterface = mitfConnector
'End Property
'
''''
''''
''''
'Public Property Get DataTableSheetFormattingInterface() As IDataTableSheetFormatter
'
'    Set DataTableSheetFormattingInterface = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface
'End Property
'
'
''///////////////////////////////////////////////
''/// Properties - connoted ADOConnectionSetting
''///////////////////////////////////////////////
'Public Property Get ConnectSetting() As ADOConnectionSetting
'
'    Set ConnectSetting = mobjConnector.ConnectSetting
'End Property
'
''''
'''' unit is second. [s]
''''
'Public Property Let CommandTimeout(ByVal vintTimeout As Long)
'
'    mobjConnector.CommandTimeout = vintTimeout
'End Property
'Public Property Get CommandTimeout() As Long
'
'    CommandTimeout = mobjConnector.CommandTimeout
'End Property
'
'
''///////////////////////////////////////////////
''/// Properties - Access database connection by ACCESS OLE-DB provider
''///////////////////////////////////////////////
'Public Property Get ConnectingFilePath() As String
'
'    ConnectingFilePath = mobjConnector.ConnectingFilePath
'End Property
'
'
''///////////////////////////////////////////////
''/// Implemented operations
''///////////////////////////////////////////////
''''
''''
''''
'Private Function IOutputBookPath_GetPreservedOutputBookPath() As String
'
'    IOutputBookPath_GetPreservedOutputBookPath = GetAccDbConnectingPrearrangedLogBookPath()
'End Function
'
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''**---------------------------------------------
''** set ADO connection string all parameters directly
''**---------------------------------------------
''''
''''
''''
'Public Sub SetMsAccessConnection(ByVal vstrDbPath As String, _
'        Optional ByVal vstrOldTypeDbPassword As String = "", _
'        Optional ByVal venmAccessOLEDBProviderType As AccessOLEDBProviderType = AccessOLEDBProviderType.AceOLEDB_12P0)
'
'
'    mobjConnector.SetMsAccessConnection vstrDbPath, vstrOldTypeDbPassword, venmAccessOLEDBProviderType
'End Sub
'
''**---------------------------------------------
''** set ADO connection string parameters by a User-form interface
''**---------------------------------------------
''''
''''
''''
'''' <Argument>vstrSettingKeyName: Input</Argument>
'''' <Argument>vstrDbPath: Input</Argument>
'''' <Argument>venmAccessOLEDBProviderType: Input</Argument>
'''' <Argument>vstrOldTypeDbPassword: Input</Argument>
'''' <Return>Boolean</Return>
'Public Function IsAdoConnectionContinuedAfterAccessOleDbConnectionParametersByForm(ByVal vstrSettingKeyName As String, _
'        Optional ByVal vstrDbPath As String = "", _
'        Optional ByVal venmAccessOLEDBProviderType As AccessOLEDBProviderType = AccessOLEDBProviderType.AceOLEDB_12P0, _
'        Optional ByVal vstrOldTypeDbPassword As String = "")
'
'
'    IsAdoConnectionContinuedAfterAccessOleDbConnectionParametersByForm = mobjConnector.IsAdoConnectionContinuedAfterAccessOleDbConnectionParametersByForm(vstrSettingKeyName, vstrDbPath, venmAccessOLEDBProviderType, vstrOldTypeDbPassword)
'End Function
'
''''
'''' Close ADO connection
''''
'Public Sub CloseConnection()
'
'    mobjConnector.CloseConnection
'End Sub
'
''''
''''
''''
'Public Sub CloseCacheOutputBookOfOpenByPresetModeProcess()
'
'    mobjAdoRecordsetSheetExpander.CloseCacheOutputBookOfOpenByPresetModeProcess
'End Sub
'
''''
''''
''''
'Public Sub CloseAll()
'
'    CloseConnection
'
'    CloseCacheOutputBookOfOpenByPresetModeProcess
'End Sub
'
''**---------------------------------------------
''** Output Recordset and logs to cells on sheet
''**---------------------------------------------
''''
'''' query SQL and output result to Excel sheet
''''
'Public Sub OutputToSheetFrom(ByVal vstrSQL As String, _
'        ByVal vobjOutputSheet As Excel.Worksheet, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'    mobjAdoRecordsetSheetExpander.OutputToSheetFrom vstrSQL, vobjOutputSheet, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedAccDbPathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
'''' query SQL and output result to the Existed Excel sheet
''''
'Public Sub OutputInExistedBook(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputInExistedBook vstrSQL, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedAccDbPathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
'Public Sub OutputInAlreadyExistedBook(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputInAlreadyExistedBook vstrSQL, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedAccDbPathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
''''
''''
'Public Sub OutputInExistedBookByUnitTestMode(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputInExistedBookByUnitTestMode vstrSQL, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedAccDbPathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputToSpecifiedBook(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputToSpecifiedBook vstrSQL, vstrOutputBookPath, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedAccDbPathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
'Public Sub OutputToSpecifiedBookByUnitTestMode(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'    mobjAdoRecordsetSheetExpander.OutputToSpecifiedBookByUnitTestMode vstrSQL, vstrOutputBookPath, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedAccDbPathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
'''' query SQL and output result to the already specified Excel sheet
''''
'Public Sub OutputToAlreadySpecifiedBook(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'    mobjAdoRecordsetSheetExpander.OutputToAlreadySpecifiedBook vstrSQL, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedAccDbPathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
''''
''''
'Public Sub SetOutputExcelBookPath(ByVal vstrOutputBookPath As String, _
'        Optional ByVal venmOutputRSetExcelBookOpenModeProcessFlag As OutputRSetExcelBookOpenModeProcessFlag = OutputRSetExcelBookOpenModeProcessFlag.OpenOutputBookByUnitTestMode, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag)
'
'    mobjAdoRecordsetSheetExpander.SetOutputExcelBookPath vstrOutputBookPath, venmOutputRSetExcelBookOpenModeProcessFlag
'End Sub
'
''**---------------------------------------------
''** Excecute SQL command (UPDATE, INSERT, DELETE)
''**---------------------------------------------
''''
'''' command SQL and output result-log to Excel sheet
''''
'Public Sub OutputCommandLogToSheetFrom(ByVal vstrSQL As String, _
'        ByVal vobjOutputSqlCommandLogSheet As Excel.Worksheet, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogToSheetFrom vstrSQL, vobjOutputSqlCommandLogSheet, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedAccDbPathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputCommandLogToSpecifiedBook(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogToSpecifiedBook vstrSQL, vstrOutputBookPath, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedAccDbPathInsertText(), vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputCommandLogToAlreadySpecifiedBook(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogToAlreadySpecifiedBook vstrSQL, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedAccDbPathInsertText(), vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputCommandLogInAlreadyExistedBook(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogInAlreadyExistedBook vstrSQL, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedAccDbPathInsertText(), vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputCommandLogToSpecifiedBookByUnitTestMode(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogToSpecifiedBookByUnitTestMode vstrSQL, vstrOutputBookPath, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedAccDbPathInsertText(), vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
'''' query SQL and output result to the prepared Excel sheet
''''
'Public Sub OutputCommandLogInExistedBookByUnitTestMode(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogInExistedBookByUnitTestMode vstrSQL, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedAccDbPathInsertText(), vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
''''
''''
''''
'Private Function mfobjGetConnectedAccDbPathInsertText() As Collection
'
'    Dim objInsertTexts As Collection
'
'    Set objInsertTexts = New Collection
'
'    With mobjConnector
'
'        objInsertTexts.Add "<File Name> " & GetFileNameFromPathByVbaDir(.ConnectingFilePath) & ", <Directory> " & GetParentDirectoryPathByVbaDir(.ConnectingFilePath)
'    End With
'
'    Set mfobjGetConnectedAccDbPathInsertText = objInsertTexts
'End Function
'
'
'
'''--VBA_Code_File--<SqlUTfAccDbSheetExpander.bas>--
'Attribute VB_Name = "SqlUTfAccDbSheetExpander"
''
''   Sanity test to output SQL result into a Excel sheet for Access database using ADO OLE DB
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on ADO
''       Dependent on Microsoft Office Access Data Base Engine OLE DB Provider,
''       which should have been the installed when the Microsoft Office Excel software has been installed.
''       Dependent on SqlAdoConnectingUTfAccDb.bas
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Mon, 15/May/2023    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
'
''///////////////////////////////////////////////
''/// Control VBE windows
''///////////////////////////////////////////////
''**---------------------------------------------
''** Open relative VBE code-panes
''**---------------------------------------------
'Private Sub OpenVBECodePaneRelativeToSqlUTfAccDbSheetExpander()
'
'    ' Focusing on this procdure, please press [F5] key to start this. Therefore this scope is private.
'
'    On Error Resume Next ' If the 'ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma' isn't included in the VBA project, the following causes no syntax error.
'
'    Application.Run "ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma", "UTfAdoConnectAccDb,UTfDaoConnectAccDb"
'End Sub
'
'
''///////////////////////////////////////////////
''/// Sanity tests
''///////////////////////////////////////////////
''**---------------------------------------------
''** AccDbAdoSheetExpander
''**---------------------------------------------
''''
'''' Create an in-line table from the only SELECT sql inside
''''
'Private Sub msubSanityTestToConnectToAccDbBySqlInlineVirtualTableByAccDbAdoSheetExpander()
'
'    Dim strSQL As String
'
'    With New AccDbAdoSheetExpander
'
'        .SetMsAccessConnection PrepareAccessDbAfterCreateSampleAccDbIfItDoesntExist("TestDbOfAccDbAdoSheetExpander.accdb")
'
'        .AllowToRecordSQLLog = True
'
'        'strSQL = "SELECT 'ABC', 'DEF' UNION SELECT 'GHI', 'JKL'" ' Error occurs
'
'        'strSQL = "SELECT 1"
'
'        'strSQL = "SELECT 'ABC', 'DEF'"
'
'        strSQL = "SELECT 'ABC' AS A_Column, 'DEF' AS B_Column"
'
'        .OutputInExistedBookByUnitTestMode strSQL, "Test_ADO_VirtualTable"
'
'        .CloseCacheOutputBookOfOpenByPresetModeProcess
'    End With
'End Sub
'
'
''''
''''
''''
'Private Sub msubSanityTestToConnectToAccDbBySqlInsert()
'
'    Dim strSQL As String, strDbPath As String, objOutputBook As Excel.Workbook
'
'    strDbPath = GetAccDbPathAfterTestingToConnectToAccDbAdoConnectorWithSqlInsert()
'
'    Set objOutputBook = GetWorkbookAndPrepareForUnitTest(mfstrGetTempAccDbSqlTestOutputBookPath())
'
'
'    With New AccDbAdoSheetExpander
'
'        .SetMsAccessConnection strDbPath
'
'        .DataTableSheetFormattingInterface.RecordBordersType = RecordCellsGrayAll
'
'        strSQL = "INSERT INTO Test_Table VALUES (13, 14, 15, 'Type3');"
'
'        .OutputCommandLogToSpecifiedBook strSQL, mfstrGetTempAccDbSqlTestOutputBookPath()
'
'        strSQL = "INSERT INTO Test_Table VALUES (16, 17, 18, 'Type3');"
'
'        .OutputCommandLogToSpecifiedBook strSQL, mfstrGetTempAccDbSqlTestOutputBookPath()
'
'
'        strSQL = "SELECT * FROM Test_Table"
'
'        .OutputToSpecifiedBook strSQL, mfstrGetTempAccDbSqlTestOutputBookPath(), "SqlAllResults", AppendingSqlQueryLogsInSheetCellsOnIndependentSheet
'
'        .CloseConnection
'    End With
'
'    DeleteDummySheetWhenItExists objOutputBook
'End Sub
'
''''
'''' Test for SQL DELETE
''''
'Private Sub msubSanityTestToConnectToAccDbBySqlDelete()
'
'    Dim strSQL As String, strDbPath As String, objOutputBook As Excel.Workbook
'
'    strDbPath = GetAccDbPathAfterTestingToConnectToAccDbAdoConnectorWithSqlInsert()
'
'    Set objOutputBook = GetWorkbookAndPrepareForUnitTest(mfstrGetTempAccDbSqlTestOutputBookPath())
'
'
'    With New AccDbAdoSheetExpander
'
'        .SetMsAccessConnection strDbPath
'
'        .DataTableSheetFormattingInterface.RecordBordersType = RecordCellsGrayAll
'
'        strSQL = "DELETE FROM Test_Table WHERE ColText = 'Type2';"
'
'        .OutputCommandLogToSpecifiedBook strSQL, mfstrGetTempAccDbSqlTestOutputBookPath()
'
'        strSQL = "SELECT * FROM Test_Table"
'
'        .OutputToSpecifiedBook strSQL, mfstrGetTempAccDbSqlTestOutputBookPath(), "SqlAllResults", SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable
'
'        .CloseAll
'    End With
'
'    DeleteDummySheetWhenItExists objOutputBook
'End Sub
'
''''
''''
''''
'Private Sub msubSanityTestToConnectToAccDbBySqlUpdate()
'
'    Dim strSQL As String, strDbPath As String, objOutputBook As Excel.Workbook
'
'    strDbPath = GetAccDbPathAfterTestingToConnectToAccDbAdoConnectorWithSqlInsert()
'
'    Set objOutputBook = GetWorkbookAndPrepareForUnitTest(mfstrGetTempAccDbSqlTestOutputBookPath())
'
'
'    With New AccDbAdoSheetExpander
'
'        .SetMsAccessConnection strDbPath
'
'        .DataTableSheetFormattingInterface.RecordBordersType = RecordCellsGrayAll
'
'        strSQL = "UPDATE Test_Table SET Col1 = 41, Col2 = 42 WHERE Col3 = 12 AND ColText = 'Type2';"
'
'        .OutputCommandLogToSpecifiedBook strSQL, mfstrGetTempAccDbSqlTestOutputBookPath()
'
'        strSQL = "SELECT * FROM Test_Table"
'
'        .OutputToSpecifiedBook strSQL, mfstrGetTempAccDbSqlTestOutputBookPath(), "SqlAllResults", AppendingSqlQueryLogsInSheetCellsOnIndependentSheet
'
'        .CloseAll
'    End With
'
'    DeleteDummySheetWhenItExists objOutputBook
'End Sub
'
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
''''
''''
''''
'Private Function mfstrGetTempAccDbSqlTestOutputBookPath() As String
'
'    mfstrGetTempAccDbSqlTestOutputBookPath = mfstrGetTempAccDbSqlTestOutputBookDir() & "\SqlTestResultBookOfAccDbAdoSheetExpander.xlsx"
'End Function
'
'
''''
''''
''''
'Private Function mfstrGetTempAccDbSqlTestOutputBookDir() As String
'
'    Dim strDir As String
'
'    strDir = GetCurrentBookOutputDir() & "\SQLTests\FromAccDbConnect"
'
'    ForceToCreateDirectory strDir
'
'    mfstrGetTempAccDbSqlTestOutputBookDir = strDir
'End Function
'
'
'''--VBA_Code_File--<PgSqlOdbcSheetExpander.cls>--
'VERSION 1.0 CLASS
'BEGIN
'  MultiUse = -1  'True
'END
'Attribute VB_Name = "PgSqlOdbcSheetExpander"
'Attribute VB_GlobalNameSpace = False
'Attribute VB_Creatable = False
'Attribute VB_PredeclaredId = False
'Attribute VB_Exposed = False
''
''   output SQL result into Excel.Worksheet after connecting a PostgreSQL database by ADO ODBC interface
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on both ADO and Excel
''       Dependent on both PsqlOdbc driver at this Windows and an PostgreSQL database server in either a network somewhere or the local-host
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Wed, 26/Jul/2023    Kalmclaeyd Tarclanus    Started to redesign
''
'
'Option Explicit
'
'
''///////////////////////////////////////////////
''/// Interface implementations
''///////////////////////////////////////////////
'Implements IOutputBookPath
'
''///////////////////////////////////////////////
''/// Constants
''///////////////////////////////////////////////
'#If Win64 Then
'
'    Private Const mstrDriverName As String = "{PostgreSQL Unicode(x64)}"
'#Else
'    Private Const mstrDriverName As String = "{PostgreSQL Unicode}"
'#End If
'
'Private Const mintPostgreSQLDefaultPortNumber As Long = 5432
'
''///////////////////////////////////////////////
''/// Declarations
''///////////////////////////////////////////////
'Private mobjConnector As PgSqlOdbcConnector: Private mitfConnector As IADOConnector
'
'Private mobjAdoRecordsetSheetExpander As AdoRSetSheetExpander
'
'
''///////////////////////////////////////////////
''/// Event Handlers
''///////////////////////////////////////////////
'Private Sub Class_Initialize()
'
'    Set mobjConnector = New PgSqlOdbcConnector
'
'    Set mitfConnector = mobjConnector
'
'    Set mobjAdoRecordsetSheetExpander = New AdoRSetSheetExpander
'
'    With mobjAdoRecordsetSheetExpander
'
'        Set .ADOConnectorInterface = mobjConnector
'
'        Set .OutputBookPathInterface = Me
'
'        .DataTableSheetFormattingInterface.FieldTitleInteriorType = ColumnNameInteriorOfPostgreSQL
'    End With
'End Sub
'
''''
''''
''''
'Private Sub Class_Terminate()
'
'    mobjAdoRecordsetSheetExpander.CloseCacheOutputBookOfOpenByPresetModeProcess
'
'    Debug.Print "class AccDbAdoSheetExpander terminate -> " & mobjAdoRecordsetSheetExpander Is Nothing
'
'    Me.CloseConnection
'
'    Set mobjAdoRecordsetSheetExpander = Nothing
'
'    Set mobjConnector = Nothing
'End Sub
'
'
''///////////////////////////////////////////////
''/// Properties - connoted ADO connector
''///////////////////////////////////////////////
'Public Property Get SQLExecutionResult() As SQLResult
'
'    Set SQLExecutionResult = mobjConnector.SQLExecutionResult
'End Property
'
'Public Property Get IsSqlExecutionFailed() As Boolean
'
'    IsSqlExecutionFailed = mobjConnector.IsSqlExecutionFailed
'End Property
'
'Public Property Let AllowToRecordSQLLog(ByVal vblnAllowToRecord As Boolean)
'
'    mobjConnector.AllowToRecordSQLLog = vblnAllowToRecord
'End Property
'Public Property Get AllowToRecordSQLLog() As Boolean
'
'    AllowToRecordSQLLog = mobjConnector.AllowToRecordSQLLog
'End Property
'
'
'Public Property Get CurrentSheet() As Excel.Worksheet
'
'    Set CurrentSheet = mobjAdoRecordsetSheetExpander.IExpandAdoRecordsetOnSheet_CurrentSheet
'End Property
'
'
'Public Property Get CurrentBook() As Excel.Workbook
'
'    If Not Me.CurrentSheet Is Nothing Then
'
'        Set CurrentBook = Me.CurrentSheet.Parent
'    Else
'        Set CurrentBook = Nothing
'    End If
'End Property
'
'
''///////////////////////////////////////////////
''/// Properties
''///////////////////////////////////////////////
''''
''''
''''
'Public Property Get ADOConnectorInterface() As IADOConnector
'
'    Set ADOConnectorInterface = mitfConnector
'End Property
'
''''
''''
''''
'Public Property Get DataTableSheetFormattingInterface() As IDataTableSheetFormatter
'
'    Set DataTableSheetFormattingInterface = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface
'End Property
'
'
''///////////////////////////////////////////////
''/// Properties - connoted ADOConnectionSetting
''///////////////////////////////////////////////
'Public Property Get ConnectSetting() As ADOConnectionSetting
'
'    Set ConnectSetting = mobjConnector.ConnectSetting
'End Property
'
''''
'''' unit is second. [s]
''''
'Public Property Let CommandTimeout(ByVal vintTimeout As Long)
'
'    mobjConnector.CommandTimeout = vintTimeout
'End Property
'Public Property Get CommandTimeout() As Long
'
'    CommandTimeout = mobjConnector.CommandTimeout
'End Property
'
''///////////////////////////////////////////////
''/// Implemented operations
''///////////////////////////////////////////////
''''
''''
''''
'Private Function IOutputBookPath_GetPreservedOutputBookPath() As String
'
'    IOutputBookPath_GetPreservedOutputBookPath = GetPgSqlConnectingPrearrangedLogBookPath()
'End Function
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''**---------------------------------------------
''** set ADO connection string all parameters directly
''**---------------------------------------------
''''
'''' set ODBC connection parameters by a registered Data Source Name (DSN)
''''
'Public Sub SetODBCParametersByDSN(ByVal vstrDSN As String, ByVal vstrUID As String, ByVal vstrPWD As String)
'
'    mobjConnector.SetODBCConnectionByDSN vstrDSN, vstrUID, vstrPWD
'End Sub
'
''''
'''' set ODBC connection parameters without Data Source Name (DSN) setting
''''
'Public Sub SetODBCParametersWithoutDSN(ByVal vstrDriverName As String, ByVal vstrServerHostName As String, ByVal vstrDatabaseName As String, ByVal vstrUserid As String, ByVal vstrPassword As String, Optional ByVal vintPortNumber As Long = mintPostgreSQLDefaultPortNumber)
'
'    mobjConnector.SetODBCParametersWithoutDSN vstrDriverName, vstrServerHostName, vstrDatabaseName, vstrUserid, vstrPassword, vintPortNumber
'End Sub
'
'
''**---------------------------------------------
''** set ADO connection string parameters by a User-form interface
''**---------------------------------------------
''''
'''' Using a User-form, set DSN connection
''''
'''' <Argument>vstrSettingKeyName: Input - This is to be written in Windows-registry</Argument>
'''' <Argument>vstrDSN: Input</Argument>
'''' <Argument>vstrUID: Input</Argument>
'''' <Argument>vstrPWD: Input</Argument>
'Public Function IsAdoConnectionContinuedAfterOdbcConnectionDsnParametersByForm(ByVal vstrSettingKeyName As String, _
'        Optional ByVal vstrDSN As String = "", _
'        Optional ByVal vstrUID As String = "", _
'        Optional ByVal vstrPWD As String = "") As Boolean
'
'
'    IsAdoConnectionContinuedAfterOdbcConnectionDsnParametersByForm = mobjConnector.IsAdoConnectionContinuedAfterOdbcConnectionDsnParametersByForm(vstrSettingKeyName, vstrDSN, vstrUID, vstrPWD)
'End Function
'
''''
'''' Using a User-form, set DSN connection
''''
'''' <Argument>vstrSettingKeyName: Input - This is to be written in Windows-registry</Argument>
'''' <Argument>vstrDriverName: Input</Argument>
'''' <Argument>vstrServerHostName: Input</Argument>
'''' <Argument>vstrDatabaseName: Input</Argument>
'''' <Argument>vstrUserid: Input</Argument>
'''' <Argument>vstrPassword: Input</Argument>
'''' <Argument>vintPortNumber: Input</Argument>
'Public Function IsAdoConnectionContinuedAfterOdbcConnectionDsnlessParametersByForm(ByVal vstrSettingKeyName As String, _
'        Optional ByVal vstrDriverName As String = mstrDriverName, _
'        Optional ByVal vstrServerHostName As String = "", _
'        Optional ByVal vstrDatabaseName As String = "", _
'        Optional ByVal vstrUserid As String = "", _
'        Optional ByVal vstrPassword As String = "", _
'        Optional ByVal vintPortNumber As Long = mintPostgreSQLDefaultPortNumber) As Boolean
'
'
'    IsAdoConnectionContinuedAfterOdbcConnectionDsnlessParametersByForm = mobjConnector.IsAdoConnectionContinuedAfterOdbcConnectionDsnlessParametersByForm(vstrSettingKeyName, vstrDriverName, vstrServerHostName, vstrDatabaseName, vstrUserid, vstrPassword, vintPortNumber)
'End Function
'
''''
'''' Close ADO connection
''''
'Public Sub CloseConnection()
'
'    mobjConnector.CloseConnection
'End Sub
'
''''
''''
''''
'Public Sub CloseCacheOutputBookOfOpenByPresetModeProcess()
'
'    mobjAdoRecordsetSheetExpander.CloseCacheOutputBookOfOpenByPresetModeProcess
'End Sub
'
''''
''''
''''
'Public Sub CloseAll()
'
'    CloseConnection
'
'    CloseCacheOutputBookOfOpenByPresetModeProcess
'End Sub
'
''**---------------------------------------------
''** Output Recordset and logs to cells on sheet
''**---------------------------------------------
''''
'''' query SQL and output result to Excel sheet
''''
'Public Sub OutputToSheetFrom(ByVal vstrSQL As String, _
'        ByVal vobjOutputSheet As Excel.Worksheet, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'    mobjAdoRecordsetSheetExpander.OutputToSheetFrom vstrSQL, vobjOutputSheet, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, Nothing, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
'''' query SQL and output result to the Existed Excel sheet
''''
'Public Sub OutputInExistedBook(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputInExistedBook vstrSQL, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, Nothing, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
'Public Sub OutputInAlreadyExistedBook(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputInAlreadyExistedBook vstrSQL, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, Nothing, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
''''
''''
'Public Sub OutputInExistedBookByUnitTestMode(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputInExistedBookByUnitTestMode vstrSQL, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, Nothing, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputToSpecifiedBook(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputToSpecifiedBook vstrSQL, vstrOutputBookPath, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, Nothing, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
'Public Sub OutputToSpecifiedBookByUnitTestMode(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'    mobjAdoRecordsetSheetExpander.OutputToSpecifiedBookByUnitTestMode vstrSQL, vstrOutputBookPath, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, Nothing, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
'''' query SQL and output result to the already specified Excel sheet
''''
'Public Sub OutputToAlreadySpecifiedBook(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'    mobjAdoRecordsetSheetExpander.OutputToAlreadySpecifiedBook vstrSQL, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, Nothing, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
''''
''''
'Public Sub SetOutputExcelBookPath(ByVal vstrOutputBookPath As String, _
'        Optional ByVal venmOutputRSetExcelBookOpenModeProcessFlag As OutputRSetExcelBookOpenModeProcessFlag = OutputRSetExcelBookOpenModeProcessFlag.OpenOutputBookByUnitTestMode, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag)
'
'    mobjAdoRecordsetSheetExpander.SetOutputExcelBookPath vstrOutputBookPath, venmOutputRSetExcelBookOpenModeProcessFlag
'End Sub
'
''**---------------------------------------------
''** Excecute SQL command (UPDATE, INSERT, DELETE)
''**---------------------------------------------
''''
'''' command SQL and output result-log to Excel sheet
''''
'Public Sub OutputCommandLogToSheetFrom(ByVal vstrSQL As String, _
'        ByVal vobjOutputSqlCommandLogSheet As Excel.Worksheet, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogToSheetFrom vstrSQL, vobjOutputSqlCommandLogSheet, venmShowUpAdoErrorOptionFlag, Nothing, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputCommandLogToSpecifiedBook(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogToSpecifiedBook vstrSQL, vstrOutputBookPath, venmShowUpAdoErrorOptionFlag, Nothing, vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputCommandLogToAlreadySpecifiedBook(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogToAlreadySpecifiedBook vstrSQL, venmShowUpAdoErrorOptionFlag, Nothing, vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputCommandLogInAlreadyExistedBook(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogInAlreadyExistedBook vstrSQL, venmShowUpAdoErrorOptionFlag, Nothing, vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputCommandLogToSpecifiedBookByUnitTestMode(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogToSpecifiedBookByUnitTestMode vstrSQL, vstrOutputBookPath, venmShowUpAdoErrorOptionFlag, Nothing, vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
'''' query SQL and output result to the prepared Excel sheet
''''
'Public Sub OutputCommandLogInExistedBookByUnitTestMode(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogInExistedBookByUnitTestMode vstrSQL, venmShowUpAdoErrorOptionFlag, Nothing, vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
'
'
'
'
'''--VBA_Code_File--<SqlUTfPgSqlOdbcSheetExpander.bas>--
'Attribute VB_Name = "SqlUTfPgSqlOdbcSheetExpander"
''
''   Sanity test to output SQL results into Excel sheets for a PostgreSQL database using ADO ODBC
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on both ADO and Excel
''       Dependent on both PsqlOdbc driver at this Windows and an PostgreSQL database server in either a network somewhere or the local-host
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Wed, 26/Jul/2023    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Constants
''///////////////////////////////////////////////
'Private Const mstrConStrInfoSettingKeyNameOfDSN As String = "TestPgSqlDSNKey"
'
'Private Const mstrConStrInfoSettingKeyNameOfDSNless As String = "TestPgSqlDSNlessKey"
'
'
'
'#If Win64 Then
'
'    Private Const mstrDriverName As String = "{PostgreSQL Unicode(x64)}"
'#Else
'    Private Const mstrDriverName As String = "{PostgreSQL Unicode}"
'#End If
'
'Private Const mintPostgreSQLDefaultPortNumber As Long = 5432
'
''///////////////////////////////////////////////
''/// Control VBE windows
''///////////////////////////////////////////////
''**---------------------------------------------
''** Open relative VBE code-panes
''**---------------------------------------------
'Private Sub OpenVBECodePaneRelativeToSqlUTfPgSqlOdbcSheetExpander()
'
'    ' Focusing on this procdure, please press [F5] key to start this. Therefore this scope is private.
'
'    On Error Resume Next ' If the 'ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma' isn't included in the VBA project, the following causes no syntax error.
'
'    Application.Run "ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma", "SqlUTfPgSqlOdbcSheetExpander,SqlAdoConnectOdbcUTfPgSql,SqlAdoConnectOdbcPTfPgSql,PgSqlOdbcConnector,PgSqlOdbcSheetExpander,PgSqlCommonPath,PgSqlDSNlessTest"
'End Sub
'
'
''///////////////////////////////////////////////
''/// Sanity tests
''///////////////////////////////////////////////
''**---------------------------------------------
''** PgSqlOdbcSheetExpander
''**---------------------------------------------
''''
'''' Create an in-line table from the only SELECT sql inside
''''
'Private Sub msubSanityTestToConnectByVirtualTablePgSqlOdbcSheetExpander()
'
'    Dim strSQL As String
'
'    With New PgSqlOdbcSheetExpander
'
'        ' for default connection setting soon after the PostgreSQL typcal configuration is installed in this local computer.
'
'        .SetODBCParametersWithoutDSN mstrDriverName, "LocalHost", "postgres", "postgres", "postgres"
'
'        .AllowToRecordSQLLog = True
'
'        strSQL = "select 'ABC' as A_Column, 'DEF' as B_Column"
'
'        .OutputInExistedBookByUnitTestMode strSQL, "Test_ADO_VirtualTable"
'
'        .CloseAll
'    End With
'End Sub
'
'
''///////////////////////////////////////////////
''/// Sanity tests
''///////////////////////////////////////////////
''**---------------------------------------------
''** Use IsAdoConnectionContinuedAfterOdbcConnectionDsnlessParametersByForm interface for ADO connecting
''**---------------------------------------------
''''
'''' get PostgreSQL version by SQL
''''
'Private Sub msubSanityTestToOutputSqlVersionOnSheetAfterSqlVersionOfPostgreSQL()
'
'    Dim strSQL As String
'
'    With New PgSqlOdbcSheetExpander
'
'        ' for default connection setting soon after the PostgreSQL typcal configuration is installed in this local computer.
'
'        If .IsAdoConnectionContinuedAfterOdbcConnectionDsnlessParametersByForm(mstrConStrInfoSettingKeyNameOfDSNless, , "LocalHost", "postgres", "postgres") Then
'
'            .AllowToRecordSQLLog = True
'
'            strSQL = "select * from version()"
'
'            .OutputInExistedBookByUnitTestMode strSQL, "PgSQL_version"
'
'            .CloseCacheOutputBookOfOpenByPresetModeProcess
'        Else
'            Debug.Print "The ADO connection information inputs have canceled."
'        End If
'    End With
'End Sub
'
''**---------------------------------------------
''** Use presetting object for ADO connecting
''**---------------------------------------------
''''
'''' SQL insert test with output Excel sheet log
''''
'Private Sub msubSanityTestToOutputSqlResultOnSheetAfterPostgreSQLInsertExecution()
'
'    Dim strSQL As String, objAdoSheetExpander As PgSqlOdbcSheetExpander
'
'    ' You must implement 'GetCurrentThisSytemTestingPgSqlOdbcSheetExpander' function before you execute the following.
'
'    Set objAdoSheetExpander = mfobjGetPgSqlOdbcSheetExpanderAfterPrepareTestTableOfPostgreSQLTestingDataBaseForSqlTests()
'
'    If Not objAdoSheetExpander Is Nothing Then
'
'        With objAdoSheetExpander
'
'            strSQL = "insert into Test_Table values (13, 15, 16, 'Type3')"
'
'            .OutputCommandLogToSpecifiedBook strSQL, GetPgSqlConnectingPrearrangedLogBookPath()
'
'            .ADOConnectorInterface.CommitTransaction
'
'            strSQL = "select * from Test_Table"
'
'            .OutputToSpecifiedBook strSQL, GetPgSqlConnectingPrearrangedLogBookPath(), "SqlResults", SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable
'
'            .CloseAll
'        End With
'    Else
'        Debug.Print "Attention!!!: the 'GetCurrentThisSytemTestingPgSqlOdbcConnector' function hasn't been implemented yet."
'    End If
'End Sub
'
''''
'''' SQL delete test with output Excel sheet log
''''
'Private Sub msubSanityTestToOutputSqlResultOnSheetAfterPostgreSQLDeleteExecution()
'
'    Dim strSQL As String, objAdoSheetExpander As PgSqlOdbcSheetExpander, strOutputBookPath As String, strDir As String
'
'    ' You must implement 'GetCurrentThisSytemTestingPgSqlOdbcSheetExpander' function before you execute the following.
'
'    strDir = GetCurrentBookOutputDir() & "\SQLTests\FromPgSqlConnect"
'
'    ForceToCreateDirectory strDir
'
'    strOutputBookPath = strDir & "\AdoConnectToPgSqlTest01.xlsx"
'
'    Set objAdoSheetExpander = mfobjGetPgSqlOdbcSheetExpanderAfterPrepareTestTableOfPostgreSQLTestingDataBaseForSqlTests()
'
'    If Not objAdoSheetExpander Is Nothing Then
'
'        With objAdoSheetExpander
'
'            strSQL = "delete from Test_Table where ColText = 'Type2'"
'
'            .OutputCommandLogToSpecifiedBookByUnitTestMode strSQL, strOutputBookPath
'
'            .ADOConnectorInterface.CommitTransaction
'
'            strSQL = "select * from Test_Table"
'
'            .OutputToSpecifiedBookByUnitTestMode strSQL, strOutputBookPath, "SqlResults", AppendingSqlQueryLogsInSheetCellsOnIndependentSheet
'
'            .CloseAll
'        End With
'    Else
'        Debug.Print "Attention!!!: the 'GetCurrentThisSytemTestingPgSqlOdbcConnector' function hasn't been implemented yet."
'    End If
'End Sub
'
''''
'''' SQL update test with output Excel sheet log
''''
'Private Sub msubSanityTestToOutputSqlResultOnSheetAfterPostgreSQLUpdateExecution()
'
'    Dim strSQL As String, objAdoSheetExpander As PgSqlOdbcSheetExpander
'
'    ' You must implement 'GetCurrentThisSytemTestingPgSqlOdbcSheetExpander' function before you execute the following.
'
'    Set objAdoSheetExpander = mfobjGetPgSqlOdbcSheetExpanderAfterPrepareTestTableOfPostgreSQLTestingDataBaseForSqlTests()
'
'    If Not objAdoSheetExpander Is Nothing Then
'
'        With objAdoSheetExpander
'
'            strSQL = "update Test_Table set Col1 = 51, Col2 = 52 where Col3 = 9 AND ColText = 'Type2'"
'
'
'            .OutputCommandLogInExistedBookByUnitTestMode strSQL
'
'            .ADOConnectorInterface.CommitTransaction
'
'            strSQL = "select * from Test_Table"
'
'            .OutputInExistedBookByUnitTestMode strSQL, "SqlResults", AppendingSqlQueryLogsInSheetCellsOnIndependentSheet
'
'            .CloseAll
'        End With
'    Else
'        Debug.Print "Attention!!!: the 'GetCurrentThisSytemTestingPgSqlOdbcConnector' function hasn't been implemented yet."
'    End If
'End Sub
'
''''
'''' SQL insert test with output Excel sheet log
''''
'Private Sub msubSanityTestToOutputSqlResultOnSheetAfterPostgreSQLPluralInsertExecution()
'
'    Dim strSQL As String, objAdoSheetExpander As PgSqlOdbcSheetExpander
'
'    Dim i As Long
'    Dim int01 As Long, int02 As Long, int03 As Long, intType As Long
'
'
'    ' You must implement 'GetCurrentThisSytemTestingPgSqlOdbcSheetExpander' function before you execute the following.
'
'    Set objAdoSheetExpander = mfobjGetPgSqlOdbcSheetExpanderAfterPrepareTestTableOfPostgreSQLTestingDataBaseForSqlTests()
'
'    If Not objAdoSheetExpander Is Nothing Then
'
'        With objAdoSheetExpander
'
'            For i = 1 To 15
'
'                ' sample values
'
'                int01 = 3 * (i + 4)
'
'                int02 = int01 + 1
'
'                int03 = int02 + 1
'
'                intType = i + 5
'
'                strSQL = "insert into Test_Table values (" & CStr(int01) & ", " & CStr(int02) & ", " & CStr(int03) & ", 'Type" & CStr(intType) & "')"
'
'                .OutputCommandLogInExistedBookByUnitTestMode strSQL
'            Next
'
'            .ADOConnectorInterface.CommitTransaction
'
'            strSQL = "select * from Test_Table"
'
'            .OutputInExistedBookByUnitTestMode strSQL, "SqlResults", SqlQueryLogsByAutoShapesOnSheetWithOutputtingRecordsetDataTable
'
'            .CloseAll
'        End With
'    Else
'        Debug.Print "Attention!!!: the 'GetCurrentThisSytemTestingPgSqlOdbcConnector' function hasn't been implemented yet."
'    End If
'End Sub
'
'
''''
'''' SQL insert test with output Excel sheet log
''''
'Private Sub msubSanityTestToOutputSqlResultOnSheetAfterPostgreSQLPluralInsertExecutionWithOtherLoggingOption()
'
'    Dim strSQL As String, objAdoSheetExpander As PgSqlOdbcSheetExpander
'
'    Dim i As Long
'    Dim int01 As Long, int02 As Long, int03 As Long, intType As Long
'
'
'    ' You must implement 'GetCurrentThisSytemTestingPgSqlOdbcSheetExpander' function before you execute the following.
'
'    Set objAdoSheetExpander = mfobjGetPgSqlOdbcSheetExpanderAfterPrepareTestTableOfPostgreSQLTestingDataBaseForSqlTests()
'
'    If Not objAdoSheetExpander Is Nothing Then
'
'        With objAdoSheetExpander
'
'            For i = 1 To 15
'
'                ' sample values
'
'                int01 = 3 * (i + 4)
'
'                int02 = int01 + 1
'
'                int03 = int02 + 1
'
'                intType = i + 5
'
'                strSQL = "insert into Test_Table values (" & CStr(int01) & ", " & CStr(int02) & ", " & CStr(int03) & ", 'Type" & CStr(intType) & "')"
'
'                .OutputCommandLogInExistedBookByUnitTestMode strSQL, NoControlToShowUpAdoSqlErrorFlag, "TestingSqlCommandLog"
'            Next
'
'            .ADOConnectorInterface.CommitTransaction
'
'            strSQL = "select * from Test_Table"
'
'            .OutputInExistedBookByUnitTestMode strSQL, "SqlResults", SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable
'
'            .CloseCacheOutputBookOfOpenByPresetModeProcess
'        End With
'    Else
'        Debug.Print "Attention!!!: the 'GetCurrentThisSytemTestingPgSqlOdbcConnector' function hasn't been implemented yet."
'    End If
'End Sub
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
''''
''''
''''
'Private Function mfobjGetPgSqlOdbcSheetExpanderAfterPrepareTestTableOfPostgreSQLTestingDataBaseForSqlTests() As PgSqlOdbcSheetExpander
'
'    Dim strSQL As String, objAdoSheetExpander As PgSqlOdbcSheetExpander
'
'    ' You must implement 'GetCurrentThisSytemTestingPgSqlOdbcSheetExpander' function before you execute the following.
'
'    Set objAdoSheetExpander = mfobjGetPgSqlOdbcSheetExpanderWhichIncludesConnectionInfo()
'
'    If Not objAdoSheetExpander Is Nothing Then
'
'        PrepareTestTableOfPostgreSQLTestingDataBaseForSqlTests objAdoSheetExpander.ADOConnectorInterface
'
'        objAdoSheetExpander.DataTableSheetFormattingInterface.RecordBordersType = RecordCellsGrayAll
'    Else
'        Debug.Print "Attention!!!: the 'GetCurrentThisSytemTestingPgSqlOdbcSheetExpander' function hasn't been implemented yet."
'    End If
'
'    Set mfobjGetPgSqlOdbcSheetExpanderAfterPrepareTestTableOfPostgreSQLTestingDataBaseForSqlTests = objAdoSheetExpander
'End Function
'
'
''''
''''
''''
'Private Function mfobjGetPgSqlOdbcSheetExpanderWhichIncludesConnectionInfo() As PgSqlOdbcSheetExpander
'
'    Dim objPgSqlOdbcSheetExpander As PgSqlOdbcSheetExpander
'
'    On Error Resume Next
'
'    Set objPgSqlOdbcSheetExpander = Application.Run("GetCurrentThisSytemTestingPgSqlOdbcSheetExpander")
'
'    On Error GoTo 0
'
'    Set mfobjGetPgSqlOdbcSheetExpanderWhichIncludesConnectionInfo = objPgSqlOdbcSheetExpander
'End Function
'
'''--VBA_Code_File--<OracleOdbcSheetExpander.cls>--
'VERSION 1.0 CLASS
'BEGIN
'  MultiUse = -1  'True
'END
'Attribute VB_Name = "OracleOdbcSheetExpander"
'Attribute VB_GlobalNameSpace = False
'Attribute VB_Creatable = False
'Attribute VB_PredeclaredId = False
'Attribute VB_Exposed = False
''
''   output SQL results into Excel worksheets after connecting a Oracle database by ADO ODBC interface
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on both ADO and Excel
''       Dependent on both Oracle ODBC driver at this Windows and an Oracle database server in a network somewhere
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   Modification History:
''       Wed, 26/Jul/2023    Kalmclaeyd Tarclanus    Initial creation
''
'
'Option Explicit
'
'
''///////////////////////////////////////////////
''/// Interface implementations
''///////////////////////////////////////////////
'Implements IOutputBookPath
'
''///////////////////////////////////////////////
''/// Constants
''///////////////////////////////////////////////
'#If Win64 Then
'
'    Private Const mstrDriverName As String = "{Oracle in OraClient11g_home1}"
'#Else
'    Private Const mstrDriverName As String = "{Oracle in OraClient10g_home1}"
'#End If
'
'Private Const mintOracleDefaultPortNumber As Long = 1521
'
''///////////////////////////////////////////////
''/// Declarations
''///////////////////////////////////////////////
'Private mobjConnector As OracleOdbcConnector
'
'Private mobjAdoRecordsetSheetExpander As AdoRSetSheetExpander
'
'
''///////////////////////////////////////////////
''/// Event Handlers
''///////////////////////////////////////////////
'Private Sub Class_Initialize()
'
'    Set mobjConnector = New OracleOdbcConnector
'
'    Set mobjAdoRecordsetSheetExpander = New AdoRSetSheetExpander
'
'    With mobjAdoRecordsetSheetExpander
'
'        Set .ADOConnectorInterface = mobjConnector
'
'        Set .OutputBookPathInterface = Me
'
'        .DataTableSheetFormattingInterface.FieldTitleInteriorType = ColumnNameInteriorOfDefaultTypeSQL
'    End With
'End Sub
'
''''
''''
''''
'Private Sub Class_Terminate()
'
'    mobjAdoRecordsetSheetExpander.CloseCacheOutputBookOfOpenByPresetModeProcess
'
'    Debug.Print "class AccDbAdoSheetExpander terminate -> " & mobjAdoRecordsetSheetExpander Is Nothing
'
'    Me.CloseConnection
'
'    Set mobjAdoRecordsetSheetExpander = Nothing
'
'    Set mobjConnector = Nothing
'End Sub
'
'
''///////////////////////////////////////////////
''/// Properties - connoted ADO connector
''///////////////////////////////////////////////
'Public Property Get SQLExecutionResult() As SQLResult
'
'    Set SQLExecutionResult = mobjConnector.SQLExecutionResult
'End Property
'
'Public Property Get IsSqlExecutionFailed() As Boolean
'
'    IsSqlExecutionFailed = mobjConnector.IsSqlExecutionFailed
'End Property
'
'Public Property Let AllowToRecordSQLLog(ByVal vblnAllowToRecord As Boolean)
'
'    mobjConnector.AllowToRecordSQLLog = vblnAllowToRecord
'End Property
'Public Property Get AllowToRecordSQLLog() As Boolean
'
'    AllowToRecordSQLLog = mobjConnector.AllowToRecordSQLLog
'End Property
'
'
'Public Property Get CurrentSheet() As Excel.Worksheet
'
'    Set CurrentSheet = mobjAdoRecordsetSheetExpander.IExpandAdoRecordsetOnSheet_CurrentSheet
'End Property
'
'
'Public Property Get CurrentBook() As Excel.Workbook
'
'    If Not Me.CurrentSheet Is Nothing Then
'
'        Set CurrentBook = Me.CurrentSheet.Parent
'    Else
'        Set CurrentBook = Nothing
'    End If
'End Property
'
'
''///////////////////////////////////////////////
''/// Properties
''///////////////////////////////////////////////
''''
''''
''''
'Public Property Get DataTableSheetFormattingInterface() As IDataTableSheetFormatter
'
'    Set DataTableSheetFormattingInterface = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface
'End Property
'
'
''///////////////////////////////////////////////
''/// Properties - connoted ADOConnectionSetting
''///////////////////////////////////////////////
'Public Property Get ConnectSetting() As ADOConnectionSetting
'
'    Set ConnectSetting = mobjConnector.ConnectSetting
'End Property
'
''''
'''' unit is second. [s]
''''
'Public Property Let CommandTimeout(ByVal vintTimeout As Long)
'
'    mobjConnector.CommandTimeout = vintTimeout
'End Property
'Public Property Get CommandTimeout() As Long
'
'    CommandTimeout = mobjConnector.CommandTimeout
'End Property
'
''///////////////////////////////////////////////
''/// Implemented operations
''///////////////////////////////////////////////
''''
''''
''''
'Private Function IOutputBookPath_GetPreservedOutputBookPath() As String
'
'    IOutputBookPath_GetPreservedOutputBookPath = GetOracleConnectingPrearrangedLogBookPath()
'End Function
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''**---------------------------------------------
''** set ADO connection string all parameters directly
''**---------------------------------------------
''''
'''' set ODBC connection parameters by a registered Data Source Name (DSN)
''''
'Public Sub SetODBCParametersByDSN(ByVal vstrDSN As String, ByVal vstrUID As String, ByVal vstrPWD As String)
'
'    mobjConnector.SetODBCParametersByDSN vstrDSN, vstrUID, vstrPWD
'End Sub
'
''''
'''' set ODBC connection parameters without Data Source Name (DSN) setting
''''
'Public Sub SetODBCParametersWithoutDSN(ByVal vstrDriverName As String, ByVal vstrServerHostName As String, ByVal vstrNetworkServiceName As String, ByVal vstrUserid As String, ByVal vstrPassword As String, Optional ByVal vintPortNumber As Long = mintOracleDefaultPortNumber)
'
'    mobjConnector.SetODBCParametersWithoutDSN vstrDriverName, vstrServerHostName, vstrNetworkServiceName, vstrUserid, vstrPassword, vintPortNumber
'End Sub
'
'
''**---------------------------------------------
''** set ADO connection string parameters by a User-form interface
''**---------------------------------------------
''''
'''' Using a User-form, set DSN connection
''''
'''' <Argument>vstrSettingKeyName: Input - This is to be written in Windows-registry</Argument>
'''' <Argument>vstrDSN: Input</Argument>
'''' <Argument>vstrUID: Input</Argument>
'''' <Argument>vstrPWD: Input</Argument>
'Public Function IsAdoConnectionContinuedAfterOdbcConnectionDsnParametersByForm(ByVal vstrSettingKeyName As String, _
'        Optional ByVal vstrDSN As String = "", _
'        Optional ByVal vstrUID As String = "", _
'        Optional ByVal vstrPWD As String = "") As Boolean
'
'
'    IsAdoConnectionContinuedAfterOdbcConnectionDsnParametersByForm = mobjConnector.IsAdoConnectionContinuedAfterOdbcConnectionDsnParametersByForm(vstrSettingKeyName, vstrDSN, vstrUID, vstrPWD)
'End Function
'
''''
'''' Using a User-form, set DSN connection
''''
'''' <Argument>vstrSettingKeyName: Input - This is to be written in Windows-registry</Argument>
'''' <Argument>vstrDriverName: Input</Argument>
'''' <Argument>vstrServerHostName: Input</Argument>
'''' <Argument>vstrNetworkServiceName: Input</Argument>
'''' <Argument>vstrUserid: Input</Argument>
'''' <Argument>vstrPassword: Input</Argument>
'''' <Argument>vintPortNumber: Input</Argument>
'Public Function IsAdoConnectionContinuedAfterOdbcConnectionDsnlessParametersByForm(ByVal vstrSettingKeyName As String, _
'        Optional ByVal vstrDriverName As String = mstrDriverName, _
'        Optional ByVal vstrServerHostName As String = "", _
'        Optional ByVal vstrNetworkServiceName As String = "", _
'        Optional ByVal vstrUserid As String = "", _
'        Optional ByVal vstrPassword As String = "", _
'        Optional ByVal vintPortNumber As Long = mintPostgreSQLDefaultPortNumber) As Boolean
'
'
'    IsAdoConnectionContinuedAfterOdbcConnectionDsnlessParametersByForm = mobjConnector.IsAdoConnectionContinuedAfterOdbcConnectionDsnlessParametersByForm(vstrSettingKeyName, vstrDriverName, vstrServerHostName, vstrNetworkServiceName, vstrUserid, vstrPassword, vintPortNumber)
'End Function
'
''''
'''' Close ADO connection
''''
'Public Sub CloseConnection()
'
'    mobjConnector.CloseConnection
'End Sub
'
'
''''
''''
''''
'Public Sub CloseCacheOutputBookOfOpenByPresetModeProcess()
'
'    mobjAdoRecordsetSheetExpander.CloseCacheOutputBookOfOpenByPresetModeProcess
'End Sub
'
''''
''''
''''
'Public Sub CloseAll()
'
'    CloseConnection
'
'    CloseCacheOutputBookOfOpenByPresetModeProcess
'End Sub
'
''**---------------------------------------------
''** Output Recordset and logs to cells on sheet
''**---------------------------------------------
''''
'''' query SQL and output result to Excel sheet
''''
'Public Function OutputToSheetFrom(ByVal vstrSQL As String, _
'        ByVal vobjOutputSheet As Excel.Worksheet, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable) As ADODB.Recordset
'
'    Set OutputToSheetFrom = mobjAdoRecordsetSheetExpander.OutputToSheetFrom(vstrSQL, vobjOutputSheet, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType)
'End Function
'
'
''''
'''' query SQL and output result to the Existed Excel sheet
''''
'Public Function OutputInExistedBook(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable) As ADODB.Recordset
'
'
'    Set OutputInExistedBook = mobjAdoRecordsetSheetExpander.OutputInExistedBook(vstrSQL, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType)
'End Function
'
'
'Public Function OutputInAlreadyExistedBook(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable) As ADODB.Recordset
'
'
'    Set OutputInAlreadyExistedBook = mobjAdoRecordsetSheetExpander.OutputInAlreadyExistedBook(vstrSQL, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType)
'End Function
'
''''
''''
''''
'Public Function OutputInExistedBookByUnitTestMode(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable) As ADODB.Recordset
'
'
'    Set OutputInExistedBookByUnitTestMode = mobjAdoRecordsetSheetExpander.OutputInExistedBookByUnitTestMode(vstrSQL, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType)
'End Function
'
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Function OutputToSpecifiedBook(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable) As ADODB.Recordset
'
'
'    Set OutputToSpecifiedBook = mobjAdoRecordsetSheetExpander.OutputToSpecifiedBook(vstrSQL, vstrOutputBookPath, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType)
'End Function
'
'
'Public Function OutputToSpecifiedBookByUnitTestMode(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable) As ADODB.Recordset
'
'    Set OutputToSpecifiedBookByUnitTestMode = mobjAdoRecordsetSheetExpander.OutputToSpecifiedBookByUnitTestMode(vstrSQL, vstrOutputBookPath, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType)
'End Function
'
'
''''
'''' query SQL and output result to the already specified Excel sheet
''''
'Public Function OutputToAlreadySpecifiedBook(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable) As ADODB.Recordset
'
'    Set OutputToAlreadySpecifiedBook = mobjAdoRecordsetSheetExpander.OutputToAlreadySpecifiedBook(vstrSQL, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType)
'End Function
'
'
''''
''''
''''
'Public Sub SetOutputExcelBookPath(ByVal vstrOutputBookPath As String, _
'        Optional ByVal venmOutputRSetExcelBookOpenModeProcessFlag As OutputRSetExcelBookOpenModeProcessFlag = OutputRSetExcelBookOpenModeProcessFlag.OpenOutputBookByUnitTestMode)
'
'    mobjAdoRecordsetSheetExpander.SetOutputExcelBookPath vstrOutputBookPath, venmOutputRSetExcelBookOpenModeProcessFlag
'End Sub
'
''**---------------------------------------------
''** Excecute SQL command (UPDATE, INSERT, DELETE)
''**---------------------------------------------
''''
'''' command SQL and output result-log to Excel sheet
''''
'Public Sub OutputCommandLogToSheetFrom(ByVal vstrSQL As String, _
'        ByVal vobjOutputSheet As Excel.Worksheet, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable)
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogToSheetFrom vstrSQL, vobjOutputSheet, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType
'End Sub
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputCommandLogToSpecifiedBook(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogToSpecifiedBook vstrSQL, GetNewWorksheetAfterAllExistedSheets(vstrOutputBookPath), vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType
'End Sub
'
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputCommandLogToAlreadySpecifiedBook(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogToAlreadySpecifiedBook vstrSQL, vstrOutputBookPath, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType
'End Sub
'
'
'
'
'
'''--VBA_Code_File--<SqLiteAdoSheetExpander.cls>--
'VERSION 1.0 CLASS
'BEGIN
'  MultiUse = -1  'True
'END
'Attribute VB_Name = "SqLiteAdoSheetExpander"
'Attribute VB_GlobalNameSpace = False
'Attribute VB_Creatable = False
'Attribute VB_PredeclaredId = False
'Attribute VB_Exposed = False
''
''   output SQL result into Excel.Worksheet after connecting a SQLite database file by ADO ODBC interface
''   Probably, this capsuled class design will not be needed for almost VBA developers, because it is too complicated.
''   However the VBA programming interfaces will be united.
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on both ADO and Excel
''       Dependent on an installed SQLite ODBC driver
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Wed, 26/Jul/2023    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
'
''///////////////////////////////////////////////
''/// Interface implementations
''///////////////////////////////////////////////
'Implements IOutputBookPath
'
''///////////////////////////////////////////////
''/// Declarations
''///////////////////////////////////////////////
'Private mobjConnector As SqLiteAdoConnector: Private mitfConnector As IADOConnector
'
'Private mobjAdoRecordsetSheetExpander As AdoRSetSheetExpander
'
'
''///////////////////////////////////////////////
''/// Event Handlers
''///////////////////////////////////////////////
'Private Sub Class_Initialize()
'
'    Set mobjConnector = New SqLiteAdoConnector
'
'    Set mitfConnector = mobjConnector
'
'    Set mobjAdoRecordsetSheetExpander = New AdoRSetSheetExpander
'
'    With mobjAdoRecordsetSheetExpander
'
'        Set .ADOConnectorInterface = mobjConnector
'
'        Set .OutputBookPathInterface = Me
'
'        .DataTableSheetFormattingInterface.FieldTitleInteriorType = ColumnNameInteriorOfDefaultTypeSQLByDarkBrownFontLightSilverBgMeiryo
'    End With
'End Sub
'
''''
''''
''''
'Private Sub Class_Terminate()
'
'    mobjAdoRecordsetSheetExpander.CloseCacheOutputBookOfOpenByPresetModeProcess
'
'    Debug.Print "class AccDbAdoSheetExpander terminate -> " & mobjAdoRecordsetSheetExpander Is Nothing
'
'    Me.CloseConnection
'
'    Set mobjAdoRecordsetSheetExpander = Nothing
'
'    Set mobjConnector = Nothing
'End Sub
'
'
''///////////////////////////////////////////////
''/// Properties - connoted ADO connector
''///////////////////////////////////////////////
'Public Property Get SQLExecutionResult() As SQLResult
'
'    Set SQLExecutionResult = mobjConnector.SQLExecutionResult
'End Property
'
'Public Property Get IsSqlExecutionFailed() As Boolean
'
'    IsSqlExecutionFailed = mobjConnector.IsSqlExecutionFailed
'End Property
'
'Public Property Let AllowToRecordSQLLog(ByVal vblnAllowToRecord As Boolean)
'
'    mobjConnector.AllowToRecordSQLLog = vblnAllowToRecord
'End Property
'Public Property Get AllowToRecordSQLLog() As Boolean
'
'    AllowToRecordSQLLog = mobjConnector.AllowToRecordSQLLog
'End Property
'
'
'Public Property Get CurrentSheet() As Excel.Worksheet
'
'    Set CurrentSheet = mobjAdoRecordsetSheetExpander.IExpandAdoRecordsetOnSheet_CurrentSheet
'End Property
'
'
'Public Property Get CurrentBook() As Excel.Workbook
'
'    If Not Me.CurrentSheet Is Nothing Then
'
'        Set CurrentBook = Me.CurrentSheet.Parent
'    Else
'        Set CurrentBook = Nothing
'    End If
'End Property
'
'
''///////////////////////////////////////////////
''/// Properties
''///////////////////////////////////////////////
''''
''''
''''
'Public Property Get ADOConnectorInterface() As IADOConnector
'
'    Set ADOConnectorInterface = mitfConnector
'End Property
'
''''
''''
''''
'Public Property Get DataTableSheetFormattingInterface() As IDataTableSheetFormatter
'
'    Set DataTableSheetFormattingInterface = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface
'End Property
'
'
''///////////////////////////////////////////////
''/// Properties - connoted ADOConnectionSetting
''///////////////////////////////////////////////
'Public Property Get ConnectSetting() As ADOConnectionSetting
'
'    Set ConnectSetting = mobjConnector.ConnectSetting
'End Property
'
''''
'''' unit is second. [s]
''''
'Public Property Let CommandTimeout(ByVal vintTimeout As Long)
'
'    mobjConnector.CommandTimeout = vintTimeout
'End Property
'Public Property Get CommandTimeout() As Long
'
'    CommandTimeout = mobjConnector.CommandTimeout
'End Property
'
'
''///////////////////////////////////////////////
''/// Properties - SQLite database connection by an ODBC driver
''///////////////////////////////////////////////
'Public Property Get ConnectingFilePath() As String
'
'    ConnectingFilePath = mobjConnector.ConnectingFilePath
'End Property
'
'
''///////////////////////////////////////////////
''/// Implemented operations
''///////////////////////////////////////////////
''''
''''
''''
'Private Function IOutputBookPath_GetPreservedOutputBookPath() As String
'
'    IOutputBookPath_GetPreservedOutputBookPath = GetSqLiteConnectingPrearrangedLogBookPath()
'End Function
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''**---------------------------------------------
''** set ADO connection string all parameters directly
''**---------------------------------------------
''''
''''
''''
'Public Sub SetODBCConnectionByDSN(ByVal vstrDSN As String)
'
'    mobjConnector.SetODBCConnectionByDSN vstrDSN
'End Sub
'
''''
''''
''''
'Public Sub SetODBCParametersWithoutDSN(ByVal vstrDbFilePath As String)
'
'    mobjConnector.SetODBCParametersWithoutDSN vstrDbFilePath
'End Sub
'
'
''**---------------------------------------------
''** set ADO connection string parameters by a User-form interface
''**---------------------------------------------
''''
'''' Using a User-form, set DSN connection
''''
'''' <Argument>vstrSettingKeyName: Input - This is to be written in Windows-registry</Argument>
'''' <Argument>vstrDSN: Input</Argument>
'Public Function IsAdoConnectionContinuedAfterOdbcConnectionDsnParametersByForm(ByVal vstrSettingKeyName As String, _
'        Optional ByVal vstrDSN As String = "") As Boolean
'
'
'    IsAdoConnectionContinuedAfterOdbcConnectionDsnParametersByForm = mobjConnector.IsAdoConnectionContinuedAfterOdbcConnectionDsnParametersByForm(vstrSettingKeyName, vstrDSN)
'End Function
'
''''
'''' Using a User-form, set DSN connection
''''
'''' <Argument>vstrSettingKeyName: Input - This is to be written in Windows-registry</Argument>
'''' <Argument>vstrDbFilePath: Input</Argument>
'''' <Argument>vstrDriverName: Input</Argument>
'Public Function IsAdoConnectionContinuedAfterOdbcConnectionDsnlessParametersByForm(ByVal vstrSettingKeyName As String, _
'        Optional ByVal vstrDbFilePath As String = "", _
'        Optional ByVal vstrDriverName As String = "") As Boolean
'
'
'    IsAdoConnectionContinuedAfterOdbcConnectionDsnlessParametersByForm = mobjConnector.IsAdoConnectionContinuedAfterOdbcConnectionDsnlessParametersByForm(vstrSettingKeyName, vstrDbFilePath, vstrDriverName)
'End Function
'
'
''''
'''' Close ADO connection
''''
'Public Sub CloseConnection()
'
'    mobjConnector.CloseConnection
'End Sub
'
''''
''''
''''
'Public Sub CloseCacheOutputBookOfOpenByPresetModeProcess()
'
'    mobjAdoRecordsetSheetExpander.CloseCacheOutputBookOfOpenByPresetModeProcess
'End Sub
'
''''
''''
''''
'Public Sub CloseAll()
'
'    CloseConnection
'
'    CloseCacheOutputBookOfOpenByPresetModeProcess
'End Sub
'
''**---------------------------------------------
''** Output Recordset and logs to cells on sheet
''**---------------------------------------------
''''
'''' query SQL and output result to Excel sheet
''''
'Public Sub OutputToSheetFrom(ByVal vstrSQL As String, _
'        ByVal vobjOutputSheet As Excel.Worksheet, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'    mobjAdoRecordsetSheetExpander.OutputToSheetFrom vstrSQL, vobjOutputSheet, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedSqLitePathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
'''' query SQL and output result to the Existed Excel sheet
''''
'Public Sub OutputInExistedBook(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputInExistedBook vstrSQL, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedSqLitePathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
'Public Sub OutputInAlreadyExistedBook(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputInAlreadyExistedBook vstrSQL, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedSqLitePathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
''''
''''
'Public Sub OutputInExistedBookByUnitTestMode(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputInExistedBookByUnitTestMode vstrSQL, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedSqLitePathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputToSpecifiedBook(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputToSpecifiedBook vstrSQL, vstrOutputBookPath, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedSqLitePathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
'Public Sub OutputToSpecifiedBookByUnitTestMode(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'    mobjAdoRecordsetSheetExpander.OutputToSpecifiedBookByUnitTestMode vstrSQL, vstrOutputBookPath, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedSqLitePathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
'''' query SQL and output result to the already specified Excel sheet
''''
'Public Sub OutputToAlreadySpecifiedBook(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'    mobjAdoRecordsetSheetExpander.OutputToAlreadySpecifiedBook vstrSQL, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedSqLitePathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
''''
''''
'Public Sub SetOutputExcelBookPath(ByVal vstrOutputBookPath As String, _
'        Optional ByVal venmOutputRSetExcelBookOpenModeProcessFlag As OutputRSetExcelBookOpenModeProcessFlag = OutputRSetExcelBookOpenModeProcessFlag.OpenOutputBookByUnitTestMode, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag)
'
'    mobjAdoRecordsetSheetExpander.SetOutputExcelBookPath vstrOutputBookPath, venmOutputRSetExcelBookOpenModeProcessFlag
'End Sub
'
''**---------------------------------------------
''** Excecute SQL command (UPDATE, INSERT, DELETE)
''**---------------------------------------------
''''
'''' command SQL and output result-log to Excel sheet
''''
'Public Sub OutputCommandLogToSheetFrom(ByVal vstrSQL As String, _
'        ByVal vobjOutputSqlCommandLogSheet As Excel.Worksheet, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogToSheetFrom vstrSQL, vobjOutputSqlCommandLogSheet, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedSqLitePathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputCommandLogToSpecifiedBook(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogToSpecifiedBook vstrSQL, vstrOutputBookPath, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedSqLitePathInsertText(), vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputCommandLogToAlreadySpecifiedBook(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogToAlreadySpecifiedBook vstrSQL, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedSqLitePathInsertText(), vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputCommandLogInAlreadyExistedBook(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogInAlreadyExistedBook vstrSQL, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedSqLitePathInsertText(), vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputCommandLogToSpecifiedBookByUnitTestMode(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogToSpecifiedBookByUnitTestMode vstrSQL, vstrOutputBookPath, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedSqLitePathInsertText(), vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
'''' query SQL and output result to the prepared Excel sheet
''''
'Public Sub OutputCommandLogInExistedBookByUnitTestMode(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogInExistedBookByUnitTestMode vstrSQL, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedSqLitePathInsertText(), vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
''''
''''
''''
'Private Function mfobjGetConnectedSqLitePathInsertText() As Collection
'
'    Dim objInsertTexts As Collection
'
'    Set objInsertTexts = New Collection
'
'    With mobjConnector
'
'        objInsertTexts.Add "<File Name> " & GetFileNameFromPathByVbaDir(.ConnectingFilePath) & ", <Directory> " & GetParentDirectoryPathByVbaDir(.ConnectingFilePath)
'    End With
'
'    Set mfobjGetConnectedSqLitePathInsertText = objInsertTexts
'End Function
'
'
'''--VBA_Code_File--<SqlUTfSqLiteOdbcSheetExpander.bas>--
'Attribute VB_Name = "SqlUTfSqLiteOdbcSheetExpander"
''
''   Sanity test to output SQL results into Excel sheets for a SQLite database using ADO ODBC
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on both ADO and Excel
''       Dependent on an installed SQLite ODBC driver
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Wed, 26/Jul/2023    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Constants
''///////////////////////////////////////////////
'Private Const mstrTestTableName As String = "Test_Table"
'
'
''///////////////////////////////////////////////
''/// Control VBE windows
''///////////////////////////////////////////////
''**---------------------------------------------
''** Open relative VBE code-panes
''**---------------------------------------------
'Private Sub OpenVBECodePaneRelativeToSQLUTfSqLiteConnector()
'
'    ' Focusing on this procdure, please press [F5] key to start this. Therefore this scope is private.
'
'    On Error Resume Next ' If the 'ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma' isn't included in the VBA project, the following causes no syntax error.
'
'    Application.Run "ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma", "SqLiteAdoConnector,SqLiteAdoSheetExpander,UTfOperateSqLite3,OperateSqLite3,UTfAdoConnectSqLiteDb,ADOConStrOfOdbcSqLite,ADOConStrOfDsnlessSqLite"
'End Sub
'
'
''///////////////////////////////////////////////
''/// Sanity tests
''///////////////////////////////////////////////
''**---------------------------------------------
''** SqLiteAdoSheetExpander
''**---------------------------------------------
''''
'''' Create an in-line table from the only SELECT sql inside
''''
'Private Sub msubSanityTestToConnectByVirtualTableSqLiteAdoSheetExpander()
'
'    Dim strSQL As String
'
'    With New SqLiteAdoSheetExpander
'
'        .SetODBCParametersWithoutDSN PrepareSQLiteDbAfterCreateSampleSQLiteDbIfItDoesntExist("TestDbOfSqLiteAdoSheetExpander.sqlitedb")
'
'        .AllowToRecordSQLLog = True
'
'        'strSQL = "SELECT 1"
'
'        'strSQL = "SELECT 'ABC', 'DEF'"
'
'        strSQL = "SELECT 'ABC' AS A_Column, 'DEF' AS B_Column"
'
'        .OutputInExistedBookByUnitTestMode strSQL, "Test_ADO_VirtualTable", AppendingSqlQueryLogsInSheetCellsOnIndependentSheet
'
'        .CloseAll
'    End With
'End Sub
'
'
''''
'''' Create an in-line table from the only SELECT sql inside
''''
'Private Sub msubSanityTestToConfirmSqLiteVersion()
'
'    Dim strSQL As String
'
'    With New SqLiteAdoSheetExpander
'
'        .SetODBCParametersWithoutDSN PrepareSQLiteDbAfterCreateSampleSQLiteDbIfItDoesntExist("TestDbOfSqLiteAdoSheetExpander.sqlitedb")
'
'        .AllowToRecordSQLLog = True
'
'        strSQL = "SELECT sqlite_version()"
'
'        .OutputInExistedBookByUnitTestMode strSQL, "SqLiteVersion"
'
'        .CloseAll
'    End With
'End Sub
'
''''
''''
''''
'Private Sub msubOpenCmdWithSqLiteDbParentFolder()
'
'    OpenCmdWithSpecifyingBothWorkingDirectoryAndCmdWindowTitleText GetTemporarySqLiteDataBaseDir(), "Test SQLite db"
'End Sub
'
'
''''
''''
''''
'Private Sub msubSanityTestToGetTableList()
'
'    Dim strSQL As String
'
'    With New SqLiteAdoSheetExpander
'
'        With .DataTableSheetFormattingInterface
'
'            .RecordBordersType = RecordCellsGrayAll
'        End With
'
'        .SetODBCParametersWithoutDSN PrepareSQLiteDbAfterCreateSampleSQLiteDbIfItDoesntExist("TestDbOfSqLiteAdoSheetExpander.sqlitedb")
'
'        .AllowToRecordSQLLog = True
'
'
'        strSQL = "SELECT name FROM sqlite_master WHERE type = 'table'"
'
'        .OutputInExistedBookByUnitTestMode strSQL, "SqLiteTableList", SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable
'
'        .CloseAll
'    End With
'End Sub
'
'
''''
''''
''''
'Private Sub msubSanityTestToCreateTable()
'
'    Dim strSQL As String, strDbPath As String
'
'    strDbPath = GetTemporarySqLiteDataBaseDir() & "TestDb02OfSqLiteAdoSheetExpander.sqlitedb"
'
'    If FileExistsByVbaDir(strDbPath) Then VBA.Kill strDbPath
'
'    With New SqLiteAdoSheetExpander
'
'        .SetODBCParametersWithoutDSN strDbPath
'
'        .AllowToRecordSQLLog = True
'
'        ' strSQL = "CREATE TABLE " & mstrTestTableName & "(Col1, Col2, Col3)"
'
'        strSQL = "CREATE TABLE Test_Table(Col1, Col2, Col3)"
'
'        .OutputCommandLogInExistedBookByUnitTestMode strSQL
'
'        strSQL = "SELECT name FROM sqlite_master WHERE type = 'table'"
'
'        .OutputInExistedBookByUnitTestMode strSQL, "TableList", AppendingSqlQueryLogsInSheetCellsOnIndependentSheet
'
'        .CloseAll
'    End With
'End Sub
'
''''
'''' SQLite database sanity-test for SQL INSERT
''''
'Private Sub msubSanityTestToConnectToSqLiteBySqlInsertTable()
'
'    Dim strSQL As String
'
'    With New SqLiteAdoSheetExpander
'
'        .SetODBCParametersWithoutDSN GetTemporarySqLiteDataBaseDir() & "\" & "TestSimpleDbOfSqLiteAdoSheetExpander.sqlitedb"
'
'        .DataTableSheetFormattingInterface.RecordBordersType = RecordCellsGrayAll
'
'        .AllowToRecordSQLLog = True
'
'        PrepareTestTable4ColumnOfSqLiteTestingDataBaseForSqlTests .ADOConnectorInterface
'
'
'        strSQL = "INSERT INTO " & mstrTestTableName & " VALUES (13, 14, 15, 'Type3')"
'
'        .OutputCommandLogToSpecifiedBookByUnitTestMode strSQL, GetSqLiteConnectingPrearrangedLogBookPath()
'
'
'
'        strSQL = "SELECT * FROM Test_Table"
'
'        .OutputToSpecifiedBookByUnitTestMode strSQL, GetSqLiteConnectingPrearrangedLogBookPath(), "SqlResult", SqlQueryLogsByAutoShapesOnSheetWithOutputtingRecordsetDataTable
'
'        strSQL = "SELECT name FROM sqlite_master WHERE type = 'table'"
'
'        .OutputToSpecifiedBookByUnitTestMode strSQL, GetSqLiteConnectingPrearrangedLogBookPath(), "TableList", SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable
'
'        .CloseAll
'    End With
'End Sub
'
''''
'''' SQLite database sanity-test for SQL DELETE
''''
'Private Sub msubSanityTestToConnectToSqLiteBySqlDeleteTable()
'
'    Dim strSQL As String, strDbPath As String
'
'    strDbPath = GetTemporarySqLiteDataBaseDir() & "\" & "TestSimpleDbOfSqLiteAdoSheetExpander.sqlitedb"
'
'    If FileExistsByVbaDir(strDbPath) Then VBA.Kill strDbPath
'
'    With New SqLiteAdoSheetExpander
'
'        .SetODBCParametersWithoutDSN strDbPath
'
'        .DataTableSheetFormattingInterface.RecordBordersType = RecordCellsGrayAll
'
'        .AllowToRecordSQLLog = True
'
'        PrepareTestTable4ColumnOfSqLiteTestingDataBaseForSqlTests .ADOConnectorInterface
'
'
'        strSQL = "DELETE FROM " & mstrTestTableName & " WHERE ColText = 'Type1'"
'
'        .OutputCommandLogInExistedBookByUnitTestMode strSQL
'
'
'
'        strSQL = "SELECT * FROM Test_Table"
'
'        .OutputInExistedBookByUnitTestMode strSQL, "SqlResult", AppendingSqlQueryLogsInSheetCellsOnIndependentSheet
'
'        .CloseAll
'    End With
'End Sub
'
'
''''
'''' SQLite database sanity-test for SQL UPDATE
''''
'Private Sub msubSanityTestToConnectToSqLiteBySqlUpdateTable()
'
'    Dim strSQL As String, strDbPath As String
'
'    strDbPath = GetTemporarySqLiteDataBaseDir() & "\" & "TestSimpleDbOfSqLiteAdoSheetExpander.sqlitedb"
'
'    If FileExistsByVbaDir(strDbPath) Then VBA.Kill strDbPath
'
'    With New SqLiteAdoSheetExpander
'
'        .SetODBCParametersWithoutDSN strDbPath
'
'        .DataTableSheetFormattingInterface.RecordBordersType = RecordCellsGrayAll
'
'        .AllowToRecordSQLLog = True
'
'        PrepareTestTable4ColumnOfSqLiteTestingDataBaseForSqlTests .ADOConnectorInterface
'
'
'        strSQL = "UPDATE " & mstrTestTableName & " SET Col1 = 31, Col2 = 32 WHERE Col3 = 9 AND ColText = 'Type2'"
'
'        .OutputCommandLogToSpecifiedBook strSQL, GetSqLiteConnectingPrearrangedLogBookPath(), "SqlUpdate", SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable
'
'
'
'        strSQL = "SELECT * FROM Test_Table"
'
'        .OutputToSpecifiedBook strSQL, GetSqLiteConnectingPrearrangedLogBookPath(), "SqlResult", SqlQueryLogsByAutoShapesOnSheetWithOutputtingRecordsetDataTable
'
'        .CloseCacheOutputBookOfOpenByPresetModeProcess
'    End With
'End Sub
'''--VBA_Code_File--<ExpandCodeOnWORDOut.bas>--
'Attribute VB_Name = "ExpandCodeOnWORDOut"
''
''   output VBA code as the Word document
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on Word
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Thu,  2/Jun/2022    Kalmclaeyd Tarclanus    Initial Creation
''
'
''///////////////////////////////////////////////
''/// Directives
''///////////////////////////////////////////////
'#Const HAS_REF = False
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Constants
''///////////////////////////////////////////////
'Private Const mstrCodePrintingTemplateDocumentBaseName As String = "CodePrints"
'
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''''
'''' close all opened documents and quit Word application
''''
'Public Sub CloseAllWordDocumentWithoutDisplayAleart()
'
'    #If HAS_REF Then
'        Dim objWordApplication As Word.Application, objDoc As Word.Document
'        Dim enmOldWdAlertLevel As Word.WdAlertLevel
'    #Else
'        Dim objWordApplication As Object, objDoc As Object
'        Dim enmOldWdAlertLevel As Long
'    #End If
'
'    Dim i As Long
'
'    Set objWordApplication = mfobjGetWordApplication()
'
'    enmOldWdAlertLevel = objWordApplication.DisplayAlerts
'
'    objWordApplication.DisplayAlerts = 0 'wdAlertsNone
'
'    On Error GoTo ErrHandler
'
'    With objWordApplication
'
'        For i = .Documents.Count To 1 Step -1
'
'            Set objDoc = .Documents.Item(i)
'
'            #If HAS_REF Then
'
'                objDoc.Close Word.WdSaveOptions.wdDoNotSaveChanges
'            #Else
'                objDoc.Close 0
'            #End If
'
'            Set objDoc = Nothing
'        Next
'    End With
'
'
'    On Error GoTo 0
'
'ErrHandler:
'    If Not objWordApplication Is Nothing Then
'
'        objWordApplication.DisplayAlerts = enmOldWdAlertLevel
'
'        objWordApplication.Quit
'    End If
'
'    Set objWordApplication = Nothing
'End Sub
'
'
''''
'''' Output Code into Word.Document from a specified .dotx template file
''''
'Public Sub LoadCodeToWordDocument(ByVal vstrVBComponentName As String)
'
'    Dim strCodePath As String, objVBProject As VBIDE.VBProject
'
'#If HAS_REF Then
'
'    Dim objWordApplication As Word.Application
'#Else
'    Dim objWordApplication As Object
'#End If
'
'    With GetCurrentOfficeFileObject()
'
'        Set objVBProject = .VBProject
'    End With
'
'    With mfobjOutputVBACodeFromVBProject(objVBProject, "", "", vstrVBComponentName)
'
'        strCodePath = .Item(1)
'    End With
'
'    Set objWordApplication = mfobjGetWordApplication()
'
'    msubCodeTextToDocx strCodePath, objWordApplication
'
'    ' display the created Word object
'    If Not objWordApplication.Visible Then
'
'        ' When the new WORD process is started, the default value of the Visible is false.
'        objWordApplication.Visible = True
'    End If
'
'    Set objWordApplication = Nothing
'End Sub
'
''''
''''
''''
'#If HAS_REF Then
'
'Public Function GetSimpleDocumentAfterLoadingTextFile(ByVal vstrTextFilePath As String) As Word.Document
'
'#Else
'Public Function GetSimpleDocumentAfterLoadingTextFile(ByVal vstrTextFilePath As String) As Object
'#End If
'
'    Set GetSimpleDocumentAfterLoadingTextFile = mfobjGetSimpleDocxAfterLoadingTextFile(vstrTextFilePath, mfobjGetWordApplication())
'End Function
'
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
''''
'''' If a WORD process has already created, this get the WORD object
''''
'#If HAS_REF Then
'
'Private Function mfobjGetWordApplication() As Word.Application
'
'    Dim objWordApplication As Word.Application
'#Else
'
'Private Function mfobjGetWordApplication() As Object
'
'    Dim objWordApplication As Object
'#End If
'
'    Set objWordApplication = Nothing
'
'    On Error Resume Next
'
'    ' If WORD process has already been stared, this will get WORD object, but the WORD doesn't exist in the loaded processes, this will be failed
'    Set objWordApplication = GetObject(Class:="Word.Application")
'
'    If Err.Number <> 0 Then
'
'        Set objWordApplication = CreateObject("Word.Application")
'    End If
'
'    On Error GoTo 0
'
'    Set mfobjGetWordApplication = objWordApplication
'End Function
'
'
''''
'''' transfer the code text file to a specified format docx file
''''
'#If HAS_REF Then
'
'Private Sub msubCodeTextToDocx(ByVal vstrCodeTextFilePath As String, ByVal vobjWordApplication As Word.Application)
'
'#Else
'
'Private Sub msubCodeTextToDocx(ByVal vstrCodeTextFilePath As String, ByVal vobjWordApplication As Object)     ' Word.Application
'#End If
'
'    Dim strTemplatePath As String, blnContinue As Boolean
'
'    #If HAS_REF Then
'        Dim objDocument As Word.Document, objParagraph As Word.Paragraph, objFS As Scripting.FileSystemObject
'
'        Set objFS = New Scripting.FileSystemObject
'    #Else
'        Dim objDocument As Object, objParagraph As Object, objFS As Object
'
'        Set objFS = CreateObject("Scripting.FileSystemObject")
'    #End If
'
'    strTemplatePath = GetOfficeTemplatesRootDir() & "\" & mstrCodePrintingTemplateDocumentBaseName & ".dotx"
'
'    blnContinue = True
'
'    With objFS
'
'        If Not .FileExists(strTemplatePath) Then
'
'            blnContinue = False
'
'            MsgBox "Not found Word template file for outputting codes", vbExclamation Or vbOKOnly, "Output codes as Word document"
'        End If
'    End With
'
'    If blnContinue Then
'
'        Set objDocument = mfobjGetNewWordDocument(vobjWordApplication, strTemplatePath)
'
'        Debug.Assert Not objDocument Is Nothing
'
'        msubLoadTextToDocument objDocument, vstrCodeTextFilePath, objFS
'
'        Set objParagraph = objDocument.Paragraphs.Item(1)
'
'        msubSetParagraphFormatForSoftwareCodes objParagraph.Range.ParagraphFormat
'
'        msubSetCodeFilePathToAllWordDocumentSections objDocument, vstrCodeTextFilePath, objFS
'
'        vobjWordApplication.ActivePrinter = "Microsoft Print to PDF"
'    End If
'End Sub
'
''''
''''
''''
'#If HAS_REF Then
'Private Function mfobjGetSimpleDocxAfterLoadingTextFile(ByVal vstrTextFilePath As String, ByVal vobjWordApplication As Word.Application) As Word.Document
'
'    Dim objDocument As Word.Document, objParagraph As Word.Paragraph, objFS As Scripting.FileSystemObject
'
'    Set objFS = New Scripting.FileSystemObject
'#Else
'Private Function mfobjGetSimpleDocxAfterLoadingTextFile(ByVal vstrTextFilePath As String, ByVal vobjWordApplication As Object) As Object
'
'    Dim objDocument As Object, objParagraph As Object, objFS As Object
'
'    Set objFS = CreateObject("Scripting.FileSystemObject")
'#End If
'
'    Set objDocument = mfobjGetNewWordDocument(vobjWordApplication)
'
'    msubLoadTextToDocument objDocument, vstrTextFilePath, objFS
'
'    msubSetCodeFilePathToAllWordDocumentSections objDocument, vstrTextFilePath, objFS
'
'    vobjWordApplication.ActivePrinter = "Microsoft Print to PDF"
'
'    Set mfobjGetSimpleDocxAfterLoadingTextFile = objDocument
'End Function
'
''''
''''
''''
'Private Sub msubLoadTextToDocument(ByRef robjDocument As Word.Document, ByRef rstrTextFilePath As String, ByRef robjFS As Scripting.FileSystemObject)
'
'    Dim objParagraph As Word.Paragraph
'
'    With robjFS.OpenTextFile(rstrTextFilePath)
'
'        Set objParagraph = robjDocument.Paragraphs.Item(1)
'
'        objParagraph.Range.Text = .ReadAll
'
'        .Close
'    End With
'End Sub
'
'
''''
''''
''''
'#If HAS_REF Then
'
'Private Sub msubSetCodeFilePathToAllWordDocumentSections(ByRef robjDocument As Word.Document, ByVal vstrCodeFilePath As String, ByRef robjFS As Scripting.FileSystemObject)
'
'    Dim objSection As Word.Section, objHeader As Word.HeaderFooter
'
'#Else
'
'Private Sub msubSetCodeFilePathToAllWordDocumentSections(ByRef robjDocument As Object, ByVal vstrCodeFilePath As String, ByRef robjFS As Object)
'
'    Dim objSection As Object, objHeader As Object
'
'#End If
'
'    With robjDocument
'
'        For Each objSection In .Sections
'
'            For Each objHeader In objSection.Headers
'
'                With objHeader.Range
'
'                    .ParagraphFormat.Alignment = 2  ' wdAlignParagraphRight
'
'                    With .Font
'
'                        .Name = "Arial"
'
'                        .UnderlineColor = &HFF000000  ' wdColorAutomatic
'
'                        .Underline = 1  ' wdUnderlineSingle
'                    End With
'
'                    .Text = robjFS.GetFileName(vstrCodeFilePath)
'                End With
'            Next
'        Next
'    End With
'End Sub
'
'
''''
''''
''''
'#If HAS_REF Then
'
'Private Sub msubSetParagraphFormatForSoftwareCodes(ByRef robjParagraphFormat As Word.ParagraphFormat)
'#Else
'Private Sub msubSetParagraphFormatForSoftwareCodes(ByRef robjParagraphFormat As Object)
'#End If
'
'    With robjParagraphFormat
'
'        .LineSpacingRule = wdLineSpaceExactly
'
'        .LineSpacing = 12
'    End With
'End Sub
'
'
''''
''''
''''
'#If HAS_REF Then
'
'Private Function mfobjGetNewWordDocument(ByVal vobjWordApplication As Word.Application, Optional ByVal vstrTemplateDocumentFilePath As String = "") As Word.Document
'
'#Else
'
'Private Function mfobjGetNewWordDocument(ByVal vobjWordApplication As Object, Optional ByVal vstrTemplateDocumentFilePath As String = "") As Object
'#End If
'
'#If HAS_REF Then
'
'    Dim objDocument As Word.Document, objProtectedViewWindow As Word.ProtectedViewWindow
'#Else
'    Dim objDocument As Object, objProtectedViewWindow As Object
'#End If
'
'    On Error Resume Next
'
'    Set objDocument = Nothing
'
'    If vstrTemplateDocumentFilePath <> "" Then
'
'        Set objDocument = vobjWordApplication.Documents.Add(vstrTemplateDocumentFilePath, False, 0)       ' WdNewDocumentType.wdNewBlankDocument = 0
'    Else
'        Set objDocument = vobjWordApplication.Documents.Add()
'    End If
'
'    If Err.Number <> 0 Then
'
'        Set objProtectedViewWindow = Nothing
'
'        Set objProtectedViewWindow = vobjWordApplication.ProtectedViewWindows.Item(1)
'
'        If Not objProtectedViewWindow Is Nothing Then
'
'            Set objDocument = objProtectedViewWindow.Edit()
'
'            If Not objDocument Is Nothing Then
'
'                Set objProtectedViewWindow = Nothing
'            End If
'        End If
'    End If
'
'    On Error GoTo 0
'
'    Set mfobjGetNewWordDocument = objDocument
'End Function
'
'
'
'
''///////////////////////////////////////////////
''/// Sanity tests
''///////////////////////////////////////////////
''''
'''' Sanity-test for the Code format docx creation
''''
'Public Sub msubSanityTestCodeToDocxFromCoreCode(Optional ByVal vstrClassifyingKey01 As String = "OfficeCommonCore", _
'        Optional ByVal vstrClassifyingKey02 As String = "", _
'        Optional ByVal vstrFilteringComponentNames As String = "")
'
'    Dim objCodePaths As Collection, varCodePath As Variant, strCodePath As String
'
'#If HAS_REF Then
'    Dim objWordApplication As Word.Application
'#Else
'    Dim objWordApplication As Object
'#End If
'
'    Set objCodePaths = GetComponentCodeFullPathsAtDefaultOnlyFileRepository(vstrClassifyingKey01, vstrClassifyingKey02, vstrFilteringComponentNames)
'
'    If Not objCodePaths Is Nothing Then
'
'        Set objWordApplication = mfobjGetWordApplication()
'
'        For Each varCodePath In objCodePaths
'
'            strCodePath = varCodePath
'
'            msubCodeTextToDocx strCodePath, objWordApplication
'        Next
'
'        ' display the created Word object
'        If Not objWordApplication.Visible Then
'
'            ' When the new WORD process is started, the default value of the Visible is false.
'            objWordApplication.Visible = True
'        End If
'    Else
'        MsgBox "Not searched VB component file-path", vbInformation Or vbOKOnly, "VBA codes to Word document process"
'    End If
'
'    Set objWordApplication = Nothing
'End Sub
'
''''
''''
''''
'Private Sub msubSanityTestToCodeToDocxAboudDumpModule()
'
'    LoadCodeToWordDocument "DumpModule"
'End Sub
'
'
''''
''''
''''
'Private Sub msubSanityTestToDumpOutputCodePaths()
'
'    msubDumpOutputCodePaths "", "", "DumpModule"
'End Sub
'
'
''''
''''
''''
'Private Sub msubDumpOutputCodePaths(ByVal vstrClassifyingKey01 As String, ByVal vstrClassifyingKey02 As String, ByVal vstrFilteringComponentNames As String)
'
'    Dim varFilePath As Variant
'
'    For Each varFilePath In mfobjGetOutputCodePaths(vstrClassifyingKey01, vstrClassifyingKey02, vstrFilteringComponentNames)
'
'        Debug.Print varFilePath
'    Next
'End Sub
'
''''
''''
''''
'Private Function mfobjOutputVBACodeFromVBProject(ByVal vobjVBProject As VBIDE.VBProject, ByVal vstrClassifyingKey01 As String, ByVal vstrClassifyingKey02 As String, ByVal vstrFilteringComponentNames As String) As Collection
'
'    Dim objFilePaths As Collection, objFS As Scripting.FileSystemObject, strDir As String
'    Dim varFilePath As Variant, objOutputFilePaths As Collection, objVBComponent As VBIDE.VBComponent, strComponentName As String
'
'
'    Set objFilePaths = mfobjGetOutputCodePaths(vstrClassifyingKey01, vstrClassifyingKey02, vstrFilteringComponentNames)
'
'    Set objOutputFilePaths = New Collection
'
'    Set objFS = New Scripting.FileSystemObject
'
'    With vobjVBProject
'
'        For Each varFilePath In objFilePaths
'
'            strComponentName = objFS.GetBaseName(varFilePath)
'
'            Set objVBComponent = Nothing
'
'            On Error Resume Next
'
'            Set objVBComponent = .VBComponents.Item(strComponentName)
'
'            On Error GoTo 0
'
'
'            If Not objVBComponent Is Nothing Then
'
'                With objFS
'
'                    strDir = objFS.GetParentFolderName(varFilePath)
'
'                    ForceToCreateDirectory strDir, objFS
'
'                    If .FileExists(varFilePath) Then
'
'                        .DeleteFile varFilePath, True
'                    End If
'                End With
'
'                objVBComponent.Export varFilePath
'
'                objOutputFilePaths.Add varFilePath
'            End If
'        Next
'    End With
'
'    Set mfobjOutputVBACodeFromVBProject = objOutputFilePaths
'End Function
'
'
''''
''''
''''
'Private Function mfobjGetOutputCodePaths(ByVal vstrClassifyingKey01 As String, ByVal vstrClassifyingKey02 As String, ByVal vstrFilteringComponentNames As String) As Collection
'
'    Dim strBaseDir As String, objComponentNameToRelativeFilePathDic As Scripting.Dictionary
'    Dim varComponentName As Variant
'    Dim strFilePath As String, objCol As Collection
'
'
'    Set objCol = New Collection
'
'    strBaseDir = GetTmpCompareRootPathOriginal()
'
'    Set objComponentNameToRelativeFilePathDic = GetComponentNameToRelativeFilePathDic(vstrClassifyingKey01, vstrClassifyingKey02, vstrFilteringComponentNames)
'
'    With objComponentNameToRelativeFilePathDic
'
'        For Each varComponentName In .Keys
'
'            strFilePath = strBaseDir & "\" & .Item(varComponentName)
'
'            objCol.Add strFilePath
'        Next
'    End With
'
'    Set mfobjGetOutputCodePaths = objCol
'End Function
'
'
''''
''''
''''
'Public Sub SanityTestCodeToDocxFromCoreCode()
'
'    Dim strSpecifiedComponentNames As String
'
'    'strSpecifiedComponentNames = ""
'    'strSpecifiedComponentNames = "DataTableSheetOut"
'
'    'strSpecifiedComponentNames = "DecorationSetterToXlSheet,DecorationGetterFromXlSheet,ADOSheetOut,DataTableSheetOut,LoadAndModifyModule"
'
'    'msubSanityTestCodeToDocxFromCoreCode "", "", strSpecifiedComponentNames
'
'
'
'    msubSanityTestCodeToDocxFromCoreCode "CoreCode,ADOAbstract,ADOODBC,AdoErrorHandlingTools,XlAdoConnectingTools", "LibGeneral,DevMManaging"
'
'    'msubSanityTestCodeToDocxFromCoreCode "CoreCode,IndependentCellParam", "LibGeneral", strSpecifiedComponentNames
'
'    'msubSanityTestCodeToDocxFromCoreCode "CoreCode,ADOODBC,XlAdoConnectingTools,MakeHyperLinks,ADOAbstract", "LIbGeneral"
'
'    'msubSanityTestCodeToDocxFromCoreCode "ADOAbstract", "LibGeneral"
'
'End Sub
'
''''
''''
''''
'Public Sub SanityTestCodeToDocxFromConverToVBS()
'
'    'msubSanityTestCodeToDocxFromCoreCode "", "DevMManaging"
'
'    msubSanityTestCodeToDocxFromCoreCode "", "", "DumpModule"
'End Sub

