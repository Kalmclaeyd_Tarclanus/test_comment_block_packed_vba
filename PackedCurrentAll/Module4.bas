Attribute VB_Name = "Module4"
'
'   PackedCurrentAll [ 04 / 04 ] Self-extracting comment-block packed VBA source codes, which is generated automatically
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Count of packed modules:
'       29 / 359
'
'   Lack user-form sources:
'       UTestFormTextRClickClipBoard - CommonOfficeVBA\FormTools\UnitTest\UTestFormTextRClickClipBoard.frm
'       UInputPasswordBox - CommonOfficeVBA\FormTools\ManagePassword\UInputPasswordBox.frm
'       LogTextForm - CommonOfficeVBA\LoggingModules\LogTextForm\LogTextForm.frm
'       UProgressBarForm - CommonOfficeVBA\UserFormTools\UProgressBarForm.frm
'       UTestFormNoFormTop - CommonOfficeVBA\WindowControlModules\CommonControlHandlers\UnitTest\UTestFormNoFormTop.frm
'       UTestFormWithFormTop - CommonOfficeVBA\WindowControlModules\CommonControlHandlers\UnitTest\UTestFormWithFormTop.frm
'       UTestFormKeepStateReg - CommonOfficeVBA\RegistryAccess\UTestFormKeepStateReg.frm
'       CodePaneSwitchMultiLstForm - CommonOfficeVBA\DevelopmentAssist\SwitchCodePanes\CodePaneSwitchMultiLstForm.frm
'       CodePaneSwitchMultiLsvForm - CommonOfficeVBA\DevelopmentAssist\SwitchCodePanes\CodePaneSwitchMultiLsvForm.frm
'       CodePaneSwitchSingleLsvForm - CommonOfficeVBA\DevelopmentAssist\SwitchCodePanes\CodePaneSwitchSingleLsvForm.frm
'       USetAdoOleDbConStrForXlBook - CommonOfficeVBA\ADOModules\USetAdoOleDbConStrForXlBook.frm
'       USetAdoOdbcConStrForSqLite - CommonOfficeVBA\SQLiteConnector\USetAdoOdbcConStrForSqLite.frm
'       USetAdoOleDbConStrForAccdb - CommonOfficeVBA\ADOModules\MsAccessTool\USetAdoOleDbConStrForAccdb.frm
'       USetAdoOdbcConStrForPgSql - CommonOfficeVBA\PostgreSQLConnector\USetAdoOdbcConStrForPgSql.frm
'       USetAdoOdbcConStrForOracle - CommonOfficeVBA\OracleConnector\USetAdoOdbcConStrForOracle.frm
'       UErrorADOSQLForm - CommonOfficeVBA\ADOModules\AdoErrorHandling\UErrorADOSQLForm.frm
'       UAdjustPlottingPaperSheet - ExcelVBA\SheetAsPlottingPaper\UAdjustPlottingPaperSheet.frm
'
'   In this packed source files:
'       WinINIGeneralByAdoXl.bas, UTfWinINIGeneralByAdoXl.bas, XlAdoSheetExpander.cls, UTfAdoConnectExcelSheet.bas, SqlUTfXlSheetExpander.bas
'       SqlUTfXlSheetExpanderByPassword.bas, UTfReadFromShapeByAdoXl.bas, CsvAdoSheetExpander.cls, SqlUTfCsvAdoSheetExpander.bas, UTfReadFromShapeByAdoCsv.bas
'       UTfAdoConnectAccDbForXl.bas, AccDbAdoSheetExpander.cls, SqlUTfAccDbSheetExpander.bas, PgSqlOdbcSheetExpander.cls, SqlUTfPgSqlOdbcSheetExpander.bas
'       OracleOdbcSheetExpander.cls, SqLiteAdoSheetExpander.cls, SqlUTfSqLiteOdbcSheetExpander.bas, EnumerateMsWords.bas, OperateOutlook.bas
'       OperateOutlookToClassify.bas, OperateOutlookForXl.bas, OperateOutlookToClassifyForXl.bas, EMailMovedPersonalBoxLogs.cls, EMailClassifyingCondition.cls
'       OutlookHyperLink.bas, OutlookHyperLinkForXl.bas, UTfOperateOutlook.bas, UTfOperateOutlookForXl.bas
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Sun, 10/Nov/2024    Tarclanus-generator     Generated automatically
'
Option Explicit

'///////////////////////////////////////////////
'/// Directives
'///////////////////////////////////////////////
#Const HAS_REF = False

'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////

Private Const mstrThisExtractionModuleName As String = "Module4"

Private Const mstrALoadingDirectionVBProjectName As String = "ConvergenceProj"

'**---------------------------------------------
'** Block preserved keyword definitions
'**---------------------------------------------
Private Const mstrBlockDelimiterOfCodesBegin As String = "'''--Begin the comment out codes block--"

Private Const mstrBlockDelimiterOfASourceFileBeginLeft As String = "'''--VBA_Code_File--<"

Private Const mstrBlockDelimiterOfASourceFileBeginRight As String = ">--"

'**---------------------------------------------
'** Temporary files paths
'**---------------------------------------------
Private Const mstrTmpSubDir As String = "TmpCBSubDirModule4"

'///////////////////////////////////////////////
'/// Declarations
'///////////////////////////////////////////////
Private mstrCacheWorkTmpDir As String

'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
''' Extract all VBA source files from this file comment block and load these into the specified VBA project name Macro book
'''
Public Sub ExtractVbaCodesFrom_PackedCurrentAll04_InThisCommentBlock()

    Dim objBook As Excel.Workbook
    
    Set objBook = mfobjGetBookAfterLoadingVbaCodesFromThisVbaModuleCommentBlock()
End Sub


'''
'''
'''
Private Sub msubSanityTestToWriteSourceCodesFromThisVbaModuleCommentBlockToTemporaryDirectory()

    msubWriteSourceCodesFromThisVbaModuleCommentBlockToTemporaryDirectory
End Sub


'''
'''
'''
Private Sub msubSanityTestToGetFileNameToTextDicFromThisVbaModuleCommentBlock()

    Dim objDic As Scripting.Dictionary: Set objDic = mfobjGetFileNameToTextDicFromThisVbaModuleCommentBlock()

    'DebugDic objDic
End Sub

'''
'''
'''
Private Sub msubSanityTestToConfirmThisText()

    Debug.Print mfstrGetThisVbaModuleText()
End Sub



'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////

'''
'''
'''
Private Function mfobjGetBookAfterLoadingVbaCodesFromThisVbaModuleCommentBlock(Optional ByVal vstrTmpVbaCodesSubDir As String = mstrTmpSubDir) As Excel.Workbook

    Dim objBook As Excel.Workbook
    
    Dim strTmpDir As String
   
#If HAS_REF Then

    Dim objFS As Scripting.FileSystemObject, objDir As Scripting.Folder, objFile As Scripting.File
    
    Dim objVBProject As VBIDE.VBProject
    
    Set objFS = New Scripting.FileSystemObject
#Else
    Dim objFS As Object, objDir As Object, objFile As Object
    
    Dim objVBProject As Object
    
    Set objFS = CreateObject("Scripting.FileSystemObject")
#End If
  
    msubWriteSourceCodesFromThisVbaModuleCommentBlockToTemporaryDirectory vstrTmpVbaCodesSubDir


    Set objBook = mfobjGetTargetWorkbookOfSpecifiedVBProjectName()

    Set objVBProject = objBook.VBProject

    strTmpDir = mfstrGetWorkingDir() & "\" & vstrTmpVbaCodesSubDir

    Set objDir = objFS.GetFolder(strTmpDir)

    For Each objFile In objDir.Files
    
        objVBProject.VBComponents.Import objFile.Path
    Next

    Set mfobjGetBookAfterLoadingVbaCodesFromThisVbaModuleCommentBlock = objBook
End Function

'''
'''
'''
Private Function mfobjGetTargetWorkbookOfSpecifiedVBProjectName() As Excel.Workbook

    Dim objBook As Excel.Workbook, objTargetBook As Excel.Workbook
    
#If HAS_REF Then

    Dim objVBProject As VBIDE.VBProject
#Else
    Dim objVBProject As Object
#End If

    Set objTargetBook = Nothing

    For Each objBook In ThisWorkbook.Application.Workbooks
    
        Set objVBProject = objBook.VBProject
        
        If StrComp(objVBProject.Name, mstrALoadingDirectionVBProjectName) = 0 Then
        
            Set objTargetBook = objBook
            
            Exit For
        End If
    Next
    
    If objTargetBook Is Nothing Then
    
        Set objTargetBook = ThisWorkbook.Application.Workbooks.Add()
        
        objTargetBook.VBProject.Name = mstrALoadingDirectionVBProjectName
    End If

    Set mfobjGetTargetWorkbookOfSpecifiedVBProjectName = objTargetBook
End Function


'''
'''
'''
Private Sub msubWriteSourceCodesFromThisVbaModuleCommentBlockToTemporaryDirectory(Optional ByVal vstrTmpVbaCodesSubDir As String = mstrTmpSubDir)

#If HAS_REF Then
    Dim objSourceFileNameToCodesDic As Scripting.Dictionary
    
    Dim objFS As Scripting.FileSystemObject, objFile As Scripting.File
    
    Set objFS = New Scripting.FileSystemObject
#Else
    Dim objSourceFileNameToCodesDic As Object
    
    Dim objFS As Object, objFile As Object
    
    Set objFS = CreateObject("Scripting.FileSystemObject")
#End If

    Dim varSourceFileName As Variant, strSourceFilePath As String
    Dim strTmpDir As String
    
    
    strTmpDir = mfstrGetWorkingDir() & "\" & vstrTmpVbaCodesSubDir
    
    With objFS
    
        If Not .FolderExists(strTmpDir) Then
        
            .CreateFolder strTmpDir
        Else
            ' delete all
            
            For Each objFile In .GetFolder(strTmpDir).Files
        
                objFile.Delete
            Next
        End If
    End With
    
    
    Set objSourceFileNameToCodesDic = mfobjGetFileNameToTextDicFromThisVbaModuleCommentBlock()

    With objSourceFileNameToCodesDic

        For Each varSourceFileName In .Keys
        
            strSourceFilePath = strTmpDir & "\" & varSourceFileName
        
            With objFS
            
                With .CreateTextFile(strSourceFilePath, True)
                
                    .Write objSourceFileNameToCodesDic.Item(varSourceFileName)
                
                    .Close
                End With
            End With
        Next
    End With
End Sub


'''
'''
'''
Private Function mfstrGetWorkingDir() As String

    Dim strTmpDir As String, strSystemDriveLetter As String, strDirectoryName

    Dim strWorkDir As String
    
#If HAS_REF Then
    Dim objWshShell As IWshRuntimeLibrary.WshShell

    Dim objFS As Scripting.FileSystemObject, objFile As Scripting.File, objParentFolder As Scripting.Folder, objFolder As Scripting.Folder
    
    Set objFS = New Scripting.FileSystemObject
    
    Set objWshShell = New IWshRuntimeLibrary.WshShell
#Else
    Dim objWshShell As Object
    
    Dim objFS As Object, objFile As Object, objParentFolder As Object, objFolder As Object
    
    Set objFS = CreateObject("Scripting.FileSystemObject")
    
    Set objWshShell = CreateObject("WScript.Shell")
#End If

    If mstrCacheWorkTmpDir = "" Then

        strTmpDir = ThisWorkbook.Path
        
        With objFS
        
            If Not .FolderExists(strTmpDir) Then
            
                strSystemDriveLetter = Left(.GetSpecialFolder(1), 1)    ' Scripting.SpecialFolderConst.SystemFolder is 1
            
                Set objParentFolder = .GetFolder(strSystemDriveLetter & ":")
            
                strWorkDir = ""
            
                strDirectoryName = VBA.Dir(strSystemDriveLetter & ":\", vbDirectory)

                Do While strDirectoryName <> ""
                
                    Select Case LCase(strDirectoryName) ' the file name is also matched by the VBA.Dir() specification
                    
                        Case "tmp", "work", "localwork"
                    
                            If .FolderExists(strSystemDriveLetter & ":\" & strDirectoryName) Then
                            
                                strWorkDir = strSystemDriveLetter & ":\" & strDirectoryName
                                
                                Exit Do
                            End If
                    End Select
                
                    strDirectoryName = VBA.Dir()
                Loop
                
            
                If strWorkDir = "" Then
                
                    strWorkDir = objWshShell.SpecialFolders("MyDocuments") & "\tmp"
                    
                    If Not .FolderExists(strWorkDir) Then
                    
                        .CreateFolder strWorkDir
                    End If
                    
                    strWorkDir = strWorkDir & "\Vba"
                    
                    If Not .FolderExists(strWorkDir) Then
                    
                        .CreateFolder strWorkDir
                    End If
                End If
            
                strTmpDir = strWorkDir
            End If
        End With
        
        mstrCacheWorkTmpDir = strTmpDir
    Else
        strTmpDir = mstrCacheWorkTmpDir
    End If

    mfstrGetWorkingDir = strTmpDir
End Function

'''
'''
'''
#If HAS_REF Then
Private Function mfobjGetFileNameToTextDicFromThisVbaModuleCommentBlock() As Scripting.Dictionary

    Dim objDic As Scripting.Dictionary: Set objDic = New Scripting.Dictionary
#Else
Private Function mfobjGetFileNameToTextDicFromThisVbaModuleCommentBlock() As Object

    Dim objDic As Object: Set objDic = CreateObject("Scripting.Dictionary")
#End If
    Dim varLineText As Variant
    Dim blnFoundBlockOfCodesBegin As Boolean
    Dim blnInASourceFileBlock As Boolean, strCurrentSourceFileName As String
    Dim intLeftPoint As Long, intRightPoint As Long, intChildSourceLineNumber As Long
    Dim strCurrentChildSourceText As String
    
    
    blnFoundBlockOfCodesBegin = False
    
    blnInASourceFileBlock = False
    
    strCurrentChildSourceText = ""

    For Each varLineText In Split(mfstrGetThisVbaModuleText(), vbNewLine)

        If Not blnFoundBlockOfCodesBegin Then
        
            If InStr(1, varLineText, "'") = 1 Then
            
                If InStr(1, varLineText, mstrBlockDelimiterOfCodesBegin) > 0 Then
                
                    blnFoundBlockOfCodesBegin = True
                End If
            End If
        End If

        If blnFoundBlockOfCodesBegin Then
        
            If Not blnInASourceFileBlock Then
            
                intLeftPoint = InStr(1, varLineText, mstrBlockDelimiterOfASourceFileBeginLeft)
                
                If intLeftPoint > 0 Then
                
                    intRightPoint = InStrRev(varLineText, mstrBlockDelimiterOfASourceFileBeginRight)
                    
                    strCurrentSourceFileName = Mid(varLineText, intLeftPoint + Len(mstrBlockDelimiterOfASourceFileBeginLeft), intRightPoint - (intLeftPoint + Len(mstrBlockDelimiterOfASourceFileBeginLeft)))
                    
                    intChildSourceLineNumber = 0
                    
                    blnInASourceFileBlock = True
                End If
            End If
            
            
            If blnInASourceFileBlock Then
            
                If intChildSourceLineNumber > 0 Then
                
                    intLeftPoint = InStr(1, varLineText, mstrBlockDelimiterOfASourceFileBeginLeft)
                    
                    If intLeftPoint > 0 Then
                    
                        ' save cache the current source text
                        If strCurrentChildSourceText <> "" Then
                        
                            objDic.Add strCurrentSourceFileName, strCurrentChildSourceText
                        End If
                    
                    
                        ' Prepare the new file
                        intRightPoint = InStrRev(varLineText, mstrBlockDelimiterOfASourceFileBeginRight)
                    
                        strCurrentSourceFileName = Mid(varLineText, intLeftPoint + Len(mstrBlockDelimiterOfASourceFileBeginLeft), intRightPoint - (intLeftPoint + Len(mstrBlockDelimiterOfASourceFileBeginLeft)))
                    
                        intChildSourceLineNumber = 0
                    
                        strCurrentChildSourceText = ""
                        
                        blnInASourceFileBlock = True
                    End If
                End If
                
                If intChildSourceLineNumber > 0 Then
                
                    ' confirm the line head character is the line comment char
                    If InStr(1, varLineText, "'") = 1 Then
                    
                        If strCurrentChildSourceText <> "" Then
                        
                            strCurrentChildSourceText = strCurrentChildSourceText & vbNewLine
                        End If
                    
                        ' get a source code line
                    
                        strCurrentChildSourceText = strCurrentChildSourceText & Right(varLineText, Len(varLineText) - 1)
                    Else
                        ' save cache the current source text
                        If strCurrentChildSourceText <> "" Then
                        
                            objDic.Add strCurrentSourceFileName, strCurrentChildSourceText
                        End If
                        
                        intChildSourceLineNumber = 0
                    
                        strCurrentChildSourceText = ""
                        
                        blnInASourceFileBlock = False
                    End If
                End If
            
                intChildSourceLineNumber = intChildSourceLineNumber + 1
            End If
        End If
    Next

    Set mfobjGetFileNameToTextDicFromThisVbaModuleCommentBlock = objDic
End Function


'''
'''
'''
Private Function mfstrGetThisVbaModuleText() As String

#If HAS_REF Then
    Dim objVBProject As VBIDE.VBProject, objVBComponent As VBIDE.VBComponent
#Else
    Dim objVBProject As Object, objVBComponent As Object
#End If
    Dim strText As String
    
    Set objVBProject = ThisWorkbook.VBProject
    
    Set objVBComponent = objVBProject.VBComponents.Item(mstrThisExtractionModuleName)

    With objVBComponent.CodeModule
    
        strText = .Lines(1, .CountOfLines)
    End With

    mfstrGetThisVbaModuleText = strText
End Function


'''--Begin the comment out codes block--
'''--VBA_Code_File--<WinINIGeneralByAdoXl.bas>--
'Attribute VB_Name = "WinINIGeneralByAdoXl"
''
''   With ADODB connection, output read INI file results to Excel worksheet
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on both Windows OS and Excel
''       Dependent on ADODB
''       Dependent on WinINIGeneralForXl.bas
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2024 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Sun, 18/Aug/2024    Kalmclaeyd Tarclanus    Separated from WinINIGeneralForXl.bas
''
'
'Option Explicit
'
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''**---------------------------------------------
''** About Excel three columns sheet compatible INI file data structure
''**---------------------------------------------
''''
'''' load test
''''
'Public Function OutputIniCompatibleExcelSheetAllKeyValueToBook(ByVal vstrInputINICompatibleBookPath As String, _
'        ByVal vstrInputINICompatibleSheetName As String, _
'        ByVal vstrOutputBookPath As String, _
'        Optional ByVal vstrOutputSheetName As String = "INI_All", _
'        Optional ByVal vstrFieldTitleToColumnWidthDelimitedByComma As String = "Section,15,Key,15,Value,15") As Excel.Worksheet
'
'
'    Dim objSectionToKeyValueDic As Scripting.Dictionary, objSheet As Excel.Worksheet
'
'    Set objSectionToKeyValueDic = GetSectionToKeyValueDicFromThreeColumnsFormattedExcelBookByAdoSQLWithSpecialConversion(vstrInputINICompatibleBookPath, vstrInputINICompatibleSheetName)
'
'    Set objSheet = GetWorkbookAndAnWorksheetPrepareForUnitTest(vstrOutputBookPath, vstrOutputSheetName)
'
'    OutputOneNestedDicToSheetRestrictedByDataTableSheetFormatter objSheet, _
'            objSectionToKeyValueDic, _
'            GetDataTableSheetFormatterForIniSectionKeyValueDic(vstrFieldTitleToColumnWidthDelimitedByComma, True)
'
'    AddAutoShapeGeneralMessage objSheet, GetAutoShapeTextLogOfIniAllKeyValue(objSheet, vstrInputINICompatibleBookPath, True)
'
'    Set OutputIniCompatibleExcelSheetAllKeyValueToBook = objSheet
'End Function
'
'
''**---------------------------------------------
''** About INI file
''**---------------------------------------------
''''
'''' An improvement point - this doesn't need to open Excel book before get a nested Dictionary object
''''
'''' <Argument>vstrInputBookPath: Input</Argument>
'''' <Argument>vstrInputSheetName: Input</Argument>
'''' <Argument>vblnAllowToCloseBookAfterDetectData: Input</Argument>
'''' <Return>Dictionary(Of String[Section], Dictionary(Of String[Key], Variant[Variant]))</Return>
'Public Function GetSectionToKeyValueDicFromThreeColumnsFormattedExcelBookByAdoSQLWithSpecialConversion(ByVal vstrInputBookPath As String, _
'        Optional ByVal vstrInputSheetName As String = "INI_All", _
'        Optional ByVal vblnAllowToCloseBookAfterDetectData As Boolean = True) As Scripting.Dictionary
'
'    Dim objDic As Scripting.Dictionary, strSQL As String
'    Dim objSectionToKeyValueDic As Scripting.Dictionary, objBook As Excel.Workbook, objSheet As Excel.Worksheet
'
'    ' Probably, Avoiding ADO causes faster getting results
'
'    With New XlAdoConnector
'
'        .SetInputExcelBookPath vstrInputBookPath
'
'        ' Key is a SQL reserved word, then ADO (SQL92) needs brackets, such as '[Key]'
'
'        strSQL = "SELECT [Section], [Key], [Value] FROM [" & vstrInputSheetName & "$] WHERE [Key] IS NOT NULL AND [Value] IS NOT NULL"
'
'        Set objDic = GetTableOneNestedDictionaryFromRSetWithSpecialConversion(.GetRecordset(strSQL))
'    End With
'
'    Set GetSectionToKeyValueDicFromThreeColumnsFormattedExcelBookByAdoSQLWithSpecialConversion = objDic
'End Function
'
'
''''
'''' A defect point - this needs to open Excel book before get a nested Dictionary object
''''
'''' <Argument>vstrInputBookPath: Input</Argument>
'''' <Argument>vstrInputSheetName: Input</Argument>
'''' <Argument>vblnAllowToCloseBookAfterDetectData: Input</Argument>
'''' <Return>Dictionary(Of String[Section], Dictionary(Of String[Key], Variant[Variant]))</Return>
'Public Function GetSectionToKeyValueDicFromThreeColumnsFormattedExcelBookByAdoSQL(ByVal vstrInputBookPath As String, _
'        Optional ByVal vstrInputSheetName As String = "INI_All", _
'        Optional ByVal vblnAllowToCloseBookAfterDetectData As Boolean = True) As Scripting.Dictionary
'
'    Dim objDic As Scripting.Dictionary, strSQL As String
'    Dim objSectionToKeyValueDic As Scripting.Dictionary, objBook As Excel.Workbook, objSheet As Excel.Worksheet
'
'
'    Set objBook = GetWorkbook(vstrInputBookPath, False)
'
'    Set objSheet = objBook.Worksheets.Item(vstrInputSheetName)
'
'    If IsAtLeastOneMergedCellIncluded(objSheet, GetColFromLineDelimitedChar("Section")) Then
'
'        RemoveMergedCellsToTable objSheet, GetColFromLineDelimitedChar("Section")
'
'        ForceToSaveBookWithoutDisplayAlert objBook
'    End If
'
'    If vblnAllowToCloseBookAfterDetectData Then
'
'        ' Need to save for the next SQL query
'
'        If Not objBook.Saved Then
'
'            ForceToSaveBookWithoutDisplayAlert objBook
'        End If
'
'        CloseBookWithoutDisplayAlerts objBook
'    End If
'
'    ' Probably, Avoiding ADO causes faster getting results
'
'    With New XlAdoConnector
'
'        .SetInputExcelBookPath vstrInputBookPath
'
'
'        ' Key is a SQL reserved word, then ADO (SQL92) needs brackes, such as '[Key]'
'
'
'        strSQL = "SELECT [Section], [Key], [Value] FROM [" & vstrInputSheetName & "$] WHERE [Section] IS NOT NULL AND [Key] IS NOT NULL AND [Value] IS NOT NULL"
'
'
'        'strSQL = "SELECT [Section], [Key], [Value] FROM [" & vstrInputSheetName & "$]"
'
'        'strSQL = "SELECT * FROM [" & vstrInputSheetName & "$]"
'
'
'        Set objDic = GetTableOneNestedDictionaryFromRSet(.GetRecordset(strSQL))
'    End With
'
'    Set GetSectionToKeyValueDicFromThreeColumnsFormattedExcelBookByAdoSQL = objDic
'End Function
'
'''--VBA_Code_File--<UTfWinINIGeneralByAdoXl.bas>--
'Attribute VB_Name = "UTfWinINIGeneralByAdoXl"
''
''   Sanity tests for editing Windows INI file by Windows API
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on both Windows OS and Excel
''       Dependent on ADODB
''       Dependent on InterfaceCall.bas and WinAPIMessageWithTimeOut
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2024 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Sun, 18/Aug/2024    Kalmclaeyd Tarclanus    Refactoring
''
'
'Option Explicit
'
'
''///////////////////////////////////////////////
''/// Sanity tests
''///////////////////////////////////////////////
''''
''''
''''
'Private Sub msubSanityTestToGetSectionToKeyValueDicWithoutOpenBookFromSheet01()
'
'    Dim objSectionToKeyValueDic As Scripting.Dictionary, strInputBookPath As String, strIniBaseName As String
'
'    Dim strBookBaseName As String
'
'
'    strBookBaseName = "PermittedCharactersINIAll"
'
'    strInputBookPath = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\" & strBookBaseName & ".xlsx"
'
'    With New Scripting.FileSystemObject
'
'        If Not .FileExists(strInputBookPath) Then
'
'            GetAllIniBookPathAfterCreateSpecialTestingCharacters strBookBaseName, "CharacterLimitationINITest"
'        End If
'    End With
'
'    ' using DataTableSheetIn.bas
'    Set objSectionToKeyValueDic = GetSectionToKeyValueDicFromThreeColumnsFormattedExcelBookByAdoSQLWithSpecialConversion(strInputBookPath)
'
'    DebugDic objSectionToKeyValueDic
'End Sub
'
'
''''
''''
''''
'Private Sub msubSanityTestToGetSectionToKeyValueDicWithoutOpenBookFromSheet02()
'
'    Dim objSectionToKeyValueDic As Scripting.Dictionary, strInputBookPath As String, strIniBaseName As String
'    Dim strBookBaseName As String
'
'    strBookBaseName = "OrdinalCharactersINIAll"
'
'    strInputBookPath = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\" & strBookBaseName & ".xlsx"
'
'    With New Scripting.FileSystemObject
'
'        If Not .FileExists(strInputBookPath) Then
'
'            GetAllIniBookPathAfterCreateOrdinalSampleCharacters strBookBaseName, "OrdinalCharactersINITest"
'        End If
'    End With
'
'    Set objSectionToKeyValueDic = GetSectionToKeyValueDicFromThreeColumnsFormattedExcelBookByAdoSQLWithSpecialConversion(strInputBookPath)
'
'    DebugDic objSectionToKeyValueDic
'End Sub
'
'
''''
''''
''''
'Private Sub msubSanityTestToGetSectionToKeyValueDicFromSheet01()
'
'    Dim objSectionToKeyValueDic As Scripting.Dictionary, strInputBookPath As String, strIniBaseName As String
'    Dim strBookBaseName As String
'
'    strBookBaseName = "PermittedCharactersINIAll"
'
'    strInputBookPath = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\" & strBookBaseName & ".xlsx"
'
'    With New Scripting.FileSystemObject
'
'        If Not .FileExists(strInputBookPath) Then
'
'            GetAllIniBookPathAfterCreateSpecialTestingCharacters strBookBaseName, "CharacterLimitationINITest"
'        End If
'    End With
'
'    ' using ADO
'    'Set objSectionToKeyValueDic = GetSectionToKeyValueDicFromThreeColumnsFormattedExcelBookByAdoSQL(strInputBookPath)
'
'    ' using DataTableSheetIn.bas
'    Set objSectionToKeyValueDic = GetSectionToKeyValueDicFromThreeColumnsFormattedExcelBook(strInputBookPath)
'
'    DebugDic objSectionToKeyValueDic
'End Sub
'
'
''''
''''
''''
'Private Sub msubSanityTestToGetSectionToKeyValueDicFromSheet02()
'
'    Dim objSectionToKeyValueDic As Scripting.Dictionary, strInputBookPath As String, strIniBaseName As String
'    Dim strBookBaseName As String
'
'    strBookBaseName = "OrdinalCharactersINIAll"
'
'    strInputBookPath = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\" & strBookBaseName & ".xlsx"
'
'    With New Scripting.FileSystemObject
'
'        If Not .FileExists(strInputBookPath) Then
'
'            GetAllIniBookPathAfterCreateOrdinalSampleCharacters strBookBaseName, "OrdinalCharactersINITest"
'        End If
'    End With
'
'    Set objSectionToKeyValueDic = GetSectionToKeyValueDicFromThreeColumnsFormattedExcelBookByAdoSQL(strInputBookPath)
'
'    'Set objSectionToKeyValueDic = GetSectionToKeyValueDicFromThreeColumnsFormattedExcelBook(strInputBookPath)
'
'    DebugDic objSectionToKeyValueDic
'End Sub
'
'''--VBA_Code_File--<XlAdoSheetExpander.cls>--
'VERSION 1.0 CLASS
'BEGIN
'  MultiUse = -1  'True
'END
'Attribute VB_Name = "XlAdoSheetExpander"
'Attribute VB_GlobalNameSpace = False
'Attribute VB_Creatable = False
'Attribute VB_PredeclaredId = False
'Attribute VB_Exposed = False
''
''   output SQL result into Excel.Worksheet after connecting Excel.Workbook by ADO OLE DB interface
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on both ADO and Excel
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Thu,  9/Jun/2022    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Interface implementations
''///////////////////////////////////////////////
'Implements IOutputBookPath
'
''///////////////////////////////////////////////
''/// Declarations
''///////////////////////////////////////////////
'Private mobjConnector As XlAdoConnector: Private mitfConnector As IADOConnector
'
'Private mobjAdoRecordsetSheetExpander As AdoRSetSheetExpander
'
'
''///////////////////////////////////////////////
''/// Event Handlers
''///////////////////////////////////////////////
'Private Sub Class_Initialize()
'
'    Set mobjConnector = New XlAdoConnector
'
'    Set mitfConnector = mobjConnector
'
'    Set mobjAdoRecordsetSheetExpander = New AdoRSetSheetExpander
'
'    With mobjAdoRecordsetSheetExpander
'
'        Set .ADOConnectorInterface = mobjConnector
'
'        Set .OutputBookPathInterface = Me
'
'        .DataTableSheetFormattingInterface.FieldTitleInteriorType = ColumnNameInteriorOfExcelSheetAdoSQL
'    End With
'End Sub
'
''''
''''
''''
'Private Sub Class_Terminate()
'
'    mobjAdoRecordsetSheetExpander.CloseCacheOutputBookOfOpenByPresetModeProcess
'
'    Debug.Print "class XlAdoSheetExpander terminate -> " & mobjAdoRecordsetSheetExpander Is Nothing
'
'    Me.CloseConnection
'
'    Set mobjAdoRecordsetSheetExpander = Nothing
'
'    Set mobjConnector = Nothing
'End Sub
'
'
''///////////////////////////////////////////////
''/// Properties - connoted ADO connector
''///////////////////////////////////////////////
'Public Property Get SQLExecutionResult() As SQLResult
'
'    Set SQLExecutionResult = mobjConnector.SQLExecutionResult
'End Property
'
'Public Property Get IsSqlExecutionFailed() As Boolean
'
'    IsSqlExecutionFailed = mobjConnector.IsSqlExecutionFailed
'End Property
'
'Public Property Let AllowToRecordSQLLog(ByVal vblnAllowToRecord As Boolean)
'
'    mobjConnector.AllowToRecordSQLLog = vblnAllowToRecord
'End Property
'Public Property Get AllowToRecordSQLLog() As Boolean
'
'    AllowToRecordSQLLog = mobjConnector.AllowToRecordSQLLog
'End Property
'
'
'Public Property Get CurrentSheet() As Excel.Worksheet
'
'    Set CurrentSheet = mobjAdoRecordsetSheetExpander.IExpandAdoRecordsetOnSheet_CurrentSheet
'End Property
'
'
'Public Property Get CurrentBook() As Excel.Workbook
'
'    If Not Me.CurrentSheet Is Nothing Then
'
'        Set CurrentBook = Me.CurrentSheet.Parent
'    Else
'        Set CurrentBook = Nothing
'    End If
'End Property
'
'
''///////////////////////////////////////////////
''/// Properties
''///////////////////////////////////////////////
''''
''''
''''
'Public Property Get ADOConnectorInterface() As IADOConnector
'
'    Set ADOConnectorInterface = mitfConnector
'End Property
'
''''
''''
''''
'Public Property Get DataTableSheetFormattingInterface() As IDataTableSheetFormatter
'
'    Set DataTableSheetFormattingInterface = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface
'End Property
'
'
''**---------------------------------------------
''** connoted DataTableSheetFormatter
''**---------------------------------------------
'Public Property Get TopLeftRowIndex() As Long
'
'    TopLeftRowIndex = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.TopLeftRowIndex
'End Property
'Public Property Let TopLeftRowIndex(ByVal vintTopLeftRowIndex As Long)
'
'    mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.TopLeftRowIndex = vintTopLeftRowIndex
'End Property
'
'Public Property Get TopLeftColumnIndex() As Long
'
'    TopLeftColumnIndex = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.TopLeftColumnIndex
'End Property
'Public Property Let TopLeftColumnIndex(ByVal vintTopLeftColumnIndex As Long)
'
'    mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.TopLeftColumnIndex = vintTopLeftColumnIndex
'End Property
'
'
'Public Property Let AllowToShowFieldTitle(ByVal vblnAllowToShowFieldTitle As Boolean)
'
'    mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.AllowToShowFieldTitle = vblnAllowToShowFieldTitle
'End Property
'Public Property Get AllowToShowFieldTitle() As Boolean
'
'    AllowToShowFieldTitle = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.AllowToShowFieldTitle
'End Property
'
'
'
'Public Property Let AllowToSetAutoFilter(ByVal vblnAllowToSetAutoFilter As Boolean)
'
'    mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.AllowToSetAutoFilter = vblnAllowToSetAutoFilter
'End Property
'Public Property Get AllowToSetAutoFilter() As Boolean
'
'    AllowToSetAutoFilter = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.AllowToSetAutoFilter
'End Property
'
''''
'''' Dictionary(Of String(column title), Double(column-width))
''''
'Public Property Set FieldTitleToColumnWidthDic(ByVal vobjFieldTitleToColumnWidthDic As Scripting.Dictionary)
'
'    Set mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.FieldTitleToColumnWidthDic = vobjFieldTitleToColumnWidthDic
'End Property
'Public Property Get FieldTitleToColumnWidthDic() As Scripting.Dictionary
'
'    Set FieldTitleToColumnWidthDic = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.FieldTitleToColumnWidthDic
'End Property
'
''''
'''' Dictionary(Of String(column title order, such as 'Col1', 'Col07'), Double(column-width))
''''
'Public Property Set FieldTitleOrderToColumnWidthDic(ByVal vobjFieldTitleOrderToColumnWidthDic As Scripting.Dictionary)
'
'    Set mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.FieldTitleOrderToColumnWidthDic = vobjFieldTitleOrderToColumnWidthDic
'End Property
'Public Property Get FieldTitleOrderToColumnWidthDic() As Scripting.Dictionary
'
'    Set FieldTitleOrderToColumnWidthDic = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.FieldTitleOrderToColumnWidthDic
'End Property
'
'
'
''''
'''' When it is true, then ConvertDataForColumns procedure is used.
''''
'Public Property Get AllowToConvertDataForSpecifiedColumns() As Boolean
'
'    AllowToConvertDataForSpecifiedColumns = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.AllowToConvertDataForSpecifiedColumns
'End Property
'Public Property Let AllowToConvertDataForSpecifiedColumns(ByVal vblnAllowToConvertDataForSpecifiedColumns As Boolean)
'
'    mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.AllowToConvertDataForSpecifiedColumns = vblnAllowToConvertDataForSpecifiedColumns
'End Property
''''
'''' Dictionary(Of String(column title), ColumnDataConvertType(Enum definition))
''''
'Public Property Get FieldTitlesToColumnDataConvertTypeDic() As Scripting.Dictionary
'
'    Set FieldTitlesToColumnDataConvertTypeDic = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.FieldTitlesToColumnDataConvertTypeDic
'End Property
'Public Property Set FieldTitlesToColumnDataConvertTypeDic(ByVal vobjFieldTitlesToColumnDataConvertTypeDic As Scripting.Dictionary)
'
'    Set mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.FieldTitlesToColumnDataConvertTypeDic = vobjFieldTitlesToColumnDataConvertTypeDic
'End Property
'
''''
'''' ColumnsNumberFormatLocalParam
''''
'Public Property Set ColumnsNumberFormatLocal(ByVal vobjColumnsNumberFormatLocalParam As ColumnsNumberFormatLocalParam)
'
'    Set mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.ColumnsNumberFormatLocal = vobjColumnsNumberFormatLocalParam
'End Property
'Public Property Get ColumnsNumberFormatLocal() As ColumnsNumberFormatLocalParam
'
'    Set ColumnsNumberFormatLocal = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.ColumnsNumberFormatLocal
'End Property
'
''''
'''' set format-condition for each column
''''
'Public Property Set ColumnsFormatCondition(ByVal vobjColumnsFormatConditionParam As ColumnsFormatConditionParam)
'
'    Set mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.ColumnsFormatCondition = vobjColumnsFormatConditionParam
'End Property
'Public Property Get ColumnsFormatCondition() As ColumnsFormatConditionParam
'
'    Set ColumnsFormatCondition = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.ColumnsFormatCondition
'End Property
'
''''
'''' merge cells when the same values continue
''''
'Public Property Let AllowToMergeCellsByContinuousSameValues(ByVal vblnAllowToMergeCellsByContinuousSameValues As Boolean)
'
'    mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.AllowToMergeCellsByContinuousSameValues = vblnAllowToMergeCellsByContinuousSameValues
'End Property
'Public Property Get AllowToMergeCellsByContinuousSameValues() As Boolean
'
'    AllowToMergeCellsByContinuousSameValues = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.AllowToMergeCellsByContinuousSameValues
'End Property
'Public Property Let AllowToSetVerticalTopAlignmentTopOfMergeCells(ByVal vblnAllowToSetVerticalTopAlignmentTopOfMergeCells As Boolean)
'
'    mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.AllowToSetVerticalTopAlignmentTopOfMergeCells = vblnAllowToSetVerticalTopAlignmentTopOfMergeCells
'End Property
'Public Property Get AllowToSetVerticalTopAlignmentTopOfMergeCells() As Boolean
'
'    AllowToSetVerticalTopAlignmentTopOfMergeCells = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.AllowToSetVerticalTopAlignmentTopOfMergeCells
'End Property
'
'Public Property Set FieldTitlesToMergeCells(ByVal vobjFieldTitlesToMergeCells As Collection)
'
'    Set mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.FieldTitlesToMergeCells = vobjFieldTitlesToMergeCells
'End Property
'Public Property Get FieldTitlesToMergeCells()
'
'    Set FieldTitlesToMergeCells = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.FieldTitlesToMergeCells
'End Property
'
'Public Property Let FieldTitleInteriorType(ByVal venmFieldTitleInteriorType As FieldTitleInterior)
'
'    mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.FieldTitleInteriorType = venmFieldTitleInteriorType
'End Property
'Public Property Get FieldTitleInteriorType() As FieldTitleInterior
'
'    FieldTitleInteriorType = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.FieldTitleInteriorType
'End Property
'
'Public Property Let RecordBordersType(ByVal venmRecordCellsBordersType As RecordCellsBorders)
'
'    mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.RecordBordersType = venmRecordCellsBordersType
'End Property
'Public Property Get RecordBordersType() As RecordCellsBorders
'
'    RecordBordersType = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.RecordBordersType
'End Property
'
''///////////////////////////////////////////////
''/// Properties - connoted ADOConnectionSetting
''///////////////////////////////////////////////
'Public Property Get ConnectSetting() As ADOConnectionSetting
'
'    Set ConnectSetting = mobjConnector.ConnectSetting
'End Property
'
''''
'''' unit is second. [s]
''''
'Public Property Let CommandTimeout(ByVal vintTimeout As Long)
'
'    mobjConnector.CommandTimeout = vintTimeout
'End Property
'Public Property Get CommandTimeout() As Long
'
'    CommandTimeout = mobjConnector.CommandTimeout
'End Property
'
'
''///////////////////////////////////////////////
''/// Properties - Excel-book connection by ACCESS OLE-DB provider
''///////////////////////////////////////////////
'Public Property Get ConnectingFilePath() As String
'
'    ConnectingFilePath = mobjConnector.ConnectingFilePath
'End Property
'
''''
'''' when the load Excel sheet doesn't have the field header title text, this is False
''''
'Public Property Get HeaderFieldTitleExists() As Boolean
'
'    HeaderFieldTitleExists = mobjConnector.HeaderFieldTitleExists
'End Property
'
'Public Property Get BookReadOnly() As Boolean
'
'    BookReadOnly = mobjConnector.BookReadOnly
'End Property
'
'Public Property Get IMEX() As IMEXMode
'
'    IMEX = mobjConnector.IMEX
'End Property
'
''///////////////////////////////////////////////
''/// Implemented operations
''///////////////////////////////////////////////
''''
''''
''''
'Private Function IOutputBookPath_GetPreservedOutputBookPath() As String
'
'    IOutputBookPath_GetPreservedOutputBookPath = GetXlConnectingPrearrangedLogBookPath()
'End Function
'
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''**---------------------------------------------
''** set ADO connection string all parameters directly
''**---------------------------------------------
''''
'''' set input Excel book path
''''
'Public Sub SetInputExcelBookPath(ByVal vstrBookPath As String, _
'        Optional venmIMEXMode As IMEXMode = IMEXMode.NoUseIMEXMode, _
'        Optional vblnHeaderFieldTitleExists As Boolean = True, _
'        Optional vblnBookReadOnly As Boolean = False, _
'        Optional ByVal vstrLockBookPassword As String = "", _
'        Optional vblnIsConnectedToOffice95 As Boolean = False)
'
'    mobjConnector.SetInputExcelBookPath vstrBookPath, venmIMEXMode, vblnHeaderFieldTitleExists, vblnBookReadOnly, vstrLockBookPassword, vblnIsConnectedToOffice95
'End Sub
'
''**---------------------------------------------
''** set ADO connection string parameters by a User-form interface
''**---------------------------------------------
''''
''''
''''
'''' <Argument>vstrSettingKeyName: Input</Argument>
'''' <Argument>vstrBookPath: Input</Argument>
'''' <Argument>venmIMEXMode: Input</Argument>
'''' <Argument>vblnHeaderFieldTitleExists: Input</Argument>
'''' <Argument>vblnBookReadOnly: Input</Argument>
'''' <Argument>venmAccessOLEDBProviderType: Input</Argument>
'''' <Argument>vstrBookLockPassword: Input</Argument>
'''' <Return>Boolean</Return>
'Public Function IsAdoConnectionContinuedAfterAccessOleDbConnectionParametersByForm(ByVal vstrSettingKeyName As String, _
'        Optional ByVal vstrBookPath As String = "", _
'        Optional ByVal venmIMEXMode As IMEXMode = IMEXMode.NoUseIMEXMode, _
'        Optional ByVal vblnHeaderFieldTitleExists As Boolean = True, _
'        Optional ByVal vblnBookReadOnly As Boolean = False, _
'        Optional ByVal venmAccessOLEDBProviderType As AccessOLEDBProviderType = AccessOLEDBProviderType.AceOLEDB_12P0, _
'        Optional ByVal vstrBookLockPassword As String = "")
'
'
'    IsAdoConnectionContinuedAfterAccessOleDbConnectionParametersByForm = mobjConnector.IsAdoConnectionContinuedAfterAccessOleDbConnectionParametersByForm(vstrSettingKeyName, _
'            vstrBookPath, _
'            venmIMEXMode, _
'            vblnHeaderFieldTitleExists, _
'            vblnBookReadOnly, _
'            venmAccessOLEDBProviderType, _
'            vstrBookLockPassword)
'End Function
'
'
'
''''
'''' Close ADO connection
''''
'Public Sub CloseConnection()
'
'    mobjConnector.CloseConnection
'End Sub
'
''''
''''
''''
'Public Sub CloseCacheOutputBookOfOpenByPresetModeProcess()
'
'    mobjAdoRecordsetSheetExpander.CloseCacheOutputBookOfOpenByPresetModeProcess
'End Sub
'
''''
''''
''''
'Public Sub CloseAll()
'
'    CloseConnection
'
'    CloseCacheOutputBookOfOpenByPresetModeProcess
'End Sub
'
''**---------------------------------------------
''** Use SQL after the ADO connected
''**---------------------------------------------
''''
'''' get recordset object by the Select SQL
''''
'Public Function GetRecordset(ByVal vstrSQL As String) As ADODB.Recordset
'
'    Set GetRecordset = mobjConnector.GetRecordset(vstrSQL)
'End Function
'
''**---------------------------------------------
''** Output Recordset and logs to cells on sheet
''**---------------------------------------------
''''
'''' query SQL and output result to Excel sheet
''''
'Public Sub OutputToSheetFrom(ByVal vstrSQL As String, _
'        ByVal vobjOutputSheet As Excel.Worksheet, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputToSheetFrom vstrSQL, _
'            vobjOutputSheet, _
'            venmExecutedAdoSqlQueryLogPositionType, _
'            venmShowUpAdoErrorOptionFlag, _
'            mfobjGetConnectedBookPathInsertText(), _
'            vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
'''' query SQL and output result to the Existed Excel sheet
''''
'Public Sub OutputInExistedBook(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputInExistedBook vstrSQL, _
'            vstrNewSheetName, _
'            venmExecutedAdoSqlQueryLogPositionType, _
'            venmShowUpAdoErrorOptionFlag, _
'            mfobjGetConnectedBookPathInsertText(), _
'            vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
'Public Sub OutputInAlreadyExistedBook(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputInAlreadyExistedBook vstrSQL, _
'            vstrNewSheetName, _
'            venmExecutedAdoSqlQueryLogPositionType, _
'            venmShowUpAdoErrorOptionFlag, _
'            mfobjGetConnectedBookPathInsertText(), _
'            vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
''''
''''
'Public Sub OutputInExistedBookByUnitTestMode(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputInExistedBookByUnitTestMode vstrSQL, _
'            vstrNewSheetName, _
'            venmExecutedAdoSqlQueryLogPositionType, _
'            venmShowUpAdoErrorOptionFlag, _
'            mfobjGetConnectedBookPathInsertText(), _
'            vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputToSpecifiedBook(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputToSpecifiedBook vstrSQL, _
'            vstrOutputBookPath, _
'            vstrNewSheetName, _
'            venmExecutedAdoSqlQueryLogPositionType, _
'            venmShowUpAdoErrorOptionFlag, _
'            mfobjGetConnectedBookPathInsertText(), _
'            vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
'Public Sub OutputToSpecifiedBookByUnitTestMode(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputToSpecifiedBookByUnitTestMode vstrSQL, _
'            vstrOutputBookPath, _
'            vstrNewSheetName, _
'            venmExecutedAdoSqlQueryLogPositionType, _
'            venmShowUpAdoErrorOptionFlag, _
'            mfobjGetConnectedBookPathInsertText(), _
'            vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
'''' query SQL and output result to the already specified Excel sheet
''''
'Public Sub OutputToAlreadySpecifiedBook(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'    mobjAdoRecordsetSheetExpander.OutputToAlreadySpecifiedBook vstrSQL, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedBookPathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
''''
''''
'Public Sub SetOutputExcelBookPath(ByVal vstrOutputBookPath As String, _
'        Optional ByVal venmOutputRSetExcelBookOpenModeProcessFlag As OutputRSetExcelBookOpenModeProcessFlag = OutputRSetExcelBookOpenModeProcessFlag.OpenOutputBookByUnitTestMode)
'
'
'    mobjAdoRecordsetSheetExpander.SetOutputExcelBookPath vstrOutputBookPath, _
'            venmOutputRSetExcelBookOpenModeProcessFlag
'End Sub
'
''**---------------------------------------------
''** Excecute SQL command (UPDATE, INSERT, DELETE)
''**---------------------------------------------
''''
'''' command SQL and output result-log to Excel sheet
''''
'Public Sub OutputCommandLogToSheetFrom(ByVal vstrSQL As String, _
'        ByVal vobjOutputSqlCommandLogSheet As Excel.Worksheet, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogToSheetFrom vstrSQL, _
'            vobjOutputSqlCommandLogSheet, _
'            venmShowUpAdoErrorOptionFlag, _
'            mfobjGetConnectedBookPathInsertText(), _
'            vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputCommandLogToSpecifiedBook(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogToSpecifiedBook vstrSQL, vstrOutputBookPath, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedBookPathInsertText(), vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputCommandLogToAlreadySpecifiedBook(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogToAlreadySpecifiedBook vstrSQL, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedBookPathInsertText(), vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputCommandLogInAlreadyExistedBook(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogInAlreadyExistedBook vstrSQL, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedBookPathInsertText(), vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputCommandLogToSpecifiedBookByUnitTestMode(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogToSpecifiedBookByUnitTestMode vstrSQL, vstrOutputBookPath, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedBookPathInsertText(), vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
'''' query SQL and output result to the prepared Excel sheet
''''
'Public Sub OutputCommandLogInExistedBookByUnitTestMode(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogInExistedBookByUnitTestMode vstrSQL, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedBookPathInsertText(), vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
''''
''''
''''
'Private Function mfobjGetConnectedBookPathInsertText() As Collection
'
'    Dim objInsertTexts As Collection
'
'    Set objInsertTexts = New Collection
'
'    With mobjConnector
'
'        objInsertTexts.Add "<File Name> " & GetFileNameFromPathByVbaDir(.ConnectingFilePath) & ", <Directory> " & GetParentDirectoryPathByVbaDir(.ConnectingFilePath)
'    End With
'
'    Set mfobjGetConnectedBookPathInsertText = objInsertTexts
'End Function
'
'
'''--VBA_Code_File--<UTfAdoConnectExcelSheet.bas>--
'Attribute VB_Name = "UTfAdoConnectExcelSheet"
''
''   Sanity tests for USetAdoOleDbConStrForXlBook.frm
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on both ADO and Excel
''       Dependent on UTfDataTableSecurity.bas
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Tue, 25/Jul/2023    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
'
''///////////////////////////////////////////////
''/// Constants
''///////////////////////////////////////////////
'
'Private Const mstrSheetName As String = "TestSheet"
'
'
''///////////////////////////////////////////////
''/// Sanity tests
''///////////////////////////////////////////////
''**---------------------------------------------
''** XlAdoSheetExpander tests, which need to open the password locked Excel book by Microsoft Excel specification
''**---------------------------------------------
''''
'''' Use a User-form
''''
'Private Sub msubSanityTestToXlAdoSheetExpanderWithForm()
'
'    Dim strBookPath As String, strSQL As String
'
'    strBookPath = mfstrPrepareTestBookWithLockPassword()
'
'    With New XlAdoConnector
'
'        ' input the password by a User-form
'
'        If .IsAdoConnectionContinuedAfterAccessOleDbConnectionParametersByForm("TestAccOleDbToExcelWithPasswordKey", strBookPath) Then
'
'            strSQL = "SELECT * FROM [" & mstrSheetName & "$]"
'
'            DebugCol GetTableCollectionFromRSet(.GetRecordset(strSQL))
'        End If
'    End With
'End Sub
'
'Private Sub msubSanityTestToXlAdoSheetExpander()
'
'    Dim strBookPath As String, strSQL As String
'
'    strBookPath = mfstrPrepareTestBookWithLockPassword()
'
'    With New XlAdoSheetExpander
'
'        ' In the case, input the password directly
'
'        .SetInputExcelBookPath strBookPath, vstrLockBookPassword:="1234"
'
'        strSQL = "SELECT * FROM [" & mstrSheetName & "$]"
'
'        .OutputInExistedBookByUnitTestMode strSQL, "TestSQLResults", SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable
'    End With
'End Sub
'
''**---------------------------------------------
''** USetAdoOleDbConStrForXlBook form tests
''**---------------------------------------------
''''
'''' delete testing SettingKeyName from the registory
''''
'Private Sub msubSanityTestToDeleteTestingSettingKeyNamesForConnectingToAccessOleDbWithExcelSheet()
'
'    DeleteWinRegKeyValuesOfAdoConnectionStringInfo "TestAccOleDbToExcelKey"
'
'    DeleteWinRegKeyValuesOfAdoConnectionStringInfo "TestAccOleDbToExcelWithPasswordKey"
'End Sub
'
''''
'''' Excel book, which is not locked by any password.
''''
'Private Sub msubSanityTestToSetOdbcConnectionParametersByAccessOleDbToExcelSheetForm()
'
'    Dim blnIsRequestedToCancelAdoConnection As Boolean, objADOConStrOfAceOledbXl As ADOConStrOfAceOleDbXl
'    Dim strSettingKeyName As String, strBookPath As String
'
'
'    strSettingKeyName = "TestAccOleDbToExcelKey"
'
'    Set objADOConStrOfAceOledbXl = New ADOConStrOfAceOleDbXl
'
'    strBookPath = mfstrPrepareTestBook()
'
'    SetOleDbConnectionParametersByAccessOleDbToExcelSheetForm blnIsRequestedToCancelAdoConnection, objADOConStrOfAceOledbXl, strSettingKeyName, strBookPath
'End Sub
'
''''
'''' Connect a password locked Excel book
''''
'Private Sub msubSanityTestToSetOdbcConnectionParametersByAccessOleDbToExcelSheetFormWithPasswordLock()
'
'    Dim blnIsRequestedToCancelAdoConnection As Boolean, objADOConStrOfAceOledbXl As ADOConStrOfAceOleDbXl
'    Dim strSettingKeyName As String, strBookPath As String
'
'
'    strSettingKeyName = "TestAccOleDbToExcelWithPasswordKey"
'
'    Set objADOConStrOfAceOledbXl = New ADOConStrOfAceOleDbXl
'
'    strBookPath = mfstrPrepareTestBookWithLockPassword()
'
'    SetOleDbConnectionParametersByAccessOleDbToExcelSheetForm blnIsRequestedToCancelAdoConnection, objADOConStrOfAceOledbXl, strSettingKeyName, strBookPath, vstrBookLockPassword:="1234"
'End Sub
'
''''
'''' Connect a password locked Excel book
''''
'Private Sub msubSanityTestToSetOdbcConnectionParametersByAccessOleDbToExcelSheetFormWithPasswordLockAndOpen()
'
'    Dim blnIsRequestedToCancelAdoConnection As Boolean, objADOConStrOfAceOledbXl As ADOConStrOfAceOleDbXl
'    Dim strSettingKeyName As String, strBookPath As String
'
'
'    strSettingKeyName = "TestAccOleDbToExcelWithPasswordKey"
'
'    Set objADOConStrOfAceOledbXl = New ADOConStrOfAceOleDbXl
'
'    strBookPath = mfstrPrepareTestBookWithLockPassword()
'
'    SetOleDbConnectionParametersByAccessOleDbToExcelSheetForm blnIsRequestedToCancelAdoConnection, objADOConStrOfAceOledbXl, strSettingKeyName, strBookPath
'End Sub
'
''''
'''' Connect a password locked Excel book using a User-form
''''
'Private Sub msubSanityTestToSetOdbcConnectionParametersByAccessOleDbToExcelSheetFormWithPasswordLockAndOpenFormIfItNeeded()
'
'    Dim blnIsRequestedToCancelAdoConnection As Boolean, objADOConStrOfAceOledbXl As ADOConStrOfAceOleDbXl
'    Dim strSettingKeyName As String, strBookPath As String
'
'
'    strSettingKeyName = "TestAccOleDbToExcelKey"
'
'    Set objADOConStrOfAceOledbXl = New ADOConStrOfAceOleDbXl
'
'    strBookPath = mfstrPrepareTestBookWithLockPassword()
'
'    SetOleDbConnectionParametersByAccessOleDbToExcelSheetForm blnIsRequestedToCancelAdoConnection, objADOConStrOfAceOledbXl, strSettingKeyName, strBookPath
'End Sub
'
'
'
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
''''
''''
''''
'Private Function mfstrPrepareTestBookWithLockPassword() As String
'
'    Dim strPath As String
'
'    strPath = mfstrGetTemporaryAdoConnectingTestExcelBookDir() & "\AdoConectionUITestWithLockPassword.xlsx"
'
'    PrepareTestBookWithLockingPasswordAndCreating strPath, mstrSheetName, "1234"
'
'    mfstrPrepareTestBookWithLockPassword = strPath
'End Function
'
''''
''''
''''
'Private Function mfstrPrepareTestBook() As String
'
'    Dim strPath As String
'
'    strPath = mfstrGetTemporaryAdoConnectingTestExcelBookDir() & "\AdoConectionUITest.xlsx"
'
'    PrepareTestBookWithLockingPasswordAndCreating strPath, mstrSheetName, ""
'
'    mfstrPrepareTestBook = strPath
'End Function
'
'
''''
''''
''''
'Private Function mfstrGetTemporaryAdoConnectingTestExcelBookDir() As String
'
'    Dim strDir As String
'
'    strDir = GetParentDirectoryPathOfCurrentOfficeFile() & "\TemporaryBooks\AdoConnectionUITests"
'
'    'strDir = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\AdoConnectionUITests"
'
'    ForceToCreateDirectory strDir
'
'    mfstrGetTemporaryAdoConnectingTestExcelBookDir = strDir
'End Function
'
'''--VBA_Code_File--<SqlUTfXlSheetExpander.bas>--
'Attribute VB_Name = "SqlUTfXlSheetExpander"
''
''   Sanity test to output SQL result into an output Excel sheet for a source Excel file sheets in a directory using ADO OLE DB
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on both ADO and Excel
''       Dependent on Microsoft Office Access Data Base Engine OLE DB Provider,
''       which should have been the installed when the Microsoft Office Excel software has been installed.
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Fri, 20/Jan/2023    Kalmclaeyd Tarclanus    Added the multi-language caption function and tested
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Constants
''///////////////////////////////////////////////
'Private Const mstrOutputBaseName As String = "SQLResultsOfUTfXlAdoSheetExpander"
'
'
''///////////////////////////////////////////////
''/// Sanity tests
''///////////////////////////////////////////////
''**---------------------------------------------
''** XlAdoSheetExpander
''**---------------------------------------------
''''
'''' Create an in-line table from the only SELECT sql inside
''''
'Private Sub msubSanityTestToConnectByVirtualTableXlConnector()
'
'    Dim strSQL As String
'
'    With New XlAdoSheetExpander
'
'        .SetInputExcelBookPath GetXlConnectingPrearrangedLogBookPath()
'
'        .AllowToRecordSQLLog = True
'
'        'strSQL = "SELECT 'ABC', 'DEF' UNION SELECT 'GHI', 'JKL'" ' Error occurs
'
'        'strSQL = "SELECT 1"
'
'        'strSQL = "SELECT 'ABC', 'DEF'"
'
'        strSQL = "SELECT 'ABC' AS A_Column, 'DEF' AS B_Column"
'
'        .OutputInExistedBookByUnitTestMode strSQL, "Test_ADO_VirtualTable"
'
'
'        .CloseCacheOutputBookOfOpenByPresetModeProcess
'    End With
'End Sub
'
''''
'''' Create an in-line table from the only SELECT sql inside
''''
'Private Sub msubSanityTestToConnectForExpandingOperations()
'
'    Dim objSheet As Excel.Worksheet, strOutputBookPath As String, objBook As Excel.Workbook
'    Dim strSQL As String
'
'    ForceToCreateDirectory GetTemporaryDevelopmentVBARootDir()
'
'    strOutputBookPath = GetTemporaryDevelopmentVBARootDir() & "\SanityTestToOutputOperationsOfAdoRecordsetSheetExpander.xlsx"
'
'    Set objBook = GetWorkbookAndPrepareForUnitTest(strOutputBookPath)
'
'    With New XlAdoSheetExpander
'
'        .SetInputExcelBookPath GetXlConnectingPrearrangedLogBookPath()
'
'        .AllowToRecordSQLLog = True
'
'
'
'        strSQL = "SELECT 'ABC' AS A_Column, 'DEF' AS B_Column"
'
'
'        .OutputInExistedBookByUnitTestMode strSQL, "Test_ADO_VirtualTable"
'
'        .OutputInAlreadyExistedBook strSQL, "VirtualTable02"
'
'        Set objSheet = GetNewWorksheetAfterAllExistedSheetsFromBook(objBook)
'
'        objSheet.Name = "VirtualTable03"
'
'        .OutputToSheetFrom strSQL, objSheet
'
'        .OutputInExistedBook strSQL, "VirtualTable04"
'
'        .OutputToSpecifiedBookByUnitTestMode strSQL, strOutputBookPath, "VirtualTable05"
'
'        .OutputToSpecifiedBook strSQL, strOutputBookPath, "VirtualTable06"
'
'
'        .CloseCacheOutputBookOfOpenByPresetModeProcess
'
'        DeleteDummySheetWhenItExists .CurrentSheet
'    End With
'End Sub
'
''''
'''' SQL logs will be appended on a specified sheet.
''''
'Private Sub msubSanityTestToConnectByVirtualTableXlConnectorForSQLLogAppending()
'
'    Dim strSQL As String
'
'    With New XlAdoSheetExpander
'
'        .SetInputExcelBookPath GetXlConnectingPrearrangedLogBookPath()
'
'        strSQL = "SELECT 'ABC' AS A_Column, 'DEF' AS B_Column"
'
'        .OutputInExistedBookByUnitTestMode strSQL, "Test_ADO_VirtualTable", AppendingSqlQueryLogsInSheetCellsOnIndependentSheet
'
'        strSQL = "SELECT 'EFG' AS C_Column, 'HIJ' AS D_Column"
'
'        .OutputInExistedBookByUnitTestMode strSQL, "Test_ADO_VirtualTable02", AppendingSqlQueryLogsInSheetCellsOnIndependentSheet
'
'        strSQL = "SELECT 'KLM' AS E_Column, 'OPQ' AS F_Column"
'
'        .OutputInExistedBookByUnitTestMode strSQL, "Test_ADO_VirtualTable03", AppendingSqlQueryLogsInSheetCellsOnIndependentSheet
'
'
'        DeleteDummySheetWhenItExists .CurrentBook
'
'        .CloseCacheOutputBookOfOpenByPresetModeProcess
'    End With
'End Sub
'
''''
'''' Confirm the auto shape log of SQL select
''''
'Private Sub msubSanityTestToConnectByVirtualTableXlConnectorWithAutoShapeLog()
'
'    Dim strSQL As String
'
'    With New XlAdoSheetExpander
'
'        .SetInputExcelBookPath GetXlConnectingPrearrangedLogBookPath()
'
'        strSQL = "SELECT 'ABC' AS A_Column, 'DEF' AS B_Column"
'
'        .OutputInExistedBookByUnitTestMode strSQL, "Test_ADO_VirtualTable", SqlQueryLogsByAutoShapesOnSheetWithOutputtingRecordsetDataTable
'
'        .CloseCacheOutputBookOfOpenByPresetModeProcess
'    End With
'End Sub
'
''''
'''' show UErrorADOSQLForm tests
''''
'Private Sub msubSanityTestToShowErrorTrapFormByIllegalSQL()
'
'    Dim strSQL As String, objRSet As ADODB.Recordset
'
'    With New XlAdoSheetExpander
'
'        .SetInputExcelBookPath GetXlConnectingPrearrangedLogBookPath()
'
'        strSQL = "(SELECT 'ABC', 'DEF') UNION ALL (SELECT 'GHI', 'JKL')" ' This causes a error. It seems that UNION ALL is not applicable....
'
'        'strSQL = "(SELECT 'ABC' AS A_Column, 'DEF' AS B_Column) UNION ALL (SELECT 'GHI', 'JKL')" ' Error occurs
'
'        .OutputInExistedBookByUnitTestMode strSQL, "Test_ADO_VirtualTable"
'    End With
'End Sub
'
'
'
''''
''''
''''
'Private Sub msubSanityTest01ToConnectExcelBookTestAfterCreateBook()
'
'    GetOutputExcelBookPathOfConnectingBookTestAfterCreateBook BasicSampleDT, 100, False
'End Sub
'
''''
''''
''''
'Private Sub msubSanityTest02ToConnectExcelBookTestAfterCreateBook()
'
'    GetOutputExcelBookPathOfConnectingBookTestAfterCreateBook IncludeFictionalProductionNumberSampleDT, 1000, True
'End Sub
'
'
''''
''''
''''
'Private Sub msubSanityTestToSQLJoinWithConnectingExcelBookAfterItIsCreated()
'
'    Dim strInputBookPath As String, strSheetName01 As String, strSheetName02 As String, strSQL As String
'
'    strInputBookPath = GetOutputBookPathAfterCreateALeastTestSheetSet()
'
'    With New XlAdoSheetExpander
'
'        .SetInputExcelBookPath strInputBookPath
'
'        strSheetName01 = "[" & GetSheetNameOfPreparedSampleDataTableExcelBookForTest(IncludeFictionalProductionNumberSampleDT) & "$]"
'
'        strSheetName02 = "[" & GetSheetNameOfPreparedSampleDataTableExcelBookForTest(FishTypeSampleMasterDT) & "$]"
'
'        strSQL = "SELECT * FROM " & strSheetName01
'
'        .OutputInExistedBookByUnitTestMode strSQL, "CreatedTable01"
'
'        strSQL = "SELECT * FROM " & strSheetName02
'
'        .OutputInExistedBookByUnitTestMode strSQL, "SampleMasterTable02"
'
'        strSQL = "SELECT A.RowNumber, A.RealNumber, B.FishTypeJpn, B.RandAlpha FROM " & strSheetName01 & " AS A LEFT JOIN " & strSheetName02 & " AS B ON A.FishType = B.FishTypeEng"
'
'        .OutputInExistedBookByUnitTestMode strSQL, "SQLResult"
'    End With
'
'End Sub
'
'
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''''
'''' This is also referenced from UTfReadFromShapeOnXlSheet.bas
''''
'Public Function GetOutputExcelBookPathOfConnectingBookTestAfterCreateBook(ByVal venmUnitTestPreparedSampleDataTable As UnitTestPreparedSampleDataTable, _
'        Optional ByVal vintCountOfRows As Long = 100, Optional _
'        ByVal vblnIncludesSQLLogToSheet As Boolean = True) As String
'
'    Dim strInputBookPath As String, strSQL As String, strLog As String, strSheetName As String
'
'    Dim strOutputBookPath As String
'
'    strInputBookPath = GetExcelBookPathAfterCheckFileExistenceOrCreateWhenItDoesntExist(strSheetName, venmUnitTestPreparedSampleDataTable, vintCountOfRows)
'
'    With New XlAdoSheetExpander
'
'        .SetInputExcelBookPath strInputBookPath
'
'        With .DataTableSheetFormattingInterface
'
'            Set .FieldTitleOrderToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("Col1,12,Col2,15")
'
'            Set .FieldTitleToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("FlowerTypeSample,19,RandomInteger,16")
'
'            .RecordBordersType = RecordCellsGrayAll
'        End With
'
'        strSQL = "SELECT * FROM [" & strSheetName & "$]"
'
'        .OutputInExistedBookByUnitTestMode strSQL, "Load_" & strSheetName, vblnIncludesSQLLogToSheet
'
'
'        strLog = GetLogTextOfPreparedSampleDataTableBaseName(venmUnitTestPreparedSampleDataTable)
'
'
'        AddAutoShapeGeneralMessage .CurrentSheet, strLog
'
'        strOutputBookPath = .CurrentBook.FullName
'
'        DeleteDummySheetWhenItExists .CurrentBook
'    End With
'
'    GetOutputExcelBookPathOfConnectingBookTestAfterCreateBook = strOutputBookPath
'End Function
'
'
'
''''
'''' XlAdoSheetExpander unit test
''''
'Public Sub SanityTestAboutXlSheetExpander()
'
'    Dim strInputPath As String, strOutputPath As String, strSheetName As String
'
'    Dim strSQL As String, strSQLTableName As String, enmUnitTestPreparedSampleDataTable As UnitTestPreparedSampleDataTable
'
'
'    enmUnitTestPreparedSampleDataTable = BasicSampleDT
'
'
'    strInputPath = GetExcelBookPathAfterCheckFileExistenceOrCreateWhenItDoesntExist(strSheetName, enmUnitTestPreparedSampleDataTable, 50)
'
'
'    strOutputPath = GetCurrentBookOutputDir() & mstrOutputBaseName & ".xlsx"
'
'    With New XlAdoSheetExpander
'
'        .SetInputExcelBookPath strInputPath
'
'        .AllowToRecordSQLLog = True
'
'        Set .FieldTitleToColumnWidthDic = GetPreparedSampleDataTableFieldTitleToColumnWidthDicForSanityTest(enmUnitTestPreparedSampleDataTable)
'
'
'        strSQLTableName = "[" & strSheetName & "$]"
'
'        strSQL = "SELECT * FROM " & strSQLTableName
'
'        .OutputToSpecifiedBookByUnitTestMode strSQL, strOutputPath, "QueryTest", True
'    End With
'End Sub
'
'
'
'
'
'''--VBA_Code_File--<SqlUTfXlSheetExpanderByPassword.bas>--
'Attribute VB_Name = "SqlUTfXlSheetExpanderByPassword"
''
''   Sanity test to connect Excel sheet with a locked password for opening
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on both Excel and ADO
''       Dependent on UTfDataTableSecurity.bas, UTfDecorationSetterToXlSheet.bas
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Thu, 29/Jun/2023    Kalmclaeyd Tarclanus    Initial creation
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Constants
''///////////////////////////////////////////////
'
'Private Const mstrSheetName As String = "TestSheet"
'
'
''**---------------------------------------------
''** XlAdoConnector tests for password locked books
''**---------------------------------------------
''''
''''
''''
'Private Sub msubSanityTestToXlAdoConnectorForPasswordLockedBook()
'
'    Dim strBookPath As String, strSQL As String
'
'    strBookPath = GetBookSecurityTemporaryGeneratingDirectoryPath() & "\SamplePasswordLockedBook.xlsx"
'
'    PrepareTestBookWithLockingPasswordAndCreating strBookPath, mstrSheetName, "1234"
'
'    With New XlAdoConnector
'
'        .SetInputExcelBookPath strBookPath, vstrLockBookPassword:="1234"
'
'        strSQL = "SELECT * FROM [" & mstrSheetName & "$]"
'
'        DebugCol GetTableCollectionFromRSet(.GetRecordset(strSQL))
'    End With
'End Sub
'
''''
''''
''''
'Private Sub msubSanityTestToXlAdoExpanderForPasswordLockedBook()
'
'    Dim strBookPath As String, strSQL As String
'
'    strBookPath = GetBookSecurityTemporaryGeneratingDirectoryPath() & "\SamplePasswordLockedBook.xlsx"
'
'    PrepareTestBookWithLockingPasswordAndCreating strBookPath, mstrSheetName, "1234"
'
'    With New XlAdoSheetExpander
'
'        .SetInputExcelBookPath strBookPath, vstrLockBookPassword:="1234"
'
'        strSQL = "SELECT * FROM [" & mstrSheetName & "$]"
'
'        .OutputInExistedBookByUnitTestMode strSQL, "ATest", SqlQueryLogsByAutoShapesOnSheetWithOutputtingRecordsetDataTable
'
'        DeleteDummySheetWhenItExists .CurrentBook
'
'        .CloseAll
'    End With
'End Sub
'
'
''**---------------------------------------------
''** ADO connection tests
''**---------------------------------------------
''''
''''
''''
'Private Sub msubSanityTestToAdoConnectionToNoPasswordBook()
'
'    Dim strBookPath As String, objBook As Excel.Workbook
'    Dim objADOConStrOfAceOledbXl As ADOConStrOfAceOleDbXl, objConnection As ADODB.Connection
'
'
'    strBookPath = GetBookSecurityTemporaryGeneratingDirectoryPath() & "\SampleBookWithNoLocked.xlsx"
'
'    PrepareTestBookWithLockingPasswordAndCreating strBookPath, mstrSheetName, ""
'
'    Set objADOConStrOfAceOledbXl = New ADOConStrOfAceOleDbXl
'
'    With objADOConStrOfAceOledbXl
'
'        .SetExcelBookConnection strBookPath, NoUseIMEXMode
'    End With
'
'    Set objConnection = New ADODB.Connection
'
'    With objConnection
'
'        .ConnectionString = objADOConStrOfAceOledbXl.GetConnectionString
'
'        On Error Resume Next
'
'        .Open
'
'        If Err.Number = 0 Then
'
'            Debug.Print "Expected result to be connected"
'
'            .Close
'        End If
'
'        On Error GoTo 0
'    End With
'
'End Sub
'
''''
''''
''''
'Private Sub msubSanityTestToAdoConnectionToPasswordLockedBook()
'
'    Dim strBookPath As String, objBook As Excel.Workbook
'    Dim objADOConStrOfAceOledbXl As ADOConStrOfAceOleDbXl, objConnection As ADODB.Connection
'
'
'    strBookPath = GetBookSecurityTemporaryGeneratingDirectoryPath() & "\SamplePasswordLockedBook.xlsx"
'
'    PrepareTestBookWithLockingPasswordAndCreating strBookPath, mstrSheetName, "1234"
'
'    Set objADOConStrOfAceOledbXl = New ADOConStrOfAceOleDbXl
'
'    With objADOConStrOfAceOledbXl
'
'        .SetExcelBookConnection strBookPath, NoUseIMEXMode
'    End With
'
'    Set objConnection = New ADODB.Connection
'
'    With objConnection
'
'        .ConnectionString = objADOConStrOfAceOledbXl.GetConnectionString
'
'        On Error Resume Next
'
'        .Open
'
'        If Err.Number <> 0 Then
'
'            Debug.Print "Expected result to be failed connecting"
'        Else
'            .Close
'        End If
'
'        On Error GoTo 0
'    End With
'End Sub
'
'
''''
''''
''''
'Private Sub msubSanityTestToAdoConnectionToPasswordLockedBookWithOpening()
'
'    Dim strBookPath As String, objBook As Excel.Workbook
'    Dim objADOConStrOfAceOledbXl As ADOConStrOfAceOleDbXl, objConnection As ADODB.Connection
'
'
'    strBookPath = GetBookSecurityTemporaryGeneratingDirectoryPath() & "\SamplePasswordLockedBook.xlsx"
'
'    PrepareTestBookWithLockingPasswordAndCreating strBookPath, mstrSheetName, "1234"
'
'    Set objBook = GetWorkbook(strBookPath, False, vstrPassword:="1234")
'
'    Set objADOConStrOfAceOledbXl = New ADOConStrOfAceOleDbXl
'
'    With objADOConStrOfAceOledbXl
'
'        .SetExcelBookConnection strBookPath, NoUseIMEXMode
'    End With
'
'    Set objConnection = New ADODB.Connection
'
'    With objConnection
'
'        .ConnectionString = objADOConStrOfAceOledbXl.GetConnectionString
'
'        On Error Resume Next
'
'        .Open
'
'        If Err.Number = 0 Then
'
'            Debug.Print "Expected result to be connected, when the password locked Book is opened in this Excel"
'
'            .Close
'        End If
'
'        On Error GoTo 0
'    End With
'End Sub
'
'''--VBA_Code_File--<UTfReadFromShapeByAdoXl.bas>--
'Attribute VB_Name = "UTfReadFromShapeByAdoXl"
''
''   Sanity tests for getting key-value dictionary from auto-shape on Excel sheet
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on Excel
''       Dependent on ReadFromShapeOnXlSheet.bas
''       Dependent on both UTfReadFromShapeOnXlSheet.bas and SqlUTfXlSheetExpander.bas
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2024 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Sun, 18/Aug/2024    Kalmclaeyd Tarclanus    Separated from UTfReadFromShapeOnXlSheet.bas
''
'
'Option Explicit
'
'
''///////////////////////////////////////////////
''/// Sanity tests
''///////////////////////////////////////////////
''**---------------------------------------------
''** get Dictionary test
''**---------------------------------------------
''''
'''' About XlAdoSheetExpander
''''
'Private Sub msubSanityTestToGetKeyValueDicByReadingFromExcelAdoExpanderAutoShapes()
'
'    DebugDic mfobjGetKeyValueDicFromReadingFromExcelAdoExpanderAutoShapes()
'End Sub
'
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
''**---------------------------------------------
''** Tools for getting Dictinary objects
''**---------------------------------------------
''''
''''
''''
'Private Function mfobjGetShapeNameToKeyValueDicFromReadingFromExcelAdoExpanderAutoShapes() As Scripting.Dictionary
'
'    Set mfobjGetShapeNameToKeyValueDicFromReadingFromExcelAdoExpanderAutoShapes = mfobjGetDicFromReadingFromExcelAdoExpanderAutoShapes(True)
'End Function
'
''''
''''
''''
'Private Function mfobjGetKeyValueDicFromReadingFromExcelAdoExpanderAutoShapes() As Scripting.Dictionary
'
'    Set mfobjGetKeyValueDicFromReadingFromExcelAdoExpanderAutoShapes = mfobjGetDicFromReadingFromExcelAdoExpanderAutoShapes(False)
'End Function
'
'
''''
''''
''''
'Private Function mfobjGetDicFromReadingFromExcelAdoExpanderAutoShapes(Optional ByVal vblnGetShapeNameToKeyValueDic As Boolean = True) As Scripting.Dictionary
'
'    Dim strBookPath As String, objSheet As Excel.Worksheet
'    Dim objDic As Scripting.Dictionary
'
'    strBookPath = GetOutputExcelBookPathOfConnectingBookTestAfterCreateBook(BasicSampleDT, 100, False)
'
'    With GetWorkbook(strBookPath)
'
'        Set objSheet = .Worksheets.Item(1)
'
'        If vblnGetShapeNameToKeyValueDic Then
'
'            Set objDic = GetShapeNameToKeyValueDicFromShapes(objSheet)
'        Else
'            Set objDic = GetKeyValueDicFromShapes(objSheet)
'        End If
'    End With
'
'    Set mfobjGetDicFromReadingFromExcelAdoExpanderAutoShapes = objDic
'End Function
'''--VBA_Code_File--<CsvAdoSheetExpander.cls>--
'VERSION 1.0 CLASS
'BEGIN
'  MultiUse = -1  'True
'END
'Attribute VB_Name = "CsvAdoSheetExpander"
'Attribute VB_GlobalNameSpace = False
'Attribute VB_Creatable = False
'Attribute VB_PredeclaredId = False
'Attribute VB_Exposed = False
''
''   output SQL result into Excel.Worksheet after connecting CSV file by ADO-DB interface
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on both ADO and Excel
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Fri, 10/Jun/2022    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
'
''///////////////////////////////////////////////
''/// Interface implementations
''///////////////////////////////////////////////
'Implements IOutputBookPath
'
''///////////////////////////////////////////////
''/// Declarations
''///////////////////////////////////////////////
'Private mobjConnector As CsvAdoConnector: Private mitfConnector As IADOConnector
'
'Private mobjAdoRecordsetSheetExpander As AdoRSetSheetExpander
'
'
''///////////////////////////////////////////////
''/// Event Handlers
''///////////////////////////////////////////////
'Private Sub Class_Initialize()
'
'    Set mobjConnector = New CsvAdoConnector
'
'    Set mitfConnector = mobjConnector
'
'    Set mobjAdoRecordsetSheetExpander = New AdoRSetSheetExpander
'
'    With mobjAdoRecordsetSheetExpander
'
'        Set .ADOConnectorInterface = mobjConnector
'
'        Set .OutputBookPathInterface = Me
'
'        .DataTableSheetFormattingInterface.FieldTitleInteriorType = ColumnNameInteriorOfCsvAdoSQL
'    End With
'End Sub
'
'Private Sub Class_Terminate()
'
'    mobjAdoRecordsetSheetExpander.CloseCacheOutputBookOfOpenByPresetModeProcess
'
'    Me.CloseConnection
'
'    Set mobjAdoRecordsetSheetExpander = Nothing
'
'    Set mobjConnector = Nothing
'End Sub
'
''///////////////////////////////////////////////
''/// Properties - connoted ADO connector
''///////////////////////////////////////////////
'Public Property Get SQLExecutionResult() As SQLResult
'
'    Set SQLExecutionResult = mobjConnector.SQLExecutionResult
'End Property
'
'Public Property Get IsSqlExecutionFailed() As Boolean
'
'    IsSqlExecutionFailed = mobjConnector.IsSqlExecutionFailed
'End Property
'
'Public Property Let AllowToRecordSQLLog(ByVal vblnAllowToRecord As Boolean)
'
'    mobjConnector.AllowToRecordSQLLog = vblnAllowToRecord
'End Property
'Public Property Get AllowToRecordSQLLog() As Boolean
'
'    AllowToRecordSQLLog = mobjConnector.AllowToRecordSQLLog
'End Property
'
'
'Public Property Get CurrentSheet() As Excel.Worksheet
'
'    Set CurrentSheet = mobjAdoRecordsetSheetExpander.IExpandAdoRecordsetOnSheet_CurrentSheet
'End Property
'
'
'Public Property Get CurrentBook() As Excel.Workbook
'
'    If Not Me.CurrentSheet Is Nothing Then
'
'        Set CurrentBook = Me.CurrentSheet.Parent
'    Else
'        Set CurrentBook = Nothing
'    End If
'End Property
'
''///////////////////////////////////////////////
''/// Properties
''///////////////////////////////////////////////
''''
''''
''''
'Public Property Get ADOConnectorInterface() As IADOConnector
'
'    Set ADOConnectorInterface = mitfConnector
'End Property
'
''''
''''
''''
'Public Property Get DataTableSheetFormattingInterface() As IDataTableSheetFormatter
'
'    Set DataTableSheetFormattingInterface = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface
'End Property
'
'
''**---------------------------------------------
''** connoted DataTableSheetFormatter
''**---------------------------------------------
'Public Property Get TopLeftRowIndex() As Long
'
'    TopLeftRowIndex = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.TopLeftRowIndex
'End Property
'Public Property Let TopLeftRowIndex(ByVal vintTopLeftRowIndex As Long)
'
'    mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.TopLeftRowIndex = vintTopLeftRowIndex
'End Property
'Public Property Get TopLeftColumnIndex() As Long
'
'    TopLeftColumnIndex = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.TopLeftColumnIndex
'End Property
'Public Property Let TopLeftColumnIndex(ByVal vintTopLeftColumnIndex As Long)
'
'    mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.TopLeftColumnIndex = vintTopLeftColumnIndex
'End Property
'
'
'Public Property Let AllowToShowFieldTitle(ByVal vblnAllowToShowFieldTitle As Boolean)
'
'    mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.AllowToShowFieldTitle = vblnAllowToShowFieldTitle
'End Property
'Public Property Get AllowToShowFieldTitle() As Boolean
'
'    AllowToShowFieldTitle = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.AllowToShowFieldTitle
'End Property
'
'
'
'Public Property Let AllowToSetAutoFilter(ByVal vblnAllowToSetAutoFilter As Boolean)
'
'    mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.AllowToSetAutoFilter = vblnAllowToSetAutoFilter
'End Property
'Public Property Get AllowToSetAutoFilter() As Boolean
'
'    AllowToSetAutoFilter = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.AllowToSetAutoFilter
'End Property
'
''''
'''' Dictionary(Of String(column title), Double(column-width))
''''
'Public Property Set FieldTitleToColumnWidthDic(ByVal vobjFieldTitleToColumnWidthDic As Scripting.Dictionary)
'
'    Set mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.FieldTitleToColumnWidthDic = vobjFieldTitleToColumnWidthDic
'End Property
'Public Property Get FieldTitleToColumnWidthDic() As Scripting.Dictionary
'
'    Set FieldTitleToColumnWidthDic = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.FieldTitleToColumnWidthDic
'End Property
'
''''
'''' Dictionary(Of String(column title order, such as 'Col1', 'Col07'), Double(column-width))
''''
'Public Property Set FieldTitleOrderToColumnWidthDic(ByVal vobjFieldTitleOrderToColumnWidthDic As Scripting.Dictionary)
'
'    Set mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.FieldTitleOrderToColumnWidthDic = vobjFieldTitleOrderToColumnWidthDic
'End Property
'Public Property Get FieldTitleOrderToColumnWidthDic() As Scripting.Dictionary
'
'    Set FieldTitleOrderToColumnWidthDic = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.FieldTitleOrderToColumnWidthDic
'End Property
'
'
''''
'''' When it is true, then ConvertDataForColumns procedure is used.
''''
'Public Property Get AllowToConvertDataForSpecifiedColumns() As Boolean
'
'    AllowToConvertDataForSpecifiedColumns = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.AllowToConvertDataForSpecifiedColumns
'End Property
'Public Property Let AllowToConvertDataForSpecifiedColumns(ByVal vblnAllowToConvertDataForSpecifiedColumns As Boolean)
'
'    mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.AllowToConvertDataForSpecifiedColumns = vblnAllowToConvertDataForSpecifiedColumns
'End Property
''''
'''' Dictionary(Of String(column title), ColumnDataConvertType(Enum definition))
''''
'Public Property Get FieldTitlesToColumnDataConvertTypeDic() As Scripting.Dictionary
'
'    Set FieldTitlesToColumnDataConvertTypeDic = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.FieldTitlesToColumnDataConvertTypeDic
'End Property
'Public Property Set FieldTitlesToColumnDataConvertTypeDic(ByVal vobjFieldTitlesToColumnDataConvertTypeDic As Scripting.Dictionary)
'
'    Set mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.FieldTitlesToColumnDataConvertTypeDic = vobjFieldTitlesToColumnDataConvertTypeDic
'End Property
'
''''
'''' ColumnsNumberFormatLocalParam
''''
'Public Property Set ColumnsNumberFormatLocal(ByVal vobjColumnsNumberFormatLocalParam As ColumnsNumberFormatLocalParam)
'
'    Set mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.ColumnsNumberFormatLocal = vobjColumnsNumberFormatLocalParam
'End Property
'Public Property Get ColumnsNumberFormatLocal() As ColumnsNumberFormatLocalParam
'
'    Set ColumnsNumberFormatLocal = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.ColumnsNumberFormatLocal
'End Property
'
''''
'''' set format-condition for each column
''''
'Public Property Set ColumnsFormatCondition(ByVal vobjColumnsFormatConditionParam As ColumnsFormatConditionParam)
'
'    Set mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.ColumnsFormatCondition = vobjColumnsFormatConditionParam
'End Property
'Public Property Get ColumnsFormatCondition() As ColumnsFormatConditionParam
'
'    Set ColumnsFormatCondition = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.ColumnsFormatCondition
'End Property
'
''''
'''' merge cells when the same values continue
''''
'Public Property Let AllowToMergeCellsByContinuousSameValues(ByVal vblnAllowToMergeCellsByContinuousSameValues As Boolean)
'
'    mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.AllowToMergeCellsByContinuousSameValues = vblnAllowToMergeCellsByContinuousSameValues
'End Property
'Public Property Get AllowToMergeCellsByContinuousSameValues() As Boolean
'
'    AllowToMergeCellsByContinuousSameValues = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.AllowToMergeCellsByContinuousSameValues
'End Property
'Public Property Let AllowToSetVerticalTopAlignmentTopOfMergeCells(ByVal vblnAllowToSetVerticalTopAlignmentTopOfMergeCells As Boolean)
'
'    mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.AllowToSetVerticalTopAlignmentTopOfMergeCells = vblnAllowToSetVerticalTopAlignmentTopOfMergeCells
'End Property
'Public Property Get AllowToSetVerticalTopAlignmentTopOfMergeCells() As Boolean
'
'    AllowToSetVerticalTopAlignmentTopOfMergeCells = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.AllowToSetVerticalTopAlignmentTopOfMergeCells
'End Property
'
'Public Property Set FieldTitlesToMergeCells(ByVal vobjFieldTitlesToMergeCells As Collection)
'
'    Set mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.FieldTitlesToMergeCells = vobjFieldTitlesToMergeCells
'End Property
'Public Property Get FieldTitlesToMergeCells()
'
'    Set FieldTitlesToMergeCells = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.FieldTitlesToMergeCells
'End Property
'
'Public Property Let FieldTitleInteriorType(ByVal venmFieldTitleInteriorType As FieldTitleInterior)
'
'    mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.FieldTitleInteriorType = venmFieldTitleInteriorType
'End Property
'Public Property Get FieldTitleInteriorType() As FieldTitleInterior
'
'    FieldTitleInteriorType = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.FieldTitleInteriorType
'End Property
'
'Public Property Let RecordBordersType(ByVal venmRecordCellsBordersType As RecordCellsBorders)
'
'    mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.RecordBordersType = venmRecordCellsBordersType
'End Property
'Public Property Get RecordBordersType() As RecordCellsBorders
'
'    RecordBordersType = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.RecordBordersType
'End Property
'
''///////////////////////////////////////////////
''/// Properties - connoted ADOConnectionSetting
''///////////////////////////////////////////////
'Public Property Get ConnectSetting() As ADOConnectionSetting
'
'    Set ConnectSetting = mobjConnector.ConnectSetting
'End Property
'
''''
'''' unit is second. [s]
''''
'Public Property Let CommandTimeout(ByVal vintTimeout As Long)
'
'    mobjConnector.CommandTimeout = vintTimeout
'End Property
'Public Property Get CommandTimeout() As Long
'
'    CommandTimeout = mobjConnector.CommandTimeout
'End Property
'
''///////////////////////////////////////////////
''/// Properties - CSV files connection by ACCESS OLE-DB provider
''///////////////////////////////////////////////
''''
'''' connecting CSV file path
''''
'Public Property Get ConnectingFilePath() As String
'
'    ConnectingFilePath = mobjConnector.ConnectingFilePath
'End Property
'
''''
'''' connecting directory path, which includes CSV files
''''
'Public Property Get ConnectingDirectoryPath() As String
'
'    ConnectingDirectoryPath = mobjConnector.ConnectingDirectoryPath
'End Property
'
''''
'''' when the load Excel sheet doesn't have the field header title text, this is False
''''
'Public Property Get HeaderFieldTitleExists() As Boolean
'
'    HeaderFieldTitleExists = mobjConnector.HeaderFieldTitleExists
'End Property
'
'
''///////////////////////////////////////////////
''/// Implemented operations
''///////////////////////////////////////////////
'Private Function IOutputBookPath_GetPreservedOutputBookPath() As String
'
'    IOutputBookPath_GetPreservedOutputBookPath = GetCsvAdoConnectingGeneralLogBookPath()
'End Function
'
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''**---------------------------------------------
''** preparation before SQL execution
''**---------------------------------------------
''''
'''' set input CSV file path
''''
'Public Sub SetInputCsvFileConnection(ByVal vstrCSVFilePath As String, _
'        Optional ByVal vblnHeaderFieldTitleExists As Boolean = True, _
'        Optional ByVal venmAccessOLEDBProviderType As AccessOLEDBProviderType = AccessOLEDBProviderType.AceOLEDB_12P0)
'
'    mobjConnector.SetInputCsvFileConnection vstrCSVFilePath, vblnHeaderFieldTitleExists, venmAccessOLEDBProviderType
'End Sub
'
''''
'''' set input directory path, which includes CSV files
''''
'Public Sub SetInputCsvDirectoryConnection(ByVal vstrCsvDirectoryPath As String, _
'        Optional ByVal vblnHeaderFieldTitleExists As Boolean = True, _
'        Optional ByVal venmAccessOLEDBProviderType As AccessOLEDBProviderType = AccessOLEDBProviderType.AceOLEDB_12P0)
'
'    mobjConnector.SetInputCsvDirectoryConnection vstrCsvDirectoryPath, vblnHeaderFieldTitleExists, venmAccessOLEDBProviderType
'End Sub
'
''''
'''' Close ADO connection
''''
'Public Sub CloseConnection()
'
'    mobjConnector.CloseConnection
'End Sub
'
''''
''''
''''
'Public Sub CloseCacheOutputBookOfOpenByPresetModeProcess()
'
'    mobjAdoRecordsetSheetExpander.CloseCacheOutputBookOfOpenByPresetModeProcess
'End Sub
'
''''
''''
''''
'Public Sub CloseAll()
'
'    CloseConnection
'
'    CloseCacheOutputBookOfOpenByPresetModeProcess
'End Sub
'
''''
'''' get recordset object by the Select SQL
''''
'Public Function GetRecordset(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag) As ADODB.Recordset
'
'    Set GetRecordset = mobjConnector.GetRecordset(vstrSQL, venmShowUpAdoErrorOptionFlag)
'End Function
'
'
''**---------------------------------------------
''** Output Recordset and logs to cells on sheet
''**---------------------------------------------
''''
'''' query SQL and output result to Excel sheet
''''
'Public Sub OutputToSheetFrom(ByVal vstrSQL As String, _
'        ByVal vobjOutputSheet As Excel.Worksheet, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'    mobjAdoRecordsetSheetExpander.OutputToSheetFrom vstrSQL, _
'            vobjOutputSheet, _
'            venmExecutedAdoSqlQueryLogPositionType, _
'            venmShowUpAdoErrorOptionFlag, _
'            mfobjGetConnectedCSVPathInsertText(), _
'            vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
'''' query SQL and output result to the Existed Excel sheet
''''
'Public Sub OutputInExistedBook(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputInExistedBook vstrSQL, _
'            vstrNewSheetName, _
'            venmExecutedAdoSqlQueryLogPositionType, _
'            venmShowUpAdoErrorOptionFlag, _
'            mfobjGetConnectedCSVPathInsertText(), _
'            vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
'Public Sub OutputInAlreadyExistedBook(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputInAlreadyExistedBook vstrSQL, _
'            vstrNewSheetName, _
'            venmExecutedAdoSqlQueryLogPositionType, _
'            venmShowUpAdoErrorOptionFlag, _
'            mfobjGetConnectedCSVPathInsertText(), _
'            vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
''''
''''
'Public Sub OutputInExistedBookByUnitTestMode(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputInExistedBookByUnitTestMode vstrSQL, _
'            vstrNewSheetName, _
'            venmExecutedAdoSqlQueryLogPositionType, _
'            venmShowUpAdoErrorOptionFlag, _
'            mfobjGetConnectedCSVPathInsertText(), _
'            vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputToSpecifiedBook(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputToSpecifiedBook vstrSQL, _
'            vstrOutputBookPath, _
'            vstrNewSheetName, _
'            venmExecutedAdoSqlQueryLogPositionType, _
'            venmShowUpAdoErrorOptionFlag, _
'            mfobjGetConnectedCSVPathInsertText(), _
'            vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
'Public Sub OutputToSpecifiedBookByUnitTestMode(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'    mobjAdoRecordsetSheetExpander.OutputToSpecifiedBookByUnitTestMode vstrSQL, vstrOutputBookPath, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedCSVPathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
'''' query SQL and output result to the already specified Excel sheet
''''
'Public Sub OutputToAlreadySpecifiedBook(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputToAlreadySpecifiedBook vstrSQL, _
'            vstrNewSheetName, _
'            venmExecutedAdoSqlQueryLogPositionType, _
'            venmShowUpAdoErrorOptionFlag, _
'            mfobjGetConnectedCSVPathInsertText(), _
'            vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
''''
''''
'Public Sub SetOutputExcelBookPath(ByVal vstrOutputBookPath As String, _
'        Optional ByVal venmOutputRSetExcelBookOpenModeProcessFlag As OutputRSetExcelBookOpenModeProcessFlag = OutputRSetExcelBookOpenModeProcessFlag.OpenOutputBookByUnitTestMode)
'
'
'    mobjAdoRecordsetSheetExpander.SetOutputExcelBookPath vstrOutputBookPath, _
'            venmOutputRSetExcelBookOpenModeProcessFlag
'End Sub
'
''**---------------------------------------------
''** Excecute SQL command (UPDATE, INSERT, DELETE)
''**---------------------------------------------
''''
'''' command SQL and output result-log to Excel sheet
''''
'Public Sub OutputCommandLogToSheetFrom(ByVal vstrSQL As String, _
'        ByVal vobjOutputSqlCommandLogSheet As Excel.Worksheet, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogToSheetFrom vstrSQL, vobjOutputSqlCommandLogSheet, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedCSVPathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputCommandLogToSpecifiedBook(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogToSpecifiedBook vstrSQL, vstrOutputBookPath, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedCSVPathInsertText(), vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputCommandLogToAlreadySpecifiedBook(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogToAlreadySpecifiedBook vstrSQL, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedCSVPathInsertText(), vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputCommandLogInAlreadyExistedBook(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogInAlreadyExistedBook vstrSQL, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedCSVPathInsertText(), vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputCommandLogToSpecifiedBookByUnitTestMode(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogToSpecifiedBookByUnitTestMode vstrSQL, vstrOutputBookPath, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedCSVPathInsertText(), vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
'''' query SQL and output result to the prepared Excel sheet
''''
'Public Sub OutputCommandLogInExistedBookByUnitTestMode(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogInExistedBookByUnitTestMode vstrSQL, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedCSVPathInsertText(), vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
'Private Function mfobjGetConnectedCSVPathInsertText() As Collection
'
'    Dim objInsertTexts As Collection
'
'    Set objInsertTexts = New Collection
'
'    With Me
'        If FileExistsByVbaDir(.ConnectingFilePath) Then
'
'            objInsertTexts.Add "<File Name> " & GetFileNameFromPathByVbaDir(.ConnectingFilePath) & ", <CSV Directory> " & GetParentDirectoryPathByVbaDir(.ConnectingFilePath)
'        Else
'            objInsertTexts.Add "<CSV Directory> " & .ConnectingDirectoryPath
'        End If
'    End With
'
'    Set mfobjGetConnectedCSVPathInsertText = objInsertTexts
'End Function
'
'
'''--VBA_Code_File--<SqlUTfCsvAdoSheetExpander.bas>--
'Attribute VB_Name = "SqlUTfCsvAdoSheetExpander"
''
''   Sanity test to output SQL result into a Excel sheet for CSV files in a directory using ADO OLE DB
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on both ADO and Excel
''       Dependent on Microsoft Office Access Data Base Engine OLE DB Provider,
''       which should have been the installed when the Microsoft Office Excel software has been installed.
''       Dependent on UTfCreateDataTable.bas
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Wed, 26/Jul/2023    Kalmclaeyd Tarclanus    Redesigned
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Constants
''///////////////////////////////////////////////
'Private Const mstrTestCSVFileNamePrefix As String = "TestGenerated"
'
'
''///////////////////////////////////////////////
''/// Sanity tests
''///////////////////////////////////////////////
''**---------------------------------------------
''** CsvAdoSheetExpander test
''**---------------------------------------------
''''
'''' Test-connecting
''''
'Private Sub ConnectTestToCSVFileByADO()
'
'    Dim strSQL As String, strPath As String, blnCSVGenerated As Boolean
'
'
'    blnCSVGenerated = False
'
'    strPath = mfstrGetSimpleTestCSVFilePath
'
'    With New Scripting.FileSystemObject
'
'        If Not .FileExists(strPath) Then
'
'            msubGenerateTestingCSVFile
'
'            blnCSVGenerated = True
'        End If
'    End With
'
'
'    With New CsvAdoSheetExpander
'
'        .SetInputCsvFileConnection strPath
'
'        strSQL = "SELECT * FROM " & GetCSVTableNameLeteral(strPath)
'
'
'        With .DataTableSheetFormattingInterface
'
'            Set .FieldTitleToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("col3,13")
'
'            Set .ColumnsNumberFormatLocal = New ColumnsNumberFormatLocalParam
'
'            With .ColumnsNumberFormatLocal
'
'                .SetNumberFormatLocalAndApplyFieldTitles "ddd, yyyy/m/d", GetColFromLineDelimitedChar("col3")
'            End With
'        End With
'
'
'        .OutputInExistedBookByUnitTestMode strSQL, _
'                "Load_sample_csv"
'
'
'        If blnCSVGenerated Then
'
'            AddAutoShapeGeneralMessage .CurrentSheet, _
'                    CreateObject("Scripting.FileSystemObject").GetFileName(strPath) & " is created now."
'        End If
'    End With
'End Sub
'
''**---------------------------------------------
''** About SheetExpander comprehensive parameters
''**---------------------------------------------
''''
'''' About CsvAdoSheetExpander
''''
'Private Sub msubComprehensiveTestToCsvAdoSheetExpander()
'
'    Dim varRowCol As Variant, objRowCol As Collection, intTopLeftRowIndex As Long, intTopLeftColumnIndex As Long
'    Dim strOutputBookPath As String, objBook As Excel.Workbook, objSheet As Excel.Worksheet, strSheetName As String, i As Long
'    Dim strSQL As String
'
'    Dim varAllowToShowFieldTitle As Variant
'    Dim varAllowToRecordSQLLog As Variant, blnAfterAllowToRecordSQLLog As Boolean
'    Dim objAdditionalLogDic As Scripting.Dictionary
'
'    Dim strInputCSVPath As String
'
'    Dim varExecutedAdoSqlQueryLogPositionType As Variant, enmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType
'
'
'    strOutputBookPath = mfstrGetSheetDecorationTestBookDir() & "\CsvAdoSheetExpanderOutputComprensiveTest.xlsx"
'
'    Set objBook = GetWorkbookAndPrepareForUnitTest(strOutputBookPath)
'
'    i = 1
'
'    strInputCSVPath = GetCSVPathAfterCheckFileExistenceOrCreateWhenItDoesntExist(BasicSampleDT, 20)
'
'    With New CsvAdoSheetExpander
'
'
'        .SetInputCsvFileConnection strInputCSVPath
'
'        strSQL = "SELECT * FROM " & GetCSVTableNameLeteral(strInputCSVPath)
'
'        For Each varAllowToRecordSQLLog In GetBooleanColFromLineDelimitedChar("True,False")
'
'            For Each varExecutedAdoSqlQueryLogPositionType In GetAllExecutedAdoSqlQueryLogPositionTypesCol()
'
'                enmExecutedAdoSqlQueryLogPositionType = varExecutedAdoSqlQueryLogPositionType
'
'                For Each varAllowToShowFieldTitle In GetBooleanColFromLineDelimitedChar("True,False")
'
'                    For Each varRowCol In GetDataTableSheetCreateTestConditionOnlyTopLeftDTCol(5, 7)
'
'                        Set objRowCol = varRowCol
'
'                        GetDataTableTopLeftWhenCreateTableOnSheet intTopLeftRowIndex, intTopLeftColumnIndex, objRowCol
'
'
'                        .TopLeftRowIndex = intTopLeftRowIndex
'
'                        .TopLeftColumnIndex = intTopLeftColumnIndex
'
'                        .AllowToShowFieldTitle = varAllowToShowFieldTitle
'
'                        .AllowToRecordSQLLog = varAllowToRecordSQLLog
'
'                        .RecordBordersType = RecordCellsGrayAll
'
'                        If .AllowToShowFieldTitle Then
'
'                            Set .FieldTitleToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("FixedString,12,RowNumber,13,FlowerTypeSample,18,RandomInteger,17")
'                        Else
'                            Set .FieldTitleOrderToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("Col1,12,Col2,13,Col3,18,Col4,17")
'                        End If
'
'
'                        Set objSheet = GetNewWorksheetAfterAllExistedSheetsFromBook(objBook)
'
'                        strSheetName = "CsvSQLTest_" & Format(i, "00")
'
'                        objSheet.Name = strSheetName
'
'                        .OutputToSheetFrom strSQL, objSheet, enmExecutedAdoSqlQueryLogPositionType
'
'
'                        Set objAdditionalLogDic = New Scripting.Dictionary
'
'                        With objAdditionalLogDic
'
'                            .Add "Input AllowToRecordSQLLog", varAllowToRecordSQLLog
''
''                            .Add "After-SQL-executed AllowToRecordSQLLog", blnAfterAllowToRecordSQLLog
'
'                            .Add "ExecutedAdoSqlQueryLogPositionType", GetExecutedAdoSqlQueryLogPositionTypeTextFromEnm(enmExecutedAdoSqlQueryLogPositionType)
'
'                            .Add "AllowToShowFieldTitle", varAllowToShowFieldTitle
'
'                            .Add "TopLeftRowIndex", intTopLeftRowIndex
'
'                            .Add "TopLeftColumnIndex", intTopLeftColumnIndex
'                        End With
'
'                        OutputDicToAutoShapeLogTextOnSheet objSheet, objAdditionalLogDic, "CSV connecting ADOSheetFormatter some parameters test"
'
'                        i = i + 1
'                    Next
'                Next
'            Next
'        Next
'    End With
'
'    DeleteDummySheetWhenItExists objBook
'End Sub
'
''''
''''
''''
'Private Function mfstrGetSheetDecorationTestBookDir() As String
'
'    Dim strDir As String
'
'    strDir = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\ADOSheetFormatterTest"
'
'    ForceToCreateDirectory strDir
'
'    mfstrGetSheetDecorationTestBookDir = strDir
'End Function
'
'
''**---------------------------------------------
''** CsvAdoConnector connecting by generated sample CSV
''**---------------------------------------------
''''
''''
''''
'Private Sub msubSanityTest01ToConnectCSVTestAfterCSVCreate()
'
'    GetOutputBookPathOfConnectingAdoCSVTestAfterCSVCreate BasicSampleDT, _
'            100, _
'            False
'End Sub
'
'
'Private Sub msubSanityTest02ToConnectCSVTestAfterCSVCreate()
'
'    GetOutputBookPathOfConnectingAdoCSVTestAfterCSVCreate IncludeFictionalProductionNumberSampleDT, _
'            10, _
'            True
'End Sub
'
'
'Private Sub msubSanityTest02ToConnectCSVTestAfterCSVSampleMasterTableOfFishType()
'
'    GetOutputBookPathOfConnectingAdoCSVTestAfterCSVCreate FishTypeSampleMasterDT, _
'            vblnIncludesSQLLogToSheet:=True
'End Sub
'
'Private Sub msubSanityTest02ToConnectCSVTestAfterCSVSampleMasterTableOfFlowerType()
'
'    GetOutputBookPathOfConnectingAdoCSVTestAfterCSVCreate FlowerTypeSampleMasterDT, _
'            vblnIncludesSQLLogToSheet:=True
'End Sub
'
'
''''
''''
''''
'Private Sub msubSanityTestToConectCSVWithJOINSQL()
'
'    Dim strInputCsvPath01 As String, strInputCsvPath02 As String
'    Dim strTableName01 As String, strTableName02 As String, strDir As String
'    Dim s As String
'
'    strInputCsvPath01 = GetCSVPathAfterCreateSampleDataTable(IncludeFictionalProductionNumberSampleDT, 1000)
'
'    strInputCsvPath02 = GetCSVPathAfterCreateSampleDataTable(FishTypeSampleMasterDT)
'
'    With New Scripting.FileSystemObject
'
'        strDir = .GetParentFolderName(strInputCsvPath01)
'
'        strTableName01 = "[" & .GetFileName(strInputCsvPath01) & "]"
'
'        strTableName02 = "[" & .GetFileName(strInputCsvPath02) & "]"
'
'    End With
'
'    With New CsvAdoSheetExpander
'
'        .SetInputCsvDirectoryConnection strDir
'
'        .RecordBordersType = RecordCellsGrayAll
'
'        Set .FieldTitleToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("RowNumber,12,RealNumber,12,FishType,11,FictionalProductionSerialNumber,30,FishTypeJpn,11,RandAlpha,11")
'
'
'        s = "SELECT * FROM " & strTableName01
'
'        .OutputInExistedBookByUnitTestMode s, "1stTable"
'
'        s = "SELECT * FROM " & strTableName02
'
'        .OutputInExistedBookByUnitTestMode s, "2ndTable"
'
'        's = "SELECT A.RowNumber, A.RealNumber, A.FishType FROM " & strTableName01 & " AS A"
'
'        s = "SELECT A.RowNumber, A.RealNumber, A.FishType, B.FishTypeJpn, B.RandAlpha "
'
'        s = s & vbNewLine & "    FROM " & strTableName01 & " AS A "
'
'        s = s & vbNewLine & "    LEFT JOIN " & strTableName02 & " AS B "
'
'        s = s & vbNewLine & "    ON A.FishType = B.FishTypeEng"
'
'
'        .OutputInExistedBookByUnitTestMode s, _
'                "SQLResult"
'
'        AddAutoShapeGeneralMessage .CurrentSheet, _
'                "The SQL 'JOIN' can be also used by connecting plural CSV files in a conntected directory"
'
'        DeleteDummySheetWhenItExists .CurrentBook
'    End With
'End Sub
'
'
''**---------------------------------------------
''** CSV directory connection tests
''**---------------------------------------------
''''
''''
''''
'Private Sub msubSanityTestToTryToConnectCSVDirectory()
'
'    Dim strSQL As String, strPath As String, blnCSVGenerated As Boolean
'    Dim objCsvADOConnectStrGenerator As ADOConStrOfAceOleDbCsv
'
'    blnCSVGenerated = False
'
'    msubDeleteTestingCSVFile    ' If the directory path exists, the ADO connection is to be succeeded even though no CSV file exist.
'
'    strPath = mfstrGetSimpleTestCSVFilePath
'
'    Set objCsvADOConnectStrGenerator = New ADOConStrOfAceOleDbCsv
'
'    objCsvADOConnectStrGenerator.SetCsvFileConnection strPath
'
'    ConnectToCsvFileByAdo objCsvADOConnectStrGenerator.GetConnectionString
'
'    ' Since no CSV file exist, the SQL will be failed.
'End Sub
'
''''
'''' Confirm that an error occurs
''''
'Private Sub msubSanityTestToTryToConnectCSVDirectoryWhichDoesNotExist()
'
'    Dim strSQL As String, strPath As String, blnCSVGenerated As Boolean
'    Dim objCsvADOConnectStrGenerator As ADOConStrOfAceOleDbCsv
'
'    blnCSVGenerated = False
'
'    strPath = mfstrGetTempCSVDirectoryPath("TemporaryCSF02", False)
'
'    Set objCsvADOConnectStrGenerator = New ADOConStrOfAceOleDbCsv
'
'    objCsvADOConnectStrGenerator.SetCsvDirectoryConnection strPath
'
'    ConnectToCsvFileByAdo objCsvADOConnectStrGenerator.GetConnectionString
'End Sub
'
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''''
''''
''''
'''' <Argument>venmUnitTestPreparedSampleDataTable: Input</Argument>
'''' <Argument>vintCountOfRows: Input</Argument>
'''' <Argument>vblnIncludesSQLLogToSheet: Input</Argument>
'Public Function GetOutputBookPathOfConnectingAdoCSVTestAfterCSVCreate(ByVal venmUnitTestPreparedSampleDataTable As UnitTestPreparedSampleDataTable, _
'        Optional ByVal vintCountOfRows As Long = 100, _
'        Optional ByVal vblnIncludesSQLLogToSheet As Boolean = True) As String
'
'
'    Dim strInputCSVPath As String, strSQL As String, strLog As String
'
'    Dim strOutputBookPath As String
'
'    strInputCSVPath = GetCSVPathAfterCheckFileExistenceOrCreateWhenItDoesntExist(venmUnitTestPreparedSampleDataTable, vintCountOfRows)
'
'    With New CsvAdoSheetExpander
'
'        .SetInputCsvFileConnection strInputCSVPath
'
'        .DataTableSheetFormattingInterface.RecordBordersType = RecordCellsGrayAll
'
'        strSQL = "SELECT * FROM " & GetCSVTableNameLeteral(strInputCSVPath)
'
'        .OutputInExistedBookByUnitTestMode strSQL, "Load_sample_csv", vblnIncludesSQLLogToSheet
'
'
'        strLog = GetLogTextOfPreparedSampleDataTableBaseName(venmUnitTestPreparedSampleDataTable)
'
'
'        AddAutoShapeGeneralMessage .CurrentSheet, strLog
'
'        strOutputBookPath = .CurrentBook.FullName
'
'        .CloseAll
'    End With
'
'    GetOutputBookPathOfConnectingAdoCSVTestAfterCSVCreate = strOutputBookPath
'End Function
'
'
''''
'''' CSV directory connection test
''''
'Public Sub ConnectToCsvFileByAdo(ByVal vstrConnectionString As String)
'
'    Dim objCon As ADODB.Connection
'
'    Set objCon = New ADODB.Connection
'
'    With objCon
'
'        .ConnectionString = vstrConnectionString
'
'        On Error GoTo ErrHandler
'
'        .Open   ' If the directory path exists, the ADO connection is to be succeeded even though no CSV file exist.
'
'        .Close
'
'ErrHandler:
'        If Err.Number <> 0 Then
'
'            Debug.Print "Failed to connect directory of CSV files: " & vstrConnectionString
'
'            MsgBox "Failed to connect directory of CSV files: " & vbNewLine & vstrConnectionString, vbCritical Or vbOKOnly
'        Else
'            Debug.Print "Connected to the directory of CSV files"
'        End If
'
'        On Error GoTo 0
'    End With
'End Sub
'
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
''**---------------------------------------------
''** test CSV file generation
''**---------------------------------------------
''''
''''
''''
'Private Sub msubGenerateTestingCSVFile()
'
'    Dim strPath As String
'
'    strPath = mfstrGetSimpleTestCSVFilePath
'
'    With New Scripting.FileSystemObject
'
'        With .CreateTextFile(strPath, True)
'
'            ' field title
'            .WriteLine "co1,col2,col3"
'
'            ' data
'            .WriteLine "abc,123," & Format(DateAdd("d", -1, Now()), "yyyy/m/d") '  2021/6/28"
'
'            .WriteLine "def,456," & Format(DateAdd("d", -2, Now()), "yyyy/m/d") '  2021/6/29"
'
'            .WriteLine "ghi,789," & Format(DateAdd("d", -3, Now()), "yyyy/m/d") '  2021/6/30"
'
'            .Close
'        End With
'    End With
'End Sub
'
''''
'''' delete a generated sample CSV file
''''
'Private Sub msubDeleteTestingCSVFile()
'
'    Dim strPath As String
'
'    strPath = mfstrGetSimpleTestCSVFilePath
'
'    With New Scripting.FileSystemObject
'
'        If .FileExists(strPath) Then
'
'            .DeleteFile strPath, True
'        End If
'    End With
'End Sub
'
''''
''''
''''
'Private Function mfstrGetSimpleTestCSVFilePath()
'
'    Dim strDir As String, strFileName As String, strPath As String
'
'
'    strDir = mfstrGetTempCSVDirectoryPath
'
'    strFileName = mstrTestCSVFileNamePrefix & ".csv"
'
'    strPath = strDir & "\" & strFileName
'
'    mfstrGetSimpleTestCSVFilePath = strPath
'End Function
'
'
''''
''''
''''
'Private Function mfstrGetTempCSVDirectoryPath(Optional ByVal vstrChildDirectoryName As String = "TemporaryCSVFiles", _
'        Optional ByVal vblnAllowToCreateDirectory As Boolean = True) As String
'    Dim strDir As String
'
'    With New Scripting.FileSystemObject
'
'        strDir = GetCurrentOfficeFileObject().Path & "\" & vstrChildDirectoryName
'
'        If vblnAllowToCreateDirectory Then
'
'            ForceToCreateDirectory strDir
'        End If
'    End With
'
'    mfstrGetTempCSVDirectoryPath = strDir
'End Function
'
'
'
'
'
'
'''--VBA_Code_File--<UTfReadFromShapeByAdoCsv.bas>--
'Attribute VB_Name = "UTfReadFromShapeByAdoCsv"
''
''   Sanity tests for getting key-value dictionary from auto-shape on Excel sheet
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on Excel
''       Dependent on ReadFromShapeOnXlSheet.bas
''       Dependent on both UTfReadFromShapeOnXlSheet.bas and SqlUTfCsvAdoSheetExpander.bas
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2024 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Sun, 18/Aug/2024    Kalmclaeyd Tarclanus    Separated from UTfReadFromShapeOnXlSheet.bas
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Sanity tests
''///////////////////////////////////////////////
''**---------------------------------------------
''** get Dictionary test
''**---------------------------------------------
''''
'''' get Dictionary(Of String[ShapeName], Dictionary(Of String[Key], String[Value]))
''''
'Private Sub msubSanityTestToGetShapeNameToKeyValueDicByReadingFromCsvAdoExpanderAutoShapes()
'
'    DebugDic mfobjGetShapeNameToKeyValueDicFromReadingFromCsvAdoExpanderAutoShapes()
'End Sub
'
'
''''
'''' get Dictionary(Of String[Key], String[Value]) about CsvAdoSheetExpander
''''
'Private Sub msubSanityTestToGetKeyValueDicByReadingFromCsvAdoExpanderAutoShapes()
'
'    Dim objDic As Scripting.Dictionary
'
'    Set objDic = mfobjGetKeyValueDicFromReadingFromCsvAdoExpanderAutoShapes()
'
'    DebugDic objDic
'End Sub
'
''''
''''
''''
'Private Sub msubSanityTestToGetShapeNameToKeyValueDicByReadingFromExcelAdoExpanderAutoShapes()
'
'    DebugDic mfobjGetShapeNameToKeyValueDicFromReadingFromCsvAdoExpanderAutoShapes()
'End Sub
'
''**---------------------------------------------
''** Sanity test to output results to Excel book
''**---------------------------------------------
''''
'''' get Dictionary(Of String[ShapeName], Dictionary(Of String[Key], String[Value]))
''''
'Private Sub msubSanityTestToOutputToExcelBookAfterGetShapeNameToKeyValueDicByReadingFromCsvAdoExpanderAutoShapes()
'
'    Dim objDic As Scripting.Dictionary, strBookPath As String
'
'    Set objDic = mfobjGetShapeNameToKeyValueDicFromReadingFromCsvAdoExpanderAutoShapes()
'
'    strBookPath = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\SanityTestReadAllShapesKeyValuesOfSheet.xlsx"
'
'    OutputSheetShapeNameToKeyValuesDicToBook objDic, strBookPath
'End Sub
'
''''
'''' get Dictionary(Of String[Key], String[Value]) about CsvAdoSheetExpander
''''
'Private Sub msubSanityTestToOutputToExcelBookAfterGetKeyValueDicByReadingFromCsvAdoExpanderAutoShapes()
'
'    Dim objDic As Scripting.Dictionary, strBookPath As String
'
'    Set objDic = mfobjGetKeyValueDicFromReadingFromCsvAdoExpanderAutoShapes()
'
'    strBookPath = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\SanityTestReadAllShapesKeyValuesOfSheet.xlsx"
'
'    OutputSheetShapesKeyValuesToBook objDic, strBookPath
'End Sub
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
''**---------------------------------------------
''** Tools for getting Dictinary objects
''**---------------------------------------------
''''
''''
''''
'Private Function mfobjGetShapeNameToKeyValueDicFromReadingFromCsvAdoExpanderAutoShapes() As Scripting.Dictionary
'
'    Set mfobjGetShapeNameToKeyValueDicFromReadingFromCsvAdoExpanderAutoShapes = mfobjGetDicFromReadingFromCsvAdoExpanderAutoShapes(True)
'End Function
'
''''
''''
''''
'Private Function mfobjGetKeyValueDicFromReadingFromCsvAdoExpanderAutoShapes() As Scripting.Dictionary
'
'    Set mfobjGetKeyValueDicFromReadingFromCsvAdoExpanderAutoShapes = mfobjGetDicFromReadingFromCsvAdoExpanderAutoShapes(False)
'End Function
'
''''
''''
''''
'Private Function mfobjGetDicFromReadingFromCsvAdoExpanderAutoShapes(Optional ByVal vblnGetShapeNameToKeyValueDic As Boolean = True) As Scripting.Dictionary
'
'    Dim strBookPath As String, objSheet As Excel.Worksheet
'    Dim objDic As Scripting.Dictionary
'
'    strBookPath = GetOutputBookPathOfConnectingAdoCSVTestAfterCSVCreate(BasicSampleDT, 100, False)
'
'    With GetWorkbook(strBookPath)
'
'        Set objSheet = .Worksheets.Item(1)
'
'        If vblnGetShapeNameToKeyValueDic Then
'
'            Set objDic = GetShapeNameToKeyValueDicFromShapes(objSheet)
'        Else
'            Set objDic = GetKeyValueDicFromShapes(objSheet)
'        End If
'    End With
'
'    Set mfobjGetDicFromReadingFromCsvAdoExpanderAutoShapes = objDic
'End Function
'
'''--VBA_Code_File--<UTfAdoConnectAccDbForXl.bas>--
'Attribute VB_Name = "UTfAdoConnectAccDbForXl"
''
''   Serving Microsoft Access db serving tests by ADO without Access application
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on both ADO and Excel
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Tue, 16/May/2023    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
'
''///////////////////////////////////////////////
''/// Sanity tests
''///////////////////////////////////////////////
''''
''''
''''
'Private Sub msubSanityTestToLoadAccDbTableToExcelSheetWithUsingQueryTable()
'
'    Dim strDbPath As String, strDbName As String, strSQL As String, strConnectionString As String
'    Dim strBookName As String
'    Dim strOutputBookPath As String, objBook As Excel.Workbook, objSheet As Excel.Worksheet
'    Dim objQueryTable As Excel.QueryTable
'
'
'    strDbName = "TestFromVba01.accdb"
'
'    strDbPath = GetPathAfterCreateSampleAccDb(strDbName)
'
'
'
'    strBookName = Replace(strDbName, ".", "_") & ".xlsx"
'
'    ' prepare a AccDb
'    strOutputBookPath = mfstrGetTemporaryBookDir() & "\" & strBookName
'
'    Set objSheet = GetWorkbookAndAnWorksheetPrepareForUnitTest(strOutputBookPath, "FromAccDb")
'
'
'    'strSQL = "select * from TestTable;"
'
'    strSQL = "select * from TestTable"
'
'    strConnectionString = "ODBC;DSN=MS Access Database;DBQ=" & strDbPath
'
'
'    Set objQueryTable = objSheet.QueryTables.Add(strConnectionString, objSheet.Range("A1"), strSQL)
'
'    With objQueryTable
'
'        .Connection = strConnectionString
'
'        .Name = "A_Query"
'
'        '.BackgroundQuery = False  ' prevent the back-ground processing
'
'        .Refresh
'
'        '.Delete  ' delete query-table
'    End With
'End Sub
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
''''
''''
''''
'Private Function mfstrGetTemporaryBookDir() As String
'
'    Dim strDir As String
'
'    strDir = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\LoadedFromAccDb"
'
'    ForceToCreateDirectory strDir
'
'    mfstrGetTemporaryBookDir = strDir
'End Function
'
'''--VBA_Code_File--<AccDbAdoSheetExpander.cls>--
'VERSION 1.0 CLASS
'BEGIN
'  MultiUse = -1  'True
'END
'Attribute VB_Name = "AccDbAdoSheetExpander"
'Attribute VB_GlobalNameSpace = False
'Attribute VB_Creatable = False
'Attribute VB_PredeclaredId = False
'Attribute VB_Exposed = False
''
''   output SQL result into Excel.Worksheet after connecting Access database by ADO OLE DB interface
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on both ADO and Excel
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Wed, 26/Jul/2023    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Interface implementations
''///////////////////////////////////////////////
'Implements IOutputBookPath
'
''///////////////////////////////////////////////
''/// Declarations
''///////////////////////////////////////////////
'Private mobjConnector As AccDbAdoConnector
'
'Private mobjAdoRecordsetSheetExpander As AdoRSetSheetExpander
'
'
''///////////////////////////////////////////////
''/// Event Handlers
''///////////////////////////////////////////////
'Private Sub Class_Initialize()
'
'    Set mobjConnector = New AccDbAdoConnector
'
'    Set mobjAdoRecordsetSheetExpander = New AdoRSetSheetExpander
'
'    With mobjAdoRecordsetSheetExpander
'
'        Set .ADOConnectorInterface = mobjConnector
'
'        Set .OutputBookPathInterface = Me
'
'        .DataTableSheetFormattingInterface.FieldTitleInteriorType = ColumnNameInteriorOrMSAccessRDBSQL
'    End With
'End Sub
'
''''
''''
''''
'Private Sub Class_Terminate()
'
'    mobjAdoRecordsetSheetExpander.CloseCacheOutputBookOfOpenByPresetModeProcess
'
'    Debug.Print "class AccDbAdoSheetExpander terminate -> " & mobjAdoRecordsetSheetExpander Is Nothing
'
'    Me.CloseConnection
'
'    Set mobjAdoRecordsetSheetExpander = Nothing
'
'    Set mobjConnector = Nothing
'End Sub
'
'
''///////////////////////////////////////////////
''/// Properties - connoted ADO connector
''///////////////////////////////////////////////
'Public Property Get SQLExecutionResult() As SQLResult
'
'    Set SQLExecutionResult = mobjConnector.SQLExecutionResult
'End Property
'
'Public Property Get IsSqlExecutionFailed() As Boolean
'
'    IsSqlExecutionFailed = mobjConnector.IsSqlExecutionFailed
'End Property
'
'Public Property Let AllowToRecordSQLLog(ByVal vblnAllowToRecord As Boolean)
'
'    mobjConnector.AllowToRecordSQLLog = vblnAllowToRecord
'End Property
'Public Property Get AllowToRecordSQLLog() As Boolean
'
'    AllowToRecordSQLLog = mobjConnector.AllowToRecordSQLLog
'End Property
'
'
'Public Property Get CurrentSheet() As Excel.Worksheet
'
'    Set CurrentSheet = mobjAdoRecordsetSheetExpander.IExpandAdoRecordsetOnSheet_CurrentSheet
'End Property
'
'
'Public Property Get CurrentBook() As Excel.Workbook
'
'    If Not Me.CurrentSheet Is Nothing Then
'
'        Set CurrentBook = Me.CurrentSheet.Parent
'    Else
'        Set CurrentBook = Nothing
'    End If
'End Property
'
'
''///////////////////////////////////////////////
''/// Properties
''///////////////////////////////////////////////
''''
''''
''''
'Public Property Get ADOConnectorInterface() As IADOConnector
'
'    Set ADOConnectorInterface = mobjConnector.ADOConnectorInterface
'End Property
'
''''
''''
''''
'Public Property Get DataTableSheetFormattingInterface() As IDataTableSheetFormatter
'
'    Set DataTableSheetFormattingInterface = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface
'End Property
'
'
''///////////////////////////////////////////////
''/// Properties - connoted ADOConnectionSetting
''///////////////////////////////////////////////
'Public Property Get ConnectSetting() As ADOConnectionSetting
'
'    Set ConnectSetting = mobjConnector.ConnectSetting
'End Property
'
''''
'''' unit is second. [s]
''''
'Public Property Let CommandTimeout(ByVal vintTimeout As Long)
'
'    mobjConnector.CommandTimeout = vintTimeout
'End Property
'Public Property Get CommandTimeout() As Long
'
'    CommandTimeout = mobjConnector.CommandTimeout
'End Property
'
'
''///////////////////////////////////////////////
''/// Properties - Access database connection by ACCESS OLE-DB provider
''///////////////////////////////////////////////
'Public Property Get ConnectingFilePath() As String
'
'    ConnectingFilePath = mobjConnector.ConnectingFilePath
'End Property
'
'
''///////////////////////////////////////////////
''/// Implemented operations
''///////////////////////////////////////////////
''''
''''
''''
'Private Function IOutputBookPath_GetPreservedOutputBookPath() As String
'
'    IOutputBookPath_GetPreservedOutputBookPath = GetAccDbConnectingPrearrangedLogBookPath()
'End Function
'
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''**---------------------------------------------
''** set ADO connection string all parameters directly
''**---------------------------------------------
''''
''''
''''
'Public Sub SetMsAccessConnection(ByVal vstrDbPath As String, _
'        Optional ByVal vstrOldTypeDbPassword As String = "", _
'        Optional ByVal venmAccessOLEDBProviderType As AccessOLEDBProviderType = AccessOLEDBProviderType.AceOLEDB_12P0)
'
'
'    mobjConnector.SetMsAccessConnection vstrDbPath, vstrOldTypeDbPassword, venmAccessOLEDBProviderType
'End Sub
'
''**---------------------------------------------
''** set ADO connection string parameters by a User-form interface
''**---------------------------------------------
''''
''''
''''
'''' <Argument>vstrSettingKeyName: Input</Argument>
'''' <Argument>vstrDbPath: Input</Argument>
'''' <Argument>venmAccessOLEDBProviderType: Input</Argument>
'''' <Argument>vstrOldTypeDbPassword: Input</Argument>
'''' <Return>Boolean</Return>
'Public Function IsAdoConnectionContinuedAfterAccessOleDbConnectionParametersByForm(ByVal vstrSettingKeyName As String, _
'        Optional ByVal vstrDbPath As String = "", _
'        Optional ByVal venmAccessOLEDBProviderType As AccessOLEDBProviderType = AccessOLEDBProviderType.AceOLEDB_12P0, _
'        Optional ByVal vstrOldTypeDbPassword As String = "")
'
'
'    IsAdoConnectionContinuedAfterAccessOleDbConnectionParametersByForm = mobjConnector.IsAdoConnectionContinuedAfterAccessOleDbConnectionParametersByForm(vstrSettingKeyName, _
'            vstrDbPath, _
'            venmAccessOLEDBProviderType, _
'            vstrOldTypeDbPassword)
'End Function
'
''''
'''' Close ADO connection
''''
'Public Sub CloseConnection()
'
'    mobjConnector.CloseConnection
'End Sub
'
''''
''''
''''
'Public Sub CloseCacheOutputBookOfOpenByPresetModeProcess()
'
'    mobjAdoRecordsetSheetExpander.CloseCacheOutputBookOfOpenByPresetModeProcess
'End Sub
'
''''
''''
''''
'Public Sub CloseAll()
'
'    CloseConnection
'
'    CloseCacheOutputBookOfOpenByPresetModeProcess
'End Sub
'
''**---------------------------------------------
''** Output Recordset and logs to cells on sheet
''**---------------------------------------------
''''
'''' query SQL and output result to Excel sheet
''''
'Public Sub OutputToSheetFrom(ByVal vstrSQL As String, _
'        ByVal vobjOutputSheet As Excel.Worksheet, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'    mobjAdoRecordsetSheetExpander.OutputToSheetFrom vstrSQL, _
'            vobjOutputSheet, _
'            venmExecutedAdoSqlQueryLogPositionType, _
'            venmShowUpAdoErrorOptionFlag, _
'            mfobjGetConnectedAccDbPathInsertText(), _
'            vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
'''' query SQL and output result to the Existed Excel sheet
''''
'Public Sub OutputInExistedBook(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputInExistedBook vstrSQL, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedAccDbPathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
'Public Sub OutputInAlreadyExistedBook(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputInAlreadyExistedBook vstrSQL, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedAccDbPathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
''''
''''
'Public Sub OutputInExistedBookByUnitTestMode(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputInExistedBookByUnitTestMode vstrSQL, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedAccDbPathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputToSpecifiedBook(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputToSpecifiedBook vstrSQL, vstrOutputBookPath, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedAccDbPathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
'Public Sub OutputToSpecifiedBookByUnitTestMode(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'    mobjAdoRecordsetSheetExpander.OutputToSpecifiedBookByUnitTestMode vstrSQL, vstrOutputBookPath, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedAccDbPathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
'''' query SQL and output result to the already specified Excel sheet
''''
'Public Sub OutputToAlreadySpecifiedBook(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'    mobjAdoRecordsetSheetExpander.OutputToAlreadySpecifiedBook vstrSQL, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedAccDbPathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
''''
''''
'Public Sub SetOutputExcelBookPath(ByVal vstrOutputBookPath As String, _
'        Optional ByVal venmOutputRSetExcelBookOpenModeProcessFlag As OutputRSetExcelBookOpenModeProcessFlag = OutputRSetExcelBookOpenModeProcessFlag.OpenOutputBookByUnitTestMode)
'
'    mobjAdoRecordsetSheetExpander.SetOutputExcelBookPath vstrOutputBookPath, venmOutputRSetExcelBookOpenModeProcessFlag
'End Sub
'
''**---------------------------------------------
''** Excecute SQL command (UPDATE, INSERT, DELETE)
''**---------------------------------------------
''''
'''' command SQL and output result-log to Excel sheet
''''
'Public Sub OutputCommandLogToSheetFrom(ByVal vstrSQL As String, _
'        ByVal vobjOutputSqlCommandLogSheet As Excel.Worksheet, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogToSheetFrom vstrSQL, vobjOutputSqlCommandLogSheet, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedAccDbPathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputCommandLogToSpecifiedBook(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogToSpecifiedBook vstrSQL, vstrOutputBookPath, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedAccDbPathInsertText(), vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputCommandLogToAlreadySpecifiedBook(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogToAlreadySpecifiedBook vstrSQL, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedAccDbPathInsertText(), vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputCommandLogInAlreadyExistedBook(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogInAlreadyExistedBook vstrSQL, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedAccDbPathInsertText(), vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputCommandLogToSpecifiedBookByUnitTestMode(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogToSpecifiedBookByUnitTestMode vstrSQL, vstrOutputBookPath, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedAccDbPathInsertText(), vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
'''' query SQL and output result to the prepared Excel sheet
''''
'Public Sub OutputCommandLogInExistedBookByUnitTestMode(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogInExistedBookByUnitTestMode vstrSQL, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedAccDbPathInsertText(), vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
''''
''''
''''
'Private Function mfobjGetConnectedAccDbPathInsertText() As Collection
'
'    Dim objInsertTexts As Collection
'
'    Set objInsertTexts = New Collection
'
'    With mobjConnector
'
'        objInsertTexts.Add "<File Name> " & GetFileNameFromPathByVbaDir(.ConnectingFilePath) & ", <Directory> " & GetParentDirectoryPathByVbaDir(.ConnectingFilePath)
'    End With
'
'    Set mfobjGetConnectedAccDbPathInsertText = objInsertTexts
'End Function
'
'
'
'''--VBA_Code_File--<SqlUTfAccDbSheetExpander.bas>--
'Attribute VB_Name = "SqlUTfAccDbSheetExpander"
''
''   Sanity test to output SQL result into a Excel sheet for Access database using ADO OLE DB
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on ADO
''       Dependent on Microsoft Office Access Data Base Engine OLE DB Provider,
''       which should have been the installed when the Microsoft Office Excel software has been installed.
''       Dependent on SqlAdoConnectingUTfAccDb.bas
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Mon, 15/May/2023    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
'
''///////////////////////////////////////////////
''/// Control VBE windows
''///////////////////////////////////////////////
''**---------------------------------------------
''** Open relative VBE code-panes
''**---------------------------------------------
'Private Sub OpenVBECodePaneRelativeToSqlUTfAccDbSheetExpander()
'
'    ' Focusing on this procdure, please press [F5] key to start this. Therefore this scope is private.
'
'    On Error Resume Next ' If the 'ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma' isn't included in the VBA project, the following causes no syntax error.
'
'    Application.Run "ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma", "UTfAdoConnectAccDb,UTfDaoConnectAccDb"
'End Sub
'
'
''///////////////////////////////////////////////
''/// Sanity tests
''///////////////////////////////////////////////
''**---------------------------------------------
''** AccDbAdoSheetExpander
''**---------------------------------------------
''''
'''' Create an in-line table from the only SELECT sql inside
''''
'Private Sub msubSanityTestToConnectToAccDbBySqlInlineVirtualTableByAccDbAdoSheetExpander()
'
'    Dim strSQL As String
'
'    With New AccDbAdoSheetExpander
'
'        .SetMsAccessConnection PrepareAccessDbAfterCreateSampleAccDbIfItDoesntExist("TestDbOfAccDbAdoSheetExpander.accdb")
'
'        .AllowToRecordSQLLog = True
'
'        'strSQL = "SELECT 'ABC', 'DEF' UNION SELECT 'GHI', 'JKL'" ' Error occurs
'
'        'strSQL = "SELECT 1"
'
'        'strSQL = "SELECT 'ABC', 'DEF'"
'
'        strSQL = "SELECT 'ABC' AS A_Column, 'DEF' AS B_Column"
'
'        .OutputInExistedBookByUnitTestMode strSQL, "Test_ADO_VirtualTable"
'
'        .CloseCacheOutputBookOfOpenByPresetModeProcess
'    End With
'End Sub
'
'
''''
''''
''''
'Private Sub msubSanityTestToConnectToAccDbBySqlInsert()
'
'    Dim strSQL As String, strDbPath As String, objOutputBook As Excel.Workbook
'
'    strDbPath = GetAccDbPathAfterTestingToConnectToAccDbAdoConnectorWithSqlInsert()
'
'    Set objOutputBook = GetWorkbookAndPrepareForUnitTest(mfstrGetTempAccDbSqlTestOutputBookPath())
'
'
'    With New AccDbAdoSheetExpander
'
'        .SetMsAccessConnection strDbPath
'
'        .DataTableSheetFormattingInterface.RecordBordersType = RecordCellsGrayAll
'
'        strSQL = "INSERT INTO Test_Table VALUES (13, 14, 15, 'Type3');"
'
'        .OutputCommandLogToSpecifiedBook strSQL, mfstrGetTempAccDbSqlTestOutputBookPath()
'
'        strSQL = "INSERT INTO Test_Table VALUES (16, 17, 18, 'Type3');"
'
'        .OutputCommandLogToSpecifiedBook strSQL, mfstrGetTempAccDbSqlTestOutputBookPath()
'
'
'        strSQL = "SELECT * FROM Test_Table"
'
'        .OutputToSpecifiedBook strSQL, mfstrGetTempAccDbSqlTestOutputBookPath(), "SqlAllResults", AppendingSqlQueryLogsInSheetCellsOnIndependentSheet
'
'        .CloseConnection
'    End With
'
'    DeleteDummySheetWhenItExists objOutputBook
'End Sub
'
''''
'''' Test for SQL DELETE
''''
'Private Sub msubSanityTestToConnectToAccDbBySqlDelete()
'
'    Dim strSQL As String, strDbPath As String, objOutputBook As Excel.Workbook
'
'    strDbPath = GetAccDbPathAfterTestingToConnectToAccDbAdoConnectorWithSqlInsert()
'
'    Set objOutputBook = GetWorkbookAndPrepareForUnitTest(mfstrGetTempAccDbSqlTestOutputBookPath())
'
'
'    With New AccDbAdoSheetExpander
'
'        .SetMsAccessConnection strDbPath
'
'        .DataTableSheetFormattingInterface.RecordBordersType = RecordCellsGrayAll
'
'        strSQL = "DELETE FROM Test_Table WHERE ColText = 'Type2';"
'
'        .OutputCommandLogToSpecifiedBook strSQL, mfstrGetTempAccDbSqlTestOutputBookPath()
'
'        strSQL = "SELECT * FROM Test_Table"
'
'        .OutputToSpecifiedBook strSQL, mfstrGetTempAccDbSqlTestOutputBookPath(), "SqlAllResults", SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable
'
'        .CloseAll
'    End With
'
'    DeleteDummySheetWhenItExists objOutputBook
'End Sub
'
''''
''''
''''
'Private Sub msubSanityTestToConnectToAccDbBySqlUpdate()
'
'    Dim strSQL As String, strDbPath As String, objOutputBook As Excel.Workbook
'
'    strDbPath = GetAccDbPathAfterTestingToConnectToAccDbAdoConnectorWithSqlInsert()
'
'    Set objOutputBook = GetWorkbookAndPrepareForUnitTest(mfstrGetTempAccDbSqlTestOutputBookPath())
'
'
'    With New AccDbAdoSheetExpander
'
'        .SetMsAccessConnection strDbPath
'
'        .DataTableSheetFormattingInterface.RecordBordersType = RecordCellsGrayAll
'
'        strSQL = "UPDATE Test_Table SET Col1 = 41, Col2 = 42 WHERE Col3 = 12 AND ColText = 'Type2';"
'
'        .OutputCommandLogToSpecifiedBook strSQL, mfstrGetTempAccDbSqlTestOutputBookPath()
'
'        strSQL = "SELECT * FROM Test_Table"
'
'        .OutputToSpecifiedBook strSQL, mfstrGetTempAccDbSqlTestOutputBookPath(), "SqlAllResults", AppendingSqlQueryLogsInSheetCellsOnIndependentSheet
'
'        .CloseAll
'    End With
'
'    DeleteDummySheetWhenItExists objOutputBook
'End Sub
'
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
''''
''''
''''
'Private Function mfstrGetTempAccDbSqlTestOutputBookPath() As String
'
'    mfstrGetTempAccDbSqlTestOutputBookPath = mfstrGetTempAccDbSqlTestOutputBookDir() & "\SqlTestResultBookOfAccDbAdoSheetExpander.xlsx"
'End Function
'
'
''''
''''
''''
'Private Function mfstrGetTempAccDbSqlTestOutputBookDir() As String
'
'    Dim strDir As String
'
'    strDir = GetCurrentBookOutputDir() & "\SQLTests\FromAccDbConnect"
'
'    ForceToCreateDirectory strDir
'
'    mfstrGetTempAccDbSqlTestOutputBookDir = strDir
'End Function
'
'
'''--VBA_Code_File--<PgSqlOdbcSheetExpander.cls>--
'VERSION 1.0 CLASS
'BEGIN
'  MultiUse = -1  'True
'END
'Attribute VB_Name = "PgSqlOdbcSheetExpander"
'Attribute VB_GlobalNameSpace = False
'Attribute VB_Creatable = False
'Attribute VB_PredeclaredId = False
'Attribute VB_Exposed = False
''
''   output SQL result into Excel.Worksheet after connecting a PostgreSQL database by ADO ODBC interface
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on both ADO and Excel
''       Dependent on both PsqlOdbc driver at this Windows and an PostgreSQL database server in either a network somewhere or the local-host
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Wed, 26/Jul/2023    Kalmclaeyd Tarclanus    Started to redesign
''
'
'Option Explicit
'
'
''///////////////////////////////////////////////
''/// Interface implementations
''///////////////////////////////////////////////
'Implements IOutputBookPath
'
''///////////////////////////////////////////////
''/// Constants
''///////////////////////////////////////////////
'#If Win64 Then
'
'    Private Const mstrDriverName As String = "{PostgreSQL Unicode(x64)}"
'#Else
'    Private Const mstrDriverName As String = "{PostgreSQL Unicode}"
'#End If
'
'Private Const mintPostgreSQLDefaultPortNumber As Long = 5432
'
''///////////////////////////////////////////////
''/// Declarations
''///////////////////////////////////////////////
'Private mobjConnector As PgSqlOdbcConnector: Private mitfConnector As IADOConnector
'
'Private mobjAdoRecordsetSheetExpander As AdoRSetSheetExpander
'
'
''///////////////////////////////////////////////
''/// Event Handlers
''///////////////////////////////////////////////
'Private Sub Class_Initialize()
'
'    Set mobjConnector = New PgSqlOdbcConnector
'
'    Set mitfConnector = mobjConnector
'
'    Set mobjAdoRecordsetSheetExpander = New AdoRSetSheetExpander
'
'    With mobjAdoRecordsetSheetExpander
'
'        Set .ADOConnectorInterface = mobjConnector
'
'        Set .OutputBookPathInterface = Me
'
'        .DataTableSheetFormattingInterface.FieldTitleInteriorType = ColumnNameInteriorOfPostgreSQL
'    End With
'End Sub
'
''''
''''
''''
'Private Sub Class_Terminate()
'
'    mobjAdoRecordsetSheetExpander.CloseCacheOutputBookOfOpenByPresetModeProcess
'
'    Debug.Print "class AccDbAdoSheetExpander terminate -> " & mobjAdoRecordsetSheetExpander Is Nothing
'
'    Me.CloseConnection
'
'    Set mobjAdoRecordsetSheetExpander = Nothing
'
'    Set mobjConnector = Nothing
'End Sub
'
'
''///////////////////////////////////////////////
''/// Properties - connoted ADO connector
''///////////////////////////////////////////////
'Public Property Get SQLExecutionResult() As SQLResult
'
'    Set SQLExecutionResult = mobjConnector.SQLExecutionResult
'End Property
'
'Public Property Get IsSqlExecutionFailed() As Boolean
'
'    IsSqlExecutionFailed = mobjConnector.IsSqlExecutionFailed
'End Property
'
'Public Property Let AllowToRecordSQLLog(ByVal vblnAllowToRecord As Boolean)
'
'    mobjConnector.AllowToRecordSQLLog = vblnAllowToRecord
'End Property
'Public Property Get AllowToRecordSQLLog() As Boolean
'
'    AllowToRecordSQLLog = mobjConnector.AllowToRecordSQLLog
'End Property
'
'
'Public Property Get CurrentSheet() As Excel.Worksheet
'
'    Set CurrentSheet = mobjAdoRecordsetSheetExpander.IExpandAdoRecordsetOnSheet_CurrentSheet
'End Property
'
'
'Public Property Get CurrentBook() As Excel.Workbook
'
'    If Not Me.CurrentSheet Is Nothing Then
'
'        Set CurrentBook = Me.CurrentSheet.Parent
'    Else
'        Set CurrentBook = Nothing
'    End If
'End Property
'
'
''///////////////////////////////////////////////
''/// Properties
''///////////////////////////////////////////////
''''
''''
''''
'Public Property Get ADOConnectorInterface() As IADOConnector
'
'    Set ADOConnectorInterface = mitfConnector
'End Property
'
''''
''''
''''
'Public Property Get DataTableSheetFormattingInterface() As IDataTableSheetFormatter
'
'    Set DataTableSheetFormattingInterface = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface
'End Property
'
'
''///////////////////////////////////////////////
''/// Properties - connoted ADOConnectionSetting
''///////////////////////////////////////////////
'Public Property Get ConnectSetting() As ADOConnectionSetting
'
'    Set ConnectSetting = mobjConnector.ConnectSetting
'End Property
'
''''
'''' unit is second. [s]
''''
'Public Property Let CommandTimeout(ByVal vintTimeout As Long)
'
'    mobjConnector.CommandTimeout = vintTimeout
'End Property
'Public Property Get CommandTimeout() As Long
'
'    CommandTimeout = mobjConnector.CommandTimeout
'End Property
'
''///////////////////////////////////////////////
''/// Implemented operations
''///////////////////////////////////////////////
''''
''''
''''
'Private Function IOutputBookPath_GetPreservedOutputBookPath() As String
'
'    IOutputBookPath_GetPreservedOutputBookPath = GetPgSqlConnectingPrearrangedLogBookPath()
'End Function
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''**---------------------------------------------
''** set ADO connection string all parameters directly
''**---------------------------------------------
''''
'''' set ODBC connection parameters by a registered Data Source Name (DSN)
''''
'''' <Argument>vstrDSN: Input</Argument>
'''' <Argument>vstrUID: Input</Argument>
'''' <Argument>vstrPWD: Input</Argument>
'Public Sub SetODBCParametersByDSN(ByVal vstrDSN As String, _
'        ByVal vstrUID As String, _
'        ByVal vstrPWD As String)
'
'    mobjConnector.SetODBCParametersByDSN vstrDSN, vstrUID, vstrPWD
'End Sub
'
''''
'''' set ODBC connection parameters without Data Source Name (DSN) setting
''''
'''' <Argument>vstrDriverName: Input</Argument>
'''' <Argument>vstrServerHostName: Input</Argument>
'''' <Argument>vstrDatabaseName: Input</Argument>
'''' <Argument>vstrUserid: Input</Argument>
'''' <Argument>vstrPassword: Input</Argument>
'''' <Argument>vintPortNumber: Input</Argument>
'Public Sub SetODBCParametersWithoutDSN(ByVal vstrDriverName As String, _
'        ByVal vstrServerHostName As String, _
'        ByVal vstrDatabaseName As String, _
'        ByVal vstrUserid As String, _
'        ByVal vstrPassword As String, _
'        Optional ByVal vintPortNumber As Long = mintPostgreSQLDefaultPortNumber)
'
'
'    mobjConnector.SetODBCParametersWithoutDSN vstrDriverName, _
'            vstrServerHostName, _
'            vstrDatabaseName, _
'            vstrUserid, _
'            vstrPassword, _
'            vintPortNumber
'End Sub
'
'
''**---------------------------------------------
''** set ADO connection string parameters by a User-form interface
''**---------------------------------------------
''''
'''' Using a User-form, set DSN connection
''''
'''' <Argument>vstrSettingKeyName: Input - This is to be written in Windows-registry</Argument>
'''' <Argument>vstrDSN: Input</Argument>
'''' <Argument>vstrUID: Input</Argument>
'''' <Argument>vstrPWD: Input</Argument>
'Public Function IsAdoConnectionContinuedAfterOdbcConnectionDsnParametersByForm(ByVal vstrSettingKeyName As String, _
'        Optional ByVal vstrDSN As String = "", _
'        Optional ByVal vstrUID As String = "", _
'        Optional ByVal vstrPWD As String = "") As Boolean
'
'
'    IsAdoConnectionContinuedAfterOdbcConnectionDsnParametersByForm = mobjConnector.IsAdoConnectionContinuedAfterOdbcConnectionDsnParametersByForm(vstrSettingKeyName, _
'            vstrDSN, _
'            vstrUID, _
'            vstrPWD)
'End Function
'
''''
'''' Using a User-form, set DSN connection
''''
'''' <Argument>vstrSettingKeyName: Input - This is to be written in Windows-registry</Argument>
'''' <Argument>vstrDriverName: Input</Argument>
'''' <Argument>vstrServerHostName: Input</Argument>
'''' <Argument>vstrDatabaseName: Input</Argument>
'''' <Argument>vstrUserid: Input</Argument>
'''' <Argument>vstrPassword: Input</Argument>
'''' <Argument>vintPortNumber: Input</Argument>
'Public Function IsAdoConnectionContinuedAfterOdbcConnectionDsnlessParametersByForm(ByVal vstrSettingKeyName As String, _
'        Optional ByVal vstrDriverName As String = mstrDriverName, _
'        Optional ByVal vstrServerHostName As String = "", _
'        Optional ByVal vstrDatabaseName As String = "", _
'        Optional ByVal vstrUserid As String = "", _
'        Optional ByVal vstrPassword As String = "", _
'        Optional ByVal vintPortNumber As Long = mintPostgreSQLDefaultPortNumber) As Boolean
'
'
'    IsAdoConnectionContinuedAfterOdbcConnectionDsnlessParametersByForm = mobjConnector.IsAdoConnectionContinuedAfterOdbcConnectionDsnlessParametersByForm(vstrSettingKeyName, _
'            vstrDriverName, _
'            vstrServerHostName, _
'            vstrDatabaseName, _
'            vstrUserid, _
'            vstrPassword, _
'            vintPortNumber)
'End Function
'
''''
'''' Close ADO connection
''''
'Public Sub CloseConnection()
'
'    mobjConnector.CloseConnection
'End Sub
'
''''
''''
''''
'Public Sub CloseCacheOutputBookOfOpenByPresetModeProcess()
'
'    mobjAdoRecordsetSheetExpander.CloseCacheOutputBookOfOpenByPresetModeProcess
'End Sub
'
''''
''''
''''
'Public Sub CloseAll()
'
'    CloseConnection
'
'    CloseCacheOutputBookOfOpenByPresetModeProcess
'End Sub
'
''**---------------------------------------------
''** Output Recordset and logs to cells on sheet
''**---------------------------------------------
''''
'''' query SQL and output result to Excel sheet
''''
'''' <Argument>vstrSQL: Input</Argument>
'''' <Argument>vobjOutputSheet: Input-Output</Argument>
'''' <Argument>venmExecutedAdoSqlQueryLogPositionType: Input</Argument>
'''' <Argument>venmShowUpAdoErrorOptionFlag: Input</Argument>
'''' <Argument>vblnChangeVisibleExcelApplicationIfItIsImvisible: Input</Argument>
'Public Sub OutputToSheetFrom(ByVal vstrSQL As String, _
'        ByVal vobjOutputSheet As Excel.Worksheet, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputToSheetFrom vstrSQL, _
'            vobjOutputSheet, _
'            venmExecutedAdoSqlQueryLogPositionType, _
'            venmShowUpAdoErrorOptionFlag, _
'            Nothing, _
'            vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
'''' query SQL and output result to the Existed Excel sheet
''''
'''' <Argument>vstrSQL: Input</Argument>
'''' <Argument>vstrNewSheetName: Input</Argument>
'''' <Argument>venmExecutedAdoSqlQueryLogPositionType: Input</Argument>
'''' <Argument>venmShowUpAdoErrorOptionFlag: Input</Argument>
'''' <Argument>vblnChangeVisibleExcelApplicationIfItIsImvisible: Input</Argument>
'Public Sub OutputInExistedBook(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputInExistedBook vstrSQL, _
'            vstrNewSheetName, _
'            venmExecutedAdoSqlQueryLogPositionType, _
'            venmShowUpAdoErrorOptionFlag, _
'            Nothing, _
'            vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
''''
''''
'''' <Argument>vstrSQL: Input</Argument>
'''' <Argument>vstrNewSheetName: Input</Argument>
'''' <Argument>venmExecutedAdoSqlQueryLogPositionType: Input</Argument>
'''' <Argument>venmShowUpAdoErrorOptionFlag: Input</Argument>
'''' <Argument>vblnChangeVisibleExcelApplicationIfItIsImvisible: Input</Argument>
'Public Sub OutputInAlreadyExistedBook(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputInAlreadyExistedBook vstrSQL, _
'            vstrNewSheetName, _
'            venmExecutedAdoSqlQueryLogPositionType, _
'            venmShowUpAdoErrorOptionFlag, _
'            Nothing, _
'            vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
''''
''''
'''' <Argument>vstrSQL: Input</Argument>
'''' <Argument>vstrNewSheetName: Input</Argument>
'''' <Argument>venmExecutedAdoSqlQueryLogPositionType: Input</Argument>
'''' <Argument>venmShowUpAdoErrorOptionFlag: Input</Argument>
'''' <Argument>vblnChangeVisibleExcelApplicationIfItIsImvisible: Input</Argument>
'Public Sub OutputInExistedBookByUnitTestMode(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputInExistedBookByUnitTestMode vstrSQL, _
'            vstrNewSheetName, _
'            venmExecutedAdoSqlQueryLogPositionType, _
'            venmShowUpAdoErrorOptionFlag, _
'            Nothing, _
'            vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'''' <Argument>vstrSQL: Input</Argument>
'''' <Argument>vstrOutputBookPath: Input</Argument>
'''' <Argument>vstrNewSheetName: Input</Argument>
'''' <Argument>venmExecutedAdoSqlQueryLogPositionType: Input</Argument>
'''' <Argument>venmShowUpAdoErrorOptionFlag: Input</Argument>
'''' <Argument>vblnChangeVisibleExcelApplicationIfItIsImvisible: Input</Argument>
'Public Sub OutputToSpecifiedBook(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputToSpecifiedBook vstrSQL, _
'            vstrOutputBookPath, _
'            vstrNewSheetName, _
'            venmExecutedAdoSqlQueryLogPositionType, _
'            venmShowUpAdoErrorOptionFlag, _
'            Nothing, _
'            vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
''''
''''
'''' <Argument>vstrSQL: Input</Argument>
'''' <Argument>vstrOutputBookPath: Input</Argument>
'''' <Argument>vstrNewSheetName: Input</Argument>
'''' <Argument>venmExecutedAdoSqlQueryLogPositionType: Input</Argument>
'''' <Argument>venmShowUpAdoErrorOptionFlag: Input</Argument>
'''' <Argument>vblnChangeVisibleExcelApplicationIfItIsImvisible: Input</Argument>
'Public Sub OutputToSpecifiedBookByUnitTestMode(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputToSpecifiedBookByUnitTestMode vstrSQL, _
'            vstrOutputBookPath, _
'            vstrNewSheetName, _
'            venmExecutedAdoSqlQueryLogPositionType, _
'            venmShowUpAdoErrorOptionFlag, _
'            Nothing, _
'            vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
'''' query SQL and output result to the already specified Excel sheet
''''
'''' <Argument>vstrSQL: Input</Argument>
'''' <Argument>vstrNewSheetName: Input</Argument>
'''' <Argument>venmExecutedAdoSqlQueryLogPositionType: Input</Argument>
'''' <Argument>venmShowUpAdoErrorOptionFlag: Input</Argument>
'''' <Argument>vblnChangeVisibleExcelApplicationIfItIsImvisible: Input</Argument>
'Public Sub OutputToAlreadySpecifiedBook(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputToAlreadySpecifiedBook vstrSQL, _
'            vstrNewSheetName, _
'            venmExecutedAdoSqlQueryLogPositionType, _
'            venmShowUpAdoErrorOptionFlag, _
'            Nothing, _
'            vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
''''
''''
'''' <Argument>vstrOutputBookPath: Input</Argument>
'''' <Argument>venmOutputRSetExcelBookOpenModeProcessFlag: Input</Argument>
'Public Sub SetOutputExcelBookPath(ByVal vstrOutputBookPath As String, _
'        Optional ByVal venmOutputRSetExcelBookOpenModeProcessFlag As OutputRSetExcelBookOpenModeProcessFlag = OutputRSetExcelBookOpenModeProcessFlag.OpenOutputBookByUnitTestMode)
'
'    mobjAdoRecordsetSheetExpander.SetOutputExcelBookPath vstrOutputBookPath, _
'            venmOutputRSetExcelBookOpenModeProcessFlag
'End Sub
'
''**---------------------------------------------
''** Excecute SQL command (UPDATE, INSERT, DELETE)
''**---------------------------------------------
''''
'''' command SQL and output result-log to Excel sheet
''''
'''' <Argument>vstrSQL: Input</Argument>
'''' <Argument>vobjOutputSqlCommandLogSheet: Input</Argument>
'''' <Argument>venmShowUpAdoErrorOptionFlag: Input</Argument>
'''' <Argument>vblnChangeVisibleExcelApplicationIfItIsImvisible: Input</Argument>
'Public Sub OutputCommandLogToSheetFrom(ByVal vstrSQL As String, _
'        ByVal vobjOutputSqlCommandLogSheet As Excel.Worksheet, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogToSheetFrom _
'            vstrSQL, _
'            vobjOutputSqlCommandLogSheet, _
'            venmShowUpAdoErrorOptionFlag, _
'            Nothing, _
'            vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'''' <Argument>vstrSQL: Input</Argument>
'''' <Argument>vstrOutputBookPath: Input</Argument>
'''' <Argument>venmShowUpAdoErrorOptionFlag: Input</Argument>
'''' <Argument>vstrNewSqlCommandLogSheetName: Input</Argument>
'''' <Argument>vblnChangeVisibleExcelApplicationIfItIsImvisible: Input</Argument>
'Public Sub OutputCommandLogToSpecifiedBook(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogToSpecifiedBook vstrSQL, _
'            vstrOutputBookPath, _
'            venmShowUpAdoErrorOptionFlag, _
'            Nothing, _
'            vstrNewSqlCommandLogSheetName, _
'            vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'''' <Argument>vstrSQL: Input</Argument>
'''' <Argument>venmShowUpAdoErrorOptionFlag: Input</Argument>
'''' <Argument>vstrNewSqlCommandLogSheetName: Input</Argument>
'''' <Argument>vblnChangeVisibleExcelApplicationIfItIsImvisible: Input</Argument>
'Public Sub OutputCommandLogToAlreadySpecifiedBook(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogToAlreadySpecifiedBook vstrSQL, _
'            venmShowUpAdoErrorOptionFlag, _
'            Nothing, _
'            vstrNewSqlCommandLogSheetName, _
'            vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'''' <Argument>vstrSQL: Input</Argument>
'''' <Argument>venmShowUpAdoErrorOptionFlag: Input</Argument>
'''' <Argument>vstrNewSqlCommandLogSheetName: Input</Argument>
'''' <Argument>vblnChangeVisibleExcelApplicationIfItIsImvisible: Input</Argument>
'Public Sub OutputCommandLogInAlreadyExistedBook(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogInAlreadyExistedBook vstrSQL, _
'            venmShowUpAdoErrorOptionFlag, _
'            Nothing, _
'            vstrNewSqlCommandLogSheetName, _
'            vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'''' <Argument>vstrSQL: Input</Argument>
'''' <Argument>vstrOutputBookPath</Argument>
'''' <Argument>venmShowUpAdoErrorOptionFlag: Input</Argument>
'''' <Argument>vstrNewSqlCommandLogSheetName: Input</Argument>
'''' <Argument>vblnChangeVisibleExcelApplicationIfItIsImvisible: Input</Argument>
'Public Sub OutputCommandLogToSpecifiedBookByUnitTestMode(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogToSpecifiedBookByUnitTestMode vstrSQL, _
'            vstrOutputBookPath, _
'            venmShowUpAdoErrorOptionFlag, _
'            Nothing, _
'            vstrNewSqlCommandLogSheetName, _
'            vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
'''' query SQL and output result to the prepared Excel sheet
''''
'''' <Argument>vstrSQL: Input</Argument>
'''' <Argument>venmShowUpAdoErrorOptionFlag: Input</Argument>
'''' <Argument>vstrNewSqlCommandLogSheetName: Input</Argument>
'''' <Argument>vblnChangeVisibleExcelApplicationIfItIsImvisible: Input</Argument>
'Public Sub OutputCommandLogInExistedBookByUnitTestMode(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogInExistedBookByUnitTestMode vstrSQL, _
'            venmShowUpAdoErrorOptionFlag, _
'            Nothing, _
'            vstrNewSqlCommandLogSheetName, _
'            vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
'
'
'
'
'''--VBA_Code_File--<SqlUTfPgSqlOdbcSheetExpander.bas>--
'Attribute VB_Name = "SqlUTfPgSqlOdbcSheetExpander"
''
''   Sanity test to output SQL results into Excel sheets for a PostgreSQL database using ADO ODBC
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on both ADO and Excel
''       Dependent on both PsqlOdbc driver at this Windows and an PostgreSQL database server in either a network somewhere or the local-host
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Wed, 26/Jul/2023    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Constants
''///////////////////////////////////////////////
'Private Const mstrConStrInfoSettingKeyNameOfDSN As String = "TestPgSqlDSNKey"
'
'Private Const mstrConStrInfoSettingKeyNameOfDSNless As String = "TestPgSqlDSNlessKey"
'
'
'
'#If Win64 Then
'
'    Private Const mstrDriverName As String = "{PostgreSQL Unicode(x64)}"
'#Else
'    Private Const mstrDriverName As String = "{PostgreSQL Unicode}"
'#End If
'
'Private Const mintPostgreSQLDefaultPortNumber As Long = 5432
'
''///////////////////////////////////////////////
''/// Control VBE windows
''///////////////////////////////////////////////
''**---------------------------------------------
''** Open relative VBE code-panes
''**---------------------------------------------
'Private Sub OpenVBECodePaneRelativeToSqlUTfPgSqlOdbcSheetExpander()
'
'    ' Focusing on this procdure, please press [F5] key to start this. Therefore this scope is private.
'
'    On Error Resume Next ' If the 'ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma' isn't included in the VBA project, the following causes no syntax error.
'
'    Application.Run "ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma", _
'            "SqlUTfPgSqlOdbcSheetExpander,SqlAdoConnectOdbcUTfPgSql,SqlAdoConnectOdbcPTfPgSql,PgSqlOdbcConnector,PgSqlOdbcSheetExpander,PgSqlCommonPath,PgSqlDSNlessTest"
'End Sub
'
'
''///////////////////////////////////////////////
''/// Sanity tests
''///////////////////////////////////////////////
''**---------------------------------------------
''** PgSqlOdbcSheetExpander
''**---------------------------------------------
''''
'''' Create an in-line table from the only SELECT sql inside
''''
'Private Sub msubSanityTestToConnectByVirtualTablePgSqlOdbcSheetExpander()
'
'    Dim strSQL As String
'
'    With New PgSqlOdbcSheetExpander
'
'        ' for default connection setting soon after the PostgreSQL typcal configuration is installed in this local computer.
'
'        .SetODBCParametersWithoutDSN mstrDriverName, "LocalHost", "postgres", "postgres", "postgres"
'
'        .AllowToRecordSQLLog = True
'
'        strSQL = "select 'ABC' as A_Column, 'DEF' as B_Column"
'
'        .OutputInExistedBookByUnitTestMode strSQL, "Test_ADO_VirtualTable"
'
'        .CloseAll
'    End With
'End Sub
'
'
''///////////////////////////////////////////////
''/// Sanity tests
''///////////////////////////////////////////////
''**---------------------------------------------
''** Use IsAdoConnectionContinuedAfterOdbcConnectionDsnlessParametersByForm interface for ADO connecting
''**---------------------------------------------
''''
'''' get PostgreSQL version by SQL
''''
'Private Sub msubSanityTestToOutputSqlVersionOnSheetAfterSqlVersionOfPostgreSQL()
'
'    Dim strSQL As String
'
'    With New PgSqlOdbcSheetExpander
'
'        ' for default connection setting soon after the PostgreSQL typcal configuration is installed in this local computer.
'
'        If .IsAdoConnectionContinuedAfterOdbcConnectionDsnlessParametersByForm(mstrConStrInfoSettingKeyNameOfDSNless, , "LocalHost", "postgres", "postgres") Then
'
'            .AllowToRecordSQLLog = True
'
'            strSQL = "select * from version()"
'
'            .OutputInExistedBookByUnitTestMode strSQL, "PgSQL_version"
'
'            .CloseCacheOutputBookOfOpenByPresetModeProcess
'        Else
'            Debug.Print "The ADO connection information inputs have canceled."
'        End If
'    End With
'End Sub
'
''**---------------------------------------------
''** Use presetting object for ADO connecting
''**---------------------------------------------
''''
'''' SQL insert test with output Excel sheet log
''''
'Private Sub msubSanityTestToOutputSqlResultOnSheetAfterPostgreSQLInsertExecution()
'
'    Dim strSQL As String, objAdoSheetExpander As PgSqlOdbcSheetExpander
'
'    ' You must implement 'GetCurrentThisSytemTestingPgSqlOdbcSheetExpander' function before you execute the following.
'
'    Set objAdoSheetExpander = mfobjGetPgSqlOdbcSheetExpanderAfterPrepareTestTableOfPostgreSQLTestingDataBaseForSqlTests()
'
'    If Not objAdoSheetExpander Is Nothing Then
'
'        With objAdoSheetExpander
'
'            strSQL = "insert into Test_Table values (13, 15, 16, 'Type3')"
'
'            .OutputCommandLogToSpecifiedBook strSQL, GetPgSqlConnectingPrearrangedLogBookPath()
'
'            .ADOConnectorInterface.CommitTransaction
'
'            strSQL = "select * from Test_Table"
'
'            .OutputToSpecifiedBook strSQL, GetPgSqlConnectingPrearrangedLogBookPath(), "SqlResults", SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable
'
'            .CloseAll
'        End With
'    Else
'        Debug.Print "Attention!!!: the 'GetCurrentThisSytemTestingPgSqlOdbcConnector' function hasn't been implemented yet."
'    End If
'End Sub
'
''''
'''' SQL delete test with output Excel sheet log
''''
'Private Sub msubSanityTestToOutputSqlResultOnSheetAfterPostgreSQLDeleteExecution()
'
'    Dim strSQL As String, objAdoSheetExpander As PgSqlOdbcSheetExpander, strOutputBookPath As String, strDir As String
'
'    ' You must implement 'GetCurrentThisSytemTestingPgSqlOdbcSheetExpander' function before you execute the following.
'
'    strDir = GetCurrentBookOutputDir() & "\SQLTests\FromPgSqlConnect"
'
'    ForceToCreateDirectory strDir
'
'    strOutputBookPath = strDir & "\AdoConnectToPgSqlTest01.xlsx"
'
'    Set objAdoSheetExpander = mfobjGetPgSqlOdbcSheetExpanderAfterPrepareTestTableOfPostgreSQLTestingDataBaseForSqlTests()
'
'    If Not objAdoSheetExpander Is Nothing Then
'
'        With objAdoSheetExpander
'
'            strSQL = "delete from Test_Table where ColText = 'Type2'"
'
'            .OutputCommandLogToSpecifiedBookByUnitTestMode strSQL, strOutputBookPath
'
'            .ADOConnectorInterface.CommitTransaction
'
'            strSQL = "select * from Test_Table"
'
'            .OutputToSpecifiedBookByUnitTestMode strSQL, strOutputBookPath, "SqlResults", AppendingSqlQueryLogsInSheetCellsOnIndependentSheet
'
'            .CloseAll
'        End With
'    Else
'        Debug.Print "Attention!!!: the 'GetCurrentThisSytemTestingPgSqlOdbcConnector' function hasn't been implemented yet."
'    End If
'End Sub
'
''''
'''' SQL update test with output Excel sheet log
''''
'Private Sub msubSanityTestToOutputSqlResultOnSheetAfterPostgreSQLUpdateExecution()
'
'    Dim strSQL As String, objAdoSheetExpander As PgSqlOdbcSheetExpander
'
'    ' You must implement 'GetCurrentThisSytemTestingPgSqlOdbcSheetExpander' function before you execute the following.
'
'    Set objAdoSheetExpander = mfobjGetPgSqlOdbcSheetExpanderAfterPrepareTestTableOfPostgreSQLTestingDataBaseForSqlTests()
'
'    If Not objAdoSheetExpander Is Nothing Then
'
'        With objAdoSheetExpander
'
'            strSQL = "update Test_Table set Col1 = 51, Col2 = 52 where Col3 = 9 AND ColText = 'Type2'"
'
'
'            .OutputCommandLogInExistedBookByUnitTestMode strSQL
'
'            .ADOConnectorInterface.CommitTransaction
'
'            strSQL = "select * from Test_Table"
'
'            .OutputInExistedBookByUnitTestMode strSQL, "SqlResults", AppendingSqlQueryLogsInSheetCellsOnIndependentSheet
'
'            .CloseAll
'        End With
'    Else
'        Debug.Print "Attention!!!: the 'GetCurrentThisSytemTestingPgSqlOdbcConnector' function hasn't been implemented yet."
'    End If
'End Sub
'
''''
'''' SQL insert test with output Excel sheet log
''''
'Private Sub msubSanityTestToOutputSqlResultOnSheetAfterPostgreSQLPluralInsertExecution()
'
'    Dim strSQL As String, objAdoSheetExpander As PgSqlOdbcSheetExpander
'
'    Dim i As Long
'    Dim int01 As Long, int02 As Long, int03 As Long, intType As Long
'
'
'    ' You must implement 'GetCurrentThisSytemTestingPgSqlOdbcSheetExpander' function before you execute the following.
'
'    Set objAdoSheetExpander = mfobjGetPgSqlOdbcSheetExpanderAfterPrepareTestTableOfPostgreSQLTestingDataBaseForSqlTests()
'
'    If Not objAdoSheetExpander Is Nothing Then
'
'        With objAdoSheetExpander
'
'            For i = 1 To 15
'
'                ' sample values
'
'                int01 = 3 * (i + 4)
'
'                int02 = int01 + 1
'
'                int03 = int02 + 1
'
'                intType = i + 5
'
'                strSQL = "insert into Test_Table values (" & CStr(int01) & ", " & CStr(int02) & ", " & CStr(int03) & ", 'Type" & CStr(intType) & "')"
'
'                .OutputCommandLogInExistedBookByUnitTestMode strSQL
'            Next
'
'            .ADOConnectorInterface.CommitTransaction
'
'            strSQL = "select * from Test_Table"
'
'            .OutputInExistedBookByUnitTestMode strSQL, "SqlResults", SqlQueryLogsByAutoShapesOnSheetWithOutputtingRecordsetDataTable
'
'            .CloseAll
'        End With
'    Else
'        Debug.Print "Attention!!!: the 'GetCurrentThisSytemTestingPgSqlOdbcConnector' function hasn't been implemented yet."
'    End If
'End Sub
'
'
''''
'''' SQL insert test with output Excel sheet log
''''
'Private Sub msubSanityTestToOutputSqlResultOnSheetAfterPostgreSQLPluralInsertExecutionWithOtherLoggingOption()
'
'    Dim strSQL As String, objAdoSheetExpander As PgSqlOdbcSheetExpander
'
'    Dim i As Long
'    Dim int01 As Long, int02 As Long, int03 As Long, intType As Long
'
'
'    ' You must implement 'GetCurrentThisSytemTestingPgSqlOdbcSheetExpander' function before you execute the following.
'
'    Set objAdoSheetExpander = mfobjGetPgSqlOdbcSheetExpanderAfterPrepareTestTableOfPostgreSQLTestingDataBaseForSqlTests()
'
'    If Not objAdoSheetExpander Is Nothing Then
'
'        With objAdoSheetExpander
'
'            For i = 1 To 15
'
'                ' sample values
'
'                int01 = 3 * (i + 4)
'
'                int02 = int01 + 1
'
'                int03 = int02 + 1
'
'                intType = i + 5
'
'                strSQL = "insert into Test_Table values (" & CStr(int01) & ", " & CStr(int02) & ", " & CStr(int03) & ", 'Type" & CStr(intType) & "')"
'
'                .OutputCommandLogInExistedBookByUnitTestMode strSQL, NoControlToShowUpAdoSqlErrorFlag, "TestingSqlCommandLog"
'            Next
'
'            .ADOConnectorInterface.CommitTransaction
'
'            strSQL = "select * from Test_Table"
'
'            .OutputInExistedBookByUnitTestMode strSQL, "SqlResults", SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable
'
'            .CloseCacheOutputBookOfOpenByPresetModeProcess
'        End With
'    Else
'        Debug.Print "Attention!!!: the 'GetCurrentThisSytemTestingPgSqlOdbcConnector' function hasn't been implemented yet."
'    End If
'End Sub
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
''''
''''
''''
'Private Function mfobjGetPgSqlOdbcSheetExpanderAfterPrepareTestTableOfPostgreSQLTestingDataBaseForSqlTests() As PgSqlOdbcSheetExpander
'
'    Dim strSQL As String, objAdoSheetExpander As PgSqlOdbcSheetExpander
'
'    ' You must implement 'GetCurrentThisSytemTestingPgSqlOdbcSheetExpander' function before you execute the following.
'
'    Set objAdoSheetExpander = mfobjGetPgSqlOdbcSheetExpanderWhichIncludesConnectionInfo()
'
'    If Not objAdoSheetExpander Is Nothing Then
'
'        PrepareTestTableOfPostgreSQLTestingDataBaseForSqlTests objAdoSheetExpander.ADOConnectorInterface
'
'        objAdoSheetExpander.DataTableSheetFormattingInterface.RecordBordersType = RecordCellsGrayAll
'    Else
'        Debug.Print "Attention!!!: the 'GetCurrentThisSytemTestingPgSqlOdbcSheetExpander' function hasn't been implemented yet."
'    End If
'
'    Set mfobjGetPgSqlOdbcSheetExpanderAfterPrepareTestTableOfPostgreSQLTestingDataBaseForSqlTests = objAdoSheetExpander
'End Function
'
'
''''
''''
''''
'Private Function mfobjGetPgSqlOdbcSheetExpanderWhichIncludesConnectionInfo() As PgSqlOdbcSheetExpander
'
'    Dim objPgSqlOdbcSheetExpander As PgSqlOdbcSheetExpander
'
'    On Error Resume Next
'
'    Set objPgSqlOdbcSheetExpander = Application.Run("GetCurrentThisSytemTestingPgSqlOdbcSheetExpander")
'
'    On Error GoTo 0
'
'    Set mfobjGetPgSqlOdbcSheetExpanderWhichIncludesConnectionInfo = objPgSqlOdbcSheetExpander
'End Function
'
'''--VBA_Code_File--<OracleOdbcSheetExpander.cls>--
'VERSION 1.0 CLASS
'BEGIN
'  MultiUse = -1  'True
'END
'Attribute VB_Name = "OracleOdbcSheetExpander"
'Attribute VB_GlobalNameSpace = False
'Attribute VB_Creatable = False
'Attribute VB_PredeclaredId = False
'Attribute VB_Exposed = False
''
''   output SQL results into Excel worksheets after connecting a Oracle database by ADO ODBC interface
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on both ADO and Excel
''       Dependent on both Oracle ODBC driver at this Windows and an Oracle database server in a network somewhere
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   Modification History:
''       Wed, 26/Jul/2023    Kalmclaeyd Tarclanus    Initial creation
''
'
'Option Explicit
'
'
''///////////////////////////////////////////////
''/// Interface implementations
''///////////////////////////////////////////////
'Implements IOutputBookPath
'
''///////////////////////////////////////////////
''/// Constants
''///////////////////////////////////////////////
'#If Win64 Then
'
'    Private Const mstrDriverName As String = "{Oracle in OraClient11g_home1}"
'#Else
'    Private Const mstrDriverName As String = "{Oracle in OraClient10g_home1}"
'#End If
'
'Private Const mintOracleDefaultPortNumber As Long = 1521
'
'Private Const mintPostgreSQLDefaultPortNumber As Long = 5432
'
''///////////////////////////////////////////////
''/// Declarations
''///////////////////////////////////////////////
'Private mobjConnector As OracleOdbcConnector
'
'Private mobjAdoRecordsetSheetExpander As AdoRSetSheetExpander
'
'
''///////////////////////////////////////////////
''/// Event Handlers
''///////////////////////////////////////////////
'Private Sub Class_Initialize()
'
'    Set mobjConnector = New OracleOdbcConnector
'
'    Set mobjAdoRecordsetSheetExpander = New AdoRSetSheetExpander
'
'    With mobjAdoRecordsetSheetExpander
'
'        Set .ADOConnectorInterface = mobjConnector
'
'        Set .OutputBookPathInterface = Me
'
'        .DataTableSheetFormattingInterface.FieldTitleInteriorType = ColumnNameInteriorOfDefaultTypeSQL
'    End With
'End Sub
'
''''
''''
''''
'Private Sub Class_Terminate()
'
'    mobjAdoRecordsetSheetExpander.CloseCacheOutputBookOfOpenByPresetModeProcess
'
'    Debug.Print "class AccDbAdoSheetExpander terminate -> " & mobjAdoRecordsetSheetExpander Is Nothing
'
'    Me.CloseConnection
'
'    Set mobjAdoRecordsetSheetExpander = Nothing
'
'    Set mobjConnector = Nothing
'End Sub
'
'
''///////////////////////////////////////////////
''/// Properties - connoted ADO connector
''///////////////////////////////////////////////
'Public Property Get SQLExecutionResult() As SQLResult
'
'    Set SQLExecutionResult = mobjConnector.SQLExecutionResult
'End Property
'
'Public Property Get IsSqlExecutionFailed() As Boolean
'
'    IsSqlExecutionFailed = mobjConnector.IsSqlExecutionFailed
'End Property
'
'Public Property Let AllowToRecordSQLLog(ByVal vblnAllowToRecord As Boolean)
'
'    mobjConnector.AllowToRecordSQLLog = vblnAllowToRecord
'End Property
'Public Property Get AllowToRecordSQLLog() As Boolean
'
'    AllowToRecordSQLLog = mobjConnector.AllowToRecordSQLLog
'End Property
'
'
'Public Property Get CurrentSheet() As Excel.Worksheet
'
'    Set CurrentSheet = mobjAdoRecordsetSheetExpander.IExpandAdoRecordsetOnSheet_CurrentSheet
'End Property
'
'
'Public Property Get CurrentBook() As Excel.Workbook
'
'    If Not Me.CurrentSheet Is Nothing Then
'
'        Set CurrentBook = Me.CurrentSheet.Parent
'    Else
'        Set CurrentBook = Nothing
'    End If
'End Property
'
'
''///////////////////////////////////////////////
''/// Properties
''///////////////////////////////////////////////
''''
''''
''''
'Public Property Get DataTableSheetFormattingInterface() As IDataTableSheetFormatter
'
'    Set DataTableSheetFormattingInterface = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface
'End Property
'
'
''///////////////////////////////////////////////
''/// Properties - connoted ADOConnectionSetting
''///////////////////////////////////////////////
'Public Property Get ConnectSetting() As ADOConnectionSetting
'
'    Set ConnectSetting = mobjConnector.ConnectSetting
'End Property
'
''''
'''' unit is second. [s]
''''
'Public Property Let CommandTimeout(ByVal vintTimeout As Long)
'
'    mobjConnector.CommandTimeout = vintTimeout
'End Property
'Public Property Get CommandTimeout() As Long
'
'    CommandTimeout = mobjConnector.CommandTimeout
'End Property
'
''///////////////////////////////////////////////
''/// Implemented operations
''///////////////////////////////////////////////
''''
''''
''''
'Private Function IOutputBookPath_GetPreservedOutputBookPath() As String
'
'    IOutputBookPath_GetPreservedOutputBookPath = GetOracleConnectingPrearrangedLogBookPath()
'End Function
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''**---------------------------------------------
''** set ADO connection string all parameters directly
''**---------------------------------------------
''''
'''' set ODBC connection parameters by a registered Data Source Name (DSN)
''''
'Public Sub SetODBCParametersByDSN(ByVal vstrDSN As String, ByVal vstrUID As String, ByVal vstrPWD As String)
'
'    mobjConnector.SetODBCParametersByDSN vstrDSN, vstrUID, vstrPWD
'End Sub
'
''''
'''' set ODBC connection parameters without Data Source Name (DSN) setting
''''
'Public Sub SetODBCParametersWithoutDSN(ByVal vstrDriverName As String, ByVal vstrServerHostName As String, ByVal vstrNetworkServiceName As String, ByVal vstrUserid As String, ByVal vstrPassword As String, Optional ByVal vintPortNumber As Long = mintOracleDefaultPortNumber)
'
'    mobjConnector.SetODBCParametersWithoutDSN vstrDriverName, vstrServerHostName, vstrNetworkServiceName, vstrUserid, vstrPassword, vintPortNumber
'End Sub
'
'
''**---------------------------------------------
''** set ADO connection string parameters by a User-form interface
''**---------------------------------------------
''''
'''' Using a User-form, set DSN connection
''''
'''' <Argument>vstrSettingKeyName: Input - This is to be written in Windows-registry</Argument>
'''' <Argument>vstrDSN: Input</Argument>
'''' <Argument>vstrUID: Input</Argument>
'''' <Argument>vstrPWD: Input</Argument>
'Public Function IsAdoConnectionContinuedAfterOdbcConnectionDsnParametersByForm(ByVal vstrSettingKeyName As String, _
'        Optional ByVal vstrDSN As String = "", _
'        Optional ByVal vstrUID As String = "", _
'        Optional ByVal vstrPWD As String = "") As Boolean
'
'
'    IsAdoConnectionContinuedAfterOdbcConnectionDsnParametersByForm = mobjConnector.IsAdoConnectionContinuedAfterOdbcConnectionDsnParametersByForm(vstrSettingKeyName, vstrDSN, vstrUID, vstrPWD)
'End Function
'
''''
'''' Using a User-form, set DSN connection
''''
'''' <Argument>vstrSettingKeyName: Input - This is to be written in Windows-registry</Argument>
'''' <Argument>vstrDriverName: Input</Argument>
'''' <Argument>vstrServerHostName: Input</Argument>
'''' <Argument>vstrNetworkServiceName: Input</Argument>
'''' <Argument>vstrUserid: Input</Argument>
'''' <Argument>vstrPassword: Input</Argument>
'''' <Argument>vintPortNumber: Input</Argument>
'Public Function IsAdoConnectionContinuedAfterOdbcConnectionDsnlessParametersByForm(ByVal vstrSettingKeyName As String, _
'        Optional ByVal vstrDriverName As String = mstrDriverName, _
'        Optional ByVal vstrServerHostName As String = "", _
'        Optional ByVal vstrNetworkServiceName As String = "", _
'        Optional ByVal vstrUserid As String = "", _
'        Optional ByVal vstrPassword As String = "", _
'        Optional ByVal vintPortNumber As Long = mintPostgreSQLDefaultPortNumber) As Boolean
'
'
'    IsAdoConnectionContinuedAfterOdbcConnectionDsnlessParametersByForm = mobjConnector.IsAdoConnectionContinuedAfterOdbcConnectionDsnlessParametersByForm(vstrSettingKeyName, vstrDriverName, vstrServerHostName, vstrNetworkServiceName, vstrUserid, vstrPassword, vintPortNumber)
'End Function
'
''''
'''' Close ADO connection
''''
'Public Sub CloseConnection()
'
'    mobjConnector.CloseConnection
'End Sub
'
'
''''
''''
''''
'Public Sub CloseCacheOutputBookOfOpenByPresetModeProcess()
'
'    mobjAdoRecordsetSheetExpander.CloseCacheOutputBookOfOpenByPresetModeProcess
'End Sub
'
''''
''''
''''
'Public Sub CloseAll()
'
'    CloseConnection
'
'    CloseCacheOutputBookOfOpenByPresetModeProcess
'End Sub
'
''**---------------------------------------------
''** Output Recordset and logs to cells on sheet
''**---------------------------------------------
''''
'''' query SQL and output result to Excel sheet
''''
'''' <Argument>vstrSQL: Input</Argument>
'''' <Argument>vobjOutputSheet: Input-Output</Argument>
'''' <Argument>venmExecutedAdoSqlQueryLogPositionType: Input</Argument>
'''' <Argument>venmShowUpAdoErrorOptionFlag: Input</Argument>
'''' <Argument>vblnChangeVisibleExcelApplicationIfItIsImvisible: Input</Argument>
'Public Sub OutputToSheetFrom(ByVal vstrSQL As String, _
'        ByVal vobjOutputSheet As Excel.Worksheet, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputToSheetFrom vstrSQL, _
'            vobjOutputSheet, _
'            venmExecutedAdoSqlQueryLogPositionType, _
'            venmShowUpAdoErrorOptionFlag, _
'            Nothing, _
'            vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
'''' query SQL and output result to the Existed Excel sheet
''''
'''' <Argument>vstrSQL: Input</Argument>
'''' <Argument>vstrNewSheetName: Input</Argument>
'''' <Argument>venmExecutedAdoSqlQueryLogPositionType: Input</Argument>
'''' <Argument>venmShowUpAdoErrorOptionFlag: Input</Argument>
'''' <Argument>vblnChangeVisibleExcelApplicationIfItIsImvisible: Input</Argument>
'Public Sub OutputInExistedBook(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputInExistedBook vstrSQL, _
'            vstrNewSheetName, _
'            venmExecutedAdoSqlQueryLogPositionType, _
'            venmShowUpAdoErrorOptionFlag, _
'            Nothing, _
'            vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
''''
''''
'''' <Argument>vstrSQL: Input</Argument>
'''' <Argument>vstrNewSheetName: Input</Argument>
'''' <Argument>venmExecutedAdoSqlQueryLogPositionType: Input</Argument>
'''' <Argument>venmShowUpAdoErrorOptionFlag: Input</Argument>
'''' <Argument>vblnChangeVisibleExcelApplicationIfItIsImvisible: Input</Argument>
'Public Sub OutputInAlreadyExistedBook(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputInAlreadyExistedBook vstrSQL, _
'            vstrNewSheetName, _
'            venmExecutedAdoSqlQueryLogPositionType, _
'            venmShowUpAdoErrorOptionFlag, _
'            Nothing, _
'            vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
''''
''''
'''' <Argument>vstrSQL: Input</Argument>
'''' <Argument>vstrNewSheetName: Input</Argument>
'''' <Argument>venmExecutedAdoSqlQueryLogPositionType: Input</Argument>
'''' <Argument>venmShowUpAdoErrorOptionFlag: Input</Argument>
'''' <Argument>vblnChangeVisibleExcelApplicationIfItIsImvisible: Input</Argument>
'Public Sub OutputInExistedBookByUnitTestMode(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputInExistedBookByUnitTestMode vstrSQL, _
'            vstrNewSheetName, _
'            venmExecutedAdoSqlQueryLogPositionType, _
'            venmShowUpAdoErrorOptionFlag, _
'            Nothing, _
'            vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'''' <Argument>vstrSQL: Input</Argument>
'''' <Argument>vstrOutputBookPath: Input</Argument>
'''' <Argument>vstrNewSheetName: Input</Argument>
'''' <Argument>venmExecutedAdoSqlQueryLogPositionType: Input</Argument>
'''' <Argument>venmShowUpAdoErrorOptionFlag: Input</Argument>
'''' <Argument>vblnChangeVisibleExcelApplicationIfItIsImvisible: Input</Argument>
'Public Sub OutputToSpecifiedBook(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputToSpecifiedBook vstrSQL, _
'            vstrOutputBookPath, _
'            vstrNewSheetName, _
'            venmExecutedAdoSqlQueryLogPositionType, _
'            venmShowUpAdoErrorOptionFlag, _
'            Nothing, _
'            vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
''''
''''
'''' <Argument>vstrSQL: Input</Argument>
'''' <Argument>vstrOutputBookPath: Input</Argument>
'''' <Argument>vstrNewSheetName: Input</Argument>
'''' <Argument>venmExecutedAdoSqlQueryLogPositionType: Input</Argument>
'''' <Argument>venmShowUpAdoErrorOptionFlag: Input</Argument>
'''' <Argument>vblnChangeVisibleExcelApplicationIfItIsImvisible: Input</Argument>
'Public Sub OutputToSpecifiedBookByUnitTestMode(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputToSpecifiedBookByUnitTestMode vstrSQL, _
'            vstrOutputBookPath, _
'            vstrNewSheetName, _
'            venmExecutedAdoSqlQueryLogPositionType, _
'            venmShowUpAdoErrorOptionFlag, _
'            Nothing, _
'            vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
'''' query SQL and output result to the already specified Excel sheet
''''
'''' <Argument>vstrSQL: Input</Argument>
'''' <Argument>vstrNewSheetName: Input</Argument>
'''' <Argument>venmExecutedAdoSqlQueryLogPositionType: Input</Argument>
'''' <Argument>venmShowUpAdoErrorOptionFlag: Input</Argument>
'''' <Argument>vblnChangeVisibleExcelApplicationIfItIsImvisible: Input</Argument>
'Public Sub OutputToAlreadySpecifiedBook(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputToAlreadySpecifiedBook vstrSQL, _
'            vstrNewSheetName, _
'            venmExecutedAdoSqlQueryLogPositionType, _
'            venmShowUpAdoErrorOptionFlag, _
'            Nothing, _
'            vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
''''
''''
'''' <Argument>vstrOutputBookPath: Input</Argument>
'''' <Argument>venmOutputRSetExcelBookOpenModeProcessFlag: Input</Argument>
'Public Sub SetOutputExcelBookPath(ByVal vstrOutputBookPath As String, _
'        Optional ByVal venmOutputRSetExcelBookOpenModeProcessFlag As OutputRSetExcelBookOpenModeProcessFlag = OutputRSetExcelBookOpenModeProcessFlag.OpenOutputBookByUnitTestMode)
'
'    mobjAdoRecordsetSheetExpander.SetOutputExcelBookPath vstrOutputBookPath, _
'            venmOutputRSetExcelBookOpenModeProcessFlag
'End Sub
'
''**---------------------------------------------
''** Excecute SQL command (UPDATE, INSERT, DELETE)
''**---------------------------------------------
''''
'''' command SQL and output result-log to Excel sheet
''''
'''' <Argument>vstrSQL: Input</Argument>
'''' <Argument>vobjOutputSqlCommandLogSheet: Input</Argument>
'''' <Argument>venmShowUpAdoErrorOptionFlag: Input</Argument>
'''' <Argument>vblnChangeVisibleExcelApplicationIfItIsImvisible: Input</Argument>
'Public Sub OutputCommandLogToSheetFrom(ByVal vstrSQL As String, _
'        ByVal vobjOutputSqlCommandLogSheet As Excel.Worksheet, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogToSheetFrom _
'            vstrSQL, _
'            vobjOutputSqlCommandLogSheet, _
'            venmShowUpAdoErrorOptionFlag, _
'            Nothing, _
'            vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'''' <Argument>vstrSQL: Input</Argument>
'''' <Argument>vstrOutputBookPath: Input</Argument>
'''' <Argument>venmShowUpAdoErrorOptionFlag: Input</Argument>
'''' <Argument>vstrNewSqlCommandLogSheetName: Input</Argument>
'''' <Argument>vblnChangeVisibleExcelApplicationIfItIsImvisible: Input</Argument>
'Public Sub OutputCommandLogToSpecifiedBook(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogToSpecifiedBook vstrSQL, _
'            vstrOutputBookPath, _
'            venmShowUpAdoErrorOptionFlag, _
'            Nothing, _
'            vstrNewSqlCommandLogSheetName, _
'            vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'''' <Argument>vstrSQL: Input</Argument>
'''' <Argument>venmShowUpAdoErrorOptionFlag: Input</Argument>
'''' <Argument>vstrNewSqlCommandLogSheetName: Input</Argument>
'''' <Argument>vblnChangeVisibleExcelApplicationIfItIsImvisible: Input</Argument>
'Public Sub OutputCommandLogToAlreadySpecifiedBook(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogToAlreadySpecifiedBook vstrSQL, _
'            venmShowUpAdoErrorOptionFlag, _
'            Nothing, _
'            vstrNewSqlCommandLogSheetName, _
'            vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'''' <Argument>vstrSQL: Input</Argument>
'''' <Argument>venmShowUpAdoErrorOptionFlag: Input</Argument>
'''' <Argument>vstrNewSqlCommandLogSheetName: Input</Argument>
'''' <Argument>vblnChangeVisibleExcelApplicationIfItIsImvisible: Input</Argument>
'Public Sub OutputCommandLogInAlreadyExistedBook(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogInAlreadyExistedBook vstrSQL, _
'            venmShowUpAdoErrorOptionFlag, _
'            Nothing, _
'            vstrNewSqlCommandLogSheetName, _
'            vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'''' <Argument>vstrSQL: Input</Argument>
'''' <Argument>vstrOutputBookPath</Argument>
'''' <Argument>venmShowUpAdoErrorOptionFlag: Input</Argument>
'''' <Argument>vstrNewSqlCommandLogSheetName: Input</Argument>
'''' <Argument>vblnChangeVisibleExcelApplicationIfItIsImvisible: Input</Argument>
'Public Sub OutputCommandLogToSpecifiedBookByUnitTestMode(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogToSpecifiedBookByUnitTestMode vstrSQL, _
'            vstrOutputBookPath, _
'            venmShowUpAdoErrorOptionFlag, _
'            Nothing, _
'            vstrNewSqlCommandLogSheetName, _
'            vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
'''' query SQL and output result to the prepared Excel sheet
''''
'''' <Argument>vstrSQL: Input</Argument>
'''' <Argument>venmShowUpAdoErrorOptionFlag: Input</Argument>
'''' <Argument>vstrNewSqlCommandLogSheetName: Input</Argument>
'''' <Argument>vblnChangeVisibleExcelApplicationIfItIsImvisible: Input</Argument>
'Public Sub OutputCommandLogInExistedBookByUnitTestMode(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogInExistedBookByUnitTestMode vstrSQL, _
'            venmShowUpAdoErrorOptionFlag, _
'            Nothing, _
'            vstrNewSqlCommandLogSheetName, _
'            vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
'
'
'
'''--VBA_Code_File--<SqLiteAdoSheetExpander.cls>--
'VERSION 1.0 CLASS
'BEGIN
'  MultiUse = -1  'True
'END
'Attribute VB_Name = "SqLiteAdoSheetExpander"
'Attribute VB_GlobalNameSpace = False
'Attribute VB_Creatable = False
'Attribute VB_PredeclaredId = False
'Attribute VB_Exposed = False
''
''   output SQL result into Excel.Worksheet after connecting a SQLite database file by ADO ODBC interface
''   Probably, this capsuled class design will not be needed for almost VBA developers, because it is too complicated.
''   However the VBA programming interfaces will be united.
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on both ADO and Excel
''       Dependent on an installed SQLite ODBC driver
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Wed, 26/Jul/2023    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
'
''///////////////////////////////////////////////
''/// Interface implementations
''///////////////////////////////////////////////
'Implements IOutputBookPath
'
''///////////////////////////////////////////////
''/// Declarations
''///////////////////////////////////////////////
'Private mobjConnector As SqLiteAdoConnector: Private mitfConnector As IADOConnector
'
'Private mobjAdoRecordsetSheetExpander As AdoRSetSheetExpander
'
'
''///////////////////////////////////////////////
''/// Event Handlers
''///////////////////////////////////////////////
'Private Sub Class_Initialize()
'
'    Set mobjConnector = New SqLiteAdoConnector
'
'    Set mitfConnector = mobjConnector
'
'    Set mobjAdoRecordsetSheetExpander = New AdoRSetSheetExpander
'
'    With mobjAdoRecordsetSheetExpander
'
'        Set .ADOConnectorInterface = mobjConnector
'
'        Set .OutputBookPathInterface = Me
'
'        .DataTableSheetFormattingInterface.FieldTitleInteriorType = ColumnNameInteriorOfDefaultTypeSQLByDarkBrownFontLightSilverBgMeiryo
'    End With
'End Sub
'
''''
''''
''''
'Private Sub Class_Terminate()
'
'    mobjAdoRecordsetSheetExpander.CloseCacheOutputBookOfOpenByPresetModeProcess
'
'    Debug.Print "class AccDbAdoSheetExpander terminate -> " & mobjAdoRecordsetSheetExpander Is Nothing
'
'    Me.CloseConnection
'
'    Set mobjAdoRecordsetSheetExpander = Nothing
'
'    Set mobjConnector = Nothing
'End Sub
'
'
''///////////////////////////////////////////////
''/// Properties - connoted ADO connector
''///////////////////////////////////////////////
'Public Property Get SQLExecutionResult() As SQLResult
'
'    Set SQLExecutionResult = mobjConnector.SQLExecutionResult
'End Property
'
'Public Property Get IsSqlExecutionFailed() As Boolean
'
'    IsSqlExecutionFailed = mobjConnector.IsSqlExecutionFailed
'End Property
'
'Public Property Let AllowToRecordSQLLog(ByVal vblnAllowToRecord As Boolean)
'
'    mobjConnector.AllowToRecordSQLLog = vblnAllowToRecord
'End Property
'Public Property Get AllowToRecordSQLLog() As Boolean
'
'    AllowToRecordSQLLog = mobjConnector.AllowToRecordSQLLog
'End Property
'
'
'Public Property Get CurrentSheet() As Excel.Worksheet
'
'    Set CurrentSheet = mobjAdoRecordsetSheetExpander.IExpandAdoRecordsetOnSheet_CurrentSheet
'End Property
'
'
'Public Property Get CurrentBook() As Excel.Workbook
'
'    If Not Me.CurrentSheet Is Nothing Then
'
'        Set CurrentBook = Me.CurrentSheet.Parent
'    Else
'        Set CurrentBook = Nothing
'    End If
'End Property
'
'
''///////////////////////////////////////////////
''/// Properties
''///////////////////////////////////////////////
''''
''''
''''
'Public Property Get ADOConnectorInterface() As IADOConnector
'
'    Set ADOConnectorInterface = mitfConnector
'End Property
'
''''
''''
''''
'Public Property Get DataTableSheetFormattingInterface() As IDataTableSheetFormatter
'
'    Set DataTableSheetFormattingInterface = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface
'End Property
'
'
''///////////////////////////////////////////////
''/// Properties - connoted ADOConnectionSetting
''///////////////////////////////////////////////
'Public Property Get ConnectSetting() As ADOConnectionSetting
'
'    Set ConnectSetting = mobjConnector.ConnectSetting
'End Property
'
''''
'''' unit is second. [s]
''''
'Public Property Let CommandTimeout(ByVal vintTimeout As Long)
'
'    mobjConnector.CommandTimeout = vintTimeout
'End Property
'Public Property Get CommandTimeout() As Long
'
'    CommandTimeout = mobjConnector.CommandTimeout
'End Property
'
'
''///////////////////////////////////////////////
''/// Properties - SQLite database connection by an ODBC driver
''///////////////////////////////////////////////
'Public Property Get ConnectingFilePath() As String
'
'    ConnectingFilePath = mobjConnector.ConnectingFilePath
'End Property
'
'
''///////////////////////////////////////////////
''/// Implemented operations
''///////////////////////////////////////////////
''''
''''
''''
'Private Function IOutputBookPath_GetPreservedOutputBookPath() As String
'
'    IOutputBookPath_GetPreservedOutputBookPath = GetSqLiteConnectingPrearrangedLogBookPath()
'End Function
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''**---------------------------------------------
''** set ADO connection string all parameters directly
''**---------------------------------------------
''''
''''
''''
'Public Sub SetODBCConnectionByDSN(ByVal vstrDSN As String)
'
'    mobjConnector.SetODBCConnectionByDSN vstrDSN
'End Sub
'
''''
''''
''''
'Public Sub SetODBCParametersWithoutDSN(ByVal vstrDbFilePath As String)
'
'    mobjConnector.SetODBCParametersWithoutDSN vstrDbFilePath
'End Sub
'
'
''**---------------------------------------------
''** set ADO connection string parameters by a User-form interface
''**---------------------------------------------
''''
'''' Using a User-form, set DSN connection
''''
'''' <Argument>vstrSettingKeyName: Input - This is to be written in Windows-registry</Argument>
'''' <Argument>vstrDSN: Input</Argument>
'Public Function IsAdoConnectionContinuedAfterOdbcConnectionDsnParametersByForm(ByVal vstrSettingKeyName As String, _
'        Optional ByVal vstrDSN As String = "") As Boolean
'
'
'    IsAdoConnectionContinuedAfterOdbcConnectionDsnParametersByForm = mobjConnector.IsAdoConnectionContinuedAfterOdbcConnectionDsnParametersByForm(vstrSettingKeyName, vstrDSN)
'End Function
'
''''
'''' Using a User-form, set DSN connection
''''
'''' <Argument>vstrSettingKeyName: Input - This is to be written in Windows-registry</Argument>
'''' <Argument>vstrDbFilePath: Input</Argument>
'''' <Argument>vstrDriverName: Input</Argument>
'Public Function IsAdoConnectionContinuedAfterOdbcConnectionDsnlessParametersByForm(ByVal vstrSettingKeyName As String, _
'        Optional ByVal vstrDbFilePath As String = "", _
'        Optional ByVal vstrDriverName As String = "") As Boolean
'
'
'    IsAdoConnectionContinuedAfterOdbcConnectionDsnlessParametersByForm = mobjConnector.IsAdoConnectionContinuedAfterOdbcConnectionDsnlessParametersByForm(vstrSettingKeyName, vstrDbFilePath, vstrDriverName)
'End Function
'
'
''''
'''' Close ADO connection
''''
'Public Sub CloseConnection()
'
'    mobjConnector.CloseConnection
'End Sub
'
''''
''''
''''
'Public Sub CloseCacheOutputBookOfOpenByPresetModeProcess()
'
'    mobjAdoRecordsetSheetExpander.CloseCacheOutputBookOfOpenByPresetModeProcess
'End Sub
'
''''
''''
''''
'Public Sub CloseAll()
'
'    CloseConnection
'
'    CloseCacheOutputBookOfOpenByPresetModeProcess
'End Sub
'
''**---------------------------------------------
''** Output Recordset and logs to cells on sheet
''**---------------------------------------------
''''
'''' query SQL and output result to Excel sheet
''''
'Public Sub OutputToSheetFrom(ByVal vstrSQL As String, _
'        ByVal vobjOutputSheet As Excel.Worksheet, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'    mobjAdoRecordsetSheetExpander.OutputToSheetFrom vstrSQL, vobjOutputSheet, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedSqLitePathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
'''' query SQL and output result to the Existed Excel sheet
''''
'Public Sub OutputInExistedBook(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputInExistedBook vstrSQL, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedSqLitePathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
'Public Sub OutputInAlreadyExistedBook(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputInAlreadyExistedBook vstrSQL, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedSqLitePathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
''''
''''
'Public Sub OutputInExistedBookByUnitTestMode(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputInExistedBookByUnitTestMode vstrSQL, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedSqLitePathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputToSpecifiedBook(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputToSpecifiedBook vstrSQL, vstrOutputBookPath, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedSqLitePathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
'Public Sub OutputToSpecifiedBookByUnitTestMode(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'    mobjAdoRecordsetSheetExpander.OutputToSpecifiedBookByUnitTestMode vstrSQL, vstrOutputBookPath, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedSqLitePathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
'''' query SQL and output result to the already specified Excel sheet
''''
'Public Sub OutputToAlreadySpecifiedBook(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'    mobjAdoRecordsetSheetExpander.OutputToAlreadySpecifiedBook vstrSQL, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedSqLitePathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
''''
''''
'Public Sub SetOutputExcelBookPath(ByVal vstrOutputBookPath As String, _
'        Optional ByVal venmOutputRSetExcelBookOpenModeProcessFlag As OutputRSetExcelBookOpenModeProcessFlag = OutputRSetExcelBookOpenModeProcessFlag.OpenOutputBookByUnitTestMode, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag)
'
'    mobjAdoRecordsetSheetExpander.SetOutputExcelBookPath vstrOutputBookPath, venmOutputRSetExcelBookOpenModeProcessFlag
'End Sub
'
''**---------------------------------------------
''** Excecute SQL command (UPDATE, INSERT, DELETE)
''**---------------------------------------------
''''
'''' command SQL and output result-log to Excel sheet
''''
'Public Sub OutputCommandLogToSheetFrom(ByVal vstrSQL As String, _
'        ByVal vobjOutputSqlCommandLogSheet As Excel.Worksheet, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogToSheetFrom vstrSQL, vobjOutputSqlCommandLogSheet, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedSqLitePathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputCommandLogToSpecifiedBook(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogToSpecifiedBook vstrSQL, vstrOutputBookPath, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedSqLitePathInsertText(), vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputCommandLogToAlreadySpecifiedBook(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogToAlreadySpecifiedBook vstrSQL, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedSqLitePathInsertText(), vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputCommandLogInAlreadyExistedBook(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogInAlreadyExistedBook vstrSQL, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedSqLitePathInsertText(), vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputCommandLogToSpecifiedBookByUnitTestMode(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogToSpecifiedBookByUnitTestMode vstrSQL, vstrOutputBookPath, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedSqLitePathInsertText(), vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
'''' query SQL and output result to the prepared Excel sheet
''''
'Public Sub OutputCommandLogInExistedBookByUnitTestMode(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogInExistedBookByUnitTestMode vstrSQL, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedSqLitePathInsertText(), vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
''''
''''
''''
'Private Function mfobjGetConnectedSqLitePathInsertText() As Collection
'
'    Dim objInsertTexts As Collection
'
'    Set objInsertTexts = New Collection
'
'    With mobjConnector
'
'        objInsertTexts.Add "<File Name> " & GetFileNameFromPathByVbaDir(.ConnectingFilePath) & ", <Directory> " & GetParentDirectoryPathByVbaDir(.ConnectingFilePath)
'    End With
'
'    Set mfobjGetConnectedSqLitePathInsertText = objInsertTexts
'End Function
'
'
'''--VBA_Code_File--<SqlUTfSqLiteOdbcSheetExpander.bas>--
'Attribute VB_Name = "SqlUTfSqLiteOdbcSheetExpander"
''
''   Sanity test to output SQL results into Excel sheets for a SQLite database using ADO ODBC
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on both ADO and Excel
''       Dependent on an installed SQLite ODBC driver
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Wed, 26/Jul/2023    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Constants
''///////////////////////////////////////////////
'Private Const mstrTestTableName As String = "Test_Table"
'
'
''///////////////////////////////////////////////
''/// Control VBE windows
''///////////////////////////////////////////////
''**---------------------------------------------
''** Open relative VBE code-panes
''**---------------------------------------------
'Private Sub OpenVBECodePaneRelativeToSQLUTfSqLiteConnector()
'
'    ' Focusing on this procdure, please press [F5] key to start this. Therefore this scope is private.
'
'    On Error Resume Next ' If the 'ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma' isn't included in the VBA project, the following causes no syntax error.
'
'    Application.Run "ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma", "SqLiteAdoConnector,SqLiteAdoSheetExpander,UTfOperateSqLite3,OperateSqLite3,UTfAdoConnectSqLiteDb,ADOConStrOfOdbcSqLite,ADOConStrOfDsnlessSqLite"
'End Sub
'
'
''///////////////////////////////////////////////
''/// Sanity tests
''///////////////////////////////////////////////
''**---------------------------------------------
''** SqLiteAdoSheetExpander
''**---------------------------------------------
''''
'''' Create an in-line table from the only SELECT sql inside
''''
'Private Sub msubSanityTestToConnectByVirtualTableSqLiteAdoSheetExpander()
'
'    Dim strSQL As String
'
'    With New SqLiteAdoSheetExpander
'
'        .SetODBCParametersWithoutDSN PrepareSQLiteDbAfterCreateSampleSQLiteDbIfItDoesntExist("TestDbOfSqLiteAdoSheetExpander.sqlitedb")
'
'        .AllowToRecordSQLLog = True
'
'        'strSQL = "SELECT 1"
'
'        'strSQL = "SELECT 'ABC', 'DEF'"
'
'        strSQL = "SELECT 'ABC' AS A_Column, 'DEF' AS B_Column"
'
'        .OutputInExistedBookByUnitTestMode strSQL, "Test_ADO_VirtualTable", AppendingSqlQueryLogsInSheetCellsOnIndependentSheet
'
'        .CloseAll
'    End With
'End Sub
'
'
''''
'''' Create an in-line table from the only SELECT sql inside
''''
'Private Sub msubSanityTestToConfirmSqLiteVersion()
'
'    Dim strSQL As String
'
'    With New SqLiteAdoSheetExpander
'
'        .SetODBCParametersWithoutDSN PrepareSQLiteDbAfterCreateSampleSQLiteDbIfItDoesntExist("TestDbOfSqLiteAdoSheetExpander.sqlitedb")
'
'        .AllowToRecordSQLLog = True
'
'        strSQL = "SELECT sqlite_version()"
'
'        .OutputInExistedBookByUnitTestMode strSQL, "SqLiteVersion"
'
'        .CloseAll
'    End With
'End Sub
'
''''
''''
''''
'Private Sub msubOpenCmdWithSqLiteDbParentFolder()
'
'    OpenCmdWithSpecifyingBothWorkingDirectoryAndCmdWindowTitleText GetTemporarySqLiteDataBaseDir(), "Test SQLite db"
'End Sub
'
'
''''
''''
''''
'Private Sub msubSanityTestToGetTableList()
'
'    Dim strSQL As String
'
'    With New SqLiteAdoSheetExpander
'
'        With .DataTableSheetFormattingInterface
'
'            .RecordBordersType = RecordCellsGrayAll
'        End With
'
'        .SetODBCParametersWithoutDSN PrepareSQLiteDbAfterCreateSampleSQLiteDbIfItDoesntExist("TestDbOfSqLiteAdoSheetExpander.sqlitedb")
'
'        .AllowToRecordSQLLog = True
'
'
'        strSQL = "SELECT name FROM sqlite_master WHERE type = 'table'"
'
'        .OutputInExistedBookByUnitTestMode strSQL, "SqLiteTableList", SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable
'
'        .CloseAll
'    End With
'End Sub
'
'
''''
''''
''''
'Private Sub msubSanityTestToCreateTable()
'
'    Dim strSQL As String, strDbPath As String
'
'    strDbPath = GetTemporarySqLiteDataBaseDir() & "TestDb02OfSqLiteAdoSheetExpander.sqlitedb"
'
'    If FileExistsByVbaDir(strDbPath) Then VBA.Kill strDbPath
'
'    With New SqLiteAdoSheetExpander
'
'        .SetODBCParametersWithoutDSN strDbPath
'
'        .AllowToRecordSQLLog = True
'
'        ' strSQL = "CREATE TABLE " & mstrTestTableName & "(Col1, Col2, Col3)"
'
'        strSQL = "CREATE TABLE Test_Table(Col1, Col2, Col3)"
'
'        .OutputCommandLogInExistedBookByUnitTestMode strSQL
'
'        strSQL = "SELECT name FROM sqlite_master WHERE type = 'table'"
'
'        .OutputInExistedBookByUnitTestMode strSQL, "TableList", AppendingSqlQueryLogsInSheetCellsOnIndependentSheet
'
'        .CloseAll
'    End With
'End Sub
'
''''
'''' SQLite database sanity-test for SQL INSERT
''''
'Private Sub msubSanityTestToConnectToSqLiteBySqlInsertTable()
'
'    Dim strSQL As String
'
'    With New SqLiteAdoSheetExpander
'
'        .SetODBCParametersWithoutDSN GetTemporarySqLiteDataBaseDir() & "\" & "TestSimpleDbOfSqLiteAdoSheetExpander.sqlitedb"
'
'        .DataTableSheetFormattingInterface.RecordBordersType = RecordCellsGrayAll
'
'        .AllowToRecordSQLLog = True
'
'        PrepareTestTable4ColumnOfSqLiteTestingDataBaseForSqlTests .ADOConnectorInterface
'
'
'        strSQL = "INSERT INTO " & mstrTestTableName & " VALUES (13, 14, 15, 'Type3')"
'
'        .OutputCommandLogToSpecifiedBookByUnitTestMode strSQL, GetSqLiteConnectingPrearrangedLogBookPath()
'
'
'
'        strSQL = "SELECT * FROM Test_Table"
'
'        .OutputToSpecifiedBookByUnitTestMode strSQL, GetSqLiteConnectingPrearrangedLogBookPath(), "SqlResult", SqlQueryLogsByAutoShapesOnSheetWithOutputtingRecordsetDataTable
'
'        strSQL = "SELECT name FROM sqlite_master WHERE type = 'table'"
'
'        .OutputToSpecifiedBookByUnitTestMode strSQL, GetSqLiteConnectingPrearrangedLogBookPath(), "TableList", SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable
'
'        .CloseAll
'    End With
'End Sub
'
''''
'''' SQLite database sanity-test for SQL DELETE
''''
'Private Sub msubSanityTestToConnectToSqLiteBySqlDeleteTable()
'
'    Dim strSQL As String, strDbPath As String
'
'    strDbPath = GetTemporarySqLiteDataBaseDir() & "\" & "TestSimpleDbOfSqLiteAdoSheetExpander.sqlitedb"
'
'    If FileExistsByVbaDir(strDbPath) Then VBA.Kill strDbPath
'
'    With New SqLiteAdoSheetExpander
'
'        .SetODBCParametersWithoutDSN strDbPath
'
'        .DataTableSheetFormattingInterface.RecordBordersType = RecordCellsGrayAll
'
'        .AllowToRecordSQLLog = True
'
'        PrepareTestTable4ColumnOfSqLiteTestingDataBaseForSqlTests .ADOConnectorInterface
'
'
'        strSQL = "DELETE FROM " & mstrTestTableName & " WHERE ColText = 'Type1'"
'
'        .OutputCommandLogInExistedBookByUnitTestMode strSQL
'
'
'
'        strSQL = "SELECT * FROM Test_Table"
'
'        .OutputInExistedBookByUnitTestMode strSQL, "SqlResult", AppendingSqlQueryLogsInSheetCellsOnIndependentSheet
'
'        .CloseAll
'    End With
'End Sub
'
'
''''
'''' SQLite database sanity-test for SQL UPDATE
''''
'Private Sub msubSanityTestToConnectToSqLiteBySqlUpdateTable()
'
'    Dim strSQL As String, strDbPath As String
'
'    strDbPath = GetTemporarySqLiteDataBaseDir() & "\" & "TestSimpleDbOfSqLiteAdoSheetExpander.sqlitedb"
'
'    If FileExistsByVbaDir(strDbPath) Then VBA.Kill strDbPath
'
'    With New SqLiteAdoSheetExpander
'
'        .SetODBCParametersWithoutDSN strDbPath
'
'        .DataTableSheetFormattingInterface.RecordBordersType = RecordCellsGrayAll
'
'        .AllowToRecordSQLLog = True
'
'        PrepareTestTable4ColumnOfSqLiteTestingDataBaseForSqlTests .ADOConnectorInterface
'
'
'        strSQL = "UPDATE " & mstrTestTableName & " SET Col1 = 31, Col2 = 32 WHERE Col3 = 9 AND ColText = 'Type2'"
'
'        .OutputCommandLogToSpecifiedBook strSQL, GetSqLiteConnectingPrearrangedLogBookPath(), "SqlUpdate", SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable
'
'
'
'        strSQL = "SELECT * FROM Test_Table"
'
'        .OutputToSpecifiedBook strSQL, GetSqLiteConnectingPrearrangedLogBookPath(), "SqlResult", SqlQueryLogsByAutoShapesOnSheetWithOutputtingRecordsetDataTable
'
'        .CloseCacheOutputBookOfOpenByPresetModeProcess
'    End With
'End Sub
'''--VBA_Code_File--<EnumerateMsWords.bas>--
'Attribute VB_Name = "EnumerateMsWords"
''
''   Detect all Word.Document which includes the outside process of Word.Application
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on both Word and Windows OS
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Sat, 11/Aug/2023    Kalmclaeyd Tarclanus    Initial creation
''
'
'Option Explicit
'
'
''///////////////////////////////////////////////
''/// Windows API Declarations
''///////////////////////////////////////////////
'#If VBA7 Then
'    Private Declare PtrSafe Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hwnd As LongPtr, ByVal Msg As LongPtr, ByVal wParam As LongPtr, lParam As Any) As LongPtr
'
'    Private Declare PtrSafe Function IIDFromString Lib "ole32" (lpsz As Any, lpiid As Any) As LongPtr
'
'    Private Declare PtrSafe Function ObjectFromLresult Lib "oleacc" (ByVal lResult As LongPtr, riid As Any, ByVal wParam As LongPtr, ppvObject As Any) As LongPtr
'
'    Private Declare PtrSafe Function IsWindow Lib "user32" (ByVal hwnd As LongPtr) As LongPtr
'
'
'    Private Declare PtrSafe Function SetForegroundWindow Lib "user32" (ByVal hwnd As LongPtr) As Long
'
'    Private Declare PtrSafe Sub Sleep Lib "kernel32" (ByVal ms As LongPtr)
'#Else
'    Private Declare Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hWnd As Long, ByVal Msg As Long, ByVal wParam As Long, lParam As Any) As Long
'
'    Private Declare Function IIDFromString Lib "ole32" (lpsz As Any, lpiid As Any) As Long
'
'    Private Declare Function ObjectFromLresult Lib "oleacc" (ByVal lResult As Long, riid As Any, ByVal wParam As Long, ppvObject As Any) As Long
'
'    Private Declare Function IsWindow Lib "user32" (ByVal hWnd As Long) As Long
'
'
'    Private Declare Function SetForegroundWindow Lib "user32" (ByVal hwnd As Long) As Long
'
'    Private Declare Sub Sleep Lib "kernel32" (ByVal ms As Long)
'#End If
'
'
'Private Const OBJID_NATIVEOM = &HFFFFFFF0
'Private Const OBJID_CLIENT = &HFFFFFFFC
'
'' Interface IDentification
'Private Const IID_IMdcList As String = "{8BD21D23-EC42-11CE-9E0D-00AA006002F3}"
'Private Const IID_IUnknown As String = "{00000000-0000-0000-C000-000000000046}"
'Private Const IID_IDispatch As String = "{00020400-0000-0000-C000-000000000046}"
'
'Private Const WM_GETOBJECT = &H3D&
'
''-----
'' class name of Windows programs
'Private Const WINDOW_CLASS_EXCEL = "XLMAIN" ' Excel Application
'Private Const WINDOW_CLASS_VBE = "wndclass_desked_gsk"  ' Visual Basic Editor
'Private Const WINDOW_CALSS_EXPLORER = "CabinetWClass"
'
'Private Const WINDOW_CLASS_MS_WORD = "OpusApp" ' Word Application
'
'Private Const CHILD_CLASS_EXCELWINDOW = "EXCEL7"
'Private Const CHILD_CLASS_VBE = "VbaWindow"
'
'Private Const CHILD_CLASS_WORDWINDOW = "_WwG"
'
''///////////////////////////////////////////////
''/// Declarations
''///////////////////////////////////////////////
''**---------------------------------------------
''** Filtering by each window title text
''**---------------------------------------------
'Private mobjSearchingRegExp As VBScript_RegExp_55.RegExp
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''**---------------------------------------------
''** Word.Application state control for outside this VBIDE process
''**---------------------------------------------
''''
'''' This can be also used from either Excel application or PowerPoint application
''''
'Public Sub CloseAllProcessMsWordDocumentsExceptMySelfApplication()
'
'    Dim varHWnd As Variant, objApplication As Word.Application, objDocument As Word.Document
'
'    Dim strThisDocumentFullName As String
'
'    Dim varWindow As Variant, objWindow As Word.Window, varOldDisplayAlerts As Variant
'
'#If VBA7 Then
'
'    Dim intHWnd As LongPtr
'#Else
'    Dim intHWnd As Long
'#End If
'
'    strThisDocumentFullName = ""
'
'    If Trim(Replace(LCase(Application.Name), "microsoft", "")) = "word" Then
'
'        strThisDocumentFullName = GetCurrentOfficeFileObject().FullName
'    End If
'
'    With GetExecutedHWndToMsWordWindowsDic()
'
'        For Each varHWnd In .Keys
'
'            intHWnd = varHWnd
'
'            Set objWindow = .Item(varHWnd)
'
'            With objWindow.Application
'
'                Set objDocument = objWindow.Document
'
'                If objDocument.FullName <> strThisDocumentFullName Then
'
'                    varOldDisplayAlerts = .DisplayAlerts
'
'                    .DisplayAlerts = wdAlertsNone
'
'                    objDocument.Saved = True
'
'                    objDocument.Close
'
'                    .DisplayAlerts = varOldDisplayAlerts
'                End If
'
'                If .Documents.Count = 0 Then
'
'                    .Quit
'                End If
'            End With
'        Next
'    End With
'
'
'    If strThisDocumentFullName = "" Then
'
'        On Error Resume Next
'
'        Set objApplication = GetObject(, "Word.Application")
'
'        If Not objApplication Is Nothing Then
'
'            objApplication.Quit
'        End If
'
'        On Error GoTo 0
'    End If
'End Sub
'
'
''''
'''' This can be also used from either Excel application or PowerPoint application
''''
'Public Sub ChangeVisibleAllProcessWordDocumentsExceptMySelfApplication()
'
'    Dim varHWnd As Variant, objApplication As Word.Application, objDocument As Word.Document
'    Dim strThisDocumentFullName As String
'
'    Dim varWindow As Variant, objWindow As Word.Window, varOldDisplayAlerts As Variant
'
'#If VBA7 Then
'
'    Dim intHWnd As LongPtr
'#Else
'    Dim intHWnd As Long
'#End If
'
'    strThisDocumentFullName = ""
'
'    If Trim(Replace(LCase(Application.Name), "microsoft", "")) = "word" Then
'
'        strThisDocumentFullName = GetCurrentOfficeFileObject().FullName
'    End If
'
'    With GetExecutedHWndToMsWordWindowsDic()
'
'        For Each varHWnd In .Keys
'
'            intHWnd = varHWnd
'
'            Set objWindow = .Item(varHWnd)
'
'            With objWindow.Application
'
'                Set objDocument = objWindow.Document
'
'                If objDocument.FullName <> strThisDocumentFullName Then
'
'                    varOldDisplayAlerts = .DisplayAlerts
'
'                    .DisplayAlerts = wdAlertsNone
'
'                    If Not .Visible Then
'
'                        .Visible = True
'                    End If
'
'                    .DisplayAlerts = varOldDisplayAlerts
'                End If
'            End With
'        Next
'    End With
'End Sub
'
''**---------------------------------------------
''** Control specified window order
''**---------------------------------------------
''''
''''
''''
'Public Function IsSpecifiedMicrosoftWordWindowSetForegroundByIncludingWindowCaptionPart(Optional ByVal vstrWindowCaption As String = "", _
'        Optional ByVal vblnEnablingIgnoreCase As Boolean = False) As Boolean
'
'    Dim varWindowCaption As Variant, intRet As Long, blnIsTheWindowFound As Boolean
'
'    Dim varHWnd As Variant
'
'#If VBA7 Then
'    Dim intHWnd As LongPtr
'#Else
'    Dim intHWnd As Long
'#End If
'
'    blnIsTheWindowFound = False
'
'    With GetExecutedHWndToMsWordWindowsDic(vstrWindowTextRegExpPattern:=vstrWindowCaption, _
'            vblnIgnoreCase:=vblnEnablingIgnoreCase)
'
'        If .Count > 0 Then
'
'            For Each varHWnd In .Keys
'
'                intHWnd = varHWnd
'
'                intRet = SetForegroundWindow(intHWnd)
'
'                Interaction.DoEvents
'
'                Sleep 50
'
'                blnIsTheWindowFound = True
'            Next
'        End If
'    End With
'
'    IsSpecifiedMicrosoftWordWindowSetForegroundByIncludingWindowCaptionPart = blnIsTheWindowFound
'End Function
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
''''
'''' get all Word application window object for all Windows processes in execution
''''
'''' <Return>Dictionary(Of LongPtr[HWnd], Word.Window): current the Word window objects</Return>
'Private Function GetExecutedHWndToMsWordWindowsDic(Optional ByVal vblnAllowToExcludeProtectedViewWindowType As Boolean = False, _
'        Optional ByVal vstrWindowTextRegExpPattern As String = "", _
'        Optional ByVal vblnIgnoreCase As Boolean = False) As Scripting.Dictionary
'
'    'get objects as if this enumerate WordProcess().excelWindow().hWnd
'    Dim varHWndArray As Variant, varSafeWordWindowArray As Variant
'    Dim objDic As Scripting.Dictionary, blnIsDisabledHWnd As Boolean, strText As String
'
'#If VBA7 Then
'
'    Dim intHWnd As LongPtr
'#Else
'    Dim intHWnd As Long
'#End If
'
'    Set objDic = New Scripting.Dictionary
'
'    If vstrWindowTextRegExpPattern <> "" Then
'
'        msubSetupSearchingRegExp vstrWindowTextRegExpPattern, vblnIgnoreCase
'    End If
'
'    varHWndArray = EnumFindChildWindows(vstrParentClassName:=WINDOW_CLASS_MS_WORD, vstrChildClassName:=CHILD_CLASS_WORDWINDOW)
'
'    If LBound(varHWndArray) <= UBound(varHWndArray) Then
'
'        ReDim varSafeWordWindowArray(LBound(varHWndArray) To UBound(varHWndArray))
'
'        Dim i As Long, j As Long: j = LBound(varHWndArray) - 1
'
'        For i = LBound(varHWndArray) To UBound(varHWndArray)
'
'            blnIsDisabledHWnd = False
'
'            j = j + 1
'
'            Set varSafeWordWindowArray(j) = GetWordWindowByHWnd(varHWndArray(i))
'
'            ' To be failed to get it, because of the lack ot the authority
'            If varSafeWordWindowArray(j) Is Nothing Then
'
'                j = j - 1
'
'                blnIsDisabledHWnd = True
'            End If
'
'            If vblnAllowToExcludeProtectedViewWindowType Then
'
'                ' Disabled to operate this because of the protected view of the Word window. If you try to operate it, then the Word application process will be aborted.
'
'                If TypeName(varSafeWordWindowArray(j)) = "ProtectedViewWindow" Then
'
'                    j = j - 1
'
'                    blnIsDisabledHWnd = True
'                End If
'            End If
'
''            ' Disabled to operate this because of the protected view of the Word window. If you try to operate it, then the Word application process will be aborted.
''            If varSafeWordWindowArray(j).Application.hwnd = 0 Then j = j - 1
'
'            If Not blnIsDisabledHWnd Then
'
'                Select Case True
'
'                    Case mobjSearchingRegExp Is Nothing, mobjSearchingRegExp.Pattern = ""
'
'                        objDic.Add "" & varHWndArray(i), varSafeWordWindowArray(j)
'                    Case Else
'
'                        ' Need to filter the Word window caption text by the RegExp object pattern
'
'                        intHWnd = varHWndArray(i)
'
'                        strText = GetWindowTextVBAString(intHWnd)
'
'                        If strText <> "" Then
'
'                            If mobjSearchingRegExp.Test(strText) Then
'
'                                objDic.Add "" & varHWndArray(i), varSafeWordWindowArray(j)
'                            End If
'                        Else
'                            objDic.Add "" & varHWndArray(i), varSafeWordWindowArray(j)
'                        End If
'                End Select
'            End If
'        Next
'    End If
'
'    Set GetExecutedHWndToMsWordWindowsDic = objDic
'End Function
'
'
''''
'''' get Word.Window instances from hWnd window-handle
''''
'''' <Argument>hWnd : window handle</Argument>
'''' <Return>Word.Window</Return>
'Private Function GetWordWindowByHWnd(ByRef rvarHWnd As Variant) As Word.Window
'
'#If VBA7 Then
'    Dim intMessageResult As LongPtr, intReturn  As LongPtr
'#Else
'    Dim intMessageResult As Long, intReturn  As Long
'#End If
'
'    Dim intID(0 To 3) As Long, bytID() As Byte
'    Dim objWordWindow As Word.Window
'
'
'    If IsWindow(rvarHWnd) = 0 Then Exit Function
'
'    intMessageResult = SendMessage(rvarHWnd, WM_GETOBJECT, 0, ByVal OBJID_NATIVEOM)
'
'    If intMessageResult Then
'
'        bytID = IID_IDispatch & vbNullChar
'
'        IIDFromString bytID(0), intID(0)
'
'        intReturn = ObjectFromLresult(intMessageResult, intID(0), 0, objWordWindow)
'    End If
'
'    Set GetWordWindowByHWnd = objWordWindow
'End Function
'
''''
'''' set up the standard-module private variable RegExp object
''''
'Private Sub msubSetupSearchingRegExp(ByVal vstrNewPattern As String, Optional ByVal vblnIgnoreCase As Boolean = False)
'
'    SetupRegExpForSearchingTopLevelHWnd mobjSearchingRegExp, vstrNewPattern, vblnIgnoreCase, False
'End Sub
'
'
''///////////////////////////////////////////////
''/// Sanity tests
''///////////////////////////////////////////////
''''
''''
''''
'Private Sub msubSanityTestToIsSpecifiedMicrosoftWordWindowSetForegroundByIncludingWindowCaptionPart()
'
'    If IsSpecifiedMicrosoftWordWindowSetForegroundByIncludingWindowCaptionPart() Then
'
'        Debug.Print "If the Word process exists, then it should have been in the foreground."
'    End If
'End Sub
'
'
'
''''
'''' Enumerate all Word Window objects which includes other processes
''''
'Private Sub msubSanityTestToCurrentExecutedWordWindows()
'
'    Dim varWindow As Variant, objWindow As Word.Window, varHWnd As Variant
'
'#If VBA7 Then
'
'    Dim intHWnd As LongPtr
'#Else
'    Dim intHWnd As Long
'#End If
'
'    Debug.Print "HWnd", "WindowTitleBarCaption", "FullPath"
'
'    With GetExecutedHWndToMsWordWindowsDic()
'
'        For Each varHWnd In .Keys
'
'            intHWnd = varHWnd
'
'            Set objWindow = .Item(varHWnd)
'
'            Debug.Print CStr(intHWnd), objWindow.Caption, objWindow.Document.FullName
'        Next
'    End With
'End Sub
'
'
''''
''''
''''
'Private Sub msubSanityTestToCurrentExecutedWordWindows02()
'
'    Dim varWindow As Variant, objWindow As Word.Window, varHWnd As Variant
'
'#If VBA7 Then
'
'    Dim intHWnd As LongPtr
'#Else
'    Dim intHWnd As Long
'#End If
'
'    Debug.Print "HWnd", "WindowTitleBarCaption", "FullPath"
'
'    With GetExecutedHWndToMsWordWindowsDic(vstrWindowTextRegExpPattern:="文書")
'
'        For Each varHWnd In .Keys
'
'            intHWnd = varHWnd
'
'            Set objWindow = .Item(varHWnd)
'
'            Debug.Print CStr(intHWnd), objWindow.Caption, objWindow.Document.FullName
'        Next
'    End With
'End Sub
'
''''
'''' Enumerate all Word Window objects which includes other processes
''''
'Private Sub msubSanityTestToExecWordWindowsOldType()
'
'    Dim varWindow As Variant, objWindow As Word.Window
'
'    Debug.Print "WindowTitleBarCaption"
'
'    For Each varWindow In GetExecutedHWndToMsWordWindowsDic().Items
'
'        Set objWindow = varWindow
'
'        Debug.Print objWindow.Caption
'    Next
'End Sub
'
'
'
'''--VBA_Code_File--<OperateOutlook.bas>--
'Attribute VB_Name = "OperateOutlook"
''
''   Tools to operate Microsoft Outlook
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on Outlook
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Sat, 15/Apr/2023    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
'
''///////////////////////////////////////////////
''/// Enumerations
''///////////////////////////////////////////////
''''
'''' olm; microsoft OutLook-Mails
''''
'Public Enum OutlookMailItemPropertyType
'
'    olmSenderEmailAddress = 0   ' Text
'
'    olmNameOfSenderObject = 1   ' Text
'
'    olmSubject = 2  ' Text
'
'    olmBody = 3     ' Text
'
'    olmUnRead = 4   ' Boolean
'End Enum
'
'
''''
'''' UnRead state, olm; microsoft OutLook-Mails
''''
'Public Enum OutlookMailUnReadState
'
'    olmNoChecksMailUnRead = 0
'
'    olmMailIsUnRead = 1 ' Unread Is True
'
'    olmMailHasAlreadyRead = 2   ' Unread Is False
'End Enum
'
''///////////////////////////////////////////////
''/// Control VBE windows
''///////////////////////////////////////////////
''**---------------------------------------------
''** Open relative VBE code-panes
''**---------------------------------------------
'Private Sub OpenVBECodePaneRelativeToOperateOutlook()
'
'    ' Focusing on this procdure, please press [F5] key to start this. Therefore this scope is private.
'
'    On Error Resume Next
'
'    ' If the 'ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma' isn't included in the VBA project, the following causes no syntax error.
'
'    Application.Run "ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma", _
'            "OperateOutlookToClassify,OperateOutlookForXl,OperateOutlookToClassifyForXl,EMailClassifyingCondition,EMailMovedPersonalBoxLogs,OutlookHyperLink,UTfOperateOutlook,UTfOperateOutlookForXl,FindKeywords,FindURLAndEmailAddresses"
'End Sub
'
''///////////////////////////////////////////////
''/// Sanity tests
''///////////////////////////////////////////////
''''
''''
''''
'Private Sub msubSanityTestToGetMailItemInformationsAboutDefaultInbox()
'
'    Dim objCol As Collection
'
'    Set objCol = GetDefaultInboxMailItemInfoCol()
'
'    DebugCol objCol
'End Sub
'
'
''''
''''
''''
'Private Sub msubSanityTestToGetAllFolderDetailInformations()
'
'    Dim objPaths As Collection
'
'    Set objPaths = GetMailFolderPathsWithSubFolders(True)
'
'    DebugCol objPaths
'End Sub
'
'
''''
''''
''''
'Private Sub msubSanityTestToGetAllFolderNames()
'
'    Dim objPaths As Collection
'
'    Set objPaths = GetMailFolderPathsWithSubFolders()
'
'    DebugCol objPaths
'End Sub
'
'
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''**---------------------------------------------
''** Get the current selected object on the current Outlook process
''**---------------------------------------------
''''
''''
''''
'Public Function GetSelectedOneMailItems(ByRef robjExplorer As Outlook.Explorer) As Outlook.MailItem
'
'    Dim objSelectedItem As Object, objMailItem As Outlook.MailItem
'
'    For Each objSelectedItem In robjExplorer.Selection
'
'        If TypeOf objSelectedItem Is Outlook.MailItem Then
'
'            Set objMailItem = objSelectedItem
'
'            Exit For
'        End If
'    Next
'
'    Set GetSelectedOneMailItems = objMailItem
'End Function
'
''''
''''
''''
'Public Function GetSelectedMailFolder(ByRef robjExplorer As Outlook.Explorer) As Outlook.Folder
'
'    Dim objFolder As Outlook.Folder
'
'    Set objFolder = GetSelectedOneMailItems(robjExplorer).Parent
'
'    Set GetSelectedMailFolder = objFolder
'End Function
'
'
''**---------------------------------------------
''** Serve classifying rule enuemrations
''**---------------------------------------------
''''
''''
''''
'Public Function GetEmailClassifyingConditionFromKeyText(ByVal vstrKeyText As String, Optional ByVal vstrCondition2ndDelimiter As String = "-") As EMailClassifyingCondition
'
'    Dim objEMailClassifyingCondition As EMailClassifyingCondition
'    Dim varMatchingCondition As Variant, strMatchingCondition As String
'
'
'    Set objEMailClassifyingCondition = New EMailClassifyingCondition
'
'    With objEMailClassifyingCondition
'
'        For Each varMatchingCondition In Split(vstrKeyText, vstrCondition2ndDelimiter)
'
'            strMatchingCondition = CStr(varMatchingCondition)
'
'            Select Case strMatchingCondition
'
'                Case "Subject", "SenderEmailAddress", "NameOfSenderObject", "Body", "UnRead"
'
'                    .SearchingMailItemPropertyType = GetMailItemPropertyTypeFromText(strMatchingCondition)
'
'                Case "AndOp", "OrOp", "NotAll"
'
'                    .SearchingMatchingTextBoolCondition = GetMatchingTextBoolConditionTypeFromText(strMatchingCondition)
'
'                Case "LCase", "Lcase"
'
'                    .NeedToUseLowerCase = True
'
'                Case "ExactMatch"
'
'                    .NeedToExactMatch = True
'
'            End Select
'        Next
'    End With
'
'    Set GetEmailClassifyingConditionFromKeyText = objEMailClassifyingCondition
'End Function
'
'
''''
''''
''''
'Public Function GetMailItemPropertyTypeFromText(ByVal vstrMailPropertyName As String) As OutlookMailItemPropertyType
'
'    Dim enmMailItemPropertyType As OutlookMailItemPropertyType
'
'    Select Case vstrMailPropertyName
'
'        Case "SenderEmailAddress"
'
'            enmMailItemPropertyType = OutlookMailItemPropertyType.olmSenderEmailAddress
'
'        Case "NameOfSenderObject"
'
'            enmMailItemPropertyType = OutlookMailItemPropertyType.olmNameOfSenderObject
'
'        Case "Subject"
'
'            enmMailItemPropertyType = OutlookMailItemPropertyType.olmSubject
'
'        Case "Body"
'
'            enmMailItemPropertyType = OutlookMailItemPropertyType.olmBody
'
'        Case "UnRead"
'
'            enmMailItemPropertyType = OutlookMailItemPropertyType.olmUnRead
'    End Select
'
'    GetMailItemPropertyTypeFromText = enmMailItemPropertyType
'End Function
'
''''
'''' convert a String data delimited some character into Collection object
''''
'Public Function GetMailItemPropertyTypeColFromLineDelimitedChar(ByVal vstrLineDelimited As String, Optional vstrDelimiter As String = ",") As Collection
'
'    Dim objCol As Collection, i As Long, strArray() As String
'
'    Set objCol = New Collection
'
'    If vstrLineDelimited <> "" Then
'
'        strArray = Split(vstrLineDelimited, vstrDelimiter)
'
'        For i = LBound(strArray) To UBound(strArray)
'
'            objCol.Add GetMailItemPropertyTypeFromText(Trim(strArray(i)))
'        Next
'    End If
'
'    Set GetMailItemPropertyTypeColFromLineDelimitedChar = objCol
'End Function
'
''**---------------------------------------------
''** Initialize EMailMovedPersonalBoxLogs object
''**---------------------------------------------
''''
''''
''''
'Public Sub SetupEMailMovedPersonalBoxLogs(ByRef robjEMailMovedPersonalBoxLogs As EMailMovedPersonalBoxLogs, _
'        ByVal vblnAllowToGetMovingBasicMailLogAsTable As Boolean, _
'        ByVal vblnAllowToCollectMovedMailBodyHyperlinksLogAsTable As Boolean)
'
'    If robjEMailMovedPersonalBoxLogs Is Nothing Then Set robjEMailMovedPersonalBoxLogs = New EMailMovedPersonalBoxLogs
'
'    With robjEMailMovedPersonalBoxLogs
'
'        .AllowToGetMovingBasicMailLogAsTable = vblnAllowToGetMovingBasicMailLogAsTable
'
'        .AllowToCollectMovedMailBodyHyperlinksLogAsTable = vblnAllowToCollectMovedMailBodyHyperlinksLogAsTable
'    End With
'End Sub
'
'
''''
''''
''''
'Public Sub OutputEMailMovedPersonalBoxLogsToImmediateWindow(ByRef robjEMailMovedPersonalBoxLogs As EMailMovedPersonalBoxLogs)
'
'    With robjEMailMovedPersonalBoxLogs
'
'        Debug.Print GetTwoDimentionalTextFromDic(.MainMovingProcessLogDic, "", ",", True, True)
'
'        If .AllowToGetMovingBasicMailLogAsTable Then
'
'            Debug.Print GetTwoDimentionalTextFromCol(.MovingBasicMailLogDTCol)
'        End If
'    End With
'End Sub
'
'
''**---------------------------------------------
''** Move Outlook mails to Outlook personal folder
''**---------------------------------------------
''''
''''
''''
'Public Sub MoveMailsToSpecifiedFolderFromDestinationCondition(ByRef robjEMailMovedPersonalBoxLogs As EMailMovedPersonalBoxLogs, _
'        ByVal vstrSourceMailFolderPath As String, _
'        ByVal vobjMailAddressConditionToSomeInfoDic As Scripting.Dictionary, _
'        ByVal vobjConditionKeyToDescriptionDic As Scripting.Dictionary, _
'        ByVal vstrConditionFieldTitlesDelimitedByComma As String, _
'        ByVal vstrPersonalOutlookBoxName As String, _
'        ByVal vobjDestinationPathPartCol As Collection, _
'        Optional ByVal vblnPreventExecutingToMoveMail As Boolean = False)
'
'
'    Dim objNamespace As Outlook.Namespace
'    Dim objSourceFolder As Outlook.Folder, objDestinationFolder As Outlook.Folder, objMailItem As Outlook.MailItem
'    Dim objPathToFolderObjectDic As Scripting.Dictionary
'
'    Dim blnDoMove As Boolean
'    Dim objPlusConditions As Collection, strNameOfSenderObject As String, strDestinationPathPart As String, objFilteringConditions As Collection
'    Dim strDescription As String
'    Dim objDoubleStopWatch As DoubleStopWatch, i As Long
'
'    Dim objMovingMailLogDTCol As Collection, intNotMovedCount As Long, intMovedCount As Long
'    Dim objExtractedMovedMailBodySomeInfoLogDTCol As Collection
'
'    Dim objReportItem As Outlook.ReportItem
'
'    Set objDoubleStopWatch = New DoubleStopWatch
'
'
'    intNotMovedCount = 0: intMovedCount = 0
'
'    With objDoubleStopWatch
'
'        .MeasureStart
'
'        With New Outlook.Application
'
'            Set objNamespace = .GetNamespace("MAPI")
'
'            Set objPathToFolderObjectDic = mfobjGetMailFolderPathPartToFolderObjectDic(vstrPersonalOutlookBoxName, _
'                    vobjDestinationPathPartCol, _
'                    objNamespace)
'
'            Set objFilteringConditions = GetMailItemPropertyTypeColFromLineDelimitedChar(vstrConditionFieldTitlesDelimitedByComma)
'
'            Set objSourceFolder = GetMailFolder(vstrSourceMailFolderPath, objNamespace.Folders)
'
'            For i = objSourceFolder.Items.Count To 1 Step -1
'
'                Select Case TypeName(objSourceFolder.Items.Item(i))
'
'                    Case "MailItem"
'
'                        Set objMailItem = objSourceFolder.Items.Item(i)
'
'                        blnDoMove = False
'
'                        With objMailItem
'
'                            ' If you need to include Outlook.MailItem.UnRead property branching, I recommend you to use the MoveMailsBySpecifiedDetailConditions
'
'                            msubGetDestinationFolderWhenMailItemHasToBeMoved blnDoMove, _
'                                    strDestinationPathPart, _
'                                    strDescription, _
'                                    objMailItem, _
'                                    vobjMailAddressConditionToSomeInfoDic, _
'                                    vobjConditionKeyToDescriptionDic, _
'                                    objFilteringConditions
'
'                            If blnDoMove Then
'
'                                MoveMailWithLogging intMovedCount, _
'                                        intNotMovedCount, _
'                                        objMovingMailLogDTCol, _
'                                        strDestinationPathPart, _
'                                        strDescription, _
'                                        objMailItem, _
'                                        objPathToFolderObjectDic, _
'                                        robjEMailMovedPersonalBoxLogs.AllowToGetMovingBasicMailLogAsTable, _
'                                        vblnPreventExecutingToMoveMail
'
'                                If robjEMailMovedPersonalBoxLogs.AllowToCollectMovedMailBodyHyperlinksLogAsTable Then
'
'                                    ' confirm strings all of hyperlink and E-mail addresses and UNC pathes in mail body-texts
'
'                                    GetOneRowMultiLinesHyperLinksAndEmailAddressesAndUNCPathsAndSomeInfoDTColFromMail objExtractedMovedMailBodySomeInfoLogDTCol, _
'                                            objMailItem
'                                End If
'                            Else
'                                intNotMovedCount = intNotMovedCount + 1
'                            End If
'                        End With
'
'                    Case "ReportItem"
'
'                        ' Yet implementation
'
'                        Set objReportItem = objSourceFolder.Items.Item(i)
'
'                End Select
'            Next
'        End With
'
'        .MeasureInterval
'    End With
'
'
'    With robjEMailMovedPersonalBoxLogs
'
'        Set .MainMovingProcessLogDic = GetBasicLogDicOfMovingEmailsProcessing(vobjMailAddressConditionToSomeInfoDic.Count, _
'                intNotMovedCount, _
'                intMovedCount, _
'                objDoubleStopWatch.ElapsedTimeByString)
'
'        If .AllowToGetMovingBasicMailLogAsTable Then
'
'            Set .MovingBasicMailLogDTCol = objMovingMailLogDTCol
'        End If
'
'        If .AllowToCollectMovedMailBodyHyperlinksLogAsTable Then
'
'            Set .ExtractedMovedMailBodySomeInfoLogDTCol = objExtractedMovedMailBodySomeInfoLogDTCol
'        End If
'    End With
'End Sub
'
''''
''''
''''
'Public Function GetBasicLogDicOfMovingEmailsProcessing(ByRef rintCountOfMovingEMailConditions As Long, _
'        ByRef rintNotMovedCount As Long, _
'        ByRef rintMovedCount As Long, _
'        ByVal vstrMovingElapsedTimeByString As String)
'
'    Dim objDic As Scripting.Dictionary
'
'    Set objDic = New Scripting.Dictionary
'
'    With objDic
'
'        .Add "Count of moving e-mail conditions", rintCountOfMovingEMailConditions
'
'        .Add "Not moved count", rintNotMovedCount
'
'        .Add "Moved count", rintMovedCount
'
'        .Add "Moving elapsed time", vstrMovingElapsedTimeByString
'
'        .Add "Logging date-time", Format(Now(), "ddd, yyyy/m/d hh:mm:ss")
'    End With
'
'    Set GetBasicLogDicOfMovingEmailsProcessing = objDic
'End Function
'
'
''''
''''
''''
'Public Sub MoveMailWithLogging(ByRef rintMovedCount As Long, _
'        ByRef rintNotMovedCount As Long, _
'        ByRef robjMovingMailLogDTCol As Collection, _
'        ByRef rstrDestinationPathPart As String, _
'        ByRef rstrDescription As String, _
'        ByRef robjMailItem As Outlook.MailItem, _
'        ByRef robjPathToFolderObjectDic As Scripting.Dictionary, _
'        Optional ByVal vblnAllowToGetMovingMailLogAsTable As Boolean = True, _
'        Optional ByVal vblnPreventExecutingToMoveMail As Boolean = False)
'
'
'    Dim objDestinationFolder As Outlook.Folder
'
'
'    Set objDestinationFolder = robjPathToFolderObjectDic.Item(rstrDestinationPathPart)
'
'
'    If vblnAllowToGetMovingMailLogAsTable Then
'
'        AddMovingMailLog robjMovingMailLogDTCol, rstrDestinationPathPart, rstrDescription, robjMailItem
'    End If
'
'    If Not vblnPreventExecutingToMoveMail Then
'
'        On Error Resume Next
'
'        robjMailItem.Move objDestinationFolder
'
'        If Err.Number = 0 Then
'
'            rintMovedCount = rintMovedCount + 1
'        Else
'            rintNotMovedCount = rintNotMovedCount + 1
'        End If
'
'        On Error GoTo 0
'    Else
'        rintNotMovedCount = rintNotMovedCount + 1
'    End If
'End Sub
'
'
''''
''''
''''
'Public Sub AddMovingMailLog(ByRef robjMovingMailLogDTCol As Collection, _
'        ByRef rstrDestinationPathPart As String, _
'        ByRef rstrDescription As String, _
'        ByRef robjMailItem As Outlook.MailItem)
'
'
'    Dim objRowCol As Collection
'
'
'    If robjMovingMailLogDTCol Is Nothing Then Set robjMovingMailLogDTCol = New Collection
'
'    Set objRowCol = New Collection
'
'    With objRowCol
'
'        .Add rstrDestinationPathPart
'
'        .Add rstrDescription
'
'        AddMailItemSomeTypesLogToRowCol objRowCol, robjMailItem
'    End With
'
'    robjMovingMailLogDTCol.Add objRowCol
'End Sub
'
'
''''
'''' common logging
''''
'Public Sub AddMailItemSomeTypesLogToRowCol(ByRef robjRowCol As Collection, ByRef robjMailItem As Outlook.MailItem)
'
'    With robjRowCol
'
'        .Add robjMailItem.SenderEmailAddress
'
'        .Add robjMailItem.Sender.Name
'
'        .Add robjMailItem.UnRead
'
'        .Add robjMailItem.ReceivedTime
'
'        .Add Left(robjMailItem.Subject, 256)
'
'        .Add Left(robjMailItem.Body, 256)
'    End With
'End Sub
'
'
''''
''''
''''
'Private Sub msubGetDestinationFolderWhenMailItemHasToBeMoved(ByRef rblnIsASatisfiedConditionSetFound As Boolean, _
'        ByRef rstrDestinationPathPart As String, _
'        ByRef rstrDescription As String, _
'        ByRef robjMailItem As Outlook.MailItem, _
'        ByRef robjMailAddressConditionToSomeInfoDic As Scripting.Dictionary, _
'        ByRef vobjConditionKeyToDescriptionDic As Scripting.Dictionary, _
'        ByRef robjFilteringConditions As Collection)
'
'
'    Dim objDestinationFolder As Outlook.Folder, strConditionTypeName As String
'
'    Dim varFirstCondition As Variant, varCondition As Variant, objSecondLaterConditions As Collection, i As Long
'
'    Dim enmMailItemPropertyType As OutlookMailItemPropertyType, strDistinctKey As String ' , strDestinationPathPart As String, strDescription As String
'
'
'    Set objDestinationFolder = Nothing
'
'    With robjMailAddressConditionToSomeInfoDic
'
'        rblnIsASatisfiedConditionSetFound = False
'
'        For Each varFirstCondition In .Keys
'
'            strDistinctKey = ""
'
'            ' In this case, varFirstCondition is SenderEmailAddress
'
'            enmMailItemPropertyType = robjFilteringConditions.Item(1)
'
'            If mfblnIsAMailFilteringConditionsSatisfied(robjMailItem, varFirstCondition, enmMailItemPropertyType) Then
'
'                strDistinctKey = strDistinctKey & varFirstCondition
'
'                Set objSecondLaterConditions = .Item(varFirstCondition)
'
'                i = 1
'
'                For Each varCondition In objSecondLaterConditions
'
'                    If i < objSecondLaterConditions.Count Then
'
'                        ' In this case (1st), varCondition is NameOfSenderObject
'
'                        enmMailItemPropertyType = robjFilteringConditions.Item(i + 1)
'
'                        If Not mfblnIsAMailFilteringConditionsSatisfied(robjMailItem, varCondition, enmMailItemPropertyType) Then
'
'                            Exit For
'                        Else
'                            strDistinctKey = strDistinctKey & "," & varCondition
'                        End If
'                    Else
'                        ' In this case (2nd), varCondition is DestinationPathPart
'
'                        ' Use strDistinctKey then, get strDescription
'
'                        rstrDescription = vobjConditionKeyToDescriptionDic.Item(strDistinctKey)
'
'                        rstrDestinationPathPart = varCondition
'
'                        rblnIsASatisfiedConditionSetFound = True
'
'                        Exit For
'                    End If
'
'                    i = i + 1
'                Next
'
'                If rblnIsASatisfiedConditionSetFound Then
'
'                    Exit For
'                End If
'            End If
'        Next
'    End With
'End Sub
'
'
''''
''''
''''
'Private Function mfblnIsAMailFilteringConditionsSatisfied(ByRef robjMailItem As Outlook.MailItem, _
'        ByVal vstrConditions As String, _
'        ByRef renmMailItemPropertyType As OutlookMailItemPropertyType, _
'        Optional ByVal vstrDelimiter As String = ";") As Boolean
'
'
'    Dim blnIsSatisfied As Boolean, strConditions() As String, i As Long
'
'    blnIsSatisfied = False
'
'    strConditions = Split(vstrConditions, vstrDelimiter)
'
'    For i = LBound(strConditions) To UBound(strConditions)
'
'        If mfblnIsAMailAnFilteringConditionSatisfied(robjMailItem, strConditions(i), renmMailItemPropertyType) Then
'
'            blnIsSatisfied = True
'
'            Exit For
'        End If
'    Next
'
'    mfblnIsAMailFilteringConditionsSatisfied = blnIsSatisfied
'End Function
'
'
''''
'''' This supports only partial matches for searching words
''''
'Private Function mfblnIsAMailAnFilteringConditionSatisfied(ByRef robjMailItem As Outlook.MailItem, _
'        ByVal vstrAnCondition As String, _
'        ByRef renmMailItemPropertyType As OutlookMailItemPropertyType) As Boolean
'
'
'    Dim blnIsSatisfied As Boolean
'
'    blnIsSatisfied = False
'
'    With robjMailItem
'
'        Select Case renmMailItemPropertyType
'
'            Case OutlookMailItemPropertyType.olmSenderEmailAddress
'
'                blnIsSatisfied = (InStr(1, .SenderEmailAddress, vstrAnCondition) > 0)
'
'            Case OutlookMailItemPropertyType.olmNameOfSenderObject
'
'                blnIsSatisfied = (InStr(1, .Sender.Name, vstrAnCondition) > 0)
'
'            Case OutlookMailItemPropertyType.olmSubject
'
'                blnIsSatisfied = (InStr(1, .Subject, vstrAnCondition) > 0)
'
'            Case OutlookMailItemPropertyType.olmBody
'
'                blnIsSatisfied = (InStr(1, .Body, vstrAnCondition) > 0)
'        End Select
'
'    End With
'
'    mfblnIsAMailAnFilteringConditionSatisfied = blnIsSatisfied
'End Function
'
'
''''
'''' get dictionary of Key DestinationFolderPathPart string and Value DestinationFolder object based on the vobjDestinationPathPartCol
''''
'Private Function mfobjGetMailFolderPathPartToFolderObjectDic(ByVal vstrPersonalOutlookBoxName As String, _
'        ByVal vobjDestinationPathPartCol As Collection, _
'        ByVal vobjNamespace As Outlook.Namespace) As Scripting.Dictionary
'
'
'    Dim varDestinationPathPart As Variant, strDestinationFolderPath As String, objPathToFolderObjectDic As Scripting.Dictionary
'
'    Dim objDestinationFolder As Outlook.Folder
'
'
'    Set objPathToFolderObjectDic = New Scripting.Dictionary
'
'    For Each varDestinationPathPart In vobjDestinationPathPartCol
'
'        strDestinationFolderPath = "\\" & vstrPersonalOutlookBoxName & "\" & varDestinationPathPart
'
'        Set objDestinationFolder = GetMailFolder(strDestinationFolderPath, vobjNamespace.Folders)
'
'        If Not objDestinationFolder Is Nothing Then
'
'            objPathToFolderObjectDic.Add varDestinationPathPart, objDestinationFolder
'        Else
'            MsgBox "Probably, the Mail folder hasn't been created yet;" & vbNewLine & strDestinationFolderPath
'        End If
'    Next
'
'    Set mfobjGetMailFolderPathPartToFolderObjectDic = objPathToFolderObjectDic
'End Function
'
'
'
''**---------------------------------------------
''** Get the current Outlook mail information
''**---------------------------------------------
''''
''''
''''
'Public Function GetDefaultInboxMailItemInfoCol() As Collection
'
'    Dim objNamespace As Outlook.Namespace, objFolder As Outlook.Folder
'    Dim objCol As Collection
'
'    With New Outlook.Application
'
'        Set objNamespace = .GetNamespace("MAPI")
'
'        Set objFolder = objNamespace.GetDefaultFolder(olFolderInbox)
'
'        Set objCol = GetMailItemInfoColInAFolder(objFolder)
'    End With
'
'    Set GetDefaultInboxMailItemInfoCol = objCol
'End Function
'
'
''''
''''
''''
'Public Function GetMailItemInfoColInAFolder(ByVal vobjFolder As Outlook.Folder) As Collection
'
'    Dim objCol As Collection
'
'    Set objCol = New Collection
'
'    CollectMailItemInfoColInAFolder objCol, vobjFolder
'
'    Set GetMailItemInfoColInAFolder = objCol
'End Function
'
'
''''
''''
''''
'Public Function CollectMailItemInfoColInAFolder(ByRef robjCol As Collection, _
'        ByVal vobjFolder As Outlook.Folder) As Collection
'
'    Dim objRowCol As Collection
'
'    Dim objMailItem As Outlook.MailItem
'
'
'    For Each objMailItem In vobjFolder.Items
'
'        Set objRowCol = New Collection
'
'        With objRowCol
'
'            .Add objMailItem.Subject
'
'            .Add objMailItem.SenderName
'
'            .Add objMailItem.Sender.Name
'
'            .Add objMailItem.SenderEmailAddress
'
'            .Add objMailItem.ReceivedTime
'
'            .Add objMailItem.Attachments.Count
'
'            .Add objMailItem.BodyFormat
'
'            .Add objMailItem.Size
'
'            .Add objMailItem.UnRead
'
'        End With
'
'        robjCol.Add objRowCol
'    Next
'End Function
'
'
''''
''''
''''
'Public Function GetSpecifiedMailFolderDetailInformationCol(ByVal vobjMailFolderPaths As Collection) As Collection
'
'    Dim varMailFolderPath As Variant, strMailFolderPath As String
'    Dim objFolder As Outlook.Folder, objNamespace As Outlook.Namespace
'    Dim objCol As Collection
'
'
'    Set objCol = New Collection
'
'    With New Outlook.Application
'
'        Set objNamespace = .GetNamespace("MAPI")
'
'        For Each varMailFolderPath In vobjMailFolderPaths
'
'            strMailFolderPath = varMailFolderPath
'
'            Set objFolder = GetMailFolder(strMailFolderPath, objNamespace.Folders)
'
'            If Not objFolder Is Nothing Then
'
'                msubCollectMailFolderDetails objCol, objFolder
'            End If
'        Next
'    End With
'
'    Set GetSpecifiedMailFolderDetailInformationCol = objCol
'End Function
'
'
'
''''
''''
''''
'Public Function GetMailFolderPathsWithSubFolders(Optional ByVal vblnCollectMailDetailInfo As Boolean = False, _
'        Optional ByVal vblnFilterOfIncludingAtLeastOneMail As Boolean = False) As Collection
'
'
'    Dim objFolder As Outlook.Folder, strRootMailFolderPath As String
'
'    Dim intHierarchyOrder As Long
'
'    Dim objMailFolderInfos As Collection, blnAddInfo As Boolean
'
'
'    intHierarchyOrder = 1
'
'
'    Set objMailFolderInfos = New Collection
'
'    With New Outlook.Application
'
'        'For Each objFolder In .Session.Folders
'
'        For Each objFolder In .GetNamespace("MAPI").Folders
'
'            'Debug.Print CStr(intHierarchyOrder) & ", " & objFolder.Name
'
'            strRootMailFolderPath = "\\" & objFolder.Name
'
'            blnAddInfo = True
'
'            If vblnFilterOfIncludingAtLeastOneMail Then
'
'                If objFolder.Items.Count = 0 Then
'
'                    blnAddInfo = False
'                End If
'            End If
'
'            If blnAddInfo Then
'
'                If vblnCollectMailDetailInfo Then
'
'                    msubCollectMailFolderDetails objMailFolderInfos, objFolder
'                Else
'                    objMailFolderInfos.Add strRootMailFolderPath
'                End If
'            End If
'
'
'            If objFolder.Folders.Count > 0 Then
'
'                msubCollectSubMailFolder objMailFolderInfos, _
'                        strRootMailFolderPath, _
'                        objFolder, _
'                        intHierarchyOrder + 1, _
'                        vblnCollectMailDetailInfo, _
'                        vblnFilterOfIncludingAtLeastOneMail
'            End If
'        Next
'
'    End With
'
'    Set GetMailFolderPathsWithSubFolders = objMailFolderInfos
'End Function
'
'
''''
''''
''''
'Public Function GetMailFolderDetailInformationCol(ByVal vstrPersonalOutlookBoxName As String, _
'        ByVal vobjDestinationPathPartCol As Collection) As Collection
'
'
'    Dim objNamespace As Outlook.Namespace, varDestinationPathPart As Variant
'
'    Dim strMailFolderPath As String, objFolder As Outlook.Folder
'
'    Dim objMailFolderInfos As Collection, objRowCol As Collection
'
'
'    Set objMailFolderInfos = New Collection
'
'    With New Outlook.Application
'
'        Set objNamespace = .GetNamespace("MAPI")
'
'        For Each varDestinationPathPart In vobjDestinationPathPartCol
'
'            strMailFolderPath = "\\" & vstrPersonalOutlookBoxName & "\" & varDestinationPathPart
'
'            Set objFolder = GetMailFolder(strMailFolderPath, objNamespace.Folders)
'
'            If Not objFolder Is Nothing Then
'
'                msubCollectMailFolderDetails objMailFolderInfos, objFolder
'            End If
'        Next
'    End With
'
'    Set GetMailFolderDetailInformationCol = objMailFolderInfos
'End Function
'
'
''''
''''
''''
'Public Function CreateMailFolderPathsFromDestinationPathPartCol(ByVal vstrPersonalOutlookBoxName As String, _
'        ByVal vobjDestinationPathPartCol As Collection) As Collection
'
'
'    Dim objNamespace As Outlook.Namespace, varDestinationPathPart As Variant
'
'    Dim strMailFolderPath As String, objFolder As Outlook.Folder
'
'    Dim objCol As Collection
'
'
'    Set objCol = New Collection
'
'    With New Outlook.Application
'
'        Set objNamespace = .GetNamespace("MAPI")
'
'        For Each varDestinationPathPart In vobjDestinationPathPartCol
'
'            strMailFolderPath = "\\" & vstrPersonalOutlookBoxName & "\" & varDestinationPathPart
'
'            Set objFolder = GetMailFolder(strMailFolderPath, objNamespace.Folders)
'
'            If objFolder Is Nothing Then
'
'                Set objFolder = CreateMailFolder(strMailFolderPath, objNamespace.Folders)
'
'                If Not objFolder Is Nothing Then
'
'                    objCol.Add strMailFolderPath
'                End If
'            End If
'        Next
'    End With
'
'    Set CreateMailFolderPathsFromDestinationPathPartCol = objCol
'End Function
'
'
''''
''''
''''
'Public Sub CreateMailFoldersFromMailFolderPathDicIfTheUserNeedsIts(ByRef robjNotExistedMailFolderPathDic As Scripting.Dictionary, _
'        Optional ByVal vobjOutlookApplication As Outlook.Application = Nothing)
'
'
'    Dim objOutlookApplication As Outlook.Application, objNamespace As Outlook.Namespace
'
'    Dim enmMsgResult As VbMsgBoxResult, varMailFolderPath As Variant, strMailFolderPath As String, objFolder As Outlook.Folder
'
'    Dim intCountOfCreate As Long
'
'
'    If Not robjNotExistedMailFolderPathDic Is Nothing Then
'
'        If robjNotExistedMailFolderPathDic.Count > 0 Then
'
'            enmMsgResult = MsgBox(mfstrGetMessagePromptAboutCreateOutlookSubFolders(robjNotExistedMailFolderPathDic), _
'                    vbYesNo Or vbInformation, _
'                    "Confirm you want whether to create Outlook sub-folders in a personal boxes or not")
'
'            If enmMsgResult = vbYes Then
'
'                If Not vobjOutlookApplication Is Nothing Then
'
'                    Set objOutlookApplication = vobjOutlookApplication
'                Else
'                    Set objOutlookApplication = New Outlook.Application
'                End If
'
'                intCountOfCreate = 0
'
'                With objOutlookApplication
'
'                    Set objNamespace = .GetNamespace("MAPI")
'
'                    For Each varMailFolderPath In GetEnumeratorKeysColFromDic(robjNotExistedMailFolderPathDic)
'
'                        strMailFolderPath = varMailFolderPath
'
'                        Set objFolder = CreateMailFolder(strMailFolderPath, objNamespace.Folders)
'
'                        If Not objFolder Is Nothing Then
'
'                            intCountOfCreate = intCountOfCreate + 1
'                        End If
'                    Next
'                End With
'
'                If intCountOfCreate = robjNotExistedMailFolderPathDic.Count Then
'
'                    robjNotExistedMailFolderPathDic.RemoveAll
'                End If
'            End If
'        End If
'    End If
'End Sub
'
'
''''
''''
''''
'Private Function mfstrGetMessagePromptAboutCreateOutlookSubFolders(ByRef robjNotExistedMailFolderPathDic As Scripting.Dictionary) As String
'
'    Dim varMailFolderPath As Variant, strPrompt As String
'
'    strPrompt = "Do you allow to create the following Outlook sub-folders:" & vbNewLine
'
'    For Each varMailFolderPath In GetEnumeratorKeysColFromDic(robjNotExistedMailFolderPathDic)
'
'        strPrompt = strPrompt & vbNewLine & CStr(varMailFolderPath)
'    Next
'
'    mfstrGetMessagePromptAboutCreateOutlookSubFolders = strPrompt
'End Function
'
'
''''
''''
''''
'Public Function GetMailFolderPathToExistsTableCol(ByVal vstrPersonalOutlookBoxName As String, _
'        ByVal vobjDestinationPathPartCol As Collection) As Collection
'
'
'    Dim objNamespace As Outlook.Namespace, varDestinationPathPart As Variant, strMailFolderPath As String
'
'    Dim objFolder As Outlook.Folder
'
'    Dim objCol As Collection, objRowCol As Collection
'
'
'    Set objCol = New Collection
'
'    With New Outlook.Application
'
'        Set objNamespace = .GetNamespace("MAPI")
'
'        For Each varDestinationPathPart In vobjDestinationPathPartCol
'
'            strMailFolderPath = "\\" & vstrPersonalOutlookBoxName & "\" & varDestinationPathPart
'
'            Set objFolder = GetMailFolder(strMailFolderPath, objNamespace.Folders)
'
'            Set objRowCol = New Collection
'
'            With objRowCol
'
'                .Add strMailFolderPath
'
'                If Not objFolder Is Nothing Then
'
'                    .Add True
'
'                    .Add objFolder.Items.Count
'                Else
'                    .Add False
'
'                    .Add Empty
'                End If
'
'                objCol.Add objRowCol
'            End With
'        Next
'
'    End With
'
'    Set GetMailFolderPathToExistsTableCol = objCol
'End Function
'
'
''''
''''
''''
'Public Function GetMailFolderWithPersonalOutlookBoxName(ByRef rstrPersonalOutlookBoxName As String, _
'        ByRef rstrDestinationPathPart As String, _
'        ByVal vobjMAPIFolders As Outlook.Folders) As Outlook.Folder
'
'
'    Dim strMailFolderPath As String
'
'
'    strMailFolderPath = "\\" & rstrPersonalOutlookBoxName & "\" & rstrDestinationPathPart
'
'    Set GetMailFolderWithPersonalOutlookBoxName = GetMailFolder(strMailFolderPath, vobjMAPIFolders)
'End Function
'
'
''''
''''
''''
'Public Function GetMailFolder(ByRef rstrMailFolderFullPath As String, ByVal vobjMAPIFolders As Outlook.Folders) As Outlook.Folder
'
'    Set GetMailFolder = GetMailFolderWithCreating(rstrMailFolderFullPath, vobjMAPIFolders, False)
'End Function
'
'Public Function CreateMailFolder(ByVal vstrMailFolderFullPath As String, ByVal vobjMAPIFolders As Outlook.Folders) As Outlook.Folder
'
'    Set CreateMailFolder = GetMailFolderWithCreating(vstrMailFolderFullPath, vobjMAPIFolders, True)
'End Function
'
'
''''
''''
''''
'Private Function GetMailFolderWithCreating(ByRef rstrMailFolderFullPath As String, ByVal vobjMAPIFolders As Outlook.Folders, _
'        Optional ByVal vblnCreateFolder As Boolean = True) As Outlook.Folder
'
'
'    Dim strDelimitedFolderNames() As String, i As Long, intCounter As Long, strPath As String
'
'    Dim objFolder As Outlook.Folder, objParentFolders As Outlook.Folders
'
'
'    If InStr(1, rstrMailFolderFullPath, "\\") = 1 Then
'
'        strPath = Right(rstrMailFolderFullPath, Len(rstrMailFolderFullPath) - 2)
'    Else
'        strPath = rstrMailFolderFullPath
'    End If
'
'    If InStrRev(strPath, "\") = Len(strPath) Then
'
'        strPath = Left(strPath, Len(strPath) - 1)
'    End If
'
'    strDelimitedFolderNames = Split(strPath, "\")
'
'    intCounter = 0
'
'    For i = LBound(strDelimitedFolderNames) To UBound(strDelimitedFolderNames)
'
'        If intCounter = 0 Then
'
'            Set objParentFolders = vobjMAPIFolders
'        Else
'            Set objParentFolders = objFolder.Folders
'        End If
'
'
'        Set objFolder = Nothing
'
'        On Error Resume Next
'
'        Set objFolder = objParentFolders.Item(strDelimitedFolderNames(i))
'
'        On Error GoTo 0
'
'        If objFolder Is Nothing Then
'
'            If vblnCreateFolder Then
'
'                Set objFolder = objParentFolders.Add(strDelimitedFolderNames(i))
'            Else
'
'                Exit For
'            End If
'        End If
'
'        intCounter = intCounter + 1
'    Next
'
'    Set GetMailFolderWithCreating = objFolder
'End Function
'
'
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
''''
''''
''''
'Private Sub msubCollectSubMailFolder(ByRef robjMailFolderInfos As Collection, _
'        ByVal vstrParentFolderPath As String, _
'        ByVal vobjFolder As Outlook.Folder, _
'        ByVal vintHierarchyOrder As Long, _
'        Optional ByVal vblnCollectMailDetailInfo As Boolean = False, _
'        Optional ByVal vblnFilterOfIncludingAtLeastOneMail As Boolean = False)
'
'
'    Dim objChildFolder As Outlook.Folder, strCurrentFolderPath As String
'
'    Dim blnAddInfo As Boolean
'
'
'    For Each objChildFolder In vobjFolder.Folders
'
'        'Debug.Print CStr(vintHierarchyOrder) & ", " & objChildFolder.Name
'
'        strCurrentFolderPath = mfstrGetMailFolderPathString(vstrParentFolderPath, objChildFolder)
'
'        blnAddInfo = True
'
'        If vblnFilterOfIncludingAtLeastOneMail Then
'
'            If objChildFolder.Items.Count = 0 Then
'
'                blnAddInfo = False
'            End If
'        End If
'
'
'        If blnAddInfo Then
'
'            If vblnCollectMailDetailInfo Then
'
'                msubCollectMailFolderDetails robjMailFolderInfos, objChildFolder
'            Else
'
'                robjMailFolderInfos.Add strCurrentFolderPath
'            End If
'        End If
'
'        If objChildFolder.Folders.Count > 0 Then
'
'            msubCollectSubMailFolder robjMailFolderInfos, _
'                    strCurrentFolderPath, _
'                    objChildFolder, _
'                    vintHierarchyOrder + 1, _
'                    vblnCollectMailDetailInfo, _
'                    vblnFilterOfIncludingAtLeastOneMail
'        End If
'    Next
'End Sub
'
'
''''
''''
''''
'Private Sub msubCollectMailFolderDetails(ByRef robjCol As Collection, ByRef robjFolder As Outlook.Folder)
'
'    Dim objRowCol As Collection
'
'    Set objRowCol = New Collection
'
'
'    With robjFolder
'
'        objRowCol.Add .Name
'
'        objRowCol.Add .Items.Count
'
'        objRowCol.Add .UnReadItemCount
'
'        objRowCol.Add .FolderPath
'
'        objRowCol.Add .EntryID
'
'        objRowCol.Add .Description
'
'        objRowCol.Add .DefaultItemType
'    End With
'
'    robjCol.Add objRowCol
'End Sub
'
''''
''''
''''
'Private Function mfstrGetMailFolderPathString(ByRef rstrParentFolderPath As String, _
'        ByRef robjFolder As Outlook.Folder) As String
'
'    Dim strCurrentFolderPath As String
'
'    If rstrParentFolderPath <> "" Then
'
'        strCurrentFolderPath = rstrParentFolderPath & "\" & robjFolder.Name
'    Else
'        strCurrentFolderPath = "\\" & robjFolder.Name
'    End If
'
'    mfstrGetMailFolderPathString = strCurrentFolderPath
'End Function
'
'
'
'''--VBA_Code_File--<OperateOutlookToClassify.bas>--
'Attribute VB_Name = "OperateOutlookToClassify"
''
''   Tools to operate Microsoft Outlook using Windows API, which serves Windows INI files
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on both Outlook and Windows API(serving INI files)
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Fri, 28/Apr/2023    Kalmclaeyd Tarclanus    Initial Creation
''       Wed, 17/Jul/2024    Kalmclaeyd Tarclanus    Supported the moving the receiver E-mail address misrepresentation string type
''
'
'Option Explicit
'
'
''///////////////////////////////////////////////
''/// Constants
''///////////////////////////////////////////////
'Private Const mstrReceiverMailAddressMisrepresentationKey As String = "\ReceiverMailAddressMisrepresentation"
'
'Private Const mstrFirstPartOfReceiverMailAddressMisrepresentation As String = mstrReceiverMailAddressMisrepresentationKey & ":=FirstPart"
'
'Private Const mstrAllOfReceiverMailAddressMisrepresentation As String = mstrReceiverMailAddressMisrepresentationKey & ":=All"
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''**---------------------------------------------
''** Move mail-items by detailed conditions
''**---------------------------------------------
''''
''''
''''
'Public Sub MoveMailsBySpecifiedDetailConditionsFromIniFile(ByVal vstrSourceMailFolderPath As String, _
'        ByVal vstrPersonalOutlookBoxName As String, _
'        ByVal vstrIniPath As String, _
'        Optional ByVal vblnAllowToGetMovingMailLogAsTable As Boolean = True, _
'        Optional ByVal vblnAllowToCollectMovedMailBodyHyperlinksLogAsTable As Boolean = False, _
'        Optional ByVal vblnPreventExecutingToMoveMail As Boolean = False)
'
'
'    Dim objSectionToKeyValueDic As Scripting.Dictionary, objEMailMovedPersonalBoxLogs As EMailMovedPersonalBoxLogs
'
'
'    ' !Attention - Since the Windows ini file is a Shift_JIS, the Chinese Kanji characters should have been lost in Japanese locale Windows OS.
'
'    Set objSectionToKeyValueDic = GetSectionToKeyValueDicFromIni(vstrIniPath)
'
'
'    SetupEMailMovedPersonalBoxLogs objEMailMovedPersonalBoxLogs, _
'            vblnAllowToGetMovingMailLogAsTable, _
'            vblnAllowToCollectMovedMailBodyHyperlinksLogAsTable
'
'    MoveMailsBySpecifiedDetailConditions objEMailMovedPersonalBoxLogs, _
'            vstrSourceMailFolderPath, _
'            vstrPersonalOutlookBoxName, _
'            objSectionToKeyValueDic, _
'            vblnPreventExecutingToMoveMail
'
'    ' output log to the Immediate-window
'
'    OutputEMailMovedPersonalBoxLogsToImmediateWindow objEMailMovedPersonalBoxLogs
'End Sub
'
'
''''
''''
''''
'Public Sub MoveMailsBySpecifiedDetailConditions(ByRef robjEMailMovedPersonalBoxLogs As EMailMovedPersonalBoxLogs, _
'        ByVal vstrSourceMailFolderPath As String, _
'        ByVal vstrPersonalOutlookBoxName As String, _
'        ByRef robjSectionToKeyValueDic As Scripting.Dictionary, _
'        Optional ByVal vblnPreventExecutingToMoveMail As Boolean = False)
'
'
'
'    Dim objNamespace As Outlook.Namespace, objSourceFolder As Outlook.Folder, objMailItem As Outlook.MailItem
'    Dim i As Long
'
'    Dim objPathToFolderObjectDic As Scripting.Dictionary
'
'    Dim varConditionSet As Variant, objConditionSet As Collection, varCondition1stKey As Variant
'    Dim objEMailClassifyingCondition As EMailClassifyingCondition, objFindingKeyWords As Collection
'    Dim objSecondLaterConditions As Collection
'    Dim strDescription As String
'
'    Dim objCondition1stKeyToConditionSetDic As Scripting.Dictionary             ' Dictionary(Of Key, Collection(Of SomeObjects))
'    Dim objCondition1stKeyToSecondLaterConditionsDic As Scripting.Dictionary    ' Dictionary(Of Key, Collection(Of Collection(Of SomeObjects)))
'    Dim objCondition1stKeyToDestinationPathPartDic As Scripting.Dictionary      ' Dictionary(Of Key, String)
'    Dim objCondition1stKeyToDescriptionDic As Scripting.Dictionary              ' Dictionary(Of Key, String)
'    Dim blnAre2ndConditionsSatisfied As Boolean
'
'
'
'    Dim blnNeedToMove As Boolean, strDestinationPathPart As String, strDestinationFolderPath As String
'    Dim objDestinationFolder As Outlook.Folder
'    Dim objMovingMailLogDTCol As Collection, intNotMovedCount As Long, intMovedCount As Long
'    Dim objExtractedMovedMailBodySomeInfoLogDTCol As Collection
'    Dim objDoubleStopWatch As DoubleStopWatch
'
'
'    Dim objOutlook As Outlook.Application
'
'
'    Dim objReportItem As Outlook.ReportItem
'
'    intNotMovedCount = 0: intMovedCount = 0
'
'
'    Set objOutlook = New Outlook.Application
'
'    Set objNamespace = objOutlook.GetNamespace("MAPI")
'
'    msubInitializeCondition1stKeyToConditionSetDic _
'            objCondition1stKeyToConditionSetDic, _
'            objCondition1stKeyToSecondLaterConditionsDic, _
'            objCondition1stKeyToDestinationPathPartDic, _
'            objCondition1stKeyToDescriptionDic, _
'            robjSectionToKeyValueDic, _
'            objNamespace
'
'    Set objDoubleStopWatch = New DoubleStopWatch
'
'    With objDoubleStopWatch
'
'        .MeasureStart
'
'        With objOutlook
'
'            Set objNamespace = .GetNamespace("MAPI")
'
'            Set objPathToFolderObjectDic = mfobjGetMailFolderPathPartToFolderObjectDicFromMailTrasportingDetailSectionToKeyValueDic(vstrPersonalOutlookBoxName, _
'                    robjSectionToKeyValueDic, _
'                    objNamespace)
'
'
'            Set objSourceFolder = GetMailFolder(vstrSourceMailFolderPath, objNamespace.Folders)
'
'            For i = objSourceFolder.Items.Count To 1 Step -1
'
'                Select Case TypeName(objSourceFolder.Items.Item(i))
'
'                    Case "MailItem"
'
'                        Set objMailItem = objSourceFolder.Items.Item(i)
'
'                        blnNeedToMove = False
'
'                        With objCondition1stKeyToConditionSetDic
'
'
'                            msubFindFirstSatisfyingDetailConditionSet blnNeedToMove, _
'                                    strDestinationPathPart, _
'                                    strDescription, _
'                                    objMailItem, _
'                                    objCondition1stKeyToConditionSetDic, _
'                                    objCondition1stKeyToSecondLaterConditionsDic, _
'                                    objCondition1stKeyToDestinationPathPartDic, _
'                                    objCondition1stKeyToDescriptionDic
'
'
'                            If blnNeedToMove Then
'
'                                MoveMailWithLogging intMovedCount, _
'                                        intNotMovedCount, _
'                                        objMovingMailLogDTCol, _
'                                        strDestinationPathPart, _
'                                        strDescription, _
'                                        objMailItem, _
'                                        objPathToFolderObjectDic, _
'                                        robjEMailMovedPersonalBoxLogs.AllowToGetMovingBasicMailLogAsTable, _
'                                        vblnPreventExecutingToMoveMail
'
'
'                                If robjEMailMovedPersonalBoxLogs.AllowToCollectMovedMailBodyHyperlinksLogAsTable Then
'
'                                    ' confirm strings all of hyperlink and E-mail addresses and UNC pathes in mail body-texts
'
'                                    GetOneRowMultiLinesHyperLinksAndEmailAddressesAndUNCPathsAndSomeInfoDTColFromMail objExtractedMovedMailBodySomeInfoLogDTCol, _
'                                            objMailItem
'                                End If
'                            Else
'                                intNotMovedCount = intNotMovedCount + 1
'                            End If
'                        End With
'
'                    Case "ReportItem"
'
'                        Set objReportItem = objSourceFolder.Items.Item(i)
'
'                        Debug.Print "ReportItem received: " & objReportItem.Subject
'
'                    Case Else
'
'                        Debug.Print "Not supported - " & TypeName(objSourceFolder.Items.Item(i))
'
'                End Select
'
'            Next
'        End With
'
'        .MeasureInterval
'    End With
'
'
'    With robjEMailMovedPersonalBoxLogs
'
'        Set .MainMovingProcessLogDic = GetBasicLogDicOfMovingEmailsProcessing(objCondition1stKeyToConditionSetDic.Count, _
'                intNotMovedCount, _
'                intMovedCount, _
'                objDoubleStopWatch.ElapsedTimeByString)
'
'
'        If .AllowToGetMovingBasicMailLogAsTable Then
'
'            Set .MovingBasicMailLogDTCol = objMovingMailLogDTCol
'        End If
'
'        If .AllowToCollectMovedMailBodyHyperlinksLogAsTable Then
'
'            Set .ExtractedMovedMailBodySomeInfoLogDTCol = objExtractedMovedMailBodySomeInfoLogDTCol
'        End If
'    End With
'End Sub
'
'
''''
'''' get dictionary of Key DestinationFolderPathPart string and Value DestinationFolder object based on the robjSectionToKeyValueDic detail e-mail transport rules dictionary
''''
'Private Function mfobjGetMailFolderPathPartToFolderObjectDicFromMailTrasportingDetailSectionToKeyValueDic(ByVal vstrPersonalOutlookBoxName As String, _
'        ByRef robjSectionToKeyValueDic As Scripting.Dictionary, _
'        ByVal vobjNamespace As Outlook.Namespace) As Scripting.Dictionary
'
'
'    Dim varCondition1stKey As Variant, objChildConditionsDic As Scripting.Dictionary, objPathToFolderObjectDic As Scripting.Dictionary
'
'    Dim strDestinationPathPart As String, strDestinationFolderPath As String
'
'    Dim objDestinationFolder As Outlook.Folder
'
'    Const strTransportDestinationKey As String = "TransportDestination"
'
'
'    Set objPathToFolderObjectDic = New Scripting.Dictionary
'
'    With robjSectionToKeyValueDic
'
'        For Each varCondition1stKey In .Keys
'
'            Set objChildConditionsDic = .Item(varCondition1stKey)
'
'            With objChildConditionsDic
'
'                If .Exists(strTransportDestinationKey) Then
'
'                    strDestinationPathPart = .Item(strTransportDestinationKey)
'
'                    With objPathToFolderObjectDic
'
'                        If Not .Exists(strDestinationPathPart) Then
'
'                            strDestinationFolderPath = "\\" & vstrPersonalOutlookBoxName & "\" & strDestinationPathPart
'
'                            Set objDestinationFolder = GetMailFolder(strDestinationFolderPath, vobjNamespace.Folders)
'
'                            If Not objDestinationFolder Is Nothing Then
'
'                                objPathToFolderObjectDic.Add strDestinationPathPart, objDestinationFolder
'                            Else
'                                MsgBox "Probably, the Mail folder hasn't been created yet;" & vbNewLine & strDestinationFolderPath
'                            End If
'                        End If
'                    End With
'                Else
'                    MsgBox "No TransportDestination key-value setting in " & CStr(varCondition1stKey), vbExclamation Or vbOKOnly
'                End If
'            End With
'        Next
'    End With
'
'    Set mfobjGetMailFolderPathPartToFolderObjectDicFromMailTrasportingDetailSectionToKeyValueDic = objPathToFolderObjectDic
'End Function
'
'
''''
''''
''''
'Private Sub msubFindFirstSatisfyingDetailConditionSet(ByRef rblnIsASatisfiedConditionSetFound As Boolean, _
'        ByRef rstrDestinationPathPart As String, _
'        ByRef rstrDescription As String, _
'        ByRef robjMailItem As Outlook.MailItem, _
'        ByRef robjCondition1stKeyToConditionSetDic As Scripting.Dictionary, _
'        ByRef robjCondition1stKeyToSecondLaterConditionsDic As Scripting.Dictionary, _
'        ByRef robjCondition1stKeyToDestinationPathPartDic As Scripting.Dictionary, _
'        ByRef robjCondition1stKeyToDescriptionDic As Scripting.Dictionary)
'
'
'    Dim varCondition1stKey As Variant
'    Dim objEMailClassifyingCondition As EMailClassifyingCondition, objFindingKeyWords As Collection
'    Dim objSecondLaterConditions As Collection, objConditionSet  As Collection, varConditionSet As Variant
'    Dim blnAre2ndConditionsSatisfied As Boolean
'
'    Dim strTmpDescription As String
'
'    With robjCondition1stKeyToConditionSetDic
'
'        For Each varCondition1stKey In .Keys
'
'            msubGetClassifyingMatchConditionSet objEMailClassifyingCondition, objFindingKeyWords, .Item(varCondition1stKey)
'
'            ' Whether the first condition is satisfied or not
'
'            If mfblnIsDetailAConditionSatisfied(robjMailItem, objEMailClassifyingCondition, objFindingKeyWords) Then
'
'                blnAre2ndConditionsSatisfied = True
'
''                strTmpDescription = robjCondition1stKeyToDescriptionDic.Item(varCondition1stKey)
''
''                If InStr(1, strTmpDescription, "<Description>") > 0 Then
''
''                    Debug.Print "Mail sorting description - breaking point"
''                End If
'
'                Set objSecondLaterConditions = robjCondition1stKeyToSecondLaterConditionsDic.Item(varCondition1stKey)
'
'                If objSecondLaterConditions.Count > 0 Then
'
'                    For Each varConditionSet In objSecondLaterConditions
'
'                        Set objConditionSet = varConditionSet
'
'                        msubGetClassifyingMatchConditionSet objEMailClassifyingCondition, objFindingKeyWords, objConditionSet
'
'                        If Not mfblnIsDetailAConditionSatisfied(robjMailItem, objEMailClassifyingCondition, objFindingKeyWords) Then
'
'                            blnAre2ndConditionsSatisfied = False
'
'                            Exit For
'                        End If
'                    Next
'                End If
'
'                If blnAre2ndConditionsSatisfied Then
'
'                    ' Need to move
'
'                    rstrDescription = robjCondition1stKeyToDescriptionDic.Item(varCondition1stKey)
'
'                    rstrDestinationPathPart = robjCondition1stKeyToDestinationPathPartDic.Item(varCondition1stKey)
'
'                    Debug.Print "! Need to move - [" & robjMailItem.Subject & "] for " & rstrDescription
'
'                    rblnIsASatisfiedConditionSetFound = True ' Need To Move
'
'                    Exit For
'                End If
'            End If
'        Next
'    End With
'End Sub
'
'
'
''''
'''' a finding base
''''
'Private Function mfblnIsDetailAConditionSatisfied(ByRef robjMailItem As Outlook.MailItem, _
'        ByRef robjEMailClassifyingCondition As EMailClassifyingCondition, _
'        ByRef robjFindingKeyWords As Collection) As Boolean
'
'
'    Dim enmMatchingTextBoolCondition As MatchingTextBoolCondition, i As Long
'
'    Dim varFindingKeyWord As Variant, strFindingKeyWord As String
'
'    Dim blnIsOneKeywordMatched As Boolean
'
'    Dim blnAllConditionsSatisfied As Boolean
'
'
'    blnAllConditionsSatisfied = True
'
'    With robjEMailClassifyingCondition
'
'        enmMatchingTextBoolCondition = .SearchingMatchingTextBoolCondition
'
'        i = 1
'
'        For Each varFindingKeyWord In robjFindingKeyWords
'
'            strFindingKeyWord = varFindingKeyWord
'
'            blnIsOneKeywordMatched = False
'
'            Select Case .SearchingMailItemPropertyType
'
'                Case OutlookMailItemPropertyType.olmSenderEmailAddress
'
'                    blnIsOneKeywordMatched = IsOneKeywordMatchedAboutATextString(robjMailItem.SenderEmailAddress, _
'                            strFindingKeyWord, _
'                            .NeedToUseLowerCase, _
'                            .NeedToExactMatch)
'
'                Case OutlookMailItemPropertyType.olmNameOfSenderObject
'
'                    If Not robjMailItem.Sender Is Nothing Then
'
'                        blnIsOneKeywordMatched = IsOneKeywordMatchedAboutATextString(robjMailItem.Sender.Name, _
'                                strFindingKeyWord, _
'                                .NeedToUseLowerCase, _
'                                .NeedToExactMatch)
'                    End If
'
'                Case OutlookMailItemPropertyType.olmSubject
'
'                    blnIsOneKeywordMatched = IsOneKeywordMatchedAboutATextString(robjMailItem.Subject, _
'                            strFindingKeyWord, _
'                            .NeedToUseLowerCase, _
'                            .NeedToExactMatch)
'
'                Case OutlookMailItemPropertyType.olmBody
'
'                    blnIsOneKeywordMatched = IsOneKeywordMatchedAboutATextString(robjMailItem.Body, _
'                            strFindingKeyWord, _
'                            .NeedToUseLowerCase, _
'                            .NeedToExactMatch)
'
'                Case OutlookMailItemPropertyType.olmUnRead
'
'                    If robjMailItem.UnRead = CBool(varFindingKeyWord) Then
'
'                        blnIsOneKeywordMatched = True
'                    End If
'
'            End Select
'
'            AreAllMatchingTextConditionsSatisfied blnAllConditionsSatisfied, _
'                    enmMatchingTextBoolCondition, _
'                    blnIsOneKeywordMatched, _
'                    i, _
'                    robjFindingKeyWords.Count
'
'            If enmMatchingTextBoolCondition = MatchingTextBoolOrOp And blnIsOneKeywordMatched Then
'
'                ' blnAllConditionsSatisfied Is True
'
'                Exit For
'            End If
'
'            If Not blnAllConditionsSatisfied Then
'
'                Exit For
'            End If
'
'            i = i + 1
'        Next
'    End With
'
'
'    mfblnIsDetailAConditionSatisfied = blnAllConditionsSatisfied
'End Function
'
'
'
'
''''
''''
''''
'''' <Argument>robjCondition1stKeyToConditionSetDic: Output - Nested dictionary - Dictionary(Of Key[Section], Of Dictionary(Of Key[KeyInSection], Value))</Argument>
'''' <Argument>robjCondition1stKeyToSecondLaterConditionsDic: Output</Argument>
'''' <Argument>robjCondition1stKeyToDestinationPathPartDic: Output</Argument>
'''' <Argument>robjCondition1stKeyToDescriptionDic: Output</Argument>
'''' <Argument>robjSectionToKeyValueDic: Input</Argument>
'''' <Argument>robjNameSpace: Input</Argument>
'''' <Argument>vstrConditionDelimiter: Output</Argument>
'''' <Argument>vstrCondition2ndDelimiter: Output</Argument>
'Private Sub msubInitializeCondition1stKeyToConditionSetDic(ByRef robjCondition1stKeyToConditionSetDic As Scripting.Dictionary, _
'        ByRef robjCondition1stKeyToSecondLaterConditionsDic As Scripting.Dictionary, _
'        ByRef robjCondition1stKeyToDestinationPathPartDic As Scripting.Dictionary, _
'        ByRef robjCondition1stKeyToDescriptionDic As Scripting.Dictionary, _
'        ByRef robjSectionToKeyValueDic As Scripting.Dictionary, _
'        ByRef robjNameSpace As Outlook.Namespace, _
'        Optional ByVal vstrConditionDelimiter As String = ";", _
'        Optional ByVal vstrCondition2ndDelimiter As String = "-")
'
'
'    Dim varCondition1stKey As Variant, strCondition1st As String, str1stConditions() As String
'
'    Dim i As Long, j As Long
'
'    Dim varMatchingCondition As Variant, strMatchingCondition As String
'
'    Dim objChildKeyValueDic As Scripting.Dictionary, varKey As Variant, strKey As String, varFindingKeyWord As Variant
'
'    Dim objEMailClassifyingCondition As EMailClassifyingCondition
'
'    Dim objFindingKeyWords As Collection
'
'    Dim objConditionSet As Collection, objSecondLaterConditions As Collection
'
'    Dim strDestinationPathPart As String, strDescription As String
'
'
'
'    If robjCondition1stKeyToConditionSetDic Is Nothing Then Set robjCondition1stKeyToConditionSetDic = New Scripting.Dictionary
'
'    If robjCondition1stKeyToSecondLaterConditionsDic Is Nothing Then Set robjCondition1stKeyToSecondLaterConditionsDic = New Scripting.Dictionary
'
'    If robjCondition1stKeyToDestinationPathPartDic Is Nothing Then Set robjCondition1stKeyToDestinationPathPartDic = New Scripting.Dictionary
'
'    If robjCondition1stKeyToDescriptionDic Is Nothing Then Set robjCondition1stKeyToDescriptionDic = New Scripting.Dictionary
'
'
'
'    With robjSectionToKeyValueDic
'
'        For Each varCondition1stKey In .Keys
'
'            msubSetClassifyingMatchConditionSetFrom1stStringConditionKey objConditionSet, _
'                    varCondition1stKey, _
'                    robjNameSpace, _
'                    vstrConditionDelimiter, _
'                    vstrCondition2ndDelimiter
'
'            robjCondition1stKeyToConditionSetDic.Add varCondition1stKey, _
'                    objConditionSet
'
'
'            Set objChildKeyValueDic = .Item(varCondition1stKey)
'
'            msubSetClassifyingMatchConditionSetFrom2ndChildKeyValueDic objSecondLaterConditions, _
'                    strDestinationPathPart, _
'                    strDescription, _
'                    objChildKeyValueDic, _
'                    robjNameSpace, _
'                    vstrConditionDelimiter, _
'                    vstrCondition2ndDelimiter
'
'            robjCondition1stKeyToSecondLaterConditionsDic.Add varCondition1stKey, _
'                    objSecondLaterConditions
'
'            robjCondition1stKeyToDestinationPathPartDic.Add varCondition1stKey, _
'                    strDestinationPathPart
'
'            robjCondition1stKeyToDescriptionDic.Add varCondition1stKey, _
'                    strDescription
'        Next
'    End With
'End Sub
'
'
''''
''''
''''
'''' <Argument>robjSecondLaterConditions: Output</Argument>
'''' <Argument>rstrDestinationPathPart: Output</Argument>
'''' <Argument>rstrDescription: Output</Argument>
'''' <Argument>vobjChildKeyValueDic: Input</Argument>
'''' <Argument>robjNameSpace: Input</Argument>
'''' <Argument>vstrConditionDelimiter: Input</Argument>
'''' <Argument>vstrCondition2ndDelimiter: Input</Argument>
'Private Sub msubSetClassifyingMatchConditionSetFrom2ndChildKeyValueDic(ByRef robjSecondLaterConditions As Collection, _
'        ByRef rstrDestinationPathPart As String, _
'        ByRef rstrDescription As String, _
'        ByVal vobjChildKeyValueDic As Scripting.Dictionary, _
'        ByRef robjNameSpace As Outlook.Namespace, _
'        Optional ByVal vstrConditionDelimiter As String = ";", _
'        Optional ByVal vstrCondition2ndDelimiter As String = "-")
'
'
'    Dim objEMailClassifyingCondition As EMailClassifyingCondition, objFindingKeyWords As Collection
'    Dim varKey As Variant, strKey As String, objConditionSet As Collection
'    Dim varFindingKeyWord As Variant, strFindingKeyWord As String
'
'
'    Set robjSecondLaterConditions = New Collection
'
'    With vobjChildKeyValueDic
'
'        For Each varKey In .Keys
'
'            strKey = varKey
'
'            Set objEMailClassifyingCondition = Nothing: Set objFindingKeyWords = Nothing
'
'            Select Case True
'
'                Case StrComp("TransportDestination", strKey) = 0
'
'                    rstrDestinationPathPart = .Item(varKey)
'
'                Case StrComp("Description", strKey) = 0
'
'                    rstrDescription = .Item(varKey)
'
'                Case Else
'
'                    Set objEMailClassifyingCondition = GetEmailClassifyingConditionFromKeyText(strKey, vstrCondition2ndDelimiter)
'
'                    For Each varFindingKeyWord In Split(CStr(.Item(varKey)), vstrConditionDelimiter)
'
'                        If objFindingKeyWords Is Nothing Then Set objFindingKeyWords = New Collection
'
'                        ' ReceiverMailAddressMisrepresentation
'
'                        strFindingKeyWord = mfstrGetRawKeyWordOrConvertedKeyWord(CStr(varFindingKeyWord), robjNameSpace)
'
'                        objFindingKeyWords.Add strFindingKeyWord
'                    Next
'
'                    msubSetClassifyingMatchConditionSet objConditionSet, _
'                            objEMailClassifyingCondition, _
'                            objFindingKeyWords
'
'                    robjSecondLaterConditions.Add objConditionSet
'            End Select
'        Next
'    End With
'End Sub
'
''''
''''
''''
'''' <Argument>vstrFindingKeyWord: Input</Argument>
'''' <Argument>robjNameSpace: Input</Argument>
'''' <Return>String: raw or converted</Return>
'Private Function mfstrGetRawKeyWordOrConvertedKeyWord(ByVal vstrFindingKeyWord As String, _
'        ByRef robjNameSpace As Outlook.Namespace) As String
'
'    Dim strFindingWord As String
'
'    Dim strCurrentUserEMailAddress As String
'
'    Dim strCurrentUserFirstPartOfEMailAddress As String
'
'
'    strFindingWord = vstrFindingKeyWord
'
'    Select Case UCase(vstrFindingKeyWord)
'
'        Case UCase(mstrFirstPartOfReceiverMailAddressMisrepresentation)
'
'            strFindingWord = Left(robjNameSpace.CurrentUser.Address, InStr(1, robjNameSpace.CurrentUser.Address, "@") - 1)
'
'        Case UCase(mstrAllOfReceiverMailAddressMisrepresentation)
'
'            strFindingWord = robjNameSpace.CurrentUser.Address
'
'        Case UCase(mstrReceiverMailAddressMisrepresentationKey)
'
'            strFindingWord = Left(robjNameSpace.CurrentUser.Address, InStr(1, robjNameSpace.CurrentUser.Address, "@") - 1)
'    End Select
'
'    mfstrGetRawKeyWordOrConvertedKeyWord = strFindingWord
'End Function
'
'
''''
'''' for 1st Section-key
''''
'''' <Argument>robjConditionSet: Output - collection including both robjEMailClassifyingCondition and robjFindingKeyWords</Argument>
'''' <Argument>vstrCondition1stKey: Input</Argument>
'''' <Argument>robjNameSpace: Input</Argument>
'''' <Argument>vstrConditionDelimiter: Input</Argument>
'''' <Argument>vstrCondition2ndDelimiter: Input</Argument>
'Private Sub msubSetClassifyingMatchConditionSetFrom1stStringConditionKey(ByRef robjConditionSet As Collection, _
'        ByVal vstrCondition1stKey As String, _
'        ByRef robjNameSpace As Outlook.Namespace, _
'        Optional ByVal vstrConditionDelimiter As String = ";", _
'        Optional ByVal vstrCondition2ndDelimiter As String = "-")
'
'    Dim str1stConditions() As String, i As Long
'
'    Dim objFindingKeyWords As Collection, objEMailClassifyingCondition As EMailClassifyingCondition
'
'    Dim strFindingKeyWord As String
'
'
'    str1stConditions = Split(vstrCondition1stKey, vstrConditionDelimiter)
'
'    Set objFindingKeyWords = Nothing
'
'    For i = LBound(str1stConditions) To UBound(str1stConditions)
'
'        If i = LBound(str1stConditions) Then
'
'            Set objEMailClassifyingCondition = GetEmailClassifyingConditionFromKeyText(str1stConditions(i), vstrCondition2ndDelimiter)
'        Else
'
'            If objFindingKeyWords Is Nothing Then Set objFindingKeyWords = New Collection
'
'            ' ReceiverMailAddressMisrepresentation
'
'            strFindingKeyWord = mfstrGetRawKeyWordOrConvertedKeyWord(str1stConditions(i), robjNameSpace)
'
'            objFindingKeyWords.Add strFindingKeyWord
'        End If
'    Next
'
'    msubSetClassifyingMatchConditionSet robjConditionSet, objEMailClassifyingCondition, objFindingKeyWords
'End Sub
'
''''
'''' get both robjEMailClassifyingCondition and robjFindingKeyWords
''''
'''' <Argument>robjEMailClassifyingCondition: Output</Argument>
'''' <Argument>robjFindingKeyWords: Output</Argument>
'''' <Argument>robjConditionSet: Input</Argument>
'Private Sub msubGetClassifyingMatchConditionSet(ByRef robjEMailClassifyingCondition As EMailClassifyingCondition, _
'        ByRef robjFindingKeyWords As Collection, _
'        ByRef robjConditionSet As Collection)
'
'    With robjConditionSet
'
'        Set robjEMailClassifyingCondition = .Item("EMailClassifyingCondition")
'
'        Set robjFindingKeyWords = .Item("FindingKeyWords")
'    End With
'End Sub
'
''''
'''' get robjConditionSet
''''
'''' <Argument>robjConditionSet: Output - collection including both robjEMailClassifyingCondition and robjFindingKeyWords</Argument>
'''' <Argument>robjEMailClassifyingCondition: Input</Argument>
'''' <Argument>robjFindingKeyWords: Input</Argument>
'Private Sub msubSetClassifyingMatchConditionSet(ByRef robjConditionSet As Collection, _
'        ByRef robjEMailClassifyingCondition As EMailClassifyingCondition, _
'        ByRef robjFindingKeyWords As Collection)
'
'    Set robjConditionSet = New Collection
'
'    With robjConditionSet
'
'        .Add robjEMailClassifyingCondition, "EMailClassifyingCondition"
'
'        .Add robjFindingKeyWords, "FindingKeyWords"
'    End With
'End Sub
'
'
''**---------------------------------------------
''** Visualize INI file state with Outlook Personal box
''**---------------------------------------------
''''
''''
''''
'Public Function GetDetailConditionInformationOfMailTransPortINIFileWithoutPersonalOutlookBoxCurrentInfo(ByVal vstrvstrDetailConditionsINIFilePath As String) As Collection
'
'    Dim objDummyDic As Scripting.Dictionary
'
'    Set GetDetailConditionInformationOfMailTransPortINIFileWithoutPersonalOutlookBoxCurrentInfo = _
'            GetDetailConditionInformationOfMailTransportingByINIFile(objDummyDic, vstrvstrDetailConditionsINIFilePath)
'End Function
'
'
'
''''
''''
''''
'Public Function GetDetailConditionInformationOfMailTransportingByINIFile(ByRef robjNotExistedMailFolderPathDic As Scripting.Dictionary, _
'        ByVal vstrDetailConditionsINIFilePath As String, Optional ByVal vstrPersonalOutlookBoxName As String = "") As Collection
'
'    Dim objSectionToKeyValueDic As Scripting.Dictionary
'
'    With New Scripting.FileSystemObject
'
'        If .FileExists(vstrDetailConditionsINIFilePath) Then
'
'            Set objSectionToKeyValueDic = GetSectionToKeyValueDicFromIni(vstrDetailConditionsINIFilePath)
'        End If
'    End With
'
'    Set GetDetailConditionInformationOfMailTransportingByINIFile = GetDetailConditionInformationOfMailTransporting(robjNotExistedMailFolderPathDic, _
'            objSectionToKeyValueDic, _
'            vstrPersonalOutlookBoxName)
'End Function
'
''''
''''
''''
'Public Function GetDetailConditionInformationOfMailTransporting(ByRef robjNotExistedMailFolderPathDic As Scripting.Dictionary, _
'        ByRef robjSectionToKeyValueDic As Scripting.Dictionary, _
'        Optional ByVal vstrPersonalOutlookBoxName As String = "") As Collection
'
'
'    Dim objDTCol As Collection, var1stKey As Variant, var2ndKey As Variant, objChildDic As Scripting.Dictionary, intCountOfConditions As Long
'
'    Dim objRowCol As Collection
'
'    Dim strDestinationPathPart As String, strClassifyingRuleName As String
'
'    Dim blnAddToOutlookPersonalOutlookBoxState As Boolean, objOutlookApplication As Outlook.Application, objNamespace As Outlook.Namespace
'
'    Dim objFolder As Outlook.Folder, strMailFolderPath As String
'
'    Dim i As Long
'
'
'    Set objDTCol = New Collection
'
'    With robjSectionToKeyValueDic
'
'        If .Count > 0 Then
'
'            blnAddToOutlookPersonalOutlookBoxState = False
'
'            If vstrPersonalOutlookBoxName <> "" Then
'
'                blnAddToOutlookPersonalOutlookBoxState = True
'
'                Set objOutlookApplication = New Outlook.Application
'
'                Set objNamespace = objOutlookApplication.GetNamespace("MAPI")
'
'                If robjNotExistedMailFolderPathDic Is Nothing Then
'
'                    Set robjNotExistedMailFolderPathDic = New Scripting.Dictionary
'                End If
'            End If
'        End If
'
'        For Each var1stKey In .Keys
'
'            intCountOfConditions = 1
'
'            Set objChildDic = .Item(var1stKey)
'
'            With objChildDic
'
'                For Each var2ndKey In .Keys
'
'                    Select Case var2ndKey
'
'                        Case "TransportDestination"
'
'                            strDestinationPathPart = .Item(var2ndKey)
'
'                        Case "Description"
'
'                            strClassifyingRuleName = .Item(var2ndKey)
'                        Case Else
'
'                            intCountOfConditions = intCountOfConditions + 1
'                    End Select
'                Next
'            End With
'
'            Set objRowCol = New Collection
'
'            With objRowCol
'
'                .Add strClassifyingRuleName
'
'                .Add strDestinationPathPart
'
'                .Add intCountOfConditions
'
'                If blnAddToOutlookPersonalOutlookBoxState Then
'
'                    strMailFolderPath = "\\" & vstrPersonalOutlookBoxName & "\" & strDestinationPathPart
'
'                    Set objFolder = GetMailFolder(strMailFolderPath, objNamespace.Folders)
'
'                    If Not objFolder Is Nothing Then
'
'                        .Add True
'
'                        .Add objFolder.Items.Count
'                    Else
'
'                        .Add False
'
'                        .Add Empty
'
'                        With robjNotExistedMailFolderPathDic
'
'                            If Not .Exists(strMailFolderPath) Then
'
'                                .Add strMailFolderPath, vstrPersonalOutlookBoxName
'                            End If
'                        End With
'                    End If
'                End If
'            End With
'
'            objDTCol.Add objRowCol
'        Next
'    End With
'
'
'    If Not robjNotExistedMailFolderPathDic Is Nothing Then
'
'        If robjNotExistedMailFolderPathDic.Count > 0 Then
'
'            ' The user message prompt will be shown. if the user want it, the sub-folders are to be created.
'
'            CreateMailFoldersFromMailFolderPathDicIfTheUserNeedsIts robjNotExistedMailFolderPathDic, objOutlookApplication
'        End If
'    End If
'
'
'    Set objOutlookApplication = Nothing
'
'    Set GetDetailConditionInformationOfMailTransporting = objDTCol
'End Function
'
'''--VBA_Code_File--<OperateOutlookForXl.bas>--
'Attribute VB_Name = "OperateOutlookForXl"
''
''   Tools to operate Microsoft Outlook and output results to Excel work-sheet
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on both Outlook and Excel
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Sat, 15/Apr/2023    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''**---------------------------------------------
''** Moving outlook e-mails logs
''**---------------------------------------------
''''
'''' simple e-mail sorting rule and output some results to a Excel.Worksheet object
''''
'Public Sub MoveMailsToSpecifiedFolderFromDestinationConditionWithOutputtingResultLogToSheet(ByVal vstrSourceMailFolderPath As String, _
'        ByVal vobjMailAddressConditionToSomeInfoDic As Scripting.Dictionary, _
'        ByVal vobjConditionKeyToDescriptionDic As Scripting.Dictionary, _
'        ByVal vstrConditionFieldTitlesDelimitedByComma As String, _
'        ByVal vstrPersonalOutlookBoxName As String, _
'        ByVal vobjDestinationPathPartCol As Collection, _
'        Optional ByVal vblnAllowToGetMovingMailLogAsTable As Boolean = True, _
'        Optional ByVal vblnAllowToCollectMovedMailBodyHyperlinksLogAsTable As Boolean = False, _
'        Optional ByVal vblnPreventExecutingToMoveMail As Boolean = False)
'
'
'    Dim objEMailMovedPersonalBoxLogs As EMailMovedPersonalBoxLogs, strOutputBookPath As String
'
'
'    SetupEMailMovedPersonalBoxLogs objEMailMovedPersonalBoxLogs, vblnAllowToGetMovingMailLogAsTable, vblnAllowToCollectMovedMailBodyHyperlinksLogAsTable
'
'    MoveMailsToSpecifiedFolderFromDestinationCondition objEMailMovedPersonalBoxLogs, vstrSourceMailFolderPath, vobjMailAddressConditionToSomeInfoDic, vobjConditionKeyToDescriptionDic, vstrConditionFieldTitlesDelimitedByComma, vstrPersonalOutlookBoxName, vobjDestinationPathPartCol, vblnPreventExecutingToMoveMail
'
'    ' Output logs to a sheet
'
'    strOutputBookPath = GetLoggingBookPathForMovingOutlookEMails(vblnPreventExecutingToMoveMail)
'
'    OutputEMailMovedPersonalBoxLogsToSheet objEMailMovedPersonalBoxLogs, strOutputBookPath, vblnPreventExecutingToMoveMail
'End Sub
'
''''
''''
''''
'Public Sub OutputEMailMovedPersonalBoxLogsToSheet(ByRef robjEMailMovedPersonalBoxLogs As EMailMovedPersonalBoxLogs, ByVal vstrOutputBookPath As String, Optional ByVal vblnPreventExecutingToMoveMail As Boolean = False, Optional ByVal vstrTitleTextOfMainMovingProcessLog As String = "Moving E-mails logs")
'
'    Dim objSheet As Excel.Worksheet, objExtractedHyperlinksSheet As Excel.Worksheet
'
'    With robjEMailMovedPersonalBoxLogs
'
'        If .AllowToGetMovingBasicMailLogAsTable Then
'
'            Set objSheet = OutputMovingMailLogDTToSheet(.MovingBasicMailLogDTCol, vstrOutputBookPath)
'
'            OutputDicToAutoShapeLogTextOnSheet objSheet, .MainMovingProcessLogDic, vstrTitleTextOfMainMovingProcessLog
'
'            If vblnPreventExecutingToMoveMail Then
'
'                AddAutoShapeGeneralMessage objSheet, "Now, their mails haven't been moved yet.", XlShapeTextLogInteriorFillGradientYellow
'            End If
'
'            If .AllowToCollectMovedMailBodyHyperlinksLogAsTable Then
'
'                Set objExtractedHyperlinksSheet = GetNewWorksheetAfterSpecifiedSheets(objSheet, "ExtractedMailBodyTextInfos")
'
'                OutputExtractedMailBodyInfosAndSomeInfoDTColToSheet objExtractedHyperlinksSheet, .ExtractedMovedMailBodySomeInfoLogDTCol
'            End If
'        Else
'
'            OutputEMailMovedPersonalBoxLogsToImmediateWindow robjEMailMovedPersonalBoxLogs
'        End If
'    End With
'End Sub
'
'
'
''''
''''
''''
'Public Function OutputMovingMailLogDTToSheet(ByVal vobjMovingMailLogDTCol As Collection, ByVal vstrOutputBookPath As String) As Excel.Worksheet
'
'    Dim objSheet As Excel.Worksheet
'
'    Set objSheet = GetWorkbookAndAnWorksheetPrepareForUnitTest(vstrOutputBookPath, "MovingEMails")
'
'    OutputColToSheetRestrictedByDataTableSheetFormatter objSheet, vobjMovingMailLogDTCol, mfobjGetDTSheetFormatterForMovingMailLogDT()
'
'    SortXlSheetBasically objSheet, "SenderEmailAddress,Ascending,ReceivedTime,Decending"
'
'    SetMergedCellsToTable objSheet, GetColFromLineDelimitedChar("DestinationPathPart,Description,SenderEmailAddress,SenderOfName,UnRead,Subject"), 1, 1, True
'
'    Set OutputMovingMailLogDTToSheet = objSheet
'End Function
'
''''
''''
''''
'''' <Argument>vblnPreventExecutingToMoveMail: If the e-mail matching tests have been executed, then this will be True</Argument>
'Public Function GetLoggingBookPathForMovingOutlookEMails(Optional ByVal vblnPreventExecutingToMoveMail As Boolean = False) As String
'
'    Dim strDir As String, strBookName As String, strPath As String
'
'    strDir = GetCurrentBookOutputDir() & "\OutlookLogs"
'
'    If vblnPreventExecutingToMoveMail Then
'
'        strDir = strDir & "\CheckToMailDestinations"
'
'        strBookName = "ChecksMailDestinations_" & Format(Now(), "yyyymmdd_hhmmss") & ".xlsx"
'    Else
'        strDir = strDir & "\MovedMailsLogs"
'
'        strBookName = "MovedMails_" & Format(Now(), "yyyymmdd_hhmmss") & ".xlsx"
'    End If
'
'    ForceToCreateDirectory strDir
'
'    strPath = strDir & "\" & strBookName
'
'    GetLoggingBookPathForMovingOutlookEMails = strPath
'End Function
'
'
''''
''''
''''
'Public Function GetFieldTitleToColumnWidthStringForMailTypicalProperties() As String
'
'    GetFieldTitleToColumnWidthStringForMailTypicalProperties = "SenderEmailAddress,41,SenderOfName,16,UnRead,10,ReceivedTime,20,Subject,20,BodyLeftPart,20"
'End Function
'
'
''''
''''
''''
'Private Function mfobjGetDTSheetFormatterForMovingMailLogDT() As DataTableSheetFormatter
'
'    Dim objDTSheetFormatter As DataTableSheetFormatter, objFormatConditionExpander As FormatConditionExpander
'
'    Set objDTSheetFormatter = New DataTableSheetFormatter
'
'    With objDTSheetFormatter
'
'        .FieldTitleInteriorType = ColumnNameInteriorOfGatheredColTable
'
'        Set .FieldTitleToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("DestinationPathPart,39,Description,41," & GetFieldTitleToColumnWidthStringForMailTypicalProperties())
'
'
'
'        Set .ColumnsNumberFormatLocal = New ColumnsNumberFormatLocalParam
'
'        With .ColumnsNumberFormatLocal
'
'            .SetNumberFormatLocalAndApplyFieldTitles "yyyy/m/d ""(""aaa"")"" hh:mm:ss", GetColFromLineDelimitedChar("ReceivedTime")
'        End With
'
'        Set .ColumnsFormatCondition = New ColumnsFormatConditionParam
'
'        With .ColumnsFormatCondition
'
'            Set objFormatConditionExpander = New FormatConditionExpander
'
'            With objFormatConditionExpander
'
'                .AddTextContainCondition "False", FontDeepGreenBgLightGreen
'            End With
'
'            .FieldTitleToCellFormatCondition.Add "UnRead", objFormatConditionExpander
'        End With
'
'
''        .AllowToMergeCellsByContinuousSameValues = True
''        .AllowToSetVerticalTopAlignmentTopOfMergeCells = True
''
''        Set .FieldTitlesToMergeCells = GetColFromLineDelimitedChar("DestinationPathPart,Description,SenderEmailAddress,SenderOfName,UnRead,Subject")
'
'        .RecordBordersType = RecordCellsGrayAll
'
'    End With
'
'
'    Set mfobjGetDTSheetFormatterForMovingMailLogDT = objDTSheetFormatter
'End Function
'
''**---------------------------------------------
''** About outlook mail folders data-table
''**---------------------------------------------
''''
'''' for Outlook mail Folder detail information
''''
'Public Function GetDataTableSheetFormatterForOutlookMailFolderPath(Optional ByVal vblnCollectMailDetailInfo As Boolean = False) As DataTableSheetFormatter
'
'    Dim objDTSheetFormatter As DataTableSheetFormatter
'
'    Set objDTSheetFormatter = New DataTableSheetFormatter
'
'    With objDTSheetFormatter
'
'        If vblnCollectMailDetailInfo Then
'
'            Set .FieldTitleToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("Name,36,MailsCount,8,UnReadItemCount,8,FolderPath,86,EntryID,10,Description,10,DefaultItemType,10")
'        Else
'
'            Set .FieldTitleToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("MailFolderPaths,86")
'        End If
'
'        .FieldTitleInteriorType = ColumnNameInteriorOfGatheredColTable
'
'        .RecordBordersType = RecordCellsGrayAll
'
'    End With
'
'    Set GetDataTableSheetFormatterForOutlookMailFolderPath = objDTSheetFormatter
'End Function
'
'
'
''''
'''' for Outlook mail Folder existence checks
''''
'Public Function GetDataTableSheetFormatterForOutlookMailFolderExistence() As DataTableSheetFormatter
'
'    Dim objDTSheetFormatter As DataTableSheetFormatter, objFormatConditionExpander As FormatConditionExpander
'
'    Set objDTSheetFormatter = New DataTableSheetFormatter
'
'    With objDTSheetFormatter
'
'        .FieldTitleInteriorType = ColumnNameInteriorOfGatheredColTable
'
'        Set .FieldTitleToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("OutlookMailFolderPath,65,FolderExists,10,CountOfMails,11")
'
'
'        Set .ColumnsFormatCondition = New ColumnsFormatConditionParam
'
'        With .ColumnsFormatCondition
'
'            Set objFormatConditionExpander = New FormatConditionExpander
'
'            With objFormatConditionExpander
'
'                .CondtionText = "False"
'
'                .CellFormatConditionType = CellBackground.FormatConditionTextContain
'
'                .CellColorArrangementType = FontDeepRedBgLightRed
'            End With
'
'            .FieldTitleToCellFormatCondition.Add "FolderExists", objFormatConditionExpander
'
'        End With
'
'        .RecordBordersType = RecordCellsGrayAll
'    End With
'
'    Set GetDataTableSheetFormatterForOutlookMailFolderExistence = objDTSheetFormatter
'End Function
'
'
'''--VBA_Code_File--<OperateOutlookToClassifyForXl.bas>--
'Attribute VB_Name = "OperateOutlookToClassifyForXl"
''
''   Tools to operate Microsoft Outlook using both Excel and Windows API, which serves Windows INI files
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on all of Outlook, Excel and Windows API(serving INI files)
''       Dependent on ADODB
''       Dependent on XlAdoConnector.cls
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Sat,  6/May/2023    Kalmclaeyd Tarclanus    Separated from OperateOutlookToClassify.bas
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''**---------------------------------------------
''** Align and decorate the section-key-value table of Excel book, which is compatible INI file data
''**---------------------------------------------
''''
''''
''''
'Public Sub PadSameAboveValuesOnEmptyCellsForEachColumnOnWorksheetAboutSectionColumnAtActiveCell()
'
'    Dim objRange As Excel.Range, objSheet As Excel.Worksheet
'
'    Set objRange = ActiveCell
'
'    With objRange
'
'        PadSameAboveValuesOnEmptyCellsForEachColumnOnWorksheetAboutSectionColumn .Worksheet, .CurrentRegion.Row, .CurrentRegion.Column, True, True
'    End With
'
'    Set objSheet = objRange.Worksheet
'
'    DecorateGridSimply objSheet, ManualDecoratingFieldTitlesRangePattern.WhiteFontGrayBgMeiryo
'End Sub
'
''''
''''
''''
'Public Sub PadSameAboveValuesOnEmptyCellsForEachColumnOnWorksheetAboutSectionColumn(ByVal vobjSheet As Excel.Worksheet, Optional ByVal vintFieldTitlesRowIndex As Long = 1, Optional ByVal vintTopLeftColumnIndex As Long = 1, Optional ByVal vblnAllowToMergeCellsAfterPadding As Boolean = False, Optional ByVal vblnAllowToSetVerticalAlignmentTop As Boolean = False)
'
'    PadSameAboveValuesOnEmptyCellsForEachColumnOnWorksheet vobjSheet, GetColAsDateFromLineDelimitedChar("Section"), vintFieldTitlesRowIndex, vintTopLeftColumnIndex, vblnAllowToMergeCellsAfterPadding, vblnAllowToSetVerticalAlignmentTop
'End Sub
'
'
''**---------------------------------------------
''** Ini file interface
''**---------------------------------------------
''''
''''
''''
'Public Sub MoveMailsBySpecifiedDetailConditionsFromIniFileWithOutputtingResultLogToSheet(ByVal vstrSourceMailFolderPath As String, _
'        ByVal vstrPersonalOutlookBoxName As String, _
'        ByVal vstrIniPath As String, _
'        Optional ByVal vblnAllowToGetMovingMailLogAsTable As Boolean = True, _
'        Optional ByVal vblnPreventExecutingToMoveMail As Boolean = False)
'
'    Dim objSectionToKeyValueDic As Scripting.Dictionary
'
'    ' !Attention - The Chinese Kanji characters should have been lost in Japanese locale Windows OS.
'
'    Set objSectionToKeyValueDic = GetSectionToKeyValueDicFromIni(vstrIniPath)
'
'    MoveMailsBySpecifiedDetailConditionsWithOutputtingResultLogToSheet vstrSourceMailFolderPath, vstrPersonalOutlookBoxName, objSectionToKeyValueDic, vblnAllowToGetMovingMailLogAsTable, vblnPreventExecutingToMoveMail
'End Sub
'
'
''**---------------------------------------------
''** Move mail-items by detailed conditions
''**---------------------------------------------
''''
''''
''''
'Public Sub MoveMailsBySpecifiedDetailConditionsFromExcelBookWithOutputtingResultLogToSheet(ByVal vstrSourceMailFolderPath As String, _
'        ByVal vstrPersonalOutlookBoxName As String, _
'        ByVal vstrBookPath As String, _
'        Optional ByVal vstrSheetName As String = "DetailConditions", _
'        Optional ByVal vblnAllowToGetMovingMailLogAsTable As Boolean = True, _
'        Optional ByVal vblnAllowToCollectMovedMailBodyHyperlinksLogAsTable As Boolean = False, _
'        Optional ByVal vblnPreventExecutingToMoveMail As Boolean = False)
'
'    Dim objSectionToKeyValueDic As Scripting.Dictionary
'
'
'    Set objSectionToKeyValueDic = GetSectionToKeyValueDicFromThreeColumnsFormattedExcelBookByAdoSQLWithSpecialConversion(vstrBookPath, vstrSheetName)
'
'    MoveMailsBySpecifiedDetailConditionsWithOutputtingResultLogToSheet vstrSourceMailFolderPath, vstrPersonalOutlookBoxName, objSectionToKeyValueDic, vblnAllowToGetMovingMailLogAsTable, vblnAllowToCollectMovedMailBodyHyperlinksLogAsTable, vblnPreventExecutingToMoveMail
'End Sub
'
''''
''''
''''
'Public Sub MoveMailsBySpecifiedDetailConditionsFromExcelBook(ByVal vstrSourceMailFolderPath As String, _
'        ByVal vstrPersonalOutlookBoxName As String, _
'        ByVal vstrBookPath As String, _
'        Optional ByVal vstrSheetName As String = "DetailConditions", _
'        Optional ByVal vblnAllowToGetMovingMailLogAsTable As Boolean = True, _
'        Optional ByVal vblnAllowToCollectMovedMailBodyHyperlinksLogAsTable As Boolean = False, _
'        Optional ByVal vblnPreventExecutingToMoveMail As Boolean = False)
'
'    Dim objSectionToKeyValueDic As Scripting.Dictionary, objEMailMovedPersonalBoxLogs As EMailMovedPersonalBoxLogs
'
'
'    'Set objSectionToKeyValueDic = GetSectionToKeyValueDicFromThreeColumnsFormattedExcelBook(vstrBookPath, vstrSheetName)
'
'    Set objSectionToKeyValueDic = GetSectionToKeyValueDicFromThreeColumnsFormattedExcelBookByAdoSQLWithSpecialConversion(vstrBookPath, vstrSheetName)
'
'
'    SetupEMailMovedPersonalBoxLogs objEMailMovedPersonalBoxLogs, vblnAllowToGetMovingMailLogAsTable, vblnAllowToCollectMovedMailBodyHyperlinksLogAsTable
'
'    MoveMailsBySpecifiedDetailConditions objEMailMovedPersonalBoxLogs, vstrSourceMailFolderPath, vstrPersonalOutlookBoxName, objSectionToKeyValueDic, vblnPreventExecutingToMoveMail
'
'
'    ' output log to the Immediate-window
'
'    OutputEMailMovedPersonalBoxLogsToImmediateWindow objEMailMovedPersonalBoxLogs
'End Sub
'
'
''''
''''
''''
'Public Sub MoveMailsBySpecifiedDetailConditionsWithOutputtingResultLogToSheet(ByVal vstrSourceMailFolderPath As String, _
'        ByVal vstrPersonalOutlookBoxName As String, _
'        ByRef robjSectionToKeyValueDic As Scripting.Dictionary, _
'        Optional ByVal vblnAllowToGetMovingMailLogAsTable As Boolean = True, _
'        Optional ByVal vblnAllowToCollectMovedMailBodyHyperlinksLogAsTable As Boolean = False, _
'        Optional ByVal vblnPreventExecutingToMoveMail As Boolean = False)
'
'
'    Dim strOutputBookPath As String, objEMailMovedPersonalBoxLogs As EMailMovedPersonalBoxLogs
'
'
'    SetupEMailMovedPersonalBoxLogs objEMailMovedPersonalBoxLogs, vblnAllowToGetMovingMailLogAsTable, vblnAllowToCollectMovedMailBodyHyperlinksLogAsTable
'
'    MoveMailsBySpecifiedDetailConditions objEMailMovedPersonalBoxLogs, vstrSourceMailFolderPath, vstrPersonalOutlookBoxName, robjSectionToKeyValueDic, vblnPreventExecutingToMoveMail
'
'    ' Output logs to a sheet
'
'    strOutputBookPath = GetLoggingBookPathForMovingOutlookEMails(vblnPreventExecutingToMoveMail)
'
'    OutputEMailMovedPersonalBoxLogsToSheet objEMailMovedPersonalBoxLogs, strOutputBookPath, vblnPreventExecutingToMoveMail, "Moving E-mails logs with detail conditions"
'End Sub
'
'
'
'
''**---------------------------------------------
''** Visualize Excel sheet state, which has INI file structure, with Outlook Personal box
''**---------------------------------------------
''''
''''
''''
'Public Function OutputMailTransportDetailINICompatibleExcelSheetConditionsToBook(ByVal vstrOutputBookPath As String, ByVal vstrDetailConditionsINICompatibleBookPath As String, Optional ByVal vstrINICompatibleSheetName As String = "DetailConditions", Optional ByVal vstrPersonalOutlookBoxName As String = "") As Excel.Worksheet
'
'    Dim objSheet As Excel.Worksheet, objDTCol As Collection, objNotExistedMailFolderPathDic As Scripting.Dictionary
'
'    Set objSheet = GetWorkbookAndAnWorksheetPrepareForUnitTest(vstrOutputBookPath, "DetailTransportINI")
'
'    Set objDTCol = GetDetailConditionInformationOfMailTransportingByExcelBook(objNotExistedMailFolderPathDic, vstrDetailConditionsINICompatibleBookPath, vstrINICompatibleSheetName, vstrPersonalOutlookBoxName)
'
'    OutputColToSheetRestrictedByDataTableSheetFormatter objSheet, objDTCol, GetDTSheetFormatterForMailTransportDetailConditionInformation(vstrPersonalOutlookBoxName, True)
'
'    Set OutputMailTransportDetailINICompatibleExcelSheetConditionsToBook = objSheet
'End Function
'
''''
''''
''''
'Public Function GetDetailConditionInformationOfMailTransportingByExcelBook(ByRef robjNotExistedMailFolderPathDic As Scripting.Dictionary, ByVal vstrDetailConditionsExcelBookPath As String, Optional ByVal vstrSheetName As String = "DetailConditions", Optional ByVal vstrPersonalOutlookBoxName As String = "") As Collection
'
'    Dim objSectionToKeyValueDic As Scripting.Dictionary
'
'    With New Scripting.FileSystemObject
'
'        If .FileExists(vstrDetailConditionsExcelBookPath) Then
'
'            'Set objSectionToKeyValueDic = GetSectionToKeyValueDicFromThreeColumnsFormattedExcelBook(vstrDetailConditionsExcelBookPath, vstrSheetName)
'
'            Set objSectionToKeyValueDic = GetSectionToKeyValueDicFromThreeColumnsFormattedExcelBookByAdoSQLWithSpecialConversion(vstrDetailConditionsExcelBookPath, vstrSheetName)
'        End If
'    End With
'
'    Set GetDetailConditionInformationOfMailTransportingByExcelBook = GetDetailConditionInformationOfMailTransporting(robjNotExistedMailFolderPathDic, objSectionToKeyValueDic, vstrPersonalOutlookBoxName)
'End Function
'
'
''**---------------------------------------------
''** output INI file state information to Excel book
''**---------------------------------------------
''''
''''
''''
'Public Function OutputMailTransportDetailINIConditionsToBook(ByVal vstrOutputBookPath As String, ByVal vstrDetailConditionsINIFilePath As String, Optional ByVal vstrPersonalOutlookBoxName As String = "") As Excel.Worksheet
'
'    Dim objSheet As Excel.Worksheet, objDTCol As Collection, objNotExistedMailFolderPathDic As Scripting.Dictionary
'
'    Set objSheet = GetWorkbookAndAnWorksheetPrepareForUnitTest(vstrOutputBookPath, "DetailTransportINI")
'
'    Set objDTCol = GetDetailConditionInformationOfMailTransportingByINIFile(objNotExistedMailFolderPathDic, vstrDetailConditionsINIFilePath, vstrPersonalOutlookBoxName)
'
'    OutputColToSheetRestrictedByDataTableSheetFormatter objSheet, objDTCol, GetDTSheetFormatterForMailTransportDetailConditionInformation(vstrPersonalOutlookBoxName)
'
'    Set OutputMailTransportDetailINIConditionsToBook = objSheet
'End Function
'
''''
''''
''''
'Public Function GetDTSheetFormatterForMailTransportDetailConditionInformation(Optional ByVal vstrPersonalOutlookBoxName As String = "", Optional ByVal vblnAllowToUseINIComatibleExcelSheet As Boolean = False) As DataTableSheetFormatter
'
'    Dim objDTSheetFormatter As DataTableSheetFormatter
'
'    Dim objFormatConditionExpander As FormatConditionExpander
'
'    Set objDTSheetFormatter = New DataTableSheetFormatter
'
'    With objDTSheetFormatter
'
'        If vblnAllowToUseINIComatibleExcelSheet Then
'
'            .FieldTitleInteriorType = ColumnNameInteriorOfExtractedTableFromSheet
'        Else
'            .FieldTitleInteriorType = ColumnNameInteriorOfLoadedINIFileAsTable
'        End If
'
'        .RecordBordersType = RecordCellsGrayAll
'
'        If vstrPersonalOutlookBoxName <> "" Then
'
'
'            Set .FieldTitleToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("ClassifyingRuleName,41,DestinationPathPart,40,CountOfConditions,12,IsFolderExisted,9,CountOfMails,9")
'
'
'            Set .ColumnsFormatCondition = New ColumnsFormatConditionParam
'
'            With .ColumnsFormatCondition
'
'                Set objFormatConditionExpander = New FormatConditionExpander
'
'                With objFormatConditionExpander
'
'                    .AddTextContainCondition "False", FontDeepRedBgLightRed
'                End With
'
'                .FieldTitleToCellFormatCondition.Add "IsFolderExisted", objFormatConditionExpander
'            End With
'        Else
'            Set .FieldTitleToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("ClassifyingRuleName,41,DestinationPathPart,40,CountOfConditions,12")
'        End If
'
'        .AllowToMergeCellsByContinuousSameValues = True
'        .AllowToSetVerticalTopAlignmentTopOfMergeCells = True
'
'        Set .FieldTitlesToMergeCells = GetColFromLineDelimitedChar("DestinationPathPart,CountOfConditions,IsFolderExisted,CountOfMails")
'
'    End With
'
'    Set GetDTSheetFormatterForMailTransportDetailConditionInformation = objDTSheetFormatter
'End Function
'
'
''**---------------------------------------------
''** Prepare a detail condition INI file from Excel book
''**---------------------------------------------
''''
''''
''''
'Public Function GetIniPathAfterOutputMailTransportDetailConditionsINIFileFromSettingExcelBook(ByVal vstrInputSectionToKeyValueBookPath As String, Optional ByVal vstrInputSheetName As String = "DetailConditions") As String
'
'    Dim strIniPath As String, objSectionToKeyValueDic As Scripting.Dictionary
'
'    With New Scripting.FileSystemObject
'
'        strIniPath = .GetParentFolderName(vstrInputSectionToKeyValueBookPath) & "\" & .GetBaseName(vstrInputSectionToKeyValueBookPath) & ".ini"
'
'        If .FileExists(strIniPath) Then
'
'            .DeleteFile strIniPath, True
'        End If
'    End With
'
'
'    Set objSectionToKeyValueDic = GetSectionToKeyValueDicFromThreeColumnsFormattedExcelBookByAdoSQLWithSpecialConversion(vstrInputSectionToKeyValueBookPath, vstrInputSheetName)
'
'    WriteIniFromSectionToKeyValueDic strIniPath, objSectionToKeyValueDic
'
'    GetIniPathAfterOutputMailTransportDetailConditionsINIFileFromSettingExcelBook = strIniPath
'End Function
'
'''--VBA_Code_File--<EMailMovedPersonalBoxLogs.cls>--
'VERSION 1.0 CLASS
'BEGIN
'  MultiUse = -1  'True
'END
'Attribute VB_Name = "EMailMovedPersonalBoxLogs"
'Attribute VB_GlobalNameSpace = False
'Attribute VB_Creatable = False
'Attribute VB_PredeclaredId = False
'Attribute VB_Exposed = False
''
''   A data class of the outlook e-mail moved logs
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on Outlook
''       Independent from Excel
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Wed, 10/May/2023    Kalmclaeyd Tarclanus    Initial creation
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Declarations
''///////////////////////////////////////////////
'
'Private mobjMainMovingProcessLogDic As Scripting.Dictionary ' get always on each E-mails moving
'
'
'Private mblnAllowToGetMovingBasicMailLogAsTable As Boolean
'
'Private mobjMovingBasicMailLogDTCol As Collection
'
'
'Private mblnAllowToCollectMovedMailBodyHyperlinksLogAsTable As Boolean
'
'Private mobjExtractedMovedMailBodySomeInfoLogDTCol As Collection
'
'
'
''///////////////////////////////////////////////
''/// Event handlers
''///////////////////////////////////////////////
''''
''''
''''
'Private Sub Class_Initialize()
'
'    mblnAllowToGetMovingBasicMailLogAsTable = True
'
'    mblnAllowToCollectMovedMailBodyHyperlinksLogAsTable = False
'End Sub
'
'
''///////////////////////////////////////////////
''/// Properties
''///////////////////////////////////////////////
''''
''''
''''
'Public Property Get MainMovingProcessLogDic() As Scripting.Dictionary
'
'    Set MainMovingProcessLogDic = mobjMainMovingProcessLogDic
'End Property
'Public Property Set MainMovingProcessLogDic(ByVal vobjMainMovingProcessLogDic As Scripting.Dictionary)
'
'    Set mobjMainMovingProcessLogDic = vobjMainMovingProcessLogDic
'End Property
'
'
''''
''''
''''
'Public Property Get AllowToGetMovingBasicMailLogAsTable() As Boolean
'
'    AllowToGetMovingBasicMailLogAsTable = mblnAllowToGetMovingBasicMailLogAsTable
'End Property
'Public Property Let AllowToGetMovingBasicMailLogAsTable(ByVal vblnAllowToGetMovingBasicMailLogAsTable As Boolean)
'
'    mblnAllowToGetMovingBasicMailLogAsTable = vblnAllowToGetMovingBasicMailLogAsTable
'End Property
'
''''
''''
''''
'Public Property Get MovingBasicMailLogDTCol() As Collection
'
'    Set MovingBasicMailLogDTCol = mobjMovingBasicMailLogDTCol
'End Property
'Public Property Set MovingBasicMailLogDTCol(ByVal vobjMovingBasicMailLogDTCol As Collection)
'
'    Set mobjMovingBasicMailLogDTCol = vobjMovingBasicMailLogDTCol
'End Property
'
'
''''
''''
''''
'Public Property Get AllowToCollectMovedMailBodyHyperlinksLogAsTable() As Boolean
'
'    AllowToCollectMovedMailBodyHyperlinksLogAsTable = mblnAllowToCollectMovedMailBodyHyperlinksLogAsTable
'End Property
'Public Property Let AllowToCollectMovedMailBodyHyperlinksLogAsTable(ByVal vblnAllowToCollectMovedMailBodyHyperlinksLogAsTable As Boolean)
'
'    mblnAllowToCollectMovedMailBodyHyperlinksLogAsTable = vblnAllowToCollectMovedMailBodyHyperlinksLogAsTable
'End Property
'
'
''''
''''
''''
'Public Property Get ExtractedMovedMailBodySomeInfoLogDTCol() As Collection
'
'    Set ExtractedMovedMailBodySomeInfoLogDTCol = mobjExtractedMovedMailBodySomeInfoLogDTCol
'End Property
'Public Property Set ExtractedMovedMailBodySomeInfoLogDTCol(ByVal vobjExtractedMovedMailBodySomeInfoLogDTCol As Collection)
'
'    Set mobjExtractedMovedMailBodySomeInfoLogDTCol = vobjExtractedMovedMailBodySomeInfoLogDTCol
'End Property
'''--VBA_Code_File--<EMailClassifyingCondition.cls>--
'VERSION 1.0 CLASS
'BEGIN
'  MultiUse = -1  'True
'END
'Attribute VB_Name = "EMailClassifyingCondition"
'Attribute VB_GlobalNameSpace = False
'Attribute VB_Creatable = False
'Attribute VB_PredeclaredId = False
'Attribute VB_Exposed = False
''
''   A data class of the outlook e-mail classification, when no use of RegExp patterns
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on Outlook
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Sat,  6/May/2023    Kalmclaeyd Tarclanus    Initial creation
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Declarations
''///////////////////////////////////////////////
'
'Private menmMailItemPropertyType As OutlookMailItemPropertyType
'
'Private menmMatchingTextBoolCondition As MatchingTextBoolCondition
'
'Private mblnNeedToUseLowerCase As Boolean
'
'Private mblnNeedToExactMatch As Boolean
'
''///////////////////////////////////////////////
''/// Event handlers
''///////////////////////////////////////////////
''''
''''
''''
'Private Sub Class_Initialize()
'
'    menmMailItemPropertyType = olmSubject
'
'    menmMatchingTextBoolCondition = MatchingTextBoolOrOp
'
'    mblnNeedToUseLowerCase = False
'
'    mblnNeedToExactMatch = False
'End Sub
'
''///////////////////////////////////////////////
''/// Properties
''///////////////////////////////////////////////
'
'
'Public Property Get SearchingMailItemPropertyType() As OutlookMailItemPropertyType
'
'    SearchingMailItemPropertyType = menmMailItemPropertyType
'End Property
'Public Property Let SearchingMailItemPropertyType(ByVal venmMailItemPropertyType As OutlookMailItemPropertyType)
'
'    menmMailItemPropertyType = venmMailItemPropertyType
'End Property
'
'
'Public Property Get SearchingMatchingTextBoolCondition() As MatchingTextBoolCondition
'
'    SearchingMatchingTextBoolCondition = menmMatchingTextBoolCondition
'End Property
'Public Property Let SearchingMatchingTextBoolCondition(ByVal venmMatchingTextBoolCondition As MatchingTextBoolCondition)
'
'    menmMatchingTextBoolCondition = venmMatchingTextBoolCondition
'End Property
'
'
'Public Property Get NeedToUseLowerCase() As Boolean
'
'    NeedToUseLowerCase = mblnNeedToUseLowerCase
'End Property
'Public Property Let NeedToUseLowerCase(ByVal vblnNeedToUseLowerCase As Boolean)
'
'    mblnNeedToUseLowerCase = vblnNeedToUseLowerCase
'End Property
'
'
'Public Property Get NeedToExactMatch() As Boolean
'
'    NeedToExactMatch = mblnNeedToExactMatch
'End Property
'Public Property Let NeedToExactMatch(ByVal vblnNeedToExactMatch As Boolean)
'
'    mblnNeedToExactMatch = vblnNeedToExactMatch
'End Property
'
'''--VBA_Code_File--<OutlookHyperLink.bas>--
'Attribute VB_Name = "OutlookHyperLink"
''
''   get some informations, such as hyper-links, e-mail addresses, and UNC paths of the body text of a Outlook.MailItem
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on Outlook
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   Modification History:
''       Mon,  8/May/2023    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Directives
''///////////////////////////////////////////////
'
'#Const HAS_WORD_REF = False
'
''///////////////////////////////////////////////
''/// Sanity tests
''///////////////////////////////////////////////
''''
''''
''''
'Private Sub msubSanityTestToGetHyperLinksOfSelectedMail()
'
'    Dim strMailFolderPath As String, objFraudMailStoredFolder As Outlook.Folder
'    Dim objMailItem As Outlook.MailItem, objSelectedItem As Object
'    Dim objDTCol As Collection
'
'    Dim objAddresses As Collection
'
'
'    Set objDTCol = New Collection
'
'    ClearRegExpsOfHyperlinks
'
'    With New Outlook.Application
'
'        Set objMailItem = GetSelectedOneMailItems(.ActiveExplorer)
'
'        Debug.Print objMailItem.Subject
'
'        DebugCol GetHyperLinksColFromMailItem(objMailItem)
'
'        Set objAddresses = GetAddressesColFromMailItem(objMailItem)
'
'        If objAddresses.Count > 0 Then
'
'            Debug.Print "E-mail addresses of body-text:"
'
'            DebugCol objAddresses
'        End If
'    End With
'
'End Sub
'
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''**---------------------------------------------
''** get data-table collection
''**---------------------------------------------
''''
''''
''''
'Public Sub GetMailBodyHyperLinksAndSomeInfoDTColFromMail(ByRef robjDTCol As Collection, _
'        ByRef robjMailItem As Outlook.MailItem, _
'        Optional ByVal vblnUseWordDocumentConversion As Boolean = False)
'
'
'    Dim objInspector As Outlook.Inspector, objDocument As Word.Document, objHyperlink As Word.Hyperlink
'
'    Dim objHyperLinks As Collection, varHyperLink As Variant
'
'    Dim strHyperLink As String, strAddress As String, objRowCol As Collection, objBaseMailInfoRowCol As Collection
'
'
'    If robjDTCol Is Nothing Then Set robjDTCol = New Collection
'
'    With robjMailItem
'
'        Set objHyperLinks = GetHyperLinksColFromMailItem(robjMailItem, vblnUseWordDocumentConversion)
'
'        If objHyperLinks.Count > 0 Then
'
'            Set objBaseMailInfoRowCol = New Collection
'
'            AddMailItemSomeTypesLogToRowCol objBaseMailInfoRowCol, robjMailItem
'
'            For Each varHyperLink In objHyperLinks
'
'                Set objRowCol = New Collection
'
'                objRowCol.Add varHyperLink
'
'                UnionDoubleCollectionsToSingle objRowCol, objBaseMailInfoRowCol
'
'                robjDTCol.Add objRowCol
'            Next
'        End If
'    End With
'End Sub
'
''''
''''
''''
'Public Sub GetOneRowMultiLinesHyperLinksAndEmailAddressesAndUNCPathsAndSomeInfoDTColFromMail(ByRef robjDTCol As Collection, _
'        ByRef robjMailItem As Outlook.MailItem)
'
'    Dim objHyperLinks As Collection, objEMailAddresses As Collection, objUNCPaths As Collection
'
'    Dim strMultiLineHyperLinks As String, strMultiLineEMailAddresses As String, strMultiLineUNCPaths As String
'
'    Dim objRowCol As Collection
'
'    If robjDTCol Is Nothing Then Set robjDTCol = New Collection
'
'
'    With robjMailItem
'
'        Set objHyperLinks = GetHyperLinksColFromMailItem(robjMailItem)
'
'        strMultiLineHyperLinks = GetMultiLineTextForGridCellFromCol(objHyperLinks, "<", ">")
'
'        Set objEMailAddresses = GetAddressesColFromMailItem(robjMailItem)
'
'        strMultiLineEMailAddresses = GetMultiLineTextForGridCellFromCol(objEMailAddresses, "", "", ";" & vbLf)
'
'        Set objUNCPaths = GetUNCPathsColFromMailItem(robjMailItem)
'
'        strMultiLineUNCPaths = GetMultiLineTextForGridCellFromCol(objUNCPaths, "<", ">")
'    End With
'
'    Set objRowCol = New Collection
'
'    With objRowCol
'
'        .Add strMultiLineHyperLinks
'
'        .Add strMultiLineEMailAddresses
'
'        .Add strMultiLineUNCPaths
'    End With
'
'    AddMailItemSomeTypesLogToRowCol objRowCol, robjMailItem
'
'    robjDTCol.Add objRowCol
'End Sub
'
''**---------------------------------------------
''** get hyperlinks from a Mail-Item
''**---------------------------------------------
''''
''''
''''
'Public Function GetHyperLinksColFromMailItem(ByRef robjMailItem As Outlook.MailItem, _
'        Optional ByVal vblnUseWordDocumentConversion As Boolean = False) As Collection
'
'    Dim objHyperLinks As Collection
'
'    If robjMailItem.BodyFormat = olFormatPlain Then
'
'        Set objHyperLinks = GetHyperLinksColFromText(robjMailItem.Body)
'    Else
'        If vblnUseWordDocumentConversion Then
'
'            Set objHyperLinks = GetHyperLinksColFromMailItemByUsingWordDocument(robjMailItem)
'        Else
'            Set objHyperLinks = GetHyperLinksColFromText(robjMailItem.HTMLBody)
'        End If
'    End If
'
'    Set GetHyperLinksColFromMailItem = objHyperLinks
'End Function
'
'
''''
''''
''''
'Public Function GetHyperLinksColFromMailItemByUsingWordDocument(ByRef robjMailItem As Outlook.MailItem) As Collection
'
'    Dim objHyperLinkDic As Scripting.Dictionary, objHyperLinks As Collection
'
'    Dim objInspector As Outlook.Inspector, strHyperLink As String
'
'
'#If HAS_WORD_REF Then
'
'    Dim objDocument As Word.Document, objHyperlink As Word.Hyperlink
'#Else
'    Dim objDocument As Object, objHyperlink As Object
'#End If
'
'    Set objInspector = robjMailItem.GetInspector
'
'    Set objDocument = objInspector.WordEditor
'
'    Set objHyperLinkDic = New Scripting.Dictionary
'
'    For Each objHyperlink In objDocument.Hyperlinks
'
'        'Debug.Print objHyperlink.Address
'
'        strHyperLink = objHyperlink.Address
'
'        With objHyperLinkDic
'
'            If Not .Exists(strHyperLink) Then
'
'                .Add strHyperLink, 0
'            End If
'        End With
'    Next
'
'    Set GetHyperLinksColFromMailItemByUsingWordDocument = GetEnumeratorKeysColFromDic(objHyperLinkDic)
'End Function
'
'
'
''**---------------------------------------------
''** get some information from a body text of a Mail-Item
''**---------------------------------------------
''''
'''' get E-mail addresses from a body text of a Mail-Item
''''
'Public Function GetAddressesColFromMailItem(ByRef robjMailItem As Outlook.MailItem) As Collection
'
'    Dim objAddresses As Collection
'
'    If robjMailItem.BodyFormat = olFormatPlain Then
'
'        Set objAddresses = GetEmailAddressesColFromText(robjMailItem.Body)
'    Else
'        Set objAddresses = GetEmailAddressesColFromText(robjMailItem.HTMLBody)
'    End If
'
'    Set GetAddressesColFromMailItem = objAddresses
'End Function
'
''''
'''' get UNC pathes from a body text of a Mail-Item
''''
'Public Function GetUNCPathsColFromMailItem(ByRef robjMailItem As Outlook.MailItem) As Collection
'
'    Dim objUNCPaths As Collection
'
'    If robjMailItem.BodyFormat = olFormatPlain Then
'
'        Set objUNCPaths = GetUNCPathsColFromText(robjMailItem.Body)
'    Else
'        Set objUNCPaths = GetUNCPathsColFromText(robjMailItem.HTMLBody)
'    End If
'
'    Set GetUNCPathsColFromMailItem = objUNCPaths
'End Function
'''--VBA_Code_File--<OutlookHyperLinkForXl.bas>--
'Attribute VB_Name = "OutlookHyperLinkForXl"
''
''   Output results of get some informations from the body text of a Outlook.MailItem to Excel sheet
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on both Outlook and Excel
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Tue,  9/May/2023    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Sanity tests
''///////////////////////////////////////////////
''''
'''' About hyperlinks on an Outlook.MailItem object
''''
'Public Sub msubSanityTestToOutputMailBodyHyperLinksOfASelectedMailToSheet()
'
'    Dim objMailItem As Outlook.MailItem, objDTCol As Collection
'
'    With New Outlook.Application
'
'        Set objMailItem = GetSelectedOneMailItems(.ActiveExplorer)
'
'        GetMailBodyHyperLinksAndSomeInfoDTColFromMail objDTCol, objMailItem
'    End With
'
'    OutputMailBodyHyperLinksAndSomeInfoDTColToSheet objDTCol, mfstrGetTemporaryMailBodyHyperLinksAndSomeInfoBookPath()
'End Sub
'
''''
'''' About hyperlinks on plural Outlook.MailItem objects of a folder
''''
'Public Sub msubSanityTestToOutputMailBodyHyperLinksOfASelectedFolderToSheet()
'
'    Dim objMailItem As Outlook.MailItem, objDTCol As Collection
'
'    With New Outlook.Application
'
'        For Each objMailItem In GetSelectedMailFolder(.ActiveExplorer).Items
'
'            GetMailBodyHyperLinksAndSomeInfoDTColFromMail objDTCol, objMailItem
'        Next
'    End With
'
'    OutputMailBodyHyperLinksAndSomeInfoDTColToSheet objDTCol, mfstrGetTemporaryMailBodyHyperLinksAndSomeInfoBookPath()
'End Sub
'
''''
'''' About hyperlinks, e-mail addresses, and UNC pathes on the mail-body of an Outlook.MailItem object
''''
'Public Sub msubSanityTestToOutputExtractedMailBodySomeInfosOfASelectedMailToSheet()
'
'    Dim objMailItem As Outlook.MailItem, objDTCol As Collection
'
'    With New Outlook.Application
'
'        Set objMailItem = GetSelectedOneMailItems(.ActiveExplorer)
'
'        GetOneRowMultiLinesHyperLinksAndEmailAddressesAndUNCPathsAndSomeInfoDTColFromMail objDTCol, objMailItem
'    End With
'
'    OutputExtractedMailBodyInfosAndSomeInfoDTColToSheetWithPreparingBook objDTCol, mfstrGetTemporaryExtractedMailBodySomeInfosAndSomeInfoBookPath()
'End Sub
'
'
''''
'''' About hyperlinks, e-mail addresses, and UNC pathes on the mail-body of an Outlook.MailItem object
''''
'Public Sub msubSanityTestToOutputExtractedMailBodySomeInfosOfASelectedFolderToSheet()
'
'    Dim objMailItem As Outlook.MailItem, objDTCol As Collection
'
'    With New Outlook.Application
'
'        For Each objMailItem In GetSelectedMailFolder(.ActiveExplorer).Items
'
'            GetOneRowMultiLinesHyperLinksAndEmailAddressesAndUNCPathsAndSomeInfoDTColFromMail objDTCol, objMailItem
'        Next
'    End With
'
'    OutputExtractedMailBodyInfosAndSomeInfoDTColToSheetWithPreparingBook objDTCol, mfstrGetTemporaryExtractedMailBodySomeInfosAndSomeInfoBookPath()
'End Sub
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''''
''''
''''
'Public Sub OutputMailBodyHyperLinksAndSomeInfoDTColToSheet(ByRef robjDTCol As Collection, ByVal vstrOutputBookPath As String)
'
'    Dim objSheet As Excel.Worksheet
'
'    Set objSheet = GetWorkbookAndAnWorksheetPrepareForUnitTest(vstrOutputBookPath, "MailBodyHyperlinks")
'
'    OutputColToSheetRestrictedByDataTableSheetFormatter objSheet, robjDTCol, mfobjGetDTFormatterForMailBodyHyperLinksAndSomeInfo()
'End Sub
'
''''
''''
''''
'Public Sub OutputExtractedMailBodyInfosAndSomeInfoDTColToSheetWithPreparingBook(ByRef robjDTCol As Collection, ByVal vstrOutputBookPath As String)
'
'    Dim objSheet As Excel.Worksheet
'
'    Set objSheet = GetWorkbookAndAnWorksheetPrepareForUnitTest(vstrOutputBookPath, "ExtractedMailBodyInfos")
'
'    OutputExtractedMailBodyInfosAndSomeInfoDTColToSheet objSheet, robjDTCol
'End Sub
'
''''
''''
''''
'Public Sub OutputExtractedMailBodyInfosAndSomeInfoDTColToSheet(ByRef robjSheet As Excel.Worksheet, ByRef robjDTCol As Collection)
'
'    OutputColToSheetRestrictedByDataTableSheetFormatter robjSheet, robjDTCol, mfobjGetDTFormatterForExtractedMailBodyInfosAndSomeInfo()
'
'    AdjustRowHeightForIndividualCellLineFeedsCountOfSpecifiedColumns robjSheet, GetColFromLineDelimitedChar("MailBodyHyperlink")
'End Sub
'
''''
''''
''''
'Private Function mfstrGetTemporaryMailBodyHyperLinksAndSomeInfoBookPath()
'
'    Dim strDir As String, strPath As String
'
'    strDir = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks"
'
'    ForceToCreateDirectory strDir
'
'    strPath = strDir & "\TempMailBodyHyperLinksAndSomeInfo.xlsx"
'
'    mfstrGetTemporaryMailBodyHyperLinksAndSomeInfoBookPath = strPath
'End Function
'
''''
''''
''''
'Private Function mfstrGetTemporaryExtractedMailBodySomeInfosAndSomeInfoBookPath()
'
'    Dim strDir As String, strPath As String
'
'    strDir = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks"
'
'    ForceToCreateDirectory strDir
'
'    strPath = strDir & "\TempMailBodyExtractedInfosAndSomeInfo.xlsx"
'
'    mfstrGetTemporaryExtractedMailBodySomeInfosAndSomeInfoBookPath = strPath
'End Function
'
''''
'''' about hyperlinks
''''
'Private Function mfobjGetDTFormatterForMailBodyHyperLinksAndSomeInfo() As DataTableSheetFormatter
'
'    Dim objDTFormatter As DataTableSheetFormatter
'
'    Set objDTFormatter = New DataTableSheetFormatter
'
'    With objDTFormatter
'
'        .FieldTitleInteriorType = ColumnNameInteriorOfGatheredColTable
'
'        Set .FieldTitleToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("MailBodyHyperlink,55," & GetFieldTitleToColumnWidthStringForMailTypicalProperties())
'
'        .RecordBordersType = RecordCellsGrayAll
'
'
'        .AllowToMergeCellsByContinuousSameValues = True
'
'        .AllowToSetVerticalTopAlignmentTopOfMergeCells = True
'
'        Set .FieldTitlesToMergeCells = GetEnumeratorKeysColFromDic(GetTextToRealNumberDicFromLineDelimitedChar(GetFieldTitleToColumnWidthStringForMailTypicalProperties()))
'
'
'        Set .ColumnsNumberFormatLocal = New ColumnsNumberFormatLocalParam
'
'        With .ColumnsNumberFormatLocal
'
'            .SetNumberFormatLocalAndApplyFieldTitles "ddd, yyyy/m/d hh:mm:ss", GetColFromLineDelimitedChar("ReceivedTime")
'        End With
'
'    End With
'
'    Set mfobjGetDTFormatterForMailBodyHyperLinksAndSomeInfo = objDTFormatter
'End Function
'
''''
'''' about one-cell multi-lines of hyperlinks, e-mail addresses, and UNC pathes
''''
'Private Function mfobjGetDTFormatterForExtractedMailBodyInfosAndSomeInfo() As DataTableSheetFormatter
'
'    Dim objDTFormatter As DataTableSheetFormatter
'
'    Set objDTFormatter = New DataTableSheetFormatter
'
'    With objDTFormatter
'
'        .FieldTitleInteriorType = ColumnNameInteriorOfGatheredColTable
'
'        Set .FieldTitleToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("MailBodyHyperlink,55,MailBodyEmailAddresses,35,MailBodyUNCPaths,40," & GetFieldTitleToColumnWidthStringForMailTypicalProperties())
'
'        .RecordBordersType = RecordCellsGrayAll
'
'
'        .AllowToMergeCellsByContinuousSameValues = True
'
'        .AllowToSetVerticalTopAlignmentTopOfMergeCells = True
'
'        Set .FieldTitlesToMergeCells = GetEnumeratorKeysColFromDic(GetTextToRealNumberDicFromLineDelimitedChar(GetFieldTitleToColumnWidthStringForMailTypicalProperties()))
'
'
'        Set .ColumnsNumberFormatLocal = New ColumnsNumberFormatLocalParam
'
'        With .ColumnsNumberFormatLocal
'
'            .SetNumberFormatLocalAndApplyFieldTitles "ddd, yyyy/m/d hh:mm:ss", GetColFromLineDelimitedChar("ReceivedTime")
'        End With
'
'        '.AllowToAutoFitRowsHeight = True
'    End With
'
'    Set mfobjGetDTFormatterForExtractedMailBodyInfosAndSomeInfo = objDTFormatter
'End Function
'
'''--VBA_Code_File--<UTfOperateOutlook.bas>--
'Attribute VB_Name = "UTfOperateOutlook"
''
''   Sanity test to operate Microsoft Outlook
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Fri, 14/Apr/2023    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
'
''///////////////////////////////////////////////
''/// Directives
''///////////////////////////////////////////////
'
'#Const HAS_REF = True
'
'
''///////////////////////////////////////////////
''/// Sanity tests
''///////////////////////////////////////////////
''''
''''
''''
'Public Sub msubSanityTestToConfirmCurrentOutlookDefaultAccountUser()
'
'#If HAS_REF Then
'
'    Dim objNamespace As Outlook.Namespace, objFolder As Outlook.Folder, objOutlook As Outlook.Application
'#Else
'    Dim objNamespace As Object, objFolder As Object objOutlook As Object
'#End If
'
'    Set objOutlook = Nothing
'
'    On Error Resume Next
'
'    Set objOutlook = GetObject(, "Outlook.Application")
'
'    On Error GoTo 0
'
'    If objOutlook Is Nothing Then
'
'        Set objOutlook = CreateObject("Outlook.Application")
'    End If
'
'    With objOutlook
'
'        Set objNamespace = .GetNamespace("MAPI")
'
'        Debug.Print objNamespace.CurrentUser.Address
'
'        Debug.Print objNamespace.CurrentUser.AddressEntry
'    End With
'End Sub
'
''''
''''
''''
'Private Sub msubSanityTestToLogOnOutlook01()
'
'#If HAS_REF Then
'
'    With New Outlook.Application
'#Else
'    With CreateObject("Outlook.Application")
'#End If
'        With .Session
'
'            .Logon "Outlook", ""
'
'            .SendAndReceive True    ' All e-mails send and receive
'
'        End With
'
'        .Quit
'    End With
'End Sub
'
'
''''
''''
''''
'Private Sub msubSanityTestToDisplayInBox()
'
'    ' Display the Outlook Inbox
'
'    msubDisplayOutlookDefaultMailFolder olFolderInbox
'End Sub
'
''''
''''
'''''
'Private Sub msubSanityTestToDisplayJunk()
'
'    ' Display the Outlook Junk - 迷惑フォルダ
'
'    msubDisplayOutlookDefaultMailFolder Outlook.OlDefaultFolders.olFolderJunk
'End Sub
'
''''
''''
'''''
'Private Sub msubSanityTestToDisplaySent()
'
'    ' Display the Outlook sent mails
'
'    msubDisplayOutlookDefaultMailFolder Outlook.OlDefaultFolders.olFolderSentMail
'End Sub
'
''''
''''
''''
'Private Sub msubDisplayOutlookDefaultMailFolder(Optional ByVal venmDefaultFolders As Outlook.OlDefaultFolders = Outlook.OlDefaultFolders.olFolderInbox)
'
'#If HAS_REF Then
'
'    Dim objNamespace As Outlook.Namespace, objFolder As Outlook.Folder
'#Else
'    Dim objNamespace As Object, objFolder As Object
'#End If
'
'    With CreateObject("Outlook.Application") ' New Outlook.Application
'
'        Set objNamespace = .GetNamespace("MAPI")
'
'        Set objFolder = objNamespace.GetDefaultFolder(venmDefaultFolders)   ' Inbox
'
'        objFolder.Display ' display the Outlook Inbox
'    End With
'End Sub
'
'
''''
''''
''''
'Private Sub msubSanityTestToGetFolderNames()
'
'#If HAS_REF Then
'
'    Dim objNamespace As Outlook.Namespace, objFolder As Outlook.Folder
'#Else
'    Dim objNamespace As Object, objFolder As Object
'#End If
'
'    With CreateObject("Outlook.Application") ' New Outlook.Application
'
'        Set objNamespace = .GetNamespace("MAPI")
'
'        For Each objFolder In objNamespace.Folders
'
'            Debug.Print objFolder.Name
'        Next
'    End With
'
'End Sub
'
'
''''
''''
''''
'Private Sub msubSanityTestToGetFolderNamesWithSubFolders()
'
'#If HAS_REF Then
'
'    Dim objFolder As Outlook.Folder
'#Else
'    Dim objFolder As Object
'#End If
'
'    Dim intHierarchyOrder As Long
'
'    intHierarchyOrder = 1
'
'    With CreateObject("Outlook.Application") ' New Outlook.Application
'
'        'For Each objFolder In .Session.Folders
'
'        'For Each objFolder In .Session.Folders("OutlookPersonalMails20220713").Folders
'
'        For Each objFolder In .GetNamespace("MAPI").Folders
'
'            Debug.Print CStr(intHierarchyOrder) & ", " & objFolder.Name
'
'            msubLoadMailSubFolder objFolder, intHierarchyOrder + 1
'        Next
'
'    End With
'
'    ' .Session.Folders("OutlookPersonalMails20220713")
'
'End Sub
'
''''
''''
''''
'Private Sub msubLoadMailSubFolder(ByVal vobjFolder As Outlook.Folder, ByVal vintHierarchyOrder As Long)
'
'    Dim objChildFolder As Outlook.Folder
'
'    For Each objChildFolder In vobjFolder.Folders
'
'        Debug.Print CStr(vintHierarchyOrder) & ", " & objChildFolder.Name
'
'        msubLoadMailSubFolder objChildFolder, vintHierarchyOrder + 1
'    Next
'End Sub
'
'
''''
''''
''''
'Private Sub msubSanityTestToListUpOutlookMails()
'
'    Dim objFolder As Outlook.Folder
'    Dim objMailItem As Outlook.MailItem
'
'    Dim strLog As String
'
'    Dim j As Long
'
'    ' Set objFolder = objOutlookApplication.Session.Folders("Outlook データ ファイル").Folders("受信保存").Folders("×××")
'
'    With New Outlook.Application
'
'        Set objFolder = .Session.Folders("prowmiheneth@brown.plala.or.jp").Folders("受信トレイ")
'
'        For Each objMailItem In objFolder.Items
'
'            strLog = ""
'
'            With objMailItem
'
'                'strLog = .Subject & ", " & .SenderName & ", " & Format(.ReceivedTime, "yyyy/m/d - aaa - hh:mm:ss") & ", " & .Body
'
'                strLog = .Subject & ", " & .SenderName & ", " & .SenderEmailAddress & ", " & Format(.ReceivedTime, "yyyy/m/d - aaa - hh:mm:ss")
'            End With
'
'            Debug.Print strLog
'
'    '        For j = 1 To objMailItem.Attachments.Count
'    '
'    '            Cells(i, j + 4) = objMailItem.Attachments.Item(j).fileName
'    '
'    '            ' If you need to save the attachment files
'    '
'    '            'objMailItem.Attachments.Item(j).SaveAsFile Path:="フルパス"
'    '        Next
'
'        Next
'
'    End With
'
'End Sub
'
'
''''
''''
''''
'Private Sub msubSanityTestToMoveOutlookMails()
'
'    Dim objSrcFolder As Outlook.Folder, objDstFolder As Outlook.Folder
'    Dim objMailItem As Outlook.MailItem
'
'    Dim strLog As String
'
'    Dim blnDoMove As Boolean
'
'    With New Outlook.Application
'
'        Set objSrcFolder = .Session.Folders("prowmiheneth@brown.plala.or.jp").Folders("受信トレイ")
'
'        Set objDstFolder = .Session.Folders("OutlookPersonalMails20220713").Folders("事業").Folders("転職活動").Folders("リクナビNEXT")
'
'        For Each objMailItem In objSrcFolder.Items
'
'            blnDoMove = False
'
'            With objMailItem
'
'                'strLog = .Subject & ", " & .SenderName & ", " & Format(.ReceivedTime, "yyyy/m/d - aaa - hh:mm:ss") & ", " & .Body
'
'                strLog = .Subject & ", " & .SenderName & ", " & .SenderEmailAddress & ", " & Format(.ReceivedTime, "yyyy/m/d - aaa - hh:mm:ss")
'
'                Debug.Print strLog
'
'                If InStr(1, .SenderName, "リクナビＮＥＸＴ") > 0 And InStr(1, .SenderEmailAddress, "next.rikunabi.com") > 0 Then
'
'                    blnDoMove = True
'                End If
'
'
'            End With
'
'            If blnDoMove Then
'
'                objMailItem.Move objDstFolder
'
'            End If
'
'        Next
'
'    End With
'
'End Sub
'
'
''''
'''' About Outlook.Store object
''''
'Private Sub msubSanityTestToListUpOutlookStore()
'
'    Dim objStore As Outlook.Store
'
'    With New Outlook.Application
'
'        For Each objStore In .Session.Stores
'
'            With objStore
'
'                'Debug.Print .StoreID & ", " & .IsDataFileStore & ", " & .GetRootFolder.FolderPath
'
'                'Debug.Print .Session.CurrentUser
'
'                'Debug.Print .DisplayName
'
'                'Debug.Print .ExchangeStoreType
'
'                'Debug.Print .FilePath
'
'                Debug.Print .DisplayName & ", " & .IsDataFileStore & ", " & .IsCachedExchange & ", " & .FilePath & ", " & .GetRootFolder.FolderPath
'
'
'            End With
'        Next
'
'    End With
'
'End Sub
'
'''--VBA_Code_File--<UTfOperateOutlookForXl.bas>--
'Attribute VB_Name = "UTfOperateOutlookForXl"
''
''   Output operated Microsoft Outlook results into Excel sheet
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Sat, 15/Apr/2023    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
'
''///////////////////////////////////////////////
''/// Sanity tests
''///////////////////////////////////////////////
''''
''''
''''
'Private Sub msubSanityTestToOutputOutlookMailItemsInbox()
'
'    Dim objNamespace As Outlook.Namespace
'    Dim objFolder As Outlook.Folder
'
'    With New Outlook.Application
'
'        Set objNamespace = .GetNamespace("MAPI")
'
'        Set objFolder = objNamespace.GetDefaultFolder(Outlook.OlDefaultFolders.olFolderInbox)    ' Inbox
'
'        msubOutputOutlookMailItemsInfoToTemporaryBook objFolder
'    End With
'
'End Sub
''''
''''
''''
'Private Sub msubSanityTestToOutputOutlookMailFolderPaths()
'
'    msubOutputOutlookMailFolderInfoToBook False
'End Sub
'
''''
''''
''''
'Private Sub msubSanityTestToOutputOutlookMailFolderPathsWithDetails()
'
'    msubOutputOutlookMailFolderInfoToBook True
'End Sub
'
''''
''''
''''
'Private Sub msubSanityTestToOutputOutlookMailFolderPathsWithDetailsWhichIncludesAtLeastOneMail()
'
'    msubOutputOutlookMailFolderInfoToBook True, True
'End Sub
'
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
''**---------------------------------------------
''** Output Outlook collected information to Excel book
''**---------------------------------------------
''''
''''
''''
'Private Sub msubOutputOutlookMailItemsInfoToTemporaryBook(ByVal vobjFolder As Outlook.Folder)
'
'    Dim strBookPath As String
'
'    strBookPath = mfstrGetTemporaryOutputBookDir() & "\CurrentOutlookMailItemsInformation.xlsx"
'
'    msubOutputOutlookMailItemsInfoToBook vobjFolder, strBookPath
'End Sub
'
'
'
''''
''''
''''
'Private Sub msubOutputOutlookMailItemsInfoToBook(ByVal vobjFolder As Outlook.Folder, ByVal vstrBookPath As String)
'
'    Dim objSheet As Excel.Worksheet, objBook As Excel.Workbook, strBookPath As String
'    Dim objMailInfoCol As Collection, objCollectingDataStopWatch As DoubleStopWatch, objOutputSheetStopWatch As DoubleStopWatch
'
'
'    Set objCollectingDataStopWatch = New DoubleStopWatch
'    Set objOutputSheetStopWatch = New DoubleStopWatch
'
'
'    Set objBook = GetWorkbookAndPrepareForUnitTest(vstrBookPath)
'
'    Set objSheet = GetNewWorksheetAfterAllExistedSheetsFromBook(objBook)
'
'    objSheet.Name = "MailItems"
'
'
'    objCollectingDataStopWatch.MeasureStart
'
'    Set objMailInfoCol = GetMailItemInfoColInAFolder(vobjFolder)
'
'    objCollectingDataStopWatch.MeasureInterval
'
'
'    objOutputSheetStopWatch.MeasureStart
'
'    OutputColToSheetRestrictedByDataTableSheetFormatter objSheet, objMailInfoCol, mfobjGetDataTableSheetFormatterForOutlookMailItems()
'
'
'    SortXlSheetBasically objSheet, "SenderName,Ascending,SenderEmailAddress,Ascending,ReceivedTime,Decending"
'
'    SetMergedCellsToTable objSheet, GetColFromLineDelimitedChar("SenderName,SenderEmailAddress,NameOfSenderObject"), 1, 1, True
'
'    objOutputSheetStopWatch.MeasureInterval
'
'
'    AddAutoShapeGeneralMessage objSheet, mfstrGetLogOfOutlookMailFolder(vobjFolder, objCollectingDataStopWatch, objOutputSheetStopWatch)
'
'    DeleteDummySheetWhenItExists objBook
'End Sub
'
'
''''
''''
''''
'Private Function mfstrGetLogOfOutlookMailFolder(ByVal vobjFolder As Outlook.Folder, ByVal vobjCollectingDataStopWatch As DoubleStopWatch, ByVal vobjOutputSheetStopWatch As DoubleStopWatch) As String
'
'    Dim strLog As String
'
'    strLog = "Logged time: " & Format(Now(), "ddd, yyyy/mm/dd hh:mm:ss") & vbNewLine
'
'    strLog = strLog & "Collecting data elasped time: " & vobjCollectingDataStopWatch.ElapsedTimeByString & vbNewLine
'
'    strLog = strLog & "Output Excel sheet elasped time: " & vobjOutputSheetStopWatch.ElapsedTimeByString & vbNewLine
'
'    strLog = strLog & "User name: " & vobjFolder.Session.CurrentUser & vbNewLine
'
'    strLog = strLog & "Folder path: " & vobjFolder.FolderPath & vbNewLine
'
'    strLog = strLog & "Count of mail-items: " & vobjFolder.Items.Count & vbNewLine
'
'    strLog = strLog & "Count of unread mail-items: " & vobjFolder.UnReadItemCount
'
'    mfstrGetLogOfOutlookMailFolder = strLog
'End Function
'
'
'
''''
''''
''''
'Private Sub msubOutputOutlookMailFolderInfoToBook(Optional ByVal vblnCollectMailDetailInfo As Boolean = False, Optional ByVal vblnFilterOfIncludingAtLeastOneMail As Boolean = False)
'
'    Dim objSheet As Excel.Worksheet, objBook As Excel.Workbook, strBookPath As String
'    Dim objMailFolderInfoCol As Collection
'
'    If vblnCollectMailDetailInfo Then
'
'        strBookPath = mfstrGetTemporaryOutputBookDir() & "\CurrentOutlookMailFolderPathsWithDetails.xlsx"
'    Else
'        strBookPath = mfstrGetTemporaryOutputBookDir() & "\CurrentOutlookMailFolderPaths.xlsx"
'    End If
'
'    Set objBook = GetWorkbookAndPrepareForUnitTest(strBookPath)
'
'    Set objSheet = GetNewWorksheetAfterAllExistedSheetsFromBook(objBook)
'
'    If vblnCollectMailDetailInfo Then
'
'        objSheet.Name = "MailFolderDetails"
'    Else
'        objSheet.Name = "MailFolderPaths"
'    End If
'
'    Set objMailFolderInfoCol = GetMailFolderPathsWithSubFolders(vblnCollectMailDetailInfo, vblnFilterOfIncludingAtLeastOneMail)
'
'
'    OutputColToSheetRestrictedByDataTableSheetFormatter objSheet, objMailFolderInfoCol, GetDataTableSheetFormatterForOutlookMailFolderPath(vblnCollectMailDetailInfo)
'
'    If vblnFilterOfIncludingAtLeastOneMail Then
'
'        SortXlSheetBasically objSheet, "Name,Ascending"
'
'        AddAutoShapeGeneralMessage objSheet, "Filtered mail-folders by including at least one mail", AutoShapeMessageFillPatternType.XlShapeTextLogInteriorFillGradientYellow
'    End If
'
'    DeleteDummySheetWhenItExists objBook
'End Sub
'
'
''''
''''
''''
'Private Function mfstrGetTemporaryOutputBookDir() As String
'
'    Dim strPath As String
'
'    strPath = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks"
'
'    ForceToCreateDirectory strPath
'
'    mfstrGetTemporaryOutputBookDir = strPath
'End Function
'
'
''''
''''
''''
'Private Function mfobjGetDataTableSheetFormatterForOutlookMailItems() As DataTableSheetFormatter
'
'    Dim objDTSheetFormatter As DataTableSheetFormatter
'
'    Set objDTSheetFormatter = New DataTableSheetFormatter
'
'    With objDTSheetFormatter
'
'        Set .FieldTitleToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("Subject,40,SenderName,30,NameOfSenderObject,15,SenderEmailAddress,25,ReceivedTime,18,CountOfAttachments,8,BodyFormat,8,Size,8,UnRead,8")
'
'        .FieldTitleInteriorType = ColumnNameInteriorOfGatheredColTable
'
'        .RecordBordersType = RecordCellsGrayAll
'
'        Set .ColumnsNumberFormatLocal = New ColumnsNumberFormatLocalParam
'
'        With .ColumnsNumberFormatLocal
'
'            .SetNumberFormatLocalAndApplyFieldTitles "ddd, yyyy/m/d hh:mm", GetColFromLineDelimitedChar("ReceivedTime")
'
'        End With
'
''        .AllowToMergeCellsByContinuousSameValues = True
''        .AllowToSetVerticalTopAlignmentTopOfMergeCells = True
''        Set .FieldTitlesToMergeCells = GetColFromLineDelimitedChar("SenderName,SenderEmailAddress")
'
'    End With
'
'    Set mfobjGetDataTableSheetFormatterForOutlookMailItems = objDTSheetFormatter
'End Function
'
'
'

