Attribute VB_Name = "Module6"
'
'   PackedExperimentalMetaVba [ 06 / 06 ] Self-extracting comment-block packed VBA source codes, which is generated automatically
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Count of packed modules:
'       43 / 393
'
'   Lack user-form sources:
'       UTestFormTextRClickClipBoard - CommonOfficeVBA\FormTools\UnitTest\UTestFormTextRClickClipBoard.frm
'       UInputPasswordBox - CommonOfficeVBA\FormTools\ManagePassword\UInputPasswordBox.frm
'       LogTextForm - CommonOfficeVBA\LoggingModules\LogTextForm\LogTextForm.frm
'       UProgressBarForm - CommonOfficeVBA\UserFormTools\UProgressBarForm.frm
'       UTestFormNoFormTop - CommonOfficeVBA\WindowControlModules\CommonControlHandlers\UnitTest\UTestFormNoFormTop.frm
'       UTestFormWithFormTop - CommonOfficeVBA\WindowControlModules\CommonControlHandlers\UnitTest\UTestFormWithFormTop.frm
'       UTestFormKeepStateReg - CommonOfficeVBA\RegistryAccess\UTestFormKeepStateReg.frm
'       CodePaneSwitchMultiLstForm - CommonOfficeVBA\DevelopmentAssist\SwitchCodePanes\CodePaneSwitchMultiLstForm.frm
'       CodePaneSwitchMultiLsvForm - CommonOfficeVBA\DevelopmentAssist\SwitchCodePanes\CodePaneSwitchMultiLsvForm.frm
'       CodePaneSwitchSingleLsvForm - CommonOfficeVBA\DevelopmentAssist\SwitchCodePanes\CodePaneSwitchSingleLsvForm.frm
'       USetAdoOleDbConStrForXlBook - CommonOfficeVBA\ADOModules\USetAdoOleDbConStrForXlBook.frm
'       USetAdoOdbcConStrForSqLite - CommonOfficeVBA\SQLiteConnector\USetAdoOdbcConStrForSqLite.frm
'       USetAdoOleDbConStrForAccdb - CommonOfficeVBA\ADOModules\MsAccessTool\USetAdoOleDbConStrForAccdb.frm
'       USetAdoOdbcConStrForPgSql - CommonOfficeVBA\PostgreSQLConnector\USetAdoOdbcConStrForPgSql.frm
'       USetAdoOdbcConStrForOracle - CommonOfficeVBA\OracleConnector\USetAdoOdbcConStrForOracle.frm
'       UErrorADOSQLForm - CommonOfficeVBA\ADOModules\AdoErrorHandling\UErrorADOSQLForm.frm
'
'   In this packed source files:
'       UTfDecorateXlShapeFontInterior.bas, UTfAddAutoShapes.bas, ReadFromShapeOnXlSheet.bas, UTfReadFromShapeOnXlSheet.bas, WinRegViewForXl.bas
'       UTfWinAPIRegEnumForXl.bas, MakeHyperLinksModule.bas, EnumerateAppWindowsForXl.bas, EnumerateMsExcels.bas, EnumerateMsExcelsAndClose.bas
'       UTfCreateDataTableForXl.bas, SolveSavePathForXl.bas, UTfCurrentLocalize.bas, AddAutoLogShapesForAdo.bas, ADOSheetOut.bas
'       ADOSheetIn.bas, ADOExSheetOut.bas, UTfADOExSheetOut.bas, ADOSheetFormatter.cls, ADOLogTextSheetOut.bas
'       ADOFailedLogSheetOut.bas, UTfADOSheetExpander.bas, SQLGeneralFromXlSheet.bas, ExpandAdoRsetOnSheet.bas, AppendAdoRsetOnSheet.bas
'       IExpandAdoRecordsetOnSheet.cls, AdoRSetSheetExpander.cls, ISqlRsetSheetExpander.cls, XlAdoSheetExpander.cls, UTfAdoConnectExcelSheet.bas
'       SqlUTfXlSheetExpander.bas, SqlUTfXlSheetExpanderByPassword.bas, CsvAdoSheetExpander.cls, SqlUTfCsvAdoSheetExpander.bas, UTfAdoConnectAccDbForXl.bas
'       AccDbAdoSheetExpander.cls, SqlUTfAccDbSheetExpander.bas, PgSqlOdbcSheetExpander.cls, SqlUTfPgSqlOdbcSheetExpander.bas, OracleOdbcSheetExpander.cls
'       SqLiteAdoSheetExpander.cls, SqlUTfSqLiteOdbcSheetExpander.bas, ExpandCodeOnWORDOut.bas
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Mon, 22/Jan/2024    Tarclanus-generator     Generated automatically
'
Option Explicit

'///////////////////////////////////////////////
'/// Directives
'///////////////////////////////////////////////
#Const HAS_REF = False

'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////

Private Const mstrThisExtractionModuleName As String = "Module6"

Private Const mstrALoadingDirectionVBProjectName As String = "ConvergenceProj"

'**---------------------------------------------
'** Block preserved keyword definitions
'**---------------------------------------------
Private Const mstrBlockDelimiterOfCodesBegin As String = "'''--Begin the comment out codes block--"

Private Const mstrBlockDelimiterOfASourceFileBeginLeft As String = "'''--VBA_Code_File--<"

Private Const mstrBlockDelimiterOfASourceFileBeginRight As String = ">--"

'**---------------------------------------------
'** Temporary files paths
'**---------------------------------------------
Private Const mstrTmpSubDir As String = "TmpCBSubDirModule6"


'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
''' Extract all VBA source files from this file comment block and load these into the specified VBA project name Macro book
'''
Public Sub ExtractVbaCodesFrom_PackedExperimentalMetaVba06_InThisCommentBlock()

    Dim objBook As Excel.Workbook
    
    Set objBook = mfobjGetBookAfterLoadingVbaCodesFromThisVbaModuleCommentBlock()
End Sub


'''
'''
'''
Private Sub msubSanityTestToWriteSourceCodesFromThisVbaModuleCommentBlockToTemporaryDirectory()

    msubWriteSourceCodesFromThisVbaModuleCommentBlockToTemporaryDirectory
End Sub


'''
'''
'''
Private Sub msubSanityTestToGetFileNameToTextDicFromThisVbaModuleCommentBlock()

    Dim objDic As Scripting.Dictionary: Set objDic = mfobjGetFileNameToTextDicFromThisVbaModuleCommentBlock()

    'DebugDic objDic
End Sub

'''
'''
'''
Private Sub msubSanityTestToConfirmThisText()

    Debug.Print mfstrGetThisVbaModuleText()
End Sub



'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////

'''
'''
'''
Private Function mfobjGetBookAfterLoadingVbaCodesFromThisVbaModuleCommentBlock(Optional ByVal vstrTmpVbaCodesSubDir As String = mstrTmpSubDir) As Excel.Workbook

    Dim objBook As Excel.Workbook
    
    Dim strTmpDir As String
   
#If HAS_REF Then

    Dim objFS As Scripting.FileSystemObject, objDir As Scripting.Folder, objFile As Scripting.File
    
    Dim objVBProject As VBIDE.VBProject
    
    Set objFS = New Scripting.FileSystemObject
#Else
    Dim objFS As Object, objDir As Object, objFile As Object
    
    Dim objVBProject As Object
    
    Set objFS = CreateObject("Scripting.FileSystemObject")
#End If
  
    msubWriteSourceCodesFromThisVbaModuleCommentBlockToTemporaryDirectory vstrTmpVbaCodesSubDir


    Set objBook = mfobjGetTargetWorkbookOfSpecifiedVBProjectName()

    Set objVBProject = objBook.VBProject

    strTmpDir = ThisWorkbook.Path & "\" & vstrTmpVbaCodesSubDir

    Set objDir = objFS.GetFolder(strTmpDir)

    For Each objFile In objDir.Files
    
        objVBProject.VBComponents.Import objFile.Path
    Next

    Set mfobjGetBookAfterLoadingVbaCodesFromThisVbaModuleCommentBlock = objBook
End Function

'''
'''
'''
Private Function mfobjGetTargetWorkbookOfSpecifiedVBProjectName() As Excel.Workbook

    Dim objBook As Excel.Workbook, objTargetBook As Excel.Workbook
    
#If HAS_REF Then

    Dim objVBProject As VBIDE.VBProject
#Else
    Dim objVBProject As Object
#End If

    Set objTargetBook = Nothing

    For Each objBook In ThisWorkbook.Application.Workbooks
    
        Set objVBProject = objBook.VBProject
        
        If StrComp(objVBProject.Name, mstrALoadingDirectionVBProjectName) = 0 Then
        
            Set objTargetBook = objBook
            
            Exit For
        End If
    Next
    
    If objTargetBook Is Nothing Then
    
        Set objTargetBook = ThisWorkbook.Application.Workbooks.Add()
        
        objTargetBook.VBProject.Name = mstrALoadingDirectionVBProjectName
    End If

    Set mfobjGetTargetWorkbookOfSpecifiedVBProjectName = objTargetBook
End Function


'''
'''
'''
Private Sub msubWriteSourceCodesFromThisVbaModuleCommentBlockToTemporaryDirectory(Optional ByVal vstrTmpVbaCodesSubDir As String = mstrTmpSubDir)

#If HAS_REF Then
    Dim objSourceFileNameToCodesDic As Scripting.Dictionary
    
    Dim objFS As Scripting.FileSystemObject, objFile As Scripting.File
    
    Set objFS = New Scripting.FileSystemObject
#Else
    Dim objSourceFileNameToCodesDic As Object
    
    Dim objFS As Object, objFile As Object
    
    Set objFS = CreateObject("Scripting.FileSystemObject")
#End If

    Dim varSourceFileName As Variant, strSourceFilePath As String
    Dim strTmpDir As String
    
    
    strTmpDir = ThisWorkbook.Path & "\" & vstrTmpVbaCodesSubDir
    
    With objFS
    
        If Not .FolderExists(strTmpDir) Then
        
            .CreateFolder strTmpDir
        Else
            ' delete all
            
            For Each objFile In .GetFolder(strTmpDir).Files
        
                objFile.Delete
            Next
        End If
    End With
    
    
    Set objSourceFileNameToCodesDic = mfobjGetFileNameToTextDicFromThisVbaModuleCommentBlock()

    With objSourceFileNameToCodesDic

        For Each varSourceFileName In .Keys
        
            strSourceFilePath = strTmpDir & "\" & varSourceFileName
        
            With objFS
            
                With .CreateTextFile(strSourceFilePath, True)
                
                    .Write objSourceFileNameToCodesDic.Item(varSourceFileName)
                
                    .Close
                End With
            End With
        Next
    End With
End Sub


'''
'''
'''
#If HAS_REF Then
Private Function mfobjGetFileNameToTextDicFromThisVbaModuleCommentBlock() As Scripting.Dictionary

    Dim objDic As Scripting.Dictionary: Set objDic = New Scripting.Dictionary
#Else
Private Function mfobjGetFileNameToTextDicFromThisVbaModuleCommentBlock() As Object

    Dim objDic As Object: Set objDic = CreateObject("Scripting.Dictionary")
#End If
    Dim varLineText As Variant
    Dim blnFoundBlockOfCodesBegin As Boolean
    Dim blnInASourceFileBlock As Boolean, strCurrentSourceFileName As String
    Dim intLeftPoint As Long, intRightPoint As Long, intChildSourceLineNumber As Long
    Dim strCurrentChildSourceText As String
    
    
    blnFoundBlockOfCodesBegin = False
    
    blnInASourceFileBlock = False
    
    strCurrentChildSourceText = ""

    For Each varLineText In Split(mfstrGetThisVbaModuleText(), vbNewLine)

        If Not blnFoundBlockOfCodesBegin Then
        
            If InStr(1, varLineText, "'") = 1 Then
            
                If InStr(1, varLineText, mstrBlockDelimiterOfCodesBegin) > 0 Then
                
                    blnFoundBlockOfCodesBegin = True
                End If
            End If
        End If

        If blnFoundBlockOfCodesBegin Then
        
            If Not blnInASourceFileBlock Then
            
                intLeftPoint = InStr(1, varLineText, mstrBlockDelimiterOfASourceFileBeginLeft)
                
                If intLeftPoint > 0 Then
                
                    intRightPoint = InStrRev(varLineText, mstrBlockDelimiterOfASourceFileBeginRight)
                    
                    strCurrentSourceFileName = Mid(varLineText, intLeftPoint + Len(mstrBlockDelimiterOfASourceFileBeginLeft), intRightPoint - (intLeftPoint + Len(mstrBlockDelimiterOfASourceFileBeginLeft)))
                    
                    intChildSourceLineNumber = 0
                    
                    blnInASourceFileBlock = True
                End If
            End If
            
            
            If blnInASourceFileBlock Then
            
                If intChildSourceLineNumber > 0 Then
                
                    intLeftPoint = InStr(1, varLineText, mstrBlockDelimiterOfASourceFileBeginLeft)
                    
                    If intLeftPoint > 0 Then
                    
                        ' save cache the current source text
                        If strCurrentChildSourceText <> "" Then
                        
                            objDic.Add strCurrentSourceFileName, strCurrentChildSourceText
                        End If
                    
                    
                        ' Prepare the new file
                        intRightPoint = InStrRev(varLineText, mstrBlockDelimiterOfASourceFileBeginRight)
                    
                        strCurrentSourceFileName = Mid(varLineText, intLeftPoint + Len(mstrBlockDelimiterOfASourceFileBeginLeft), intRightPoint - (intLeftPoint + Len(mstrBlockDelimiterOfASourceFileBeginLeft)))
                    
                        intChildSourceLineNumber = 0
                    
                        strCurrentChildSourceText = ""
                        
                        blnInASourceFileBlock = True
                    End If
                End If
                
                If intChildSourceLineNumber > 0 Then
                
                    ' confirm the line head character is the line comment char
                    If InStr(1, varLineText, "'") = 1 Then
                    
                        If strCurrentChildSourceText <> "" Then
                        
                            strCurrentChildSourceText = strCurrentChildSourceText & vbNewLine
                        End If
                    
                        ' get a source code line
                    
                        strCurrentChildSourceText = strCurrentChildSourceText & Right(varLineText, Len(varLineText) - 1)
                    Else
                        ' save cache the current source text
                        If strCurrentChildSourceText <> "" Then
                        
                            objDic.Add strCurrentSourceFileName, strCurrentChildSourceText
                        End If
                        
                        intChildSourceLineNumber = 0
                    
                        strCurrentChildSourceText = ""
                        
                        blnInASourceFileBlock = False
                    End If
                End If
            
                intChildSourceLineNumber = intChildSourceLineNumber + 1
            End If
        End If
    Next

    Set mfobjGetFileNameToTextDicFromThisVbaModuleCommentBlock = objDic
End Function


'''
'''
'''
Private Function mfstrGetThisVbaModuleText() As String

#If HAS_REF Then
    Dim objVBProject As VBIDE.VBProject, objVBComponent As VBIDE.VBComponent
#Else
    Dim objVBProject As Object, objVBComponent As Object
#End If
    Dim strText As String
    
    Set objVBProject = ThisWorkbook.VBProject
    
    Set objVBComponent = objVBProject.VBComponents.Item(mstrThisExtractionModuleName)

    With objVBComponent.CodeModule
    
        strText = .Lines(1, .CountOfLines)
    End With

    mfstrGetThisVbaModuleText = strText
End Function


'''--Begin the comment out codes block--
'''--VBA_Code_File--<UTfDecorateXlShapeFontInterior.bas>--
'Attribute VB_Name = "UTfDecorateXlShapeFontInterior"
''
''   Sanity test for decorate Excel Auto-shape interior and font
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on Excel
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Wed,  3/May/2023    Kalmclaeyd Tarclanus    Initial creation
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Sanity tests
''///////////////////////////////////////////////
''''
'''' comprehensive AutoShapeLog enumeration test
''''
'Private Sub msubComprehensiveTestToCreateExcelShapeOfAutoShapeLogType()
'
'    Dim objSheet As Excel.Worksheet, strBookPath As String, objShape As Excel.Shape
'    Dim enmAutoShapeLog As AutoShapeLog, objAutoShapeAdjuster As ASWindowPositionAdjuster, varAutoShapeLog As Variant
'
'
'    strBookPath = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\AutoShapeGeneratingComprehensiveTests.xlsx"
'
'    Set objSheet = GetWorkbookAndAnWorksheetPrepareForUnitTest(strBookPath, "TypesOfAutoShapeLogs")
'
'    Set objAutoShapeAdjuster = New ASWindowPositionAdjuster
'
'    For Each varAutoShapeLog In mfobjGetAutoShapeLogCol()
'
'        enmAutoShapeLog = varAutoShapeLog
'
'        Set objShape = GetAdjustedPositionXlAutoShapeByAutoShapeLogType(objSheet, enmAutoShapeLog, objAutoShapeAdjuster)
'
'        objShape.TextFrame2.TextRange.Characters.Text = "Enumeration " & GetAutoShapeLogTextFromEnm(enmAutoShapeLog) & " test-text"
'
'        DecorateFontForeColorAndInterior objShape, enmAutoShapeLog
'    Next
'
'    ' logging
'    objSheet.Cells(1.1).Value = "Logged time: " & Format(Now(), "ddd, yyyy/m/d hh:mm:ss")
'
'End Sub
'
''''
'''' test for GetShapeAfterCreateLogTextShapeOfSpecifiedCharacteristics operation
''''
'Private Sub msubSanityTestToGetShapeAfterCreateLogTextShapeOfSpecifiedCharacteristics()
'
'    ' GetShapeAfterCreateLogTextShapeOfSpecifiedCharacteristics
'
'    Dim objSheet As Excel.Worksheet, strBookPath As String, objShape As Excel.Shape, strDisplayText As String
'    Dim enmAutoShapeLog As AutoShapeLog, objAutoShapeAdjuster As ASWindowPositionAdjuster, varAutoShapeLog As Variant
'
'
'    strBookPath = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\AutoShapeGeneratingComprehensiveTests.xlsx"
'
'    Set objSheet = GetWorkbookAndAnWorksheetPrepareForUnitTest(strBookPath, "TypesOfAutoShapeLogs")
'
'    Set objAutoShapeAdjuster = New ASWindowPositionAdjuster
'
'    For Each varAutoShapeLog In mfobjGetAutoShapeLogCol()
'
'        enmAutoShapeLog = varAutoShapeLog
'
'        strDisplayText = "Enumeration " & GetAutoShapeLogTextFromEnm(enmAutoShapeLog) & " test-text"
'
'        Set objShape = GetShapeAfterCreateLogTextShapeOfSpecifiedCharacteristics(objSheet, objAutoShapeAdjuster, enmAutoShapeLog, strDisplayText)
'    Next
'
'    ' logging
'    objSheet.Cells(1, 1).Value = "Sanity-test for GetShapeAfterCreateLogTextShapeOfSpecifiedCharacteristics"
'
'    objSheet.Cells(2, 1).Value = "Logged time: " & Format(Now(), "ddd, yyyy/m/d hh:mm:ss")
'End Sub
'
'
'
''''
'''' comprehensive AutoShapeMessageFillPatternType enumeration test
''''
'Private Sub msubComprehensiveTestToCreateExcelShapeOfAutoShapeMessageFillPatternType()
'
'    Dim objSheet As Excel.Worksheet, strBookPath As String, objShape As Excel.Shape
'    Dim enmAutoShapeMessageFillPatternType As AutoShapeMessageFillPatternType, objAutoShapeAdjuster As ASWindowPositionAdjuster, varAutoShapeMessageFillPatternType As Variant
'
'
'    strBookPath = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\AutoShapeGeneratingComprehensiveTests.xlsx"
'
'    Set objSheet = GetWorkbookAndAnWorksheetPrepareForUnitTest(strBookPath, "TypesOfShapeFillPattern")
'
'    Set objAutoShapeAdjuster = New ASWindowPositionAdjuster
'
'
'    For Each varAutoShapeMessageFillPatternType In mfobjGetAutoShapeMessageFillPatternTypeCol()
'
'        enmAutoShapeMessageFillPatternType = varAutoShapeMessageFillPatternType
'
'        Set objShape = GetAdjustedPositionXlAutoShapeByAutoShapeLogType(objSheet, XlShapeTextLogOfGeneral, objAutoShapeAdjuster)
'
'        objShape.TextFrame2.TextRange.Characters.Text = "Enumeration " & GetAutoShapeMessageFillPatternTypeTextFromEnm(enmAutoShapeMessageFillPatternType) & " test-text"
'
'        DecorateFontForeColorAndInterior objShape, XlShapeTextLogOfGeneral, enmAutoShapeMessageFillPatternType
'    Next
'
'    ' logging
'    objSheet.Cells(1.1).Value = "Logged time: " & Format(Now(), "ddd, yyyy/m/d hh:mm:ss")
'End Sub
'
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
''''
''''
''''
'Private Function mfobjGetAutoShapeLogCol() As Collection
'
'    Dim objCol As Collection
'
'    Set objCol = New Collection
'
'    With objCol
'
'        .Add AutoShapeLog.XlShapeTextLogOfGeneral
'
'        .Add AutoShapeLog.XlShapeTextLogOfUsedActualSQL
'
'        .Add AutoShapeLog.XlShapeTextLogOfElapsedTimeAndRowCountAppearance
'
'        .Add AutoShapeLog.XlShapeTextLogOfInputSourceTableFilePath
'
'        .Add AutoShapeLog.XlShapeTextLogOfMetaDesignInformation
'    End With
'
'    Set mfobjGetAutoShapeLogCol = objCol
'End Function
'
''''
''''
''''
'Private Function mfobjGetAutoShapeMessageFillPatternTypeCol() As Collection
'
'    Dim objCol As Collection
'
'    Set objCol = New Collection
'
'    With objCol
'
'        .Add AutoShapeMessageFillPatternType.XlShapeTextLogInteriorFillDefault
'
'        .Add AutoShapeMessageFillPatternType.XlShapeTextLogInteriorFillGradientYellow
'
'        .Add AutoShapeMessageFillPatternType.XlShapeTextLogInteriorFillRedCornerGradient
'
'        .Add AutoShapeMessageFillPatternType.XlShapeTextLogInteriorFillOfWinRegistry
'
'        .Add AutoShapeMessageFillPatternType.XlShapeTextLogInteriorFillOfNearPureBlackForDataTableTexts
'    End With
'
'    Set mfobjGetAutoShapeMessageFillPatternTypeCol = objCol
'
'End Function
'
'
'''--VBA_Code_File--<UTfAddAutoShapes.bas>--
'Attribute VB_Name = "UTfAddAutoShapes"
''
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       This is dependent on Excel refercence
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Tue, 10/May/2022    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Constants
''///////////////////////////////////////////////
'
'' Sanity test sheet name
'Private Const mstrSheetNamePrefix = "ASTAddingTest" ' "AutoShapeTextAddingTest"
'
''///////////////////////////////////////////////
''/// Enumerations
''///////////////////////////////////////////////
''''
'''' unit-test mode change enumeration
''''
'Private Enum AddShapeTestMode
'
'    Normal = 0
'
'    AddtionalHeaderText = 1
'
'    IndependentMessageAdd = 2
'End Enum
'
'
''///////////////////////////////////////////////
''/// Sanity tests
''///////////////////////////////////////////////
''''
''''
''''
'Public Sub SanityTestToAddAutoShapeMetaDesignInformation()
'
'    Dim objSheet As Excel.Worksheet, strBookPath As String
'
'    strBookPath = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\AutoShapeGeneratingSanityTests.xlsx"
'
'    Set objSheet = GetWorkbookAndAnWorksheetPrepareForUnitTest(strBookPath, "TestOfAutoShapeLogs")
'
'    msubSanityTestsToAddAutoShapeMetaDesignInformation objSheet
'End Sub
'
''''
''''
''''
'Public Sub SanityTestToAddAutoShapeMetaDesignInformationOnActiveSheet()
'
'    Dim objSheet As Excel.Worksheet
'
'    Set objSheet = ActiveSheet
'
'    msubSanityTestsToAddAutoShapeMetaDesignInformation objSheet
'End Sub
'
''''
''''
''''
'Private Sub msubSanityTestsToAddAutoShapeMetaDesignInformation(ByRef robjSheet As Excel.Worksheet)
'
'    AddAutoShapeMetaDesignInformation robjSheet, "TestInfo1"
'
'    AddAutoShapeMetaDesignInformation robjSheet, "TestInfo2", "Test-Subject"
'
'    AddAutoShapeMetaDesignInformation robjSheet, "Key06: Opq", "Test-Subject02"
'End Sub
'
'
''''
''''
''''
'Public Sub SanityTestToAddAutoShapeGeneralMessage()
'
'    Dim objSheet As Excel.Worksheet, strBookPath As String
'
'    strBookPath = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\AutoShapeGeneratingSanityTests.xlsx"
'
'    Set objSheet = GetWorkbookAndAnWorksheetPrepareForUnitTest(strBookPath, "TestOfAutoShapeLogs")
'
'    ClearCacheOfBackgroundRGBToBoolOfUsingWhiteFontDic
'
'    msubSanityTestToAddAutoShapeGeneralMessageAboutAutoShapeMessageFillPatternType objSheet
'End Sub
'
''''
''''
''''
'Public Sub SanityTestToAddAutoShapeGeneralMessageOnActiveSheet()
'
'    Dim objSheet As Excel.Worksheet
'
'    Set objSheet = ActiveSheet
'
'    ClearCacheOfBackgroundRGBToBoolOfUsingWhiteFontDic
'
'    msubSanityTestToAddAutoShapeGeneralMessageAboutAutoShapeMessageFillPatternType objSheet
'End Sub
'
''''
''''
''''
'Private Sub msubSanityTestToAddAutoShapeGeneralMessageAboutAutoShapeMessageFillPatternType(ByRef robjSheet As Excel.Worksheet)
'
'    AddAutoShapeGeneralMessage robjSheet, "Test1"
'
'    AddAutoShapeGeneralMessage robjSheet, "Key02: Abc"
'
'    AddAutoShapeGeneralMessage robjSheet, "Test2", AutoShapeMessageFillPatternType.XlShapeTextLogInteriorFillGradientYellow
'
'    AddAutoShapeGeneralMessage robjSheet, "Key03: Def", AutoShapeMessageFillPatternType.XlShapeTextLogInteriorFillGradientYellow
'
'    AddAutoShapeGeneralMessage robjSheet, "Test3", XlShapeTextLogInteriorFillOfWinRegistry
'
'    AddAutoShapeGeneralMessage robjSheet, "Key04: Hij", XlShapeTextLogInteriorFillOfWinRegistry
'
'    AddAutoShapeGeneralMessage robjSheet, "Key05: Klm", XlShapeTextLogInteriorFillRedCornerGradient
'End Sub
'
'
''''
'''' get cell position of the ActiveCell
''''
'Private Sub GetActiveCellPositionToMsgBox()
'
'    Dim strRes As String, objBook As Excel.Workbook, objSheet As Excel.Worksheet
'    Dim objWindow As Excel.Window, objShape As Excel.Shape
'
'    Set objBook = ActiveCell.Worksheet.Parent
'
'    Set objWindow = Application.Windows.Item(objBook.Name)
'
'    With ActiveCell
'
'        strRes = "Cell Left: " & CStr(.Left) & ", Cell Top: " & CStr(.Top) & ", Cell Width: " & CStr(.Width) & ", Cell Height: " & CStr(.Height)
'    End With
'
'    With objWindow
'
'        strRes = strRes & vbCrLf & "Window Left: " & CStr(.Left) & ", Window Top: " & CStr(.Top) & ", Window Width: " & CStr(.Width) & ", Window Height: " & CStr(.Height)
'    End With
'
'
'    'MsgBox strRes
'    Debug.Print strRes
'
'
'
'    On Error Resume Next
'
'    Set objSheet = ActiveCell.Worksheet
'
'    Set objShape = Nothing
'
'    Debug.Print TypeName(Selection)
'
'    For Each objShape In objSheet.Shapes
'
'        With objShape
'
'            strRes = "ShapeName: = " & .Name & ",  Left: " & CStr(.Left) & ",  Top: " & CStr(.Top) & ",  Width: " & CStr(.Width) & ",  Height: " & CStr(.Height)
'        End With
'
'        Debug.Print strRes
'    Next
'
'    On Error GoTo 0
'
'End Sub
'
''''
'''' delete auto created shapes by unit test
''''
'Private Sub msubDeleteAutoAddedRectangleTestShapes()
'
'    Dim blnOldDisplayAlerts As Boolean
'    Dim objSheet As Excel.Worksheet
'    Dim objApplication As Excel.Application
'
'    Set objApplication = ThisWorkbook.Application
'
'    With objApplication
'
'        blnOldDisplayAlerts = .DisplayAlerts
'
'        .DisplayAlerts = False
'
'        On Error GoTo ErrorHandler:
'
'        For Each objSheet In ThisWorkbook.Worksheets
'
'            If InStr(1, objSheet.Name, mstrSheetNamePrefix) > 0 Then
'
'                objSheet.Delete
'            End If
'        Next
'
'ErrorHandler:
'        .DisplayAlerts = blnOldDisplayAlerts
'    End With
'
'End Sub
'
'
''''
''''
''''
'Private Sub msubAddRectangleShapeUnitTest()
'
'    Dim objBook As Excel.Workbook, strBookPath As String
'
'    strBookPath = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\AutoShapeSimplyGenerateTests.xlsx"
'
'    Set objBook = GetWorkbookAndPrepareForUnitTest(strBookPath)
'
'
'    msubAddGeneralMessageWithSheetPadding objBook, 1, 3
'
'    msubAddGeneralMessageWithSheetPadding objBook, 55, 5
'
'    msubAddGeneralMessageWithSheetPadding objBook, 5, 55, AutoShapeMessageFillPatternType.XlShapeTextLogInteriorFillOfWinRegistry
'
'    msubAddGeneralMessageWithSheetPadding objBook, 20, 5, AutoShapeMessageFillPatternType.XlShapeTextLogInteriorFillGradientYellow
'
'    msubAddGeneralMessageWithSheetPadding objBook, 20, 5, AutoShapeMessageFillPatternType.XlShapeTextLogInteriorFillRedCornerGradient
'
'
'    DeleteDummySheetWhenItExists objBook
'End Sub
'
'
''''
''''
''''
'Private Sub msubSanityTestToCreateObjectOfTextAutoShapePositionSize()
'
'    Dim objTextAutoShapePositionSize As TextAutoShapePositionSize
'
'    Set objTextAutoShapePositionSize = New TextAutoShapePositionSize
'
'    Debug.Print "Created a TextAutoShapePositionSize object"
'End Sub
'
''''
''''
''''
'Private Sub msubSanityTestToCreateObjectOfTextAutoShapeFontInterior()
'
'    Dim objTextAutoShapeFontInterior As TextAutoShapeFontInterior
'
'    Set objTextAutoShapeFontInterior = New TextAutoShapeFontInterior
'
'    Debug.Print "Created a TextAutoShapeFontInterior object"
'
'End Sub
'
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
''''
'''' test pattern codes
''''
'Private Sub msubAddGeneralMessageWithSheetPadding(ByVal vobjBook As Excel.Workbook, _
'        ByVal vintCountOfRows As Long, _
'        ByVal vintCountOfColumns As Long, _
'        Optional ByVal venmAutoShapeMessageFillPatternType As AutoShapeMessageFillPatternType = AutoShapeMessageFillPatternType.XlShapeTextLogInteriorFillDefault)
'
'
'    Dim objSheet As Excel.Worksheet, objShape As Excel.Shape, strSheetName As String
'
'    strSheetName = mstrSheetNamePrefix & "GMessage_R" & CStr(vintCountOfRows) & "_C" & CStr(vintCountOfColumns)
'
'    Set objSheet = GetNewWorksheetAfterAllExistedSheetsFromBook(vobjBook)
'
'    With objSheet
'
'        .Name = FindNewSheetNameWhenAlreadyExist(strSheetName, vobjBook)
'    End With
'
'    SetDummyValues objSheet, vintCountOfRows, vintCountOfColumns
'
'    AddAutoShapeGeneralMessage objSheet, "General Single Message adding-test at " & FormatDateTime(Now(), vbLongDate) & ", " & FormatDateTime(Now(), vbLongTime), venmAutoShapeMessageFillPatternType
'End Sub
'
'
'
''''
''''
''''
'Private Sub SetDummyValues(ByVal vobjSheet As Excel.Worksheet, ByVal vintCountOfRows As Long, ByVal vintCountOfColumns As Long)
'
'    Dim varValues() As Variant, intRowIdx As Long, intColumnIdx As Long
'
'
'    ReDim varValues(1 To vintCountOfRows, 1 To vintCountOfColumns)
'
'    For intRowIdx = 1 To UBound(varValues, 1)
'
'        For intColumnIdx = 1 To UBound(varValues, 2)
'
'            varValues(intRowIdx, intColumnIdx) = "(" & CStr(intRowIdx) & ", " & CStr(intColumnIdx) & ")"
'        Next
'    Next
'
'    vobjSheet.Range("A1").Resize(vintCountOfRows, vintCountOfColumns).Value = varValues
'End Sub
'''--VBA_Code_File--<ReadFromShapeOnXlSheet.bas>--
'Attribute VB_Name = "ReadFromShapeOnXlSheet"
''
''   Get key-value dictionary from auto-shape on Excel sheet
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on Excel
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Sat, 22/Apr/2023    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Control VBE windows
''///////////////////////////////////////////////
''**---------------------------------------------
''** Open relative VBE code-panes
''**---------------------------------------------
'Private Sub OpenVBECodePaneRelativeToReadFromShapeOnXlSheet()
'
'    ' Focusing on this procdure, please press [F5] key to start this. Therefore this scope is private.
'
'    On Error Resume Next ' If the 'ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma' isn't included in the VBA project, the following causes no syntax error.
'
'    Application.Run "ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma", "UTfReadFromShapeOnXlSheet,WinINIGeneral,WinINIGeneralForXl,UTfWinINIGeneral"
'End Sub
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''**---------------------------------------------
''** get some type Dictionary object
''**---------------------------------------------
''''
''''
''''
'''' <Argument>vobjSheet: Input</Argument>
'''' <Argument>vstrKeyValueDelimiter</Argument>
'''' <Argument>vstrFilterShapeName</Argument>
'''' <Argument>vblnAllowToAvoidDuplicatedKey</Argument>
'''' <Return>Dictionary(Of Key, Value)</Return>
'Public Function GetKeyValueDicFromShapes(ByVal vobjSheet As Excel.Worksheet, _
'        Optional ByVal vstrKeyValueDelimiter As String = ": ", _
'        Optional ByVal vstrFilterShapeName As String = "", _
'        Optional ByVal vblnAllowToAvoidDuplicatedKey As Boolean = True) As Scripting.Dictionary
'
'
'    Dim objDic As Scripting.Dictionary, objShape As Excel.Shape
'    Dim blnContinue As Boolean, blnNeedToFilter As Boolean
'
'
'    Set objDic = New Scripting.Dictionary
'
'    blnNeedToFilter = False
'
'    If vstrFilterShapeName <> "" Then
'
'        blnNeedToFilter = True
'    End If
'
'    With vobjSheet
'
'        For Each objShape In .Shapes
'
'            blnContinue = True
'
'            If blnNeedToFilter Then
'
'                If InStr(1, objShape.Name, vstrFilterShapeName) = 0 Then
'
'                    blnContinue = False
'                End If
'            End If
'
'            If blnContinue Then
'
'                msubAddKeyValueToDicInAShapeText objDic, objShape, vstrKeyValueDelimiter, vblnAllowToAvoidDuplicatedKey
'            End If
'        Next
'    End With
'
'    Set GetKeyValueDicFromShapes = objDic
'End Function
'
'
''''
''''
''''
'''' <Argument>vobjSheet: Input</Argument>
'''' <Argument>vstrKeyValueDelimiter</Argument>
'''' <Argument>vstrFilterShapeName</Argument>
'''' <Argument>vblnAllowToAvoidDuplicatedKey</Argument>
'''' <Return>Dictionary(Of ShapeNameKey, Dictionary(Of Key, Value))</Return>
'Public Function GetShapeNameToKeyValueDicFromShapes(ByVal vobjSheet As Excel.Worksheet, _
'        Optional ByVal vstrKeyValueDelimiter As String = ": ", _
'        Optional ByVal vstrFilterShapeName As String = "", _
'        Optional ByVal vblnAllowToAvoidDuplicatedKey As Boolean = True) As Scripting.Dictionary
'
'
'    Dim objShapeNameToKeyValueDic As Scripting.Dictionary, objDic As Scripting.Dictionary, objShape As Excel.Shape
'    Dim blnContinue As Boolean, blnNeedToFilter As Boolean
'
'
'    blnNeedToFilter = False
'
'    If vstrFilterShapeName <> "" Then
'
'        blnNeedToFilter = True
'    End If
'
'
'    Set objShapeNameToKeyValueDic = New Scripting.Dictionary
'
'    With vobjSheet
'
'        For Each objShape In .Shapes
'
'            With objShape
'
'                blnContinue = True
'
'                If blnNeedToFilter Then
'
'                    If InStr(1, .Name, vstrFilterShapeName) = 0 Then
'
'                        blnContinue = False
'                    End If
'                End If
'
'                If blnContinue Then
'
'                    Set objDic = New Scripting.Dictionary
'
'                    msubAddKeyValueToDicInAShapeText objDic, objShape, vstrKeyValueDelimiter, vblnAllowToAvoidDuplicatedKey
'
'                    If objDic.Count > 0 Then
'
'                        If Not objShapeNameToKeyValueDic.Exists(.Name) Then
'
'                            objShapeNameToKeyValueDic.Add .Name, objDic
'                        End If
'                    End If
'                End If
'            End With
'        Next
'    End With
'
'
'    Set GetShapeNameToKeyValueDicFromShapes = objShapeNameToKeyValueDic
'End Function
'
'
''''
''''
''''
'Public Sub AddKeyValueToDicInFromText(ByRef robjKeyValueDic As Scripting.Dictionary, _
'        ByRef rstrFirstRowLine As String, _
'        ByRef rstrInputText As String, _
'        Optional ByVal vstrKeyValueDelimiter As String = ": ", _
'        Optional ByVal vblnPriorToUseLineFeedChar As Boolean = False, _
'        Optional ByVal vblnAllowToAvoidDuplicatedKey As Boolean = True)
'
'
'    Dim strRowTexts() As String, i As Long, intP1 As Long, strKey As String, strValue As String, blnIsFirstLineStringKeyValue As Boolean, intFirstIndex As Long
'
'    blnIsFirstLineStringKeyValue = False
'
'    msubGetStringLinesArray strRowTexts, rstrInputText, vblnPriorToUseLineFeedChar
'
'    intFirstIndex = LBound(strRowTexts)
'
'    For i = intFirstIndex To UBound(strRowTexts)
'
'        intP1 = InStr(1, strRowTexts(i), vstrKeyValueDelimiter)
'
'        If intP1 > 0 Then
'
'            strKey = Trim(Left(strRowTexts(i), intP1 - 1))
'
'            strValue = Trim(Right(strRowTexts(i), Len(strRowTexts(i)) - intP1))
'
'            If Not IsEmpty(strKey) Then
'
'                If vblnAllowToAvoidDuplicatedKey Then
'
'                    With robjKeyValueDic
'
'                        If Not .Exists(strKey) Then
'
'                            .Add strKey, strValue
'
'                            If i = intFirstIndex Then
'
'                                blnIsFirstLineStringKeyValue = True
'                            End If
'                        End If
'                    End With
'                Else
'                    robjKeyValueDic.Add strKey, strValue
'
'                    If i = intFirstIndex Then
'
'                        blnIsFirstLineStringKeyValue = True
'                    End If
'                End If
'            End If
'        End If
'    Next
'
'    If Not blnIsFirstLineStringKeyValue Then
'
'        rstrFirstRowLine = strRowTexts(LBound(strRowTexts))
'    End If
'End Sub
'
''**---------------------------------------------
''** Output a Dictionary object to a Sheet
''**---------------------------------------------
''''
'''' output Dictionary(Of Key, Value) to Excel book
''''
'Public Function OutputSheetShapesKeyValuesToBook(ByVal vobjKeyValueDic As Scripting.Dictionary, _
'        ByVal vstrOutputBookPath As String) As Excel.Worksheet
'
'    Dim objSheet As Excel.Worksheet
'
'    Set objSheet = GetWorkbookAndAnWorksheetPrepareForUnitTest(vstrOutputBookPath, "AutoShapeKeyValue")
'
'    OutputDicToSheetRestrictedByDataTableSheetFormatter objSheet, vobjKeyValueDic, GetKeyValueDataTableSheetFormatter()
'
'    Set OutputSheetShapesKeyValuesToBook = objSheet
'End Function
'
''''
'''' output Dictionary(Of FirstKey, Dictionary(Of Key, Value)) to Excel book
''''
'Public Function OutputSheetShapeNameToKeyValuesDicToBook(ByVal vobjShapeNameToKeyValueDic As Scripting.Dictionary, _
'        ByVal vstrOutputBookPath As String) As Excel.Worksheet
'
'    Dim objSheet As Excel.Worksheet
'
'    Set objSheet = GetWorkbookAndAnWorksheetPrepareForUnitTest(vstrOutputBookPath, "AutoShapeNameAndKeyValue")
'
'    OutputOneNestedDicToSheetRestrictedByDataTableSheetFormatter objSheet, vobjShapeNameToKeyValueDic, GetOneNestedKeyValueDicDataTableSheetFormatter("ShapeName,20,Key,15,Value,15") ' GetOneNestedKeyValueDicDataTableSheetFormatter("ShapeName", 20)
'
'    Set OutputSheetShapeNameToKeyValuesDicToBook = objSheet
'End Function
'
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
''''
''''
''''
'''' <Argument>robjKeyValueDic: Output</Argument>
'''' <Argument>robjShape: Input</Argument>
'''' <Argument>vblnAllowToAvoidDuplicatedKey: Input</Argument>
'Private Sub msubAddKeyValueToDicInAShapeText(ByRef robjKeyValueDic As Scripting.Dictionary, _
'        ByRef robjShape As Excel.Shape, _
'        Optional ByVal vstrKeyValueDelimiter As String = ": ", _
'        Optional ByVal vblnAllowToAvoidDuplicatedKey As Boolean = True)
'
'
'    Dim strRowTexts() As String, i As Long, intP1 As Long, strKey As String, strValue As String, strFirstRowLine As String
'
'    With robjShape.TextFrame2.TextRange.Characters
'
'        If Not IsEmpty(.Text) Then
'
'            Debug.Print "Auto shape name: " & robjShape.Name
'
'            If InStr(1, .Text, vstrKeyValueDelimiter) > 0 Then
'
'                AddKeyValueToDicInFromText robjKeyValueDic, strFirstRowLine, .Text, vstrKeyValueDelimiter, True, vblnAllowToAvoidDuplicatedKey
'            End If
'        End If
'    End With
'End Sub
'
'
'
''''
''''
''''
'Private Sub msubGetStringLinesArray(ByRef rstrLines() As String, _
'        ByRef rstrInputString As String, _
'        ByRef rblnPriorToUseLineFeedChar As Boolean)
'
'
'    If rblnPriorToUseLineFeedChar Then
'
'        rstrLines = Split(rstrInputString, vbLf)
'    Else
'        If InStr(1, rstrInputString, vbNewLine) > 0 Then
'
'            rstrLines = Split(rstrInputString, vbNewLine)
'
'        ElseIf InStr(1, rstrInputString, vbLf) > 0 Then
'
'            rstrLines = Split(rstrInputString, vbLf)
'        Else
'            ReDim rstrLines(0)
'
'            rstrLines(0) = rstrInputString
'        End If
'    End If
'End Sub
'
'
''///////////////////////////////////////////////
''/// Sanity tests
''///////////////////////////////////////////////
''''
''''
''''
'Private Sub msubSanityTestToReadDicAfterCreateExcelSheet()
'
'    Dim strLog As String, objSheet As Excel.Worksheet
'    Dim objDic As Scripting.Dictionary
'
'    strLog = "Test auto-shape with texts" & vbNewLine & "Time: 2.0 [s]" & vbNewLine & "Search keywords:AI,機械学習"
'
'    Set objSheet = mfobjGetTestingNewSheet(strLog)
'
'
'    AddAutoShapeGeneralMessage objSheet, strLog
'
'    Set objDic = GetKeyValueDicFromShapes(objSheet)
'
'    DebugDic objDic
'End Sub
'
''''
''''
''''
'Private Function mfobjGetTestingNewSheet(ByVal vstrLog As String) As Excel.Worksheet
'
'    Dim strBookPath As String, objBook As Excel.Workbook, objSheet As Excel.Worksheet
'
'
'    strBookPath = mfstrGetTmporaryBookDirPath() & "SanityTestToGetKeyValueDicFromShapes.xlsx"
'
'    Set objBook = GetWorkbookAndPrepareForUnitTest(strBookPath)
'
'    Set objSheet = GetNewWorksheetAfterAllExistedSheetsFromBook(objBook)
'
'    objSheet.Name = "ForAutoShapes"
'
'
'    DeleteDummySheetWhenItExists objBook
'
'    Set mfobjGetTestingNewSheet = objSheet
'End Function
'
'
''''
''''
''''
'Private Function mfstrGetTmporaryBookDirPath() As String
'
'    Dim strDir As String
'
'    strDir = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks"
'
'    ForceToCreateDirectory strDir
'
'    mfstrGetTmporaryBookDirPath = strDir
'End Function
'
'
'
'
'
'
'''--VBA_Code_File--<UTfReadFromShapeOnXlSheet.bas>--
'Attribute VB_Name = "UTfReadFromShapeOnXlSheet"
''
''   Sanity tests for getting key-value dictionary from auto-shape on Excel sheet
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on Excel
''       Dependent on SqlUTfCsvAdoSheetExpander.bas, SqlUTfXlSheetExpander.bas, WinRegViewForXl
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Sat, 22/Apr/2023    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
'
'
''///////////////////////////////////////////////
''/// Sanity tests
''///////////////////////////////////////////////
''**---------------------------------------------
''** Sanity test to output results to Excel book
''**---------------------------------------------
''''
''''
''''
'Private Sub msubSanityTestToOutputToExcelBookAfterGetShapeNameToKeyValueDicByReadingFromLoadedWinRegSubKeysSheet()
'
'    Dim objDic As Scripting.Dictionary, strBookPath As String, strSubRegKey As String
'
'    strSubRegKey = "HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion"
'
'    Set objDic = mfobjGetDicFromReadingFromLoadedWinRegSubKeysBook(strSubRegKey, True)
'
'    strBookPath = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\SanityTestReadAllShapesKeyValuesOfSheet.xlsx"
'
'    OutputSheetShapeNameToKeyValuesDicToBook objDic, strBookPath
'End Sub
'
'
''''
'''' get Dictionary(Of String[ShapeName], Dictionary(Of String[Key], String[Value]))
''''
'Private Sub msubSanityTestToOutputToExcelBookAfterGetShapeNameToKeyValueDicByReadingFromCsvAdoExpanderAutoShapes()
'
'    Dim objDic As Scripting.Dictionary, strBookPath As String
'
'    Set objDic = mfobjGetShapeNameToKeyValueDicFromReadingFromCsvAdoExpanderAutoShapes()
'
'    strBookPath = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\SanityTestReadAllShapesKeyValuesOfSheet.xlsx"
'
'    OutputSheetShapeNameToKeyValuesDicToBook objDic, strBookPath
'End Sub
'
''''
'''' get Dictionary(Of String[Key], String[Value]) about CsvAdoSheetExpander
''''
'Private Sub msubSanityTestToOutputToExcelBookAfterGetKeyValueDicByReadingFromCsvAdoExpanderAutoShapes()
'
'    Dim objDic As Scripting.Dictionary, strBookPath As String
'
'    Set objDic = mfobjGetKeyValueDicFromReadingFromCsvAdoExpanderAutoShapes()
'
'    strBookPath = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\SanityTestReadAllShapesKeyValuesOfSheet.xlsx"
'
'    OutputSheetShapesKeyValuesToBook objDic, strBookPath
'End Sub
'
'
''**---------------------------------------------
''** get Dictionary test
''**---------------------------------------------
''''
'''' get Dictionary(Of String[ShapeName], Dictionary(Of String[Key], String[Value]))
''''
'Private Sub msubSanityTestToGetShapeNameToKeyValueDicByReadingFromCsvAdoExpanderAutoShapes()
'
'    DebugDic mfobjGetShapeNameToKeyValueDicFromReadingFromCsvAdoExpanderAutoShapes()
'End Sub
'
'
''''
'''' get Dictionary(Of String[Key], String[Value]) about CsvAdoSheetExpander
''''
'Private Sub msubSanityTestToGetKeyValueDicByReadingFromCsvAdoExpanderAutoShapes()
'
'    Dim objDic As Scripting.Dictionary
'
'    Set objDic = mfobjGetKeyValueDicFromReadingFromCsvAdoExpanderAutoShapes()
'
'    DebugDic objDic
'End Sub
'
''''
''''
''''
'Private Sub msubSanityTestToGetShapeNameToKeyValueDicByReadingFromExcelAdoExpanderAutoShapes()
'
'    DebugDic mfobjGetShapeNameToKeyValueDicFromReadingFromCsvAdoExpanderAutoShapes()
'End Sub
'
''''
'''' About XlAdoSheetExpander
''''
'Private Sub msubSanityTestToGetKeyValueDicByReadingFromExcelAdoExpanderAutoShapes()
'
'    DebugDic mfobjGetKeyValueDicFromReadingFromExcelAdoExpanderAutoShapes()
'End Sub
'
'
''''
'''' get Dictionary(Of ShapeName, Dictionary(Of Key, Value)) about loaded INI file
''''
'Private Sub msubSanityTestToGetShapeNameToKeyValueDicByReadingFromLoadedSampleIniDataBook()
'
'    DebugDic mfobjGetShapeNameToKeyValueDicFromReadingFromLoadedSampleIniDataBook()
'End Sub
'
'
'
''''
'''' get Dictionary(Of Key, Value) about loaded INI file
''''
'Private Sub msubSanityTestToGetKeyValueDicByReadingFromLoadedSampleIniDataBook()
'
'    DebugDic mfobjGetKeyValueDicFromReadingFromLoadedSampleIniDataBook()
'End Sub
'
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
''**---------------------------------------------
''** Tools for getting Dictinary objects
''**---------------------------------------------
''''
''''
''''
'Private Function mfobjGetShapeNameToKeyValueDicFromReadingFromCsvAdoExpanderAutoShapes() As Scripting.Dictionary
'
'    Set mfobjGetShapeNameToKeyValueDicFromReadingFromCsvAdoExpanderAutoShapes = mfobjGetDicFromReadingFromCsvAdoExpanderAutoShapes(True)
'End Function
'
''''
''''
''''
'Private Function mfobjGetKeyValueDicFromReadingFromCsvAdoExpanderAutoShapes() As Scripting.Dictionary
'
'    Set mfobjGetKeyValueDicFromReadingFromCsvAdoExpanderAutoShapes = mfobjGetDicFromReadingFromCsvAdoExpanderAutoShapes(False)
'End Function
'
''''
''''
''''
'Private Function mfobjGetDicFromReadingFromCsvAdoExpanderAutoShapes(Optional ByVal vblnGetShapeNameToKeyValueDic As Boolean = True) As Scripting.Dictionary
'
'    Dim strBookPath As String, objSheet As Excel.Worksheet
'    Dim objDic As Scripting.Dictionary
'
'    strBookPath = GetOutputBookPathOfConnectingAdoCSVTestAfterCSVCreate(BasicSampleDT, 100, False)
'
'    With GetWorkbook(strBookPath)
'
'        Set objSheet = .Worksheets.Item(1)
'
'        If vblnGetShapeNameToKeyValueDic Then
'
'            Set objDic = GetShapeNameToKeyValueDicFromShapes(objSheet)
'        Else
'            Set objDic = GetKeyValueDicFromShapes(objSheet)
'        End If
'    End With
'
'    Set mfobjGetDicFromReadingFromCsvAdoExpanderAutoShapes = objDic
'End Function
'
''''
''''
''''
'Private Function mfobjGetShapeNameToKeyValueDicFromReadingFromExcelAdoExpanderAutoShapes() As Scripting.Dictionary
'
'    Set mfobjGetKeyValueDicFromReadingFromExcelAdoExpanderAutoShapes = mfobjGetDicFromReadingFromExcelAdoExpanderAutoShapes(True)
'End Function
'
''''
''''
''''
'Private Function mfobjGetKeyValueDicFromReadingFromExcelAdoExpanderAutoShapes() As Scripting.Dictionary
'
'    Set mfobjGetKeyValueDicFromReadingFromExcelAdoExpanderAutoShapes = mfobjGetDicFromReadingFromExcelAdoExpanderAutoShapes(False)
'End Function
'
'
''''
''''
''''
'Private Function mfobjGetDicFromReadingFromExcelAdoExpanderAutoShapes(Optional ByVal vblnGetShapeNameToKeyValueDic As Boolean = True) As Scripting.Dictionary
'
'    Dim strBookPath As String, objSheet As Excel.Worksheet
'    Dim objDic As Scripting.Dictionary
'
'    strBookPath = GetOutputExcelBookPathOfConnectingBookTestAfterCreateBook(BasicSampleDT, 100, False)
'
'    With GetWorkbook(strBookPath)
'
'        Set objSheet = .Worksheets.Item(1)
'
'        If vblnGetShapeNameToKeyValueDic Then
'
'            Set objDic = GetShapeNameToKeyValueDicFromShapes(objSheet)
'        Else
'            Set objDic = GetKeyValueDicFromShapes(objSheet)
'        End If
'    End With
'
'    Set mfobjGetKeyValueDicFromReadingFromExcelAdoExpanderAutoShapes = objDic
'End Function
'
'
''''
'''' get Dictionary(Of ShapeName, Dictionary(Of Key, Value)) about sample .ini file
''''
'Private Function mfobjGetShapeNameToKeyValueDicFromReadingFromLoadedSampleIniDataBook() As Scripting.Dictionary
'
'    Set mfobjGetShapeNameToKeyValueDicFromReadingFromLoadedSampleIniDataBook = mfobjGetDicFromReadingFromLoadedSampleIniDataBook(True)
'End Function
'
'
''''
'''' get Dictionary(Of Key, Value) about sample .ini file
''''
'Private Function mfobjGetKeyValueDicFromReadingFromLoadedSampleIniDataBook() As Scripting.Dictionary
'
'    Set mfobjGetKeyValueDicFromReadingFromLoadedSampleIniDataBook = mfobjGetDicFromReadingFromLoadedSampleIniDataBook(False)
'End Function
'
'
''''
'''' About sample .ini file
''''
'Private Function mfobjGetDicFromReadingFromLoadedSampleIniDataBook(Optional ByVal vblnGetShapeNameToKeyValueDic As Boolean = True) As Scripting.Dictionary
'
'    Dim strBookPath As String, objSheet As Excel.Worksheet
'    Dim objDic As Scripting.Dictionary
'
'    strBookPath = GetOutputExcelBookPathAfterCreateBookOfLoadedIniAllData()
'
'    With GetWorkbook(strBookPath)
'
'        Set objSheet = .Worksheets.Item(1)
'
'        If vblnGetShapeNameToKeyValueDic Then
'
'            Set objDic = GetShapeNameToKeyValueDicFromShapes(objSheet)
'        Else
'            Set objDic = GetKeyValueDicFromShapes(objSheet)
'        End If
'    End With
'
'    Set mfobjGetDicFromReadingFromLoadedSampleIniDataBook = objDic
'End Function
'
'
''''
'''' About loaded Windows registry outputted sheet
''''
'Private Function mfobjGetDicFromReadingFromLoadedWinRegSubKeysBook(ByVal vstrFullSubKey As String, Optional ByVal vblnGetShapeNameToKeyValueDic As Boolean = True) As Scripting.Dictionary
'
'    Dim strBookPath As String, objSheet As Excel.Worksheet
'    Dim objDic As Scripting.Dictionary
'
'    strBookPath = GetWinRegistoriesInfomationBookPath()
'
'    OutputWinRegSubKeysToSheet vstrFullSubKey
'
'    With GetWorkbook(strBookPath)
'
'        Set objSheet = .Worksheets.Item(1)
'
'        If vblnGetShapeNameToKeyValueDic Then
'
'            Set objDic = GetShapeNameToKeyValueDicFromShapes(objSheet)
'        Else
'            Set objDic = GetKeyValueDicFromShapes(objSheet)
'        End If
'    End With
'
'    Set mfobjGetDicFromReadingFromLoadedWinRegSubKeysBook = objDic
'End Function
'''--VBA_Code_File--<WinRegViewForXl.bas>--
'Attribute VB_Name = "WinRegViewForXl"
''
''   Output the read Windows 32 bit registries data-table, such as Collection or Scripting.Dictionary ,to Excel-sheet.
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on both Windows OS and Excel
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Tue, 31/May/2022    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Constants
''///////////////////////////////////////////////
''**---------------------------------------------
''** Solute file-system path by the Windows registry
''**---------------------------------------------
'Private Const mstrModuleKey As String = "WinRegViewForXl"   ' 1st part of registry sub-key
'
'Private Const mstrSubjectNetworkDriveRootDirKey As String = "UTfNetworkDriveRootDir"   ' 2nd part of registry sub-key, recommend network drive
'
'Private Const mstrSubjectWinRegReadOutputBookPathKey As String = "WinRegReadOutputBookPath"
'
'
''///////////////////////////////////////////////
''/// Control VBE windows
''///////////////////////////////////////////////
''**---------------------------------------------
''** Open relative VBE code-panes
''**---------------------------------------------
'Private Sub OpenVBECodePaneRelativeToWinRegViewForXl()
'
'    ' Focusing on this procdure, please press [F5] key to start this. Therefore this scope is private.
'
'    On Error Resume Next
'
'    ' If the 'ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma' isn't included in the VBA project, the following causes no syntax error
'
'    Application.Run "ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma", "WinRegView,WinRegManagementDeclarations,UTfWinAPIRegEnumForXl,UTfWinAPIRegistryRW"
'End Sub
'
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''**---------------------------------------------
''** About read Windows-registory key-value Dictionary
''**---------------------------------------------
''''
''''
''''
'Public Function OutputWinRegValuesOneNestedDicInSpecifiedWinRegistryKeyToBook(ByVal vstrFullRegKey As String, _
'        ByVal vobjReadOneNestedDic As Scripting.Dictionary, _
'        ByVal vstrOutputBookPath As String, _
'        Optional ByVal vstrFieldTitleToColumnWidthDelimitedByComma As String = "LastChildSubKeyName,17,Key,15,Value,25") As Excel.Worksheet
'
'
'    Dim objSheet As Excel.Worksheet
'
'    Set objSheet = GetWorkbookAndAnWorksheetPrepareForUnitTest(vstrOutputBookPath, "WinReg_" & Left(GetLastChildRegKeyName(vstrFullRegKey), 24))
'
'    OutputOneNestedDicToSheetRestrictedByDataTableSheetFormatter objSheet, vobjReadOneNestedDic, mfobjGetDataTableSheetFormatterForOneNestedWinRegKeyValueDic(vstrFieldTitleToColumnWidthDelimitedByComma)
'
'    AddAutoShapeGeneralMessage objSheet, mfstrGetLogOfWinRegOneNestedKeysAndValues(objSheet, vstrFullRegKey)
'
'    Set OutputWinRegValuesOneNestedDicInSpecifiedWinRegistryKeyToBook = objSheet
'End Function
'
''''
''''
''''
'Public Function OutputWinRegValuesDicInSpecifiedWinRegistryKeyToBook(ByVal vstrFullRegKey As String, _
'        ByVal vobjReadDic As Scripting.Dictionary, _
'        ByVal vstrOutputBookPath As String) As Excel.Worksheet
'
'
'    Dim objSheet As Excel.Worksheet
'
'    Set objSheet = GetWorkbookAndAnWorksheetPrepareForUnitTest(vstrOutputBookPath, "WinReg_" & Left(GetLastChildRegKeyName(vstrFullRegKey), 24))
'
'    OutputDicToSheetRestrictedByDataTableSheetFormatter objSheet, vobjReadDic, mfobjGetDataTableSheetFormatterForWinRegKeyValuesDicInAKey()
'
'    AddAutoShapeGeneralMessage objSheet, mfstrGetLogOfWinRegKeyValue(vstrFullRegKey, vobjReadDic)
'
'    Set OutputWinRegValuesDicInSpecifiedWinRegistryKeyToBook = objSheet
'End Function
'
'
''''
''''
''''
'Private Function mfstrGetLogOfWinRegOneNestedKeysAndValues(ByRef robjSheet As Excel.Worksheet, _
'        ByRef rstrFullRegKey As String) As String
'
'
'    Dim intRowsCount As Long, intColumnsCount As Long, strLoggedTimeStamp As String, strLog As String
'
'    GetCountsOfRowsColumnsFromSheet robjSheet, intRowsCount, intColumnsCount, strLoggedTimeStamp
'
'    strLog = "An one-nested Dictionary by reading Windows-registry sub-keys and values" & vbNewLine
'
'    strLog = strLog & "Full Windows-registry key: " & rstrFullRegKey & vbNewLine
'
'    strLog = strLog & "Count of key-values: " & CStr(intRowsCount) & vbNewLine
'
'    strLog = strLog & "Read Windows-registry time: " & strLoggedTimeStamp
'
'    mfstrGetLogOfWinRegOneNestedKeysAndValues = strLog
'End Function
'
''''
''''
''''
'Private Function mfobjGetDataTableSheetFormatterForOneNestedWinRegKeyValueDic(Optional ByVal vstrFieldTitleToColumnWidthDelimitedByComma As String = "LastChildSubKeyName,17,Key,15,Value,25") As DataTableSheetFormatter
'
'    Dim objDTSheetFormatter As DataTableSheetFormatter
'
'    Set objDTSheetFormatter = GetOneNestedKeyValueDicDataTableSheetFormatter(vstrFieldTitleToColumnWidthDelimitedByComma)
'
'    With objDTSheetFormatter
'
'        .FieldTitleInteriorType = ColumnNameInteriorOfLoadedWinRegistryAsTable
'
'        .RecordBordersType = RecordCellsGrayAll
'    End With
'
'    Set mfobjGetDataTableSheetFormatterForOneNestedWinRegKeyValueDic = objDTSheetFormatter
'End Function
'
'
''''
''''
''''
'Private Function mfstrGetLogOfWinRegKeyValue(ByRef rstrFullRegKey As String, ByRef robjDic As Scripting.Dictionary) As String
'
'    Dim strLog As String
'
'    strLog = "A dictionary by reading a specified Windows-registry key" & vbNewLine
'
'    strLog = strLog & "Full Windows-registry key: " & rstrFullRegKey & vbNewLine
'
'    strLog = strLog & "Count of key-values: " & CStr(robjDic.Count) & vbNewLine
'
'    strLog = strLog & "Read Windows-registry time: " & Format(Now(), "ddd, yyyy/m/d hh:mm:ss")
'
'    mfstrGetLogOfWinRegKeyValue = strLog
'End Function
'
''''
''''
''''
'Private Function mfobjGetDataTableSheetFormatterForWinRegKeyValuesDicInAKey() As DataTableSheetFormatter
'
'    Dim objDTSheetFormatter As DataTableSheetFormatter
'
'    Set objDTSheetFormatter = GetKeyValueDataTableSheetFormatter
'
'    With objDTSheetFormatter
'
'        .FieldTitleInteriorType = ColumnNameInteriorOfLoadedWinRegistryAsTable
'
'        .RecordBordersType = RecordCellsGrayAll
'    End With
'
'    Set mfobjGetDataTableSheetFormatterForWinRegKeyValuesDicInAKey = objDTSheetFormatter
'End Function
'
'
'
''**---------------------------------------------
''** Output results to Excel book
''**---------------------------------------------
''''
'''' using GetWinRegistoriesInfomationBookPath
''''
'Public Sub OutputRegValuesWithChildSubKeysToSheet(ByVal vstrFullSubKey As String, _
'        Optional ByVal venmRegCollectTargetFlag As RegCollectTargetFlag = (RegCollectTargetFlag.CollectWinRegSubKeys Or RegCollectTargetFlag.CollectWinRegValues), _
'        Optional ByVal vblnNoDeletePreviousSheets As Boolean = False, _
'        Optional ByVal vstrSubKeysSheetName As String = "SubKeys", _
'        Optional ByVal vstrRegValuesSheetName As String = "RegValues", _
'        Optional ByVal vstrAdditionalLogMessage As String = "")
'
'    Dim strOutputBookPath As String
'
'    strOutputBookPath = GetWinRegistoriesInfomationBookPath()
'
'    GetRegValuesWithChildSubKeysAndOutExcelSheet vstrFullSubKey, strOutputBookPath, venmRegCollectTargetFlag, vblnNoDeletePreviousSheets, vstrSubKeysSheetName, vstrRegValuesSheetName, vstrAdditionalLogMessage
'End Sub
'
'
''''
'''' list sub-keys up on Excel sheet
''''
'Public Sub OutputWinRegSubKeysToSheet(ByVal vstrFullSubKey As String, _
'        Optional ByVal vblnNoDeletePreviousSheets As Boolean = False, _
'        Optional ByVal vstrSubKeysShName As String = "SubKeys", _
'        Optional ByVal vstrAddLogMessage As String = "")
'
'    ' only subkeys
'    OutputRegValuesWithChildSubKeysToSheet vstrFullSubKey, CollectWinRegSubKeys, vblnNoDeletePreviousSheets, vstrSubKeysSheetName:=vstrSubKeysShName, vstrAdditionalLogMessage:=vstrAddLogMessage
'
'End Sub
'
''''
'''' list registry values up on Excel sheet
''''
'Public Sub OutputWinRegValuesToSheet(ByVal vstrFullSubKey As String, _
'        Optional ByVal vblnNoDeletePreviousSheets As Boolean = False, _
'        Optional ByVal vstrRegValuesShName As String = "RegValues", _
'        Optional ByVal vstrAddLogMessage As String = "")
'
'    ' only registry values
'    OutputRegValuesWithChildSubKeysToSheet vstrFullSubKey, CollectWinRegValues, vblnNoDeletePreviousSheets, vstrRegValuesSheetName:=vstrRegValuesShName, vstrAdditionalLogMessage:=vstrAddLogMessage
'End Sub
'
'
''**---------------------------------------------
''** Output Excel sheet by loading Windows Regstries
''**---------------------------------------------
''''
'''' get registry values and sub-keys and output Excel specified test sheet
''''
'Public Sub GetRegValuesWithChildSubKeysAndOutExcelSheet(ByVal vstrFullSubKey As String, _
'        ByVal vstrOutputBookPath As String, _
'        Optional venmRegCollectTargetFlag As RegCollectTargetFlag = (RegCollectTargetFlag.CollectWinRegSubKeys Or RegCollectTargetFlag.CollectWinRegValues), _
'        Optional vblnNoDeletePreviousSheets As Boolean = False, _
'        Optional vstrSubKeysSheetName As String = "SubKeys", _
'        Optional vstrRegValuesSheetName As String = "RegValues", _
'        Optional vstrAdditionalLogMessage As String = "")
'
'    Dim intRootKey As Long, strSubKey As String, objSubKeys As Collection, objRegValues As Collection
'    Dim objRowItems As Collection, intHierarchicalOrder As Long, objBook As Excel.Workbook
'    Dim objSheet As Excel.Worksheet
'
'    Dim objDoubleStopWatch As DoubleStopWatch
'
'
'    If vblnNoDeletePreviousSheets Then
'
'        Set objBook = GetWorkbook(vstrOutputBookPath, False)
'    Else
'        Set objBook = GetWorkbookAndPrepareForUnitTest(vstrOutputBookPath)
'    End If
'
'    SeparateRegistryRootKeyAndSubKey vstrFullSubKey, intRootKey, strSubKey
'
'    intHierarchicalOrder = UBound(Split(strSubKey, "\")) + 1
'
'    If (venmRegCollectTargetFlag And CollectWinRegSubKeys) > 0 Then
'        ' set first subkey item
'        Set objSubKeys = New Collection
'
'        If IsWinFullRegKeyExisted(vstrFullSubKey) Then
'
'            Set objRowItems = New Collection
'
'            With objRowItems
'
'                .Add intHierarchicalOrder
'
'                .Add vstrFullSubKey
'            End With
'
'            objSubKeys.Add objRowItems
'        Else
'            Debug.Print "Not exist, searching registry key base: " & vstrFullSubKey
'        End If
'    End If
'
'    If (venmRegCollectTargetFlag And CollectWinRegValues) > 0 Then
'
'        Set objRegValues = New Collection
'    End If
'
'    Set objDoubleStopWatch = New DoubleStopWatch
'
'    With objDoubleStopWatch
'
'        .MeasureStart
'
'        GetRegValuesWithChildSubKeys intHierarchicalOrder + 1, intRootKey, strSubKey, objSubKeys, objRegValues, True, venmRegCollectTargetFlag
'
'        .MeasureInterval
'    End With
'
'    If (venmRegCollectTargetFlag And CollectWinRegSubKeys) > 0 Then
'
'        Set objSheet = mfobjExtractSubKeysColToSheet(objSubKeys, GetColFromLineDelimitedChar("HierarchicalOrder,KeyFullPath"), objBook, vstrSubKeysSheetName)
'
'        msubAddRegistrySearchLogOfSubKeys objSheet, objDoubleStopWatch.ElapsedSecondTime
'    End If
'
'    If (venmRegCollectTargetFlag And CollectWinRegValues) > 0 Then
'
'        Set objSheet = mfobjExtractRegValuesColToSheet(objRegValues, GetColFromLineDelimitedChar("HierarchicalOrder,RegName,RegValue,RegType,KeyFullPath"), objBook, vstrRegValuesSheetName)
'
'        msubAddRegistrySearchLogOfRegValues objSheet, objDoubleStopWatch.ElapsedSecondTime
'    End If
'
'    If vstrAdditionalLogMessage <> "" And Not objSheet Is Nothing Then
'
'        AddAutoShapeGeneralMessage objSheet, vstrAdditionalLogMessage
'    End If
'
'    DeleteDummySheetWhenItExists objBook
'End Sub
'
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
''''
''''
''''
'''' <Return>Worksheet</Return>
'Private Function mfobjExtractSubKeysColToSheet(ByVal vobjDTCol As Collection, ByVal vobjFieldTitlesCol As Collection, ByVal vobjLogBook As Workbook, ByVal vstrSheetName As String) As Worksheet
'
'    Dim objSheet As Excel.Worksheet
'    Dim objDataTableSheetFormatter As DataTableSheetFormatter
'
'
'    Set objDataTableSheetFormatter = New DataTableSheetFormatter
'
'    With objDataTableSheetFormatter
'
'        .FieldTitleInteriorType = ColumnNameInteriorOfLoadedWinRegistryAsTable
'
'        .RecordBordersType = RecordCellsGrayAll
'
'        Set .FieldTitleToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("HierarchicalOrder,6,KeyFullPath,105")
'    End With
'
'    Set objSheet = GetNewWorksheetAfterAllExistedSheetsFromBook(vobjLogBook)
'
'    objSheet.Name = vstrSheetName
'
'    OutputColToSheet objSheet, vobjDTCol, vobjFieldTitlesCol, objDataTableSheetFormatter
'
'    Set mfobjExtractSubKeysColToSheet = objSheet
'End Function
'
'
'Private Sub msubAddRegistrySearchLogOfSubKeys(ByVal vobjSheet As Worksheet, ByVal vdblElapsedTime As Double)
'
'    Dim intRowsCount As Long, intColumnsCount As Long, strLoggedTimeStamp As String
'    Dim strSearchLog As String, objDecorationSettings As Collection
'
'
'    GetCountsOfRowsColumnsFromSheet vobjSheet, intRowsCount, intColumnsCount, strLoggedTimeStamp
'
'
'    strSearchLog = "Sub-keys searching of Windows Registry" & vbNewLine
'
'    strSearchLog = strSearchLog & "Searched time: " & strLoggedTimeStamp & vbNewLine
'
'    strSearchLog = strSearchLog & "Seaching elapsed time: " & Format(CDbl(vdblElapsedTime), "0.000") & " [s]" & vbNewLine
'
'    strSearchLog = strSearchLog & "Rows Count: " & intRowsCount & vbNewLine
'
'    strSearchLog = strSearchLog & "Columns Count: " & intColumnsCount
'
'    Set objDecorationSettings = mfobjGetDecorationSettingsForRegistrySearchResults()
'
'    AddAutoShapeGeneralMessage vobjSheet, strSearchLog, AutoShapeMessageFillPatternType.XlShapeTextLogInteriorFillOfWinRegistry, vobjDecorationTagValueSettings:=objDecorationSettings
'End Sub
'
'
''''
''''
''''
'''' <Return>Excel.Worksheet</Return>
'Private Function mfobjExtractRegValuesColToSheet(ByVal vobjDTCol As Collection, ByVal vobjFieldTitlesCol As Collection, ByVal vobjLogBook As Excel.Workbook, ByVal vstrSheetName As String) As Excel.Worksheet
'
'    Dim objSheet As Excel.Worksheet
'    Dim objDataTableSheetFormatter As DataTableSheetFormatter
'
'    Set objDataTableSheetFormatter = New DataTableSheetFormatter
'
'    With objDataTableSheetFormatter
'
'        .FieldTitleInteriorType = ColumnNameInteriorOfLoadedWinRegistryAsTable
'
'        .RecordBordersType = RecordCellsGrayAll
'
'        Set .FieldTitleToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("HierarchicalOrder,6,RegName,22,RegValue,82,RegType,12.5,KeyFullPath,105")
'    End With
'
'    Set objSheet = GetNewWorksheetAfterAllExistedSheetsFromBook(vobjLogBook)
'
'    objSheet.Name = vstrSheetName
'
'    OutputColToSheet objSheet, vobjDTCol, vobjFieldTitlesCol, objDataTableSheetFormatter
'
'    Set mfobjExtractRegValuesColToSheet = objSheet
'End Function
'
'Private Sub msubAddRegistrySearchLogOfRegValues(ByVal vobjSheet As Excel.Worksheet, ByVal vdblElapsedTime As Long)
'
'    Dim intRowsCount As Long, intColumnsCount As Long, strLoggedTimeStamp As String
'    Dim strSearchLog As String
'
'
'    GetCountsOfRowsColumnsFromSheet vobjSheet, intRowsCount, intColumnsCount, strLoggedTimeStamp
'
'    strSearchLog = "Windows Registry values searching" & vbNewLine
'
'    strSearchLog = strSearchLog & "Searched time: " & strLoggedTimeStamp & vbNewLine
'
'    strSearchLog = strSearchLog & "Seaching elapsed time: " & Format(CDbl(vdblElapsedTime), "0.000") & " [s]" & vbNewLine
'
'    strSearchLog = strSearchLog & "Rows Count: " & intRowsCount & vbNewLine
'
'    strSearchLog = strSearchLog & "Columns Count: " & intColumnsCount
'
'    AddAutoShapeGeneralMessage vobjSheet, strSearchLog, AutoShapeMessageFillPatternType.XlShapeTextLogInteriorFillOfWinRegistry, vobjDecorationTagValueSettings:=mfobjGetDecorationSettingsForRegistrySearchResults()
'End Sub
'
''''
''''
''''
'Private Function mfobjGetDecorationSettingsForRegistrySearchResults() As Collection
'
'    Dim objDecorationSettings As Collection
'
'    Set objDecorationSettings = New Collection
'
'    With objDecorationSettings
'
'        .Add "Searched time:" & ";" & vbNewLine & ";10;trRGBWhiteFontColor; "
'
'        .Add "Seaching elapsed time:" & ";" & "[s]" & ";14;trRGBPaleBlueFontColor; "
'
'        .Add "Rows Count:" & ";" & vbLf & ";14;trRGBWhiteFontColor; "
'
'        .Add "Columns Count:" & ";" & vbLf & ";14;trRGBWhiteFontColor; "
'    End With
'
'    Set mfobjGetDecorationSettingsForRegistrySearchResults = objDecorationSettings
'End Function
'
''///////////////////////////////////////////////
''/// Operations - a UNC path portability solution
''///////////////////////////////////////////////
''**---------------------------------------------
''** set user-specified path
''**---------------------------------------------
''''
'''' Individual-Set-Registy
''''
'Public Sub ISRInUTfWinAPIRegEnumOfTestOutputBookPath(ByVal vstrTargetPath As String)
'
'    SetUNCPathToCurUserReg mstrModuleKey & "\" & mstrSubjectWinRegReadOutputBookPathKey, vstrTargetPath
'End Sub
'Public Sub IDelRInUTfWinAPIRegEnumOfTestOutputBookPath()
'
'    DeleteUNCPathAllToCurUserReg mstrModuleKey & "\" & mstrSubjectWinRegReadOutputBookPathKey
'End Sub
'
'
''**---------------------------------------------
''** solute path generaly
''**---------------------------------------------
''''
'''' solute directory path from registry CURRENT_USER
''''
'Public Function GetWinRegistoriesInfomationBookPath() As String
'
'    Dim strTargetPath As String, strModuleNameAndSubjectKey As String
'
'    strModuleNameAndSubjectKey = mstrModuleKey & "\" & mstrSubjectWinRegReadOutputBookPathKey
'
'    strTargetPath = GetUNCPathToCurUserReg(strModuleNameAndSubjectKey)
'
'    If strTargetPath = "" Then
'
'        strTargetPath = ManageToGetUNCPathFromCurReg(strModuleNameAndSubjectKey, mfobjGetRegParamOfStorageUNCPathColForWinRegistoriesInfomationBookPath())
'    End If
'
'    GetWinRegistoriesInfomationBookPath = strTargetPath
'End Function
'
''**---------------------------------------------
''** generate RegParamOfStorageUNCPath object
''**---------------------------------------------
''''
'''' get RegParamOfStorageUNCPath object of GetWinRegistoriesInfomationBookPath
''''
'Private Function mfobjGetRegParamOfStorageUNCPathColForWinRegistoriesInfomationBookPath() As Collection
'
'    Dim objCol As Collection, strSubDirectoryPath As String
'
'    Set objCol = Nothing
'
'    Set objCol = GetCollectionFromCallBackInterface("GetPlugInRegParamOfStorageUNCPathColForWinRegistoriesInfomationBookPath")
'
'    If objCol Is Nothing Then   ' If interface cannot be soluted
'
'        With New WshNetwork
'
'            strSubDirectoryPath = "VBAWorks\" & .UserName & "\WinRegistryLoaded"
'        End With
'
'        Set objCol = GetSingleColOfNewRegParamOfStorageUNCPath(GetNewRegParamOfStorageUNCFilePath("ReadRegistriesInfo.xlsx", StoragePathSearchingFlag.NetWorkOrLocalAsFile, strSubDirectoryPath, strSubDirectoryPath))
'    End If
'
'    Set mfobjGetRegParamOfStorageUNCPathColForWinRegistoriesInfomationBookPath = objCol
'End Function
'
'
''///////////////////////////////////////////////
''/// Sanity tests
''///////////////////////////////////////////////
''''
''''
''''
'Private Sub msubSanityTestOfGetWinRegistoriesInfomationBookPath()
'
'    Debug.Print GetWinRegistoriesInfomationBookPath()
'End Sub
'
''''
''''
''''
'Private Sub msubSanityTestOfDeleteRegWinRegistoriesInfomationBookPath()
'
'    ClearCacheOfModuleNameAndSubjectKeyToCurrentPathDic
'
'    IDelRInUTfWinAPIRegEnumOfTestOutputBookPath
'End Sub
'
'
'
'''--VBA_Code_File--<UTfWinAPIRegEnumForXl.bas>--
'Attribute VB_Name = "UTfWinAPIRegEnumForXl"
''
''   Sanity tests about WinRegViewForXl
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Tue, 31/May/2022    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Constants
''///////////////////////////////////////////////
'Private Const mstrModuleKey As String = "UTfWinAPIRegEnum"   ' 1st part of registry sub-key
'
'
''///////////////////////////////////////////////
''/// Sanity tests
''///////////////////////////////////////////////
''''
'''' sanity test of GetRegSubKeys
''''
'Private Sub msubSanityTestOfGetRegSubKeysOfCurrentVersion()
'
'    Dim strFullSubKey As String
'
'    strFullSubKey = "HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion"
'
'    ' only subkeys list
'    OutputWinRegSubKeysToSheet strFullSubKey
'End Sub
'
''''
'''' sanity test of GetRegValues
''''
'Private Sub msubSanityTestToGetRegValuesOfCurrentVersion()
'
'    Dim strFullSubKey As String
'
'    strFullSubKey = "HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion"
'
'    ' only registry values list
'    OutputWinRegValuesToSheet strFullSubKey
'End Sub
'
'
''''
'''' about "HKEY_CURRENT_USER\Software\VB and VBA Program Settings"
''''
'Private Sub msubSanityTestToGetRegSubKeysOfVBandVBAProgramSettings()
'
'    Dim strFullSubKey As String
'
'    strFullSubKey = "HKEY_CURRENT_USER\Software\VB and VBA Program Settings"
'
'    ' only subkeys list
'    OutputWinRegSubKeysToSheet strFullSubKey
'End Sub
'
'
'
''''
'''' reg-values about VB and VBA Program Settings
''''
'Private Sub msubSanityTestToGetRegValuesOfVBandVBAProgramSettings()
'
'    Dim strFullSubKey As String
'
'    strFullSubKey = "HKEY_CURRENT_USER\Software\VB and VBA Program Settings"
'
'    ' only registry values list
'    OutputWinRegValuesToSheet strFullSubKey
'End Sub
'
'
'
''''
'''' sub-keys and reg-values about VB and VBA Program Settings
''''
'Private Sub msubSanityTestToGetSubKeysAndRegValuesOfVBandVBAProgramSettings()
'
'    Dim strFullSubKey As String
'
'    strFullSubKey = "HKEY_CURRENT_USER\Software\VB and VBA Program Settings"
'
'    ' lists of subkeys and registry values
'    OutputRegValuesWithChildSubKeysToSheet strFullSubKey
'End Sub
'
'
'Private Sub msubSanityTestToTwoSubkeys()
'
'    Dim strFullSubKey As String
'
'    strFullSubKey = "HKEY_CURRENT_USER\Software\Mozilla\NativeMessagingHosts"
'
'    ' only subkeys list
'    OutputWinRegSubKeysToSheet strFullSubKey, False, "SubKeys01A", "Test additional message for SubKeys search"
'
'    strFullSubKey = "HKEY_CURRENT_USER\Software\Realtek"
'
'    ' only registry values list
'    OutputWinRegValuesToSheet strFullSubKey, True, "RegValues02B", "Test additional message for registry values search"
'End Sub
'
'''--VBA_Code_File--<MakeHyperLinksModule.bas>--
'Attribute VB_Name = "MakeHyperLinksModule"
''
''   create and control hyper-link string for each specified Excel sheet
''
''   Note:
''       At the moment, this source code often includes the infamous system-Hungarian notations.
''       Because the Visual Basic Editor has only very poor refactoring functions, Visual Basic (6.0) language
''       doesn't have 'Name Space' specification. And if you have needed to serve low-level numerical
''       calculation codes, you may have noticed that system-Hungarian notaitions are often equal to
''       application-Hungarian notations. And when you need to use Windows API, by using system-Hungarian
''       notations, both the readability and the uniformity of the VBA source will improve.
''       Also you may have to serve sources without installing some development environment,
''       such as Visual Studio Code, Eclipse, or some softwares.
''       If you cannot be assisted from the Code-snippet support by a poor code-editor in this local machine,
''       some system-Hungarian notations may often give you some hints.
''       You want to eliminate all system-Hungarian notations, I recommend to use some outside other language
''       refactoring tools.
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on Excel
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Fri, 13/May/2022    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''''
''''
''''
'Public Sub AddHyperLinkFromKeyColumnAndLinkColumn(ByVal vobjSheet As Excel.Worksheet, _
'        ByVal vstrKeyColumnName As String, _
'        ByVal vstrLinkColumnName As String, _
'        ByVal vobjKeyToLinkDic As Scripting.Dictionary, _
'        Optional ByVal vintFieldTitlesRowIndex As Long = 1, _
'        Optional ByVal vintTopLeftColumnIndex As Long = 1, _
'        Optional ByVal vsngFontSize As Single = 10)
'
'
'    Dim intKeyColumnIdx As Long, intLinkColumnIdx As Long
'    Dim objFieldTitleToPositionIndexDic As Scripting.Dictionary, objSearchingFieldTitles As Collection
'    Dim varKeyColumn() As Variant, intRowIdx As Long, strKeyValue As String, strLinkValue As String
'    Dim objRange As Excel.Range, strCurrentCellValue As String, objHyperlink As Excel.Hyperlink, objLinksRange As Excel.Range
'
'
'    Set objSearchingFieldTitles = New Collection
'
'    With objSearchingFieldTitles
'
'        .Add vstrKeyColumnName
'
'        .Add vstrLinkColumnName
'    End With
'
'    Set objFieldTitleToPositionIndexDic = GetFieldTitleToPositionIndexDic(objSearchingFieldTitles, vobjSheet, vintFieldTitlesRowIndex, vintTopLeftColumnIndex)
'
'    intKeyColumnIdx = objFieldTitleToPositionIndexDic.Item(vstrKeyColumnName)
'
'    intLinkColumnIdx = objFieldTitleToPositionIndexDic.Item(vstrLinkColumnName)
'
'    With vobjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintFieldTitlesRowIndex)).CurrentRegion
'
'        ReDim varKeyColumn(1 To .Rows.Count, 1)
'
'        varKeyColumn = .Worksheet.Range(.Cells(1, intKeyColumnIdx), .Cells(.Rows.Count, intKeyColumnIdx)).Value
'
'        For intRowIdx = 2 To .Rows.Count
'
'            strKeyValue = varKeyColumn(intRowIdx, 1)
'
'            If vobjKeyToLinkDic.Exists(strKeyValue) Then
'
'                strLinkValue = vobjKeyToLinkDic.Item(strKeyValue)
'
'                Set objRange = .Cells(intRowIdx, intLinkColumnIdx)
'
'                With objRange
'
'                    strCurrentCellValue = CStr(objRange.Value)
'
'                    If .Hyperlinks.Count = 0 Then
'
'                        If strCurrentCellValue <> "" Then
'
'                            .Hyperlinks.Add Anchor:=objRange, Address:=strLinkValue, TextToDisplay:=strCurrentCellValue
'                        Else
'                            .Hyperlinks.Add Anchor:=objRange, Address:=strLinkValue, TextToDisplay:=strLinkValue
'                        End If
'                    Else
'                        Set objHyperlink = .Hyperlinks.Item(1)
'
'                        With objHyperlink
'
'                            If strCurrentCellValue <> "" Then
'
'                                .Address = strLinkValue
'
'                                .TextToDisplay = strCurrentCellValue
'                            Else
'                                .Address = strLinkValue
'
'                                .TextToDisplay = strLinkValue
'                            End If
'                        End With
'                    End If
'                End With
'            End If
'        Next
'
'        Set objLinksRange = .Worksheet.Range(.Cells(2, intKeyColumnIdx), .Cells(.Rows.Count, intKeyColumnIdx))
'
'        With objLinksRange
'
'            With .Font
'
'                .Size = vsngFontSize
'            End With
'        End With
'    End With
'End Sub
'
''''
''''
''''
'Public Sub AddHyperLinkAboutUNCPathColumn(ByVal vobjSheet As Excel.Worksheet, _
'        ByVal vstrUNCPathColumnName As String, _
'        Optional ByVal vintFieldTitlesRowIndex As Long = 1, _
'        Optional ByVal vintTopLeftColumnIndex As Long = 1, _
'        Optional ByVal vsngFontSize As Single = 10)
'
'
'    Dim intUNCPathColumnIdx As Long, varUNCPathColumn() As Variant, strUNCPath As String
'
'    Dim objFieldTitleToPositionIndexDic As Scripting.Dictionary
'
'    Dim intRowIdx As Long, objRange As Excel.Range
'    Dim objHyperlink As Excel.Hyperlink, objLinksRange As Excel.Range
'
'
'    Set objFieldTitleToPositionIndexDic = GetFieldTitleToPositionIndexDic(GetColFromLineDelimitedChar(vstrUNCPathColumnName), vobjSheet, vintFieldTitlesRowIndex, vintTopLeftColumnIndex)
'
'    If Not objFieldTitleToPositionIndexDic Is Nothing Then
'
'        If objFieldTitleToPositionIndexDic.Exists(vstrUNCPathColumnName) Then
'
'            intUNCPathColumnIdx = objFieldTitleToPositionIndexDic.Item(vstrUNCPathColumnName)
'
'            With vobjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintFieldTitlesRowIndex)).CurrentRegion
'
'                ReDim varUNCPathColumn(1 To .Rows.Count, 1 To 1)
'
'                varUNCPathColumn = .Worksheet.Range(.Cells(1, intUNCPathColumnIdx), .Cells(.Rows.Count, intUNCPathColumnIdx)).Value
'
'                For intRowIdx = 2 To .Rows.Count
'
'                    strUNCPath = varUNCPathColumn(intRowIdx, 1)
'
'                    If strUNCPath <> "" Then
'
'                        Set objRange = .Cells(intRowIdx, intUNCPathColumnIdx)
'
'                        With objRange
'
'                            If .Hyperlinks.Count = 0 Then
'
'                                .Hyperlinks.Add Anchor:=objRange, Address:=strUNCPath, TextToDisplay:=strUNCPath
'                            Else
'                                Set objHyperlink = .Hyperlinks.Item(1)
'
'                                With objHyperlink
'
'                                    .Address = strUNCPath
'
'                                    .TextToDisplay = strUNCPath
'                                End With
'                            End If
'                        End With
'                    End If
'                Next
'
'                Set objLinksRange = .Worksheet.Range(.Cells(2, intUNCPathColumnIdx), .Cells(.Rows.Count, intUNCPathColumnIdx))
'
'                With objLinksRange
'
'                    With .Font
'
'                        .Size = vsngFontSize
'                    End With
'                End With
'            End With
'        End If
'    End If
'End Sub
'''--VBA_Code_File--<EnumerateAppWindowsForXl.bas>--
'Attribute VB_Name = "EnumerateAppWindowsForXl"
''
''   Sanity tests to list up Windows at the current Windows OS
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on both Windows OS and Excel
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Fri, 11/Aug/2023    Kalmclaeyd Tarclanus    Initial creation
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Constants
''///////////////////////////////////////////////
'Private Const WINDOW_CLASS_EXCEL = "XLMAIN" ' Excel Application
'
'Private Const WINDOW_CLASS_MS_WORD = "OpusApp" ' Word Application
'
'
'
''///////////////////////////////////////////////
''/// Sanity tests
''///////////////////////////////////////////////
''''
''''
''''
'Private Sub msubSanityTestToOutputCurrentWinProcessAndChildWindowsInfoToSheetAboutMsWord()
'
'    Dim strBookPath As String
'
'    strBookPath = mfstrGetTempWindowHWndInfoBooksDir() & "\CurrentMsWordHWndInfo.xlsx"
'
'    OutputCurrentWinProcessAndChildWindowsInfoToSheet strBookPath, WINDOW_CLASS_MS_WORD
'
'    'Dim objWindow As Word.Window
'
'
'
'End Sub
'
''''
''''
''''
'Private Sub msubSanityTestToOutputCurrentWinProcessAndChildWindowsInfoToSheetAboutMsExcel()
'
'    Dim strBookPath As String
'
'    strBookPath = mfstrGetTempWindowHWndInfoBooksDir() & "\CurrentMsExcelHWndInfo.xlsx"
'
'    OutputCurrentWinProcessAndChildWindowsInfoToSheet strBookPath, WINDOW_CLASS_EXCEL
'End Sub
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''''
''''
''''
'Public Sub OutputCurrentWinProcessAndChildWindowsInfoToSheet(ByVal vstrOutputBookPath As String, ByVal vstrParentClassName As String)
'
'    Dim objTopLevelHWndToWindowTextDic As Scripting.Dictionary, objChildWindowClassNameInfoCol As Collection
'    Dim objBook As Excel.Workbook, objTopLevelWindowInfoSheet As Excel.Worksheet, objChildWindowInfoSheet As Excel.Worksheet
'
'
'    FindCurrentProcessAndChildWindowsInfo objTopLevelHWndToWindowTextDic, objChildWindowClassNameInfoCol, vstrParentClassName
'
'    Set objBook = GetWorkbookAndPrepareForUnitTest(vstrOutputBookPath)
'
'    Set objTopLevelWindowInfoSheet = GetNewWorksheetAfterAllExistedSheetsFromBook(objBook)
'
'    objTopLevelWindowInfoSheet.Name = "ParentHWndInfo"
'
'    Set objChildWindowInfoSheet = GetNewWorksheetAfterAllExistedSheetsFromBook(objBook)
'
'    objChildWindowInfoSheet.Name = "ChildHWndInfo"
'
'    msubOutputTopLevelWindowHWndInfoToSheet objTopLevelWindowInfoSheet, objTopLevelHWndToWindowTextDic
'
'    msubOutputChildWindowHWndInfoToSheet objChildWindowInfoSheet, objChildWindowClassNameInfoCol
'
'    DeleteDummySheetWhenItExists objBook
'
'End Sub
'
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
''''
''''
''''
'Private Function mfstrGetTempWindowHWndInfoBooksDir() As String
'
'    Dim strDir As String
'
'    strDir = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\WinOsHWndInfo"
'
'    ForceToCreateDirectory strDir
'
'    mfstrGetTempWindowHWndInfoBooksDir = strDir
'End Function
'
'
''''
''''
''''
'Private Sub msubOutputTopLevelWindowHWndInfoToSheet(ByRef robjSheet As Excel.Worksheet, ByRef robjTopLevelHWndToWindowTextDic As Scripting.Dictionary)
'
'    OutputDicToSheet robjSheet, robjTopLevelHWndToWindowTextDic, GetColFromLineDelimitedChar("ParentHWnd,WindowText"), mfobjGetDataTableSheetFormatterOfHWndInfo()
'End Sub
'
''''
''''
''''
'Private Sub msubOutputChildWindowHWndInfoToSheet(ByRef robjSheet As Excel.Worksheet, ByRef robjChildWindowClassNameInfoCol As Collection)
'
'    OutputColToSheet robjSheet, robjChildWindowClassNameInfoCol, GetColFromLineDelimitedChar("WindowText,ChildClassName,ChildHWnd"), mfobjGetDataTableSheetFormatterOfHWndInfo()
'End Sub
'
'
''''
''''
''''
'Private Function mfobjGetDataTableSheetFormatterOfHWndInfo() As DataTableSheetFormatter
'
'    Dim objDTSheetFormatter As DataTableSheetFormatter
'
'    Set objDTSheetFormatter = New DataTableSheetFormatter
'
'    With objDTSheetFormatter
'
'        .FieldTitleInteriorType = ColumnNameInteriorOfDefaultTypeSQLByDarkBrownFontLightSilverBgMeiryo
'
'        .RecordBordersType = RecordCellsGrayAll
'
'        Set .FieldTitleToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("ParentHWnd,12.5,WindowText,22,ChildClassName,15,ChildHWnd,12.5")
'
'    End With
'
'    Set mfobjGetDataTableSheetFormatterOfHWndInfo = objDTSheetFormatter
'End Function
'
'
'
'''--VBA_Code_File--<EnumerateMsExcels.bas>--
'Attribute VB_Name = "EnumerateMsExcels"
''
''   Detect all Excel.Workbook which includes the outside process of Excel.Application
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on both Excel and Windows OS
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Tue,  1/Oct/2019    Mr. excel-chunchun      The original idea has been disclosed at https://www.excel-chunchun.com/entry/enumwindows-excel-vba
''       Jan, 19/Jan/2023    Kalmclaeyd Tarclanus    Modified
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Directives
''///////////////////////////////////////////////
'#Const HAS_REF = True
'
''///////////////////////////////////////////////
''/// Windows API Declarations
''///////////////////////////////////////////////
'#If VBA7 Then
'    Private Declare PtrSafe Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hwnd As LongPtr, ByVal Msg As LongPtr, ByVal wParam As LongPtr, lParam As Any) As LongPtr
'
'    Private Declare PtrSafe Function IIDFromString Lib "ole32" (lpsz As Any, lpiid As Any) As LongPtr
'
'    Private Declare PtrSafe Function ObjectFromLresult Lib "oleacc" (ByVal lResult As LongPtr, riid As Any, ByVal wParam As LongPtr, ppvObject As Any) As LongPtr
'
'    Private Declare PtrSafe Function IsWindow Lib "user32" (ByVal hwnd As LongPtr) As LongPtr
'#Else
'    Private Declare Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hWnd As Long, ByVal Msg As Long, ByVal wParam As Long, lParam As Any) As Long
'
'    Private Declare Function IIDFromString Lib "ole32" (lpsz As Any, lpiid As Any) As Long
'
'    Private Declare Function ObjectFromLresult Lib "oleacc" (ByVal lResult As Long, riid As Any, ByVal wParam As Long, ppvObject As Any) As Long
'
'    Private Declare Function IsWindow Lib "user32" (ByVal hWnd As Long) As Long
'#End If
'
'
'Private Const OBJID_NATIVEOM = &HFFFFFFF0
'Private Const OBJID_CLIENT = &HFFFFFFFC
'
'' Interface IDentification
'Private Const IID_IMdcList As String = "{8BD21D23-EC42-11CE-9E0D-00AA006002F3}"
'Private Const IID_IUnknown As String = "{00000000-0000-0000-C000-000000000046}"
'Private Const IID_IDispatch As String = "{00020400-0000-0000-C000-000000000046}"
'
'Private Const WM_GETOBJECT = &H3D&
'
''-----
'' class name of Windows programs
'Private Const WINDOW_CLASS_EXCEL = "XLMAIN" ' Excel Application
'Private Const WINDOW_CLASS_VBE = "wndclass_desked_gsk"  ' Visual Basic Editor
'Private Const WINDOW_CALSS_EXPLORER = "CabinetWClass"
'
'Private Const WINDOW_CLASS_MS_WORD = "OpusApp" ' Word Application
'
'Private Const CHILD_CLASS_EXCELWINDOW = "EXCEL7"
'Private Const CHILD_CLASS_VBE = "VbaWindow"
'
''///////////////////////////////////////////////
''/// Declarations
''///////////////////////////////////////////////
'#If HAS_REF Then
'    Private mobjFoundParentHWndToLParamDic As Scripting.Dictionary
'
'    Private mobjFoundChildHWndToParentHWndDic As Scripting.Dictionary
'#Else
'    Private mobjFoundParentHWndToLParamDic As Object  'Dictionary
'
'    Private mobjFoundChildHWndToParentHWndDic As Object   'Dictionary
'#End If
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''**---------------------------------------------
''** Excel.Application state control for outside this VBIDE process
''**---------------------------------------------
''''
'''' This can be also used from either Word application or PowerPoint application
''''
'Public Sub CloseAllProcessExcelBooksExceptMySelfApplication()
'
'    Dim strExceptHwnd As String, varHWnd As Variant, objApplication As Excel.Application, objBook As Excel.Workbook
'
'    strExceptHwnd = ""
'
'    If Trim(Replace(LCase(Application.Name), "microsoft", "")) = "excel" Then
'
'        ' The reason why use the VBA.CallByName() is that this statement can avoid a VBA syntax error in both Word or PowerPoint
'
'        strExceptHwnd = "" & VBA.CallByName(Application, "Hwnd", VbGet)
'    End If
'
'    With ExecutingHwndToExcelApplicationsDic
'
'        For Each varHWnd In .Keys
'
'            If varHWnd <> strExceptHwnd Then
'
'                Set objApplication = .Item(varHWnd)
'
'                For Each objBook In objApplication.Workbooks
'
'                    objBook.Saved = True
'
'                    objBook.Close
'                Next
'
'                objApplication.Quit
'            End If
'        Next
'    End With
'End Sub
'
''''
'''' This can be also used from either Word application or PowerPoint application
''''
'Public Sub ChangeVisibleAllProcessExcelBooksExceptMySelfApplication()
'
'    Dim strExceptHwnd As String, varHWnd As Variant, objApplication As Excel.Application
'
'    strExceptHwnd = ""
'
'    If Trim(Replace(LCase(Application.Name), "microsoft", "")) = "excel" Then
'
'        ' The reason why use the VBA.CallByName() is that this statement can avoid a VBA syntax error in both Word or PowerPoint
'
'        strExceptHwnd = "" & VBA.CallByName(Application, "Hwnd", VbGet)
'    End If
'
'    With ExecutingHwndToExcelApplicationsDic
'
'        For Each varHWnd In .Keys
'
'            If varHWnd <> strExceptHwnd Then
'
'                Set objApplication = .Item(varHWnd)
'
'                With objApplication
'
'                    If Not .Visible Then
'
'                        .Visible = True
'                    End If
'                End With
'            End If
'        Next
'    End With
'End Sub
'
'
''**---------------------------------------------
''** Pick outside Excel processes up tools by Windows API
''**---------------------------------------------
''''
'''' get all Excel application window object for all Windows processes in execution
''''
'''' <Return>Excel.Window(0 To #) - Array of the Excel window objects</Return>
''''
'''' <Attention>
''''  Remove some Excel processes of the different execution autorities, which cannot be got. However if this Excel process is executed with the Administrator authority, it can be got.
''''  Remove the protected view Excel processes even though this Excel process is also executed with the Adiministrator authority.
'''' </Attention>
'Private Function ExecExcelWindows() As Variant
'
'    'get objects as if this enumerate ExcelProcess().excelWindow().hWnd
'    Dim varHWndArray As Variant, varSafeExcelWindowArray As Variant
'
'    varHWndArray = EnumFindChildWindows(vstrParentClassName:=WINDOW_CLASS_EXCEL, vstrChildClassName:=CHILD_CLASS_EXCELWINDOW)
'
'    If LBound(varHWndArray) <= UBound(varHWndArray) Then
'
'        ReDim varSafeExcelWindowArray(LBound(varHWndArray) To UBound(varHWndArray))
'
'        Dim i As Long, j As Long: j = LBound(varHWndArray) - 1
'
'        For i = LBound(varHWndArray) To UBound(varHWndArray)
'
'            j = j + 1
'
'            Set varSafeExcelWindowArray(j) = GetExcelWindowByHWnd(varHWndArray(i))
'
'            ' To be failed to get it, because of the lack ot the authority
'            If varSafeExcelWindowArray(j) Is Nothing Then j = j - 1
'
'            ' Disabled to operate this because of the protected view of the Excel window. If you try to operate it, then the Excel application process will be aborted.
'            If TypeName(varSafeExcelWindowArray(j)) = "ProtectedViewWindow" Then j = j - 1
'
'            ' Disabled to operate this because of the protected view of the Excel window. If you try to operate it, then the Excel application process will be aborted.
'            If varSafeExcelWindowArray(j).Application.hwnd = 0 Then j = j - 1
'        Next
'
'        If j < LBound(varHWndArray) Then
'
'            ExecExcelWindows = Array()
'        Else
'            ReDim Preserve varSafeExcelWindowArray(LBound(varHWndArray) To j)
'
'            ExecExcelWindows = varSafeExcelWindowArray
'        End If
'    Else
'        ' Only when this function is executed from either Word or PowerPoint and there are no Excel process, then the following should have been executed.
'
'        ExecExcelWindows = Array()
'    End If
'End Function
'
''''
'''' get the array of the Excel.Application all objects which includes other Excel application processes
''''
'''' <Return>Excel.Application(0 To #) - array of objects</Return>
''''
'''' <Attention>
''''  The protected view Excel window returns 0 value
''''  If it have been tried to access, this Excel process might be aborted.
'''' </Attention>
'Public Function ExecExcelApps() As Variant
'
'    ' Delete duplicated Application.hWnd
'    Dim objHWndToExcelApplicationDic As Scripting.Dictionary: Set objHWndToExcelApplicationDic = CreateObject("Scripting.Dictionary")
'    Dim varWindow  As Variant, objWindow As Excel.Window
'    Dim strHWnd As String
'
'    For Each varWindow In ExecExcelWindows()
'
'        Set objWindow = varWindow
'
'        strHWnd = "" & objWindow.Application.hwnd
'
'        If strHWnd = "0" Then
''            Debug.Print "This may be a protected view of the Excel application"
'        Else
'            With objHWndToExcelApplicationDic
'
'                If Not .Exists(strHWnd) Then
'
'                    .Add strHWnd, objWindow.Application
'
'    '                Debug.Print hWnd, Win.Application.Caption
'                End If
'            End With
'        End If
'    Next
'
'    ExecExcelApps = objHWndToExcelApplicationDic.Items  ' Return array of Excel.Application object references
'End Function
'
''''
'''' get the array of the Excel.Workbook all objects which includes other Excel application processes
''''
'''' <Return>Excel.Workbook(0 To #) - array of the Workbook objects</Return>
''''
'''' <Attention>
''''  The protected view Excel window returns 0 value
''''  If it have been tried to access, this Excel process might be aborted.
'''' </Attention>
'Public Function ExecExcelWorkbooks() As Variant
'
'    'Remove the duplicated Application.hWnd
'    Dim objBookNameToBookInstanceDic As Scripting.Dictionary: Set objBookNameToBookInstanceDic = CreateObject("Scripting.Dictionary")
'
'    Dim varWindow As Variant
'    Dim objWindow As Excel.Window, objBook As Excel.Workbook
'
'    Dim strHWnd As String
'
'    For Each varWindow In ExecExcelWindows()
'
'        Set objWindow = varWindow
'
'        strHWnd = "" & objWindow.Application.hwnd
'
'        If strHWnd = "0" Then
'
''            Debug.Print "This may be a protected view of the Excel application"
'        Else
'            Set objBook = objWindow.Parent
'
'            With objBookNameToBookInstanceDic
'
'                If Not .Exists(objBook.Name) Then
'
'                    .Add objBook.Name, objBook
'
'        '            Debug.Print Win.Parent.Name, Win.Parent.Name
'                End If
'            End With
'        End If
'    Next
'
'    ExecExcelWorkbooks = objBookNameToBookInstanceDic.Items ' return Excel.Workbook(0 To #)
'End Function
'
''''
''''
''''
'Public Function ExecutingHwndToExcelApplicationsDic() As Scripting.Dictionary
'
'    Dim objHwndToExcelApplicationsDic As Scripting.Dictionary
'    Dim varWindow As Variant, objWindow As Excel.Window, strHWnd As String
'
'
'    Set objHwndToExcelApplicationsDic = New Scripting.Dictionary
'
'    For Each varWindow In ExecExcelWindows()
'
'        Set objWindow = varWindow
'
'        strHWnd = "" & objWindow.Application.hwnd
'
'        If strHWnd = "0" Then
'
''            Debug.Print "This may be a protected view of the Excel application"
'        Else
'            With objHwndToExcelApplicationsDic
'
'                If Not .Exists(strHWnd) Then
'
'                    .Add strHWnd, objWindow.Application
'                End If
'            End With
'        End If
'    Next
'
'    Set ExecutingHwndToExcelApplicationsDic = objHwndToExcelApplicationsDic
'End Function
'
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
''''
'''' get Excel.Window instances from hWnd window-handle
''''
'''' <Argument>hWnd : window handle</Argument>
'''' <Return>Excel.Window</Return>
''''
'''' <Attention>
''''  If a Excel process is executed by the other user authority,
''''  the object may be failed to get, and this returns to Nothing.
'''' </Attention>
'Private Function GetExcelWindowByHWnd(ByRef rvarHWnd As Variant) As Excel.Window
'
'    Dim intID(0 To 3) As Long
'    Dim bytID()     As Byte
'
'#If VBA7 Then
'    Dim intMessageResult As LongPtr, intReturn  As LongPtr
'#Else
'    Dim intMessageResult As Long, intReturn  As Long
'#End If
'
'    Dim objExcelWindow As Excel.Window
'
'    If IsWindow(rvarHWnd) = 0 Then Exit Function
'
'    intMessageResult = SendMessage(rvarHWnd, WM_GETOBJECT, 0, ByVal OBJID_NATIVEOM)
'
'    If intMessageResult Then
'
'        bytID = IID_IDispatch & vbNullChar
'
'        IIDFromString bytID(0), intID(0)
'
'        intReturn = ObjectFromLresult(intMessageResult, intID(0), 0, objExcelWindow)
'    End If
'
'    Set GetExcelWindowByHWnd = objExcelWindow
'End Function
'
'
''///////////////////////////////////////////////
''/// Sanity tests
''///////////////////////////////////////////////
''''
'''' Enumerate all Excel.exe which includes other processes
''''
'''' The protected view book also allow to display the App.Caption
''''
'Private Sub msubSanityTestToExecExcelApps()
'
'    Dim varApp As Variant
'
'    Debug.Print "WindowHandle", "CountOfBooks", "ApplicationName"
'
'    For Each varApp In ExecExcelApps()
'
'        Debug.Print varApp.hwnd, varApp.Workbooks.Count, varApp.Caption
'    Next
'End Sub
'
''''
'''' Enumerate all Excel Work books which includes other processes
''''
'Private Sub msubSanityTestToExecExcelWorkbooks()
'
'    Dim varWorkbook As Variant
'
'    Debug.Print "WindowHandle", "CountOfWindows", "BookName"
'
'    For Each varWorkbook In ExecExcelWorkbooks()
'
'        Debug.Print varWorkbook.Application.hwnd, varWorkbook.Windows.Count, varWorkbook.Name
'    Next
'End Sub
'
''''
'''' Enumerate all Excel Window objects which includes other processes
''''
'Private Sub msubSanityTestToExecExcelWindows()
'
'    Dim varWindow As Variant, objWindow As Excel.Window
'    'Dim hWnd As String
'
'    Debug.Print "WindowHandle", "Zero", "WindowTitleBarCaption"
'
'    For Each varWindow In ExecExcelWindows()
'
'        If varWindow Is Nothing Then
''            Debug.Print "err"
'        ElseIf TypeName(varWindow) = "ProtectedViewWindow" Then
''            Debug.Print "err"
'        ElseIf varWindow.Application.hwnd = 0 Then
''            Debug.Print "This may be a protected window"
'        Else
'            Set objWindow = varWindow
'
'            Debug.Print objWindow.Application.hwnd, 0, objWindow.Caption
'        End If
'    Next
'End Sub
'
'
'''--VBA_Code_File--<EnumerateMsExcelsAndClose.bas>--
'Attribute VB_Name = "EnumerateMsExcelsAndClose"
''
''   Close Excel book which includes the outside process of Excel.Application
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on both Excel and Windows OS
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Tue,  1/Oct/2019    Mr. excel-chunchun      A part of the idea has been disclosed at https://www.excel-chunchun.com/entry/enumwindows-excel-vba
''       Wed, 31/May/2023    Kalmclaeyd Tarclanus    Modified
''
'
'Option Explicit
'
'
''///////////////////////////////////////////////
''/// Windows API Declarations
''///////////////////////////////////////////////
'#If VBA7 Then
'    Private Declare PtrSafe Function EnumWindows Lib "user32.dll" (ByVal lpEnumFunc As LongPtr, ByVal lParam As LongPtr) As LongPtr
'
'    Private Declare PtrSafe Function EnumChildWindows Lib "user32.dll" (ByVal hWndParent As LongPtr, ByVal lpEnumFunc As LongPtr, ByVal lParam As LongPtr) As LongPtr
'
'    Private Declare PtrSafe Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hwnd As LongPtr, ByVal Msg As LongPtr, ByVal wParam As LongPtr, lParam As Any) As LongPtr
'    Private Declare PtrSafe Function IIDFromString Lib "ole32" (lpsz As Any, lpiid As Any) As LongPtr
'    Private Declare PtrSafe Function ObjectFromLresult Lib "oleacc" (ByVal lResult As LongPtr, riid As Any, ByVal wParam As LongPtr, ppvObject As Any) As LongPtr
'    Private Declare PtrSafe Function IsWindow Lib "user32" (ByVal hwnd As LongPtr) As LongPtr
'#Else
'    Private Declare Function EnumWindows Lib "user32.dll" (ByVal lpEnumFunc As Long, ByVal lParam As Long) As Long
'
'    Private Declare Function EnumChildWindows Lib "user32.dll" (ByVal hWndParent As Long, ByVal lpEnumFunc As Long, ByVal lParam As Long) As Long
'
'    Private Declare Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hWnd As Long, ByVal Msg As Long, ByVal wParam As Long, lParam As Any) As Long
'    Private Declare Function IIDFromString Lib "ole32" (lpsz As Any, lpiid As Any) As Long
'    Private Declare Function ObjectFromLresult Lib "oleacc" (ByVal lResult As Long, riid As Any, ByVal wParam As Long, ppvObject As Any) As Long
'    Private Declare Function IsWindow Lib "user32" (ByVal hWnd As Long) As Long
'#End If
'
'Private Const OBJID_NATIVEOM = &HFFFFFFF0
'Private Const OBJID_CLIENT = &HFFFFFFFC
'
'Private Const IID_IMdcList = "{8BD21D23-EC42-11CE-9E0D-00AA006002F3}"
'Private Const IID_IUnknown = "{00000000-0000-0000-C000-000000000046}"
'Private Const IID_IDispatch = "{00020400-0000-0000-C000-000000000046}"
'
'
'Private Const WM_GETOBJECT = &H3D&
'
''///////////////////////////////////////////////
''/// Declarations
''///////////////////////////////////////////////
'
'Private mobjWindowHandleToBooksDic As Scripting.Dictionary  ' Dictionary(Of LongPtr[hWnd], Of Excel.Workbook)
'
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''''
'''' use VBA.Dir
''''
'Public Sub DeleteExcelBookIfItInThisProcessOrOtherProcessesExistsWithClosing(ByVal vstrBookPath As String)
'
'    Dim strFileName As String
'
'    If FileExistsByVbaDir(vstrBookPath) Then
'
'        strFileName = GetFileNameFromPathByVbaDir(vstrBookPath)
'
'        CloseExcelForAllProcessesByBookName strFileName
'
'        VBA.Kill vstrBookPath
'    End If
'End Sub
'
'
''''
''''
''''
'Public Sub CloseExcelForAllProcessesByBookName(ByVal vstrTargetBookNameTip As String)
'
'#If VBA7 Then
'    Dim intReturn As LongPtr
'#Else
'    Dim intReturn As Long
'#End If
'
'    Dim varHWnd As Variant, objBooksCol As Collection, objBook As Excel.Workbook
'    Dim i As Long
'
'    On Error Resume Next
'
'    Set mobjWindowHandleToBooksDic = Nothing
'
'
'    On Error GoTo 0
'
'    ' get Window handle of Excel workbooks
'    intReturn = EnumWindows(AddressOf EnumWindowsProc, ByVal 0&)
'
'    If Not mobjWindowHandleToBooksDic Is Nothing Then
'
'        With mobjWindowHandleToBooksDic
'
'            For Each varHWnd In .Keys
'
'                Set objBook = .Item(varHWnd)
'
'                With objBook
'
'                    If InStr(1, .Name, vstrTargetBookNameTip) > 0 Then
'
'                        Debug.Print "To close Excel: " & .Name
'
'                        .Saved = True
'
'                        .Close
'                    End If
'                End With
'            Next
'        End With
'    End If
'End Sub
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
''''
'''' call back function
''''
'#If VBA7 Then
'Private Function EnumWindowsProc(ByVal vintHWnd As LongPtr, ByVal vintlParam As LongPtr) As Long
'
'    Dim intReturn       As LongPtr
'#Else
'Private Function EnumWindowsProc(ByVal vintHWnd As Long, ByVal vintlParam As Long) As Long
'
'    Dim intReturn       As Long
'#End If
'
'    Dim strClass        As String
'
'    strClass = GetClassNameVBAString(vintHWnd)
'
'    If strClass = "XLMAIN" Then
'
'        ' enumerate the child windows
'
'        intReturn = EnumChildWindows(vintHWnd, AddressOf EnumChildSubProc, vintlParam)
'    End If
'
'    ' continue to enumerate
'EnumPass:
'
'    EnumWindowsProc = True
'End Function
'
''''
'''' call back function: enumerate the child windows
''''
'#If VBA7 Then
'Private Function EnumChildSubProc(ByVal vintHwndChild As LongPtr, ByVal vintlParam As LongPtr) As Long
'#Else
'Private Function EnumChildSubProc(ByVal vintHwndChild As Long, ByVal vintlParam As Long) As Long
'#End If
'
'    Dim strClass As String, strWindowTitle  As String
'    Dim objBook As Excel.Workbook, strHWnd As String
'
'    strClass = GetClassNameVBAString(vintHwndChild)
'
'    If strClass = "EXCEL7" Then
'
'        strWindowTitle = GetWindowTextVBAString(vintHwndChild)
'
'        If InStr(1, LCase(strWindowTitle), ".xla") = 0 Then   ' ignore Excel Adin Workbook
'
'            If mobjWindowHandleToBooksDic Is Nothing Then Set mobjWindowHandleToBooksDic = New Scripting.Dictionary
'
'            Set objBook = GetExcelBookFromWindowHandle(vintHwndChild)
'
'            strHWnd = CStr(vintHwndChild)
'
'            mobjWindowHandleToBooksDic.Add strHWnd, objBook
'        End If
'    End If
'
'    ' continue to enumerate
'EnumChildPass:
'
'    EnumChildSubProc = True
'End Function
'
'
'
'#If VBA7 Then
'Private Function GetExcelBookFromWindowHandle(ByVal vintHWnd As LongPtr) As Excel.Workbook
'
'    Dim intExcelHWnd As LongPtr, intReturn As LongPtr, intMessageResult As LongPtr
'#Else
'Private Function GetExcelBookFromWindowHandle(ByVal vintHWnd As Long) As Excel.Workbook
'
'    Dim intExcelHWnd As Long, intReturn As Long, intMessageResult As Long
'#End If
'
'    Dim IID(0 To 3) As Long, bytID() As Byte
'    Dim objWindow As Excel.Window, objBook As Excel.Workbook
'
'    intExcelHWnd = vintHWnd
'
'    If IsWindow(intExcelHWnd) = 0 Then Exit Function
'
'    intMessageResult = SendMessage(intExcelHWnd, WM_GETOBJECT, 0, ByVal OBJID_NATIVEOM)
'
'    If intMessageResult Then
'
'        bytID = IID_IDispatch & vbNullChar
'
'        IIDFromString bytID(0), IID(0)
'
'        intReturn = ObjectFromLresult(intMessageResult, IID(0), 0, objWindow)
'
'        If Not objWindow Is Nothing Then
'
'            Set objBook = objWindow.Parent
'        End If
'    End If
'
'    Set GetExcelBookFromWindowHandle = objBook
'End Function
'
'
''///////////////////////////////////////////////
''/// Sanity tests
''///////////////////////////////////////////////
''''
''''
''''
'Private Sub msubSanityTestToCloseXlWorkbooksForAllExcelProcesses()
'
'    ' For all Excel application process, close the Excel workbooks which file-name include [Book].
'
'    CloseExcelForAllProcessesByBookName "Book"
'End Sub
'
''''
'''' this can close all out-process 'ANewMacroBook" name Excel books
''''
'Private Sub msubSanityTestToCloseSpecifiedNameBookForAllExcelProcesses()
'
'    ' For all Excel application process, close the Excel workbooks which file-name include [Book].
'
'    CloseExcelForAllProcessesByBookName "ANewMacroBook"
'End Sub
'''--VBA_Code_File--<UTfCreateDataTableForXl.bas>--
'Attribute VB_Name = "UTfCreateDataTableForXl"
''
''   output created fictional data-tables into Excel.Worksheet
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on Excel
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Wed,  8/Jun/2022    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
'
''///////////////////////////////////////////////
''/// Sanity tests
''///////////////////////////////////////////////
''**---------------------------------------------
''** Create plural data-table sheets on a book
''**---------------------------------------------
''''
''''
''''
'Private Sub msubSanityTestToCreateAllTypeTestSheetSet()
'
'    Dim objDic As Scripting.Dictionary, strBookPath As String
'
'    Set objDic = New Scripting.Dictionary
'
'    With objDic
'
'        .Add UnitTestPreparedSampleDataTable.BasicSampleDT, 50
'
'        .Add UnitTestPreparedSampleDataTable.IncludeFictionalProductionNumberSampleDT, 1000
'
'        .Add UnitTestPreparedSampleDataTable.FishTypeSampleMasterDT, 1
'
'        .Add UnitTestPreparedSampleDataTable.FlowerTypeSampleMasterDT, 1
'    End With
'
'    strBookPath = GetBookPathAfterCreatePluralSampleDataTablesToSheet(objDic, "GeneratedAllTypeSampleDataTables")
'End Sub
'
'
''''
''''
''''
'Private Sub msubOpenSampleDataTableWorkbookForSanityTest()
'
'    OpenSampleDataTableWorkbookForSanityTest IncludeFictionalProductionNumberSampleDT
'End Sub
'
'
'Private Sub msubSanityTestOfCreateSampleDataTable01ToSheet()
'
'    Dim varDataTable As Variant, objFieldTitles As Collection, objDoubleStopWatch As DoubleStopWatch
'
'    Set objDoubleStopWatch = New DoubleStopWatch
'
'    objDoubleStopWatch.MeasureStart
'
'    CreateTestingSimpleDataTable01 varDataTable, objFieldTitles, 100
'
'    objDoubleStopWatch.MeasureInterval
'
'    OutputCreatedSampleDataTableToSheet BasicSampleDT, varDataTable, objFieldTitles, mfobjGetDataTableFormatterForSample01(), objDoubleStopWatch.ElapsedSecondTime
'End Sub
'
'
'Private Sub msubSanityTestOfCreateSampleDataTable02ToSheet()
'
'    Dim varDataTable As Variant, objFieldTitles As Collection, objDoubleStopWatch As DoubleStopWatch
'
'    Set objDoubleStopWatch = New DoubleStopWatch
'
'    objDoubleStopWatch.MeasureStart
'
'    CreateTestingSimpleDataTable02 varDataTable, objFieldTitles, 1000
'
'    objDoubleStopWatch.MeasureInterval
'
'    OutputCreatedSampleDataTableToSheet IncludeFictionalProductionNumberSampleDT, varDataTable, objFieldTitles, mfobjGetDataTableFormatterForSample02(), objDoubleStopWatch.ElapsedSecondTime
'End Sub
'
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''''
''''
''''
'Public Function GetExcelBookPathAfterCheckFileExistenceOrCreateWhenItDoesntExist(ByRef rstrSheetName As String, _
'        ByVal venmUnitTestPreparedSampleDataTable As UnitTestPreparedSampleDataTable, _
'        Optional ByVal vintCountOfRows As Long = 100) As String
'
'    Dim strBookPath As String
'
'    strBookPath = GetPreparedSampleDataTableExcelBookPathForTest(venmUnitTestPreparedSampleDataTable)
'
'    rstrSheetName = GetSheetNameOfPreparedSampleDataTableExcelBookForTest(venmUnitTestPreparedSampleDataTable)
'
'    With New Scripting.FileSystemObject
'
'        If Not .FileExists(strBookPath) Then
'
'            ' create
'
'            GetBookPathAfterCreateSampleDataTableSheet venmUnitTestPreparedSampleDataTable, vintCountOfRows, True
'        End If
'    End With
'
'    GetExcelBookPathAfterCheckFileExistenceOrCreateWhenItDoesntExist = strBookPath
'End Function
'
'
''''
''''
''''
'Private Function GetBookPathAfterCreateSampleDataTableSheet(ByVal venmUnitTestPreparedSampleDataTable As UnitTestPreparedSampleDataTable, _
'        Optional ByVal vintCountOfRows As Long = 100, _
'        Optional ByVal vblnAllowToCloseWorkbookAfterFinishedCreating As Boolean = False) As String
'
'
'    Dim varDataTable As Variant, objFieldTitles As Collection, objDoubleStopWatch As DoubleStopWatch
'    Dim strBookPath As String, objSheet As Excel.Worksheet, objBook As Excel.Workbook
'
'    Set objDoubleStopWatch = New DoubleStopWatch
'
'    With objDoubleStopWatch
'
'        .MeasureStart
'
'        CreateSimpleRandomDataTable varDataTable, objFieldTitles, vintCountOfRows, venmUnitTestPreparedSampleDataTable
'
'        .MeasureInterval
'    End With
'
'
'    strBookPath = GetPreparedSampleDataTableExcelBookPathForTest(venmUnitTestPreparedSampleDataTable)
'
'    Set objSheet = mfobjOutputExcelBookFromSampleDataTable(varDataTable, objFieldTitles, venmUnitTestPreparedSampleDataTable)
'
'    If vblnAllowToCloseWorkbookAfterFinishedCreating Then
'
'        Set objBook = objSheet.Parent
'
'        ForceToSaveBookWithoutDisplayAlert objBook
'
'        objBook.Close
'    End If
'
'    GetBookPathAfterCreateSampleDataTableSheet = strBookPath
'End Function
'
'
''''
''''
''''
'Private Function GetBookPathAfterCreatePluralSampleDataTablesToSheet(ByVal vobjUnitTestPreparedSampleDataTableToParamsDic As Scripting.Dictionary, _
'        Optional ByVal vstrBookBaseName As String = "GeneratedSampleDataTablesForTests", _
'        Optional ByVal vblnAllowToCloseWorkbookAfterFinishedCreating As Boolean = False) As String
'
'
'    Dim varUnitTestPreparedSampleDataTable As Variant, enmUnitTestPreparedSampleDataTable As UnitTestPreparedSampleDataTable
'    Dim intCountOfRows As Long
'    Dim varDataTable As Variant, objFieldTitles As Collection, objDoubleStopWatch As DoubleStopWatch
'    Dim strBookPath As String, objBook As Excel.Workbook, objSheet As Excel.Worksheet, objCurrentSheet As Excel.Worksheet
'
'
'    strBookPath = GetPreparedPluralSampleDataTablesExcelBookPath(vstrBookBaseName)
'
'    Set objBook = GetWorkbookAndPrepareForUnitTest(strBookPath)
'
'    Set objDoubleStopWatch = New DoubleStopWatch
'
'    With objDoubleStopWatch
'
'        .MeasureStart
'
'        With vobjUnitTestPreparedSampleDataTableToParamsDic
'
'            For Each varUnitTestPreparedSampleDataTable In .Keys
'
'                enmUnitTestPreparedSampleDataTable = varUnitTestPreparedSampleDataTable
'
'                intCountOfRows = .Item(varUnitTestPreparedSampleDataTable)
'
'                CreateSimpleRandomDataTable varDataTable, objFieldTitles, intCountOfRows, enmUnitTestPreparedSampleDataTable
'
'                Set objSheet = GetNewWorksheetAfterAllExistedSheetsFromBook(objBook)
'
'                objSheet.Name = GetSheetNameOfPreparedSampleDataTableExcelBookForTest(enmUnitTestPreparedSampleDataTable)
'
'                OutputVarTableToSheet objSheet, varDataTable, objFieldTitles, mfobjGetDataTableSheetFormatterOfPreparedSampleType(enmUnitTestPreparedSampleDataTable)
'
'                AddAutoShapeGeneralMessage objSheet, GetLogTextOfPreparedSampleDataTableBaseName(enmUnitTestPreparedSampleDataTable)
'
'                Set objCurrentSheet = objSheet
'            Next
'
'        End With
'
'        .MeasureInterval
'    End With
'
'    AddAutoShapeGeneralMessage objCurrentSheet, "All processes elapsed time: " & objDoubleStopWatch.ElapsedTimeByString, AutoShapeMessageFillPatternType.XlShapeTextLogInteriorFillRedCornerGradient
'
'    DeleteDummySheetWhenItExists objBook
'
'    GetBookPathAfterCreatePluralSampleDataTablesToSheet = strBookPath
'End Function
'
'
''''
'''' This is used at SqlUTfXlSheetExpander
''''
'Public Function GetOutputBookPathAfterCreateALeastTestSheetSet() As String
'
'    Dim objDic As Scripting.Dictionary, strBookPath As String
'
'    Set objDic = New Scripting.Dictionary
'
'    With objDic
'
'        .Add UnitTestPreparedSampleDataTable.IncludeFictionalProductionNumberSampleDT, 1000
'
'        .Add UnitTestPreparedSampleDataTable.FishTypeSampleMasterDT, 1
'
'    End With
'
'    strBookPath = GetBookPathAfterCreatePluralSampleDataTablesToSheet(objDic)
'
'    GetOutputBookPathAfterCreateALeastTestSheetSet = strBookPath
'End Function
'
'
'
'
'Public Sub OpenSampleDataTableWorkbookForSanityTest(ByVal venmSanityTestPreparedSampleDataTable As UnitTestPreparedSampleDataTable)
'
'    GetWorkbookIfItExists GetPreparedSampleDataTableBookPathForSanityTest(venmSanityTestPreparedSampleDataTable), True
'End Sub
'
'
''''
''''
''''
'Public Sub GetBookPathAndSheetNameAndFieldTitleToColumnWidthDicFromSanityTestPreparedSampleDataTable(ByRef rstrBookPath As String, ByRef rstrSheetName As String, ByRef robjFieldTitleToColumnWidthDic As Scripting.Dictionary, ByVal venmSanityTestPreparedSampleDataTable As UnitTestPreparedSampleDataTable)
'
'    PrepareSampleDataTableWorkbookForSanityTest venmSanityTestPreparedSampleDataTable
'
'    rstrBookPath = GetPreparedSampleDataTableBookPathForSanityTest(venmSanityTestPreparedSampleDataTable)
'
'    rstrSheetName = GetPreparedSampleDataTableSheetNameForSanityTest(venmSanityTestPreparedSampleDataTable)
'
'    Set robjFieldTitleToColumnWidthDic = GetPreparedSampleDataTableFieldTitleToColumnWidthDicForSanityTest(venmSanityTestPreparedSampleDataTable)
'End Sub
'
'
''''
''''
''''
'Public Sub PrepareSampleDataTableWorkbookForSanityTest(ByVal venmSanityTestPreparedSampleDataTable As UnitTestPreparedSampleDataTable)
'
'    With New Scripting.FileSystemObject
'
'        Select Case venmSanityTestPreparedSampleDataTable
'
'            Case UnitTestPreparedSampleDataTable.BasicSampleDT
'
'                If Not .FileExists(GetPreparedSampleDataTableBookPathForSanityTest(BasicSampleDT)) Then
'
'                    msubSanityTestOfCreateSampleDataTable01ToSheet
'
'                End If
'
'            Case UnitTestPreparedSampleDataTable.IncludeFictionalProductionNumberSampleDT
'
'                If Not .FileExists(GetPreparedSampleDataTableBookPathForSanityTest(IncludeFictionalProductionNumberSampleDT)) Then
'
'                    msubSanityTestOfCreateSampleDataTable02ToSheet
'                End If
'        End Select
'    End With
'End Sub
'
'
''**---------------------------------------------
''** solute Sample book path and name
''**---------------------------------------------
''''
''''
''''
'Public Function GetPreparedSampleDataTableSheetNameForSanityTest(ByVal venmSanityTestPreparedSampleDataTable As UnitTestPreparedSampleDataTable) As String
'
'    Dim strSheetName As String
'
'    strSheetName = ""
'
'    Select Case venmSanityTestPreparedSampleDataTable
'
'        Case UnitTestPreparedSampleDataTable.BasicSampleDT
'
'            strSheetName = "SampleTest01"
'
'        Case UnitTestPreparedSampleDataTable.IncludeFictionalProductionNumberSampleDT
'
'            strSheetName = "SampleTest02"
'    End Select
'
'    GetPreparedSampleDataTableSheetNameForSanityTest = strSheetName
'End Function
'
'
'Public Function GetPreparedSampleDataTableBookPathForSanityTest(ByVal venmSanityTestPreparedSampleDataTable As UnitTestPreparedSampleDataTable) As String
'
'    GetPreparedSampleDataTableBookPathForSanityTest = GetCurrentBookOutputDir() & "\AutoGeneratedSamples\" & GetPreparedSampleDataTableBaseNameForUnitTest(venmSanityTestPreparedSampleDataTable) & ".xlsx"
'End Function
'
''''
''''
''''
'Public Function GetPreparedSampleDataTableFieldTitleToColumnWidthDicForSanityTest(ByVal venmSanityTestPreparedSampleDataTable As UnitTestPreparedSampleDataTable) As Scripting.Dictionary
'
'    Dim objDic As Scripting.Dictionary
'
'    Set objDic = Nothing
'
'    Select Case venmSanityTestPreparedSampleDataTable
'
'        Case UnitTestPreparedSampleDataTable.BasicSampleDT
'
'            Set objDic = GetTextToRealNumberDicFromLineDelimitedChar("FixedString,12.5,RowNumber,13,FlowerTypeSample,14.5,RandomInteger,16")
'
'        Case UnitTestPreparedSampleDataTable.IncludeFictionalProductionNumberSampleDT
'
'            Set objDic = GetTextToRealNumberDicFromLineDelimitedChar("RowNumber,13,RealNumber,16,FishType,14.5,FictionalProductionSerialNumber,15")
'    End Select
'
'    Set GetPreparedSampleDataTableFieldTitleToColumnWidthDicForSanityTest = objDic
'End Function
'
'
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
''**---------------------------------------------
''** craete a Excel book
''**---------------------------------------------
''''
''''
''''
'Private Sub msubSanityTestToOpenExcelBookAfterCreateSampleDataTable01()
'
'    mfstrGetBookPathAfterCreateSampleDataTableSheet01
'End Sub
'
''''
''''
''''
'Private Sub msubSanityTestToOpenExcelBookAfterCreateSampleDataTable02()
'
'    mfstrGetBookPathAfterCreateSampleDataTableSheet02
'End Sub
'
'
''**---------------------------------------------
''** output Excel.Worksheet as sample
''**---------------------------------------------
''''
''''
''''
'Private Sub OutputCreatedSampleDataTableToSheet(ByVal venmSanityTestPreparedSampleDataTable As UnitTestPreparedSampleDataTable, _
'        ByVal vvarRCTable As Variant, _
'        ByVal vobjFieldTitlesCol As Collection, _
'        ByVal vobjDataTableSheetFormatter As DataTableSheetFormatter, _
'        Optional ByVal vdblElapsedSecondTime As Double = 0#)
'
'
'    Dim strBookPath As String, objBook As Excel.Workbook, objSheet As Excel.Worksheet
'
'    strBookPath = GetPreparedSampleDataTableBookPathForSanityTest(venmSanityTestPreparedSampleDataTable)
'
'    Set objBook = GetWorkbookAndPrepareForUnitTest(strBookPath)
'
'    Set objSheet = GetNewWorksheetAfterAllExistedSheetsFromBook(objBook)
'
'    objSheet.Name = GetPreparedSampleDataTableSheetNameForSanityTest(venmSanityTestPreparedSampleDataTable)
'
'    OutputVarTableToSheet objSheet, vvarRCTable, vobjFieldTitlesCol, vobjDataTableSheetFormatter
'
'    msubAddCreatingSapmleDataTableLog objSheet, vdblElapsedSecondTime
'
'    DeleteDummySheetWhenItExists objBook
'
'    ' save
'    ForceToSaveBookWithoutDisplayAlert objBook
'End Sub
'
'
''**---------------------------------------------
''** About Excel book files
''**---------------------------------------------
''''
''''
''''
'Private Function mfstrGetBookPathAfterCreateSampleDataTableSheet01(Optional ByVal vintCountOfRows As Long = 100, Optional ByVal vblnAllowToCloseWorkbookAfterFinishedCreating As Boolean = False) As String
'
'    mfstrGetBookPathAfterCreateSampleDataTableSheet01 = GetBookPathAfterCreateSampleDataTableSheet(BasicSampleDT, vintCountOfRows, vblnAllowToCloseWorkbookAfterFinishedCreating)
'End Function
'
''''
''''
''''
'Private Function mfstrGetBookPathAfterCreateSampleDataTableSheet02(Optional ByVal vintCountOfRows As Long = 1000, Optional ByVal vblnAllowToCloseWorkbookAfterFinishedCreating As Boolean = False) As String
'
'    mfstrGetBookPathAfterCreateSampleDataTableSheet02 = GetBookPathAfterCreateSampleDataTableSheet(IncludeFictionalProductionNumberSampleDT, vintCountOfRows, vblnAllowToCloseWorkbookAfterFinishedCreating)
'End Function
'
'
'
''''
''''
''''
'Private Function mfobjOutputExcelBookFromSampleDataTable(ByRef rvarDataTable As Variant, ByRef robjFieldTitles As Collection, ByVal venmUnitTestPreparedSampleDataTable As UnitTestPreparedSampleDataTable) As Excel.Worksheet
'
'    Dim strBookPath As String, objBook As Excel.Workbook, objSheet As Excel.Worksheet
'
'
'    strBookPath = GetPreparedSampleDataTableExcelBookPathForTest(venmUnitTestPreparedSampleDataTable)
'
'    Set objBook = GetWorkbookAndPrepareForUnitTest(strBookPath)
'
'    Set objSheet = GetNewWorksheetAfterAllExistedSheetsFromBook(objBook)
'
'    objSheet.Name = GetSheetNameOfPreparedSampleDataTableExcelBookForTest(venmUnitTestPreparedSampleDataTable)
'
'    OutputVarTableToSheet objSheet, rvarDataTable, robjFieldTitles, mfobjGetDataTableSheetFormatterOfPreparedSampleType(venmUnitTestPreparedSampleDataTable)
'
'    DeleteDummySheetWhenItExists objBook
'
'
'    Set mfobjOutputExcelBookFromSampleDataTable = objSheet
'End Function
'
'
''''
''''
''''
'Private Function mfobjGetDataTableSheetFormatterOfPreparedSampleType(ByVal venmUnitTestPreparedSampleDataTable As UnitTestPreparedSampleDataTable) As DataTableSheetFormatter
'
'    Dim objDataTableSheetFormatter As DataTableSheetFormatter
'
'    Set objDataTableSheetFormatter = New DataTableSheetFormatter
'
'    With objDataTableSheetFormatter
'
'        Select Case venmUnitTestPreparedSampleDataTable
'
'            Case UnitTestPreparedSampleDataTable.BasicSampleDT
'
'                Set .FieldTitleToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("FixedString,12,RowNumber,8,FlowerTypeSample,19,RandomInteger,15")
'
'            Case UnitTestPreparedSampleDataTable.IncludeFictionalProductionNumberSampleDT
'
'                Set .FieldTitleToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("RowNumber,8,RealNumber,10,FishType,19,FictionalProductionSerialNumber,15")
'
'            Case UnitTestPreparedSampleDataTable.FishTypeSampleMasterDT
'
'                Set .FieldTitleToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("Index,10,FishTypeEng,12,FishTypeJpn,12,RandAlpha,12")
'
'            Case UnitTestPreparedSampleDataTable.FlowerTypeSampleMasterDT
'
'                Set .FieldTitleToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("Index,10,FlowerTypeEng,12,FlowerTypeJpn,12,RandAlpha,12")
'
'        End Select
'
'        .FieldTitleInteriorType = ColumnNameInteriorOfGatheredColTable
'
'        .RecordBordersType = RecordCellsGrayAll
'
'    End With
'
'    Set mfobjGetDataTableSheetFormatterOfPreparedSampleType = objDataTableSheetFormatter
'End Function
'
'
'
''**---------------------------------------------
''** About DataTableSheetFormatter for sample data-tables
''**---------------------------------------------
'Private Function mfobjGetDataTableFormatterForSample01() As DataTableSheetFormatter
'
'    Dim objDataTableFormatter As DataTableSheetFormatter
'
'    Set objDataTableFormatter = New DataTableSheetFormatter
'
'    With objDataTableFormatter
'
'        Set .FieldTitleToColumnWidthDic = GetPreparedSampleDataTableFieldTitleToColumnWidthDicForSanityTest(BasicSampleDT)
'    End With
'
'    Set mfobjGetDataTableFormatterForSample01 = objDataTableFormatter
'End Function
'
'Private Function mfobjGetDataTableFormatterForSample02() As DataTableSheetFormatter
'
'    Dim objDataTableFormatter As DataTableSheetFormatter
'
'    Set objDataTableFormatter = New DataTableSheetFormatter
'
'    With objDataTableFormatter
'
'        Set .FieldTitleToColumnWidthDic = GetPreparedSampleDataTableFieldTitleToColumnWidthDicForSanityTest(IncludeFictionalProductionNumberSampleDT)
'    End With
'
'    Set mfobjGetDataTableFormatterForSample02 = objDataTableFormatter
'End Function
'
''**---------------------------------------------
''** About addtional logging onto Excel.Worksheet
''**---------------------------------------------
'Private Sub msubAddCreatingSapmleDataTableLog(ByVal vobjSheet As Worksheet, _
'        Optional ByVal vdblElapsedSecondTime As Double = 0#)
'
'
'    Dim intRowsCount As Long, intColumnsCount As Long, strLoggedTimeStamp As String
'    Dim strLog As String
'
'    GetCountsOfRowsColumnsFromSheet vobjSheet, intRowsCount, intColumnsCount, strLoggedTimeStamp
'
'    strLog = "Created sample fictional data-table log" & vbNewLine
'
'    strLog = strLog & "Created time: " & strLoggedTimeStamp & vbNewLine
'
'    If vdblElapsedSecondTime > 0# Then
'
'        strLog = strLog & "Creation elapsed time: " & Format(vdblElapsedSecondTime, "0.000") & " [s]" & vbNewLine
'    End If
'
'    strLog = strLog & "Rows Count: " & intRowsCount & vbNewLine
'
'    strLog = strLog & "Columns Count: " & intColumnsCount
'
'    AddAutoShapeGeneralMessage vobjSheet, strLog, AutoShapeMessageFillPatternType.XlShapeTextLogInteriorFillDefault
'End Sub
'''--VBA_Code_File--<SolveSavePathForXl.bas>--
'Attribute VB_Name = "SolveSavePathForXl"
''
''   utilities to solute this VB project system common paths for Excel objects.
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on Excel
''       Dependent on WinRegStorageUNCPath.bas, InitRegParamOfStorageUNCPath.bas
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Sat,  5/Aug/2023    Kalmclaeyd Tarclanus    Separated from SolveSavePathGeneral.bas
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''**---------------------------------------------
''** Considering removable-disk
''**---------------------------------------------
''''
''''
''''
'Public Function GetTemporaryOutputBookDirWithConsideringRemovableDiskFromBook(ByVal vobjAnotherBook As Excel.Workbook) As String
'
'    GetTemporaryOutputBookDirWithConsideringRemovableDiskFromBook = GetTemporaryOutputBookDirWithConsideringRemovableDisk(vobjAnotherBook.FullName)
'End Function
'
''''
''''
''''
'Public Function GetTemporaryOutputBookDirWithConsideringRemovableDisk(ByVal vstrAnotherInputBookPath As String) As String
'
'    Dim objDrive As Scripting.Drive, strOutputTemporaryBookDir As String
'
'    With New Scripting.FileSystemObject
'
'        strOutputTemporaryBookDir = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks"
'
'        If .GetParentFolderName(vstrAnotherInputBookPath) <> "" Then
'
'            Set objDrive = .GetDrive(.GetDriveName(vstrAnotherInputBookPath))
'
'            If objDrive.DriveType = Removable Then
'
'                strOutputTemporaryBookDir = Replace(strOutputTemporaryBookDir, .GetDriveName(strOutputTemporaryBookDir), .GetDriveName(vstrAnotherInputBookPath))
'            End If
'        End If
'    End With
'
'    GetTemporaryOutputBookDirWithConsideringRemovableDisk = strOutputTemporaryBookDir
'End Function
'
'
'
''**---------------------------------------------
''** common path solutions
''**---------------------------------------------
''''
'''' solute path
''''
'Public Sub GetInstaneouslyDecidedTemporaryBookAndPath(ByRef robjBook As Excel.Workbook, ByRef rstrPath As String, ByVal vstrTemporaryDirectoryName As String, ByVal vstrFileBaseName As String, ByVal vstrRootDir As String)
'
'    Dim strTemporaryDir As String
'
'    strTemporaryDir = vstrRootDir & "\" & vstrTemporaryDirectoryName
'
'    ForceToCreateDirectory strTemporaryDir
'
'    rstrPath = strTemporaryDir & "\" & vstrFileBaseName & ".xlsx"
'
'    Set robjBook = GetWorkbookAndPrepareForUnitTest(rstrPath)
'End Sub
'
''''
''''
''''
'Public Sub GetInstaneouslyDecidedLogBookAndPathWithDate(ByRef robjBook As Excel.Workbook, ByRef rstrPath As String, ByVal vstrTemporaryDirectoryName As String, ByVal vstrFileBaseName As String, ByVal vudtDate As Date, ByVal vstrRootDir As String)
'
'    Dim strTemporaryDir As String
'
'    strTemporaryDir = vstrRootDir & "\" & vstrTemporaryDirectoryName
'
'    ForceToCreateDirectory strTemporaryDir
'
'    rstrPath = strTemporaryDir & "\" & vstrFileBaseName & "_" & Format(vudtDate, "mmdd") & ".xlsx"
'
'    Set robjBook = GetWorkbookAndPrepareForUnitTest(rstrPath)
'End Sub
'
''''
''''
''''
'Public Sub GetInstaneouslyDecidedLoggingBookAndPath(ByRef robjBook As Workbook, ByRef rstrPath As String, ByVal vstrLoggingDirectoryName As String, ByVal vstrFileBaseName As String, ByVal vstrRootDir As String)
'
'    Dim strLoggingDir As String
'
'    strLoggingDir = vstrRootDir & "\" & Format(Now(), "yyyymmdd") & "\" & vstrTemporaryDirectoryName
'
'    ForceToCreateDirectory strLoggingDir
'
'    rstrPath = strLoggingDir & "\" & vstrFileBaseName & "_" & Format(Now(), "yyyymmdd") & ".xlsx"
'
'    Set robjBook = GetWorkbookAndPrepareForUnitTest(rstrPath)
'End Sub
'
'
'Public Sub SaveAsInstaneouslyDecidedLoggingBookAndPath(ByVal vobjBook As Workbook, ByVal vstrLoggingDirectoryName As String, ByVal vstrFileBaseName As String, ByVal vstrRootDir As String)
'
'    Dim strLoggingDir As String, strPath As String
'
'    strLoggingDir = vstrRootDir & "\" & Format(Now(), "yyyymmdd") & "\" & vstrLoggingDirectoryName
'
'    ForceToCreateDirectory strLoggingDir
'
'    strPath = FindNewNameWhenAlreadyExist(strLoggingDir & "\" & vstrFileBaseName & "_" & Format(Now(), "yyyymmdd") & ".xlsx")
'
'    ForceToSaveAsBookWithoutDisplayAlert vobjBook, strPath
'
'    CloseBookWithoutDisplayAlerts vobjBook
'
'    GetWorkbookIfItExists strPath
'End Sub
'
'
'''--VBA_Code_File--<UTfCurrentLocalize.bas>--
'Attribute VB_Name = "UTfCurrentLocalize"
''
''   Design simply to localize text with dependent on ADO
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on ADO
''       Dependent on Excel for viewing the original key-value sheet
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Sat,  5/Aug/2023    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
'
''///////////////////////////////////////////////
''/// Control VBE windows
''///////////////////////////////////////////////
''**---------------------------------------------
''** Open relative VBE code-panes
''**---------------------------------------------
'Private Sub OpenVBECodePaneRelativeToUTfCurrentLocalize()
'
'    ' Focusing on this procdure, please press [F5] key to start this. Therefore this scope is private.
'
'    On Error Resume Next ' If the 'ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma' isn't included in the VBA project, the following causes no syntax error.
'
'    Application.Run "ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma", "CurrentLocalize,ADOParameters,ADOConStrToolsForExcelBookBase,LocalizationADOConnector"
'End Sub
'
'
''///////////////////////////////////////////////
''/// Sanity tests
''///////////////////////////////////////////////
''''
'''' Open related Key-Value table Excel book for viewing
''''
'Private Sub msubSanityTestToOpenStringKeyValuesTableBook()
'
'    Dim strModuleName As String
'
'    strModuleName = "UErrorADOSQLForm"
'
'    GetStringKeyValuesTableVbaLocalizationBook strModuleName
'
'    ShowIDECodePaneFromModuleNameAndVBProjectObject strModuleName, GetCurrentOfficeFileObject().VBProject
'End Sub
'
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
''''
'''' Open the Excel book which include the base Key-Value table
''''
'Private Function GetStringKeyValuesTableVbaLocalizationBook(ByVal vstrModuleName As String) As Excel.Workbook
'
'    Dim objBook As Excel.Workbook, strPath As String
'
'    Set objBook = Nothing
'
'    strPath = GetKeyValuesVbaLocalizationBookPathForEachModule(vstrModuleName)
'
'    If strPath <> "" Then
'
'        Set objBook = GetWorkbook(strPath)
'    End If
'
'    Set GetStringKeyValuesTableVbaLocalizationBook = objBook
'End Function
'
'''--VBA_Code_File--<AddAutoLogShapesForAdo.bas>--
'Attribute VB_Name = "AddAutoLogShapesForAdo"
''
''   Add auto-shape log onto the specified Excel sheet for ADO SQL results
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on Excel
''       Dependent on SQLResult.cls which dependent on LoadedADOConnectionSetting.cls
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Thu,  8/Jun/2023    Kalmclaeyd Tarclanus    Separated from AddAutoLogShapes.bas
''
'
'Option Explicit
'
'
''///////////////////////////////////////////////
''/// Declarations
''///////////////////////////////////////////////
'Private mobjFileTagOrDirectoryTagRegExp As VBScript_RegExp_55.RegExp
'
'
''**---------------------------------------------
''** Prepare RegExp objects
''**---------------------------------------------
''''
''''
''''
'Private Function mfobjGetFileTagOrDirectoryTagRegExp() As VBScript_RegExp_55.RegExp
'
'    If mobjFileTagOrDirectoryTagRegExp Is Nothing Then
'
'        Set mobjFileTagOrDirectoryTagRegExp = New VBScript_RegExp_55.RegExp
'
'        With mobjFileTagOrDirectoryTagRegExp
'
'            .Pattern = "<File Name>.*xls|<CSV Directory>"
'        End With
'    End If
'
'    Set mfobjGetFileTagOrDirectoryTagRegExp = mobjFileTagOrDirectoryTagRegExp
'End Function
'
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''''
'''' the following is dependent on SQLResult object
''''
'Public Sub AddAutoShapeRdbSqlQueryLog(ByVal vobjSheet As Excel.Worksheet, _
'        ByVal vobjSQLRes As SQLResult, _
'        ByVal vobjDataTableSheetRangeAddresses As DataTableSheetRangeAddresses, _
'        ByVal vobjHeaderInsertTexts As Collection, _
'        Optional ByVal vsngFontSize As Single = 9.5)
'
'    Dim strDisplayText As String, objAutoShapeAdjuster As ASWindowPositionAdjuster
'    Dim objShape As Excel.Shape, varText As Variant
'
'
'    Set objAutoShapeAdjuster = New ASWindowPositionAdjuster
'
'    If Not vobjSQLRes Is Nothing Then
'
'        Set objShape = GetShapeAfterCreateLogTextShapeOfSpecifiedCharacteristics(vobjSheet, objAutoShapeAdjuster, XlShapeTextLogOfUsedActualSQL, vobjSQLRes.SQL, XlShapeTextLogInteriorFillDefault, vsngFontSize + 1#)
'
'
'        strDisplayText = mfstrGetSQLSelectedTableLogOfSQLResult(vobjSQLRes, vobjDataTableSheetRangeAddresses)
'
'        Set objShape = GetShapeAfterCreateLogTextShapeOfSpecifiedCharacteristics(vobjSheet, objAutoShapeAdjuster, XlShapeTextLogOfElapsedTimeAndRowCountAppearance, strDisplayText, XlShapeTextLogInteriorFillDefault, vsngFontSize)
'    End If
'
'    If Not vobjHeaderInsertTexts Is Nothing Then
'
'        If vobjHeaderInsertTexts.Count > 0 Then
'
'            With mfobjGetFileTagOrDirectoryTagRegExp()
'
'                For Each varText In vobjHeaderInsertTexts
'
'                    If .Test(varText) Then
'
'                        ' This is either an Excel source book path or a source CSV directory path
'
'                        strDisplayText = varText
'
'                        Set objShape = GetShapeAfterCreateLogTextShapeOfSpecifiedCharacteristics(vobjSheet, objAutoShapeAdjuster, XlShapeTextLogOfInputSourceTableFilePath, strDisplayText, XlShapeTextLogInteriorFillDefault, vsngFontSize)
'
'                        msubChangeAutoShapeTargetTextFontsAboutInputSourceTableFilePath objShape
'                    Else
'                        ' Shape-general message
'
'                        AddAutoShapeGeneralMessage vobjSheet, varText, AutoShapeMessageFillPatternType.XlShapeTextLogInteriorFillDefault, objAutoShapeAdjuster
'                    End If
'                Next
'            End With
'        End If
'    End If
'End Sub
'
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
''''
''''
''''
'Private Function mfstrGetSQLSelectedTableLogOfSQLResult(ByRef robjSQLResult As SQLResult, ByVal vobjDataTableSheetRangeAddresses As DataTableSheetRangeAddresses) As String
'
'    Dim strDisplayText As String
'
'    With robjSQLResult
'
'        strDisplayText = GetTextOfStrKeyAddAutoLogShapesSqlExecutionElapsedTime() & ": " & Format(CDbl(.SQLExeElapsedTime) / 1000#, "0.000") & " " & GetTextOfStrKeyAddAutoLogShapesUnitSecond() & vbCrLf
'
'        strDisplayText = strDisplayText & GetTextOfStrKeyAddAutoLogShapesExcelSheetProcessingElapsedTime() & ": " & Format(CDbl(.ExcelSheetOutputElapsedTime) / 1000#, "0.000") & " " & GetTextOfStrKeyAddAutoLogShapesUnitSecond() & vbCrLf
'
'        strDisplayText = strDisplayText & GetTextOfStrKeyAddAutoLogShapesTableRecordCount() & ": " & CStr(.TableRecordCount) & vbCrLf
'
'        strDisplayText = strDisplayText & GetTextOfStrKeyAddAutoLogShapesTableFieldCount() & ": " & CStr(.TableFieldCount) & vbCrLf
'
'        If Not vobjDataTableSheetRangeAddresses Is Nothing Then
'
'            With vobjDataTableSheetRangeAddresses
'
'                strDisplayText = strDisplayText & "Records address" & ": " & CStr(.RecordAreaRangeAddress) & vbCrLf
'
'                strDisplayText = strDisplayText & "Field-titles address" & ": " & CStr(.FieldTitlesRangeAddress) & vbCrLf
'            End With
'        End If
'
'        strDisplayText = strDisplayText & GetTextOfStrKeyAddAutoLogShapesSqlExecutedTime() & ": " & Format(.SQLExeTime, "ddd, yyyy/m/d hh:mm:dd")
'    End With
'
'    mfstrGetSQLSelectedTableLogOfSQLResult = strDisplayText
'End Function
'
'
'
''''
''''
''''
'Private Sub msubChangeAutoShapeTargetTextFontsAboutInputSourceTableFilePath(ByRef robjShape As Excel.Shape)
'
'    Dim objDecorationTargetTextSettings As Collection
'
'    Set objDecorationTargetTextSettings = New Collection
'
'    With objDecorationTargetTextSettings
'
'        .Add "<File Name>;9.5;trRGBBeigeWhiteFontColor"
'
'        .Add "<Directory>;9.5;trRGBBeigeWhiteFontColor"
'
'        .Add "<CSV Directory>;9.5;trRGBBeigeWhiteFontColor"
'    End With
'
'    ChangeAutoShapeTargetTextFonts robjShape, objDecorationTargetTextSettings
'End Sub
'
'''--VBA_Code_File--<ADOSheetOut.bas>--
'Attribute VB_Name = "ADOSheetOut"
''
''   create a SQL result Excel sheet by using ADODB.Recordset
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on both ADO and Excel
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Tue, 10/May/2022    Kalmclaeyd Tarclanus    Improved
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Declarations
''///////////////////////////////////////////////
'Private Const mstrModuleName As String = "ADOSheetOut"
'
''**---------------------------------------------
''** Key-Value cache preparation for ADOSheetOut
''**---------------------------------------------
'Private mobjStrKeyValueADOSheetOutDic As Scripting.Dictionary
'
'Private mblnIsStrKeyValueADOSheetOutDicInitialized As Boolean   ' Uninitialized boolean value is false in VBA specification.
'
'
''///////////////////////////////////////////////
''/// Control VBE windows
''///////////////////////////////////////////////
''**---------------------------------------------
''** Open relative VBE code-panes
''**---------------------------------------------
'Private Sub OpenVBECodePaneRelativeToADOOut()
'
'    ' Focusing on this procdure, please press [F5] key to start this. Therefore this scope is private.
'
'    On Error Resume Next ' If the 'ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma' isn't included in the VBA project, the following causes no syntax error
'
'    Application.Run "ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma", "ADOLogTextSheetOut,UTfDataTableSheetOut,UTfDataTableTextOut,DataTableSheetFormatter"
'End Sub
'
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''**---------------------------------------------
''** Output Recordset data-table to Cells on sheet
''**---------------------------------------------
''''
'''' output RecordSet object to worksheet (new-type), <CQRS: Query method>
''''
'Public Function OutputRecordsetToCellsOnSheet(ByVal vobjSheet As Excel.Worksheet, _
'        ByVal vobjRSet As ADODB.Recordset, _
'        ByRef robjSQLRes As SQLResult, _
'        ByVal vobjADOSheetFormatter As ADOSheetFormatter, _
'        ByRef renmRdbConnectionInformationFlag As RdbConnectionInformationFlag, _
'        Optional ByVal vblnAllowToGetDataTableSheetRangeAddresses As Boolean = True) As DataTableSheetRangeAddresses
'
'    Dim intColumnsCountOfRecordset As Long, intRowsCountOfRecordset As Long, objDataTableSheetRangeAddresses As DataTableSheetRangeAddresses, objFieldTitlesCol As Collection
'
'
'    With vobjADOSheetFormatter
'
'        .SetSheetFormatBeforeTableOut (Not robjSQLRes Is Nothing)
'
'        .PreparationForRecordSet vobjSheet, vobjRSet, robjSQLRes
'
'        intColumnsCountOfRecordset = GetFieldTitlesCountAfterOutputRecordsetFieldTitleToCellsOnSheet(vobjSheet, vobjRSet, vobjADOSheetFormatter)
'
'        intRowsCountOfRecordset = OutputOnlyRecordsetDataTableToCellsOnSheet(vobjSheet, vobjRSet, robjSQLRes, vobjADOSheetFormatter, mfblnIsAvoidingToUseCopyFromRecordsetNeeded(renmRdbConnectionInformationFlag))
'
'        If vblnAllowToGetDataTableSheetRangeAddresses Then
'
'            Set objDataTableSheetRangeAddresses = GetDataTableSheetRangeAddressesFromDTIndexesAndFieldsCountAndDTFormatter(vobjSheet, intRowsCountOfRecordset, intColumnsCountOfRecordset, intColumnsCountOfRecordset, vobjADOSheetFormatter.DataTableSheetFormattingInterface)
'        End If
'
'        .SetSheetFormatAfterTableOut vobjSheet
'
'        .ResultOutForAdoSqlExecution vobjSheet, robjSQLRes, RdbSqlQueryType, objDataTableSheetRangeAddresses
'    End With
'
'    Set OutputRecordsetToCellsOnSheet = objDataTableSheetRangeAddresses
'End Function
'
''''
''''
''''
'Private Function mfblnIsAvoidingToUseCopyFromRecordsetNeeded(ByRef renmRdbConnectionInformationFlag As RdbConnectionInformationFlag) As Boolean
'
'    Dim blnAllowToAvoidToUseCopyFromRecordsetMethod As Boolean
'
'
'    blnAllowToAvoidToUseCopyFromRecordsetMethod = False
'
'    If (renmRdbConnectionInformationFlag And RdbConnectionInformationFlag.AdoOdbcToSqLiteRdbFlag) = RdbConnectionInformationFlag.AdoOdbcToSqLiteRdbFlag Then
'
'        ' For SQLite database, SQL "SELECT name FROM sqlite_master WHERE type = 'table'" doesn't work when the Range.CopyFromRecordset is used.
'
'        blnAllowToAvoidToUseCopyFromRecordsetMethod = True
'    End If
'
'    mfblnIsAvoidingToUseCopyFromRecordsetNeeded = blnAllowToAvoidToUseCopyFromRecordsetMethod
'End Function
'
''''
''''
''''
'Public Function GetFieldTitlesCountAfterOutputRecordsetFieldTitleToCellsOnSheet(ByVal vobjSheet As Excel.Worksheet, ByVal vobjRSet As ADODB.Recordset, ByVal vobjADOSheetFormatter As ADOSheetFormatter) As Long
'
'    Dim intColumnsCountOfRecordset As Long
'
'    intColumnsCountOfRecordset = vobjRSet.Fields.Count
'
'    With vobjADOSheetFormatter
'
'        If .AllowToShowFieldTitle Then
'
'            OutputRecordsetFieldTitleToCellsOnSheet vobjSheet, vobjRSet, .FieldTitlesRowIndex, .TopLeftColumnIndex
'        End If
'    End With
'
'    GetFieldTitlesCountAfterOutputRecordsetFieldTitleToCellsOnSheet = intColumnsCountOfRecordset
'End Function
'
''''
'''' data-table sheet out with Recordset object
''''
'Public Function OutputOnlyRecordsetDataTableToCellsOnSheet(ByVal vobjSheet As Excel.Worksheet, _
'        ByVal vobjRSet As ADODB.Recordset, _
'        ByRef robjSQLRes As SQLResult, _
'        ByVal vobjADOSheetFormatter As ADOSheetFormatter, _
'        Optional ByVal vblnAllowToAvoidToUseCopyFromRecordsetMethod As Boolean = False) As Long
'
'
'    Dim intColumnMax As Long, objRange As Excel.Range, objTopLeftDataTableRange As Excel.Range, intRowsCountOfRecords As Long
'
'    Dim objDummyDTCol As Collection
'
'
'    intRowsCountOfRecords = 0
'
'    With vobjSheet
'
'        With vobjADOSheetFormatter
'
'            Set objTopLeftDataTableRange = vobjSheet.Range(ConvertXlColumnIndexToLetter(.TopLeftColumnIndex) & CStr(.FieldTitlesRowIndex + 1))
'        End With
'
'        If vobjRSet.State <> ADODB.ObjectStateEnum.adStateClosed Then
'
'            If vblnAllowToAvoidToUseCopyFromRecordsetMethod Then
'
'                If Not vobjRSet.BOF Then
'
'                    vobjRSet.MoveFirst
'                End If
'
'                intRowsCountOfRecords = MoveNextNormalCopyFromRecordSet(objDummyDTCol, objTopLeftDataTableRange, vobjRSet)
'            Else
'                intColumnMax = vobjRSet.Fields.Count
'
'                ' ordinary-code, which use the Excel.Range.CopyFromRecordset method
'
'                With vobjADOSheetFormatter
'
'                    objTopLeftDataTableRange.CopyFromRecordset vobjRSet
'
'                    With vobjSheet.Range(ConvertXlColumnIndexToLetter(.TopLeftColumnIndex) & CStr(.FieldTitlesRowIndex + 1)).CurrentRegion
'
'                        intRowsCountOfRecords = .Rows.Count + .Row - 1 - vobjADOSheetFormatter.FieldTitlesRowIndex
'                    End With
'                End With
'            End If
'
'            If Not robjSQLRes Is Nothing Then
'
'                If Not vobjADOSheetFormatter.SupportedPropertyRecordCount Then
'
'                    robjSQLRes.TableRecordCount = intRowsCountOfRecords
'                End If
'            Else
'                Debug.Print "[ADOSheetOut.bas] OutputOnlyRecordsetDataTableToCellsOnSheet, the robjSQLRes is Nothing"
'            End If
'        End If
'    End With
'
'    OutputOnlyRecordsetDataTableToCellsOnSheet = intRowsCountOfRecords
'End Function
'
''''
''''
''''
'Private Function mfintGetEstimatedRowsCountOfRecordsetByCurrentRegion(ByRef robjSQLRes As SQLResult, _
'        ByRef robjSheet As Excel.Worksheet, _
'        ByRef robjADOSheetFormatter As ADOSheetFormatter, _
'        ByRef robjRSet As ADODB.Recordset)
'
'
'    Dim intRowsCountOfRecordset As Long, objTopLeftDataTableRange As Excel.Range
'
'    With robjADOSheetFormatter
'
'        If Not .SupportedPropertyRecordCount Then
'
'            If .AllowToShowFieldTitle Then
'
'                Set objTopLeftDataTableRange = robjSheet.Range(ConvertXlColumnIndexToLetter(.TopLeftColumnIndex) & CStr(.FieldTitlesRowIndex)).CurrentRegion
'            Else
'                Set objTopLeftDataTableRange = robjSheet.Range(ConvertXlColumnIndexToLetter(.TopLeftColumnIndex) & CStr(.FieldTitlesRowIndex + 1)).CurrentRegion
'            End If
'
'            intRowsCountOfRecordset = objTopLeftDataTableRange.Row + objTopLeftDataTableRange.Rows.Count - 1 - robjADOSheetFormatter.FieldTitlesRowIndex
'        Else
'            intRowsCountOfRecordset = robjRSet.RecordCount
'        End If
'    End With
'
'    If Not robjSQLRes Is Nothing Then
'
'        robjSQLRes.TableRecordCount = intRowsCountOfRecordset
'    End If
'
'    mfintGetEstimatedRowsCountOfRecordsetByCurrentRegion = intRowsCountOfRecordset
'End Function
'
'
'
''''
''''
''''
'Public Function OutputRecordsetFieldTitleToCellsOnSheet(ByVal vobjSheet As Excel.Worksheet, _
'        ByVal vobjRSet As ADODB.Recordset, _
'        Optional ByVal vintFieldTitlesRowIndex As Long = 1, _
'        Optional ByVal vintTopLeftColumnIndex As Long = 1) As Collection
'
'
'    Dim objField As ADOR.Field, objFieldTitlesCol As Collection
'
'    Set objFieldTitlesCol = New Collection
'
'    For Each objField In vobjRSet.Fields
'
'        objFieldTitlesCol.Add objField.Name
'    Next
'
'    OutputFieldTitlesToCellsOnSheet vobjSheet, objFieldTitlesCol, vintFieldTitlesRowIndex, vintTopLeftColumnIndex
'
'    Set OutputRecordsetFieldTitleToCellsOnSheet = objFieldTitlesCol
'End Function
'
'
'
''''
'''' Substitute this for Range.CopyFromRecordSet
''''
'''' because the CopyFromRecordSet causes often some errors which is hard to understand
''''
'Public Function MoveNextNormalCopyFromRecordSet(ByRef robjOutputDTCol As Collection, ByVal vobjRecordTopLeftRange As Excel.Range, _
'        ByVal vobjRSet As ADODB.Recordset, _
'        Optional ByVal vstrSuppressingColumnsDelimitedComma As String = "") As Long
'
'
'    Dim objSheet As Excel.Worksheet, intRowIdx As Long, intColumnIdx As Long
'    Dim intRecordTopLeftRowIndex As Long, intRecordTopLeftColumnIndex As Long, intColumnsCountOfDataTable As Long
'    Dim i As Long
'
'    Dim objDTCol As Collection
'    Dim objUnionRange As Excel.Range
'    Dim intRowsCountOfRecordset As Long
'
'    Set objSheet = vobjRecordTopLeftRange.Worksheet
'
'
'    intRowIdx = intRecordTopLeftRowIndex
'
'    With vobjRecordTopLeftRange
'
'        intRecordTopLeftRowIndex = .Row
'
'        intRecordTopLeftColumnIndex = .Column
'    End With
'
'
'    intColumnsCountOfDataTable = vobjRSet.Fields.Count
'
'    NormalConvertRSetToCollectionAndGetStringRanges objDTCol, objUnionRange, vobjRSet, objSheet, intRecordTopLeftRowIndex, intRecordTopLeftColumnIndex, vstrSuppressingColumnsDelimitedComma
'
'    Set robjOutputDTCol = objDTCol
'
'    On Error Resume Next
'
'    If Not objUnionRange Is Nothing Then
'
'        objUnionRange.NumberFormatLocal = "@"
'    End If
'
'    msubNormalOutputDTCol objDTCol, intColumnsCountOfDataTable, objSheet, intRecordTopLeftRowIndex, intRecordTopLeftColumnIndex
'
'    intRowsCountOfRecordset = objDTCol.Count
'
'    On Error GoTo 0
'
'    MoveNextNormalCopyFromRecordSet = intRowsCountOfRecordset
'End Function
'
''''
'''' because the CopyFromRecordSet causes often some errors which is hard to understand
''''
'Public Function MoveNextNormalCopyFromDTCol(ByVal vobjRecordTopLeftRange As Excel.Range, _
'        ByVal vobjInputDTCol As Collection, _
'        Optional ByVal vstrSuppressingColumnsDelimitedComma As String = "") As Long
'
'
'    Dim objSheet As Excel.Worksheet, intRowIdx As Long, intColumnIdx As Long
'    Dim intRecordTopLeftRowIndex As Long, intRecordTopLeftColumnIndex As Long, intColumnsCountOfDataTable As Long
'    Dim i As Long
'
'    Dim objOutputDTCol As Collection
'    Dim objUnionRange As Excel.Range
'    Dim intRowsCountOfRecordset As Long
'
'    Set objSheet = vobjRecordTopLeftRange.Worksheet
'
'
'    intRowIdx = intRecordTopLeftRowIndex
'
'    With vobjRecordTopLeftRange
'
'        intRecordTopLeftRowIndex = .Row
'
'        intRecordTopLeftColumnIndex = .Column
'    End With
'
'
'
'
'    NormalConvertDTColToCollectionAndGetStringRanges objOutputDTCol, objUnionRange, vobjInputDTCol, objSheet, intRecordTopLeftRowIndex, intRecordTopLeftColumnIndex, vstrSuppressingColumnsDelimitedComma
'
'    On Error Resume Next
'
'    If Not objUnionRange Is Nothing Then
'
'        objUnionRange.NumberFormatLocal = "@"
'    End If
'
'    intColumnsCountOfDataTable = objOutputDTCol.Item(1).Count
'
'    msubNormalOutputDTCol objOutputDTCol, intColumnsCountOfDataTable, objSheet, intRecordTopLeftRowIndex, intRecordTopLeftColumnIndex
'
'    intRowsCountOfRecordset = objOutputDTCol.Count
'
'    On Error GoTo 0
'
'    MoveNextNormalCopyFromDTCol = intRowsCountOfRecordset
'End Function
'
'
''''
''''
''''
'Private Sub msubNormalOutputDTCol(ByVal vobjDTCol As Collection, _
'        ByVal vintColumnsCountOfDataTable As Long, _
'        ByVal vobjSheet As Excel.Worksheet, _
'        Optional ByVal vintRecordTopLeftRowIndex As Long = 1, _
'        Optional ByVal vintRecordTopLeftColumnIndex As Long = 1)
'
'    Dim objRange As Excel.Range, varValues As Variant
'
'    Set objRange = vobjSheet.Cells(vintRecordTopLeftRowIndex, vintRecordTopLeftColumnIndex)
'
'    GetRCValuesFromRCCollection varValues, vobjDTCol
'
'    objRange.Resize(vobjDTCol.Count, vintColumnsCountOfDataTable).Value = varValues
'End Sub
'
'
'
''''
''''
''''
'Public Sub NormalConvertDTColToCollectionAndGetStringRanges(ByRef robjOutputDTCol As Collection, _
'        ByRef robjStringUnionRange As Excel.Range, _
'        ByRef robjInputDTCol As Collection, _
'        ByRef robjSheet As Excel.Worksheet, _
'        ByRef rintRecordTopLeftRowIndex As Long, _
'        ByRef rintRecordTopLeftColumnIndex As Long, _
'        Optional ByVal vstrSuppressingColumnsDelimitedComma As String = "")
'
'    On Error Resume Next
'
'    Dim i As Long, objRowCol As Collection, varInputRowCol As Variant, objInputRowCol As Collection, varInputItem As Variant
'    Dim intColumnIdx As Long, intRowIdx As Long, objSuppressingColumnsDic As Scripting.Dictionary, varColumn As Variant
'
'    Dim intColumnDecrement As Long, blnContinueToAddValue As Boolean
'
'    intRowIdx = rintRecordTopLeftRowIndex
'
'
'
'    SetUpSuppressingColumnsDic objSuppressingColumnsDic, vstrSuppressingColumnsDelimitedComma
'
'
'    Set robjOutputDTCol = New Collection
'
'    Set robjStringUnionRange = Nothing
'
'    For Each varInputRowCol In robjInputDTCol
'
'        Set objRowCol = New Collection
'
'        intColumnDecrement = 0
'
'        i = 0
'
'        Set objInputRowCol = varInputRowCol
'
'        For Each varInputItem In objInputRowCol
'
'            intColumnIdx = rintRecordTopLeftColumnIndex + i
'
'            blnContinueToAddValue = True
'
'            If vstrSuppressingColumnsDelimitedComma <> "" Then
'
'                If objSuppressingColumnsDic.Exists(i + 1) Then
'
'                    blnContinueToAddValue = False
'
'                    intColumnDecrement = intColumnDecrement + 1
'                End If
'            End If
'
'            If blnContinueToAddValue Then
'
'                SetNormalRowColValueAndStringUnionRange robjSheet, objRowCol, varInputItem, robjStringUnionRange, intRowIdx, intColumnIdx, intColumnDecrement
'            End If
'
'            i = i + 1
'        Next
'
'        robjOutputDTCol.Add objRowCol
'
'        intRowIdx = intRowIdx + 1
'    Next
'
'
'    On Error GoTo 0
'End Sub
'
''''
''''
''''
'Public Sub NormalConvertRSetToCollectionAndGetStringRanges(ByRef robjDTCol As Collection, _
'        ByRef robjStringUnionRange As Excel.Range, _
'        ByRef robjRSet As ADODB.Recordset, _
'        ByRef robjSheet As Excel.Worksheet, _
'        ByRef rintRecordTopLeftRowIndex As Long, _
'        ByRef rintRecordTopLeftColumnIndex As Long, _
'        Optional ByVal vstrSuppressingColumnsDelimitedComma As String = "")
'
'    On Error Resume Next
'
'    Dim objField As ADOR.Field, i As Long, objRowCol As Collection
'    Dim intColumnIdx As Long, intRowIdx As Long, objSuppressingColumnsDic As Scripting.Dictionary, varColumn As Variant
'
'    Dim intColumnDecrement As Long, blnContinueToAddValue As Boolean
'
'    intRowIdx = rintRecordTopLeftRowIndex
'
'    intColumnDecrement = 0
'
'    SetUpSuppressingColumnsDic objSuppressingColumnsDic, vstrSuppressingColumnsDelimitedComma
'
'
'    Set robjDTCol = New Collection
'
'    Set robjStringUnionRange = Nothing
'
'    With robjRSet
'
'        While Not .EOF
'
'            Set objRowCol = New Collection
'
'            i = 0
'
'            For Each objField In .Fields
'
'                intColumnIdx = rintRecordTopLeftColumnIndex + i
'
'                blnContinueToAddValue = True
'
'                If vstrSuppressingColumnsDelimitedComma <> "" Then
'
'                    If objSuppressingColumnsDic.Exists(intColumnIdx) Then
'
'                        blnContinueToAddValue = False
'
'                        intColumnDecrement = intColumnDecrement + 1
'                    End If
'                End If
'
'                If blnContinueToAddValue Then
'
'                    With objField
'
'                        SetNormalRowColValueAndStringUnionRange robjSheet, objRowCol, .Value, robjStringUnionRange, intRowIdx, intColumnIdx, intColumnDecrement
'                    End With
'                End If
'
'                i = i + 1
'            Next
'
'            robjDTCol.Add objRowCol
'
'            intRowIdx = intRowIdx + 1
'
'            .MoveNext
'        Wend
'    End With
'
'    On Error GoTo 0
'End Sub
'
''''
''''
''''
'Public Sub SetNormalRowColValueAndStringUnionRange(ByRef robjSheet As Excel.Worksheet, _
'        ByRef robjRowCol As Collection, _
'        ByRef rvarValue As Variant, _
'        ByRef robjStringUnionRange As Excel.Range, _
'        ByRef rintRowIdx As Long, _
'        ByRef rintColumnIdx As Long, _
'        ByRef rintColumnDecrement As Long)
'
'
'    robjRowCol.Add rvarValue
'
'    If IsNumeric(rvarValue) Then
'
'        If TypeName(rvarValue) = "String" Then
'
'            If StrComp(Left(rvarValue, 1), "0") = 0 Then
'
'                If robjStringUnionRange Is Nothing Then
'
'                    Set robjStringUnionRange = robjSheet.Cells(rintRowIdx, rintColumnIdx - rintColumnDecrement)
'                Else
'                    Set robjStringUnionRange = Union(robjStringUnionRange, robjSheet.Cells(rintRowIdx, rintColumnIdx - rintColumnDecrement))
'                End If
'            End If
'        End If
'    End If
'End Sub
'
'
'
''''
''''
''''
'Public Sub SetUpSuppressingColumnsDic(ByRef robjSuppressingColumnsDic As Scripting.Dictionary, Optional ByVal vstrSuppressingColumnsDelimitedComma As String = "")
'
'    Dim varColumn As Variant
'
'    If vstrSuppressingColumnsDelimitedComma <> "" Then
'
'        Set robjSuppressingColumnsDic = New Scripting.Dictionary
'
'        For Each varColumn In Split(vstrSuppressingColumnsDelimitedComma, ",")
'
'            If IsNumeric(varColumn) Then
'
'                robjSuppressingColumnsDic.Add CLng(varColumn), Null
'            End If
'        Next
'    End If
'End Sub
'
'
'
''**---------------------------------------------
''** Key-Value cache preparation for ADOSheetOut
''**---------------------------------------------
''''
'''' get string Value from STR_KEY_ADO_SHEETOUT_SQL_EXECUTION_ELAPSED_TIME
''''
'''' automatically-added for ADOSheetOut string-key-value management for standard module and class module
'Public Function GetTextOfStrKeyAdoSheetoutSqlExecutionElapsedTime() As String
'
'    If Not mblnIsStrKeyValueADOSheetOutDicInitialized Then
'
'        msubInitializeTextForADOSheetOut
'    End If
'
'    GetTextOfStrKeyAdoSheetoutSqlExecutionElapsedTime = mobjStrKeyValueADOSheetOutDic.Item("STR_KEY_ADO_SHEETOUT_SQL_EXECUTION_ELAPSED_TIME")
'End Function
'
''''
'''' get string Value from STR_KEY_ADO_SHEETOUT_EXCEL_SHEET_PROCESSING_ELAPSED_TIME
''''
'''' automatically-added for ADOSheetOut string-key-value management for standard module and class module
'Public Function GetTextOfStrKeyAdoSheetoutExcelSheetProcessingElapsedTime() As String
'
'    If Not mblnIsStrKeyValueADOSheetOutDicInitialized Then
'
'        msubInitializeTextForADOSheetOut
'    End If
'
'    GetTextOfStrKeyAdoSheetoutExcelSheetProcessingElapsedTime = mobjStrKeyValueADOSheetOutDic.Item("STR_KEY_ADO_SHEETOUT_EXCEL_SHEET_PROCESSING_ELAPSED_TIME")
'End Function
'
''''
'''' get string Value from STR_KEY_ADO_SHEETOUT_TABLE_RECORD_COUNT
''''
'''' automatically-added for ADOSheetOut string-key-value management for standard module and class module
'Public Function GetTextOfStrKeyAdoSheetoutTableRecordCount() As String
'
'    If Not mblnIsStrKeyValueADOSheetOutDicInitialized Then
'
'        msubInitializeTextForADOSheetOut
'    End If
'
'    GetTextOfStrKeyAdoSheetoutTableRecordCount = mobjStrKeyValueADOSheetOutDic.Item("STR_KEY_ADO_SHEETOUT_TABLE_RECORD_COUNT")
'End Function
'
''''
'''' get string Value from STR_KEY_ADO_SHEETOUT_TABLE_FIELD_COUNT
''''
'''' automatically-added for ADOSheetOut string-key-value management for standard module and class module
'Public Function GetTextOfStrKeyAdoSheetoutTableFieldCount() As String
'
'    If Not mblnIsStrKeyValueADOSheetOutDicInitialized Then
'
'        msubInitializeTextForADOSheetOut
'    End If
'
'    GetTextOfStrKeyAdoSheetoutTableFieldCount = mobjStrKeyValueADOSheetOutDic.Item("STR_KEY_ADO_SHEETOUT_TABLE_FIELD_COUNT")
'End Function
'
''''
'''' get string Value from STR_KEY_ADO_SHEETOUT_SQL_EXECUTED_TIME
''''
'''' automatically-added for ADOSheetOut string-key-value management for standard module and class module
'Public Function GetTextOfStrKeyAdoSheetoutSqlExecutedTime() As String
'
'    If Not mblnIsStrKeyValueADOSheetOutDicInitialized Then
'
'        msubInitializeTextForADOSheetOut
'    End If
'
'    GetTextOfStrKeyAdoSheetoutSqlExecutedTime = mobjStrKeyValueADOSheetOutDic.Item("STR_KEY_ADO_SHEETOUT_SQL_EXECUTED_TIME")
'End Function
'
''''
'''' get string Value from STR_KEY_ADO_SHEETOUT_UNIT_SECOND
''''
'''' automatically-added for ADOSheetOut string-key-value management for standard module and class module
'Public Function GetTextOfStrKeyAdoSheetoutUnitSecond() As String
'
'    If Not mblnIsStrKeyValueADOSheetOutDicInitialized Then
'
'        msubInitializeTextForADOSheetOut
'    End If
'
'    GetTextOfStrKeyAdoSheetoutUnitSecond = mobjStrKeyValueADOSheetOutDic.Item("STR_KEY_ADO_SHEETOUT_UNIT_SECOND")
'End Function
'
''''
'''' get string Value from STR_KEY_ADO_SHEETOUT_RECORDS_AFFECTED
''''
'''' automatically-added for ADOSheetOut string-key-value management for standard module and class module
'Public Function GetTextOfStrKeyAdoSheetoutRecordsAffected() As String
'
'    If Not mblnIsStrKeyValueADOSheetOutDicInitialized Then
'
'        msubInitializeTextForADOSheetOut
'    End If
'
'    GetTextOfStrKeyAdoSheetoutRecordsAffected = mobjStrKeyValueADOSheetOutDic.Item("STR_KEY_ADO_SHEETOUT_RECORDS_AFFECTED")
'End Function
'
''''
'''' get string Value from STR_KEY_ADO_SHEETOUT_COUNT_OF_SQL_DELETE
''''
'''' automatically-added for ADOSheetOut string-key-value management for standard module and class module
'Public Function GetTextOfStrKeyAdoSheetoutCountOfSqlDelete() As String
'
'    If Not mblnIsStrKeyValueADOSheetOutDicInitialized Then
'
'        msubInitializeTextForADOSheetOut
'    End If
'
'    GetTextOfStrKeyAdoSheetoutCountOfSqlDelete = mobjStrKeyValueADOSheetOutDic.Item("STR_KEY_ADO_SHEETOUT_COUNT_OF_SQL_DELETE")
'End Function
'
''''
'''' get string Value from STR_KEY_ADO_SHEETOUT_COUNT_OF_SQL_UPDATE
''''
'''' automatically-added for ADOSheetOut string-key-value management for standard module and class module
'Public Function GetTextOfStrKeyAdoSheetoutCountOfSqlUpdate() As String
'
'    If Not mblnIsStrKeyValueADOSheetOutDicInitialized Then
'
'        msubInitializeTextForADOSheetOut
'    End If
'
'    GetTextOfStrKeyAdoSheetoutCountOfSqlUpdate = mobjStrKeyValueADOSheetOutDic.Item("STR_KEY_ADO_SHEETOUT_COUNT_OF_SQL_UPDATE")
'End Function
'
''''
'''' get string Value from STR_KEY_ADO_SHEETOUT_COUNT_OF_SQL_INSERT
''''
'''' automatically-added for ADOSheetOut string-key-value management for standard module and class module
'Public Function GetTextOfStrKeyAdoSheetoutCountOfSqlInsert() As String
'
'    If Not mblnIsStrKeyValueADOSheetOutDicInitialized Then
'
'        msubInitializeTextForADOSheetOut
'    End If
'
'    GetTextOfStrKeyAdoSheetoutCountOfSqlInsert = mobjStrKeyValueADOSheetOutDic.Item("STR_KEY_ADO_SHEETOUT_COUNT_OF_SQL_INSERT")
'End Function
'
'
''''
'''' get string Value from STR_KEY_ADO_SHEETOUT_CLIENT_USER_NAME
''''
'''' automatically-added for ADOSheetOut string-key-value management for standard module and class module
'Public Function GetTextOfStrKeyAdoSheetoutClientUserName() As String
'
'    If Not mblnIsStrKeyValueADOSheetOutDicInitialized Then
'
'        msubInitializeTextForADOSheetOut
'    End If
'
'    GetTextOfStrKeyAdoSheetoutClientUserName = mobjStrKeyValueADOSheetOutDic.Item("STR_KEY_ADO_SHEETOUT_CLIENT_USER_NAME")
'End Function
'
''''
'''' get string Value from STR_KEY_ADO_SHEETOUT_CLIENT_HOST_NAME
''''
'''' automatically-added for ADOSheetOut string-key-value management for standard module and class module
'Public Function GetTextOfStrKeyAdoSheetoutClientHostName() As String
'
'    If Not mblnIsStrKeyValueADOSheetOutDicInitialized Then
'
'        msubInitializeTextForADOSheetOut
'    End If
'
'    GetTextOfStrKeyAdoSheetoutClientHostName = mobjStrKeyValueADOSheetOutDic.Item("STR_KEY_ADO_SHEETOUT_CLIENT_HOST_NAME")
'End Function
'
''''
'''' get string Value from STR_KEY_ADO_SHEETOUT_CLIENT_IPV4_ADDRESS
''''
'''' automatically-added for ADOSheetOut string-key-value management for standard module and class module
'Public Function GetTextOfStrKeyAdoSheetoutClientIpv4Address() As String
'
'    If Not mblnIsStrKeyValueADOSheetOutDicInitialized Then
'
'        msubInitializeTextForADOSheetOut
'    End If
'
'    GetTextOfStrKeyAdoSheetoutClientIpv4Address = mobjStrKeyValueADOSheetOutDic.Item("STR_KEY_ADO_SHEETOUT_CLIENT_IPV4_ADDRESS")
'End Function
'
''''
'''' get string Value from STR_KEY_ADO_SHEETOUT_CLIENT_MAC_ADDRESS
''''
'''' automatically-added for ADOSheetOut string-key-value management for standard module and class module
'Public Function GetTextOfStrKeyAdoSheetoutClientMacAddress() As String
'
'    If Not mblnIsStrKeyValueADOSheetOutDicInitialized Then
'
'        msubInitializeTextForADOSheetOut
'    End If
'
'    GetTextOfStrKeyAdoSheetoutClientMacAddress = mobjStrKeyValueADOSheetOutDic.Item("STR_KEY_ADO_SHEETOUT_CLIENT_MAC_ADDRESS")
'End Function
'
'
''''
'''' get string Value from STR_KEY_ADO_SHEETOUT_DELETING_ELAPSED_TIME
''''
'''' automatically-added for ADOSheetOut string-key-value management for standard module and class module
'Public Function GetTextOfStrKeyAdoSheetoutDeletingElapsedTime() As String
'
'    If Not mblnIsStrKeyValueADOSheetOutDicInitialized Then
'
'        msubInitializeTextForADOSheetOut
'    End If
'
'    GetTextOfStrKeyAdoSheetoutDeletingElapsedTime = mobjStrKeyValueADOSheetOutDic.Item("STR_KEY_ADO_SHEETOUT_DELETING_ELAPSED_TIME")
'End Function
'
''''
'''' get string Value from STR_KEY_ADO_SHEETOUT_UPDATING_ELAPSED_TIME
''''
'''' automatically-added for ADOSheetOut string-key-value management for standard module and class module
'Public Function GetTextOfStrKeyAdoSheetoutUpdatingElapsedTime() As String
'
'    If Not mblnIsStrKeyValueADOSheetOutDicInitialized Then
'
'        msubInitializeTextForADOSheetOut
'    End If
'
'    GetTextOfStrKeyAdoSheetoutUpdatingElapsedTime = mobjStrKeyValueADOSheetOutDic.Item("STR_KEY_ADO_SHEETOUT_UPDATING_ELAPSED_TIME")
'End Function
'
''''
'''' get string Value from STR_KEY_ADO_SHEETOUT_INSERTING_ELAPSED_TIME
''''
'''' automatically-added for ADOSheetOut string-key-value management for standard module and class module
'Public Function GetTextOfStrKeyAdoSheetoutInsertingElapsedTime() As String
'
'    If Not mblnIsStrKeyValueADOSheetOutDicInitialized Then
'
'        msubInitializeTextForADOSheetOut
'    End If
'
'    GetTextOfStrKeyAdoSheetoutInsertingElapsedTime = mobjStrKeyValueADOSheetOutDic.Item("STR_KEY_ADO_SHEETOUT_INSERTING_ELAPSED_TIME")
'End Function
'
''''
'''' get string Value from STR_KEY_ADO_SHEETOUT_SQL_SYNCHRONOUS_EXECUTION_ERROR
''''
'''' automatically-added for ADOSheetOut string-key-value management for standard module and class module
'Public Function GetTextOfStrKeyAdoSheetoutSqlSynchronousExecutionError() As String
'
'    If Not mblnIsStrKeyValueADOSheetOutDicInitialized Then
'
'        msubInitializeTextForADOSheetOut
'    End If
'
'    GetTextOfStrKeyAdoSheetoutSqlSynchronousExecutionError = mobjStrKeyValueADOSheetOutDic.Item("STR_KEY_ADO_SHEETOUT_SQL_SYNCHRONOUS_EXECUTION_ERROR")
'End Function
'
''''
'''' get string Value from STR_KEY_ADO_SHEETOUT_SQL_ASYNCHRONOUS_EXECUTION_ERROR
''''
'''' automatically-added for ADOSheetOut string-key-value management for standard module and class module
'Public Function GetTextOfStrKeyAdoSheetoutSqlAsynchronousExecutionError() As String
'
'    If Not mblnIsStrKeyValueADOSheetOutDicInitialized Then
'
'        msubInitializeTextForADOSheetOut
'    End If
'
'    GetTextOfStrKeyAdoSheetoutSqlAsynchronousExecutionError = mobjStrKeyValueADOSheetOutDic.Item("STR_KEY_ADO_SHEETOUT_SQL_ASYNCHRONOUS_EXECUTION_ERROR")
'End Function
'
'
''''
'''' Initialize Key-Values which values
''''
'''' automatically-added for ADOSheetOut string-key-value management for standard module and class module
'Private Sub msubInitializeTextForADOSheetOut()
'
'    Dim objKeyToTextDic As Scripting.Dictionary
'
'    If Not mblnIsStrKeyValueADOSheetOutDicInitialized Then
'
'        Set objKeyToTextDic = GetKeyToTextForLocalizingEachModule(mstrModuleName)
'
'        If objKeyToTextDic Is Nothing Then
'
'            ' If the Key-Value table file is lost, then the default setting is used
'
'            Set objKeyToTextDic = New Scripting.Dictionary
'
'            Select Case GetCurrentUICaptionLanguageType()
'
'                Case CaptionLanguageType.UIJapaneseCaptions
'
'                    AddStringKeyValueForADOSheetOutByJapaneseValues objKeyToTextDic
'                Case Else
'
'                    AddStringKeyValueForADOSheetOutByEnglishValues objKeyToTextDic
'            End Select
'        End If
'
'        mblnIsStrKeyValueADOSheetOutDicInitialized = True
'    End If
'
'    Set mobjStrKeyValueADOSheetOutDic = objKeyToTextDic
'End Sub
'
''''
'''' Add Key-Values which values are in English for ADOSheetOut key-values cache
''''
'''' automatically-added for ADOSheetOut string-key-value management for standard module and class module
'Private Sub AddStringKeyValueForADOSheetOutByEnglishValues(ByRef robjDic As Scripting.Dictionary)
'
'    With robjDic
'
'        .Add "STR_KEY_ADO_SHEETOUT_SQL_EXECUTION_ELAPSED_TIME", "SQL execution elapsed time"
'        .Add "STR_KEY_ADO_SHEETOUT_EXCEL_SHEET_PROCESSING_ELAPSED_TIME", "Excel sheet processing elapsed time"
'        .Add "STR_KEY_ADO_SHEETOUT_TABLE_RECORD_COUNT", "record count"
'        .Add "STR_KEY_ADO_SHEETOUT_TABLE_FIELD_COUNT", "field count"
'        .Add "STR_KEY_ADO_SHEETOUT_SQL_EXECUTED_TIME", "SQL executed time"
'        .Add "STR_KEY_ADO_SHEETOUT_UNIT_SECOND", "[s]"
'        .Add "STR_KEY_ADO_SHEETOUT_RECORDS_AFFECTED", "Records affected"
'        .Add "STR_KEY_ADO_SHEETOUT_COUNT_OF_SQL_DELETE", "Count of deleted records"
'        .Add "STR_KEY_ADO_SHEETOUT_COUNT_OF_SQL_UPDATE", "Count of updated records"
'        .Add "STR_KEY_ADO_SHEETOUT_COUNT_OF_SQL_INSERT", "Count of inserted records"
'        .Add "STR_KEY_ADO_SHEETOUT_CLIENT_USER_NAME", "Client-UserName"
'        .Add "STR_KEY_ADO_SHEETOUT_CLIENT_HOST_NAME", "Client-HostName"
'        .Add "STR_KEY_ADO_SHEETOUT_CLIENT_IPV4_ADDRESS", "Client-IPv4Address"
'        .Add "STR_KEY_ADO_SHEETOUT_CLIENT_MAC_ADDRESS", "Client-MACAddress"
'        .Add "STR_KEY_ADO_SHEETOUT_DELETING_ELAPSED_TIME", "Deleting elapsed time"
'        .Add "STR_KEY_ADO_SHEETOUT_UPDATING_ELAPSED_TIME", "Updating elapsed time"
'        .Add "STR_KEY_ADO_SHEETOUT_INSERTING_ELAPSED_TIME", "Inserting elapsed time"
'        .Add "STR_KEY_ADO_SHEETOUT_SQL_SYNCHRONOUS_EXECUTION_ERROR", "SQL execution error : Synchronous"
'        .Add "STR_KEY_ADO_SHEETOUT_SQL_ASYNCHRONOUS_EXECUTION_ERROR", "SQL execution error : Asynchronous"
'    End With
'End Sub
'
''''
'''' Add Key-Values which values are in Japanese for ADOSheetOut key-values cache
''''
'''' automatically-added for ADOSheetOut string-key-value management for standard module and class module
'Private Sub AddStringKeyValueForADOSheetOutByJapaneseValues(ByRef robjDic As Scripting.Dictionary)
'
'    With robjDic
'
'        .Add "STR_KEY_ADO_SHEETOUT_SQL_EXECUTION_ELAPSED_TIME", "SQL実行経過時間"
'        .Add "STR_KEY_ADO_SHEETOUT_EXCEL_SHEET_PROCESSING_ELAPSED_TIME", "Excelシート描画経過時間"
'        .Add "STR_KEY_ADO_SHEETOUT_TABLE_RECORD_COUNT", "レコード数"
'        .Add "STR_KEY_ADO_SHEETOUT_TABLE_FIELD_COUNT", "列数"
'        .Add "STR_KEY_ADO_SHEETOUT_SQL_EXECUTED_TIME", "SQL実行の日付時刻"
'        .Add "STR_KEY_ADO_SHEETOUT_UNIT_SECOND", "[秒]"
'        .Add "STR_KEY_ADO_SHEETOUT_RECORDS_AFFECTED", "変更のあったレコード数"
'        .Add "STR_KEY_ADO_SHEETOUT_COUNT_OF_SQL_DELETE", "データ削除数(DELETE: Records affected)"
'        .Add "STR_KEY_ADO_SHEETOUT_COUNT_OF_SQL_UPDATE", "データ更新数(UPDATE: Records affected)"
'        .Add "STR_KEY_ADO_SHEETOUT_COUNT_OF_SQL_INSERT", "データ追加数(INSERT: Records affected)"
'        .Add "STR_KEY_ADO_SHEETOUT_CLIENT_USER_NAME", "クライアントのユーザ名"
'        .Add "STR_KEY_ADO_SHEETOUT_CLIENT_HOST_NAME", "クライアントのホスト名"
'        .Add "STR_KEY_ADO_SHEETOUT_CLIENT_IPV4_ADDRESS", "クライアントのIPv4アドレス"
'        .Add "STR_KEY_ADO_SHEETOUT_CLIENT_MAC_ADDRESS", "クライアントのMACアドレス"
'        .Add "STR_KEY_ADO_SHEETOUT_DELETING_ELAPSED_TIME", "削除経過時間"
'        .Add "STR_KEY_ADO_SHEETOUT_UPDATING_ELAPSED_TIME", "更新経過時間"
'        .Add "STR_KEY_ADO_SHEETOUT_INSERTING_ELAPSED_TIME", "挿入経過時間"
'        .Add "STR_KEY_ADO_SHEETOUT_SQL_SYNCHRONOUS_EXECUTION_ERROR", "SQL実行時エラー : 同期実行"
'        .Add "STR_KEY_ADO_SHEETOUT_SQL_ASYNCHRONOUS_EXECUTION_ERROR", "SQL実行時エラー : 非同期実行"
'    End With
'End Sub
'
''''
'''' Remove Keys for ADOSheetOut key-values cache
''''
'''' automatically-added for ADOSheetOut string-key-value management for standard module and class module
'Private Sub RemoveKeysOfKeyValueADOSheetOut(ByRef robjDic As Scripting.Dictionary)
'
'    With robjDic
'
'        If .Exists(STR_KEY_ADO_SHEETOUT_SQL_EXECUTION_ELAPSED_TIME) Then   ' Check only the first key, otherwise omit items
'
'            .Remove "STR_KEY_ADO_SHEETOUT_SQL_EXECUTION_ELAPSED_TIME"
'            .Remove "STR_KEY_ADO_SHEETOUT_EXCEL_SHEET_PROCESSING_ELAPSED_TIME"
'            .Remove "STR_KEY_ADO_SHEETOUT_TABLE_RECORD_COUNT"
'            .Remove "STR_KEY_ADO_SHEETOUT_TABLE_FIELD_COUNT"
'            .Remove "STR_KEY_ADO_SHEETOUT_SQL_EXECUTED_TIME"
'            .Remove "STR_KEY_ADO_SHEETOUT_UNIT_SECOND"
'            .Remove "STR_KEY_ADO_SHEETOUT_RECORDS_AFFECTED"
'            .Remove "STR_KEY_ADO_SHEETOUT_COUNT_OF_SQL_DELETE"
'            .Remove "STR_KEY_ADO_SHEETOUT_COUNT_OF_SQL_UPDATE"
'            .Remove "STR_KEY_ADO_SHEETOUT_COUNT_OF_SQL_INSERT"
'            .Remove "STR_KEY_ADO_SHEETOUT_CLIENT_USER_NAME"
'            .Remove "STR_KEY_ADO_SHEETOUT_CLIENT_HOST_NAME"
'            .Remove "STR_KEY_ADO_SHEETOUT_CLIENT_IPV4_ADDRESS"
'            .Remove "STR_KEY_ADO_SHEETOUT_CLIENT_MAC_ADDRESS"
'            .Remove "STR_KEY_ADO_SHEETOUT_DELETING_ELAPSED_TIME"
'            .Remove "STR_KEY_ADO_SHEETOUT_UPDATING_ELAPSED_TIME"
'            .Remove "STR_KEY_ADO_SHEETOUT_INSERTING_ELAPSED_TIME"
'            .Remove "STR_KEY_ADO_SHEETOUT_SQL_SYNCHRONOUS_EXECUTION_ERROR"
'            .Remove "STR_KEY_ADO_SHEETOUT_SQL_ASYNCHRONOUS_EXECUTION_ERROR"
'        End If
'    End With
'End Sub
'
'
'''--VBA_Code_File--<ADOSheetIn.bas>--
'Attribute VB_Name = "ADOSheetIn"
''
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on both ADO and Excel
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Tue, 10/May/2022    Kalmclaeyd Tarclanus    Improved
''
'
'Option Explicit
'
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''''
'''' get table-collection from Excel sheet using ADO-OLEDB connection
''''
'Public Function GetDataTableColFromSheetByBookPathAndSheetName(ByVal vstrInputBookPath As String, _
'        ByVal vstrSheetName As String, _
'        Optional ByVal vintFieldTitlesRowIndex As Long = 1, _
'        Optional ByVal vintTopLeftColumnIndex As Long = 1, _
'        Optional ByVal vobjColumnsExceptionIndexList As Scripting.Dictionary = Nothing, _
'        Optional ByVal vobjColumnsFieldTitleNameExceptionList As Scripting.Dictionary = Nothing) As Collection
'
'
'    Dim objSheet As Excel.Worksheet, strSQLWherePart As String
'    Dim strSQL As String, objDataTableCol As Collection, strTableName As String
'
'    strSQLWherePart = GetSQLWhereFromNotNullConditionFromWorkbookPathAndSheetName(vstrInputBookPath, vstrSheetName, vintFieldTitlesRowIndex, vintTopLeftColumnIndex, vobjColumnsExceptionIndexList, vobjColumnsFieldTitleNameExceptionList)
'
'    With New XlAdoConnector
'
'        .SetInputExcelBookPath vstrInputBookPath, vblnBookReadOnly:=True
'
'        strTableName = "[" & vstrSheetName & "$]"
'
'        strSQL = "SELECT * FROM " & strTableName & "WHERE " & strSQLWherePart
'
'        Set objDataTableCol = GetTableCollectionFromRSet(.GetRecordset(strSQL))
'    End With
'
'    Set GetDataTableColFromSheetByBookPathAndSheetName = objDataTableCol
'End Function
'''--VBA_Code_File--<ADOExSheetOut.bas>--
'Attribute VB_Name = "ADOExSheetOut"
''
''   create a SQL result Excel sheet with additional information by using ADODB.Recordset
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on both ADO and Excel
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2024 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Sun, 14/Jan/2024    Kalmclaeyd Tarclanus    Improved
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Declarations
''///////////////////////////////////////////////
'Private Const mstrModuleName As String = "ADOExSheetOut"
'
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''''
''''
''''
'''' <Argument>vobjInfoTypeToTitleToValueDic: Dictionary(Of String, Dictionary(Of String, Variant))</Argument>
'Public Function NormalOutputRecordsetWithAdditionalInfoToCellsOnSheet(ByVal vobjSheet As Excel.Worksheet, _
'        ByVal vobjRSet As ADODB.Recordset, _
'        ByVal vobjInfoTypeToTitleToValueDic As Scripting.Dictionary, _
'        Optional ByVal vintTopLeftRowIndex As Long = 1, _
'        Optional ByVal vintTopLeftColumnIndex As Long = 1, _
'        Optional ByVal vobjFieldTitleToColumnWidthDic As Scripting.Dictionary = Nothing, _
'        Optional ByVal vstrSuppressingColumnsDelimitedComma As String = "", _
'        Optional ByVal vblnAllowToCreateNullColumnsCompressedTable As Boolean = False, _
'        Optional ByVal vblnAllowToOutputMetaColumnInfoNameToSheet As Boolean = True) As Long
'
'
'    Dim intCountRowsOfAdditionalInfo As Long, intFieldTitlesRowIndex As Long, strNullColumnsDelimitedComma As String
'
'    intCountRowsOfAdditionalInfo = vobjInfoTypeToTitleToValueDic.Count
'
'    NormalOutputOnlyAdditionalInfoToCellsOnSheet vobjSheet, vobjInfoTypeToTitleToValueDic, vintTopLeftRowIndex, vintTopLeftColumnIndex, vstrSuppressingColumnsDelimitedComma, vblnAllowToOutputMetaColumnInfoNameToSheet
'
'    intFieldTitlesRowIndex = vintTopLeftRowIndex + intCountRowsOfAdditionalInfo
'
'    strNullColumnsDelimitedComma = NormalOutputRecordsetToCellsOnSheet(vobjSheet, vobjRSet, intFieldTitlesRowIndex, vintTopLeftColumnIndex, vobjFieldTitleToColumnWidthDic, vstrSuppressingColumnsDelimitedComma, vblnAllowToCreateNullColumnsCompressedTable)
'
'    If vblnAllowToOutputMetaColumnInfoNameToSheet Then
'
'        If vintTopLeftColumnIndex > 1 Then
'
'            msubDecorateRangeOfOutputMetaColumnInfoNameAndText "FileTitles", vobjSheet, intFieldTitlesRowIndex, vintTopLeftColumnIndex
'
'            vobjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex - 1) & CStr(intFieldTitlesRowIndex)).EntireColumn.ColumnWidth = "11"
'        End If
'    End If
'
'    If vblnAllowToCreateNullColumnsCompressedTable And strNullColumnsDelimitedComma <> "" Then
'
'        ' output compressed additional info
'
'    End If
'
'End Function
'
''''
''''
''''
'Public Sub NormalOutputOnlyAdditionalInfoToCellsOnSheet(ByVal vobjSheet As Excel.Worksheet, _
'        ByVal vobjInfoTypeToTitleToValueDic As Scripting.Dictionary, _
'        Optional ByVal vintTopLeftRowIndex As Long = 1, _
'        Optional ByVal vintTopLeftColumnIndex As Long = 1, _
'        Optional ByVal vstrSuppressingColumnsDelimitedComma As String = "", _
'        Optional ByVal vblnAllowToOutputMetaColumnInfoNameToSheet As Boolean = True)
'
'    Dim objTitleToValueDic As Scripting.Dictionary, varInfoType As Variant
'    Dim intRowIndex As Long, objSuppressingColumnsDic As Scripting.Dictionary, sngFontSize As Single, enmManualDecoratingFieldTitlesRangePattern As ManualDecoratingFieldTitlesRangePattern
'    Dim strInfosArray() As String, strMetaTitle As String, blnSetHorizontalAlignmentCenter As Boolean
'
'    SetUpSuppressingColumnsDic objSuppressingColumnsDic, vstrSuppressingColumnsDelimitedComma
'
'    With vobjInfoTypeToTitleToValueDic
'
'        intRowIndex = vintTopLeftRowIndex
'
'        For Each varInfoType In .Keys
'
'            Set objTitleToValueDic = .Item(varInfoType)
'
'            GetFieldAdditionalInfosParams enmManualDecoratingFieldTitlesRangePattern, sngFontSize, blnSetHorizontalAlignmentCenter, varInfoType
'
'            NormalOutputInfoTitleToValueDicToCellsOnSheet vobjSheet, objTitleToValueDic, objSuppressingColumnsDic, intRowIndex, vintTopLeftColumnIndex, enmManualDecoratingFieldTitlesRangePattern, sngFontSize, blnSetHorizontalAlignmentCenter
'
'            If vblnAllowToOutputMetaColumnInfoNameToSheet Then
'
'                If vintTopLeftColumnIndex > 1 Then
'
'                    strInfosArray = Split(varInfoType, "_")
'
'                    strMetaTitle = Replace(strInfosArray(1), "Column", "")
'
'                    msubDecorateRangeOfOutputMetaColumnInfoNameAndText strMetaTitle, vobjSheet, intRowIndex, vintTopLeftColumnIndex
'                End If
'            End If
'
'            intRowIndex = intRowIndex + 1
'        Next
'    End With
'End Sub
'
''''
''''
''''
'Private Sub msubDecorateRangeOfOutputMetaColumnInfoNameAndText(ByVal vstrText As String, _
'        ByRef robjSheet As Excel.Worksheet, _
'        ByVal vintRowIndex As Long, _
'        ByVal vintTopLeftColumnIndex As Long)
'
'    Dim objRange As Excel.Range
'
'    Set objRange = robjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex - 1) & CStr(vintRowIndex))
'
'    objRange.Value = vstrText
'
'    ' Blue back-ground
'    DecorateFieldTitleFontAndInteriorForGatheredColTable objRange
'
'    objRange.Font.Size = 9.5
'
'End Sub
'
'
''''
''''
''''
'Public Sub GetFieldAdditionalInfosParams(ByRef renmManualDecoratingFieldTitlesRangePattern As ManualDecoratingFieldTitlesRangePattern, _
'        ByRef rsngFontSize As Single, _
'        ByRef rblnSetHorizontalAlignmentCenter As Boolean, _
'        ByVal vstrInfoType As String)
'
'    Select Case True
'
'        Case InStr(1, vstrInfoType, "No") > 0
'
'            renmManualDecoratingFieldTitlesRangePattern = SepiaFontLightToDarkBrownGraduationBgMeiryo
'
'            rsngFontSize = 9.5
'
'            rblnSetHorizontalAlignmentCenter = True
'
'        Case InStr(1, vstrInfoType, "TypeName") > 0
'
'            renmManualDecoratingFieldTitlesRangePattern = DarkBrownFontPaleGradientGreenBgMeiryo
'
'            rsngFontSize = 9
'
'            rblnSetHorizontalAlignmentCenter = False
'
'        Case InStr(1, vstrInfoType, "LengthOfType") > 0
'
'            renmManualDecoratingFieldTitlesRangePattern = DarkBrownFontMorePaleGradientGreenBgMeiryo
'
'            rsngFontSize = 10
'
'            rblnSetHorizontalAlignmentCenter = False
'
'        Case InStr(1, vstrInfoType, "ColumnDescription") > 0
'
'            renmManualDecoratingFieldTitlesRangePattern = DarkBrownFontLightSilverBgMeiryo
'
'            rsngFontSize = 9.5
'
'            rblnSetHorizontalAlignmentCenter = False
'
'        Case Else
'
'            renmManualDecoratingFieldTitlesRangePattern = DarkBrownFontLightSilverBgMeiryo
'
'            rsngFontSize = 9.5
'
'            rblnSetHorizontalAlignmentCenter = False
'    End Select
'End Sub
'
'
''''
''''
''''
'Public Sub NormalOutputInfoTitleToValueDicToCellsOnSheet(ByVal vobjSheet As Excel.Worksheet, _
'        ByVal vobjTitleToValueDic As Scripting.Dictionary, _
'        ByVal vobjSuppressingColumnsDic As Scripting.Dictionary, _
'        Optional ByVal vintTopLeftRowIndex As Long = 1, _
'        Optional ByVal vintTopLeftColumnIndex As Long = 1, _
'        Optional ByVal venmManualDecoratingFieldTitlesRangePattern As ManualDecoratingFieldTitlesRangePattern = ManualDecoratingFieldTitlesRangePattern.DarkBrownFontLightSilverBgMeiryo, _
'        Optional ByVal vsngFont As Single = 9.5, _
'        Optional ByVal vblnSetHorizontalAlignmentCenter As Boolean = False)
'
'
'    Dim varValues As Variant, i As Long, varKey As Variant, varItem As Variant, objInfosRange As Excel.Range
'    Dim intIndex As Long
'
'
'    If vobjTitleToValueDic.Count = 1 Then
'
'        varValues = vobjTitleToValueDic.Items(0)
'
'        vobjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintTopLeftRowIndex)).Value = varValues
'    Else
'        If Not vobjSuppressingColumnsDic Is Nothing Then
'
'            ReDim varValues(1 To 1, 1 To vobjTitleToValueDic.Count - vobjSuppressingColumnsDic.Count)
'        Else
'            ReDim varValues(1 To 1, 1 To vobjTitleToValueDic.Count)
'        End If
'
'
'        i = 1
'
'        intIndex = 1
'
'        With vobjTitleToValueDic
'
'            For Each varKey In .Keys
'
'                Select Case True
'
'                    Case vobjSuppressingColumnsDic Is Nothing, Not vobjSuppressingColumnsDic.Exists(i)
'
'                        varItem = .Item(varKey)
'
'                        varValues(1, intIndex) = varItem
'
'                        intIndex = intIndex + 1
'                End Select
'            Next
'        End With
'
'        If Not vobjSuppressingColumnsDic Is Nothing Then
'
'            Set objInfosRange = vobjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintTopLeftRowIndex)).Resize(1, vobjTitleToValueDic.Count - vobjSuppressingColumnsDic.Count)
'
'            objInfosRange.Value = varValues
'        Else
'
'            Set objInfosRange = vobjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintTopLeftRowIndex)).Resize(1, vobjTitleToValueDic.Count)
'
'            objInfosRange.Value = varValues
'        End If
'
'        ' decorate Range
'
'        DecorateRangeOfFieldTitleInformationsForManualGrids objInfosRange, venmManualDecoratingFieldTitlesRangePattern
'
'        With objInfosRange
'
'            .Font.Size = vsngFont
'
'            If vblnSetHorizontalAlignmentCenter Then
'
'                .HorizontalAlignment = xlCenter
'            End If
'        End With
'    End If
'End Sub
'
'
''''
''''
''''
'Public Function NormalOutputRecordsetToCellsOnSheet(ByVal vobjSheet As Excel.Worksheet, _
'        ByVal vobjRSet As ADODB.Recordset, _
'        Optional ByVal vintFieldTitlesRowIndex As Long = 1, _
'        Optional ByVal vintTopLeftColumnIndex As Long = 1, _
'        Optional ByVal vobjFieldTitleToColumnWidthDic As Scripting.Dictionary = Nothing, _
'        Optional ByVal vstrSuppressingColumnsDelimitedComma As String = "", _
'        Optional ByVal vblnAllowToCreateNullColumnsCompressedTable As Boolean = False) As String
'
'
'    Dim intRowsCountOfRecords As Long, objOutputDTCol As Collection, strNullColumnsDelimitedComma As String
'    Dim objCompressedSheet As Excel.Worksheet, objOutputFieldTitlesCol As Collection
'
'
'    Set objOutputFieldTitlesCol = NormalOutputFieldTitlesFromRSetOnSheet(vobjSheet, vobjRSet, vintFieldTitlesRowIndex, vintTopLeftColumnIndex, vstrSuppressingColumnsDelimitedComma)
'
'    intRowsCountOfRecords = NormalOutputOnlyRecordsetDataTableToCellsOnSheet(objOutputDTCol, vobjSheet, vobjRSet, vintFieldTitlesRowIndex, vintTopLeftColumnIndex, vstrSuppressingColumnsDelimitedComma)
'
'    If Not vobjFieldTitleToColumnWidthDic Is Nothing Then
'
'        AdjustColumnWidthByTitleTextWithFindingRightDirection vobjSheet, vobjFieldTitleToColumnWidthDic, vintFieldTitlesRowIndex, vintTopLeftColumnIndex
'    End If
'
'    strNullColumnsDelimitedComma = ""
'
'    If vblnAllowToCreateNullColumnsCompressedTable And objOutputDTCol.Count > 0 Then
'
'        strNullColumnsDelimitedComma = mfstrGetNullColumnsDelimitedCommaFromSqlResultDataTableCol(objOutputDTCol)
'
'        If strNullColumnsDelimitedComma <> "" Then
'
'            Set objCompressedSheet = mfobjGetNullColumnsCompressedSheet(vobjSheet)
'
'            NormalOutputFieldTitlesFromColOnSheet objCompressedSheet, objOutputFieldTitlesCol, vintFieldTitlesRowIndex, vintTopLeftColumnIndex, strNullColumnsDelimitedComma
'
'            NormalOutputOnlyDTColToCellsOnSheet objCompressedSheet, objOutputDTCol, vintFieldTitlesRowIndex, vintTopLeftColumnIndex, strNullColumnsDelimitedComma
'        End If
'    End If
'
'    NormalOutputRecordsetToCellsOnSheet = strNullColumnsDelimitedComma
'End Function
'
'
'' MoveNextNormalCopyFromDTCol
'
'
''''
''''
''''
'Public Function TransposeOutputRecordsetToCellsOnSheet(ByVal vobjSheet As Excel.Worksheet, _
'        ByVal vobjRSet As ADODB.Recordset, _
'        Optional ByVal vintFieldTitlesRowIndex As Long = 1, _
'        Optional ByVal vintTopLeftColumnIndex As Long = 1, _
'        Optional ByVal vstrSuppressingColumnsDelimitedComma As String = "", _
'        Optional ByVal vblnAllowToCreateNullColumnsCompressedTable As Boolean = False) As Long
'
'    Dim intRowsCountOfRecords As Long, objOutputDTCol As Collection
'
'
'    TransposeOutputFieldTitlesFromRSetOnSheet vobjSheet, vobjRSet, vintFieldTitlesRowIndex, vintTopLeftColumnIndex, vstrSuppressingColumnsDelimitedComma
'
'    intRowsCountOfRecords = TransposeOutputOnlyRecordsetDataTableToCellsOnSheet(objOutputDTCol, vobjSheet, vobjRSet, vintFieldTitlesRowIndex, vintTopLeftColumnIndex, vstrSuppressingColumnsDelimitedComma)
'
'    If vblnAllowToCreateNullColumnsCompressedTable And objOutputDTCol.Count > 0 Then
'
'
'
'    End If
'
'
'
'    TransposeOutputRecordsetToCellsOnSheet = intRowsCountOfRecords
'End Function
'
''''
''''
''''
'Private Function mfstrGetNullColumnsDelimitedCommaFromSqlResultDataTableCol(ByVal robjDTCol As Collection) As String
'
'    Dim strNullColumnsDelimitedComma As String, objNullColumnsDic As Scripting.Dictionary
'    Dim varValuesCol As Variant, objValuesCol As Collection
'    Dim i As Long, varItem As Variant, varIndex As Variant
'
'    Set objNullColumnsDic = New Scripting.Dictionary
'
'    For Each varValuesCol In robjDTCol
'
'        Set objValuesCol = varValuesCol
'
'        i = 1
'
'        For Each varItem In objValuesCol
'
'            objNullColumnsDic.Add i, Null
'
'            i = i + 1
'        Next
'
'        Exit For
'    Next
'
'
'    For Each varValuesCol In robjDTCol
'
'        Set objValuesCol = varValuesCol
'
'        i = 1
'
'        For Each varItem In objValuesCol
'
'            If objNullColumnsDic.Exists(i) Then
'
'                Select Case True
'
'                    Case IsNull(varItem), IsEmpty(varItem)
'
'                        ' Nothing to do
'
'                    Case TypeName(varItem) = "String"
'
'                        If varItem <> "" Then
'
'                            objNullColumnsDic.Remove i
'                        End If
'                    Case Else
'
'                        objNullColumnsDic.Remove i
'                End Select
'            End If
'
'            If objNullColumnsDic.Count = 0 Then
'
'                Exit For
'            End If
'
'            i = i + 1
'        Next
'    Next
'
'    If objNullColumnsDic.Count > 0 Then
'
'        For Each varIndex In objNullColumnsDic.Keys
'
'            If strNullColumnsDelimitedComma <> "" Then
'
'                strNullColumnsDelimitedComma = strNullColumnsDelimitedComma & ","
'            End If
'
'            strNullColumnsDelimitedComma = strNullColumnsDelimitedComma & CStr(varIndex)
'        Next
'    Else
'        strNullColumnsDelimitedComma = ""
'    End If
'
'    mfstrGetNullColumnsDelimitedCommaFromSqlResultDataTableCol = strNullColumnsDelimitedComma
'End Function
'
''''
''''
''''
'Private Function mfobjGetNullColumnsCompressedSheet(ByVal vobjSheet As Excel.Worksheet) As Excel.Worksheet
'
'    Dim strCompressedSheetName As String, objCompressedSheet As Excel.Worksheet, objBook As Excel.Workbook
'
'    Set objBook = vobjSheet.Parent
'
'    strCompressedSheetName = Left("NullC_" & vobjSheet.Name, 31)
'
'    Set objCompressedSheet = GetNewWorksheetAfterAllExistedSheetsFromBook(objBook)
'
'    objCompressedSheet.Name = strCompressedSheetName
'
'    Set mfobjGetNullColumnsCompressedSheet = objCompressedSheet
'End Function
'
'
''''
''''
''''
'Public Function NormalOutputFieldTitlesFromRSetOnSheet(ByVal vobjSheet As Excel.Worksheet, _
'        ByVal vobjRSet As ADODB.Recordset, _
'        Optional ByVal vintFieldTitlesRowIndex As Long = 1, _
'        Optional ByVal vintTopLeftColumnIndex As Long = 1, _
'        Optional ByVal vstrSuppressingColumnsDelimitedComma As String = "") As Collection
'
'    Dim varTitles As Variant, i As Long, objField As ADOR.Field, objRange As Excel.Range, objSuppressingColumnsDic As Scripting.Dictionary, varColumn As Variant
'
'    Dim intColumnDecrement As Long, blnContinueToAddValue As Boolean, objFieldTitlesCol As Collection
'
'
'    Set objFieldTitlesCol = New Collection
'
'    intColumnDecrement = 0
'
'    SetUpSuppressingColumnsDic objSuppressingColumnsDic, vstrSuppressingColumnsDelimitedComma
'
'    If vobjRSet.Fields.Count > 1 Then
'
'        If vstrSuppressingColumnsDelimitedComma <> "" Then
'
'            ReDim varTitles(1 To 1, 1 To vobjRSet.Fields.Count - objSuppressingColumnsDic.Count)
'        Else
'            ReDim varTitles(1 To 1, 1 To vobjRSet.Fields.Count)
'        End If
'
'        i = 1
'
'        For Each objField In vobjRSet.Fields
'
'            Select Case True
'
'                Case vstrSuppressingColumnsDelimitedComma = "", Not objSuppressingColumnsDic.Exists(i)
'
'                    varTitles(1, i - intColumnDecrement) = objField.Name
'
'                    objFieldTitlesCol.Add objField.Name
'                Case Else
'
'                    intColumnDecrement = intColumnDecrement + 1
'            End Select
'
'            i = i + 1
'        Next
'
'        Set objRange = vobjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintFieldTitlesRowIndex)).Resize(1, vobjRSet.Fields.Count - intColumnDecrement)
'
'        objRange.Value = varTitles
'    Else
'        varTitles = vobjRSet.Fields.Item(0).Name
'
'        Set objRange = vobjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintFieldTitlesRowIndex))
'
'        objRange.Value = varTitles
'
'        objFieldTitlesCol.Add varTitles
'    End If
'
'    ' Temporary decoration
'
'    DecorateRangeOfVariousMetaInfo objRange, WhiteFontGrayBgMeiryo
'
'
'    Set NormalOutputFieldTitlesFromRSetOnSheet = objFieldTitlesCol
'End Function
'
''''
''''
''''
'Public Sub NormalOutputFieldTitlesFromColOnSheet(ByVal vobjSheet As Excel.Worksheet, _
'        ByVal vobjInputCol As Collection, _
'        Optional ByVal vintFieldTitlesRowIndex As Long = 1, _
'        Optional ByVal vintTopLeftColumnIndex As Long = 1, _
'        Optional ByVal vstrSuppressingColumnsDelimitedComma As String = "")
'
'    Dim varTitles As Variant, i As Long, objRange As Excel.Range, objSuppressingColumnsDic As Scripting.Dictionary
'
'    Dim intColumnDecrement As Long, blnContinueToAddValue As Boolean
'    Dim varItem As Variant
'
'
'    intColumnDecrement = 0
'
'    SetUpSuppressingColumnsDic objSuppressingColumnsDic, vstrSuppressingColumnsDelimitedComma
'
'    If vobjInputCol.Count > 1 Then
'
'        If vstrSuppressingColumnsDelimitedComma <> "" Then
'
'            ReDim varTitles(1 To 1, 1 To vobjInputCol.Count - objSuppressingColumnsDic.Count)
'        Else
'            ReDim varTitles(1 To 1, 1 To vobjInputCol.Count)
'        End If
'
'        i = 1
'
'        For Each varItem In vobjInputCol
'
'            Select Case True
'
'                Case vstrSuppressingColumnsDelimitedComma = "", Not objSuppressingColumnsDic.Exists(i)
'
'                    varTitles(1, i - intColumnDecrement) = varItem
'                Case Else
'
'                    intColumnDecrement = intColumnDecrement + 1
'            End Select
'
'            i = i + 1
'        Next
'
'        Set objRange = vobjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintFieldTitlesRowIndex)).Resize(1, vobjInputCol.Count - intColumnDecrement)
'
'        objRange.Value = varTitles
'    Else
'        varTitles = vobjInputCol.Item(0)
'
'        Set objRange = vobjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintFieldTitlesRowIndex))
'
'        objRange.Value = varTitles
'    End If
'
'    ' Temporary decoration
'
'    DecorateRangeOfVariousMetaInfo objRange, WhiteFontGrayBgMeiryo
'End Sub
'
'
''''
''''
''''
'Public Function TransposeOutputFieldTitlesFromRSetOnSheet(ByVal vobjSheet As Excel.Worksheet, _
'        ByVal vobjRSet As ADODB.Recordset, _
'        Optional ByVal vintFieldTitlesRowIndex As Long = 1, _
'        Optional ByVal vintTopLeftColumnIndex As Long = 1, _
'        Optional ByVal vstrSuppressingColumnsDelimitedComma As String = "") As Collection
'
'    Dim varTitles As Variant, i As Long, objField As ADOR.Field, objRange As Excel.Range, objSuppressingColumnsDic As Scripting.Dictionary, varColumn As Variant
'
'    Dim intColumnDecrement As Long, blnContinueToAddValue As Boolean, objFieldTitlesCol As Collection
'
'
'
'    Set objFieldTitlesCol = New Collection
'
'    intColumnDecrement = 0
'
'    SetUpSuppressingColumnsDic objSuppressingColumnsDic, vstrSuppressingColumnsDelimitedComma
'
'    If vobjRSet.Fields.Count > 1 Then
'
'        If vstrSuppressingColumnsDelimitedComma <> "" Then
'
'            ReDim varTitles(1 To vobjRSet.Fields.Count - objSuppressingColumnsDic.Count, 1 To 1)
'        Else
'            ReDim varTitles(1 To vobjRSet.Fields.Count, 1 To 1)
'        End If
'
'        i = 1
'
'        For Each objField In vobjRSet.Fields
'
'            Select Case True
'
'                Case vstrSuppressingColumnsDelimitedComma = "", Not objSuppressingColumnsDic.Exists(i)
'
'                    varTitles(i - intColumnDecrement, 1) = objField.Name
'
'                    objFieldTitlesCol.Add objField.Name
'                Case Else
'
'                    intColumnDecrement = intColumnDecrement + 1
'            End Select
'
'            i = i + 1
'        Next
'
'        Set objRange = vobjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintFieldTitlesRowIndex)).Resize(vobjRSet.Fields.Count - intColumnDecrement, 1)
'
'        objRange.Value = varTitles
'    Else
'        varTitles = vobjRSet.Fields.Item(0).Name
'
'        Set objRange = vobjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintFieldTitlesRowIndex))
'
'        objRange.Value = varTitles
'
'        objFieldTitlesCol.Add varTitles
'    End If
'
'    ' Temporary decoration
'
'    DecorateRangeOfVariousMetaInfo objRange, WhiteFontGrayBgMeiryo
'
'
'    Set TransposeOutputFieldTitlesFromRSetOnSheet = objFieldTitlesCol
'End Function
'
'
'
'
''''
''''
''''
'Public Sub TransposeOutputFieldTitlesFromColOnSheet(ByVal vobjSheet As Excel.Worksheet, _
'        ByVal vobjInputCol As Collection, _
'        Optional ByVal vintFieldTitlesRowIndex As Long = 1, _
'        Optional ByVal vintTopLeftColumnIndex As Long = 1, _
'        Optional ByVal vstrSuppressingColumnsDelimitedComma As String = "")
'
'    Dim varTitles As Variant, i As Long, objRange As Excel.Range, objSuppressingColumnsDic As Scripting.Dictionary
'
'    Dim intColumnDecrement As Long, blnContinueToAddValue As Boolean
'    Dim varItem As Variant
'
'
'
'    intColumnDecrement = 0
'
'    SetUpSuppressingColumnsDic objSuppressingColumnsDic, vstrSuppressingColumnsDelimitedComma
'
'    If vobjInputCol.Count > 1 Then
'
'        If vstrSuppressingColumnsDelimitedComma <> "" Then
'
'            ReDim varTitles(1 To vobjInputCol.Count - objSuppressingColumnsDic.Count, 1 To 1)
'        Else
'            ReDim varTitles(1 To vobjInputCol.Count, 1 To 1)
'        End If
'
'        i = 1
'
'        For Each varItem In vobjInputCol
'
'            Select Case True
'
'                Case vstrSuppressingColumnsDelimitedComma = "", Not objSuppressingColumnsDic.Exists(i)
'
'                    varTitles(i - intColumnDecrement, 1) = varItem
'                Case Else
'
'                    intColumnDecrement = intColumnDecrement + 1
'            End Select
'
'            i = i + 1
'        Next
'
'        Set objRange = vobjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintFieldTitlesRowIndex)).Resize(vobjInputCol.Count - intColumnDecrement, 1)
'
'        objRange.Value = varTitles
'    Else
'        varTitles = vobjInputCol.Item(0)
'
'        Set objRange = vobjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintFieldTitlesRowIndex))
'
'        objRange.Value = varTitles
'    End If
'
'    ' Temporary decoration
'
'    DecorateRangeOfVariousMetaInfo objRange, WhiteFontGrayBgMeiryo
'
'
'    Set TransposeOutputFieldTitlesFromRSetOnSheet = objFieldTitlesCol
'End Sub
'
'
''''
'''' data-table sheet out with Recordset object
''''
'Public Function NormalOutputOnlyRecordsetDataTableToCellsOnSheet(ByRef robjOutputDTCol As Collection, ByVal vobjSheet As Excel.Worksheet, _
'        ByVal vobjRSet As ADODB.Recordset, _
'        Optional ByVal vintFieldTitlesRowIndex As Long = 1, _
'        Optional ByVal vintTopLeftColumnIndex As Long = 1, _
'        Optional ByVal vstrSuppressingColumnsDelimitedComma As String = "") As Long
'
'
'    Dim objRange As Excel.Range, objTopLeftDataTableRange As Excel.Range, intRowsCountOfRecords As Long, intColumnsCount As Long
'
'    Dim strSuppressingColumns() As String
'
'
'
'    intRowsCountOfRecords = 0
'
'    With vobjSheet
'
'        Set objTopLeftDataTableRange = vobjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintFieldTitlesRowIndex + 1))
'
'        If vobjRSet.State <> ADODB.ObjectStateEnum.adStateClosed Then
'
'            If Not vobjRSet.BOF Then
'
'                vobjRSet.MoveFirst
'            End If
'
'            If vstrSuppressingColumnsDelimitedComma <> "" Then
'
'                strSuppressingColumns = Split(vstrSuppressingColumnsDelimitedComma, ",")
'
'                intColumnsCount = vobjRSet.Fields.Count - UBound(strSuppressingColumns) - 1
'            Else
'                intColumnsCount = vobjRSet.Fields.Count
'            End If
'
'
'            intRowsCountOfRecords = MoveNextNormalCopyFromRecordSet(robjOutputDTCol, objTopLeftDataTableRange, vobjRSet, vstrSuppressingColumnsDelimitedComma)
'
'            ' temporary decoration
'
'            Set objRange = objTopLeftDataTableRange.Resize(intRowsCountOfRecords, intColumnsCount)
'
'            DecorateRangeOfRecords objRange, False, RecordCellsBlacksAndGrayInsideHorizontal
'        End If
'    End With
'
'    NormalOutputOnlyRecordsetDataTableToCellsOnSheet = intRowsCountOfRecords
'End Function
'
'
''''
'''' data-table sheet out
''''
'Public Function NormalOutputOnlyDTColToCellsOnSheet(ByVal vobjSheet As Excel.Worksheet, _
'        ByVal vobjDTCol As Collection, _
'        Optional ByVal vintFieldTitlesRowIndex As Long = 1, _
'        Optional ByVal vintTopLeftColumnIndex As Long = 1, _
'        Optional ByVal vstrSuppressingColumnsDelimitedComma As String = "") As Long
'
'
'    Dim objRange As Excel.Range, objTopLeftDataTableRange As Excel.Range, intRowsCountOfRecords As Long, intColumnsCount As Long
'
'    Dim strSuppressingColumns() As String
'
'    intRowsCountOfRecords = 0
'
'    With vobjSheet
'
'        Set objTopLeftDataTableRange = vobjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex) & CStr(vintFieldTitlesRowIndex + 1))
'
'
'        If vstrSuppressingColumnsDelimitedComma <> "" Then
'
'            strSuppressingColumns = Split(vstrSuppressingColumnsDelimitedComma, ",")
'
'            intColumnsCount = vobjDTCol.Item(1).Count - UBound(strSuppressingColumns) - 1
'        Else
'            intColumnsCount = vobjDTCol.Item(1).Count
'        End If
'
'        intRowsCountOfRecords = MoveNextNormalCopyFromDTCol(objTopLeftDataTableRange, vobjDTCol, vstrSuppressingColumnsDelimitedComma)
'
'        ' temporary decoration
'
'        Set objRange = objTopLeftDataTableRange.Resize(intRowsCountOfRecords, intColumnsCount)
'
'        DecorateRangeOfRecords objRange, False, RecordCellsBlacksAndGrayInsideHorizontal
'    End With
'
'    NormalOutputOnlyDTColToCellsOnSheet = intRowsCountOfRecords
'End Function
'
'
''''
'''' transposed data-table sheet out with Recordset object
''''
'Public Function TransposeOutputOnlyRecordsetDataTableToCellsOnSheet(ByRef robjOutputDTCol As Collection, ByVal vobjSheet As Excel.Worksheet, _
'        ByVal vobjRSet As ADODB.Recordset, _
'        Optional ByVal vintFieldTitlesRowIndex As Long = 1, _
'        Optional ByVal vintTopLeftColumnIndex As Long = 1, _
'        Optional ByVal vstrSuppressingColumnsDelimitedComma As String = "") As Long
'
'
'    Dim objRange As Excel.Range, objTopLeftDataTableRange As Excel.Range, intRowsCountOfRecords As Long, intColumnsCount As Long
'
'    Dim strSuppressingColumns() As String
'
'
'    intRowsCountOfRecords = 0
'
'    With vobjSheet
'
'        Set objTopLeftDataTableRange = vobjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex + 1) & CStr(vintFieldTitlesRowIndex))
'
'        If vobjRSet.State <> ADODB.ObjectStateEnum.adStateClosed Then
'
'            If Not vobjRSet.BOF Then
'
'                vobjRSet.MoveFirst
'            End If
'
'            If vstrSuppressingColumnsDelimitedComma <> "" Then
'
'                strSuppressingColumns = Split(vstrSuppressingColumnsDelimitedComma, ",")
'
'                intColumnsCount = vobjRSet.Fields.Count - UBound(strSuppressingColumns) - 1
'            Else
'                intColumnsCount = vobjRSet.Fields.Count
'            End If
'
'
'            intRowsCountOfRecords = MoveNextTransposeCopyFromRecordSet(robjOutputDTCol, objTopLeftDataTableRange, vobjRSet, vstrSuppressingColumnsDelimitedComma)
'
'            ' temporary decoration
'
'            Set objRange = objTopLeftDataTableRange.Resize(intColumnsCount, intRowsCountOfRecords)
'
'            DecorateRangeOfRecords objRange, False, RecordCellsBlacksAndGrayInsideVertical
'        End If
'    End With
'
'    TransposeOutputOnlyRecordsetDataTableToCellsOnSheet = intRowsCountOfRecords
'End Function
'
'
'
'
''''
'''' Substitute this for Range.CopyFromRecordSet
''''
'''' because the CopyFromRecordSet causes often some errors which is hard to understand
''''
'Public Function MoveNextTransposeCopyFromRecordSet(ByRef robjOutputDTCol As Collection, _
'        ByVal vobjRecordTopLeftRange As Excel.Range, _
'        ByVal vobjRSet As ADODB.Recordset, _
'        Optional ByVal vstrSuppressingColumnsDelimitedComma As String = "") As Long
'
'
'    Dim objSheet As Excel.Worksheet, intRowIdx As Long, intColumnIdx As Long
'    Dim intRecordTopLeftRowIndex As Long, intRecordTopLeftColumnIndex As Long, intColumnsCountOfDataTable As Long
'    Dim i As Long
'
'    Dim objDTCol As Collection, objRowValues As Collection, varTransposeValues As Variant
'    Dim objUnionRange As Excel.Range, objRange As Excel.Range, intCountOfDataTableRows As Long
'    Dim intRowsCountOfRecordset As Long
'
'    Set objSheet = vobjRecordTopLeftRange.Worksheet
'
'
'    intRowIdx = intRecordTopLeftRowIndex
'
'    With vobjRecordTopLeftRange
'
'        intRecordTopLeftRowIndex = .Row
'
'        intRecordTopLeftColumnIndex = .Column
'    End With
'
'    intColumnsCountOfDataTable = vobjRSet.Fields.Count
'
'    TransposeConvertRSetToCollectionAndGetStringRanges objDTCol, objUnionRange, vobjRSet, objSheet, intRecordTopLeftRowIndex, intRecordTopLeftColumnIndex, vstrSuppressingColumnsDelimitedComma
'
'    Set robjOutputDTCol = objDTCol
'
'    On Error Resume Next
'
'    If Not objUnionRange Is Nothing Then
'
'        objUnionRange.NumberFormatLocal = "@"
'    End If
'
'    msubTransposeOutputDTCol objDTCol, intColumnsCountOfDataTable, objSheet, intRecordTopLeftRowIndex, intRecordTopLeftColumnIndex
'
'    intRowsCountOfRecordset = objDTCol.Count
'
'    On Error GoTo 0
'
'    MoveNextTransposeCopyFromRecordSet = intRowsCountOfRecordset
'End Function
'
''''
''''
''''
'Private Sub msubTransposeOutputDTCol(ByVal vobjDTCol As Collection, _
'        ByVal vintColumnsCountOfDataTable As Long, _
'        ByVal vobjSheet As Excel.Worksheet, _
'        Optional ByVal vintRecordTopLeftRowIndex As Long = 1, _
'        Optional ByVal vintRecordTopLeftColumnIndex As Long = 1)
'
'    Dim objRange As Excel.Range, varTransposeValues As Variant
'
'    Set objRange = vobjSheet.Cells(vintRecordTopLeftRowIndex, vintRecordTopLeftColumnIndex)
'
'    GetTransposeRCValuesFromRCCollection varTransposeValues, vobjDTCol
'
'    objRange.Resize(vintColumnsCountOfDataTable, vobjDTCol.Count).Value = varTransposeValues
'End Sub
'
'
''''
''''
''''
'Public Sub TransposeConvertDTColToCollectionAndGetStringRanges(ByRef robjOutputCol As Collection, _
'        ByRef robjStringUnionRange As Excel.Range, _
'        ByRef robjRSet As ADODB.Recordset, _
'        ByRef robjSheet As Excel.Worksheet, _
'        ByRef rintRecordTopLeftRowIndex As Long, _
'        ByRef rintRecordTopLeftColumnIndex As Long, _
'        Optional ByVal vstrSuppressingColumnsDelimitedComma As String = "")
'
'    On Error Resume Next
'
'    Dim objField As ADOR.Field, i As Long, objRowCol As Collection, varInputRowCol As Variant, objInputRowCol As Collection, varInputItem As Variant
'    Dim intColumnIdx As Long, intRowIdx As Long, objSuppressingColumnsDic As Scripting.Dictionary, varColumn As Variant
'
'    Dim intColumnDecrement As Long, blnContinueToAddValue As Boolean
'
'
'    intRowIdx = rintRecordTopLeftRowIndex
'
'    intColumnDecrement = 0
'
'    SetUpSuppressingColumnsDic objSuppressingColumnsDic, vstrSuppressingColumnsDelimitedComma
'
'
'
'    Set robjOutputCol = New Collection
'
'    Set robjStringUnionRange = Nothing
'
'    With robjRSet
'
'        For Each varInputRowCol In robjInputDTCol
'
'            Set objRowCol = New Collection
'
'            i = 0
'
'            Set objInputRowCol = varInputRowCol
'
'            For Each varInputItem In objInputRowCol
'
'                intColumnIdx = rintRecordTopLeftColumnIndex + i
'
'                blnContinueToAddValue = True
'
'                If vstrSuppressingColumnsDelimitedComma <> "" Then
'
'                    If objSuppressingColumnsDic.Exists(intColumnIdx) Then
'
'                        blnContinueToAddValue = False
'
'                        intColumnDecrement = intColumnDecrement + 1
'                    End If
'                End If
'
'                If blnContinueToAddValue Then
'
'                    SetTransposeRowColValueAndStringUnionRange robjSheet, objRowCol, varInputItem, robjStringUnionRange, intRowIdx, intColumnIdx, intColumnDecrement
'                End If
'
'                i = i + 1
'            Next
'
'            robjOutputCol.Add objRowCol
'
'            intRowIdx = intRowIdx + 1
'        Next
'    End With
'
'    On Error GoTo 0
'End Sub
'
'
'
''''
''''
''''
'Public Sub TransposeConvertRSetToCollectionAndGetStringRanges(ByRef robjDTCol As Collection, _
'        ByRef robjStringUnionRange As Excel.Range, _
'        ByRef robjRSet As ADODB.Recordset, _
'        ByRef robjSheet As Excel.Worksheet, _
'        ByRef rintRecordTopLeftRowIndex As Long, _
'        ByRef rintRecordTopLeftColumnIndex As Long, _
'        Optional ByVal vstrSuppressingColumnsDelimitedComma As String = "")
'
'    On Error Resume Next
'
'    Dim objField As ADOR.Field, i As Long, objRowCol As Collection
'    Dim intColumnIdx As Long, intRowIdx As Long, objSuppressingColumnsDic As Scripting.Dictionary, varColumn As Variant
'
'    Dim intColumnDecrement As Long, blnContinueToAddValue As Boolean
'
'
'    intRowIdx = rintRecordTopLeftRowIndex
'
'    intColumnDecrement = 0
'
'    SetUpSuppressingColumnsDic objSuppressingColumnsDic, vstrSuppressingColumnsDelimitedComma
'
'
'
'    Set robjDTCol = New Collection
'
'    Set robjStringUnionRange = Nothing
'
'    With robjRSet
'
'        While Not .EOF
'
'            Set objRowCol = New Collection
'
'            i = 0
'
'            For Each objField In .Fields
'
'                intColumnIdx = rintRecordTopLeftColumnIndex + i
'
'                blnContinueToAddValue = True
'
'                If vstrSuppressingColumnsDelimitedComma <> "" Then
'
'                    If objSuppressingColumnsDic.Exists(intColumnIdx) Then
'
'                        blnContinueToAddValue = False
'
'                        intColumnDecrement = intColumnDecrement + 1
'                    End If
'                End If
'
'                If blnContinueToAddValue Then
'
'                    With objField
'
'                        SetTransposeRowColValueAndStringUnionRange robjSheet, objRowCol, .Value, robjStringUnionRange, intRowIdx, intColumnIdx, intColumnDecrement
'                    End With
'                End If
'
'                i = i + 1
'            Next
'
'            robjDTCol.Add objRowCol
'
'            intRowIdx = intRowIdx + 1
'
'            .MoveNext
'        Wend
'    End With
'
'    On Error GoTo 0
'End Sub
'
'
'
''''
''''
''''
'Public Sub SetTransposeRowColValueAndStringUnionRange(ByRef robjSheet As Excel.Worksheet, _
'        ByRef robjRowCol As Collection, _
'        ByRef rvarValue As Variant, _
'        ByRef robjStringUnionRange As Excel.Range, _
'        ByRef rintRowIdx As Long, _
'        ByRef rintColumnIdx As Long, _
'        ByRef rintColumnDecrement As Long)
'
'
'    robjRowCol.Add rvarValue
'
'    If IsNumeric(rvarValue) Then
'
'        If TypeName(rvarValue) = "String" Then
'
'            If StrComp(Left(rvarValue, 1), "0") = 0 Then
'
'                If robjStringUnionRange Is Nothing Then
'
'                    ' transposed
'
'                    Set robjStringUnionRange = robjSheet.Cells(rintColumnIdx - rintColumnDecrement, rintRowIdx)
'                Else
'                    ' transposed
'
'                    Set robjStringUnionRange = Union(robjStringUnionRange, robjSheet.Cells(rintColumnIdx - rintColumnDecrement, rintRowIdx))
'                End If
'            End If
'        End If
'    End If
'End Sub
'
'
'''--VBA_Code_File--<UTfADOExSheetOut.bas>--
'Attribute VB_Name = "UTfADOExSheetOut"
''
''   Using a local Excel ADO connection, sanity tests to create a SQL result Excel sheet with additional information by using ADODB.Recordset
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on both ADO and Excel
''       Dependent on UTfDecorationSetterToXlSheet.bas, LoadAndModifyBook.bas
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2024 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Sun, 14/Jan/2024    Kalmclaeyd Tarclanus    Initial creation
''
'
'Option Explicit
'
'
''///////////////////////////////////////////////
''/// Control VBE windows
''///////////////////////////////////////////////
''**---------------------------------------------
''** Open relative VBE code-panes
''**---------------------------------------------
'Private Sub OpenVBECodePaneRelativeToPTfADOExSheetOut()
'
'    ' Focusing on this procdure, please press [F5] key to start this. Therefore this scope is private.
'
'    On Error Resume Next ' If the 'ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma' isn't included in the VBA project, the following causes no syntax error
'
'    Application.Run "ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma", mfstrGetVBEModuleNamesToOpen()
'End Sub
'
'Private Function mfstrGetVBEModuleNamesToOpen() As String
'
'    Dim strNames As String: strNames = "ADOExSheetOut,ADOSheetOut,SolveSavePathGeneral"
'
'    mfstrGetVBEModuleNamesToOpen = strNames
'End Function
'
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
'
''''
''''
''''
'Private Sub msubSanityTestToRSetAdditionalInfosAndNormalOutputTest()
'
'
'    'msubRSetAdditionalInfosAndNormalOutputTest 5, 14, 2, 3
'
'    'msubRSetAdditionalInfosAndNormalOutputTest 5, 14, 2, 3, Nothing, "", False, True
'
'
'    msubRSetAdditionalInfosAndNormalOutputTest 5, 14, 2, 3, GetTextToRealNumberDicFromLineDelimitedChar("Column1,5,Column3,12,Column4,15"), "", False, True
'
'End Sub
'
'
''''
''''
''''
'Private Sub msubSanityTestToRSetNormalOutputTest()
'
'
'    ' msubRSetNormalOutputTest 1, 1, 1, 1
'
'    ' msubRSetNormalOutputTest 3, 2, 1, 1
'
'    'msubRSetNormalOutputTest 6, 4, 2, 3
'
'    'msubRSetNormalOutputTest 5, 14, 2, 3
'
'    msubRSetNormalOutputTest 5, 14, 2, 3, Nothing, "5,6,9,11", True
'
'
'    ' msubRSetTransposeOutputTest 3, 2, 1, 1
'
'    'msubRSetTransposeOutputTest 5, 3, 2, 4
'
'    'msubRSetTransposeOutputTest 6, 4, 2, 3
'
''    msubRSetTransposeOutputTest 4, 11, 2, 3
''
'    'msubRSetTransposeOutputTest 4, 15, 2, 3, Nothing, "5,7,10,11", True
'
'End Sub
'
'
'
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
'
''''
''''
''''
'Private Sub msubRSetTransposeOutputTest(ByVal vintInputTableRowMax As Long, _
'        ByVal vintInputTableColumnMax As Long, _
'        Optional ByVal vintFieldTitlesRowIndex As Long = 1, _
'        Optional ByVal vintTopLeftColumnIndex As Long = 1, _
'        Optional ByVal vstrNullColumnIndexDelimitedComma As String = "", _
'        Optional ByVal vblnAllowToCreateNullColumnsCompressedTable As Boolean = False)
'
'
'    Dim strOutputBookPath As String, strOutputBookDir As String, objOutputBook As Excel.Workbook
'    Dim objOutputSheet As Excel.Worksheet
'
'    Dim strSQL As String, objRSet As ADODB.Recordset, intRowsCountOfRecords As Long
'
'
'    strOutputBookDir = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\AdoExSheetOutTests"
'
'    If vstrNullColumnIndexDelimitedComma <> "" Then
'
'        strOutputBookPath = strOutputBookDir & "\RSetTransposeOutputTest_" & "R" & CStr(vintInputTableRowMax) & "C" & CStr(vintInputTableColumnMax) & "_TopLeft" & CStr(vintFieldTitlesRowIndex) & "_" & CStr(vintTopLeftColumnIndex) & "_WithNullColumns.xlsx"
'    Else
'        strOutputBookPath = strOutputBookDir & "\RSetTransposeOutputTest_" & "R" & CStr(vintInputTableRowMax) & "C" & CStr(vintInputTableColumnMax) & "_TopLeft" & CStr(vintFieldTitlesRowIndex) & "_" & CStr(vintTopLeftColumnIndex) & ".xlsx"
'    End If
'
'    Set objOutputSheet = GetWorkbookAndAnWorksheetPrepareForUnitTest(strOutputBookPath, "RSetTransposeOut")
'
'    With New ADODB.Connection
'
'        .ConnectionString = mfstrGetAdoConnectionStringToTestingExcelBook(vintInputTableRowMax, vintInputTableColumnMax, vstrNullColumnIndexDelimitedComma)
'
'        On Error Resume Next
'
'        .Open
'
'        If Err.Number = 0 Then
'
'            strSQL = "SELECT * FROM [TestTable$]"
'
'            Set objRSet = .Execute(strSQL)
'
'            If Err.Number = 0 Then
'
'                ' Transpose data-table output
'
'                TransposeOutputRecordsetToCellsOnSheet objOutputSheet, objRSet, vintFieldTitlesRowIndex, vintTopLeftColumnIndex, "", vblnAllowToCreateNullColumnsCompressedTable
'            End If
'
'            .Close
'        End If
'
'        On Error GoTo 0
'    End With
'End Sub
'
'
'
''''
''''
''''
'Private Sub msubRSetAdditionalInfosAndNormalOutputTest(ByVal vintInputTableRowMax As Long, _
'        ByVal vintInputTableColumnMax As Long, _
'        Optional ByVal vintFieldTitlesRowIndex As Long = 1, _
'        Optional ByVal vintTopLeftColumnIndex As Long = 1, _
'        Optional ByVal vobjFieldTitleToColumnWidthDic As Scripting.Dictionary = Nothing, _
'        Optional ByVal vstrNullColumnIndexDelimitedComma As String = "", _
'        Optional ByVal vblnAllowToCreateNullColumnsCompressedTable As Boolean = False, _
'        Optional ByVal vblnAllowToAddColumnNo As Boolean = False)
'
'
'    Dim strOutputBookPath As String, strOutputBookDir As String, objOutputBook As Excel.Workbook
'    Dim objOutputSheet As Excel.Worksheet
'
'    Dim strSQL As String, objRSet As ADODB.Recordset, intRowsCountOfRecords As Long
'
'    Dim objInfoTypeToTitleToValueDic As Scripting.Dictionary
'
'
'    strOutputBookDir = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\AdoExSheetOutTests"
'
'    If vstrNullColumnIndexDelimitedComma <> "" Then
'
'        strOutputBookPath = strOutputBookDir & "\RSetAdditionalInfosPlusNormalOutputTest_" & "R" & CStr(vintInputTableRowMax) & "C" & CStr(vintInputTableColumnMax) & "_TopLeft" & CStr(vintFieldTitlesRowIndex) & "_" & CStr(vintTopLeftColumnIndex) & "_WithNullColumns.xlsx"
'    Else
'        strOutputBookPath = strOutputBookDir & "\RSetAdditionalInfosPlusNormalOutputTest_" & "R" & CStr(vintInputTableRowMax) & "C" & CStr(vintInputTableColumnMax) & "_TopLeft" & CStr(vintFieldTitlesRowIndex) & "_" & CStr(vintTopLeftColumnIndex) & ".xlsx"
'    End If
'
'    Set objOutputSheet = GetWorkbookAndAnWorksheetPrepareForUnitTest(strOutputBookPath, "RSetNormalOut")
'
'    Set objInfoTypeToTitleToValueDic = GetInfoTypeToTitleToValueDicOfALeastSamples(vintInputTableColumnMax, vblnAllowToAddColumnNo)
'
'    With New ADODB.Connection
'
'        .ConnectionString = mfstrGetAdoConnectionStringToTestingExcelBook(vintInputTableRowMax, vintInputTableColumnMax, vstrNullColumnIndexDelimitedComma)
'
'        On Error Resume Next
'
'        .Open
'
'        If Err.Number = 0 Then
'
'            strSQL = "SELECT * FROM [TestTable$]"
'
'            Set objRSet = .Execute(strSQL)
'
'            If Err.Number = 0 Then
'
'                ' Normal data-table output
'
'                NormalOutputRecordsetWithAdditionalInfoToCellsOnSheet objOutputSheet, objRSet, objInfoTypeToTitleToValueDic, vintFieldTitlesRowIndex, vintTopLeftColumnIndex, vobjFieldTitleToColumnWidthDic, "", vblnAllowToCreateNullColumnsCompressedTable
'            End If
'
'            .Close
'        End If
'
'        On Error GoTo 0
'    End With
'
'
'    AddAutoShapeGeneralMessage objOutputSheet, "Test-log"
'End Sub
'
''''
''''
''''
'''' <Return>Dictionary(Of Variant[info-type], Dictionary(Variant[column-name], Variant[Info]))</Return>
'Private Function GetInfoTypeToTitleToValueDicOfALeastSamples(ByVal vintColumnMax As Long, Optional ByVal vblnAllowToAddColumnNo As Boolean = False) As Scripting.Dictionary
'
'
'    Dim objInfoTypeToTitleToValueDic As Scripting.Dictionary, strInfoTypeKey As String, objTitleToValueDic As Scripting.Dictionary
'
'    Dim objFieldTitleToColumnDescriptionDic As Scripting.Dictionary, objFieldTitleToColumnTypeNameDic As Scripting.Dictionary, objFieldTitleToColumnTypeLengthDic As Scripting.Dictionary
'
'    Dim varKey As Variant, i As Long, objFieldTitleToNumberDic As Scripting.Dictionary
'
'
'    Set objInfoTypeToTitleToValueDic = New Scripting.Dictionary
'
'    SetupPadAdditionalInfoDicsAboutColumnNames objFieldTitleToColumnDescriptionDic, objFieldTitleToColumnTypeNameDic, objFieldTitleToColumnTypeLengthDic, vintColumnMax
'
'    Set objInfoTypeToTitleToValueDic = New Scripting.Dictionary
'
'    With objInfoTypeToTitleToValueDic
'
'        If vblnAllowToAddColumnNo Then
'
'            Set objFieldTitleToNumberDic = New Scripting.Dictionary
'
'            With objFieldTitleToColumnDescriptionDic
'
'                i = 1
'
'                For Each varKey In .Keys
'
'                    objFieldTitleToNumberDic.Add varKey, i
'
'                    i = i + 1
'                Next
'            End With
'
'            strInfoTypeKey = "2_ColumnNo"
'
'            .Add strInfoTypeKey, objFieldTitleToNumberDic
'        End If
'
'
'        strInfoTypeKey = "3_ColumnTypeName"
'
'        .Add strInfoTypeKey, objFieldTitleToColumnTypeNameDic
'
'        strInfoTypeKey = "4_ColumnLengthOfType"
'
'        .Add strInfoTypeKey, objFieldTitleToColumnTypeLengthDic
'
'        strInfoTypeKey = "5_ColumnDescription"
'
'        .Add strInfoTypeKey, objFieldTitleToColumnDescriptionDic
'    End With
'
'    Set GetInfoTypeToTitleToValueDicOfALeastSamples = objInfoTypeToTitleToValueDic
'End Function
'
'
'
''''
''''
''''
'Private Sub msubRSetNormalOutputTest(ByVal vintInputTableRowMax As Long, _
'        ByVal vintInputTableColumnMax As Long, _
'        Optional ByVal vintFieldTitlesRowIndex As Long = 1, _
'        Optional ByVal vintTopLeftColumnIndex As Long = 1, _
'        Optional ByVal vobjFieldTitleToColumnWidthDic As Scripting.Dictionary = Nothing, _
'        Optional ByVal vstrNullColumnIndexDelimitedComma As String = "", _
'        Optional ByVal vblnAllowToCreateNullColumnsCompressedTable As Boolean = False)
'
'
'    Dim strOutputBookPath As String, strOutputBookDir As String, objOutputBook As Excel.Workbook
'    Dim objOutputSheet As Excel.Worksheet
'
'    Dim strSQL As String, objRSet As ADODB.Recordset, intRowsCountOfRecords As Long
'
'
'    strOutputBookDir = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\AdoExSheetOutTests"
'
'    If vstrNullColumnIndexDelimitedComma <> "" Then
'
'        strOutputBookPath = strOutputBookDir & "\RSetNormalOutputTest_" & "R" & CStr(vintInputTableRowMax) & "C" & CStr(vintInputTableColumnMax) & "_TopLeft" & CStr(vintFieldTitlesRowIndex) & "_" & CStr(vintTopLeftColumnIndex) & "_WithNullColumns.xlsx"
'    Else
'        strOutputBookPath = strOutputBookDir & "\RSetNormalOutputTest_" & "R" & CStr(vintInputTableRowMax) & "C" & CStr(vintInputTableColumnMax) & "_TopLeft" & CStr(vintFieldTitlesRowIndex) & "_" & CStr(vintTopLeftColumnIndex) & ".xlsx"
'    End If
'
'    Set objOutputSheet = GetWorkbookAndAnWorksheetPrepareForUnitTest(strOutputBookPath, "RSetNormalOut")
'
'    With New ADODB.Connection
'
'        .ConnectionString = mfstrGetAdoConnectionStringToTestingExcelBook(vintInputTableRowMax, vintInputTableColumnMax, vstrNullColumnIndexDelimitedComma)
'
'        On Error Resume Next
'
'        .Open
'
'        If Err.Number = 0 Then
'
'            strSQL = "SELECT * FROM [TestTable$]"
'
'            Set objRSet = .Execute(strSQL)
'
'            If Err.Number = 0 Then
'
'                ' Normal data-table output
'
'                NormalOutputRecordsetToCellsOnSheet objOutputSheet, objRSet, vintFieldTitlesRowIndex, vintTopLeftColumnIndex, vobjFieldTitleToColumnWidthDic, "", vblnAllowToCreateNullColumnsCompressedTable
'            End If
'
'            .Close
'        End If
'
'        On Error GoTo 0
'    End With
'End Sub
'
'
''''
''''
''''
'Private Function mfstrGetAdoConnectionStringToTestingExcelBook(ByVal vintRowMax As Long, ByVal vintColumnMax As Long, _
'        Optional ByVal vstrNullColumnIndexDelimitedComma As String = "") As String
'
'    Dim strBookPath As String
'
'    strBookPath = mfstrGetBookPathWithCreatingTestBookIfItDoesntExist(vintRowMax, vintColumnMax, vstrNullColumnIndexDelimitedComma)
'
'    mfstrGetAdoConnectionStringToTestingExcelBook = GetADODBConnectionOleDbStringToXlBook(strBookPath)
'End Function
'
''''
''''
''''
'Private Function mfstrGetBookPathWithCreatingTestBookIfItDoesntExist(ByVal vintRowMax As Long, ByVal vintColumnMax As Long, _
'        Optional ByVal vstrNullColumnIndexDelimitedComma As String = "") As String
'
'    Dim strBookPath As String
'
'    strBookPath = mfstrGetTestBookPath(vintRowMax, vintColumnMax, vstrNullColumnIndexDelimitedComma)
'
'    If Not FileExistsByVbaDir(strBookPath) Then
'
'        strBookPath = mfstrGetBookPathAfterCreatingTestBook(vintRowMax, vintColumnMax, vstrNullColumnIndexDelimitedComma)
'
'
'
'
'    End If
'
'    mfstrGetBookPathWithCreatingTestBookIfItDoesntExist = strBookPath
'End Function
'
''''
''''
''''
'Private Function mfstrGetBookPathAfterCreatingTestBook(ByVal vintRowMax As Long, ByVal vintColumnMax As Long, _
'        Optional ByVal vstrNullColumnIndexDelimitedComma As String = "") As String
'
'    Dim strBookPath As String, objBook As Excel.Workbook
'
'    strBookPath = mfstrGetTestBookPath(vintRowMax, vintColumnMax, vstrNullColumnIndexDelimitedComma)
'
'    Set objBook = mfobjGetSampleSimpleIntegerAndStringTableBook(strBookPath, "TestTable", vintRowMax, vintColumnMax, vstrNullColumnIndexDelimitedComma)
'
'    ForceToSaveAsBookWithoutDisplayAlert objBook, strBookPath
'
'    mfstrGetBookPathAfterCreatingTestBook = strBookPath
'End Function
'
''''
''''
''''
'Private Function mfstrGetTestBookPath(ByVal vintRowMax As Long, ByVal vintColumnMax As Long, _
'        Optional ByVal vstrNullColumnIndexDelimitedComma As String = "") As String
'
'    Dim strTmpDir As String, strBookName As String, strBookPath As String
'
'    strTmpDir = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\AdoExSheetOutTests"
'
'    If vstrNullColumnIndexDelimitedComma <> "" Then
'
'        strBookName = "TmpBook_" & Format(vintRowMax, "00") & "_" & Format(vintColumnMax) & "_WithNullColumns" & ".xlsx"
'    Else
'        strBookName = "TmpBook_" & Format(vintRowMax, "00") & "_" & Format(vintColumnMax) & ".xlsx"
'    End If
'
'    strBookPath = strTmpDir & "\" & strBookName
'
'    mfstrGetTestBookPath = strBookPath
'End Function
'
''''
''''
''''
'Private Function mfobjGetSampleSimpleIntegerAndStringTableBook(ByVal vstrBookPath As String, _
'        ByVal vstrSheetName As String, _
'        Optional ByVal vintRowMax As Long = 1, _
'        Optional ByVal vintColumnMax As Long = 1, _
'        Optional ByVal vstrNullColumnIndexDelimitedComma As String = "") As Excel.Workbook
'
'    Dim objDTCol As Collection, objFieldTitlesCol As Collection, objBook As Excel.Workbook, objSheet As Excel.Worksheet
'    Dim objDTSheetFormatter As DataTableSheetFormatter
'
'
'    SetupPadSimpleIntegerAndStringWithNullColumnsForRCTableColAndFieldTitles objDTCol, objFieldTitlesCol, vintRowMax, vintColumnMax, 4, vstrNullColumnIndexDelimitedComma
'
'    Set objSheet = GetWorkbookAndAnWorksheetPrepareForUnitTest(vstrBookPath, vstrSheetName)
'
'    Set objDTSheetFormatter = New DataTableSheetFormatter
'
'    objDTSheetFormatter.RecordBordersType = RecordCellsGrayAll
'
'    OutputColToSheet objSheet, objDTCol, objFieldTitlesCol, objDTSheetFormatter
'
'    Set objBook = objSheet.Parent
'
'    Set mfobjGetSampleSimpleIntegerAndStringTableBook = objBook
'End Function
'
'
'
''///////////////////////////////////////////////
''/// Sanity tests
''///////////////////////////////////////////////
''''
''''
''''
'Private Sub msubSanityTestToGetSampleSimpleIntegerAndStringTableBook()
'
'    Dim objBook As Excel.Workbook, strBookPath As String, strTmpBookDir As String
'
'
'    strTmpBookDir = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\AdoExSheetOutTests"
'
'    strBookPath = strTmpBookDir & "\TestIntAndStringData.xlsx"
'
'    Set objBook = mfobjGetSampleSimpleIntegerAndStringTableBook(strBookPath, "TestTable", 1, 6)
'End Sub
'
''''
''''
''''
'Private Sub msubSanityTestToGetBookPathAfterCreatingTestBook()
'
'    Debug.Print mfstrGetBookPathAfterCreatingTestBook(3, 4)
'End Sub
'
'''--VBA_Code_File--<ADOSheetFormatter.cls>--
'VERSION 1.0 CLASS
'BEGIN
'  MultiUse = -1  'True
'END
'Attribute VB_Name = "ADOSheetFormatter"
'Attribute VB_GlobalNameSpace = False
'Attribute VB_Creatable = False
'Attribute VB_PredeclaredId = False
'Attribute VB_Exposed = False
''
''   Excel.Worksheet decoration format parameters for outputting ADO Recordset data-table on cells in the Worksheet
''   SQL query logging parameters data object
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on both the ADO and the EXCEL
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Tue,  7/Feb/2023    Kalmclaeyd Tarclanus    Initial Creation
''       Wed,  9/Aug/2023    Kalmclaeyd Tarclanus    Added
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Declarations
''///////////////////////////////////////////////
''**---------------------------------------------
''** Variable declarations
''**---------------------------------------------
'Private mobjSheetFormatter As DataTableSheetFormatter
'
'Private mstrSQL As String
'
'
'Private menmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType    ' for SQL query , which is the SELECT sentenve
'
'Private menmExecutedAdoSqlCommandLogPositionType As ExecutedAdoSqlCommandLogPositionType ' for SQL command, such as UPDATE, INSERT, DELETE, etc all
'
'Private mstrExclusiveSQLLogSheetName As String
'
''**---------------------------------------------
''** Temporary state
''**---------------------------------------------
'Private mblnSupportedPropertyRecordCount As Boolean
'
'Private mintExcelSheetOutputTime1 As Long
'
'Private mintExcelSheetOutputTime2 As Long
'
'
''///////////////////////////////////////////////
''/// Event Handlers
''///////////////////////////////////////////////
'Private Sub Class_Initialize()
'
'    Set mobjSheetFormatter = New DataTableSheetFormatter
'
'    mstrSQL = ""
'
'    mblnSupportedPropertyRecordCount = False
'
'    menmExecutedAdoSqlQueryLogPositionType = SqlQueryLogsByAutoShapesOnSheetWithOutputtingRecordsetDataTable
'
'    mstrExclusiveSQLLogSheetName = "ExclusiveSQLLogs"
'End Sub
'
'
''///////////////////////////////////////////////
''/// Properties
''///////////////////////////////////////////////
''''
''''
''''
'Public Property Get DataTableSheetFormattingInterface() As IDataTableSheetFormatter
'
'    Set DataTableSheetFormattingInterface = mobjSheetFormatter
'End Property
'
''**---------------------------------------------
''** For general
''**---------------------------------------------
'Public Property Get FieldTitlesRowIndex() As Long
'
'    FieldTitlesRowIndex = mobjSheetFormatter.FieldTitlesRowIndex
'End Property
'Public Property Let FieldTitlesRowIndex(ByVal vintFieldTitlesRowIndex As Long)
'
'    mobjSheetFormatter.FieldTitlesRowIndex = vintFieldTitlesRowIndex
'End Property
'
'
'Public Property Get TopLeftRowIndex() As Long
'
'    TopLeftRowIndex = mobjSheetFormatter.TopLeftRowIndex
'End Property
'Public Property Let TopLeftRowIndex(ByVal vintTopLeftRowIndex As Long)
'
'    mobjSheetFormatter.TopLeftRowIndex = vintTopLeftRowIndex
'End Property
'Public Property Get TopLeftColumnIndex() As Long
'
'    TopLeftColumnIndex = mobjSheetFormatter.TopLeftColumnIndex
'End Property
'Public Property Let TopLeftColumnIndex(ByVal vintTopLeftColumnIndex As Long)
'
'    mobjSheetFormatter.TopLeftColumnIndex = vintTopLeftColumnIndex
'End Property
'
'
'Public Property Let AllowToShowFieldTitle(ByVal vblnAllowToShowFieldTitle As Boolean)
'
'    mobjSheetFormatter.AllowToShowFieldTitle = vblnAllowToShowFieldTitle
'End Property
'Public Property Get AllowToShowFieldTitle() As Boolean
'
'    AllowToShowFieldTitle = mobjSheetFormatter.AllowToShowFieldTitle
'End Property
'
'Public Property Let FieldTitleInteriorType(ByVal venmFieldTitleInteriorType As FieldTitleInterior)
'
'    mobjSheetFormatter.FieldTitleInteriorType = venmFieldTitleInteriorType
'End Property
'Public Property Get FieldTitleInteriorType() As FieldTitleInterior
'
'    FieldTitleInteriorType = mobjSheetFormatter.FieldTitleInteriorType
'End Property
'
'Public Property Let RecordBordersType(ByVal venmRecordCellsBordersType As RecordCellsBorders)
'
'    mobjSheetFormatter.RecordBordersType = venmRecordCellsBordersType
'End Property
'Public Property Get RecordBordersType() As RecordCellsBorders
'
'    RecordBordersType = mobjSheetFormatter.RecordBordersType
'End Property
'
'Public Property Let RecordsFontSize(ByVal vsngRecordsFontSize As Single)
'
'    mobjSheetFormatter.RecordsFontSize = vsngRecordsFontSize
'End Property
'Public Property Get RecordsFontSize() As Single
'
'    RecordsFontSize = mobjSheetFormatter.RecordsFontSize
'End Property
'
''**---------------------------------------------
''** ADO dependent properties
''**---------------------------------------------
'Public Property Get SupportedPropertyRecordCount() As Boolean
'
'    SupportedPropertyRecordCount = mblnSupportedPropertyRecordCount
'End Property
'
''''
'''' output SQL SELECT result ADODB.Recordset datatable to whether outputted data-table Excel sheet or the other SQL log sheet, or output it on some AutoShapes with the outputted data-table
''''
'Public Property Get ExecutedAdoSqlQueryLogPosition() As ExecutedAdoSqlQueryLogPositionType
'
'    ExecutedAdoSqlQueryLogPosition = menmExecutedAdoSqlQueryLogPositionType
'End Property
'Public Property Let ExecutedAdoSqlQueryLogPosition(ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType)
'
'    menmExecutedAdoSqlQueryLogPositionType = venmExecutedAdoSqlQueryLogPositionType
'End Property
'
''''
'''' output SQL command result to cells on logging Excel sheet
''''
'Public Property Get ExecutedAdoSqlCommandLogPosition() As ExecutedAdoSqlCommandLogPositionType
'
'    ExecutedAdoSqlCommandLogPosition = menmExecutedAdoSqlCommandLogPositionType
'End Property
'Public Property Let ExecutedAdoSqlCommandLogPosition(ByVal venmExecutedAdoSqlCommandLogPositionType As ExecutedAdoSqlCommandLogPositionType)
'
'    menmExecutedAdoSqlCommandLogPositionType = venmExecutedAdoSqlCommandLogPositionType
'End Property
'
''''
''''
''''
'Public Property Get ExclusiveSQLLogSheetName() As String
'
'    ExclusiveSQLLogSheetName = mstrExclusiveSQLLogSheetName
'End Property
'
'Public Property Let ExclusiveSQLLogSheetName(ByVal vstrExclusiveSQLLogSheetName As String)
'
'    mstrExclusiveSQLLogSheetName = vstrExclusiveSQLLogSheetName
'End Property
'
''**---------------------------------------------
''** Properties - connoted sheet-formatting objects or paramters
''**---------------------------------------------
'
'Public Property Let AllowToWriteInsertTextInSheetHeader(ByVal vblnAllowToWriteInsertTextInSheetHeader As Boolean)
'
'    mobjSheetFormatter.AllowToWriteInsertTextInSheetHeader = vblnAllowToWriteInsertTextInSheetHeader
'End Property
'Public Property Get AllowToWriteInsertTextInSheetHeader() As Boolean
'
'    AllowToWriteInsertTextInSheetHeader = mobjSheetFormatter.AllowToWriteInsertTextInSheetHeader
'End Property
'
'Public Property Set HeaderInsertTexts(ByVal vobjHeaderInsertTexts As Collection)
'
'    Set mobjSheetFormatter.HeaderInsertTexts = vobjHeaderInsertTexts
'End Property
'Public Property Get HeaderInsertTexts() As Collection
'
'    Set HeaderInsertTexts = mobjSheetFormatter.HeaderInsertTexts
'End Property
'
'
'Public Property Let AllowToSetAutoFilter(ByVal vblnAllowToSetAutoFilter As Boolean)
'
'    mobjSheetFormatter.AllowToSetAutoFilter = vblnAllowToSetAutoFilter
'End Property
'Public Property Get AllowToSetAutoFilter() As Boolean
'
'    AllowToSetAutoFilter = mobjSheetFormatter.AllowToSetAutoFilter
'End Property
'
''''
'''' Dictionary(Of String(column title), Double(column-width))
''''
'Public Property Set FieldTitleToColumnWidthDic(ByVal vobjFieldTitleToColumnWidthDic As Scripting.Dictionary)
'
'    Set mobjSheetFormatter.FieldTitleToColumnWidthDic = vobjFieldTitleToColumnWidthDic
'End Property
'Public Property Get FieldTitleToColumnWidthDic() As Scripting.Dictionary
'
'    Set FieldTitleToColumnWidthDic = mobjSheetFormatter.FieldTitleToColumnWidthDic
'End Property
'
''''
'''' Dictionary(Of String(column title order, such as 'Col1', 'Col07'), Double(column-width))
''''
'Public Property Set FieldTitleOrderToColumnWidthDic(ByVal vobjFieldTitleOrderToColumnWidthDic As Scripting.Dictionary)
'
'    Set mobjSheetFormatter.FieldTitleOrderToColumnWidthDic = vobjFieldTitleOrderToColumnWidthDic
'End Property
'Public Property Get FieldTitleOrderToColumnWidthDic() As Scripting.Dictionary
'
'    Set FieldTitleOrderToColumnWidthDic = mobjSheetFormatter.FieldTitleOrderToColumnWidthDic
'End Property
'
'
''''
'''' When it is true, then ConvertDataForColumns procedure is used.
''''
'Public Property Get AllowToConvertDataForSpecifiedColumns() As Boolean
'
'    AllowToConvertDataForSpecifiedColumns = mobjSheetFormatter.AllowToConvertDataForSpecifiedColumns
'End Property
'Public Property Let AllowToConvertDataForSpecifiedColumns(ByVal vblnAllowToConvertDataForSpecifiedColumns As Boolean)
'
'    mobjSheetFormatter.AllowToConvertDataForSpecifiedColumns = vblnAllowToConvertDataForSpecifiedColumns
'End Property
''''
'''' Dictionary(Of String(column title), ColumnDataConvertType(Enum definition))
''''
'Public Property Get FieldTitlesToColumnDataConvertTypeDic() As Scripting.Dictionary
'
'    Set FieldTitlesToColumnDataConvertTypeDic = mobjSheetFormatter.FieldTitlesToColumnDataConvertTypeDic
'End Property
'Public Property Set FieldTitlesToColumnDataConvertTypeDic(ByVal vobjFieldTitlesToColumnDataConvertTypeDic As Scripting.Dictionary)
'
'    Set mobjSheetFormatter.FieldTitlesToColumnDataConvertTypeDic = vobjFieldTitlesToColumnDataConvertTypeDic
'End Property
'
'
''''
'''' ColumnsNumberFormatLocalParam
''''
'Public Property Set ColumnsNumberFormatLocal(ByVal vobjColumnsNumberFormatLocalParam As ColumnsNumberFormatLocalParam)
'
'    Set mobjSheetFormatter.ColumnsNumberFormatLocal = vobjColumnsNumberFormatLocalParam
'End Property
'Public Property Get ColumnsNumberFormatLocal() As ColumnsNumberFormatLocalParam
'
'    Set ColumnsNumberFormatLocal = mobjSheetFormatter.ColumnsNumberFormatLocal
'End Property
'
''''
'''' set format-condition for each column
''''
'Public Property Set ColumnsFormatCondition(ByVal vobjColumnsFormatConditionParam As ColumnsFormatConditionParam)
'
'    Set mobjSheetFormatter.ColumnsFormatCondition = vobjColumnsFormatConditionParam
'End Property
'Public Property Get ColumnsFormatCondition() As ColumnsFormatConditionParam
'
'    Set ColumnsFormatCondition = mobjSheetFormatter.ColumnsFormatCondition
'End Property
'
''''
'''' merge cells when the same values continue
''''
'Public Property Let AllowToMergeCellsByContinuousSameValues(ByVal vblnAllowToMergeCellsByContinuousSameValues As Boolean)
'
'    mobjSheetFormatter.AllowToMergeCellsByContinuousSameValues = vblnAllowToMergeCellsByContinuousSameValues
'End Property
'Public Property Get AllowToMergeCellsByContinuousSameValues() As Boolean
'
'    AllowToMergeCellsByContinuousSameValues = mobjSheetFormatter.AllowToMergeCellsByContinuousSameValues
'End Property
'Public Property Let AllowToSetVerticalTopAlignmentTopOfMergeCells(ByVal vblnAllowToSetVerticalTopAlignmentTopOfMergeCells As Boolean)
'
'    mobjSheetFormatter.AllowToSetVerticalTopAlignmentTopOfMergeCells = vblnAllowToSetVerticalTopAlignmentTopOfMergeCells
'End Property
'Public Property Get AllowToSetVerticalTopAlignmentTopOfMergeCells() As Boolean
'
'    AllowToSetVerticalTopAlignmentTopOfMergeCells = mobjSheetFormatter.AllowToSetVerticalTopAlignmentTopOfMergeCells
'End Property
'
'Public Property Set FieldTitlesToMergeCells(ByVal vobjFieldTitlesToMergeCells As Collection)
'
'    Set mobjSheetFormatter.FieldTitlesToMergeCells = vobjFieldTitlesToMergeCells
'End Property
'Public Property Get FieldTitlesToMergeCells()
'
'    Set FieldTitlesToMergeCells = mobjSheetFormatter.FieldTitlesToMergeCells
'End Property
'
'
''''
'''' Enabling WrapText for each cells
''''
'Public Property Let AllowToSetEnablingWrapText(ByVal vblnAllowToSetEnablingWrapText As Boolean)
'
'    mobjSheetFormatter.AllowToSetEnablingWrapText = vblnAllowToSetEnablingWrapText
'End Property
'Public Property Get AllowToSetEnablingWrapText() As Boolean
'
'    AllowToSetEnablingWrapText = mobjSheetFormatter.AllowToSetEnablingWrapText
'End Property
'
'Public Property Set FieldTitlesToSetEnablingWrapTextCells(ByVal vobjFieldTitlesToSetEnablingWrapTextCells As Collection)
'
'    Set mobjSheetFormatter.FieldTitlesToSetEnablingWrapTextCells = vobjFieldTitlesToSetEnablingWrapTextCells
'End Property
'Public Property Get FieldTitlesToSetEnablingWrapTextCells() As Collection
'
'    Set FieldTitlesToSetEnablingWrapTextCells = mobjSheetFormatter.FieldTitlesToSetEnablingWrapTextCells
'End Property
'
''''
'''' If mblnAllowToAutoFitRowsHeight is true, Excel.Worksheet.Rows.AutoFit method is to be executed
''''
'Public Property Let AllowToAutoFitRowsHeight(ByVal vblnAllowToAutoFitRowsHeight As Boolean)
'
'    mobjSheetFormatter.AllowToAutoFitRowsHeight = vblnAllowToAutoFitRowsHeight
'End Property
'Public Property Get AllowToAutoFitRowsHeight() As Boolean
'
'    AllowToAutoFitRowsHeight = mobjSheetFormatter.AllowToAutoFitRowsHeight
'End Property
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''**---------------------------------------------
''** For general
''**---------------------------------------------
''''
''''
''''
'Public Sub SetSheetFormatBeforeTableOut(Optional ByVal vblnExecutedSQLLogExists As Boolean = True)
'
'    Dim intRowIndex As Long
'
'    Const intCountOfSQLQueryLogCellsRows As Long = 6
'
'
'    intRowIndex = Me.TopLeftRowIndex
'
'    If menmExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable Then
'
'        If Not Me.HeaderInsertTexts Is Nothing And Me.AllowToWriteInsertTextInSheetHeader Then
'
'            intRowIndex = intRowIndex + Me.HeaderInsertTexts.Count
'        End If
'
'        If vblnExecutedSQLLogExists Then
'
'            intRowIndex = intRowIndex + intCountOfSQLQueryLogCellsRows
'        End If
'    End If
'
'    If Not Me.AllowToShowFieldTitle Then
'
'        intRowIndex = intRowIndex - 1
'    End If
'
'    Me.FieldTitlesRowIndex = intRowIndex
'End Sub
'
''''
''''
''''
'Public Sub SetSheetFormatAfterTableOut(ByVal vobjSheet As Excel.Worksheet)
'
'    mobjSheetFormatter.SetDataTableSheetFormatAfterTableOut vobjSheet
'End Sub
'
'
''**---------------------------------------------
''** ADO dependent operations
''**---------------------------------------------
''''
''''
''''
'Public Sub InsertInputtedHeaderTexts(ByVal vobjSheet As Excel.Worksheet)
'
'    ' The SQL string is written at the first row of the sheet
'
'    mobjSheetFormatter.InsertInputtedHeaderTexts vobjSheet, True
'End Sub
''''
''''
''''
'Public Sub PreparationForRecordSet(ByVal vobjSheet As Excel.Worksheet, ByVal vobjRSet As ADODB.Recordset, ByRef robjSQLRes As SQLResult)
'
'    If Not robjSQLRes Is Nothing Then
'
'        DetectRecordsetRecordsCountState mblnSupportedPropertyRecordCount, robjSQLRes, vobjRSet
'
'        GetFieldCountsAndStartToMeasureTimeOfExpandingToSheet robjSQLRes, vobjRSet, mintExcelSheetOutputTime1
'    End If
'End Sub
'
'
''''
''''
''''
'Public Sub ResultOutForAdoSqlExecution(ByVal vobjOutputtedDataTableOrLogSheet As Excel.Worksheet, _
'        ByRef robjSQLRes As SQLResult, _
'        Optional ByVal venmCQRSClassification As SqlRdbCQRSClassification = SqlRdbCQRSClassification.RdbSqlQueryType, _
'        Optional ByVal vobjDataTableSheetRangeAddresses As DataTableSheetRangeAddresses = Nothing)
'
'    Dim intSQLLogStartingRowIndex As Long
'
'    If Not robjSQLRes Is Nothing Then
'
'        GetMeasureTimeOfExpandedRecordsetToSheet robjSQLRes, mintExcelSheetOutputTime1, mintExcelSheetOutputTime2
'
'        'Debug.Print "Excel sheet processing elapsed time : " & CStr(CSng(robjSQLRes.ExcelSheetOutputElapsedTime) / 1000#) & " [s]"
'
'        With Me
'
'            Select Case venmCQRSClassification
'
'                Case SqlRdbCQRSClassification.RdbSqlQueryType
'
'                    OutputAdoSqlQueryResultLogsToSheet vobjOutputtedDataTableOrLogSheet, robjSQLRes, vobjDataTableSheetRangeAddresses, .HeaderInsertTexts, .ExecutedAdoSqlQueryLogPosition, .TopLeftRowIndex, .TopLeftColumnIndex, RdbSqlQueryType, mstrExclusiveSQLLogSheetName
'
'                Case SqlRdbCQRSClassification.RdbSqlCommandType
'
'                    ' In the SQL command log case, the sheet-name has been already decided.
'
'                    OutputAdoSqlCommandResultLogsToCellsOnSpecifiedSQLLogSheet vobjOutputtedDataTableOrLogSheet, robjSQLRes, .HeaderInsertTexts, .TopLeftRowIndex, .TopLeftColumnIndex
'            End Select
'        End With
'    End If
'End Sub
'
'''--VBA_Code_File--<ADOLogTextSheetOut.bas>--
'Attribute VB_Name = "ADOLogTextSheetOut"
''
''   create a ADO 'SELECT' SQL result Excel sheet
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on both ADO and Excel
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Thu,  4/May/2023    Kalmclaeyd Tarclanus    Separated from ADOSheetOut.bas
''       Thu, 11/May/2023    Kalmclaeyd Tarclanus    Added ExecutedAdoSqlQueryLogPositionType enumeration
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Enumerations
''///////////////////////////////////////////////
''''
'''' SQL query execution log outputting position
''''
'Public Enum ExecutedAdoSqlQueryLogPositionType
'
'    SqlQueryLogsByAutoShapesOnSheetWithOutputtingRecordsetDataTable = &H0 ' auto-shapes log
'
'    SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable = 1    ' sheet cells log on the same sheet of the Recordset data-table
'
'    AppendingSqlQueryLogsInSheetCellsOnIndependentSheet = 2  ' sheet cells log on the other specified independent sheet
'End Enum
'
''''
'''' SQL command execution log outputting position
''''
'Public Enum ExecutedAdoSqlCommandLogPositionType
'
'    AppendingSqlCommandLogsInSheetCellsOnSpecifiedSheet = &H0
'
'    AppendingSqlCommandLogsInSheetCellsOnFixedIndependentSheet = 2
'End Enum
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''**---------------------------------------------
''** About enumerations
''**---------------------------------------------
''''
''''
''''
'Public Function GetAllExecutedAdoSqlQueryLogPositionTypesCol() As Collection
'
'    Dim objCol As Collection
'
'    Set objCol = New Collection
'
'    With objCol
'
'        .Add ExecutedAdoSqlQueryLogPositionType.SqlQueryLogsByAutoShapesOnSheetWithOutputtingRecordsetDataTable
'
'        .Add ExecutedAdoSqlQueryLogPositionType.SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable
'
'        .Add ExecutedAdoSqlQueryLogPositionType.AppendingSqlQueryLogsInSheetCellsOnIndependentSheet
'    End With
'
'    Set GetAllExecutedAdoSqlQueryLogPositionTypesCol = objCol
'End Function
'
''''
''''
''''
'Public Function GetExecutedAdoSqlQueryLogPositionTypeTextFromEnm(ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType) As String
'
'    Dim strText As String
'
'    Select Case venmExecutedAdoSqlQueryLogPositionType
'
'        Case ExecutedAdoSqlQueryLogPositionType.SqlQueryLogsByAutoShapesOnSheetWithOutputtingRecordsetDataTable
'
'            strText = "SqlQueryLogsByAutoShapesOnSheetWithOutputtingRecordsetDataTable"
'
'        Case ExecutedAdoSqlQueryLogPositionType.SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable
'
'            strText = "SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable"
'
'        Case ExecutedAdoSqlQueryLogPositionType.AppendingSqlQueryLogsInSheetCellsOnIndependentSheet
'
'            strText = "AppendingSqlQueryLogsInSheetCellsOnIndependentSheet"
'    End Select
'
'    GetExecutedAdoSqlQueryLogPositionTypeTextFromEnm = strText
'End Function
'
'
''**---------------------------------------------
''** Output logs into cells on sheet
''**---------------------------------------------
''''
''''
''''
'Public Sub OutputAdoSqlQueryResultLogsToSheet(ByVal vobjDataTableSheet As Excel.Worksheet, _
'        ByRef robjSQLRes As SQLResult, _
'        ByVal vobjDataTableSheetRangeAddresses As DataTableSheetRangeAddresses, _
'        ByVal vobjHeaderInsertTexts As Collection, _
'        ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType, _
'        ByVal vintTopLeftRowIndex As Long, _
'        Optional ByVal vintTopLeftColumnIndex As Long = 1, _
'        Optional ByVal venmCQRSClassification As SqlRdbCQRSClassification = SqlRdbCQRSClassification.RdbSqlQueryType, _
'        Optional ByVal vstrExclusiveSQLLogSheetName As String = "ExclusiveSQLLogs")
'
'
'    Select Case venmExecutedAdoSqlQueryLogPositionType
'
'        Case ExecutedAdoSqlQueryLogPositionType.SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable
'
'            InsertHeaderTextsIntoCellsOnSheet vobjDataTableSheet, vobjHeaderInsertTexts, vintTopLeftRowIndex, vintTopLeftColumnIndex, True, True
'
'            OutputAdoSqlQueryAndCommandResultLogsToCellsOnSpecifiedSheet vobjDataTableSheet, robjSQLRes, vobjDataTableSheetRangeAddresses, vobjHeaderInsertTexts, vintTopLeftRowIndex, vintTopLeftColumnIndex, venmCQRSClassification
'
'        Case ExecutedAdoSqlQueryLogPositionType.AppendingSqlQueryLogsInSheetCellsOnIndependentSheet
'
'            OutputAdoSqlQueryAndCommandResultLogsToCellsOnExclusiveSQLLogSheetWithFindingExclusiveLogSheet vobjDataTableSheet, robjSQLRes, vobjDataTableSheetRangeAddresses, vobjHeaderInsertTexts, vintTopLeftRowIndex, vintTopLeftColumnIndex, venmCQRSClassification, vstrExclusiveSQLLogSheetName
'
'        Case ExecutedAdoSqlQueryLogPositionType.SqlQueryLogsByAutoShapesOnSheetWithOutputtingRecordsetDataTable
'
'            AddAutoShapeRdbSqlQueryLog vobjDataTableSheet, robjSQLRes, vobjDataTableSheetRangeAddresses, vobjHeaderInsertTexts
'    End Select
'End Sub
'
''''
''''
''''
'Public Sub OutputAdoSqlCommandResultLogsToCellsOnSpecifiedSQLLogSheet(ByVal vobjSqlCommandLogSheet As Excel.Worksheet, _
'        ByRef robjSQLRes As SQLResult, _
'        ByVal vobjHeaderInsertTexts As Collection, _
'        ByVal vintTopLeftRowIndex As Long, _
'        Optional ByVal vintTopLeftColumnIndex As Long = 1)
'
'
'    Dim intBottomRowIndex As Long
'
'    intBottomRowIndex = GetBottomRowIndexFromWorksheetUsedRange(vobjSqlCommandLogSheet)
'
'    OutputAdoSqlQueryAndCommandResultLogsToCellsOnExclusiveSQLLogSheet vobjSqlCommandLogSheet, robjSQLRes, Nothing, vobjHeaderInsertTexts, intBottomRowIndex + 1, 1, RdbSqlCommandType
'End Sub
'
'
'
''''
'''' if the count of rows exceeds the vintLoggingLimitRowMax, then fine new SQL log sheet.
''''
'Public Sub OutputAdoSqlQueryAndCommandResultLogsToCellsOnExclusiveSQLLogSheetWithFindingExclusiveLogSheet(ByVal vobjDataTableSheet As Excel.Worksheet, _
'        ByRef robjSQLRes As SQLResult, _
'        ByVal vobjDataTableSheetRangeAddresses As DataTableSheetRangeAddresses, _
'        ByVal vobjHeaderInsertTexts As Collection, _
'        ByVal vintTopLeftRowIndex As Long, _
'        Optional ByVal vintTopLeftColumnIndex As Long = 1, _
'        Optional ByVal venmCQRSClassification As SqlRdbCQRSClassification = SqlRdbCQRSClassification.RdbSqlQueryType, _
'        Optional ByVal vstrExclusiveSQLLogSheetName As String = "ExclusiveSQLLogs", _
'        Optional ByVal vintLoggingLimitRowMax As Long = 300)
'
'
'    Dim objSQLLogSheet As Excel.Worksheet, objBook As Excel.Workbook, intBottomRowIndex As Long
'
'    Set objBook = vobjDataTableSheet.Parent
'
'    FindBottomRowIndexAfterGetSheetWithFindingNewNameWhenUsedRangeRowsCountExceedsLimit objSQLLogSheet, intBottomRowIndex, vstrExclusiveSQLLogSheetName, objBook, vintLoggingLimitRowMax
'
'    OutputAdoSqlQueryAndCommandResultLogsToCellsOnExclusiveSQLLogSheet objSQLLogSheet, robjSQLRes, vobjDataTableSheetRangeAddresses, vobjHeaderInsertTexts, intBottomRowIndex + 1, 1, venmCQRSClassification
'End Sub
'
''''
''''
''''
'Public Sub OutputAdoSqlQueryAndCommandResultLogsToCellsOnExclusiveSQLLogSheet(ByVal vobjSQLLogSheet As Excel.Worksheet, _
'        ByRef robjSQLRes As SQLResult, _
'        ByVal vobjDataTableSheetRangeAddresses As DataTableSheetRangeAddresses, _
'        ByVal vobjHeaderInsertTexts As Collection, _
'        ByVal vintTopLeftRowIndex As Long, _
'        Optional ByVal vintTopLeftColumnIndex As Long = 1, _
'        Optional ByVal venmCQRSClassification As SqlRdbCQRSClassification = SqlRdbCQRSClassification.RdbSqlQueryType)
'
'    OutputAdoSqlQueryAndCommandResultLogsToCellsOnSpecifiedSheet vobjSQLLogSheet, robjSQLRes, vobjDataTableSheetRangeAddresses, vobjHeaderInsertTexts, vintTopLeftRowIndex, vintTopLeftColumnIndex, venmCQRSClassification
'
'    InsertHeaderTextsIntoCellsOnSheet vobjSQLLogSheet, vobjHeaderInsertTexts, vintTopLeftRowIndex, vintTopLeftColumnIndex, True, True
'
'    WriteTopAndBottomBoundaryBorderLineOfSpecifiedRow vobjSQLLogSheet, vintTopLeftRowIndex
'
'    ShowBottomUsedRangeOfWorksheet vobjSQLLogSheet
'End Sub
'
'
''''
'''' vintTopLeftColumnIndex
''''
'Public Sub OutputAdoSqlQueryAndCommandResultLogsToCellsOnSpecifiedSheet(ByVal vobjSheet As Excel.Worksheet, _
'        ByRef robjSQLRes As SQLResult, _
'        ByVal vobjDataTableSheetRangeAddresses As DataTableSheetRangeAddresses, _
'        ByVal vobjHeaderInsertTexts As Collection, _
'        ByVal vintTopLeftRowIndex As Long, _
'        Optional ByVal vintTopLeftColumnIndex As Long = 1, _
'        Optional venmCQRSClassification As SqlRdbCQRSClassification = SqlRdbCQRSClassification.RdbSqlQueryType)
'
'
'    Dim intSQLLogStartingRowIndex As Long, enmSQLSheetLogTitleInterior As SQLSheetLogTitleInterior
'
'    enmSQLSheetLogTitleInterior = CellsInteriorOfSynchronizedSQLExec
'
'    ' Write the SQL body text
'    OutputUsedActualSQLToSpecifiedRangeOnExcelSheet robjSQLRes.SQL, vobjSheet.Cells(vintTopLeftRowIndex, vintTopLeftColumnIndex)
'
'    intSQLLogStartingRowIndex = vintTopLeftRowIndex + 1
'
'    If Not vobjHeaderInsertTexts Is Nothing Then
'
'        intSQLLogStartingRowIndex = intSQLLogStartingRowIndex + vobjHeaderInsertTexts.Count
'    End If
'
'    Select Case venmCQRSClassification
'
'        Case SqlRdbCQRSClassification.RdbSqlQueryType
'
'            OutputSQLQueryDetailLogToCellsOnSheet vobjSheet, robjSQLRes, vobjDataTableSheetRangeAddresses, intSQLLogStartingRowIndex, vintTopLeftColumnIndex, enmSQLSheetLogTitleInterior
'
'        Case SqlRdbCQRSClassification.RdbSqlCommandType
'
'            OutputSqlCommandDetailLogToCellsOnSheet vobjSheet, robjSQLRes, intSQLLogStartingRowIndex, vintTopLeftColumnIndex, False, enmSQLSheetLogTitleInterior
'    End Select
'
'    If (robjSQLRes.SQLOptionalLog And SqlOptionalLogFlagOfWorksheet.LogOfAdoConnectSettingOnSheet) > 0 Then
'
'        OutputADOConnectionParametersLogToSheet vobjSheet, robjSQLRes.LoadedADOConnectSetting, vobjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex + 7) & CStr(intSQLLogStartingRowIndex)), False, enmSQLSheetLogTitleInterior
'    End If
'
'    If (robjSQLRes.SQLOptionalLog And SqlOptionalLogFlagOfWorksheet.LogOfCurrentUserComputerDomainOnSheet) > 0 Then
'
'        OutputCurrentUserDomainLogToSheet vobjSheet, robjSQLRes.CurrentUserInfo, vobjSheet.Range(ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex + 10) & CStr(intSQLLogStartingRowIndex)), enmSQLSheetLogTitleInterior
'     End If
'End Sub
'
'
'
'
''''
''''
''''
'Public Sub OutputUsedActualSQLToSpecifiedRangeOnExcelSheet(ByVal vstrSQL As String, ByVal vobjUsedActualRange As Excel.Range, Optional ByVal venmLogTitleInterior As SQLSheetLogTitleInterior = SQLSheetLogTitleInterior.CellsInteriorOfSynchronizedSQLExec)
'
'    With vobjUsedActualRange
'
'        .Value = vstrSQL
'
'        With .Font
'
'            .Name = "Meiryo": .FontStyle = "Regular"
'
'            If (venmLogTitleInterior And SQLSheetLogTitleInterior.CellsInteriorOfSQLExecutionErrorOccured) > 0 Then
'
'                .Color = RGB(255, 0, 0)
'            Else
'                .TintAndShade = 0
'            End If
'
'            .ThemeFont = xlThemeFontNone
'        End With
'    End With
'End Sub
'
'
''**---------------------------------------------
''** output SQL result log to sheet
''**---------------------------------------------
''''
'''' output executed SQL body text to worksheet, <CQRS: Command method>
''''
'Public Sub OutputSQLBodyTextAndDetailAllLogToCellsOnSheet(ByVal vobjSheet As Excel.Worksheet, _
'        ByRef robjSQLRes As SQLResult, _
'        ByVal vobjADOSheetFormatter As ADOSheetFormatter)
'
'    With vobjADOSheetFormatter
'
'        '.InsertInputtedHeaderTexts vobjSheet
'
'        .ResultOutForAdoSqlExecution vobjSheet, robjSQLRes, SqlRdbCQRSClassification.RdbSqlCommandType
'    End With
'End Sub
'
'
''**---------------------------------------------
''** common logging
''**---------------------------------------------
''''
'''' Add the SQL query(SELECT sentence) result log into the Excel sheet.
''''
'Public Sub OutputSQLQueryDetailLogToCellsOnSheet(ByVal vobjSheet As Excel.Worksheet, _
'        ByVal vobjSQLResult As SQLResult, _
'        ByVal vobjDataTableSheetRangeAddresses As DataTableSheetRangeAddresses, _
'        ByVal vintLogStartTopLeftRowIndex As Long, _
'        Optional ByVal vintLogStartTopLeftColumnIndex As Long = 1, _
'        Optional venmLogTitleInterior As SQLSheetLogTitleInterior = SQLSheetLogTitleInterior.CellsInteriorOfSynchronizedSQLExec)
'
'
'    Dim objRange As Excel.Range, varValues() As Variant, varRangeAddresses() As Variant
'
'    With vobjSheet
'
'        ReDim varValues(1 To 5, 1 To 3)
'
'        varValues(1, 1) = GetTextOfStrKeyAdoSheetoutSqlExecutionElapsedTime(): varValues(1, 3) = GetTextOfStrKeyAdoSheetoutUnitSecond()
'
'        varValues(2, 1) = GetTextOfStrKeyAdoSheetoutExcelSheetProcessingElapsedTime(): varValues(2, 3) = GetTextOfStrKeyAdoSheetoutUnitSecond()
'
'
'        varValues(3, 1) = GetTextOfStrKeyAdoSheetoutTableRecordCount()
'
'        varValues(4, 1) = GetTextOfStrKeyAdoSheetoutTableFieldCount()
'
'        varValues(5, 1) = GetTextOfStrKeyAdoSheetoutSqlExecutedTime()
'
'
'        varValues(1, 2) = CSng(vobjSQLResult.SQLExeElapsedTime) / 1000#
'
'        varValues(2, 2) = CSng(vobjSQLResult.ExcelSheetOutputElapsedTime) / 1000#
'
'        varValues(3, 2) = vobjSQLResult.TableRecordCount
'
'        varValues(4, 2) = vobjSQLResult.TableFieldCount
'
'        varValues(5, 2) = Format(vobjSQLResult.SQLExeTime, "yyyy/mm/dd hh:mm:ss")
'
'
'        SetNumberFormatLocalForLocazliedDateTime .Cells(vintLogStartTopLeftRowIndex + 4, vintLogStartTopLeftColumnIndex + 1)
'
'        Set objRange = .Cells(vintLogStartTopLeftRowIndex, vintLogStartTopLeftColumnIndex)
'
'        objRange.Resize(5, 3).Value = varValues
'
'        .Range(ConvertXlColumnIndexToLetter(vintLogStartTopLeftColumnIndex + 1) & CStr(vintLogStartTopLeftRowIndex) & ":" & ConvertXlColumnIndexToLetter(vintLogStartTopLeftColumnIndex + 1) & CStr(vintLogStartTopLeftRowIndex + 1)).NumberFormatLocal = "0.000"
'
'
'
'        ' change appearence
'        DecorateSheetCellsFontsForSQLQueryAndCommandLog .Range(ConvertXlColumnIndexToLetter(vintLogStartTopLeftColumnIndex) & CStr(vintLogStartTopLeftRowIndex) & ":" & ConvertXlColumnIndexToLetter(vintLogStartTopLeftColumnIndex + 2) & CStr(vintLogStartTopLeftRowIndex + 4))
'
'        DecorateSheetCellsBorderlinesForSQLQueryAndCommandLog .Range(ConvertXlColumnIndexToLetter(vintLogStartTopLeftColumnIndex) & CStr(vintLogStartTopLeftRowIndex) & ":" & ConvertXlColumnIndexToLetter(vintLogStartTopLeftColumnIndex + 1) & CStr(vintLogStartTopLeftRowIndex + 4))
'
'        DecorateSheetCellsInteriorForSQLQueryAndCommandLog .Range(ConvertXlColumnIndexToLetter(vintLogStartTopLeftColumnIndex) & CStr(vintLogStartTopLeftRowIndex) & ":" & ConvertXlColumnIndexToLetter(vintLogStartTopLeftColumnIndex) & CStr(vintLogStartTopLeftRowIndex + 4)), venmLogTitleInterior
'
'        If Not vobjDataTableSheetRangeAddresses Is Nothing Then
'
'            ReDim varRangeAddresses(1 To 2, 1 To 2)
'
'            varRangeAddresses(1, 1) = "Records address": varRangeAddresses(2, 1) = "Field titles address"
'
'            With vobjDataTableSheetRangeAddresses
'
'                varRangeAddresses(1, 2) = .RecordAreaRangeAddress: varRangeAddresses(2, 2) = .FieldTitlesRangeAddress
'            End With
'
'            Set objRange = .Cells(vintLogStartTopLeftRowIndex + 2, vintLogStartTopLeftColumnIndex + 3)
'
'            objRange.Resize(2, 2).Value = varRangeAddresses
'
'            DecorateSheetCellsInteriorForSQLQueryAndCommandLog objRange.Resize(2, 1)
'
'            DecorateSheetCellsBorderlinesForSQLQueryAndCommandLog objRange.Resize(2, 2)
'
'            DecorateSheetCellsFontsForSQLQueryAndCommandLog objRange.Resize(2, 2)
'        End If
'    End With
'End Sub
'
'
'
''''
'''' Add the SQL command(UPDATE, DELETE, INSERT sentences) result log into the Excel sheet.
''''
'Public Sub OutputSqlCommandDetailLogToCellsOnSheet(ByVal vobjSheet As Excel.Worksheet, _
'        ByVal vobjSQLResult As SQLResult, _
'        ByVal vintLogStartTopLeftRowIndex As Long, _
'        Optional ByVal vintLogStartTopLeftColumnIndex As Long = 1, _
'        Optional ByVal vblnOmitRecordsAffected As Boolean = False, _
'        Optional ByVal venmLogTitleInterior As SQLSheetLogTitleInterior = SQLSheetLogTitleInterior.CellsInteriorOfSynchronizedSQLExec)
'
'
'    Dim objRange As Excel.Range, varValues() As Variant
'
'    With vobjSheet
'
'        If Not vblnOmitRecordsAffected Then
'
'            ' After RDB Command execution
'
'            ReDim varValues(1 To 3, 1 To 3)
'
'            varValues(1, 1) = GetTextOfStrKeyAdoSheetoutSqlExecutionElapsedTime()
'
'            varValues(1, 3) = GetTextOfStrKeyAdoSheetoutUnitSecond()
'
'            Select Case GetSQLSyntaxType(vobjSQLResult.SQL)
'
'                Case RdbSqlClassification.RdbSqlDeleteType
'
'                    varValues(2, 1) = GetTextOfStrKeyAdoSheetoutCountOfSqlDelete()
'
'                Case RdbSqlClassification.RdbSqlUpdateType
'
'                    varValues(2, 1) = GetTextOfStrKeyAdoSheetoutCountOfSqlUpdate()
'
'                Case RdbSqlClassification.RdbSqlInsertType
'
'                    varValues(2, 1) = GetTextOfStrKeyAdoSheetoutCountOfSqlInsert()
'
'                Case Else
'
'                    varValues(2, 1) = GetTextOfStrKeyAdoSheetoutRecordsAffected()
'            End Select
'
'            varValues(3, 1) = GetTextOfStrKeyAdoSheetoutSqlExecutedTime()
'
'            varValues(1, 2) = CSng(vobjSQLResult.SQLExeElapsedTime) / 1000#
'
'            varValues(2, 2) = vobjSQLResult.RecordsAffected
'
'            varValues(3, 2) = Format(vobjSQLResult.SQLExeTime, "yyyy/mm/dd hh:mm:ss")
'
'            SetNumberFormatLocalForLocazliedDateTime .Cells(vintLogStartTopLeftRowIndex + 2, 2)
'
'            Set objRange = .Cells(vintLogStartTopLeftRowIndex, vintLogStartTopLeftColumnIndex)
'
'            objRange.Resize(3, 3).Value = varValues
'
'            .Range(ConvertXlColumnIndexToLetter(vintLogStartTopLeftColumnIndex + 1) & CStr(vintLogStartTopLeftRowIndex) & ":" & ConvertXlColumnIndexToLetter(vintLogStartTopLeftColumnIndex + 1) & CStr(vintLogStartTopLeftRowIndex)).NumberFormatLocal = "0.000"
'
'
'            ' change appearence
'            DecorateSheetCellsFontsForSQLQueryAndCommandLog .Range(ConvertXlColumnIndexToLetter(vintLogStartTopLeftColumnIndex) & CStr(vintLogStartTopLeftRowIndex) & ":" & ConvertXlColumnIndexToLetter(vintLogStartTopLeftColumnIndex + 2) & CStr(vintLogStartTopLeftRowIndex + 2))
'
'            DecorateSheetCellsBorderlinesForSQLQueryAndCommandLog .Range(ConvertXlColumnIndexToLetter(vintLogStartTopLeftColumnIndex) & CStr(vintLogStartTopLeftRowIndex) & ":" & ConvertXlColumnIndexToLetter(vintLogStartTopLeftColumnIndex + 1) & CStr(vintLogStartTopLeftRowIndex + 2))
'
'            DecorateSheetCellsInteriorForSQLQueryAndCommandLog .Range(ConvertXlColumnIndexToLetter(vintLogStartTopLeftColumnIndex) & CStr(vintLogStartTopLeftRowIndex) & ":" & ConvertXlColumnIndexToLetter(vintLogStartTopLeftColumnIndex) & CStr(vintLogStartTopLeftRowIndex + 2)), venmLogTitleInterior
'
'            .Range(ConvertXlColumnIndexToLetter(vintLogStartTopLeftColumnIndex) & ":" & ConvertXlColumnIndexToLetter(vintLogStartTopLeftColumnIndex)).ColumnWidth = 22
'
'            .Range(CStr(vintLogStartTopLeftRowIndex + 1) & ":" & CStr(vintLogStartTopLeftRowIndex + 1)).RowHeight = 27
'
'            With .Range(ConvertXlColumnIndexToLetter(vintLogStartTopLeftColumnIndex) & CStr(vintLogStartTopLeftRowIndex + 1) & ":" & ConvertXlColumnIndexToLetter(vintLogStartTopLeftColumnIndex + 1) & CStr(vintLogStartTopLeftRowIndex + 1))
'
'                .Font.Size = 12
'            End With
'
'            .Range(ConvertXlColumnIndexToLetter(vintLogStartTopLeftColumnIndex + 3) & ":" & ConvertXlColumnIndexToLetter(vintLogStartTopLeftColumnIndex + 3)).ColumnWidth = 17
'        Else
'            ' After error occured
'
'            ReDim varValues(1 To 2, 1 To 3)
'
'            varValues(1, 1) = GetTextOfStrKeyAdoSheetoutSqlExecutionElapsedTime(): varValues(1, 3) = GetTextOfStrKeyAdoSheetoutUnitSecond()
'
'            varValues(2, 1) = GetTextOfStrKeyAdoSheetoutSqlExecutedTime()
'
'            varValues(1, 2) = CSng(vobjSQLResult.SQLExeElapsedTime) / 1000#
'
'            'varValues(2, 2) = FormatDateTime(vobjSQLResult.SQLExeTime, vbLongDate) & "," & FormatDateTime(vobjSQLResult.SQLExeTime, vbLongTime)
'            varValues(2, 2) = Format(vobjSQLResult.SQLExeTime, "yyyy/mm/dd hh:mm:ss")
'
'            SetNumberFormatLocalForLocazliedDateTime .Cells(vintLogStartTopLeftRowIndex + 1, vintLogStartTopLeftColumnIndex + 1)
'
'            Set objRange = .Cells(vintLogStartTopLeftRowIndex, vintLogStartTopLeftColumnIndex)
'
'            objRange.Resize(2, 3).Value = varValues
'
'
'            .Range(ConvertXlColumnIndexToLetter(vintLogStartTopLeftColumnIndex + 1) & CStr(vintLogStartTopLeftRowIndex) & ":" & ConvertXlColumnIndexToLetter(vintLogStartTopLeftColumnIndex + 1) & CStr(vintLogStartTopLeftRowIndex)).NumberFormatLocal = "0.000"
'
'            DecorateSheetCellsFontsForSQLQueryAndCommandLog .Range(ConvertXlColumnIndexToLetter(vintLogStartTopLeftColumnIndex + 1) & CStr(vintLogStartTopLeftRowIndex) & ":" & ConvertXlColumnIndexToLetter(vintLogStartTopLeftColumnIndex + 2) & CStr(vintLogStartTopLeftRowIndex + 1)), CellsInteriorOfSynchronizedSQLExec
'
'            DecorateSheetCellsFontsForSQLQueryAndCommandLog .Range(ConvertXlColumnIndexToLetter(vintLogStartTopLeftColumnIndex) & CStr(vintLogStartTopLeftRowIndex) & ":" & ConvertXlColumnIndexToLetter(vintLogStartTopLeftColumnIndex) & CStr(vintLogStartTopLeftRowIndex)), CellsInteriorOfSynchronizedSQLExec
'
'            DecorateSheetCellsFontsForSQLQueryAndCommandLog .Range(ConvertXlColumnIndexToLetter(vintLogStartTopLeftColumnIndex) & CStr(vintLogStartTopLeftRowIndex + 1) & ":" & ConvertXlColumnIndexToLetter(vintLogStartTopLeftColumnIndex) & CStr(vintLogStartTopLeftRowIndex + 1)), venmLogTitleInterior
'
'
'            DecorateSheetCellsBorderlinesForSQLQueryAndCommandLog .Range(ConvertXlColumnIndexToLetter(vintLogStartTopLeftColumnIndex) & CStr(vintLogStartTopLeftRowIndex) & ":" & ConvertXlColumnIndexToLetter(vintLogStartTopLeftColumnIndex + 1) & CStr(vintLogStartTopLeftRowIndex + 1))
'
'
'            DecorateSheetCellsInteriorForSQLQueryAndCommandLog .Range(ConvertXlColumnIndexToLetter(vintLogStartTopLeftColumnIndex) & CStr(vintLogStartTopLeftRowIndex) & ":" & ConvertXlColumnIndexToLetter(vintLogStartTopLeftColumnIndex) & CStr(vintLogStartTopLeftRowIndex)), CellsInteriorOfSynchronizedSQLExec
'
'            DecorateSheetCellsInteriorForSQLQueryAndCommandLog .Range(ConvertXlColumnIndexToLetter(vintLogStartTopLeftColumnIndex) & CStr(vintLogStartTopLeftRowIndex + 1) & ":" & ConvertXlColumnIndexToLetter(vintLogStartTopLeftColumnIndex) & CStr(vintLogStartTopLeftRowIndex + 1)), venmLogTitleInterior
'        End If
'    End With
'End Sub
'
'
''''
'''' logging ADO connection parameters: CommandTimeout, CursorLocation, ExecuteOptions, CursorType, and LockType
''''
'Public Sub OutputADOConnectionParametersLogToSheet(ByVal vobjSheet As Excel.Worksheet, _
'        ByVal vobjLoadedConnectSetting As LoadedADOConnectionSetting, _
'        ByVal vobjStartingRange As Excel.Range, _
'        Optional ByVal vblnSetTwoColumns As Boolean = False, _
'        Optional ByVal venmLogTitleInterior As SQLSheetLogTitleInterior = SQLSheetLogTitleInterior.CellsInteriorOfSynchronizedSQLExec)
'
'
'    Dim intTopLeftRowIndex As Long, intTopLeftColumnIndex As Long
'    Dim varValues() As Variant, objRange As Excel.Range
'
'    With vobjStartingRange
'
'        intTopLeftRowIndex = .Row
'
'        intTopLeftColumnIndex = .Column
'    End With
'
'    With vobjLoadedConnectSetting
'
'        If vblnSetTwoColumns Then
'
'            ReDim varValues(1 To 3, 1 To 5)
'
'            If .IsRecordsetExists Then
'
'                varValues(1, 4) = "CursorType"
'
'                varValues(2, 4) = "LockType"
'
'
'                varValues(1, 5) = GetADOEnumLiteralFromRecordsetCursorType(.CursorType)
'
'                varValues(2, 5) = GetADOEnumLiteralFromRecordsetLockType(.LockType)
'            End If
'        Else
'            ReDim varValues(1 To 5, 1 To 3)
'
'            If .IsRecordsetExists Then
'
'                varValues(4, 1) = "CursorType"
'
'                varValues(5, 1) = "LockType"
'
'
'                varValues(4, 2) = GetADOEnumLiteralFromRecordsetCursorType(.CursorType)
'
'                varValues(5, 2) = GetADOEnumLiteralFromRecordsetLockType(.LockType)
'            End If
'        End If
'
'        ' common area
'        varValues(1, 1) = "CursorLocation"
'
'        varValues(2, 1) = "ExecutionOption"
'
'        varValues(3, 1) = "CommandTimeout"
'
'        varValues(3, 3) = GetTextOfStrKeyAdoSheetoutUnitSecond()
'
'
'        varValues(1, 2) = GetADOEnumLiteralFromCursorLocation(.CursorLocation)
'
'        varValues(2, 2) = GetADOEnumAllLiteralsFromExecuteOption(.ExecuteOption)
'
'        varValues(3, 2) = .CommandTimeout
'
'        If vblnSetTwoColumns Then
'
'            vobjSheet.Cells(intTopLeftRowIndex, intTopLeftColumnIndex).Resize(3, 5).Value = varValues
'
'            With vobjSheet
'
'                Select Case venmLogTitleInterior
'
'                    Case SQLSheetLogTitleInterior.CellsInteriorOfSQLExecutionErrorOccured
'
'                        Set objRange = .Range(ConvertXlColumnIndexToLetter(intTopLeftColumnIndex) & CStr(intTopLeftRowIndex) & ":" & ConvertXlColumnIndexToLetter(intTopLeftColumnIndex) & CStr(intTopLeftRowIndex + 2))
'
'                        If vobjLoadedConnectSetting.IsRecordsetExists Then
'
'                            Set objRange = Union(objRange, .Range(ConvertXlColumnIndexToLetter(intTopLeftColumnIndex + 3) & CStr(intTopLeftRowIndex) & ":" & ConvertXlColumnIndexToLetter(intTopLeftColumnIndex + 3) & CStr(intTopLeftRowIndex + 2)))
'                        End If
'
'                        DecorateSheetCellsFontsForSQLQueryAndCommandLog objRange, CellsInteriorOfSQLExecutionErrorOccured
'
'                        Set objRange = .Range(ConvertXlColumnIndexToLetter(intTopLeftColumnIndex + 1) & CStr(intTopLeftRowIndex) & ":" & ConvertXlColumnIndexToLetter(intTopLeftColumnIndex + 2) & CStr(intTopLeftRowIndex + 2))
'
'                        If vobjLoadedConnectSetting.IsRecordsetExists Then
'
'                            Set objRange = Union(objRange, .Range(ConvertXlColumnIndexToLetter(intTopLeftColumnIndex + 4) & CStr(intTopLeftRowIndex) & ":" & ConvertXlColumnIndexToLetter(intTopLeftColumnIndex + 4) & CStr(intTopLeftRowIndex + 2)))
'                        End If
'
'                        DecorateSheetCellsFontsForSQLQueryAndCommandLog objRange, CellsInteriorOfSynchronizedSQLExec
'
'                    Case Else
'
'                        If vobjLoadedConnectSetting.IsRecordsetExists Then
'
'                            Set objRange = .Range(ConvertXlColumnIndexToLetter(intTopLeftColumnIndex) & CStr(intTopLeftRowIndex) & ":" & ConvertXlColumnIndexToLetter(intTopLeftColumnIndex + 4) & CStr(intTopLeftRowIndex + 2))
'                        Else
'                            Set objRange = .Range(ConvertXlColumnIndexToLetter(intTopLeftColumnIndex) & CStr(intTopLeftRowIndex) & ":" & ConvertXlColumnIndexToLetter(intTopLeftColumnIndex + 2) & CStr(intTopLeftRowIndex + 2))
'                        End If
'
'                        DecorateSheetCellsFontsForSQLQueryAndCommandLog objRange
'                End Select
'
'
'                Set objRange = .Range(ConvertXlColumnIndexToLetter(intTopLeftColumnIndex) & CStr(intTopLeftRowIndex) & ":" & ConvertXlColumnIndexToLetter(intTopLeftColumnIndex + 1) & CStr(intTopLeftRowIndex + 2))
'
'                If vobjLoadedConnectSetting.IsRecordsetExists Then
'
'                    Set objRange = Union(objRange, .Range(ConvertXlColumnIndexToLetter(intTopLeftColumnIndex + 3) & CStr(intTopLeftRowIndex) & ":" & ConvertXlColumnIndexToLetter(intTopLeftColumnIndex + 4) & CStr(intTopLeftRowIndex + 1)))
'                End If
'
'                DecorateSheetCellsBorderlinesForSQLQueryAndCommandLog objRange
'
'                Set objRange = .Range(ConvertXlColumnIndexToLetter(intTopLeftColumnIndex) & CStr(intTopLeftRowIndex) & ":" & ConvertXlColumnIndexToLetter(intTopLeftColumnIndex) & CStr(intTopLeftRowIndex + 2))
'
'                If vobjLoadedConnectSetting.IsRecordsetExists Then
'
'                    Set objRange = Union(objRange, .Range(ConvertXlColumnIndexToLetter(intTopLeftColumnIndex + 3) & CStr(intTopLeftRowIndex) & ":" & ConvertXlColumnIndexToLetter(intTopLeftColumnIndex + 3) & CStr(intTopLeftRowIndex + 1)))
'                End If
'
'                DecorateSheetCellsInteriorForSQLQueryAndCommandLog objRange, venmLogTitleInterior
'            End With
'
'        Else
'            vobjSheet.Cells(intTopLeftRowIndex, intTopLeftColumnIndex).Resize(5, 3).Value = varValues
'
'            With vobjSheet
'
'                Select Case venmLogTitleInterior
'
'                    Case SQLSheetLogTitleInterior.CellsInteriorOfSQLExecutionErrorOccured
'
'                        If vobjLoadedConnectSetting.IsRecordsetExists Then
'
'                            Set objRange = .Range(ConvertXlColumnIndexToLetter(intTopLeftColumnIndex) & CStr(intTopLeftRowIndex) & ":" & ConvertXlColumnIndexToLetter(intTopLeftColumnIndex) & CStr(intTopLeftRowIndex + 4))
'                        Else
'                            Set objRange = .Range(ConvertXlColumnIndexToLetter(intTopLeftColumnIndex) & CStr(intTopLeftRowIndex) & ":" & ConvertXlColumnIndexToLetter(intTopLeftColumnIndex) & CStr(intTopLeftRowIndex + 2))
'                        End If
'
'                        DecorateSheetCellsFontsForSQLQueryAndCommandLog objRange, CellsInteriorOfSQLExecutionErrorOccured
'
'                        If vobjLoadedConnectSetting.IsRecordsetExists Then
'
'                            Set objRange = .Range(ConvertXlColumnIndexToLetter(intTopLeftColumnIndex + 1) & CStr(intTopLeftRowIndex) & ":" & ConvertXlColumnIndexToLetter(intTopLeftColumnIndex + 3) & CStr(intTopLeftRowIndex + 4))
'                        Else
'                            Set objRange = .Range(ConvertXlColumnIndexToLetter(intTopLeftColumnIndex + 1) & CStr(intTopLeftRowIndex) & ":" & ConvertXlColumnIndexToLetter(intTopLeftColumnIndex + 3) & CStr(intTopLeftRowIndex + 2))
'                        End If
'
'                        DecorateSheetCellsFontsForSQLQueryAndCommandLog objRange, CellsInteriorOfSynchronizedSQLExec
'
'                    Case Else
'
'                        If vobjLoadedConnectSetting.IsRecordsetExists Then
'
'                            Set objRange = .Range(ConvertXlColumnIndexToLetter(intTopLeftColumnIndex) & CStr(intTopLeftRowIndex) & ":" & ConvertXlColumnIndexToLetter(intTopLeftColumnIndex + 2) & CStr(intTopLeftRowIndex + 4))
'                        Else
'                            Set objRange = .Range(ConvertXlColumnIndexToLetter(intTopLeftColumnIndex) & CStr(intTopLeftRowIndex) & ":" & ConvertXlColumnIndexToLetter(intTopLeftColumnIndex + 2) & CStr(intTopLeftRowIndex + 2))
'                        End If
'
'                        DecorateSheetCellsFontsForSQLQueryAndCommandLog objRange, CellsInteriorOfSynchronizedSQLExec
'                End Select
'
'                If vobjLoadedConnectSetting.IsRecordsetExists Then
'
'                    Set objRange = .Range(ConvertXlColumnIndexToLetter(intTopLeftColumnIndex) & CStr(intTopLeftRowIndex) & ":" & ConvertXlColumnIndexToLetter(intTopLeftColumnIndex + 1) & CStr(intTopLeftRowIndex + 4))
'                Else
'                    Set objRange = .Range(ConvertXlColumnIndexToLetter(intTopLeftColumnIndex) & CStr(intTopLeftRowIndex) & ":" & ConvertXlColumnIndexToLetter(intTopLeftColumnIndex + 1) & CStr(intTopLeftRowIndex + 2))
'                End If
'
'                DecorateSheetCellsBorderlinesForSQLQueryAndCommandLog objRange
'
'                If vobjLoadedConnectSetting.IsRecordsetExists Then
'
'                    Set objRange = .Range(ConvertXlColumnIndexToLetter(intTopLeftColumnIndex) & CStr(intTopLeftRowIndex) & ":" & ConvertXlColumnIndexToLetter(intTopLeftColumnIndex) & CStr(intTopLeftRowIndex + 4))
'                Else
'                    Set objRange = .Range(ConvertXlColumnIndexToLetter(intTopLeftColumnIndex) & CStr(intTopLeftRowIndex) & ":" & ConvertXlColumnIndexToLetter(intTopLeftColumnIndex) & CStr(intTopLeftRowIndex + 2))
'                End If
'
'                DecorateSheetCellsInteriorForSQLQueryAndCommandLog objRange, venmLogTitleInterior
'            End With
'        End If
'    End With
'End Sub
'
''''
'''' logging client(this PC) current user domain informations
''''
'Public Sub OutputCurrentUserDomainLogToSheet(ByVal vobjSheet As Excel.Worksheet, _
'        ByVal vobjCurrentUserDomain As CurrentUserDomain, _
'        ByVal vobjStartingRange As Excel.Range, _
'        Optional ByVal venmLogTitleInterior As SQLSheetLogTitleInterior = SQLSheetLogTitleInterior.CellsInteriorOfSynchronizedSQLExec)
'
'    Dim intTopLeftRowIndex As Long, intTopLeftColumnIndex As Long
'    Dim varValues() As Variant, objRange As Excel.Range, strStartingColumn As String
'
'    With vobjStartingRange
'
'        intTopLeftRowIndex = .Row
'
'        intTopLeftColumnIndex = .Column
'    End With
'
'    ReDim varValues(1 To 4, 1 To 2)
'
'    With vobjCurrentUserDomain
'
'        varValues(1, 1) = GetTextOfStrKeyAdoSheetoutClientUserName()
'
'        varValues(2, 1) = GetTextOfStrKeyAdoSheetoutClientHostName()
'
'        varValues(3, 1) = GetTextOfStrKeyAdoSheetoutClientIpv4Address()
'
'        varValues(4, 1) = GetTextOfStrKeyAdoSheetoutClientMacAddress()
'
'
'        varValues(1, 2) = .CurrentUserName
'
'        varValues(2, 2) = .CurrentNodePCName
'
'        varValues(3, 2) = .IPv4Address
'
'        varValues(4, 2) = .MACAddress
'    End With
'
'    vobjSheet.Cells(intTopLeftRowIndex, intTopLeftColumnIndex).Resize(4, 2).Value = varValues
'
'    strStartingColumn = ConvertXlColumnIndexToLetter(vobjStartingRange.Column) ' = 21#
'
'    With vobjSheet
'
'        .Columns(strStartingColumn & ":" & strStartingColumn).EntireColumn.ColumnWidth = 21
'
'        Set objRange = .Range(ConvertXlColumnIndexToLetter(intTopLeftColumnIndex) & CStr(intTopLeftRowIndex) & ":" & ConvertXlColumnIndexToLetter(intTopLeftColumnIndex + 1) & CStr(intTopLeftRowIndex + 3))
'
'        DecorateSheetCellsFontsForSQLQueryAndCommandLog objRange, CellsInteriorOfSynchronizedSQLExec
'
'        DecorateSheetCellsBorderlinesForSQLQueryAndCommandLog objRange
'
'        Set objRange = .Range(ConvertXlColumnIndexToLetter(intTopLeftColumnIndex) & CStr(intTopLeftRowIndex) & ":" & ConvertXlColumnIndexToLetter(intTopLeftColumnIndex) & CStr(intTopLeftRowIndex + 3))
'
'        DecorateSheetCellsInteriorForSQLQueryAndCommandLog objRange, venmLogTitleInterior
'    End With
'End Sub
'
'
''''
''''
''''
'Public Sub DecorateSheetCellsFontsForSQLQueryAndCommandLog(ByVal vobjRange As Excel.Range, _
'        Optional venmLogTitleInterior As SQLSheetLogTitleInterior = SQLSheetLogTitleInterior.CellsInteriorOfSynchronizedSQLExec)
'
'    With vobjRange
'
'        With .Font
'
'            .Name = "Meiryo": .FontStyle = "Regular"
'
'            .Size = 9
'
'            Select Case venmLogTitleInterior
'
'                Case SQLSheetLogTitleInterior.CellsInteriorOfSynchronizedSQLExec, SQLSheetLogTitleInterior.CellsInteriorOfAsynchronousSQLExec
'
'                    .ThemeColor = xlThemeColorLight1
'
'                    .TintAndShade = 0
'
'                Case SQLSheetLogTitleInterior.CellsInteriorOfSQLExecutionErrorOccured
'
'                    .Color = RGB(255, 255, 255)
'            End Select
'
'            .ThemeFont = xlThemeFontNone
'        End With
'    End With
'End Sub
'
'
''''
'''' about borderlines
''''
'Public Sub DecorateSheetCellsBorderlinesForSQLQueryAndCommandLog(ByVal vobjRange As Excel.Range)
'
'    Dim objBorders As Collection
'    Dim varBorder As Variant, enmBorder As Excel.XlBordersIndex
'
'    Set objBorders = GetXlBorderIndexColFromOptions()
'
'    With vobjRange
'
'        .Borders(xlDiagonalDown).LineStyle = xlNone
'
'        .Borders(xlDiagonalUp).LineStyle = xlNone
'
'        For Each varBorder In objBorders
'
'            enmBorder = varBorder
'
'            If .Borders(enmBorder).LineStyle = xlNone Then
'
'                With .Borders(enmBorder)
'
'                    .LineStyle = xlContinuous
'
'                    .ThemeColor = 1
'
'                    .TintAndShade = -0.249946592608417
'
'                    .Weight = xlThin
'                End With
'            End If
'        Next
'    End With
'End Sub
'
''''
'''' about interiors
''''
'Public Sub DecorateSheetCellsInteriorForSQLQueryAndCommandLog(ByVal vobjRange As Excel.Range, _
'        Optional ByVal venmLogTitleInterior As SQLSheetLogTitleInterior = SQLSheetLogTitleInterior.CellsInteriorOfSynchronizedSQLExec)
'
'
'    Dim intFirstColor As Long, intSecondColor As Long
'
'    Select Case venmLogTitleInterior
'
'        Case SQLSheetLogTitleInterior.CellsInteriorOfSynchronizedSQLExec
'
'            intFirstColor = RGB(255, 255, 255)
'
'            intSecondColor = RGB(235, 241, 222)
'
'        Case SQLSheetLogTitleInterior.CellsInteriorOfAsynchronousSQLExec
'
'            intFirstColor = RGB(255, 255, 255)
'
'            intSecondColor = RGB(241, 223, 227)
'
'
'        Case SQLSheetLogTitleInterior.CellsInteriorOfSQLExecutionErrorOccured
'
'            intFirstColor = RGB(249, 169, 177)
'
'            intSecondColor = RGB(255, 0, 0)
'    End Select
'
'    With vobjRange.Interior
'
'        .Pattern = xlPatternLinearGradient
'
'        With .Gradient
'
'            .Degree = 90
'
'            With .ColorStops
'
'                .Clear
'
'                With .Add(0): .Color = intFirstColor: End With
'
'                With .Add(0.5): .Color = intSecondColor: End With
'
'                With .Add(1): .Color = intFirstColor: End With
'            End With
'        End With
'    End With
'End Sub
'
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
''''
''''
''''
'Private Sub SetNumberFormatLocalForLocazliedDateTime(ByVal vobjRange As Excel.Range)
'
'    With vobjRange
'
'        Select Case GetCurrentUICaptionLanguageType()
'
'            Case CaptionLanguageType.UIJapaneseCaptions
'
'                '.NumberFormatLocal = "yyyy""年""m""月""d""日""""(""aaa"")"" hh:mm:ss"
'                .NumberFormatLocal = "m""月""d""日""""(""aaa"")"" hh:mm:ss"
'            Case Else
'                ' English default
'
'                '.NumberFormatLocal = "ddd"","" d/mmm/yyyy h:mm:ss AM/PM"
'                .NumberFormatLocal = "ddd"","" m/d h:mm:ss AM/PM"
'        End Select
'    End With
'End Sub
'
'
'''--VBA_Code_File--<ADOFailedLogSheetOut.bas>--
'Attribute VB_Name = "ADOFailedLogSheetOut"
''
''   Generate ADO error logs on cells of the specified Excel sheet,
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on both the ADO and Excel.Application
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Fri, 12/May/2023    Kalmclaeyd Tarclanus    Improved
''
'
'Option Explicit
'
'
''///////////////////////////////////////////////
''/// Declarations
''///////////////////////////////////////////////
'
'Private Const mstrModuleName As String = "ADOFailedLogSheetOut"
'
''**---------------------------------------------
''** Key-Value cache preparation for ADOFailedLogSheetOut
''**---------------------------------------------
'Private mobjStrKeyValueADOFailedLogSheetOutDic As Scripting.Dictionary
'
'Private mblnIsStrKeyValueADOFailedLogSheetOutDicInitialized As Boolean   ' Uninitialized boolean value is false in VBA specification.
'
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''**---------------------------------------------
''** clear default log-Excel book
''**---------------------------------------------
'Public Sub DeleteErrorLogForDefaultErrorLogBookPath()
'
'    With New ErrorLogSheetLocator
'
'        .DeleteErrorLogsForDefaultPathBook
'    End With
'End Sub
'
''**---------------------------------------------
''** call-back interface for outputting ADO failed executed SQL detail error-logs
''**---------------------------------------------
''''
''''
''''
'Public Sub GenerateADOSQLExecutionErrorLogSheet(ByRef robjErrorADOSQL As ErrorADOSQL)
'
'    Dim objLogSheet As Excel.Worksheet, objErrorLogSheetLocator As ErrorLogSheetLocator
'
'    Set objErrorLogSheetLocator = New ErrorLogSheetLocator
'
'    Set objLogSheet = objErrorLogSheetLocator.GetToLogSheetByDefaultSetting()
'
'    OutputAdoFailedExecutedSqlErrorObjectToSheetWithAdjustingLogginPosition objLogSheet, objErrorLogSheetLocator.DefaultErrorLogSheetName, robjErrorADOSQL
'End Sub
'
'
''**---------------------------------------------
''** Output error log to Excel.Worksheet
''**---------------------------------------------
''''
'''' Output ADO-SQL error summary to sheet
''''
'Public Sub OutputAdoFailedExecutedSqlErrorObjectToSheetWithAdjustingLogginPosition(ByVal vobjSheet As Excel.Worksheet, _
'        ByVal vstrBaseSheetName As String, _
'        ByRef robjErrorADOSQL As ErrorADOSQL)
'
'    Dim objBook As Excel.Workbook, intBottomRowIndex As Long, objFoundSqlExecutionErrorLogSheet As Excel.Worksheet
'
'
'    Set objBook = vobjSheet.Parent
'
'    ' The data-table sheet should be placed after the SQL log sheet
'
'    FindBottomRowIndexAfterGetSheetWithFindingNewNameWhenUsedRangeRowsCountExceedsLimit objFoundSqlExecutionErrorLogSheet, intBottomRowIndex, vstrBaseSheetName, objBook, 500
'
'    OutputAdoFailedExecutedSqlErrorObjectToSheet objFoundSqlExecutionErrorLogSheet, robjErrorADOSQL, intBottomRowIndex + 1, 1
'
'    WriteTopAndBottomBoundaryBorderLineOfSpecifiedRow vobjSheet, intBottomRowIndex + 1
'
'    ShowBottomUsedRangeOfWorksheet vobjSheet
'End Sub
'
''''
'''' Output ADO-SQL error summary to sheet
''''
'Public Sub OutputAdoFailedExecutedSqlErrorObjectToSheet(ByVal vobjSheet As Excel.Worksheet, _
'        ByVal vobjErrorADOSQL As ErrorADOSQL, _
'        Optional ByVal vintTopLeftRowIndex As Long, _
'        Optional ByVal vintTopLeftColumnIndex As Long)
'
'
'    ' About SQL
'    OutputUsedActualSQLToSpecifiedRangeOnExcelSheet vobjErrorADOSQL.ADOSQLResult.SQL, vobjSheet.Cells(vintTopLeftRowIndex, vintTopLeftColumnIndex), CellsInteriorOfSQLExecutionErrorOccured
'
'    ' Logging
'    With vobjSheet
'
'        OutputSqlCommandDetailLogToCellsOnSheet vobjSheet, vobjErrorADOSQL.ADOSQLResult, vintTopLeftRowIndex + 1, vintTopLeftColumnIndex, True, CellsInteriorOfSQLExecutionErrorOccured
'
'        OutputADOConnectionParametersLogToSheet vobjSheet, vobjErrorADOSQL.LoadedConnectionSetting, .Cells(vintTopLeftRowIndex + 3, vintTopLeftColumnIndex), False
'
'        OutputCurrentUserDomainLogToSheet vobjSheet, vobjErrorADOSQL.ADOSQLResult.CurrentUserInfo, .Cells(vintTopLeftRowIndex + 3, vintTopLeftColumnIndex + 3), SQLSheetLogTitleInterior.CellsInteriorOfSynchronizedSQLExec
'
'        OutputAdoSqlExecutedDetailErrorContentsToCellsOnSheet vobjSheet, vobjErrorADOSQL, vobjSheet.Cells(vintTopLeftRowIndex + 8, vintTopLeftColumnIndex), CellsInteriorOfSQLExecutionErrorOccured
'
'
'        msubAdjustColumnWidthForAdoSqlFailedExecutedSqlSheet vobjSheet, vintTopLeftColumnIndex
'    End With
'End Sub
'
''''
''''
''''
'Private Sub msubAdjustColumnWidthForAdoSqlFailedExecutedSqlSheet(ByRef robjSheet As Excel.Worksheet, Optional ByVal vintTopLeftColumnIndex As Long = 1)
'
'    Dim strColumnLetter As String, dblColumnWidth As Double
'
'    With robjSheet
'
'        strColumnLetter = ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex): dblColumnWidth = 20.5
'
'        With .Range(strColumnLetter & ":" & strColumnLetter).EntireColumn
'
'            If .ColumnWidth <> dblColumnWidth Then
'
'                .ColumnWidth = dblColumnWidth
'            End If
'        End With
'
'        strColumnLetter = ConvertXlColumnIndexToLetter(vintTopLeftColumnIndex + 1): dblColumnWidth = 25
'
'        With .Range(strColumnLetter & ":" & strColumnLetter).EntireColumn
'
'            If .ColumnWidth <> dblColumnWidth Then
'
'                .ColumnWidth = dblColumnWidth
'            End If
'        End With
'    End With
'End Sub
'
'
''''
''''
''''
'Public Sub OutputAdoSqlExecutedDetailErrorContentsToCellsOnSheet(ByVal vobjSheet As Excel.Worksheet, _
'        ByVal vobjErrorADOSQL As ErrorADOSQL, _
'        ByVal vobjStartingRange As Excel.Range, _
'        Optional ByVal venmLogTitleInterior As SQLSheetLogTitleInterior = SQLSheetLogTitleInterior.CellsInteriorOfSynchronizedSQLExec)
'
'
'    Dim intTopRowIndex As Long, intLeftColumnIndex As Long, varValues() As Variant, objRange As Excel.Range
'
'    With vobjStartingRange
'
'        intTopRowIndex = .Row
'
'        intLeftColumnIndex = .Column
'    End With
'
'    ReDim varValues(1 To 3, 1 To 2)
'
'    varValues(1, 1) = GetTextOfStrKeyAdoFailedLogSheetoutErrorDescription()
'
'    varValues(2, 1) = GetTextOfStrKeyAdoFailedLogSheetoutErrorSource()
'
'    varValues(3, 1) = GetTextOfStrKeyAdoFailedLogSheetoutErrorNumber()
'
'    With vobjErrorADOSQL
'
'        varValues(1, 2) = .ErrorDescription
'
'        varValues(2, 2) = .ErrorSource
'
'        varValues(3, 2) = .ErrorNumber
'    End With
'
'    vobjSheet.Cells(intTopRowIndex, intLeftColumnIndex).Resize(3, 2).Value = varValues
'
'    With vobjSheet
'
'        Select Case venmLogTitleInterior
'
'            Case SQLSheetLogTitleInterior.CellsInteriorOfSQLExecutionErrorOccured
'
'                Set objRange = .Range(ConvertXlColumnIndexToLetter(intLeftColumnIndex) & CStr(intTopRowIndex) & ":" & ConvertXlColumnIndexToLetter(intLeftColumnIndex) & CStr(intTopRowIndex + 2))
'
'                DecorateSheetCellsFontsForSQLQueryAndCommandLog objRange, CellsInteriorOfSQLExecutionErrorOccured
'
'                Set objRange = .Range(ConvertXlColumnIndexToLetter(intLeftColumnIndex + 1) & CStr(intTopRowIndex) & ":" & ConvertXlColumnIndexToLetter(intLeftColumnIndex + 1) & CStr(intTopRowIndex + 2))
'
'                DecorateSheetCellsFontsForSQLQueryAndCommandLog objRange, CellsInteriorOfSynchronizedSQLExec
'
'            Case Else
'
'                Set objRange = .Range(ConvertXlColumnIndexToLetter(intLeftColumnIndex) & CStr(intTopRowIndex) & ":" & ConvertXlColumnIndexToLetter(intLeftColumnIndex + 1) & CStr(intTopRowIndex + 2))
'
'                DecorateSheetCellsFontsForSQLQueryAndCommandLog objRange, venmLogTitleInterior
'        End Select
'
'        DecorateSheetCellsBorderlinesForSQLQueryAndCommandLog objRange
'
'        Set objRange = .Range(ConvertXlColumnIndexToLetter(intLeftColumnIndex) & CStr(intTopRowIndex) & ":" & ConvertXlColumnIndexToLetter(intLeftColumnIndex) & CStr(intTopRowIndex + 2))
'
'        DecorateSheetCellsInteriorForSQLQueryAndCommandLog objRange, venmLogTitleInterior
'    End With
'End Sub
'
'
''**---------------------------------------------
''** Key-Value cache preparation for ADOFailedLogSheetOut
''**---------------------------------------------
''''
'''' get string Value from STR_KEY_ADO_FAILED_LOG_SHEETOUT_ERROR_DESCRIPTION
''''
'''' automatically-added for ADOFailedLogSheetOut string-key-value management for standard module and class module
'Private Function GetTextOfStrKeyAdoFailedLogSheetoutErrorDescription() As String
'
'    If Not mblnIsStrKeyValueADOFailedLogSheetOutDicInitialized Then
'
'        msubInitializeTextForADOFailedLogSheetOut
'    End If
'
'    GetTextOfStrKeyAdoFailedLogSheetoutErrorDescription = mobjStrKeyValueADOFailedLogSheetOutDic.Item("STR_KEY_ADO_FAILED_LOG_SHEETOUT_ERROR_DESCRIPTION")
'End Function
'
''''
'''' get string Value from STR_KEY_ADO_FAILED_LOG_SHEETOUT_ERROR_SOURCE
''''
'''' automatically-added for ADOFailedLogSheetOut string-key-value management for standard module and class module
'Private Function GetTextOfStrKeyAdoFailedLogSheetoutErrorSource() As String
'
'    If Not mblnIsStrKeyValueADOFailedLogSheetOutDicInitialized Then
'
'        msubInitializeTextForADOFailedLogSheetOut
'    End If
'
'    GetTextOfStrKeyAdoFailedLogSheetoutErrorSource = mobjStrKeyValueADOFailedLogSheetOutDic.Item("STR_KEY_ADO_FAILED_LOG_SHEETOUT_ERROR_SOURCE")
'End Function
'
''''
'''' get string Value from STR_KEY_ADO_FAILED_LOG_SHEETOUT_ERROR_NUMBER
''''
'''' automatically-added for ADOFailedLogSheetOut string-key-value management for standard module and class module
'Private Function GetTextOfStrKeyAdoFailedLogSheetoutErrorNumber() As String
'
'    If Not mblnIsStrKeyValueADOFailedLogSheetOutDicInitialized Then
'
'        msubInitializeTextForADOFailedLogSheetOut
'    End If
'
'    GetTextOfStrKeyAdoFailedLogSheetoutErrorNumber = mobjStrKeyValueADOFailedLogSheetOutDic.Item("STR_KEY_ADO_FAILED_LOG_SHEETOUT_ERROR_NUMBER")
'End Function
'
''''
'''' Initialize Key-Values which values
''''
'''' automatically-added for ADOFailedLogSheetOut string-key-value management for standard module and class module
'Private Sub msubInitializeTextForADOFailedLogSheetOut()
'
'    Dim objKeyToTextDic As Scripting.Dictionary
'
'    If Not mblnIsStrKeyValueADOFailedLogSheetOutDicInitialized Then
'
'        Set objKeyToTextDic = GetKeyToTextForLocalizingEachModule(mstrModuleName)
'
'        If objKeyToTextDic Is Nothing Then
'
'            ' If the Key-Value table file is lost, then the default setting is used
'
'            Set objKeyToTextDic = New Scripting.Dictionary
'
'            Select Case GetCurrentUICaptionLanguageType()
'
'                Case CaptionLanguageType.UIJapaneseCaptions
'
'                    AddStringKeyValueForADOFailedLogSheetOutByJapaneseValues objKeyToTextDic
'                Case Else
'
'                    AddStringKeyValueForADOFailedLogSheetOutByEnglishValues objKeyToTextDic
'            End Select
'        End If
'
'        mblnIsStrKeyValueADOFailedLogSheetOutDicInitialized = True
'    End If
'
'    Set mobjStrKeyValueADOFailedLogSheetOutDic = objKeyToTextDic
'End Sub
'
''''
'''' Add Key-Values which values are in English for ADOFailedLogSheetOut key-values cache
''''
'''' automatically-added for ADOFailedLogSheetOut string-key-value management for standard module and class module
'Private Sub AddStringKeyValueForADOFailedLogSheetOutByEnglishValues(ByRef robjDic As Scripting.Dictionary)
'
'    With robjDic
'
'        .Add "STR_KEY_ADO_FAILED_LOG_SHEETOUT_ERROR_DESCRIPTION", "Error Description"
'        .Add "STR_KEY_ADO_FAILED_LOG_SHEETOUT_ERROR_SOURCE", "Error Source"
'        .Add "STR_KEY_ADO_FAILED_LOG_SHEETOUT_ERROR_NUMBER", "Error Number"
'    End With
'End Sub
'
''''
'''' Add Key-Values which values are in Japanese for ADOFailedLogSheetOut key-values cache
''''
'''' automatically-added for ADOFailedLogSheetOut string-key-value management for standard module and class module
'Private Sub AddStringKeyValueForADOFailedLogSheetOutByJapaneseValues(ByRef robjDic As Scripting.Dictionary)
'
'    With robjDic
'
'        .Add "STR_KEY_ADO_FAILED_LOG_SHEETOUT_ERROR_DESCRIPTION", "エラーの内容"
'        .Add "STR_KEY_ADO_FAILED_LOG_SHEETOUT_ERROR_SOURCE", "エラー元"
'        .Add "STR_KEY_ADO_FAILED_LOG_SHEETOUT_ERROR_NUMBER", "エラー番号"
'    End With
'End Sub
'
''''
'''' Remove Keys for ADOFailedLogSheetOut key-values cache
''''
'''' automatically-added for ADOFailedLogSheetOut string-key-value management for standard module and class module
'Private Sub RemoveKeysOfKeyValueADOFailedLogSheetOut(ByRef robjDic As Scripting.Dictionary)
'
'    With robjDic
'
'        If .Exists(STR_KEY_ADO_FAILED_LOG_SHEETOUT_ERROR_DESCRIPTION) Then   ' Check only the first key, otherwise omit items
'
'            .Remove "STR_KEY_ADO_FAILED_LOG_SHEETOUT_ERROR_DESCRIPTION"
'            .Remove "STR_KEY_ADO_FAILED_LOG_SHEETOUT_ERROR_SOURCE"
'            .Remove "STR_KEY_ADO_FAILED_LOG_SHEETOUT_ERROR_NUMBER"
'        End If
'    End With
'End Sub
'
'''--VBA_Code_File--<UTfADOSheetExpander.bas>--
'Attribute VB_Name = "UTfADOSheetExpander"
''
''   Testing for ADOSheetExpander using both XlAdoSheetExpander and CsvAdoSheetExpander
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on Excel
''       Dependent on UTfDecorationSetterToXlSheet.bas, UTfCreateDataTable.bas
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Thu, 11/May/2023    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Control VBE windows
''///////////////////////////////////////////////
''**---------------------------------------------
''** Open relative VBE code-panes
''**---------------------------------------------
'Private Sub OpenVBECodePaneRelativeToUTfADOSheetExpander()
'
'    ' Focusing on this procdure, please press [F5] key to start this. Therefore this scope is private.
'
'    On Error Resume Next ' If the 'ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma' isn't included in the VBA project, the following causes no syntax error.
'
'    Application.Run "ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma", mfstrGetModuleNamesToOpenInVBE()
'End Sub
'
''''
''''
''''
'Private Function mfstrGetModuleNamesToOpenInVBE() As String
'
'    Dim strNames As String
'
'    strNames = "DataTableSheetRangeAddresses,ADOConnectingInClasses,AdoRSetSheetExpander,ADOSheetFormatter,ColumnsNumberFormatLocalParam,"
'
'    strNames = strNames & "AdoRSetSheetExpanding,AppendAdoRsetOnSheet,ADOSheetOut,ADOLogTextSheetOut,"
'
'    strNames = strNames & "XlAdoSheetExpander,CsvAdoSheetExpander,ADOAsyncConnector,SqlUTfXlSheetExpander,SqlUTfCsvAdoSheetExpander,"
'
'    strNames = strNames & "ADOConnector,IADOConnector,IADOConnectStrGenerator,IADOConnectStrGenerator,XlAdoConnector,CsvAdoConnector,"
'
'    strNames = strNames & "IOutputBookPath,IExpandAdoRecordsetOnSheet"
'
'
'    mfstrGetModuleNamesToOpenInVBE = strNames
'End Function
'
'
''///////////////////////////////////////////////
''/// Sanity tests
''///////////////////////////////////////////////
''''
''''
''''
'Private Sub msubSanityTestToInterfaceObject()
'
'    Dim itfADOConnector As IADOConnector
'    Dim objAdoRecordsetSheetExpander As AdoRSetSheetExpander
'
'    Debug.Print objAdoRecordsetSheetExpander Is Nothing
'
'    Debug.Print itfADOConnector Is Nothing
'End Sub
'
''**---------------------------------------------
''** About comprehensive parameters
''**---------------------------------------------
''''
'''' About XlAdoSheetExpander
''''
'Private Sub msubComprehensiveTestToXlAdoSheetExpander()
'
'
'    Dim varRowCol As Variant, objRowCol As Collection, intTopLeftRowIndex As Long, intTopLeftColumnIndex As Long
'    Dim strOutputBookPath As String, objBook As Excel.Workbook, objSheet As Excel.Worksheet, strSheetName As String, i As Long
'    Dim strSQL As String
'
'    Dim varAllowToShowFieldTitle As Variant, varAllowToRecordSQLLog As Variant, blnAfterAllowToRecordSQLLog As Boolean
'    Dim objAdditionalLogDic As Scripting.Dictionary
'
'    Dim varExecutedAdoSqlQueryLogPositionType As Variant, enmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType
'
'
'    strOutputBookPath = mfstrGetSheetDecorationTestBookDir() & "\XlAdoSheetExpanderOutputComprensiveTest.xlsx"
'
'    Set objBook = GetWorkbookAndPrepareForUnitTest(strOutputBookPath)
'
'    i = 1
'
'    With New XlAdoSheetExpander
'
'        .SetInputExcelBookPath GetXlConnectingPrearrangedLogBookPath()
'
'        strSQL = "SELECT 'ABC' AS A_Column, 'DEF' AS B_Column"
'
'        For Each varAllowToRecordSQLLog In GetBooleanColFromLineDelimitedChar("True,False")
'
'            For Each varExecutedAdoSqlQueryLogPositionType In GetAllExecutedAdoSqlQueryLogPositionTypesCol()
'
'                enmExecutedAdoSqlQueryLogPositionType = varExecutedAdoSqlQueryLogPositionType
'
'                For Each varAllowToShowFieldTitle In GetBooleanColFromLineDelimitedChar("True,False")
'
'                    For Each varRowCol In GetDataTableSheetCreateTestConditionOnlyTopLeftDTCol(5, 7)
'
'                        Set objRowCol = varRowCol
'
'                        GetDataTableTopLeftWhenCreateTableOnSheet intTopLeftRowIndex, intTopLeftColumnIndex, objRowCol
'
'
'                        .TopLeftRowIndex = intTopLeftRowIndex
'
'                        .TopLeftColumnIndex = intTopLeftColumnIndex
'
'                        .AllowToShowFieldTitle = varAllowToShowFieldTitle
'
'                        .AllowToRecordSQLLog = varAllowToRecordSQLLog
'
'                        .RecordBordersType = RecordCellsGrayAll
'
'
'                        If .AllowToShowFieldTitle Then
'
'                            Set .FieldTitleToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("A_Column,12,B_Column,14")
'                        Else
'                            Set .FieldTitleOrderToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("Col1,12,Col2,14")
'                        End If
'
'
'
'                        Set objSheet = GetNewWorksheetAfterAllExistedSheetsFromBook(objBook)
'
'                        strSheetName = "XlSQLTest_" & Format(i, "00")
'
'                        .OutputToSheetFrom strSQL, objSheet, strSheetName, enmExecutedAdoSqlQueryLogPositionType
'
'                        blnAfterAllowToRecordSQLLog = .AllowToRecordSQLLog
'
'
'                        Set objAdditionalLogDic = New Scripting.Dictionary
'
'                        With objAdditionalLogDic
'
'                            .Add "Input AllowToRecordSQLLog", varAllowToRecordSQLLog
'
'                            .Add "After-SQL-executed AllowToRecordSQLLog", blnAfterAllowToRecordSQLLog
'
'                            .Add "ExecutedAdoSqlQueryLogPositionType", GetExecutedAdoSqlQueryLogPositionTypeTextFromEnm(enmExecutedAdoSqlQueryLogPositionType)
'
'                            .Add "AllowToShowFieldTitle", varAllowToShowFieldTitle
'
'                            .Add "TopLeftRowIndex", intTopLeftRowIndex
'
'                            .Add "TopLeftColumnIndex", intTopLeftColumnIndex
'                        End With
'
'                        OutputDicToAutoShapeLogTextOnSheet objSheet, objAdditionalLogDic, "Excel sheet connecting ADOSheetFormatter some parameters test"
'
'                        i = i + 1
'                    Next
'                Next
'            Next
'        Next
'    End With
'
'    DeleteDummySheetWhenItExists objBook
'End Sub
'
''''
'''' About CsvAdoSheetExpander
''''
'Private Sub msubComprehensiveTestToCsvAdoSheetExpander()
'
'    Dim varRowCol As Variant, objRowCol As Collection, intTopLeftRowIndex As Long, intTopLeftColumnIndex As Long
'    Dim strOutputBookPath As String, objBook As Excel.Workbook, objSheet As Excel.Worksheet, strSheetName As String, i As Long
'    Dim strSQL As String
'
'    Dim varAllowToShowFieldTitle As Variant
'    Dim varAllowToRecordSQLLog As Variant, blnAfterAllowToRecordSQLLog As Boolean
'    Dim objAdditionalLogDic As Scripting.Dictionary
'
'    Dim strInputCSVPath As String
'
'    Dim varExecutedAdoSqlQueryLogPositionType As Variant, enmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType
'
'
'    strOutputBookPath = mfstrGetSheetDecorationTestBookDir() & "\CsvAdoSheetExpanderOutputComprensiveTest.xlsx"
'
'    Set objBook = GetWorkbookAndPrepareForUnitTest(strOutputBookPath)
'
'    i = 1
'
'    strInputCSVPath = GetCSVPathAfterCheckFileExistenceOrCreateWhenItDoesntExist(BasicSampleDT, 20)
'
'    With New CsvAdoSheetExpander
'
'
'        .SetInputCsvFileConnection strInputCSVPath
'
'        strSQL = "SELECT * FROM " & GetCSVTableNameLeteral(strInputCSVPath)
'
'        For Each varAllowToRecordSQLLog In GetBooleanColFromLineDelimitedChar("True,False")
'
'            For Each varExecutedAdoSqlQueryLogPositionType In GetAllExecutedAdoSqlQueryLogPositionTypesCol()
'
'                enmExecutedAdoSqlQueryLogPositionType = varExecutedAdoSqlQueryLogPositionType
'
'                For Each varAllowToShowFieldTitle In GetBooleanColFromLineDelimitedChar("True,False")
'
'                    For Each varRowCol In GetDataTableSheetCreateTestConditionOnlyTopLeftDTCol(5, 7)
'
'                        Set objRowCol = varRowCol
'
'                        GetDataTableTopLeftWhenCreateTableOnSheet intTopLeftRowIndex, intTopLeftColumnIndex, objRowCol
'
'
'                        .TopLeftRowIndex = intTopLeftRowIndex
'
'                        .TopLeftColumnIndex = intTopLeftColumnIndex
'
'                        .AllowToShowFieldTitle = varAllowToShowFieldTitle
'
'                        .AllowToRecordSQLLog = varAllowToRecordSQLLog
'
'                        .RecordBordersType = RecordCellsGrayAll
'
'                        If .AllowToShowFieldTitle Then
'
'                            Set .FieldTitleToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("FixedString,12,RowNumber,13,FlowerTypeSample,18,RandomInteger,17")
'                        Else
'                            Set .FieldTitleOrderToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("Col1,12,Col2,13,Col3,18,Col4,17")
'                        End If
'
'
'                        Set objSheet = GetNewWorksheetAfterAllExistedSheetsFromBook(objBook)
'
'                        strSheetName = "CsvSQLTest_" & Format(i, "00")
'
'                        .OutputToSheetFrom strSQL, objSheet, strSheetName, enmExecutedAdoSqlQueryLogPositionType
'
'
'                        Set objAdditionalLogDic = New Scripting.Dictionary
'
'                        With objAdditionalLogDic
'
'                            .Add "Input AllowToRecordSQLLog", varAllowToRecordSQLLog
''
''                            .Add "After-SQL-executed AllowToRecordSQLLog", blnAfterAllowToRecordSQLLog
'
'                            .Add "ExecutedAdoSqlQueryLogPositionType", GetExecutedAdoSqlQueryLogPositionTypeTextFromEnm(enmExecutedAdoSqlQueryLogPositionType)
'
'                            .Add "AllowToShowFieldTitle", varAllowToShowFieldTitle
'
'                            .Add "TopLeftRowIndex", intTopLeftRowIndex
'
'                            .Add "TopLeftColumnIndex", intTopLeftColumnIndex
'                        End With
'
'                        OutputDicToAutoShapeLogTextOnSheet objSheet, objAdditionalLogDic, "CSV connecting ADOSheetFormatter some parameters test"
'
'                        i = i + 1
'                    Next
'                Next
'            Next
'        Next
'    End With
'
'    DeleteDummySheetWhenItExists objBook
'End Sub
'
'
''**---------------------------------------------
''** About a specialized parameters combination
''**---------------------------------------------
''''
''''
''''
'Private Sub msubSanityTestToXlAdoSheetExpanderWithSomeParameters()
'
'    Dim varRowCol As Variant, objRowCol As Collection, intTopLeftRowIndex As Long, intTopLeftColumnIndex As Long
'    Dim strOutputBookPath As String, objBook As Excel.Workbook, objSheet As Excel.Worksheet, strSheetName As String, i As Long
'    Dim strSQL As String
'
'    Dim varAllowToShowFieldTitle As Variant
'    Dim varAllowToRecordSQLLog As Variant, blnAfterAllowToRecordSQLLog As Boolean
'
'    Dim varExecutedAdoSqlQueryLogPositionType As Variant, enmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType
'
'    strOutputBookPath = mfstrGetSheetDecorationTestBookDir() & "\SomeParametersXlAdoSheetExpanderOutputSanityTest.xlsx"
'
'    Set objBook = GetWorkbookAndPrepareForUnitTest(strOutputBookPath)
'
'
'    i = 1
'
'    With New XlAdoSheetExpander
'
'        .SetInputExcelBookPath GetXlConnectingPrearrangedLogBookPath()
'
'        strSQL = "SELECT 'ABC' AS A_Column, 'DEF' AS B_Column"
'
'        '---
'
'        intTopLeftRowIndex = 5: intTopLeftColumnIndex = 7
'
'        varAllowToRecordSQLLog = True
'
'        enmExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable
'
'        varAllowToShowFieldTitle = True
'        'varAllowToShowFieldTitle = False
'
'
'
'        .TopLeftRowIndex = intTopLeftRowIndex
'
'        .TopLeftColumnIndex = intTopLeftColumnIndex
'
'        .AllowToShowFieldTitle = varAllowToShowFieldTitle
'
'        .AllowToRecordSQLLog = varAllowToRecordSQLLog
'
'        .RecordBordersType = RecordCellsGrayAll
'
'
'        Set objSheet = GetNewWorksheetAfterAllExistedSheetsFromBook(objBook)
'
'        strSheetName = "XlSQLTest_" & Format(i, "00")
'
'        .OutputToSheetFrom strSQL, objSheet, strSheetName, enmExecutedAdoSqlQueryLogPositionType
'
'    End With
'
'    DeleteDummySheetWhenItExists objBook
'End Sub
'
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
''**---------------------------------------------
''** About file-path
''**---------------------------------------------
''''
''''
''''
'Private Function mfstrGetSheetDecorationTestBookDir() As String
'
'    Dim strDir As String
'
'    strDir = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\ADOSheetFormatterTest"
'
'    ForceToCreateDirectory strDir
'
'    mfstrGetSheetDecorationTestBookDir = strDir
'End Function
'
'
'
'
'
'''--VBA_Code_File--<SQLGeneralFromXlSheet.bas>--
'Attribute VB_Name = "SQLGeneralFromXlSheet"
''
''   generate SQL part with dependent on Excel
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on Excel
''       Indpendent on ADO, this expects the PgSQL script creation.
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Tue,  7/Jun/2022    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''''
''''
''''
'Public Function GetSQLWhereFromNotNullConditionFromWorkbookPathAndSheetName(ByVal vstrBookPath As String, _
'        ByVal vstrSheetName As String, _
'        ByVal vintFieldTitlesRowIndex As Long, _
'        ByVal vintTopLeftColumnIndex As Long, _
'        Optional ByVal vobjColumnsExceptionIndexList As Scripting.Dictionary = Nothing, _
'        Optional ByVal vobjColumnsFieldTitleNameExceptionList As Scripting.Dictionary = Nothing) As String
'
'
'    Dim objFieldTitlesCol As Collection, varFieldTitle As Variant, strFieldTitle As String, intColumnsCount As Long
'    Dim strSQLPart As String, i As Long
'
'
'    Set objFieldTitlesCol = GetFieldTitlesColFromWorkbookPathAndSheetName(vstrBookPath, vstrSheetName, vintFieldTitlesRowIndex, vintTopLeftColumnIndex, vobjColumnsExceptionIndexList, vobjColumnsFieldTitleNameExceptionList)
'
'    intColumnsCount = objFieldTitlesCol.Count
'
'    strSQLPart = ""
'
'    i = 1
'
'    For Each varFieldTitle In objFieldTitlesCol
'
'        strFieldTitle = varFieldTitle
'
'        If intColumnsCount = 1 Then
'
'            strSQLPart = strSQLPart & strFieldTitle & " IS NOT NULL"
'        Else
'
'            strSQLPart = strSQLPart & strFieldTitle & " IS NULL"
'        End If
'
'        If i < objFieldTitlesCol.Count Then
'
'            strSQLPart = strSQLPart & " AND "
'        End If
'
'        i = i + 1
'    Next
'
'    If intColumnsCount > 1 Then
'
'        strSQLPart = "NOT (" & strSQLPart & ")"
'    End If
'
'    GetSQLWhereFromNotNullConditionFromWorkbookPathAndSheetName = strSQLPart
'End Function
'
'''--VBA_Code_File--<ExpandAdoRsetOnSheet.bas>--
'Attribute VB_Name = "ExpandAdoRsetOnSheet"
''
''   Utilities for expanding ADODB.Recordset into a region on a Excel.Worksheet with creating new sheet
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on both ADO and Excel
''       Dependent on IADOConnector.bas, ADOSheetFormatter.cls, DataTableSheetRangeAddresses.cls
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Thu,  9/Jun/2022    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Enumerations
''///////////////////////////////////////////////
''''
'''' Output created Excel-book processing flag
''''
'Public Enum OutputRSetExcelBookOpenModeProcessFlag
'
'    NoProcessAboutOutputExcelBook = 0
'
'    OpenOutputBookOnlyWhenItIsExist = &H1
'
'    DeleteAllSheetsBeforeSQLExecution = &H2
'
'    DeleteDefaultSheetsAfterSQLExecution = &H4
'
'    DeleteDummySheetWhenItExistsAfterSQLExecution = &H8
'
'
'    OpenOutputBookByUnitTestMode = DeleteAllSheetsBeforeSQLExecution Or DeleteDefaultSheetsAfterSQLExecution Or DeleteDummySheetWhenItExistsAfterSQLExecution
'
'    CloseOutputExcelBookAfterSQLExecution = &H10
'End Enum
'
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''**---------------------------------------------
''** Adding name of either ListObject or Range
''**---------------------------------------------
''''
'''' This is a complementary function.
''''
'''' If your main purpose is that the creating a table-object of SQL queried,
'''' you should simply use the Excel.Worksheet.QueryTables.Add(ConnectionString, LeftTopRange, SQL) method
''''
'Public Sub AddTableObjectWithNameOfExpandedRecordsetArea(ByVal vstrTableObjectName As String, _
'        ByVal vobjSheet As Excel.Worksheet, _
'        ByVal vobjDataTableSheetRangeAddresses As DataTableSheetRangeAddresses)
'
'    Dim objTableRange As Excel.Range
'
'    With vobjDataTableSheetRangeAddresses
'
'        If .FieldTitlesRangeAddress = "" Then
'
'            ' This means that AllowToShowFieldTitle = False
'
'            With vobjSheet.ListObjects.Add(xlSrcRange, vobjSheet.Range(.RecordAreaRangeAddress), XlListObjectHasHasHeaders:=xlNo, TableStyleName:="")
'
'                .Name = vstrTableObjectName
'            End With
'        Else
'            Set objTableRange = Union(vobjSheet.Range(.FieldTitlesRangeAddress), vobjSheet.Range(.RecordAreaRangeAddress))
'
'            With vobjSheet.ListObjects.Add(xlSrcRange, objTableRange, XlListObjectHasHasHeaders:=xlYes, TableStyleName:="")
'
'                .Name = vstrTableObjectName
'            End With
'        End If
'    End With
'End Sub
'
''''
'''' Use CurrentRegion
''''
'Public Sub AddTableObjectWithoutTableStyle(ByVal vstrTableObjectName As String, ByVal vobjTopLeftRange As Excel.Range)
'
'    Dim objTableRange As Excel.Range, objSheet As Excel.Worksheet
'
'    Set objTableRange = vobjTopLeftRange.CurrentRegion
'
'    Set objSheet = objTableRange.Worksheet
'
'    With objSheet.ListObjects.Add(xlSrcRange, objTableRange, XlListObjectHasHasHeaders:=xlYes, TableStyleName:="")
'
'        .Name = vstrTableObjectName
'    End With
'End Sub
'
'
''''
'''' Add a named range to Sheet
''''
'Public Sub AddNamedRangeOfExpandedRecordsetArea(ByVal vstrName As String, ByVal vobjSheet As Excel.Worksheet, ByVal vobjDataTableSheetRangeAddresses As DataTableSheetRangeAddresses)
'
'    Dim objTableRange As Excel.Range
'
'    With vobjDataTableSheetRangeAddresses
'
'        If .FieldTitlesRangeAddress = "" Then
'
'            ' This means that AllowToShowFieldTitle = False
'
'            With vobjSheet.Range(.RecordAreaRangeAddress)
'
'                .Name = vstrName
'            End With
'        Else
'            Set objTableRange = Union(vobjSheet.Range(.FieldTitlesRangeAddress), vobjSheet.Range(.RecordAreaRangeAddress))
'
'            With objTableRange
'
'                .Name = vstrName
'            End With
'        End If
'    End With
'End Sub
'
''''
'''' get SQL Command log Excel.Worksheet using an ADOSheetFormatter object
''''
'Public Function GetRdbSqlCommandLogWorksheet(ByRef rstrOutputSqlCommandLogBookPath As String, ByRef rstrSheetName As String, ByRef robjADOSheetFormatter As ADOSheetFormatter) As Excel.Worksheet
'
'    Dim objSheet As Excel.Worksheet, objBook As Excel.Workbook, strSqlSheetName As String
'
'    Set objBook = GetWorkbook(rstrOutputSqlCommandLogBookPath)
'
'    With robjADOSheetFormatter
'
'        Select Case .ExecutedAdoSqlCommandLogPosition
'
'            Case ExecutedAdoSqlCommandLogPositionType.AppendingSqlCommandLogsInSheetCellsOnSpecifiedSheet
'
'                strSqlSheetName = rstrSheetName
'
'            Case ExecutedAdoSqlCommandLogPositionType.AppendingSqlCommandLogsInSheetCellsOnFixedIndependentSheet
'
'                strSqlSheetName = .ExclusiveSQLLogSheetName
'        End Select
'    End With
'
'    ' The data-table sheet should be placed after the SQL log sheet
'
'    Set objSheet = GetSheetWithFindingNewNameWhenUsedRangeRowsCountExceedsLimit(strSqlSheetName, objBook)
'
'    Set GetRdbSqlCommandLogWorksheet = objSheet
'End Function
'
'
''**---------------------------------------------
''** Basic expanding
''**---------------------------------------------
''''
'''' output recordset object to sheet
''''
'Public Function OutputRecordsetToCellsOnSheetForSheetExpander(ByVal vobjHeaderInsertTexts As Collection, _
'        ByVal vobjADOSheetFormatter As ADOSheetFormatter, _
'        ByVal vobjOutputSheet As Excel.Worksheet, _
'        ByVal vobjRSet As ADODB.Recordset, _
'        ByVal vobjSQLResult As SQLResult, _
'        ByRef renmRdbConnectionInformationFlag As RdbConnectionInformationFlag, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable) As DataTableSheetRangeAddresses
'
'
'    SetSqlQueryLogOptionAndHeaderTextOptionForSheetExpander (Not vobjSQLResult Is Nothing), venmExecutedAdoSqlQueryLogPositionType, vobjHeaderInsertTexts, vobjADOSheetFormatter
'
'    Set OutputRecordsetToCellsOnSheetForSheetExpander = OutputRecordsetToCellsOnSheet(vobjOutputSheet, vobjRSet, vobjSQLResult, vobjADOSheetFormatter, renmRdbConnectionInformationFlag)
'End Function
'
'
''''
'''' query SQL and output result to Excel sheet, by interface IADOConnector object
''''
'''' <Argument>robjRSetAfterExpanded: Output</Argument>
'''' <Argument>robjDataTableSheetRangeAddresses: Output</Argument>
'''' <Argument>ritfIAdoConnector: Input</Argument>
'''' <Argument>vobjADOSheetFormatter: Input</Argument>
'''' <Argument>vstrSQL: Input</Argument>
'''' <Argument>vobjOutputSheet: Input</Argument>
'''' <Argument>venmExecutedAdoSqlQueryLogPositionType: Input</Argument>
'''' <Argument>venmShowUpAdoErrorOptionFlag: Input</Argument>
'''' <Argument>vobjHeaderInsertTexts: Input</Argument>
'''' <Argument>vblnChangeVisibleExcelApplicationIfItIsImvisible: Input, this is used only when the this code is executed from either Word or PowerPoint</Argument>
'Public Sub OutputSqlQueryResultsToSheetFromByIAdoConnector(ByRef robjRSetAfterExpanded As ADODB.Recordset, _
'        ByRef robjDataTableSheetRangeAddresses As DataTableSheetRangeAddresses, _
'        ByRef ritfIAdoConnector As IADOConnector, _
'        ByVal vobjADOSheetFormatter As ADOSheetFormatter, _
'        ByVal vstrSQL As String, _
'        ByVal vobjOutputSheet As Excel.Worksheet, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vobjHeaderInsertTexts As Collection = Nothing, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    Dim objRSet As ADODB.Recordset
'
'    Set objRSet = ritfIAdoConnector.GetRecordset(vstrSQL, venmShowUpAdoErrorOptionFlag)
'
'    If ritfIAdoConnector.IsConnected Then
'
'        Set robjDataTableSheetRangeAddresses = OutputRecordsetToCellsOnSheetForSheetExpander(vobjHeaderInsertTexts, vobjADOSheetFormatter, vobjOutputSheet, objRSet, ritfIAdoConnector.SQLExecutionResult, ritfIAdoConnector.RdbConnectionInformation, venmExecutedAdoSqlQueryLogPositionType)
'
'        If vblnChangeVisibleExcelApplicationIfItIsImvisible Then
'
'            If Not vobjOutputSheet.Application.Visible Then
'
'                ' The following is executed only when the ADO sheet-expander is used from either Word application or PowerPoint application
'
'                vobjOutputSheet.Application.Visible = True
'            End If
'        End If
'
'        If Not objRSet Is Nothing Then
'
'            If objRSet.State <> ADODB.ObjectStateEnum.adStateClosed Then
'
'                If Not objRSet.BOF Then
'
'                    objRSet.MoveFirst
'                End If
'
'                Set robjRSetAfterExpanded = objRSet
'            End If
'        End If
'    Else
'        Debug.Print "Failed to open ADODB.Connection -  the current ADODB.Connection.State is adStateClosed (is not connected.)."
'    End If
'End Sub
'
''''
''''
''''
'Public Sub SetSqlQueryLogOptionAndHeaderTextOptionForSheetExpander(ByVal vblnAllowToRecordSQLLog As Boolean, _
'        ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType, _
'        ByVal vobjHeaderInsertTexts As Collection, _
'        ByRef robjADOSheetFormatter As ADOSheetFormatter)
'
'    With robjADOSheetFormatter
'
'        If .ExecutedAdoSqlQueryLogPosition <> venmExecutedAdoSqlQueryLogPositionType Then
'
'            .ExecutedAdoSqlQueryLogPosition = venmExecutedAdoSqlQueryLogPositionType
'        End If
'    End With
'
'    SetSqlQueryAndCommandLogOptionAndHeaderTextOptionForSheetExpander vblnAllowToRecordSQLLog, vobjHeaderInsertTexts, robjADOSheetFormatter
'End Sub
'
'
''''
''''
''''
'Public Sub SetSqlQueryAndCommandLogOptionAndHeaderTextOptionForSheetExpander(ByVal vblnAllowToRecordSQLLog As Boolean, _
'        ByVal vobjHeaderInsertTexts As Collection, _
'        ByRef robjADOSheetFormatter As ADOSheetFormatter)
'
'    With robjADOSheetFormatter
'
'        If vblnAllowToRecordSQLLog Then
'
'            .AllowToWriteInsertTextInSheetHeader = True
'
'            If Not vobjHeaderInsertTexts Is Nothing Then
'
'                If .HeaderInsertTexts Is Nothing Then
'
'                    If Not .HeaderInsertTexts Is vobjHeaderInsertTexts Then
'
'                        Set .HeaderInsertTexts = vobjHeaderInsertTexts
'                    End If
'                End If
'            End If
'        Else
'            .AllowToWriteInsertTextInSheetHeader = False
'        End If
'    End With
'End Sub
'
''**---------------------------------------------
''** About SQL query Recordset sheet expander operations
''**---------------------------------------------
''''
''''
''''
'Public Sub SetupFirstCacheOfIAdoRecordsetSheetExpander(ByRef ritfAdoRecordsetSheetExpander As IExpandAdoRecordsetOnSheet)
'
'    Dim objLoadedOrCreatedOutputBook As Excel.Workbook
'
'    With ritfAdoRecordsetSheetExpander
'
'        If Not .IsCacheMemoriedOfOutputtedExcelBook Then
'
'            Set objLoadedOrCreatedOutputBook = GetOutputBookByPresetOpenModeOfSomeTypes(.CacheOutputExcelBookPath, .OutputRSetExcelBookOpenModeProcess)
'
'            .IsCacheMemoriedOfOutputtedExcelBook = True
'        End If
'    End With
'End Sub
'
'
''**---------------------------------------------
''** About SQL command sheet expander operations
''**---------------------------------------------
''''
'''' execute SQL command (UPDATE, INSERT, DELTE) and output result log to Excel sheet
''''
'Public Sub OutputSqlCommandLogToCellsOnSheetFromByIADOConnector(ByRef ritfADOConnector As IADOConnector, _
'        ByRef robjADOSheetFormatter As ADOSheetFormatter, _
'        ByVal vstrSQL As String, _
'        ByVal vobjOutputSqlCommandLogSheet As Excel.Worksheet, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vobjHeaderInsertTexts As Collection = Nothing, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'    Dim objRSet As ADODB.Recordset
'
'    With ritfADOConnector
'
'        If .IsConnected Then
'
'            Set objRSet = .ExecuteSQL(vstrSQL, venmShowUpAdoErrorOptionFlag)
'
'            SetSqlQueryAndCommandLogOptionAndHeaderTextOptionForSheetExpander (Not .SQLExecutionResult Is Nothing), vobjHeaderInsertTexts, robjADOSheetFormatter
'
'            ' SQL command log
'
'            OutputSQLBodyTextAndDetailAllLogToCellsOnSheet vobjOutputSqlCommandLogSheet, .SQLExecutionResult, robjADOSheetFormatter
'
'            If vblnChangeVisibleExcelApplicationIfItIsImvisible Then
'
'                If Not vobjOutputSqlCommandLogSheet.Application.Visible Then
'
'                    ' The following is executed only when the ADO sheet-expander is used from either Word application or PowerPoint application
'
'                    vobjOutputSqlCommandLogSheet.Application.Visible = True
'                End If
'            End If
'
'            If Not objRSet Is Nothing Then
'
'                If objRSet.State <> ADODB.ObjectStateEnum.adStateClosed Then
'
'                    objRSet.Close
'                End If
'            End If
'        Else
'            Debug.Print "[ADOConnecto...] ADO.Connection object isn't connected."
'        End If
'    End With
'End Sub
'
'
''**---------------------------------------------
''** Preprocess or Postprocess operations for AdoSheetExpander
''**---------------------------------------------
''''
'''' Pre-processing based on the OutputRSetExcelBookOpenModeProcessFlag value
''''
'Public Function GetOutputBookByPresetOpenModeOfSomeTypes(ByVal vstrExcelOutputBookPath As String, _
'        ByVal venmOutputRSetExcelBookOpenModeProcessFlag As OutputRSetExcelBookOpenModeProcessFlag) As Excel.Workbook
'
'    Dim objOutputBook As Excel.Workbook
'
'    Set objOutputBook = Nothing
'
'    If vstrExcelOutputBookPath <> "" Then
'
'        If (venmOutputRSetExcelBookOpenModeProcessFlag And OpenOutputBookByUnitTestMode) = OpenOutputBookByUnitTestMode Then
'
'            Set objOutputBook = GetWorkbookAndPrepareForUnitTest(vstrExcelOutputBookPath)
'
'        ElseIf (venmOutputRSetExcelBookOpenModeProcessFlag And OpenOutputBookOnlyWhenItIsExist) > 0 Then
'
'            With New Scripting.FileSystemObject
'
'                If .FileExists(vstrExcelOutputBookPath) Then
'
'                    Set objOutputBook = GetWorkbookIfItExists(vstrExcelOutputBookPath)
'                Else
'                    ' get New Workbook
'
'                    Set objOutputBook = GetNewWorksheetAfterAllExistedSheetsFromBook(ThisWorkbook)
'                End If
'            End With
'        Else
'            Set objOutputBook = GetWorkbook(vstrExcelOutputBookPath)
'        End If
'
'        If (venmOutputRSetExcelBookOpenModeProcessFlag And DeleteAllSheetsBeforeSQLExecution) > 0 Then
'
'            DeleteAllSheetsWithoutDummySheetButWhenTheDummySheetDoesntExistThenItIsAdded objOutputBook
'        End If
'    End If
'
'    Set GetOutputBookByPresetOpenModeOfSomeTypes = objOutputBook
'End Function
'
''''
'''' Post-processing based on the OutputRSetExcelBookOpenModeProcessFlag value
''''
'Public Sub DoAfterProcessingToOutputBook(ByVal vobjOutputBook As Excel.Workbook, _
'        ByVal venmOutputRSetExcelBookOpenModeProcessFlag As OutputRSetExcelBookOpenModeProcessFlag)
'
'    If (venmOutputRSetExcelBookOpenModeProcessFlag And DeleteDummySheetWhenItExistsAfterSQLExecution) > 0 Then
'
'        DeleteDummySheetWhenItExists vobjOutputBook
'    End If
'
'    If (venmOutputRSetExcelBookOpenModeProcessFlag And DeleteAllSheetsBeforeSQLExecution) = 0 Then
'
'        If (venmOutputRSetExcelBookOpenModeProcessFlag And DeleteDefaultSheetsAfterSQLExecution) > 0 Then
'
'            DeleteDefaultSheet vobjOutputBook
'        End If
'    End If
'
'    If (venmOutputRSetExcelBookOpenModeProcessFlag And CloseOutputExcelBookAfterSQLExecution) > 0 Then
'
'        CloseBookIfItIsOpened vobjOutputBook
'    End If
'End Sub
'
'
'''--VBA_Code_File--<AppendAdoRsetOnSheet.bas>--
'Attribute VB_Name = "AppendAdoRsetOnSheet"
''
''   Implementations for appending ADODB.Recordset to previous tables on Worksheets
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on both ADO and Excel
''       Dependent on IADOConnector.bas
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Fri, 14/Jul/2023    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''**---------------------------------------------
''** Recordset sheet appending operations
''**---------------------------------------------
''''
''''
''''
'
'
'''--VBA_Code_File--<IExpandAdoRecordsetOnSheet.cls>--
'VERSION 1.0 CLASS
'BEGIN
'  MultiUse = -1  'True
'END
'Attribute VB_Name = "IExpandAdoRecordsetOnSheet"
'Attribute VB_GlobalNameSpace = False
'Attribute VB_Creatable = False
'Attribute VB_PredeclaredId = False
'Attribute VB_Exposed = False
''
''   Interface class - interfaces for expanding Recordset into a new Excel.Worksheet
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on Excel
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Tue, 16/May/2023    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Property interfaces
''///////////////////////////////////////////////
'
'Public CurrentSheet As Excel.Worksheet
'
'Public CacheOutputExcelBookPath As String
'
'Public OutputRSetExcelBookOpenModeProcess As OutputRSetExcelBookOpenModeProcessFlag
'
'Public IsCacheMemoriedOfOutputtedExcelBook As Boolean
'
''///////////////////////////////////////////////
''/// Operation interfaces
''///////////////////////////////////////////////
'
'Public Sub CloseCacheOutputBookOfOpenByPresetModeProcess(): End Sub
'
'''--VBA_Code_File--<AdoRSetSheetExpander.cls>--
'VERSION 1.0 CLASS
'BEGIN
'  MultiUse = -1  'True
'END
'Attribute VB_Name = "AdoRSetSheetExpander"
'Attribute VB_GlobalNameSpace = False
'Attribute VB_Creatable = False
'Attribute VB_PredeclaredId = False
'Attribute VB_Exposed = False
''
''   Outputting Excel sheet common operations for SQL result Recordset object
''   Collect the actual method implementations for serveing Excel books with creating its
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on both ADO and Excel
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Fri, 12/May/2023    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Interface implementations
''///////////////////////////////////////////////
'Implements IExpandAdoRecordsetOnSheet
'
''///////////////////////////////////////////////
''/// Declarations
''///////////////////////////////////////////////
'
'Private mitfAdoConnector As IADOConnector
'
'Private mitfOutputBookPath As IOutputBookPath
'
'Private mobjADOSheetFormatter As ADOSheetFormatter
'
''**---------------------------------------------
''** About cache for adding ListObject with naming
''**---------------------------------------------
'Private mobjDataTableSheetRangeAddresses As DataTableSheetRangeAddresses
'
''**---------------------------------------------
''** About cache for Unit-test mode Workbook preparetion
''**---------------------------------------------
'Private mobjCurrentSheet As Excel.Worksheet
'
'Private mstrCacheOutputExcelBookPath As String
'
'Private menmOutputRSetExcelBookOpenModeProcessFlag As OutputRSetExcelBookOpenModeProcessFlag
'
'Private mblnIsCacheMemoriedOfOutputtedExcelBook As Boolean
'
''///////////////////////////////////////////////
''/// Event Handlers
''///////////////////////////////////////////////
'Private Sub Class_Initialize()
'
'    Set mobjADOSheetFormatter = New ADOSheetFormatter
'
'
'    Set mobjCurrentSheet = Nothing
'
'    mstrCacheOutputExcelBookPath = ""
'
'    menmOutputRSetExcelBookOpenModeProcessFlag = NoProcessAboutOutputExcelBook
'
'    mblnIsCacheMemoriedOfOutputtedExcelBook = False
'End Sub
'
'Private Sub Class_Terminate()
'
'    IExpandAdoRecordsetOnSheet_CloseCacheOutputBookOfOpenByPresetModeProcess
'End Sub
'
''///////////////////////////////////////////////
''/// Properties
''///////////////////////////////////////////////
''''
''''
''''
'Public Property Get ExpandAdoRecordsetOnSheetInterface() As IExpandAdoRecordsetOnSheet
'
'    Set ExpandAdoRecordsetOnSheetInterface = Me
'End Property
'
''///////////////////////////////////////////////
''/// Implemented properties
''///////////////////////////////////////////////
'Public Property Set IExpandAdoRecordsetOnSheet_CurrentSheet(ByVal vobjSheet As Excel.Worksheet)
'
'    Set mobjCurrentSheet = vobjSheet
'End Property
'Public Property Get IExpandAdoRecordsetOnSheet_CurrentSheet() As Excel.Worksheet
'
'    Set IExpandAdoRecordsetOnSheet_CurrentSheet = mobjCurrentSheet
'End Property
'
''''
''''
''''
'Public Property Let IExpandAdoRecordsetOnSheet_CacheOutputExcelBookPath(ByVal vstrCacheOutputExcelBookPath As String)
'
'    mstrCacheOutputExcelBookPath = vstrCacheOutputExcelBookPath
'End Property
'Public Property Get IExpandAdoRecordsetOnSheet_CacheOutputExcelBookPath() As String
'
'    IExpandAdoRecordsetOnSheet_CacheOutputExcelBookPath = mstrCacheOutputExcelBookPath
'End Property
'
'Public Property Let IExpandAdoRecordsetOnSheet_OutputRSetExcelBookOpenModeProcess(ByVal venmOutputRSetExcelBookOpenModeProcessFlag As OutputRSetExcelBookOpenModeProcessFlag)
'
'    menmOutputRSetExcelBookOpenModeProcessFlag = venmOutputRSetExcelBookOpenModeProcessFlag
'End Property
'Public Property Get IExpandAdoRecordsetOnSheet_OutputRSetExcelBookOpenModeProcess() As OutputRSetExcelBookOpenModeProcessFlag
'
'    IExpandAdoRecordsetOnSheet_OutputRSetExcelBookOpenModeProcess = menmOutputRSetExcelBookOpenModeProcessFlag
'End Property
'
'Public Property Let IExpandAdoRecordsetOnSheet_IsCacheMemoriedOfOutputtedExcelBook(ByVal vblnIsCacheMemoriedOfOutputtedExcelBook As Boolean)
'
'    mblnIsCacheMemoriedOfOutputtedExcelBook = vblnIsCacheMemoriedOfOutputtedExcelBook
'End Property
'Public Property Get IExpandAdoRecordsetOnSheet_IsCacheMemoriedOfOutputtedExcelBook() As Boolean
'
'    IExpandAdoRecordsetOnSheet_IsCacheMemoriedOfOutputtedExcelBook = mblnIsCacheMemoriedOfOutputtedExcelBook
'End Property
'
'
''///////////////////////////////////////////////
''/// Properties
''///////////////////////////////////////////////
''''
'''' Reference of an interface
''''
'Public Property Get ADOConnectorInterface() As IADOConnector
'
'    Set ADOConnectorInterface = mitfAdoConnector
'End Property
'Public Property Set ADOConnectorInterface(ByVal vitfAdoConnector As IADOConnector)
'
'    Set mitfAdoConnector = vitfAdoConnector
'End Property
'
''''
'''' Reference of an interface
''''
'Public Property Get OutputBookPathInterface() As IOutputBookPath
'
'    Set OutputBookPathInterface = mitfOutputBookPath
'End Property
'Public Property Set OutputBookPathInterface(ByVal vitfOutputBookPath As IOutputBookPath)
'
'    Set mitfOutputBookPath = vitfOutputBookPath
'End Property
'
''''
'''' old: ADOSheetFormatting
''''
'Public Property Get DataTableSheetFormattingInterface() As IDataTableSheetFormatter
'
'    Set DataTableSheetFormattingInterface = mobjADOSheetFormatter.DataTableSheetFormattingInterface
'End Property
'
''///////////////////////////////////////////////
''/// Implemented operations
''///////////////////////////////////////////////
''''
''''
''''
'Private Sub IExpandAdoRecordsetOnSheet_CloseCacheOutputBookOfOpenByPresetModeProcess()
'
'    Dim objOutputBook As Excel.Workbook
'
'    If menmOutputRSetExcelBookOpenModeProcessFlag <> OutputRSetExcelBookOpenModeProcessFlag.NoProcessAboutOutputExcelBook Then
'
'        If Not mobjCurrentSheet Is Nothing Then
'
'            Set objOutputBook = mobjCurrentSheet.Parent
'
'            If Not objOutputBook Is Nothing Then
'
'                DoAfterProcessingToOutputBook objOutputBook, menmOutputRSetExcelBookOpenModeProcessFlag
'            End If
'
'            Set mobjCurrentSheet = Nothing
'        End If
'    End If
'
'    mblnIsCacheMemoriedOfOutputtedExcelBook = False
'End Sub
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''''
''''
''''
'Public Sub CloseCacheOutputBookOfOpenByPresetModeProcess()
'
'    IExpandAdoRecordsetOnSheet_CloseCacheOutputBookOfOpenByPresetModeProcess
'End Sub
'
'
''**---------------------------------------------
''** about query SQL and expand results into Excel.Worksheet
''**---------------------------------------------
''''
'''' query SQL and output result to Excel sheet
''''
'Public Sub OutputToSheetFrom(ByVal vstrSQL As String, _
'        ByVal vobjOutputSheet As Excel.Worksheet, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vobjHeaderInsertTexts As Collection = Nothing, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'    Dim objRSetAfterExpanded As ADODB.Recordset
'
'
'    OutputSqlQueryResultsToSheetFromByIAdoConnector objRSetAfterExpanded, mobjDataTableSheetRangeAddresses, mitfAdoConnector, mobjADOSheetFormatter, vstrSQL, vobjOutputSheet, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, vobjHeaderInsertTexts, vblnChangeVisibleExcelApplicationIfItIsImvisible
'
'    Set mobjDataTableSheetRangeAddresses = mobjDataTableSheetRangeAddresses
'
'    Set mobjCurrentSheet = vobjOutputSheet
'
'    ' Close ADODB.Recordset exactly
'
'    If Not objRSetAfterExpanded Is Nothing Then
'
'        With objRSetAfterExpanded
'
'            If .State <> ADODB.ObjectStateEnum.adStateClosed Then
'
'                ' For PostgreSQL ODBC driver, Oracle ODBC driver, Access OLE DB provider, the following is not needed. The ADODB.Recordset is closed automatically when the ADODB.Connection has been closed
'
'                ' But the SQLite3 ODBC driver needs the following at the 2023 Aug.
'
'                .Close
'            End If
'        End With
'    End If
'End Sub
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputToSpecifiedBook(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vobjHeaderInsertTexts As Collection = Nothing, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'    Dim objNewSheet As Excel.Worksheet
'
'    Set objNewSheet = GetNewWorksheetAfterAllExistedSheetsAndFindNewSheetName(vstrOutputBookPath, vstrNewSheetName)
'
'    ' Debug code
''    With New Scripting.FileSystemObject
'
''        CurrentProcessInfoFromThisApplication objNewSheet.Parent, True, vstrNewSheetName & "_", .GetParentFolderName(GetCurrentOfficeFileObject().FullName)
''    End With
'
'    Me.OutputToSheetFrom vstrSQL, objNewSheet, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, vobjHeaderInsertTexts, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
'''' query SQL and output result to the Existed Excel sheet
''''
'Public Sub OutputInExistedBook(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vobjHeaderInsertTexts As Collection = Nothing, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'    Me.OutputToSpecifiedBook vstrSQL, mitfOutputBookPath.GetPreservedOutputBookPath(), vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, vobjHeaderInsertTexts, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
'''' query SQL and output result to the already specified Excel sheet
''''
'Public Sub OutputToAlreadySpecifiedBook(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vobjInsertTexts As Collection = Nothing, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    SetupFirstCacheOfIAdoRecordsetSheetExpander Me
'
'    OutputToSpecifiedBook vstrSQL, Me.ExpandAdoRecordsetOnSheetInterface.CacheOutputExcelBookPath(), vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, vobjInsertTexts, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
''''
''''
'Public Sub OutputInAlreadyExistedBook(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vobjInsertTexts As Collection = Nothing, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    With Me.ExpandAdoRecordsetOnSheetInterface
'
'        .CacheOutputExcelBookPath = mitfOutputBookPath.GetPreservedOutputBookPath()
'    End With
'
'    OutputToAlreadySpecifiedBook vstrSQL, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, vobjInsertTexts, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
''''
''''
'Public Sub OutputToSpecifiedBookByUnitTestMode(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vobjInsertTexts As Collection = Nothing, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    With Me.ExpandAdoRecordsetOnSheetInterface
'
'        .OutputRSetExcelBookOpenModeProcess = OpenOutputBookByUnitTestMode
'
'        .CacheOutputExcelBookPath = vstrOutputBookPath
'    End With
'
'    Me.OutputToAlreadySpecifiedBook vstrSQL, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, vobjInsertTexts, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
''''
''''
'Public Sub OutputInExistedBookByUnitTestMode(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vobjInsertTexts As Collection = Nothing, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    With Me.ExpandAdoRecordsetOnSheetInterface
'
'        .OutputRSetExcelBookOpenModeProcess = OpenOutputBookByUnitTestMode
'    End With
'
'    Me.OutputInAlreadyExistedBook vstrSQL, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, vobjInsertTexts, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''**---------------------------------------------
''** about command SQL
''**---------------------------------------------
''''
'''' execute SQL command (UPDATE, INSERT, DELTE) and output result log to Excel sheet
''''
'Public Sub OutputCommandLogToSheetFrom(ByVal vstrSQL As String, _
'        ByVal vobjOutputSheet As Excel.Worksheet, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vobjHeaderInsertTexts As Collection = Nothing, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'    OutputSqlCommandLogToCellsOnSheetFromByIADOConnector mitfAdoConnector, mobjADOSheetFormatter, vstrSQL, vobjOutputSheet, venmShowUpAdoErrorOptionFlag, vobjHeaderInsertTexts, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputCommandLogToSpecifiedBook(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vobjHeaderInsertTexts As Collection = Nothing, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    With mobjADOSheetFormatter
'
'        If vstrNewSqlCommandLogSheetName <> "" Then
'
'            .ExecutedAdoSqlCommandLogPosition = AppendingSqlCommandLogsInSheetCellsOnSpecifiedSheet
'        Else
'            .ExecutedAdoSqlCommandLogPosition = AppendingSqlCommandLogsInSheetCellsOnFixedIndependentSheet
'        End If
'    End With
'
'    Me.OutputCommandLogToSheetFrom vstrSQL, GetRdbSqlCommandLogWorksheet(vstrOutputBookPath, vstrNewSqlCommandLogSheetName, mobjADOSheetFormatter), venmShowUpAdoErrorOptionFlag, vobjHeaderInsertTexts, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputCommandLogToAlreadySpecifiedBook(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vobjHeaderInsertTexts As Collection = Nothing, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    SetupFirstCacheOfIAdoRecordsetSheetExpander Me
'
'    OutputCommandLogToSpecifiedBook vstrSQL, Me.ExpandAdoRecordsetOnSheetInterface.CacheOutputExcelBookPath(), venmShowUpAdoErrorOptionFlag, vobjHeaderInsertTexts, vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputCommandLogInAlreadyExistedBook(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vobjHeaderInsertTexts As Collection = Nothing, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    With Me.ExpandAdoRecordsetOnSheetInterface
'
'        .CacheOutputExcelBookPath = mitfOutputBookPath.GetPreservedOutputBookPath()
'    End With
'
'    OutputCommandLogToAlreadySpecifiedBook vstrSQL, venmShowUpAdoErrorOptionFlag, vobjHeaderInsertTexts, vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputCommandLogToSpecifiedBookByUnitTestMode(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vobjHeaderInsertTexts As Collection = Nothing, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    With Me.ExpandAdoRecordsetOnSheetInterface
'
'        .OutputRSetExcelBookOpenModeProcess = OpenOutputBookByUnitTestMode
'
'        .CacheOutputExcelBookPath = vstrOutputBookPath
'    End With
'
'    OutputCommandLogToAlreadySpecifiedBook vstrSQL, venmShowUpAdoErrorOptionFlag, vobjHeaderInsertTexts, vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputCommandLogInExistedBookByUnitTestMode(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vobjHeaderInsertTexts As Collection = Nothing, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    With Me.ExpandAdoRecordsetOnSheetInterface
'
'        .OutputRSetExcelBookOpenModeProcess = OpenOutputBookByUnitTestMode
'    End With
'
'    OutputCommandLogInAlreadyExistedBook vstrSQL, venmShowUpAdoErrorOptionFlag, vobjHeaderInsertTexts, vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
'
''**---------------------------------------------
''** About outputting Excel book control
''**---------------------------------------------
''''
''''
''''
'Public Sub SetOutputExcelBookPath(ByVal vstrOutputBookPath As String, _
'        Optional ByVal venmOutputRSetExcelBookOpenModeProcessFlag As OutputRSetExcelBookOpenModeProcessFlag = OutputRSetExcelBookOpenModeProcessFlag.OpenOutputBookByUnitTestMode)
'
'    mstrCacheOutputExcelBookPath = vstrOutputBookPath
'
'    menmOutputRSetExcelBookOpenModeProcessFlag = venmOutputRSetExcelBookOpenModeProcessFlag
'End Sub
'
'''--VBA_Code_File--<ISqlRsetSheetExpander.cls>--
'VERSION 1.0 CLASS
'BEGIN
'  MultiUse = -1  'True
'END
'Attribute VB_Name = "ISqlRsetSheetExpander"
'Attribute VB_GlobalNameSpace = False
'Attribute VB_Creatable = False
'Attribute VB_PredeclaredId = False
'Attribute VB_Exposed = False
''
''   Interface class - interfaces for expanding Recordset into a new Excel.Worksheet
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on both ADO and Excel
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Fri,  7/Jul/2023    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Operation interfaces
''///////////////////////////////////////////////
''''
''''
''''
'Public Function OutputToSheetFrom(ByVal vstrSQL As String, _
'        ByVal vobjSheet As Excel.Worksheet, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable) As ADODB.Recordset
'End Function
'
''''
''''
''''
'Public Function OutputToSpecifiedBook(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable) As ADODB.Recordset
'End Function
'
'
'
'
'''--VBA_Code_File--<XlAdoSheetExpander.cls>--
'VERSION 1.0 CLASS
'BEGIN
'  MultiUse = -1  'True
'END
'Attribute VB_Name = "XlAdoSheetExpander"
'Attribute VB_GlobalNameSpace = False
'Attribute VB_Creatable = False
'Attribute VB_PredeclaredId = False
'Attribute VB_Exposed = False
''
''   output SQL result into Excel.Worksheet after connecting Excel.Workbook by ADO OLE DB interface
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on both ADO and Excel
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Thu,  9/Jun/2022    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Interface implementations
''///////////////////////////////////////////////
'Implements IOutputBookPath
'
''///////////////////////////////////////////////
''/// Declarations
''///////////////////////////////////////////////
'Private mobjConnector As XlAdoConnector: Private mitfConnector As IADOConnector
'
'Private mobjAdoRecordsetSheetExpander As AdoRSetSheetExpander
'
'
''///////////////////////////////////////////////
''/// Event Handlers
''///////////////////////////////////////////////
'Private Sub Class_Initialize()
'
'    Set mobjConnector = New XlAdoConnector
'
'    Set mitfConnector = mobjConnector
'
'    Set mobjAdoRecordsetSheetExpander = New AdoRSetSheetExpander
'
'    With mobjAdoRecordsetSheetExpander
'
'        Set .ADOConnectorInterface = mobjConnector
'
'        Set .OutputBookPathInterface = Me
'
'        .DataTableSheetFormattingInterface.FieldTitleInteriorType = ColumnNameInteriorOfExcelSheetAdoSQL
'    End With
'End Sub
'
''''
''''
''''
'Private Sub Class_Terminate()
'
'    mobjAdoRecordsetSheetExpander.CloseCacheOutputBookOfOpenByPresetModeProcess
'
'    Debug.Print "class XlAdoSheetExpander terminate -> " & mobjAdoRecordsetSheetExpander Is Nothing
'
'    Me.CloseConnection
'
'    Set mobjAdoRecordsetSheetExpander = Nothing
'
'    Set mobjConnector = Nothing
'End Sub
'
'
''///////////////////////////////////////////////
''/// Properties - connoted ADO connector
''///////////////////////////////////////////////
'Public Property Get SQLExecutionResult() As SQLResult
'
'    Set SQLExecutionResult = mobjConnector.SQLExecutionResult
'End Property
'
'Public Property Get IsSqlExecutionFailed() As Boolean
'
'    IsSqlExecutionFailed = mobjConnector.IsSqlExecutionFailed
'End Property
'
'Public Property Let AllowToRecordSQLLog(ByVal vblnAllowToRecord As Boolean)
'
'    mobjConnector.AllowToRecordSQLLog = vblnAllowToRecord
'End Property
'Public Property Get AllowToRecordSQLLog() As Boolean
'
'    AllowToRecordSQLLog = mobjConnector.AllowToRecordSQLLog
'End Property
'
'
'Public Property Get CurrentSheet() As Excel.Worksheet
'
'    Set CurrentSheet = mobjAdoRecordsetSheetExpander.IExpandAdoRecordsetOnSheet_CurrentSheet
'End Property
'
'
'Public Property Get CurrentBook() As Excel.Workbook
'
'    If Not Me.CurrentSheet Is Nothing Then
'
'        Set CurrentBook = Me.CurrentSheet.Parent
'    Else
'        Set CurrentBook = Nothing
'    End If
'End Property
'
'
''///////////////////////////////////////////////
''/// Properties
''///////////////////////////////////////////////
''''
''''
''''
'Public Property Get ADOConnectorInterface() As IADOConnector
'
'    Set ADOConnectorInterface = mitfConnector
'End Property
'
''''
''''
''''
'Public Property Get DataTableSheetFormattingInterface() As IDataTableSheetFormatter
'
'    Set DataTableSheetFormattingInterface = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface
'End Property
'
'
''**---------------------------------------------
''** connoted DataTableSheetFormatter
''**---------------------------------------------
'Public Property Get TopLeftRowIndex() As Long
'
'    TopLeftRowIndex = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.TopLeftRowIndex
'End Property
'Public Property Let TopLeftRowIndex(ByVal vintTopLeftRowIndex As Long)
'
'    mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.TopLeftRowIndex = vintTopLeftRowIndex
'End Property
'
'Public Property Get TopLeftColumnIndex() As Long
'
'    TopLeftColumnIndex = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.TopLeftColumnIndex
'End Property
'Public Property Let TopLeftColumnIndex(ByVal vintTopLeftColumnIndex As Long)
'
'    mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.TopLeftColumnIndex = vintTopLeftColumnIndex
'End Property
'
'
'Public Property Let AllowToShowFieldTitle(ByVal vblnAllowToShowFieldTitle As Boolean)
'
'    mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.AllowToShowFieldTitle = vblnAllowToShowFieldTitle
'End Property
'Public Property Get AllowToShowFieldTitle() As Boolean
'
'    AllowToShowFieldTitle = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.AllowToShowFieldTitle
'End Property
'
'
'
'Public Property Let AllowToSetAutoFilter(ByVal vblnAllowToSetAutoFilter As Boolean)
'
'    mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.AllowToSetAutoFilter = vblnAllowToSetAutoFilter
'End Property
'Public Property Get AllowToSetAutoFilter() As Boolean
'
'    AllowToSetAutoFilter = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.AllowToSetAutoFilter
'End Property
'
''''
'''' Dictionary(Of String(column title), Double(column-width))
''''
'Public Property Set FieldTitleToColumnWidthDic(ByVal vobjFieldTitleToColumnWidthDic As Scripting.Dictionary)
'
'    Set mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.FieldTitleToColumnWidthDic = vobjFieldTitleToColumnWidthDic
'End Property
'Public Property Get FieldTitleToColumnWidthDic() As Scripting.Dictionary
'
'    Set FieldTitleToColumnWidthDic = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.FieldTitleToColumnWidthDic
'End Property
'
''''
'''' Dictionary(Of String(column title order, such as 'Col1', 'Col07'), Double(column-width))
''''
'Public Property Set FieldTitleOrderToColumnWidthDic(ByVal vobjFieldTitleOrderToColumnWidthDic As Scripting.Dictionary)
'
'    Set mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.FieldTitleOrderToColumnWidthDic = vobjFieldTitleOrderToColumnWidthDic
'End Property
'Public Property Get FieldTitleOrderToColumnWidthDic() As Scripting.Dictionary
'
'    Set FieldTitleOrderToColumnWidthDic = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.FieldTitleOrderToColumnWidthDic
'End Property
'
'
'
''''
'''' When it is true, then ConvertDataForColumns procedure is used.
''''
'Public Property Get AllowToConvertDataForSpecifiedColumns() As Boolean
'
'    AllowToConvertDataForSpecifiedColumns = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.AllowToConvertDataForSpecifiedColumns
'End Property
'Public Property Let AllowToConvertDataForSpecifiedColumns(ByVal vblnAllowToConvertDataForSpecifiedColumns As Boolean)
'
'    mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.AllowToConvertDataForSpecifiedColumns = vblnAllowToConvertDataForSpecifiedColumns
'End Property
''''
'''' Dictionary(Of String(column title), ColumnDataConvertType(Enum definition))
''''
'Public Property Get FieldTitlesToColumnDataConvertTypeDic() As Scripting.Dictionary
'
'    Set FieldTitlesToColumnDataConvertTypeDic = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.FieldTitlesToColumnDataConvertTypeDic
'End Property
'Public Property Set FieldTitlesToColumnDataConvertTypeDic(ByVal vobjFieldTitlesToColumnDataConvertTypeDic As Scripting.Dictionary)
'
'    Set mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.FieldTitlesToColumnDataConvertTypeDic = vobjFieldTitlesToColumnDataConvertTypeDic
'End Property
'
''''
'''' ColumnsNumberFormatLocalParam
''''
'Public Property Set ColumnsNumberFormatLocal(ByVal vobjColumnsNumberFormatLocalParam As ColumnsNumberFormatLocalParam)
'
'    Set mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.ColumnsNumberFormatLocal = vobjColumnsNumberFormatLocalParam
'End Property
'Public Property Get ColumnsNumberFormatLocal() As ColumnsNumberFormatLocalParam
'
'    Set ColumnsNumberFormatLocal = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.ColumnsNumberFormatLocal
'End Property
'
''''
'''' set format-condition for each column
''''
'Public Property Set ColumnsFormatCondition(ByVal vobjColumnsFormatConditionParam As ColumnsFormatConditionParam)
'
'    Set mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.ColumnsFormatCondition = vobjColumnsFormatConditionParam
'End Property
'Public Property Get ColumnsFormatCondition() As ColumnsFormatConditionParam
'
'    Set ColumnsFormatCondition = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.ColumnsFormatCondition
'End Property
'
''''
'''' merge cells when the same values continue
''''
'Public Property Let AllowToMergeCellsByContinuousSameValues(ByVal vblnAllowToMergeCellsByContinuousSameValues As Boolean)
'
'    mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.AllowToMergeCellsByContinuousSameValues = vblnAllowToMergeCellsByContinuousSameValues
'End Property
'Public Property Get AllowToMergeCellsByContinuousSameValues() As Boolean
'
'    AllowToMergeCellsByContinuousSameValues = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.AllowToMergeCellsByContinuousSameValues
'End Property
'Public Property Let AllowToSetVerticalTopAlignmentTopOfMergeCells(ByVal vblnAllowToSetVerticalTopAlignmentTopOfMergeCells As Boolean)
'
'    mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.AllowToSetVerticalTopAlignmentTopOfMergeCells = vblnAllowToSetVerticalTopAlignmentTopOfMergeCells
'End Property
'Public Property Get AllowToSetVerticalTopAlignmentTopOfMergeCells() As Boolean
'
'    AllowToSetVerticalTopAlignmentTopOfMergeCells = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.AllowToSetVerticalTopAlignmentTopOfMergeCells
'End Property
'
'Public Property Set FieldTitlesToMergeCells(ByVal vobjFieldTitlesToMergeCells As Collection)
'
'    Set mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.FieldTitlesToMergeCells = vobjFieldTitlesToMergeCells
'End Property
'Public Property Get FieldTitlesToMergeCells()
'
'    Set FieldTitlesToMergeCells = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.FieldTitlesToMergeCells
'End Property
'
'Public Property Let FieldTitleInteriorType(ByVal venmFieldTitleInteriorType As FieldTitleInterior)
'
'    mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.FieldTitleInteriorType = venmFieldTitleInteriorType
'End Property
'Public Property Get FieldTitleInteriorType() As FieldTitleInterior
'
'    FieldTitleInteriorType = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.FieldTitleInteriorType
'End Property
'
'Public Property Let RecordBordersType(ByVal venmRecordCellsBordersType As RecordCellsBorders)
'
'    mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.RecordBordersType = venmRecordCellsBordersType
'End Property
'Public Property Get RecordBordersType() As RecordCellsBorders
'
'    RecordBordersType = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.RecordBordersType
'End Property
'
''///////////////////////////////////////////////
''/// Properties - connoted ADOConnectionSetting
''///////////////////////////////////////////////
'Public Property Get ConnectSetting() As ADOConnectionSetting
'
'    Set ConnectSetting = mobjConnector.ConnectSetting
'End Property
'
''''
'''' unit is second. [s]
''''
'Public Property Let CommandTimeout(ByVal vintTimeout As Long)
'
'    mobjConnector.CommandTimeout = vintTimeout
'End Property
'Public Property Get CommandTimeout() As Long
'
'    CommandTimeout = mobjConnector.CommandTimeout
'End Property
'
'
''///////////////////////////////////////////////
''/// Properties - Excel-book connection by ACCESS OLE-DB provider
''///////////////////////////////////////////////
'Public Property Get ConnectingFilePath() As String
'
'    ConnectingFilePath = mobjConnector.ConnectingFilePath
'End Property
'
''''
'''' when the load Excel sheet doesn't have the field header title text, this is False
''''
'Public Property Get HeaderFieldTitleExists() As Boolean
'
'    HeaderFieldTitleExists = mobjConnector.HeaderFieldTitleExists
'End Property
'
'Public Property Get BookReadOnly() As Boolean
'
'    BookReadOnly = mobjConnector.BookReadOnly
'End Property
'
'Public Property Get IMEX() As IMEXMode
'
'    IMEX = mobjConnector.IMEX
'End Property
'
''///////////////////////////////////////////////
''/// Implemented operations
''///////////////////////////////////////////////
''''
''''
''''
'Private Function IOutputBookPath_GetPreservedOutputBookPath() As String
'
'    IOutputBookPath_GetPreservedOutputBookPath = GetXlConnectingPrearrangedLogBookPath()
'End Function
'
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''**---------------------------------------------
''** set ADO connection string all parameters directly
''**---------------------------------------------
''''
'''' set input Excel book path
''''
'Public Sub SetInputExcelBookPath(ByVal vstrBookPath As String, _
'        Optional venmIMEXMode As IMEXMode = IMEXMode.NoUseIMEXMode, _
'        Optional vblnHeaderFieldTitleExists As Boolean = True, _
'        Optional vblnBookReadOnly As Boolean = False, _
'        Optional ByVal vstrLockBookPassword As String = "", _
'        Optional vblnIsConnectedToOffice95 As Boolean = False)
'
'    mobjConnector.SetInputExcelBookPath vstrBookPath, venmIMEXMode, vblnHeaderFieldTitleExists, vblnBookReadOnly, vstrLockBookPassword, vblnIsConnectedToOffice95
'End Sub
'
''**---------------------------------------------
''** set ADO connection string parameters by a User-form interface
''**---------------------------------------------
''''
''''
''''
'''' <Argument>vstrSettingKeyName: Input</Argument>
'''' <Argument>vstrBookPath: Input</Argument>
'''' <Argument>venmIMEXMode: Input</Argument>
'''' <Argument>vblnHeaderFieldTitleExists: Input</Argument>
'''' <Argument>vblnBookReadOnly: Input</Argument>
'''' <Argument>venmAccessOLEDBProviderType: Input</Argument>
'''' <Argument>vstrBookLockPassword: Input</Argument>
'''' <Return>Boolean</Return>
'Public Function IsAdoConnectionContinuedAfterAccessOleDbConnectionParametersByForm(ByVal vstrSettingKeyName As String, _
'        Optional ByVal vstrBookPath As String = "", _
'        Optional ByVal venmIMEXMode As IMEXMode = IMEXMode.NoUseIMEXMode, _
'        Optional ByVal vblnHeaderFieldTitleExists As Boolean = True, _
'        Optional ByVal vblnBookReadOnly As Boolean = False, _
'        Optional ByVal venmAccessOLEDBProviderType As AccessOLEDBProviderType = AccessOLEDBProviderType.AceOLEDB_12P0, _
'        Optional ByVal vstrBookLockPassword As String = "")
'
'
'    IsAdoConnectionContinuedAfterAccessOleDbConnectionParametersByForm = mobjConnector.IsAdoConnectionContinuedAfterAccessOleDbConnectionParametersByForm(vstrSettingKeyName, vstrBookPath, venmIMEXMode, vblnHeaderFieldTitleExists, vblnBookReadOnly, venmAccessOLEDBProviderType, vstrBookLockPassword)
'End Function
'
'
'
''''
'''' Close ADO connection
''''
'Public Sub CloseConnection()
'
'    mobjConnector.CloseConnection
'End Sub
'
''''
''''
''''
'Public Sub CloseCacheOutputBookOfOpenByPresetModeProcess()
'
'    mobjAdoRecordsetSheetExpander.CloseCacheOutputBookOfOpenByPresetModeProcess
'End Sub
'
''''
''''
''''
'Public Sub CloseAll()
'
'    CloseConnection
'
'    CloseCacheOutputBookOfOpenByPresetModeProcess
'End Sub
'
''**---------------------------------------------
''** Use SQL after the ADO connected
''**---------------------------------------------
''''
'''' get recordset object by the Select SQL
''''
'Public Function GetRecordset(ByVal vstrSQL As String) As ADODB.Recordset
'
'    Set GetRecordset = mobjConnector.GetRecordset(vstrSQL)
'End Function
'
''**---------------------------------------------
''** Output Recordset and logs to cells on sheet
''**---------------------------------------------
''''
'''' query SQL and output result to Excel sheet
''''
'Public Sub OutputToSheetFrom(ByVal vstrSQL As String, _
'        ByVal vobjOutputSheet As Excel.Worksheet, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'    mobjAdoRecordsetSheetExpander.OutputToSheetFrom vstrSQL, vobjOutputSheet, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedBookPathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
'''' query SQL and output result to the Existed Excel sheet
''''
'Public Sub OutputInExistedBook(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputInExistedBook vstrSQL, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedBookPathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
'Public Sub OutputInAlreadyExistedBook(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputInAlreadyExistedBook vstrSQL, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedBookPathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
''''
''''
'Public Sub OutputInExistedBookByUnitTestMode(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputInExistedBookByUnitTestMode vstrSQL, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedBookPathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputToSpecifiedBook(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputToSpecifiedBook vstrSQL, vstrOutputBookPath, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedBookPathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
'Public Sub OutputToSpecifiedBookByUnitTestMode(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'    mobjAdoRecordsetSheetExpander.OutputToSpecifiedBookByUnitTestMode vstrSQL, vstrOutputBookPath, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedBookPathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
'''' query SQL and output result to the already specified Excel sheet
''''
'Public Sub OutputToAlreadySpecifiedBook(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'    mobjAdoRecordsetSheetExpander.OutputToAlreadySpecifiedBook vstrSQL, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedBookPathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
''''
''''
'Public Sub SetOutputExcelBookPath(ByVal vstrOutputBookPath As String, _
'        Optional ByVal venmOutputRSetExcelBookOpenModeProcessFlag As OutputRSetExcelBookOpenModeProcessFlag = OutputRSetExcelBookOpenModeProcessFlag.OpenOutputBookByUnitTestMode, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag)
'
'    mobjAdoRecordsetSheetExpander.SetOutputExcelBookPath vstrOutputBookPath, venmOutputRSetExcelBookOpenModeProcessFlag
'End Sub
'
''**---------------------------------------------
''** Excecute SQL command (UPDATE, INSERT, DELETE)
''**---------------------------------------------
''''
'''' command SQL and output result-log to Excel sheet
''''
'Public Sub OutputCommandLogToSheetFrom(ByVal vstrSQL As String, _
'        ByVal vobjOutputSqlCommandLogSheet As Excel.Worksheet, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogToSheetFrom vstrSQL, vobjOutputSqlCommandLogSheet, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedBookPathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputCommandLogToSpecifiedBook(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogToSpecifiedBook vstrSQL, vstrOutputBookPath, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedBookPathInsertText(), vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputCommandLogToAlreadySpecifiedBook(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogToAlreadySpecifiedBook vstrSQL, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedBookPathInsertText(), vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputCommandLogInAlreadyExistedBook(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogInAlreadyExistedBook vstrSQL, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedBookPathInsertText(), vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputCommandLogToSpecifiedBookByUnitTestMode(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogToSpecifiedBookByUnitTestMode vstrSQL, vstrOutputBookPath, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedBookPathInsertText(), vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
'''' query SQL and output result to the prepared Excel sheet
''''
'Public Sub OutputCommandLogInExistedBookByUnitTestMode(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogInExistedBookByUnitTestMode vstrSQL, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedBookPathInsertText(), vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
''''
''''
''''
'Private Function mfobjGetConnectedBookPathInsertText() As Collection
'
'    Dim objInsertTexts As Collection
'
'    Set objInsertTexts = New Collection
'
'    With mobjConnector
'
'        objInsertTexts.Add "<File Name> " & GetFileNameFromPathByVbaDir(.ConnectingFilePath) & ", <Directory> " & GetParentDirectoryPathByVbaDir(.ConnectingFilePath)
'    End With
'
'    Set mfobjGetConnectedBookPathInsertText = objInsertTexts
'End Function
'
'
'''--VBA_Code_File--<UTfAdoConnectExcelSheet.bas>--
'Attribute VB_Name = "UTfAdoConnectExcelSheet"
''
''   Sanity tests for USetAdoOleDbConStrForXlBook.frm
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on both ADO and Excel
''       Dependent on UTfDataTableSecurity.bas
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Tue, 25/Jul/2023    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
'
''///////////////////////////////////////////////
''/// Constants
''///////////////////////////////////////////////
'
'Private Const mstrSheetName As String = "TestSheet"
'
'
''///////////////////////////////////////////////
''/// Sanity tests
''///////////////////////////////////////////////
''**---------------------------------------------
''** XlAdoSheetExpander tests, which need to open the password locked Excel book by Microsoft Excel specification
''**---------------------------------------------
''''
'''' Use a User-form
''''
'Private Sub msubSanityTestToXlAdoSheetExpanderWithForm()
'
'    Dim strBookPath As String, strSQL As String
'
'    strBookPath = mfstrPrepareTestBookWithLockPassword()
'
'    With New XlAdoConnector
'
'        ' input the password by a User-form
'
'        If .IsAdoConnectionContinuedAfterAccessOleDbConnectionParametersByForm("TestAccOleDbToExcelWithPasswordKey", strBookPath) Then
'
'            strSQL = "SELECT * FROM [" & mstrSheetName & "$]"
'
'            DebugCol GetTableCollectionFromRSet(.GetRecordset(strSQL))
'        End If
'    End With
'End Sub
'
'Private Sub msubSanityTestToXlAdoSheetExpander()
'
'    Dim strBookPath As String, strSQL As String
'
'    strBookPath = mfstrPrepareTestBookWithLockPassword()
'
'    With New XlAdoSheetExpander
'
'        ' In the case, input the password directly
'
'        .SetInputExcelBookPath strBookPath, vstrLockBookPassword:="1234"
'
'        strSQL = "SELECT * FROM [" & mstrSheetName & "$]"
'
'        .OutputInExistedBookByUnitTestMode strSQL, "TestSQLResults", SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable
'    End With
'End Sub
'
''**---------------------------------------------
''** USetAdoOleDbConStrForXlBook form tests
''**---------------------------------------------
''''
'''' delete testing SettingKeyName from the registory
''''
'Private Sub msubSanityTestToDeleteTestingSettingKeyNamesForConnectingToAccessOleDbWithExcelSheet()
'
'    DeleteWinRegKeyValuesOfAdoConnectionStringInfo "TestAccOleDbToExcelKey"
'
'    DeleteWinRegKeyValuesOfAdoConnectionStringInfo "TestAccOleDbToExcelWithPasswordKey"
'End Sub
'
''''
'''' Excel book, which is not locked by any password.
''''
'Private Sub msubSanityTestToSetOdbcConnectionParametersByAccessOleDbToExcelSheetForm()
'
'    Dim blnIsRequestedToCancelAdoConnection As Boolean, objADOConStrOfAceOledbXl As ADOConStrOfAceOleDbXl
'    Dim strSettingKeyName As String, strBookPath As String
'
'
'    strSettingKeyName = "TestAccOleDbToExcelKey"
'
'    Set objADOConStrOfAceOledbXl = New ADOConStrOfAceOleDbXl
'
'    strBookPath = mfstrPrepareTestBook()
'
'    SetOleDbConnectionParametersByAccessOleDbToExcelSheetForm blnIsRequestedToCancelAdoConnection, objADOConStrOfAceOledbXl, strSettingKeyName, strBookPath
'End Sub
'
''''
'''' Connect a password locked Excel book
''''
'Private Sub msubSanityTestToSetOdbcConnectionParametersByAccessOleDbToExcelSheetFormWithPasswordLock()
'
'    Dim blnIsRequestedToCancelAdoConnection As Boolean, objADOConStrOfAceOledbXl As ADOConStrOfAceOleDbXl
'    Dim strSettingKeyName As String, strBookPath As String
'
'
'    strSettingKeyName = "TestAccOleDbToExcelWithPasswordKey"
'
'    Set objADOConStrOfAceOledbXl = New ADOConStrOfAceOleDbXl
'
'    strBookPath = mfstrPrepareTestBookWithLockPassword()
'
'    SetOleDbConnectionParametersByAccessOleDbToExcelSheetForm blnIsRequestedToCancelAdoConnection, objADOConStrOfAceOledbXl, strSettingKeyName, strBookPath, vstrBookLockPassword:="1234"
'End Sub
'
''''
'''' Connect a password locked Excel book
''''
'Private Sub msubSanityTestToSetOdbcConnectionParametersByAccessOleDbToExcelSheetFormWithPasswordLockAndOpen()
'
'    Dim blnIsRequestedToCancelAdoConnection As Boolean, objADOConStrOfAceOledbXl As ADOConStrOfAceOleDbXl
'    Dim strSettingKeyName As String, strBookPath As String
'
'
'    strSettingKeyName = "TestAccOleDbToExcelWithPasswordKey"
'
'    Set objADOConStrOfAceOledbXl = New ADOConStrOfAceOleDbXl
'
'    strBookPath = mfstrPrepareTestBookWithLockPassword()
'
'    SetOleDbConnectionParametersByAccessOleDbToExcelSheetForm blnIsRequestedToCancelAdoConnection, objADOConStrOfAceOledbXl, strSettingKeyName, strBookPath
'End Sub
'
''''
'''' Connect a password locked Excel book using a User-form
''''
'Private Sub msubSanityTestToSetOdbcConnectionParametersByAccessOleDbToExcelSheetFormWithPasswordLockAndOpenFormIfItNeeded()
'
'    Dim blnIsRequestedToCancelAdoConnection As Boolean, objADOConStrOfAceOledbXl As ADOConStrOfAceOleDbXl
'    Dim strSettingKeyName As String, strBookPath As String
'
'
'    strSettingKeyName = "TestAccOleDbToExcelKey"
'
'    Set objADOConStrOfAceOledbXl = New ADOConStrOfAceOleDbXl
'
'    strBookPath = mfstrPrepareTestBookWithLockPassword()
'
'    SetOleDbConnectionParametersByAccessOleDbToExcelSheetForm blnIsRequestedToCancelAdoConnection, objADOConStrOfAceOledbXl, strSettingKeyName, strBookPath
'End Sub
'
'
'
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
''''
''''
''''
'Private Function mfstrPrepareTestBookWithLockPassword() As String
'
'    Dim strPath As String
'
'    strPath = mfstrGetTemporaryAdoConnectingTestExcelBookDir() & "\AdoConectionUITestWithLockPassword.xlsx"
'
'    PrepareTestBookWithLockingPasswordAndCreating strPath, mstrSheetName, "1234"
'
'    mfstrPrepareTestBookWithLockPassword = strPath
'End Function
'
''''
''''
''''
'Private Function mfstrPrepareTestBook() As String
'
'    Dim strPath As String
'
'    strPath = mfstrGetTemporaryAdoConnectingTestExcelBookDir() & "\AdoConectionUITest.xlsx"
'
'    PrepareTestBookWithLockingPasswordAndCreating strPath, mstrSheetName, ""
'
'    mfstrPrepareTestBook = strPath
'End Function
'
'
''''
''''
''''
'Private Function mfstrGetTemporaryAdoConnectingTestExcelBookDir() As String
'
'    Dim strDir As String
'
'    strDir = GetParentDirectoryPathOfCurrentOfficeFile() & "\TemporaryBooks\AdoConnectionUITests"
'
'    'strDir = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\AdoConnectionUITests"
'
'    ForceToCreateDirectory strDir
'
'    mfstrGetTemporaryAdoConnectingTestExcelBookDir = strDir
'End Function
'
'''--VBA_Code_File--<SqlUTfXlSheetExpander.bas>--
'Attribute VB_Name = "SqlUTfXlSheetExpander"
''
''   Sanity test to output SQL result into an output Excel sheet for a source Excel file sheets in a directory using ADO OLE DB
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on both ADO and Excel
''       Dependent on Microsoft Office Access Data Base Engine OLE DB Provider,
''       which should have been the installed when the Microsoft Office Excel software has been installed.
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Fri, 20/Jan/2023    Kalmclaeyd Tarclanus    Added the multi-language caption function and tested
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Constants
''///////////////////////////////////////////////
'Private Const mstrOutputBaseName As String = "SQLResultsOfUTfXlAdoSheetExpander"
'
'
''///////////////////////////////////////////////
''/// Sanity tests
''///////////////////////////////////////////////
''**---------------------------------------------
''** XlAdoSheetExpander
''**---------------------------------------------
''''
'''' Create an in-line table from the only SELECT sql inside
''''
'Private Sub msubSanityTestToConnectByVirtualTableXlConnector()
'
'    Dim strSQL As String
'
'    With New XlAdoSheetExpander
'
'        .SetInputExcelBookPath GetXlConnectingPrearrangedLogBookPath()
'
'        .AllowToRecordSQLLog = True
'
'        'strSQL = "SELECT 'ABC', 'DEF' UNION SELECT 'GHI', 'JKL'" ' Error occurs
'
'        'strSQL = "SELECT 1"
'
'        'strSQL = "SELECT 'ABC', 'DEF'"
'
'        strSQL = "SELECT 'ABC' AS A_Column, 'DEF' AS B_Column"
'
'        .OutputInExistedBookByUnitTestMode strSQL, "Test_ADO_VirtualTable"
'
'
'        .CloseCacheOutputBookOfOpenByPresetModeProcess
'    End With
'End Sub
'
''''
'''' Create an in-line table from the only SELECT sql inside
''''
'Private Sub msubSanityTestToConnectForExpandingOperations()
'
'    Dim objSheet As Excel.Worksheet, strOutputBookPath As String, objBook As Excel.Workbook
'    Dim strSQL As String
'
'    ForceToCreateDirectory GetTemporaryDevelopmentVBARootDir()
'
'    strOutputBookPath = GetTemporaryDevelopmentVBARootDir() & "\SanityTestToOutputOperationsOfAdoRecordsetSheetExpander.xlsx"
'
'    Set objBook = GetWorkbookAndPrepareForUnitTest(strOutputBookPath)
'
'    With New XlAdoSheetExpander
'
'        .SetInputExcelBookPath GetXlConnectingPrearrangedLogBookPath()
'
'        .AllowToRecordSQLLog = True
'
'
'
'        strSQL = "SELECT 'ABC' AS A_Column, 'DEF' AS B_Column"
'
'
'        .OutputInExistedBookByUnitTestMode strSQL, "Test_ADO_VirtualTable"
'
'        .OutputInAlreadyExistedBook strSQL, "VirtualTable02"
'
'        Set objSheet = GetNewWorksheetAfterAllExistedSheetsFromBook(objBook)
'
'        objSheet.Name = "VirtualTable03"
'
'        .OutputToSheetFrom strSQL, objSheet
'
'        .OutputInExistedBook strSQL, "VirtualTable04"
'
'        .OutputToSpecifiedBookByUnitTestMode strSQL, strOutputBookPath, "VirtualTable05"
'
'        .OutputToSpecifiedBook strSQL, strOutputBookPath, "VirtualTable06"
'
'
'        .CloseCacheOutputBookOfOpenByPresetModeProcess
'
'        DeleteDummySheetWhenItExists .CurrentSheet
'    End With
'End Sub
'
''''
'''' SQL logs will be appended on a specified sheet.
''''
'Private Sub msubSanityTestToConnectByVirtualTableXlConnectorForSQLLogAppending()
'
'    Dim strSQL As String
'
'    With New XlAdoSheetExpander
'
'        .SetInputExcelBookPath GetXlConnectingPrearrangedLogBookPath()
'
'        strSQL = "SELECT 'ABC' AS A_Column, 'DEF' AS B_Column"
'
'        .OutputInExistedBookByUnitTestMode strSQL, "Test_ADO_VirtualTable", AppendingSqlQueryLogsInSheetCellsOnIndependentSheet
'
'        strSQL = "SELECT 'EFG' AS C_Column, 'HIJ' AS D_Column"
'
'        .OutputInExistedBookByUnitTestMode strSQL, "Test_ADO_VirtualTable02", AppendingSqlQueryLogsInSheetCellsOnIndependentSheet
'
'        strSQL = "SELECT 'KLM' AS E_Column, 'OPQ' AS F_Column"
'
'        .OutputInExistedBookByUnitTestMode strSQL, "Test_ADO_VirtualTable03", AppendingSqlQueryLogsInSheetCellsOnIndependentSheet
'
'
'        DeleteDummySheetWhenItExists .CurrentBook
'
'        .CloseCacheOutputBookOfOpenByPresetModeProcess
'    End With
'End Sub
'
''''
'''' Confirm the auto shape log of SQL select
''''
'Private Sub msubSanityTestToConnectByVirtualTableXlConnectorWithAutoShapeLog()
'
'    Dim strSQL As String
'
'    With New XlAdoSheetExpander
'
'        .SetInputExcelBookPath GetXlConnectingPrearrangedLogBookPath()
'
'        strSQL = "SELECT 'ABC' AS A_Column, 'DEF' AS B_Column"
'
'        .OutputInExistedBookByUnitTestMode strSQL, "Test_ADO_VirtualTable", SqlQueryLogsByAutoShapesOnSheetWithOutputtingRecordsetDataTable
'
'        .CloseCacheOutputBookOfOpenByPresetModeProcess
'    End With
'End Sub
'
''''
'''' show UErrorADOSQLForm tests
''''
'Private Sub msubSanityTestToShowErrorTrapFormByIllegalSQL()
'
'    Dim strSQL As String, objRSet As ADODB.Recordset
'
'    With New XlAdoSheetExpander
'
'        .SetInputExcelBookPath GetXlConnectingPrearrangedLogBookPath()
'
'        strSQL = "(SELECT 'ABC', 'DEF') UNION ALL (SELECT 'GHI', 'JKL')" ' This causes a error. It seems that UNION ALL is not applicable....
'
'        'strSQL = "(SELECT 'ABC' AS A_Column, 'DEF' AS B_Column) UNION ALL (SELECT 'GHI', 'JKL')" ' Error occurs
'
'        .OutputInExistedBookByUnitTestMode strSQL, "Test_ADO_VirtualTable"
'    End With
'End Sub
'
'
'
''''
''''
''''
'Private Sub msubSanityTest01ToConnectExcelBookTestAfterCreateBook()
'
'    GetOutputExcelBookPathOfConnectingBookTestAfterCreateBook BasicSampleDT, 100, False
'End Sub
'
''''
''''
''''
'Private Sub msubSanityTest02ToConnectExcelBookTestAfterCreateBook()
'
'    GetOutputExcelBookPathOfConnectingBookTestAfterCreateBook IncludeFictionalProductionNumberSampleDT, 1000, True
'End Sub
'
'
''''
''''
''''
'Private Sub msubSanityTestToSQLJoinWithConnectingExcelBookAfterItIsCreated()
'
'    Dim strInputBookPath As String, strSheetName01 As String, strSheetName02 As String, strSQL As String
'
'    strInputBookPath = GetOutputBookPathAfterCreateALeastTestSheetSet()
'
'    With New XlAdoSheetExpander
'
'        .SetInputExcelBookPath strInputBookPath
'
'        strSheetName01 = "[" & GetSheetNameOfPreparedSampleDataTableExcelBookForTest(IncludeFictionalProductionNumberSampleDT) & "$]"
'
'        strSheetName02 = "[" & GetSheetNameOfPreparedSampleDataTableExcelBookForTest(FishTypeSampleMasterDT) & "$]"
'
'        strSQL = "SELECT * FROM " & strSheetName01
'
'        .OutputInExistedBookByUnitTestMode strSQL, "CreatedTable01"
'
'        strSQL = "SELECT * FROM " & strSheetName02
'
'        .OutputInExistedBookByUnitTestMode strSQL, "SampleMasterTable02"
'
'        strSQL = "SELECT A.RowNumber, A.RealNumber, B.FishTypeJpn, B.RandAlpha FROM " & strSheetName01 & " AS A LEFT JOIN " & strSheetName02 & " AS B ON A.FishType = B.FishTypeEng"
'
'        .OutputInExistedBookByUnitTestMode strSQL, "SQLResult"
'    End With
'
'End Sub
'
'
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''''
'''' This is also referenced from UTfReadFromShapeOnXlSheet.bas
''''
'Public Function GetOutputExcelBookPathOfConnectingBookTestAfterCreateBook(ByVal venmUnitTestPreparedSampleDataTable As UnitTestPreparedSampleDataTable, Optional ByVal vintCountOfRows As Long = 100, Optional ByVal vblnIncludesSQLLogToSheet As Boolean = True) As String
'
'    Dim strInputBookPath As String, strSQL As String, strLog As String, strSheetName As String
'    Dim strOutputBookPath As String
'
'    strInputBookPath = GetExcelBookPathAfterCheckFileExistenceOrCreateWhenItDoesntExist(strSheetName, venmUnitTestPreparedSampleDataTable, vintCountOfRows)
'
'    With New XlAdoSheetExpander
'
'        .SetInputExcelBookPath strInputBookPath
'
'        With .DataTableSheetFormattingInterface
'
'            Set .FieldTitleOrderToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("Col1,12,Col2,15")
'
'            Set .FieldTitleToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("FlowerTypeSample,19,RandomInteger,16")
'
'            .RecordBordersType = RecordCellsGrayAll
'        End With
'
'        strSQL = "SELECT * FROM [" & strSheetName & "$]"
'
'        .OutputInExistedBookByUnitTestMode strSQL, "Load_" & strSheetName, vblnIncludesSQLLogToSheet
'
'
'        strLog = GetLogTextOfPreparedSampleDataTableBaseName(venmUnitTestPreparedSampleDataTable)
'
'
'        AddAutoShapeGeneralMessage .CurrentSheet, strLog
'
'        strOutputBookPath = .CurrentBook.FullName
'
'        DeleteDummySheetWhenItExists .CurrentBook
'    End With
'
'    GetOutputExcelBookPathOfConnectingBookTestAfterCreateBook = strOutputBookPath
'End Function
'
'
'
''''
'''' XlAdoSheetExpander unit test
''''
'Public Sub SanityTestAboutXlSheetExpander()
'
'    Dim strInputPath As String, strOutputPath As String, strSheetName As String, objFieldTitleToColumnWidthDic As Scripting.Dictionary
'    Dim strSQL As String, strSQLTableName As String
'
'
'    GetBookPathAndSheetNameAndFieldTitleToColumnWidthDicFromUnitTestPreparedSampleDataTable strInputPath, strSheetName, objFieldTitleToColumnWidthDic, UnitTestPreparedSampleDataTable.BasicSampleDT
'
'    strOutputPath = GetCurrentBookOutputDir() & mstrOutputBaseName & ".xlsx"
'
'    With New XlAdoSheetExpander
'
'        .SetInputExcelBookPath strInputPath
'
'        .AllowToRecordSQLLog = True
'
'        Set .FieldTitleToColumnWidthDic = objFieldTitleToColumnWidthDic
'
'
'        strSQLTableName = "[" & strSheetName & "$]"
'
'        strSQL = "SELECT * FROM " & strSQLTableName
'
'        .OutputToSpecifiedBookByUnitTestMode strSQL, strOutputPath, "QueryTest", True
'    End With
'End Sub
'
'
'
'
'
'''--VBA_Code_File--<SqlUTfXlSheetExpanderByPassword.bas>--
'Attribute VB_Name = "SqlUTfXlSheetExpanderByPassword"
''
''   Sanity test to connect Excel sheet with a locked password for opening
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on both Excel and ADO
''       Dependent on UTfDataTableSecurity.bas, UTfDecorationSetterToXlSheet.bas
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Thu, 29/Jun/2023    Kalmclaeyd Tarclanus    Initial creation
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Constants
''///////////////////////////////////////////////
'
'Private Const mstrSheetName As String = "TestSheet"
'
'
''**---------------------------------------------
''** XlAdoConnector tests for password locked books
''**---------------------------------------------
''''
''''
''''
'Private Sub msubSanityTestToXlAdoConnectorForPasswordLockedBook()
'
'    Dim strBookPath As String, strSQL As String
'
'    strBookPath = GetBookSecurityTemporaryGeneratingDirectoryPath() & "\SamplePasswordLockedBook.xlsx"
'
'    PrepareTestBookWithLockingPasswordAndCreating strBookPath, mstrSheetName, "1234"
'
'    With New XlAdoConnector
'
'        .SetInputExcelBookPath strBookPath, vstrLockBookPassword:="1234"
'
'        strSQL = "SELECT * FROM [" & mstrSheetName & "$]"
'
'        DebugCol GetTableCollectionFromRSet(.GetRecordset(strSQL))
'    End With
'End Sub
'
''''
''''
''''
'Private Sub msubSanityTestToXlAdoExpanderForPasswordLockedBook()
'
'    Dim strBookPath As String, strSQL As String
'
'    strBookPath = GetBookSecurityTemporaryGeneratingDirectoryPath() & "\SamplePasswordLockedBook.xlsx"
'
'    PrepareTestBookWithLockingPasswordAndCreating strBookPath, mstrSheetName, "1234"
'
'    With New XlAdoSheetExpander
'
'        .SetInputExcelBookPath strBookPath, vstrLockBookPassword:="1234"
'
'        strSQL = "SELECT * FROM [" & mstrSheetName & "$]"
'
'        .OutputInExistedBookByUnitTestMode strSQL, "ATest", SqlQueryLogsByAutoShapesOnSheetWithOutputtingRecordsetDataTable
'
'        DeleteDummySheetWhenItExists .CurrentBook
'
'        .CloseAll
'    End With
'End Sub
'
'
''**---------------------------------------------
''** ADO connection tests
''**---------------------------------------------
''''
''''
''''
'Private Sub msubSanityTestToAdoConnectionToNoPasswordBook()
'
'    Dim strBookPath As String, objBook As Excel.Workbook
'    Dim objADOConStrOfAceOledbXl As ADOConStrOfAceOleDbXl, objConnection As ADODB.Connection
'
'
'    strBookPath = GetBookSecurityTemporaryGeneratingDirectoryPath() & "\SampleBookWithNoLocked.xlsx"
'
'    PrepareTestBookWithLockingPasswordAndCreating strBookPath, mstrSheetName, ""
'
'    Set objADOConStrOfAceOledbXl = New ADOConStrOfAceOleDbXl
'
'    With objADOConStrOfAceOledbXl
'
'        .SetExcelBookConnection strBookPath, NoUseIMEXMode
'    End With
'
'    Set objConnection = New ADODB.Connection
'
'    With objConnection
'
'        .ConnectionString = objADOConStrOfAceOledbXl.GetConnectionString
'
'        On Error Resume Next
'
'        .Open
'
'        If Err.Number = 0 Then
'
'            Debug.Print "Expected result to be connected"
'
'            .Close
'        End If
'
'        On Error GoTo 0
'    End With
'
'End Sub
'
''''
''''
''''
'Private Sub msubSanityTestToAdoConnectionToPasswordLockedBook()
'
'    Dim strBookPath As String, objBook As Excel.Workbook
'    Dim objADOConStrOfAceOledbXl As ADOConStrOfAceOleDbXl, objConnection As ADODB.Connection
'
'
'    strBookPath = GetBookSecurityTemporaryGeneratingDirectoryPath() & "\SamplePasswordLockedBook.xlsx"
'
'    PrepareTestBookWithLockingPasswordAndCreating strBookPath, mstrSheetName, "1234"
'
'    Set objADOConStrOfAceOledbXl = New ADOConStrOfAceOleDbXl
'
'    With objADOConStrOfAceOledbXl
'
'        .SetExcelBookConnection strBookPath, NoUseIMEXMode
'    End With
'
'    Set objConnection = New ADODB.Connection
'
'    With objConnection
'
'        .ConnectionString = objADOConStrOfAceOledbXl.GetConnectionString
'
'        On Error Resume Next
'
'        .Open
'
'        If Err.Number <> 0 Then
'
'            Debug.Print "Expected result to be failed connecting"
'        Else
'            .Close
'        End If
'
'        On Error GoTo 0
'    End With
'End Sub
'
'
''''
''''
''''
'Private Sub msubSanityTestToAdoConnectionToPasswordLockedBookWithOpening()
'
'    Dim strBookPath As String, objBook As Excel.Workbook
'    Dim objADOConStrOfAceOledbXl As ADOConStrOfAceOleDbXl, objConnection As ADODB.Connection
'
'
'    strBookPath = GetBookSecurityTemporaryGeneratingDirectoryPath() & "\SamplePasswordLockedBook.xlsx"
'
'    PrepareTestBookWithLockingPasswordAndCreating strBookPath, mstrSheetName, "1234"
'
'    Set objBook = GetWorkbook(strBookPath, False, vstrPassword:="1234")
'
'    Set objADOConStrOfAceOledbXl = New ADOConStrOfAceOleDbXl
'
'    With objADOConStrOfAceOledbXl
'
'        .SetExcelBookConnection strBookPath, NoUseIMEXMode
'    End With
'
'    Set objConnection = New ADODB.Connection
'
'    With objConnection
'
'        .ConnectionString = objADOConStrOfAceOledbXl.GetConnectionString
'
'        On Error Resume Next
'
'        .Open
'
'        If Err.Number = 0 Then
'
'            Debug.Print "Expected result to be connected, when the password locked Book is opened in this Excel"
'
'            .Close
'        End If
'
'        On Error GoTo 0
'    End With
'End Sub
'
'''--VBA_Code_File--<CsvAdoSheetExpander.cls>--
'VERSION 1.0 CLASS
'BEGIN
'  MultiUse = -1  'True
'END
'Attribute VB_Name = "CsvAdoSheetExpander"
'Attribute VB_GlobalNameSpace = False
'Attribute VB_Creatable = False
'Attribute VB_PredeclaredId = False
'Attribute VB_Exposed = False
''
''   output SQL result into Excel.Worksheet after connecting CSV file by ADO-DB interface
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on both ADO and Excel
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Fri, 10/Jun/2022    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
'
''///////////////////////////////////////////////
''/// Interface implementations
''///////////////////////////////////////////////
'Implements IOutputBookPath
'
''///////////////////////////////////////////////
''/// Declarations
''///////////////////////////////////////////////
'Private mobjConnector As CsvAdoConnector
'
'Private mobjAdoRecordsetSheetExpander As AdoRSetSheetExpander
'
'
''///////////////////////////////////////////////
''/// Event Handlers
''///////////////////////////////////////////////
'Private Sub Class_Initialize()
'
'    Set mobjConnector = New CsvAdoConnector
'
'    Set mobjAdoRecordsetSheetExpander = New AdoRSetSheetExpander
'
'    With mobjAdoRecordsetSheetExpander
'
'        Set .ADOConnectorInterface = mobjConnector
'
'        Set .OutputBookPathInterface = Me
'
'        .DataTableSheetFormattingInterface.FieldTitleInteriorType = ColumnNameInteriorOfCsvAdoSQL
'    End With
'End Sub
'
'Private Sub Class_Terminate()
'
'    mobjAdoRecordsetSheetExpander.CloseCacheOutputBookOfOpenByPresetModeProcess
'
'    Me.CloseConnection
'
'    Set mobjAdoRecordsetSheetExpander = Nothing
'
'    Set mobjConnector = Nothing
'End Sub
'
''///////////////////////////////////////////////
''/// Properties - connoted ADO connector
''///////////////////////////////////////////////
'Public Property Get SQLExecutionResult() As SQLResult
'
'    Set SQLExecutionResult = mobjConnector.SQLExecutionResult
'End Property
'
'Public Property Get IsSqlExecutionFailed() As Boolean
'
'    IsSqlExecutionFailed = mobjConnector.IsSqlExecutionFailed
'End Property
'
'Public Property Let AllowToRecordSQLLog(ByVal vblnAllowToRecord As Boolean)
'
'    mobjConnector.AllowToRecordSQLLog = vblnAllowToRecord
'End Property
'Public Property Get AllowToRecordSQLLog() As Boolean
'
'    AllowToRecordSQLLog = mobjConnector.AllowToRecordSQLLog
'End Property
'
'
'Public Property Get CurrentSheet() As Excel.Worksheet
'
'    Set CurrentSheet = mobjAdoRecordsetSheetExpander.IExpandAdoRecordsetOnSheet_CurrentSheet
'End Property
'
'
'Public Property Get CurrentBook() As Excel.Workbook
'
'    If Not Me.CurrentSheet Is Nothing Then
'
'        Set CurrentBook = Me.CurrentSheet.Parent
'    Else
'        Set CurrentBook = Nothing
'    End If
'End Property
'
''///////////////////////////////////////////////
''/// Properties
''///////////////////////////////////////////////
''''
''''
''''
'Public Property Get ADOConnectorInterface() As IADOConnector
'
'    Set ADOConnectorInterface = mitfConnector
'End Property
'
''''
''''
''''
'Public Property Get DataTableSheetFormattingInterface() As IDataTableSheetFormatter
'
'    Set DataTableSheetFormattingInterface = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface
'End Property
'
'
''**---------------------------------------------
''** connoted DataTableSheetFormatter
''**---------------------------------------------
'Public Property Get TopLeftRowIndex() As Long
'
'    TopLeftRowIndex = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.TopLeftRowIndex
'End Property
'Public Property Let TopLeftRowIndex(ByVal vintTopLeftRowIndex As Long)
'
'    mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.TopLeftRowIndex = vintTopLeftRowIndex
'End Property
'Public Property Get TopLeftColumnIndex() As Long
'
'    TopLeftColumnIndex = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.TopLeftColumnIndex
'End Property
'Public Property Let TopLeftColumnIndex(ByVal vintTopLeftColumnIndex As Long)
'
'    mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.TopLeftColumnIndex = vintTopLeftColumnIndex
'End Property
'
'
'Public Property Let AllowToShowFieldTitle(ByVal vblnAllowToShowFieldTitle As Boolean)
'
'    mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.AllowToShowFieldTitle = vblnAllowToShowFieldTitle
'End Property
'Public Property Get AllowToShowFieldTitle() As Boolean
'
'    AllowToShowFieldTitle = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.AllowToShowFieldTitle
'End Property
'
'
'
'Public Property Let AllowToSetAutoFilter(ByVal vblnAllowToSetAutoFilter As Boolean)
'
'    mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.AllowToSetAutoFilter = vblnAllowToSetAutoFilter
'End Property
'Public Property Get AllowToSetAutoFilter() As Boolean
'
'    AllowToSetAutoFilter = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.AllowToSetAutoFilter
'End Property
'
''''
'''' Dictionary(Of String(column title), Double(column-width))
''''
'Public Property Set FieldTitleToColumnWidthDic(ByVal vobjFieldTitleToColumnWidthDic As Scripting.Dictionary)
'
'    Set mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.FieldTitleToColumnWidthDic = vobjFieldTitleToColumnWidthDic
'End Property
'Public Property Get FieldTitleToColumnWidthDic() As Scripting.Dictionary
'
'    Set FieldTitleToColumnWidthDic = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.FieldTitleToColumnWidthDic
'End Property
'
''''
'''' Dictionary(Of String(column title order, such as 'Col1', 'Col07'), Double(column-width))
''''
'Public Property Set FieldTitleOrderToColumnWidthDic(ByVal vobjFieldTitleOrderToColumnWidthDic As Scripting.Dictionary)
'
'    Set mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.FieldTitleOrderToColumnWidthDic = vobjFieldTitleOrderToColumnWidthDic
'End Property
'Public Property Get FieldTitleOrderToColumnWidthDic() As Scripting.Dictionary
'
'    Set FieldTitleOrderToColumnWidthDic = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.FieldTitleOrderToColumnWidthDic
'End Property
'
'
''''
'''' When it is true, then ConvertDataForColumns procedure is used.
''''
'Public Property Get AllowToConvertDataForSpecifiedColumns() As Boolean
'
'    AllowToConvertDataForSpecifiedColumns = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.AllowToConvertDataForSpecifiedColumns
'End Property
'Public Property Let AllowToConvertDataForSpecifiedColumns(ByVal vblnAllowToConvertDataForSpecifiedColumns As Boolean)
'
'    mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.AllowToConvertDataForSpecifiedColumns = vblnAllowToConvertDataForSpecifiedColumns
'End Property
''''
'''' Dictionary(Of String(column title), ColumnDataConvertType(Enum definition))
''''
'Public Property Get FieldTitlesToColumnDataConvertTypeDic() As Scripting.Dictionary
'
'    Set FieldTitlesToColumnDataConvertTypeDic = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.FieldTitlesToColumnDataConvertTypeDic
'End Property
'Public Property Set FieldTitlesToColumnDataConvertTypeDic(ByVal vobjFieldTitlesToColumnDataConvertTypeDic As Scripting.Dictionary)
'
'    Set mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.FieldTitlesToColumnDataConvertTypeDic = vobjFieldTitlesToColumnDataConvertTypeDic
'End Property
'
''''
'''' ColumnsNumberFormatLocalParam
''''
'Public Property Set ColumnsNumberFormatLocal(ByVal vobjColumnsNumberFormatLocalParam As ColumnsNumberFormatLocalParam)
'
'    Set mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.ColumnsNumberFormatLocal = vobjColumnsNumberFormatLocalParam
'End Property
'Public Property Get ColumnsNumberFormatLocal() As ColumnsNumberFormatLocalParam
'
'    Set ColumnsNumberFormatLocal = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.ColumnsNumberFormatLocal
'End Property
'
''''
'''' set format-condition for each column
''''
'Public Property Set ColumnsFormatCondition(ByVal vobjColumnsFormatConditionParam As ColumnsFormatConditionParam)
'
'    Set mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.ColumnsFormatCondition = vobjColumnsFormatConditionParam
'End Property
'Public Property Get ColumnsFormatCondition() As ColumnsFormatConditionParam
'
'    Set ColumnsFormatCondition = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.ColumnsFormatCondition
'End Property
'
''''
'''' merge cells when the same values continue
''''
'Public Property Let AllowToMergeCellsByContinuousSameValues(ByVal vblnAllowToMergeCellsByContinuousSameValues As Boolean)
'
'    mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.AllowToMergeCellsByContinuousSameValues = vblnAllowToMergeCellsByContinuousSameValues
'End Property
'Public Property Get AllowToMergeCellsByContinuousSameValues() As Boolean
'
'    AllowToMergeCellsByContinuousSameValues = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.AllowToMergeCellsByContinuousSameValues
'End Property
'Public Property Let AllowToSetVerticalTopAlignmentTopOfMergeCells(ByVal vblnAllowToSetVerticalTopAlignmentTopOfMergeCells As Boolean)
'
'    mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.AllowToSetVerticalTopAlignmentTopOfMergeCells = vblnAllowToSetVerticalTopAlignmentTopOfMergeCells
'End Property
'Public Property Get AllowToSetVerticalTopAlignmentTopOfMergeCells() As Boolean
'
'    AllowToSetVerticalTopAlignmentTopOfMergeCells = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.AllowToSetVerticalTopAlignmentTopOfMergeCells
'End Property
'
'Public Property Set FieldTitlesToMergeCells(ByVal vobjFieldTitlesToMergeCells As Collection)
'
'    Set mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.FieldTitlesToMergeCells = vobjFieldTitlesToMergeCells
'End Property
'Public Property Get FieldTitlesToMergeCells()
'
'    Set FieldTitlesToMergeCells = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.FieldTitlesToMergeCells
'End Property
'
'Public Property Let FieldTitleInteriorType(ByVal venmFieldTitleInteriorType As FieldTitleInterior)
'
'    mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.FieldTitleInteriorType = venmFieldTitleInteriorType
'End Property
'Public Property Get FieldTitleInteriorType() As FieldTitleInterior
'
'    FieldTitleInteriorType = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.FieldTitleInteriorType
'End Property
'
'Public Property Let RecordBordersType(ByVal venmRecordCellsBordersType As RecordCellsBorders)
'
'    mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.RecordBordersType = venmRecordCellsBordersType
'End Property
'Public Property Get RecordBordersType() As RecordCellsBorders
'
'    RecordBordersType = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface.RecordBordersType
'End Property
'
''///////////////////////////////////////////////
''/// Properties - connoted ADOConnectionSetting
''///////////////////////////////////////////////
'Public Property Get ConnectSetting() As ADOConnectionSetting
'
'    Set ConnectSetting = mobjConnector.ConnectSetting
'End Property
'
''''
'''' unit is second. [s]
''''
'Public Property Let CommandTimeout(ByVal vintTimeout As Long)
'
'    mobjConnector.CommandTimeout = vintTimeout
'End Property
'Public Property Get CommandTimeout() As Long
'
'    CommandTimeout = mobjConnector.CommandTimeout
'End Property
'
''///////////////////////////////////////////////
''/// Properties - CSV files connection by ACCESS OLE-DB provider
''///////////////////////////////////////////////
''''
'''' connecting CSV file path
''''
'Public Property Get ConnectingFilePath() As String
'
'    ConnectingFilePath = mobjConnector.ConnectingFilePath
'End Property
'
''''
'''' connecting directory path, which includes CSV files
''''
'Public Property Get ConnectingDirectoryPath() As String
'
'    ConnectingDirectoryPath = mobjConnector.ConnectingDirectoryPath
'End Property
'
''''
'''' when the load Excel sheet doesn't have the field header title text, this is False
''''
'Public Property Get HeaderFieldTitleExists() As Boolean
'
'    HeaderFieldTitleExists = mobjConnector.HeaderFieldTitleExists
'End Property
'
'
''///////////////////////////////////////////////
''/// Implemented operations
''///////////////////////////////////////////////
'Private Function IOutputBookPath_GetPreservedOutputBookPath() As String
'
'    IOutputBookPath_GetPreservedOutputBookPath = GetCsvAdoConnectingGeneralLogBookPath()
'End Function
'
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''**---------------------------------------------
''** preparation before SQL execution
''**---------------------------------------------
''''
'''' set input CSV file path
''''
'Public Sub SetInputCsvFileConnection(ByVal vstrCSVFilePath As String, _
'        Optional ByVal vblnHeaderFieldTitleExists As Boolean = True, _
'        Optional ByVal venmAccessOLEDBProviderType As AccessOLEDBProviderType = AccessOLEDBProviderType.AceOLEDB_12P0)
'
'    mobjConnector.SetInputCsvFileConnection vstrCSVFilePath, vblnHeaderFieldTitleExists, venmAccessOLEDBProviderType
'End Sub
'
''''
'''' set input directory path, which includes CSV files
''''
'Public Sub SetInputCsvDirectoryConnection(ByVal vstrCsvDirectoryPath As String, _
'        Optional ByVal vblnHeaderFieldTitleExists As Boolean = True, _
'        Optional ByVal venmAccessOLEDBProviderType As AccessOLEDBProviderType = AccessOLEDBProviderType.AceOLEDB_12P0)
'
'    mobjConnector.SetInputCsvDirectoryConnection vstrCsvDirectoryPath, vblnHeaderFieldTitleExists, venmAccessOLEDBProviderType
'End Sub
'
''''
'''' Close ADO connection
''''
'Public Sub CloseConnection()
'
'    mobjConnector.CloseConnection
'End Sub
'
''''
''''
''''
'Public Sub CloseCacheOutputBookOfOpenByPresetModeProcess()
'
'    mobjAdoRecordsetSheetExpander.CloseCacheOutputBookOfOpenByPresetModeProcess
'End Sub
'
''''
''''
''''
'Public Sub CloseAll()
'
'    CloseConnection
'
'    CloseCacheOutputBookOfOpenByPresetModeProcess
'End Sub
'
''''
'''' get recordset object by the Select SQL
''''
'Public Function GetRecordset(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag) As ADODB.Recordset
'
'    Set GetRecordset = mobjConnector.GetRecordset(vstrSQL, venmShowUpAdoErrorOptionFlag)
'End Function
'
'
''**---------------------------------------------
''** Output Recordset and logs to cells on sheet
''**---------------------------------------------
''''
'''' query SQL and output result to Excel sheet
''''
'Public Sub OutputToSheetFrom(ByVal vstrSQL As String, _
'        ByVal vobjOutputSheet As Excel.Worksheet, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'    mobjAdoRecordsetSheetExpander.OutputToSheetFrom vstrSQL, vobjOutputSheet, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedCSVPathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
'''' query SQL and output result to the Existed Excel sheet
''''
'Public Sub OutputInExistedBook(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputInExistedBook vstrSQL, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedCSVPathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
'Public Sub OutputInAlreadyExistedBook(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputInAlreadyExistedBook vstrSQL, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedCSVPathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
''''
''''
'Public Sub OutputInExistedBookByUnitTestMode(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputInExistedBookByUnitTestMode vstrSQL, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedCSVPathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputToSpecifiedBook(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputToSpecifiedBook vstrSQL, vstrOutputBookPath, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedCSVPathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
'Public Sub OutputToSpecifiedBookByUnitTestMode(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'    mobjAdoRecordsetSheetExpander.OutputToSpecifiedBookByUnitTestMode vstrSQL, vstrOutputBookPath, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedCSVPathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
'''' query SQL and output result to the already specified Excel sheet
''''
'Public Sub OutputToAlreadySpecifiedBook(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'    mobjAdoRecordsetSheetExpander.OutputToAlreadySpecifiedBook vstrSQL, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedCSVPathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
''''
''''
'Public Sub SetOutputExcelBookPath(ByVal vstrOutputBookPath As String, _
'        Optional ByVal venmOutputRSetExcelBookOpenModeProcessFlag As OutputRSetExcelBookOpenModeProcessFlag = OutputRSetExcelBookOpenModeProcessFlag.OpenOutputBookByUnitTestMode, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag)
'
'    mobjAdoRecordsetSheetExpander.SetOutputExcelBookPath vstrOutputBookPath, venmOutputRSetExcelBookOpenModeProcessFlag
'End Sub
'
''**---------------------------------------------
''** Excecute SQL command (UPDATE, INSERT, DELETE)
''**---------------------------------------------
''''
'''' command SQL and output result-log to Excel sheet
''''
'Public Sub OutputCommandLogToSheetFrom(ByVal vstrSQL As String, _
'        ByVal vobjOutputSqlCommandLogSheet As Excel.Worksheet, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogToSheetFrom vstrSQL, vobjOutputSqlCommandLogSheet, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedCSVPathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputCommandLogToSpecifiedBook(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogToSpecifiedBook vstrSQL, vstrOutputBookPath, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedCSVPathInsertText(), vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputCommandLogToAlreadySpecifiedBook(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogToAlreadySpecifiedBook vstrSQL, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedCSVPathInsertText(), vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputCommandLogInAlreadyExistedBook(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogInAlreadyExistedBook vstrSQL, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedCSVPathInsertText(), vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputCommandLogToSpecifiedBookByUnitTestMode(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogToSpecifiedBookByUnitTestMode vstrSQL, vstrOutputBookPath, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedCSVPathInsertText(), vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
'''' query SQL and output result to the prepared Excel sheet
''''
'Public Sub OutputCommandLogInExistedBookByUnitTestMode(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogInExistedBookByUnitTestMode vstrSQL, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedCSVPathInsertText(), vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
'Private Function mfobjGetConnectedCSVPathInsertText() As Collection
'
'    Dim objInsertTexts As Collection
'
'    Set objInsertTexts = New Collection
'
'    With Me
'        If FileExistsByVbaDir(.ConnectingFilePath) Then
'
'            objInsertTexts.Add "<File Name> " & GetFileNameFromPathByVbaDir(.ConnectingFilePath) & ", <CSV Directory> " & GetParentDirectoryPathByVbaDir(.ConnectingFilePath)
'        Else
'            objInsertTexts.Add "<CSV Directory> " & .ConnectingDirectoryPath
'        End If
'    End With
'
'    Set mfobjGetConnectedCSVPathInsertText = objInsertTexts
'End Function
'
'
'''--VBA_Code_File--<SqlUTfCsvAdoSheetExpander.bas>--
'Attribute VB_Name = "SqlUTfCsvAdoSheetExpander"
''
''   Sanity test to output SQL result into a Excel sheet for CSV files in a directory using ADO OLE DB
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on both ADO and Excel
''       Dependent on Microsoft Office Access Data Base Engine OLE DB Provider,
''       which should have been the installed when the Microsoft Office Excel software has been installed.
''       Dependent on UTfCreateDataTable.bas
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Wed, 26/Jul/2023    Kalmclaeyd Tarclanus    Redesigned
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Constants
''///////////////////////////////////////////////
'Private Const mstrTestCSVFileNamePrefix As String = "TestGenerated"
'
'
''///////////////////////////////////////////////
''/// Sanity tests
''///////////////////////////////////////////////
''**---------------------------------------------
''** CsvAdoSheetExpander test
''**---------------------------------------------
''''
'''' Test-connecting
''''
'Private Sub ConnectTestToCSVFileByADO()
'
'    Dim strSQL As String, strPath As String, blnCSVGenerated As Boolean
'
'
'    blnCSVGenerated = False
'
'    strPath = mfstrGetSimpleTestCSVFilePath
'
'    With New Scripting.FileSystemObject
'
'        If Not .FileExists(strPath) Then
'
'            msubGenerateTestingCSVFile
'
'            blnCSVGenerated = True
'        End If
'    End With
'
'
'    With New CsvAdoSheetExpander
'
'        .SetInputCsvFileConnection strPath
'
'        strSQL = "SELECT * FROM " & GetCSVTableNameLeteral(strPath)
'
'
'        With .DataTableSheetFormattingInterface
'
'            Set .FieldTitleToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("col3,13")
'
'            Set .ColumnsNumberFormatLocal = New ColumnsNumberFormatLocalParam
'
'            With .ColumnsNumberFormatLocal
'
'                .SetNumberFormatLocalAndApplyFieldTitles "ddd, yyyy/m/d", GetColFromLineDelimitedChar("col3")
'            End With
'        End With
'
'
'        .OutputInExistedBookByUnitTestMode strSQL, "Load_sample_csv"
'
'
'        If blnCSVGenerated Then
'
'            AddAutoShapeGeneralMessage .CurrentSheet, CreateObject("Scripting.FileSystemObject").GetFileName(strPath) & " is created now."
'        End If
'    End With
'End Sub
'
''**---------------------------------------------
''** CsvAdoConnector connecting by generated sample CSV
''**---------------------------------------------
''''
''''
''''
'Private Sub msubSanityTest01ToConnectCSVTestAfterCSVCreate()
'
'    GetOutputBookPathOfConnectingAdoCSVTestAfterCSVCreate BasicSampleDT, 100, False
'End Sub
'
'
'Private Sub msubSanityTest02ToConnectCSVTestAfterCSVCreate()
'
'    GetOutputBookPathOfConnectingAdoCSVTestAfterCSVCreate IncludeFictionalProductionNumberSampleDT, 10, True
'End Sub
'
'
'Private Sub msubSanityTest02ToConnectCSVTestAfterCSVSampleMasterTableOfFishType()
'
'    GetOutputBookPathOfConnectingAdoCSVTestAfterCSVCreate FishTypeSampleMasterDT, vblnIncludesSQLLogToSheet:=True
'End Sub
'
'Private Sub msubSanityTest02ToConnectCSVTestAfterCSVSampleMasterTableOfFlowerType()
'
'    GetOutputBookPathOfConnectingAdoCSVTestAfterCSVCreate FlowerTypeSampleMasterDT, vblnIncludesSQLLogToSheet:=True
'End Sub
'
'
''''
''''
''''
'Private Sub msubSanityTestToConectCSVWithJOINSQL()
'
'    Dim strInputCsvPath01 As String, strInputCsvPath02 As String
'    Dim strTableName01 As String, strTableName02 As String, strDir As String
'    Dim strSQL As String
'
'    strInputCsvPath01 = GetCSVPathAfterCreateSampleDataTable(IncludeFictionalProductionNumberSampleDT, 1000)
'
'    strInputCsvPath02 = GetCSVPathAfterCreateSampleDataTable(FishTypeSampleMasterDT)
'
'    With New Scripting.FileSystemObject
'
'        strDir = .GetParentFolderName(strInputCsvPath01)
'
'        strTableName01 = "[" & .GetFileName(strInputCsvPath01) & "]"
'
'        strTableName02 = "[" & .GetFileName(strInputCsvPath02) & "]"
'
'    End With
'
'    With New CsvAdoSheetExpander
'
'        .SetInputCsvDirectoryConnection strDir
'
'        .RecordBordersType = RecordCellsGrayAll
'
'        Set .FieldTitleToColumnWidthDic = GetTextToRealNumberDicFromLineDelimitedChar("RowNumber,12,RealNumber,12,FishType,11,FictionalProductionSerialNumber,30,FishTypeJpn,11,RandAlpha,11")
'
'
'        strSQL = "SELECT * FROM " & strTableName01
'
'        .OutputInExistedBookByUnitTestMode strSQL, "1stTable"
'
'        strSQL = "SELECT * FROM " & strTableName02
'
'        .OutputInExistedBookByUnitTestMode strSQL, "2ndTable"
'
'        'strSQL = "SELECT A.RowNumber, A.RealNumber, A.FishType FROM " & strTableName01 & " AS A"
'
'        strSQL = "SELECT A.RowNumber, A.RealNumber, A.FishType, B.FishTypeJpn, B.RandAlpha FROM " & strTableName01 & " AS A LEFT JOIN " & strTableName02 & " AS B ON A.FishType = B.FishTypeEng"
'
'
'        .OutputInExistedBookByUnitTestMode strSQL, "SQLResult"
'
'        AddAutoShapeGeneralMessage .CurrentSheet, "The SQL 'JOIN' can be also used by connecting plural CSV files in a conntected directory"
'    End With
'End Sub
'
'
''**---------------------------------------------
''** CSV directory connection tests
''**---------------------------------------------
''''
''''
''''
'Private Sub msubSanityTestToTryToConnectCSVDirectory()
'
'    Dim strSQL As String, strPath As String, blnCSVGenerated As Boolean
'    Dim objCsvADOConnectStrGenerator As ADOConStrOfAceOleDbCsv
'
'    blnCSVGenerated = False
'
'    msubDeleteTestingCSVFile    ' If the directory path exists, the ADO connection is to be succeeded even though no CSV file exist.
'
'    strPath = mfstrGetSimpleTestCSVFilePath
'
'    Set objCsvADOConnectStrGenerator = New ADOConStrOfAceOleDbCsv
'
'    objCsvADOConnectStrGenerator.SetCsvFileConnection strPath
'
'    ConnectToCsvFileByAdo objCsvADOConnectStrGenerator.GetConnectionString
'
'    ' Since no CSV file exist, the SQL will be failed.
'End Sub
'
''''
'''' Confirm that an error occurs
''''
'Private Sub msubSanityTestToTryToConnectCSVDirectoryWhichDoesNotExist()
'
'    Dim strSQL As String, strPath As String, blnCSVGenerated As Boolean
'    Dim objCsvADOConnectStrGenerator As ADOConStrOfAceOleDbCsv
'
'    blnCSVGenerated = False
'
'    strPath = mfstrGetTempCSVDirectoryPath("TemporaryCSF02", False)
'
'    Set objCsvADOConnectStrGenerator = New ADOConStrOfAceOleDbCsv
'
'    objCsvADOConnectStrGenerator.SetCsvDirectoryConnection strPath
'
'    ConnectToCsvFileByAdo objCsvADOConnectStrGenerator.GetConnectionString
'End Sub
'
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''''
''''
''''
'Public Function GetOutputBookPathOfConnectingAdoCSVTestAfterCSVCreate(ByVal venmUnitTestPreparedSampleDataTable As UnitTestPreparedSampleDataTable, Optional ByVal vintCountOfRows As Long = 100, Optional ByVal vblnIncludesSQLLogToSheet As Boolean = True) As String
'
'    Dim strInputCSVPath As String, strSQL As String, strLog As String
'    Dim strOutputBookPath As String
'
'    strInputCSVPath = GetCSVPathAfterCheckFileExistenceOrCreateWhenItDoesntExist(venmUnitTestPreparedSampleDataTable, vintCountOfRows)
'
'    With New CsvAdoSheetExpander
'
'        .SetInputCsvFileConnection strInputCSVPath
'
'        .DataTableSheetFormattingInterface.RecordBordersType = RecordCellsGrayAll
'
'        strSQL = "SELECT * FROM " & GetCSVTableNameLeteral(strInputCSVPath)
'
'        .OutputInExistedBookByUnitTestMode strSQL, "Load_sample_csv", vblnIncludesSQLLogToSheet
'
'
'        strLog = GetLogTextOfPreparedSampleDataTableBaseName(venmUnitTestPreparedSampleDataTable)
'
'
'        AddAutoShapeGeneralMessage .CurrentSheet, strLog
'
'        strOutputBookPath = .CurrentBook.FullName
'
'        .CloseAll
'    End With
'
'    GetOutputBookPathOfConnectingAdoCSVTestAfterCSVCreate = strOutputBookPath
'End Function
'
'
'
'
''''
'''' CSV directory connection test
''''
'Public Sub ConnectToCsvFileByAdo(ByVal vstrConnectionString As String)
'
'    Dim objCon As ADODB.Connection
'
'    Set objCon = New ADODB.Connection
'
'    With objCon
'
'        .ConnectionString = vstrConnectionString
'
'        On Error GoTo ErrHandler
'
'        .Open   ' If the directory path exists, the ADO connection is to be succeeded even though no CSV file exist.
'
'        .Close
'
'ErrHandler:
'        If Err.Number <> 0 Then
'
'            Debug.Print "Failed to connect directory of CSV files: " & vstrConnectionString
'
'            MsgBox "Failed to connect directory of CSV files: " & vbNewLine & vstrConnectionString, vbCritical Or vbOKOnly
'        Else
'            Debug.Print "Connected to the directory of CSV files"
'        End If
'
'        On Error GoTo 0
'    End With
'End Sub
'
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
''**---------------------------------------------
''** test CSV file generation
''**---------------------------------------------
''''
''''
''''
'Private Sub msubGenerateTestingCSVFile()
'
'    Dim strPath As String
'
'    strPath = mfstrGetSimpleTestCSVFilePath
'
'    With New Scripting.FileSystemObject
'
'        With .CreateTextFile(strPath, True)
'
'            ' field title
'            .WriteLine "co1,col2,col3"
'
'            ' data
'            .WriteLine "abc,123," & Format(DateAdd("d", -1, Now()), "yyyy/m/d") '  2021/6/28"
'
'            .WriteLine "def,456," & Format(DateAdd("d", -2, Now()), "yyyy/m/d") '  2021/6/29"
'
'            .WriteLine "ghi,789," & Format(DateAdd("d", -3, Now()), "yyyy/m/d") '  2021/6/30"
'
'            .Close
'        End With
'    End With
'End Sub
'
''''
'''' delete a generated sample CSV file
''''
'Private Sub msubDeleteTestingCSVFile()
'
'    Dim strPath As String
'
'    strPath = mfstrGetSimpleTestCSVFilePath
'
'    With New Scripting.FileSystemObject
'
'        If .FileExists(strPath) Then
'
'            .DeleteFile strPath, True
'        End If
'    End With
'End Sub
'
''''
''''
''''
'Private Function mfstrGetSimpleTestCSVFilePath()
'
'    Dim strDir As String, strFileName As String, strPath As String
'
'
'    strDir = mfstrGetTempCSVDirectoryPath
'
'    strFileName = mstrTestCSVFileNamePrefix & ".csv"
'
'    strPath = strDir & "\" & strFileName
'
'    mfstrGetSimpleTestCSVFilePath = strPath
'End Function
'
'
''''
''''
''''
'Private Function mfstrGetTempCSVDirectoryPath(Optional ByVal vstrChildDirectoryName As String = "TemporaryCSVFiles", Optional ByVal vblnAllowToCreateDirectory As Boolean = True) As String
'    Dim strDir As String
'
'    With New Scripting.FileSystemObject
'
'        strDir = GetCurrentOfficeFileObject().Path & "\" & vstrChildDirectoryName
'
'        If vblnAllowToCreateDirectory Then
'
'            ForceToCreateDirectory strDir
'        End If
'    End With
'
'    mfstrGetTempCSVDirectoryPath = strDir
'End Function
'
'
'
'
'
'
'''--VBA_Code_File--<UTfAdoConnectAccDbForXl.bas>--
'Attribute VB_Name = "UTfAdoConnectAccDbForXl"
''
''   Serving Microsoft Access db serving tests by ADO without Access application
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on both ADO and Excel
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Tue, 16/May/2023    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
'
''///////////////////////////////////////////////
''/// Sanity tests
''///////////////////////////////////////////////
''''
''''
''''
'Private Sub msubSanityTestToLoadAccDbTableToExcelSheetWithUsingQueryTable()
'
'    Dim strDbPath As String, strDbName As String, strSQL As String, strConnectionString As String
'    Dim strBookName As String
'    Dim strOutputBookPath As String, objBook As Excel.Workbook, objSheet As Excel.Worksheet
'    Dim objQueryTable As Excel.QueryTable
'
'
'    strDbName = "TestFromVba01.accdb"
'
'    strDbPath = GetPathAfterCreateSampleAccDb(strDbName)
'
'
'
'    strBookName = Replace(strDbName, ".", "_") & ".xlsx"
'
'    ' prepare a AccDb
'    strOutputBookPath = mfstrGetTemporaryBookDir() & "\" & strBookName
'
'    Set objSheet = GetWorkbookAndAnWorksheetPrepareForUnitTest(strOutputBookPath, "FromAccDb")
'
'
'    'strSQL = "select * from TestTable;"
'
'    strSQL = "select * from TestTable"
'
'    strConnectionString = "ODBC;DSN=MS Access Database;DBQ=" & strDbPath
'
'
'    Set objQueryTable = objSheet.QueryTables.Add(strConnectionString, objSheet.Range("A1"), strSQL)
'
'    With objQueryTable
'
'        .Connection
'
'        .Name = "A_Query"
'
'        '.BackgroundQuery = False  ' prevent the back-ground processing
'
'        .Refresh
'
'        '.Delete  ' delete query-table
'    End With
'End Sub
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
''''
''''
''''
'Private Function mfstrGetTemporaryBookDir() As String
'
'    Dim strDir As String
'
'    strDir = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\LoadedFromAccDb"
'
'    ForceToCreateDirectory strDir
'
'    mfstrGetTemporaryBookDir = strDir
'End Function
'
'''--VBA_Code_File--<AccDbAdoSheetExpander.cls>--
'VERSION 1.0 CLASS
'BEGIN
'  MultiUse = -1  'True
'END
'Attribute VB_Name = "AccDbAdoSheetExpander"
'Attribute VB_GlobalNameSpace = False
'Attribute VB_Creatable = False
'Attribute VB_PredeclaredId = False
'Attribute VB_Exposed = False
''
''   output SQL result into Excel.Worksheet after connecting Access database by ADO OLE DB interface
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on both ADO and Excel
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Wed, 26/Jul/2023    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Interface implementations
''///////////////////////////////////////////////
'Implements IOutputBookPath
'
''///////////////////////////////////////////////
''/// Declarations
''///////////////////////////////////////////////
'Private mobjConnector As AccDbAdoConnector
'
'Private mobjAdoRecordsetSheetExpander As AdoRSetSheetExpander
'
'
''///////////////////////////////////////////////
''/// Event Handlers
''///////////////////////////////////////////////
'Private Sub Class_Initialize()
'
'    Set mobjConnector = New AccDbAdoConnector
'
'    Set mobjAdoRecordsetSheetExpander = New AdoRSetSheetExpander
'
'    With mobjAdoRecordsetSheetExpander
'
'        Set .ADOConnectorInterface = mobjConnector
'
'        Set .OutputBookPathInterface = Me
'
'        .DataTableSheetFormattingInterface.FieldTitleInteriorType = ColumnNameInteriorOrMSAccessRDBSQL
'    End With
'End Sub
'
''''
''''
''''
'Private Sub Class_Terminate()
'
'    mobjAdoRecordsetSheetExpander.CloseCacheOutputBookOfOpenByPresetModeProcess
'
'    Debug.Print "class AccDbAdoSheetExpander terminate -> " & mobjAdoRecordsetSheetExpander Is Nothing
'
'    Me.CloseConnection
'
'    Set mobjAdoRecordsetSheetExpander = Nothing
'
'    Set mobjConnector = Nothing
'End Sub
'
'
''///////////////////////////////////////////////
''/// Properties - connoted ADO connector
''///////////////////////////////////////////////
'Public Property Get SQLExecutionResult() As SQLResult
'
'    Set SQLExecutionResult = mobjConnector.SQLExecutionResult
'End Property
'
'Public Property Get IsSqlExecutionFailed() As Boolean
'
'    IsSqlExecutionFailed = mobjConnector.IsSqlExecutionFailed
'End Property
'
'Public Property Let AllowToRecordSQLLog(ByVal vblnAllowToRecord As Boolean)
'
'    mobjConnector.AllowToRecordSQLLog = vblnAllowToRecord
'End Property
'Public Property Get AllowToRecordSQLLog() As Boolean
'
'    AllowToRecordSQLLog = mobjConnector.AllowToRecordSQLLog
'End Property
'
'
'Public Property Get CurrentSheet() As Excel.Worksheet
'
'    Set CurrentSheet = mobjAdoRecordsetSheetExpander.IExpandAdoRecordsetOnSheet_CurrentSheet
'End Property
'
'
'Public Property Get CurrentBook() As Excel.Workbook
'
'    If Not Me.CurrentSheet Is Nothing Then
'
'        Set CurrentBook = Me.CurrentSheet.Parent
'    Else
'        Set CurrentBook = Nothing
'    End If
'End Property
'
'
''///////////////////////////////////////////////
''/// Properties
''///////////////////////////////////////////////
''''
''''
''''
'Public Property Get ADOConnectorInterface() As IADOConnector
'
'    Set ADOConnectorInterface = mitfConnector
'End Property
'
''''
''''
''''
'Public Property Get DataTableSheetFormattingInterface() As IDataTableSheetFormatter
'
'    Set DataTableSheetFormattingInterface = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface
'End Property
'
'
''///////////////////////////////////////////////
''/// Properties - connoted ADOConnectionSetting
''///////////////////////////////////////////////
'Public Property Get ConnectSetting() As ADOConnectionSetting
'
'    Set ConnectSetting = mobjConnector.ConnectSetting
'End Property
'
''''
'''' unit is second. [s]
''''
'Public Property Let CommandTimeout(ByVal vintTimeout As Long)
'
'    mobjConnector.CommandTimeout = vintTimeout
'End Property
'Public Property Get CommandTimeout() As Long
'
'    CommandTimeout = mobjConnector.CommandTimeout
'End Property
'
'
''///////////////////////////////////////////////
''/// Properties - Access database connection by ACCESS OLE-DB provider
''///////////////////////////////////////////////
'Public Property Get ConnectingFilePath() As String
'
'    ConnectingFilePath = mobjConnector.ConnectingFilePath
'End Property
'
'
''///////////////////////////////////////////////
''/// Implemented operations
''///////////////////////////////////////////////
''''
''''
''''
'Private Function IOutputBookPath_GetPreservedOutputBookPath() As String
'
'    IOutputBookPath_GetPreservedOutputBookPath = GetAccDbConnectingPrearrangedLogBookPath()
'End Function
'
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''**---------------------------------------------
''** set ADO connection string all parameters directly
''**---------------------------------------------
''''
''''
''''
'Public Sub SetMsAccessConnection(ByVal vstrDbPath As String, _
'        Optional ByVal vstrOldTypeDbPassword As String = "", _
'        Optional ByVal venmAccessOLEDBProviderType As AccessOLEDBProviderType = AccessOLEDBProviderType.AceOLEDB_12P0)
'
'
'    mobjConnector.SetMsAccessConnection vstrDbPath, vstrOldTypeDbPassword, venmAccessOLEDBProviderType
'End Sub
'
''**---------------------------------------------
''** set ADO connection string parameters by a User-form interface
''**---------------------------------------------
''''
''''
''''
'''' <Argument>vstrSettingKeyName: Input</Argument>
'''' <Argument>vstrDbPath: Input</Argument>
'''' <Argument>venmAccessOLEDBProviderType: Input</Argument>
'''' <Argument>vstrOldTypeDbPassword: Input</Argument>
'''' <Return>Boolean</Return>
'Public Function IsAdoConnectionContinuedAfterAccessOleDbConnectionParametersByForm(ByVal vstrSettingKeyName As String, _
'        Optional ByVal vstrDbPath As String = "", _
'        Optional ByVal venmAccessOLEDBProviderType As AccessOLEDBProviderType = AccessOLEDBProviderType.AceOLEDB_12P0, _
'        Optional ByVal vstrOldTypeDbPassword As String = "")
'
'
'    IsAdoConnectionContinuedAfterAccessOleDbConnectionParametersByForm = mobjConnector.IsAdoConnectionContinuedAfterAccessOleDbConnectionParametersByForm(vstrSettingKeyName, vstrDbPath, venmAccessOLEDBProviderType, vstrOldTypeDbPassword)
'End Function
'
''''
'''' Close ADO connection
''''
'Public Sub CloseConnection()
'
'    mobjConnector.CloseConnection
'End Sub
'
''''
''''
''''
'Public Sub CloseCacheOutputBookOfOpenByPresetModeProcess()
'
'    mobjAdoRecordsetSheetExpander.CloseCacheOutputBookOfOpenByPresetModeProcess
'End Sub
'
''''
''''
''''
'Public Sub CloseAll()
'
'    CloseConnection
'
'    CloseCacheOutputBookOfOpenByPresetModeProcess
'End Sub
'
''**---------------------------------------------
''** Output Recordset and logs to cells on sheet
''**---------------------------------------------
''''
'''' query SQL and output result to Excel sheet
''''
'Public Sub OutputToSheetFrom(ByVal vstrSQL As String, _
'        ByVal vobjOutputSheet As Excel.Worksheet, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'    mobjAdoRecordsetSheetExpander.OutputToSheetFrom vstrSQL, vobjOutputSheet, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedAccDbPathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
'''' query SQL and output result to the Existed Excel sheet
''''
'Public Sub OutputInExistedBook(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputInExistedBook vstrSQL, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedAccDbPathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
'Public Sub OutputInAlreadyExistedBook(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputInAlreadyExistedBook vstrSQL, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedAccDbPathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
''''
''''
'Public Sub OutputInExistedBookByUnitTestMode(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputInExistedBookByUnitTestMode vstrSQL, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedAccDbPathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputToSpecifiedBook(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputToSpecifiedBook vstrSQL, vstrOutputBookPath, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedAccDbPathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
'Public Sub OutputToSpecifiedBookByUnitTestMode(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'    mobjAdoRecordsetSheetExpander.OutputToSpecifiedBookByUnitTestMode vstrSQL, vstrOutputBookPath, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedAccDbPathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
'''' query SQL and output result to the already specified Excel sheet
''''
'Public Sub OutputToAlreadySpecifiedBook(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'    mobjAdoRecordsetSheetExpander.OutputToAlreadySpecifiedBook vstrSQL, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedAccDbPathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
''''
''''
'Public Sub SetOutputExcelBookPath(ByVal vstrOutputBookPath As String, _
'        Optional ByVal venmOutputRSetExcelBookOpenModeProcessFlag As OutputRSetExcelBookOpenModeProcessFlag = OutputRSetExcelBookOpenModeProcessFlag.OpenOutputBookByUnitTestMode, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag)
'
'    mobjAdoRecordsetSheetExpander.SetOutputExcelBookPath vstrOutputBookPath, venmOutputRSetExcelBookOpenModeProcessFlag
'End Sub
'
''**---------------------------------------------
''** Excecute SQL command (UPDATE, INSERT, DELETE)
''**---------------------------------------------
''''
'''' command SQL and output result-log to Excel sheet
''''
'Public Sub OutputCommandLogToSheetFrom(ByVal vstrSQL As String, _
'        ByVal vobjOutputSqlCommandLogSheet As Excel.Worksheet, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogToSheetFrom vstrSQL, vobjOutputSqlCommandLogSheet, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedAccDbPathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputCommandLogToSpecifiedBook(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogToSpecifiedBook vstrSQL, vstrOutputBookPath, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedAccDbPathInsertText(), vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputCommandLogToAlreadySpecifiedBook(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogToAlreadySpecifiedBook vstrSQL, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedAccDbPathInsertText(), vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputCommandLogInAlreadyExistedBook(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogInAlreadyExistedBook vstrSQL, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedAccDbPathInsertText(), vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputCommandLogToSpecifiedBookByUnitTestMode(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogToSpecifiedBookByUnitTestMode vstrSQL, vstrOutputBookPath, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedAccDbPathInsertText(), vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
'''' query SQL and output result to the prepared Excel sheet
''''
'Public Sub OutputCommandLogInExistedBookByUnitTestMode(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogInExistedBookByUnitTestMode vstrSQL, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedAccDbPathInsertText(), vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
''''
''''
''''
'Private Function mfobjGetConnectedAccDbPathInsertText() As Collection
'
'    Dim objInsertTexts As Collection
'
'    Set objInsertTexts = New Collection
'
'    With mobjConnector
'
'        objInsertTexts.Add "<File Name> " & GetFileNameFromPathByVbaDir(.ConnectingFilePath) & ", <Directory> " & GetParentDirectoryPathByVbaDir(.ConnectingFilePath)
'    End With
'
'    Set mfobjGetConnectedAccDbPathInsertText = objInsertTexts
'End Function
'
'
'
'''--VBA_Code_File--<SqlUTfAccDbSheetExpander.bas>--
'Attribute VB_Name = "SqlUTfAccDbSheetExpander"
''
''   Sanity test to output SQL result into a Excel sheet for Access database using ADO OLE DB
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on ADO
''       Dependent on Microsoft Office Access Data Base Engine OLE DB Provider,
''       which should have been the installed when the Microsoft Office Excel software has been installed.
''       Dependent on SqlAdoConnectingUTfAccDb.bas
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Mon, 15/May/2023    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
'
''///////////////////////////////////////////////
''/// Control VBE windows
''///////////////////////////////////////////////
''**---------------------------------------------
''** Open relative VBE code-panes
''**---------------------------------------------
'Private Sub OpenVBECodePaneRelativeToSqlUTfAccDbSheetExpander()
'
'    ' Focusing on this procdure, please press [F5] key to start this. Therefore this scope is private.
'
'    On Error Resume Next ' If the 'ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma' isn't included in the VBA project, the following causes no syntax error.
'
'    Application.Run "ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma", "UTfAdoConnectAccDb,UTfDaoConnectAccDb"
'End Sub
'
'
''///////////////////////////////////////////////
''/// Sanity tests
''///////////////////////////////////////////////
''**---------------------------------------------
''** AccDbAdoSheetExpander
''**---------------------------------------------
''''
'''' Create an in-line table from the only SELECT sql inside
''''
'Private Sub msubSanityTestToConnectToAccDbBySqlInlineVirtualTableByAccDbAdoSheetExpander()
'
'    Dim strSQL As String
'
'    With New AccDbAdoSheetExpander
'
'        .SetMsAccessConnection PrepareAccessDbAfterCreateSampleAccDbIfItDoesntExist("TestDbOfAccDbAdoSheetExpander.accdb")
'
'        .AllowToRecordSQLLog = True
'
'        'strSQL = "SELECT 'ABC', 'DEF' UNION SELECT 'GHI', 'JKL'" ' Error occurs
'
'        'strSQL = "SELECT 1"
'
'        'strSQL = "SELECT 'ABC', 'DEF'"
'
'        strSQL = "SELECT 'ABC' AS A_Column, 'DEF' AS B_Column"
'
'        .OutputInExistedBookByUnitTestMode strSQL, "Test_ADO_VirtualTable"
'
'        .CloseCacheOutputBookOfOpenByPresetModeProcess
'    End With
'End Sub
'
'
''''
''''
''''
'Private Sub msubSanityTestToConnectToAccDbBySqlInsert()
'
'    Dim strSQL As String, strDbPath As String, objOutputBook As Excel.Workbook
'
'    strDbPath = GetAccDbPathAfterTestingToConnectToAccDbAdoConnectorWithSqlInsert()
'
'    Set objOutputBook = GetWorkbookAndPrepareForUnitTest(mfstrGetTempAccDbSqlTestOutputBookPath())
'
'
'    With New AccDbAdoSheetExpander
'
'        .SetMsAccessConnection strDbPath
'
'        .DataTableSheetFormattingInterface.RecordBordersType = RecordCellsGrayAll
'
'        strSQL = "INSERT INTO Test_Table VALUES (13, 14, 15, 'Type3');"
'
'        .OutputCommandLogToSpecifiedBook strSQL, mfstrGetTempAccDbSqlTestOutputBookPath()
'
'        strSQL = "INSERT INTO Test_Table VALUES (16, 17, 18, 'Type3');"
'
'        .OutputCommandLogToSpecifiedBook strSQL, mfstrGetTempAccDbSqlTestOutputBookPath()
'
'
'        strSQL = "SELECT * FROM Test_Table"
'
'        .OutputToSpecifiedBook strSQL, mfstrGetTempAccDbSqlTestOutputBookPath(), "SqlAllResults", AppendingSqlQueryLogsInSheetCellsOnIndependentSheet
'
'        .CloseConnection
'    End With
'
'    DeleteDummySheetWhenItExists objOutputBook
'End Sub
'
''''
'''' Test for SQL DELETE
''''
'Private Sub msubSanityTestToConnectToAccDbBySqlDelete()
'
'    Dim strSQL As String, strDbPath As String, objOutputBook As Excel.Workbook
'
'    strDbPath = GetAccDbPathAfterTestingToConnectToAccDbAdoConnectorWithSqlInsert()
'
'    Set objOutputBook = GetWorkbookAndPrepareForUnitTest(mfstrGetTempAccDbSqlTestOutputBookPath())
'
'
'    With New AccDbAdoSheetExpander
'
'        .SetMsAccessConnection strDbPath
'
'        .DataTableSheetFormattingInterface.RecordBordersType = RecordCellsGrayAll
'
'        strSQL = "DELETE FROM Test_Table WHERE ColText = 'Type2';"
'
'        .OutputCommandLogToSpecifiedBook strSQL, mfstrGetTempAccDbSqlTestOutputBookPath()
'
'        strSQL = "SELECT * FROM Test_Table"
'
'        .OutputToSpecifiedBook strSQL, mfstrGetTempAccDbSqlTestOutputBookPath(), "SqlAllResults", SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable
'
'        .CloseAll
'    End With
'
'    DeleteDummySheetWhenItExists objOutputBook
'End Sub
'
''''
''''
''''
'Private Sub msubSanityTestToConnectToAccDbBySqlUpdate()
'
'    Dim strSQL As String, strDbPath As String, objOutputBook As Excel.Workbook
'
'    strDbPath = GetAccDbPathAfterTestingToConnectToAccDbAdoConnectorWithSqlInsert()
'
'    Set objOutputBook = GetWorkbookAndPrepareForUnitTest(mfstrGetTempAccDbSqlTestOutputBookPath())
'
'
'    With New AccDbAdoSheetExpander
'
'        .SetMsAccessConnection strDbPath
'
'        .DataTableSheetFormattingInterface.RecordBordersType = RecordCellsGrayAll
'
'        strSQL = "UPDATE Test_Table SET Col1 = 41, Col2 = 42 WHERE Col3 = 12 AND ColText = 'Type2';"
'
'        .OutputCommandLogToSpecifiedBook strSQL, mfstrGetTempAccDbSqlTestOutputBookPath()
'
'        strSQL = "SELECT * FROM Test_Table"
'
'        .OutputToSpecifiedBook strSQL, mfstrGetTempAccDbSqlTestOutputBookPath(), "SqlAllResults", AppendingSqlQueryLogsInSheetCellsOnIndependentSheet
'
'        .CloseAll
'    End With
'
'    DeleteDummySheetWhenItExists objOutputBook
'End Sub
'
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
''''
''''
''''
'Private Function mfstrGetTempAccDbSqlTestOutputBookPath() As String
'
'    mfstrGetTempAccDbSqlTestOutputBookPath = mfstrGetTempAccDbSqlTestOutputBookDir() & "\SqlTestResultBookOfAccDbAdoSheetExpander.xlsx"
'End Function
'
'
''''
''''
''''
'Private Function mfstrGetTempAccDbSqlTestOutputBookDir() As String
'
'    Dim strDir As String
'
'    strDir = GetCurrentBookOutputDir() & "\SQLTests\FromAccDbConnect"
'
'    ForceToCreateDirectory strDir
'
'    mfstrGetTempAccDbSqlTestOutputBookDir = strDir
'End Function
'
'
'''--VBA_Code_File--<PgSqlOdbcSheetExpander.cls>--
'VERSION 1.0 CLASS
'BEGIN
'  MultiUse = -1  'True
'END
'Attribute VB_Name = "PgSqlOdbcSheetExpander"
'Attribute VB_GlobalNameSpace = False
'Attribute VB_Creatable = False
'Attribute VB_PredeclaredId = False
'Attribute VB_Exposed = False
''
''   output SQL result into Excel.Worksheet after connecting a PostgreSQL database by ADO ODBC interface
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on both ADO and Excel
''       Dependent on both PsqlOdbc driver at this Windows and an PostgreSQL database server in either a network somewhere or the local-host
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Wed, 26/Jul/2023    Kalmclaeyd Tarclanus    Started to redesign
''
'
'Option Explicit
'
'
''///////////////////////////////////////////////
''/// Interface implementations
''///////////////////////////////////////////////
'Implements IOutputBookPath
'
''///////////////////////////////////////////////
''/// Constants
''///////////////////////////////////////////////
'#If Win64 Then
'
'    Private Const mstrDriverName As String = "{PostgreSQL Unicode(x64)}"
'#Else
'    Private Const mstrDriverName As String = "{PostgreSQL Unicode}"
'#End If
'
'Private Const mintPostgreSQLDefaultPortNumber As Long = 5432
'
''///////////////////////////////////////////////
''/// Declarations
''///////////////////////////////////////////////
'Private mobjConnector As PgSqlOdbcConnector: Private mitfConnector As IADOConnector
'
'Private mobjAdoRecordsetSheetExpander As AdoRSetSheetExpander
'
'
''///////////////////////////////////////////////
''/// Event Handlers
''///////////////////////////////////////////////
'Private Sub Class_Initialize()
'
'    Set mobjConnector = New PgSqlOdbcConnector
'
'    Set mitfConnector = mobjConnector
'
'    Set mobjAdoRecordsetSheetExpander = New AdoRSetSheetExpander
'
'    With mobjAdoRecordsetSheetExpander
'
'        Set .ADOConnectorInterface = mobjConnector
'
'        Set .OutputBookPathInterface = Me
'
'        .DataTableSheetFormattingInterface.FieldTitleInteriorType = ColumnNameInteriorOfPostgreSQL
'    End With
'End Sub
'
''''
''''
''''
'Private Sub Class_Terminate()
'
'    mobjAdoRecordsetSheetExpander.CloseCacheOutputBookOfOpenByPresetModeProcess
'
'    Debug.Print "class AccDbAdoSheetExpander terminate -> " & mobjAdoRecordsetSheetExpander Is Nothing
'
'    Me.CloseConnection
'
'    Set mobjAdoRecordsetSheetExpander = Nothing
'
'    Set mobjConnector = Nothing
'End Sub
'
'
''///////////////////////////////////////////////
''/// Properties - connoted ADO connector
''///////////////////////////////////////////////
'Public Property Get SQLExecutionResult() As SQLResult
'
'    Set SQLExecutionResult = mobjConnector.SQLExecutionResult
'End Property
'
'Public Property Get IsSqlExecutionFailed() As Boolean
'
'    IsSqlExecutionFailed = mobjConnector.IsSqlExecutionFailed
'End Property
'
'Public Property Let AllowToRecordSQLLog(ByVal vblnAllowToRecord As Boolean)
'
'    mobjConnector.AllowToRecordSQLLog = vblnAllowToRecord
'End Property
'Public Property Get AllowToRecordSQLLog() As Boolean
'
'    AllowToRecordSQLLog = mobjConnector.AllowToRecordSQLLog
'End Property
'
'
'Public Property Get CurrentSheet() As Excel.Worksheet
'
'    Set CurrentSheet = mobjAdoRecordsetSheetExpander.IExpandAdoRecordsetOnSheet_CurrentSheet
'End Property
'
'
'Public Property Get CurrentBook() As Excel.Workbook
'
'    If Not Me.CurrentSheet Is Nothing Then
'
'        Set CurrentBook = Me.CurrentSheet.Parent
'    Else
'        Set CurrentBook = Nothing
'    End If
'End Property
'
'
''///////////////////////////////////////////////
''/// Properties
''///////////////////////////////////////////////
''''
''''
''''
'Public Property Get ADOConnectorInterface() As IADOConnector
'
'    Set ADOConnectorInterface = mitfConnector
'End Property
'
''''
''''
''''
'Public Property Get DataTableSheetFormattingInterface() As IDataTableSheetFormatter
'
'    Set DataTableSheetFormattingInterface = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface
'End Property
'
'
''///////////////////////////////////////////////
''/// Properties - connoted ADOConnectionSetting
''///////////////////////////////////////////////
'Public Property Get ConnectSetting() As ADOConnectionSetting
'
'    Set ConnectSetting = mobjConnector.ConnectSetting
'End Property
'
''''
'''' unit is second. [s]
''''
'Public Property Let CommandTimeout(ByVal vintTimeout As Long)
'
'    mobjConnector.CommandTimeout = vintTimeout
'End Property
'Public Property Get CommandTimeout() As Long
'
'    CommandTimeout = mobjConnector.CommandTimeout
'End Property
'
''///////////////////////////////////////////////
''/// Implemented operations
''///////////////////////////////////////////////
''''
''''
''''
'Private Function IOutputBookPath_GetPreservedOutputBookPath() As String
'
'    IOutputBookPath_GetPreservedOutputBookPath = GetPgSqlConnectingPrearrangedLogBookPath()
'End Function
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''**---------------------------------------------
''** set ADO connection string all parameters directly
''**---------------------------------------------
''''
'''' set ODBC connection parameters by a registered Data Source Name (DSN)
''''
'Public Sub SetODBCParametersByDSN(ByVal vstrDSN As String, ByVal vstrUID As String, ByVal vstrPWD As String)
'
'    mobjConnector.SetODBCConnectionByDSN vstrDSN, vstrUID, vstrPWD
'End Sub
'
''''
'''' set ODBC connection parameters without Data Source Name (DSN) setting
''''
'Public Sub SetODBCParametersWithoutDSN(ByVal vstrDriverName As String, ByVal vstrServerHostName As String, ByVal vstrDatabaseName As String, ByVal vstrUserid As String, ByVal vstrPassword As String, Optional ByVal vintPortNumber As Long = mintPostgreSQLDefaultPortNumber)
'
'    mobjConnector.SetODBCParametersWithoutDSN vstrDriverName, vstrServerHostName, vstrDatabaseName, vstrUserid, vstrPassword, vintPortNumber
'End Sub
'
'
''**---------------------------------------------
''** set ADO connection string parameters by a User-form interface
''**---------------------------------------------
''''
'''' Using a User-form, set DSN connection
''''
'''' <Argument>vstrSettingKeyName: Input - This is to be written in Windows-registry</Argument>
'''' <Argument>vstrDSN: Input</Argument>
'''' <Argument>vstrUID: Input</Argument>
'''' <Argument>vstrPWD: Input</Argument>
'Public Function IsAdoConnectionContinuedAfterOdbcConnectionDsnParametersByForm(ByVal vstrSettingKeyName As String, _
'        Optional ByVal vstrDSN As String = "", _
'        Optional ByVal vstrUID As String = "", _
'        Optional ByVal vstrPWD As String = "") As Boolean
'
'
'    IsAdoConnectionContinuedAfterOdbcConnectionDsnParametersByForm = mobjConnector.IsAdoConnectionContinuedAfterOdbcConnectionDsnParametersByForm(vstrSettingKeyName, vstrDSN, vstrUID, vstrPWD)
'End Function
'
''''
'''' Using a User-form, set DSN connection
''''
'''' <Argument>vstrSettingKeyName: Input - This is to be written in Windows-registry</Argument>
'''' <Argument>vstrDriverName: Input</Argument>
'''' <Argument>vstrServerHostName: Input</Argument>
'''' <Argument>vstrDatabaseName: Input</Argument>
'''' <Argument>vstrUserid: Input</Argument>
'''' <Argument>vstrPassword: Input</Argument>
'''' <Argument>vintPortNumber: Input</Argument>
'Public Function IsAdoConnectionContinuedAfterOdbcConnectionDsnlessParametersByForm(ByVal vstrSettingKeyName As String, _
'        Optional ByVal vstrDriverName As String = mstrDriverName, _
'        Optional ByVal vstrServerHostName As String = "", _
'        Optional ByVal vstrDatabaseName As String = "", _
'        Optional ByVal vstrUserid As String = "", _
'        Optional ByVal vstrPassword As String = "", _
'        Optional ByVal vintPortNumber As Long = mintPostgreSQLDefaultPortNumber) As Boolean
'
'
'    IsAdoConnectionContinuedAfterOdbcConnectionDsnlessParametersByForm = mobjConnector.IsAdoConnectionContinuedAfterOdbcConnectionDsnlessParametersByForm(vstrSettingKeyName, vstrDriverName, vstrServerHostName, vstrDatabaseName, vstrUserid, vstrPassword, vintPortNumber)
'End Function
'
''''
'''' Close ADO connection
''''
'Public Sub CloseConnection()
'
'    mobjConnector.CloseConnection
'End Sub
'
''''
''''
''''
'Public Sub CloseCacheOutputBookOfOpenByPresetModeProcess()
'
'    mobjAdoRecordsetSheetExpander.CloseCacheOutputBookOfOpenByPresetModeProcess
'End Sub
'
''''
''''
''''
'Public Sub CloseAll()
'
'    CloseConnection
'
'    CloseCacheOutputBookOfOpenByPresetModeProcess
'End Sub
'
''**---------------------------------------------
''** Output Recordset and logs to cells on sheet
''**---------------------------------------------
''''
'''' query SQL and output result to Excel sheet
''''
'Public Sub OutputToSheetFrom(ByVal vstrSQL As String, _
'        ByVal vobjOutputSheet As Excel.Worksheet, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'    mobjAdoRecordsetSheetExpander.OutputToSheetFrom vstrSQL, vobjOutputSheet, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, Nothing, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
'''' query SQL and output result to the Existed Excel sheet
''''
'Public Sub OutputInExistedBook(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputInExistedBook vstrSQL, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, Nothing, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
'Public Sub OutputInAlreadyExistedBook(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputInAlreadyExistedBook vstrSQL, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, Nothing, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
''''
''''
'Public Sub OutputInExistedBookByUnitTestMode(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputInExistedBookByUnitTestMode vstrSQL, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, Nothing, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputToSpecifiedBook(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputToSpecifiedBook vstrSQL, vstrOutputBookPath, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, Nothing, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
'Public Sub OutputToSpecifiedBookByUnitTestMode(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'    mobjAdoRecordsetSheetExpander.OutputToSpecifiedBookByUnitTestMode vstrSQL, vstrOutputBookPath, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, Nothing, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
'''' query SQL and output result to the already specified Excel sheet
''''
'Public Sub OutputToAlreadySpecifiedBook(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'    mobjAdoRecordsetSheetExpander.OutputToAlreadySpecifiedBook vstrSQL, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, Nothing, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
''''
''''
'Public Sub SetOutputExcelBookPath(ByVal vstrOutputBookPath As String, _
'        Optional ByVal venmOutputRSetExcelBookOpenModeProcessFlag As OutputRSetExcelBookOpenModeProcessFlag = OutputRSetExcelBookOpenModeProcessFlag.OpenOutputBookByUnitTestMode, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag)
'
'    mobjAdoRecordsetSheetExpander.SetOutputExcelBookPath vstrOutputBookPath, venmOutputRSetExcelBookOpenModeProcessFlag
'End Sub
'
''**---------------------------------------------
''** Excecute SQL command (UPDATE, INSERT, DELETE)
''**---------------------------------------------
''''
'''' command SQL and output result-log to Excel sheet
''''
'Public Sub OutputCommandLogToSheetFrom(ByVal vstrSQL As String, _
'        ByVal vobjOutputSqlCommandLogSheet As Excel.Worksheet, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogToSheetFrom vstrSQL, vobjOutputSqlCommandLogSheet, venmShowUpAdoErrorOptionFlag, Nothing, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputCommandLogToSpecifiedBook(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogToSpecifiedBook vstrSQL, vstrOutputBookPath, venmShowUpAdoErrorOptionFlag, Nothing, vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputCommandLogToAlreadySpecifiedBook(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogToAlreadySpecifiedBook vstrSQL, venmShowUpAdoErrorOptionFlag, Nothing, vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputCommandLogInAlreadyExistedBook(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogInAlreadyExistedBook vstrSQL, venmShowUpAdoErrorOptionFlag, Nothing, vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputCommandLogToSpecifiedBookByUnitTestMode(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogToSpecifiedBookByUnitTestMode vstrSQL, vstrOutputBookPath, venmShowUpAdoErrorOptionFlag, Nothing, vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
'''' query SQL and output result to the prepared Excel sheet
''''
'Public Sub OutputCommandLogInExistedBookByUnitTestMode(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogInExistedBookByUnitTestMode vstrSQL, venmShowUpAdoErrorOptionFlag, Nothing, vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
'
'
'
'
'''--VBA_Code_File--<SqlUTfPgSqlOdbcSheetExpander.bas>--
'Attribute VB_Name = "SqlUTfPgSqlOdbcSheetExpander"
''
''   Sanity test to output SQL results into Excel sheets for a PostgreSQL database using ADO ODBC
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on both ADO and Excel
''       Dependent on both PsqlOdbc driver at this Windows and an PostgreSQL database server in either a network somewhere or the local-host
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Wed, 26/Jul/2023    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Constants
''///////////////////////////////////////////////
'Private Const mstrConStrInfoSettingKeyNameOfDSN As String = "TestPgSqlDSNKey"
'
'Private Const mstrConStrInfoSettingKeyNameOfDSNless As String = "TestPgSqlDSNlessKey"
'
'
'
'#If Win64 Then
'
'    Private Const mstrDriverName As String = "{PostgreSQL Unicode(x64)}"
'#Else
'    Private Const mstrDriverName As String = "{PostgreSQL Unicode}"
'#End If
'
'Private Const mintPostgreSQLDefaultPortNumber As Long = 5432
'
''///////////////////////////////////////////////
''/// Control VBE windows
''///////////////////////////////////////////////
''**---------------------------------------------
''** Open relative VBE code-panes
''**---------------------------------------------
'Private Sub OpenVBECodePaneRelativeToSqlUTfPgSqlOdbcSheetExpander()
'
'    ' Focusing on this procdure, please press [F5] key to start this. Therefore this scope is private.
'
'    On Error Resume Next ' If the 'ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma' isn't included in the VBA project, the following causes no syntax error.
'
'    Application.Run "ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma", "SqlUTfPgSqlOdbcSheetExpander,SqlAdoConnectOdbcUTfPgSql,SqlAdoConnectOdbcPTfPgSql,PgSqlOdbcConnector,PgSqlOdbcSheetExpander,PgSqlCommonPath,PgSqlDSNlessTest"
'End Sub
'
'
''///////////////////////////////////////////////
''/// Sanity tests
''///////////////////////////////////////////////
''**---------------------------------------------
''** PgSqlOdbcSheetExpander
''**---------------------------------------------
''''
'''' Create an in-line table from the only SELECT sql inside
''''
'Private Sub msubSanityTestToConnectByVirtualTablePgSqlOdbcSheetExpander()
'
'    Dim strSQL As String
'
'    With New PgSqlOdbcSheetExpander
'
'        ' for default connection setting soon after the PostgreSQL typcal configuration is installed in this local computer.
'
'        .SetODBCParametersWithoutDSN mstrDriverName, "LocalHost", "postgres", "postgres", "postgres"
'
'        .AllowToRecordSQLLog = True
'
'        strSQL = "select 'ABC' as A_Column, 'DEF' as B_Column"
'
'        .OutputInExistedBookByUnitTestMode strSQL, "Test_ADO_VirtualTable"
'
'        .CloseAll
'    End With
'End Sub
'
'
''///////////////////////////////////////////////
''/// Sanity tests
''///////////////////////////////////////////////
''**---------------------------------------------
''** Use IsAdoConnectionContinuedAfterOdbcConnectionDsnlessParametersByForm interface for ADO connecting
''**---------------------------------------------
''''
'''' get PostgreSQL version by SQL
''''
'Private Sub msubSanityTestToOutputSqlVersionOnSheetAfterSqlVersionOfPostgreSQL()
'
'    Dim strSQL As String
'
'    With New PgSqlOdbcSheetExpander
'
'        ' for default connection setting soon after the PostgreSQL typcal configuration is installed in this local computer.
'
'        If .IsAdoConnectionContinuedAfterOdbcConnectionDsnlessParametersByForm(mstrConStrInfoSettingKeyNameOfDSNless, , "LocalHost", "postgres", "postgres") Then
'
'            .AllowToRecordSQLLog = True
'
'            strSQL = "select * from version()"
'
'            .OutputInExistedBookByUnitTestMode strSQL, "PgSQL_version"
'
'            .CloseCacheOutputBookOfOpenByPresetModeProcess
'        Else
'            Debug.Print "The ADO connection information inputs have canceled."
'        End If
'    End With
'End Sub
'
''**---------------------------------------------
''** Use presetting object for ADO connecting
''**---------------------------------------------
''''
'''' SQL insert test with output Excel sheet log
''''
'Private Sub msubSanityTestToOutputSqlResultOnSheetAfterPostgreSQLInsertExecution()
'
'    Dim strSQL As String, objAdoSheetExpander As PgSqlOdbcSheetExpander
'
'    ' You must implement 'GetCurrentThisSytemTestingPgSqlOdbcSheetExpander' function before you execute the following.
'
'    Set objAdoSheetExpander = mfobjGetPgSqlOdbcSheetExpanderAfterPrepareTestTableOfPostgreSQLTestingDataBaseForSqlTests()
'
'    If Not objAdoSheetExpander Is Nothing Then
'
'        With objAdoSheetExpander
'
'            strSQL = "insert into Test_Table values (13, 15, 16, 'Type3')"
'
'            .OutputCommandLogToSpecifiedBook strSQL, GetPgSqlConnectingPrearrangedLogBookPath()
'
'            .ADOConnectorInterface.CommitTransaction
'
'            strSQL = "select * from Test_Table"
'
'            .OutputToSpecifiedBook strSQL, GetPgSqlConnectingPrearrangedLogBookPath(), "SqlResults", SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable
'
'            .CloseAll
'        End With
'    Else
'        Debug.Print "Attention!!!: the 'GetCurrentThisSytemTestingPgSqlOdbcConnector' function hasn't been implemented yet."
'    End If
'End Sub
'
''''
'''' SQL delete test with output Excel sheet log
''''
'Private Sub msubSanityTestToOutputSqlResultOnSheetAfterPostgreSQLDeleteExecution()
'
'    Dim strSQL As String, objAdoSheetExpander As PgSqlOdbcSheetExpander, strOutputBookPath As String, strDir As String
'
'    ' You must implement 'GetCurrentThisSytemTestingPgSqlOdbcSheetExpander' function before you execute the following.
'
'    strDir = GetCurrentBookOutputDir() & "\SQLTests\FromPgSqlConnect"
'
'    ForceToCreateDirectory strDir
'
'    strOutputBookPath = strDir & "\AdoConnectToPgSqlTest01.xlsx"
'
'    Set objAdoSheetExpander = mfobjGetPgSqlOdbcSheetExpanderAfterPrepareTestTableOfPostgreSQLTestingDataBaseForSqlTests()
'
'    If Not objAdoSheetExpander Is Nothing Then
'
'        With objAdoSheetExpander
'
'            strSQL = "delete from Test_Table where ColText = 'Type2'"
'
'            .OutputCommandLogToSpecifiedBookByUnitTestMode strSQL, strOutputBookPath
'
'            .ADOConnectorInterface.CommitTransaction
'
'            strSQL = "select * from Test_Table"
'
'            .OutputToSpecifiedBookByUnitTestMode strSQL, strOutputBookPath, "SqlResults", AppendingSqlQueryLogsInSheetCellsOnIndependentSheet
'
'            .CloseAll
'        End With
'    Else
'        Debug.Print "Attention!!!: the 'GetCurrentThisSytemTestingPgSqlOdbcConnector' function hasn't been implemented yet."
'    End If
'End Sub
'
''''
'''' SQL update test with output Excel sheet log
''''
'Private Sub msubSanityTestToOutputSqlResultOnSheetAfterPostgreSQLUpdateExecution()
'
'    Dim strSQL As String, objAdoSheetExpander As PgSqlOdbcSheetExpander
'
'    ' You must implement 'GetCurrentThisSytemTestingPgSqlOdbcSheetExpander' function before you execute the following.
'
'    Set objAdoSheetExpander = mfobjGetPgSqlOdbcSheetExpanderAfterPrepareTestTableOfPostgreSQLTestingDataBaseForSqlTests()
'
'    If Not objAdoSheetExpander Is Nothing Then
'
'        With objAdoSheetExpander
'
'            strSQL = "update Test_Table set Col1 = 51, Col2 = 52 where Col3 = 9 AND ColText = 'Type2'"
'
'
'            .OutputCommandLogInExistedBookByUnitTestMode strSQL
'
'            .ADOConnectorInterface.CommitTransaction
'
'            strSQL = "select * from Test_Table"
'
'            .OutputInExistedBookByUnitTestMode strSQL, "SqlResults", AppendingSqlQueryLogsInSheetCellsOnIndependentSheet
'
'            .CloseAll
'        End With
'    Else
'        Debug.Print "Attention!!!: the 'GetCurrentThisSytemTestingPgSqlOdbcConnector' function hasn't been implemented yet."
'    End If
'End Sub
'
''''
'''' SQL insert test with output Excel sheet log
''''
'Private Sub msubSanityTestToOutputSqlResultOnSheetAfterPostgreSQLPluralInsertExecution()
'
'    Dim strSQL As String, objAdoSheetExpander As PgSqlOdbcSheetExpander
'
'    Dim i As Long
'    Dim int01 As Long, int02 As Long, int03 As Long, intType As Long
'
'
'    ' You must implement 'GetCurrentThisSytemTestingPgSqlOdbcSheetExpander' function before you execute the following.
'
'    Set objAdoSheetExpander = mfobjGetPgSqlOdbcSheetExpanderAfterPrepareTestTableOfPostgreSQLTestingDataBaseForSqlTests()
'
'    If Not objAdoSheetExpander Is Nothing Then
'
'        With objAdoSheetExpander
'
'            For i = 1 To 15
'
'                ' sample values
'
'                int01 = 3 * (i + 4)
'
'                int02 = int01 + 1
'
'                int03 = int02 + 1
'
'                intType = i + 5
'
'                strSQL = "insert into Test_Table values (" & CStr(int01) & ", " & CStr(int02) & ", " & CStr(int03) & ", 'Type" & CStr(intType) & "')"
'
'                .OutputCommandLogInExistedBookByUnitTestMode strSQL
'            Next
'
'            .ADOConnectorInterface.CommitTransaction
'
'            strSQL = "select * from Test_Table"
'
'            .OutputInExistedBookByUnitTestMode strSQL, "SqlResults", SqlQueryLogsByAutoShapesOnSheetWithOutputtingRecordsetDataTable
'
'            .CloseAll
'        End With
'    Else
'        Debug.Print "Attention!!!: the 'GetCurrentThisSytemTestingPgSqlOdbcConnector' function hasn't been implemented yet."
'    End If
'End Sub
'
'
''''
'''' SQL insert test with output Excel sheet log
''''
'Private Sub msubSanityTestToOutputSqlResultOnSheetAfterPostgreSQLPluralInsertExecutionWithOtherLoggingOption()
'
'    Dim strSQL As String, objAdoSheetExpander As PgSqlOdbcSheetExpander
'
'    Dim i As Long
'    Dim int01 As Long, int02 As Long, int03 As Long, intType As Long
'
'
'    ' You must implement 'GetCurrentThisSytemTestingPgSqlOdbcSheetExpander' function before you execute the following.
'
'    Set objAdoSheetExpander = mfobjGetPgSqlOdbcSheetExpanderAfterPrepareTestTableOfPostgreSQLTestingDataBaseForSqlTests()
'
'    If Not objAdoSheetExpander Is Nothing Then
'
'        With objAdoSheetExpander
'
'            For i = 1 To 15
'
'                ' sample values
'
'                int01 = 3 * (i + 4)
'
'                int02 = int01 + 1
'
'                int03 = int02 + 1
'
'                intType = i + 5
'
'                strSQL = "insert into Test_Table values (" & CStr(int01) & ", " & CStr(int02) & ", " & CStr(int03) & ", 'Type" & CStr(intType) & "')"
'
'                .OutputCommandLogInExistedBookByUnitTestMode strSQL, NoControlToShowUpAdoSqlErrorFlag, "TestingSqlCommandLog"
'            Next
'
'            .ADOConnectorInterface.CommitTransaction
'
'            strSQL = "select * from Test_Table"
'
'            .OutputInExistedBookByUnitTestMode strSQL, "SqlResults", SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable
'
'            .CloseCacheOutputBookOfOpenByPresetModeProcess
'        End With
'    Else
'        Debug.Print "Attention!!!: the 'GetCurrentThisSytemTestingPgSqlOdbcConnector' function hasn't been implemented yet."
'    End If
'End Sub
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
''''
''''
''''
'Private Function mfobjGetPgSqlOdbcSheetExpanderAfterPrepareTestTableOfPostgreSQLTestingDataBaseForSqlTests() As PgSqlOdbcSheetExpander
'
'    Dim strSQL As String, objAdoSheetExpander As PgSqlOdbcSheetExpander
'
'    ' You must implement 'GetCurrentThisSytemTestingPgSqlOdbcSheetExpander' function before you execute the following.
'
'    Set objAdoSheetExpander = mfobjGetPgSqlOdbcSheetExpanderWhichIncludesConnectionInfo()
'
'    If Not objAdoSheetExpander Is Nothing Then
'
'        PrepareTestTableOfPostgreSQLTestingDataBaseForSqlTests objAdoSheetExpander.ADOConnectorInterface
'
'        objAdoSheetExpander.DataTableSheetFormattingInterface.RecordBordersType = RecordCellsGrayAll
'    Else
'        Debug.Print "Attention!!!: the 'GetCurrentThisSytemTestingPgSqlOdbcSheetExpander' function hasn't been implemented yet."
'    End If
'
'    Set mfobjGetPgSqlOdbcSheetExpanderAfterPrepareTestTableOfPostgreSQLTestingDataBaseForSqlTests = objAdoSheetExpander
'End Function
'
'
''''
''''
''''
'Private Function mfobjGetPgSqlOdbcSheetExpanderWhichIncludesConnectionInfo() As PgSqlOdbcSheetExpander
'
'    Dim objPgSqlOdbcSheetExpander As PgSqlOdbcSheetExpander
'
'    On Error Resume Next
'
'    Set objPgSqlOdbcSheetExpander = Application.Run("GetCurrentThisSytemTestingPgSqlOdbcSheetExpander")
'
'    On Error GoTo 0
'
'    Set mfobjGetPgSqlOdbcSheetExpanderWhichIncludesConnectionInfo = objPgSqlOdbcSheetExpander
'End Function
'
'''--VBA_Code_File--<OracleOdbcSheetExpander.cls>--
'VERSION 1.0 CLASS
'BEGIN
'  MultiUse = -1  'True
'END
'Attribute VB_Name = "OracleOdbcSheetExpander"
'Attribute VB_GlobalNameSpace = False
'Attribute VB_Creatable = False
'Attribute VB_PredeclaredId = False
'Attribute VB_Exposed = False
''
''   output SQL results into Excel worksheets after connecting a Oracle database by ADO ODBC interface
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on both ADO and Excel
''       Dependent on both Oracle ODBC driver at this Windows and an Oracle database server in a network somewhere
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   Modification History:
''       Wed, 26/Jul/2023    Kalmclaeyd Tarclanus    Initial creation
''
'
'Option Explicit
'
'
''///////////////////////////////////////////////
''/// Interface implementations
''///////////////////////////////////////////////
'Implements IOutputBookPath
'
''///////////////////////////////////////////////
''/// Constants
''///////////////////////////////////////////////
'#If Win64 Then
'
'    Private Const mstrDriverName As String = "{Oracle in OraClient11g_home1}"
'#Else
'    Private Const mstrDriverName As String = "{Oracle in OraClient10g_home1}"
'#End If
'
'Private Const mintOracleDefaultPortNumber As Long = 1521
'
''///////////////////////////////////////////////
''/// Declarations
''///////////////////////////////////////////////
'Private mobjConnector As OracleOdbcConnector
'
'Private mobjAdoRecordsetSheetExpander As AdoRSetSheetExpander
'
'
''///////////////////////////////////////////////
''/// Event Handlers
''///////////////////////////////////////////////
'Private Sub Class_Initialize()
'
'    Set mobjConnector = New OracleOdbcConnector
'
'    Set mobjAdoRecordsetSheetExpander = New AdoRSetSheetExpander
'
'    With mobjAdoRecordsetSheetExpander
'
'        Set .ADOConnectorInterface = mobjConnector
'
'        Set .OutputBookPathInterface = Me
'
'        .DataTableSheetFormattingInterface.FieldTitleInteriorType = ColumnNameInteriorOfDefaultTypeSQL
'    End With
'End Sub
'
''''
''''
''''
'Private Sub Class_Terminate()
'
'    mobjAdoRecordsetSheetExpander.CloseCacheOutputBookOfOpenByPresetModeProcess
'
'    Debug.Print "class AccDbAdoSheetExpander terminate -> " & mobjAdoRecordsetSheetExpander Is Nothing
'
'    Me.CloseConnection
'
'    Set mobjAdoRecordsetSheetExpander = Nothing
'
'    Set mobjConnector = Nothing
'End Sub
'
'
''///////////////////////////////////////////////
''/// Properties - connoted ADO connector
''///////////////////////////////////////////////
'Public Property Get SQLExecutionResult() As SQLResult
'
'    Set SQLExecutionResult = mobjConnector.SQLExecutionResult
'End Property
'
'Public Property Get IsSqlExecutionFailed() As Boolean
'
'    IsSqlExecutionFailed = mobjConnector.IsSqlExecutionFailed
'End Property
'
'Public Property Let AllowToRecordSQLLog(ByVal vblnAllowToRecord As Boolean)
'
'    mobjConnector.AllowToRecordSQLLog = vblnAllowToRecord
'End Property
'Public Property Get AllowToRecordSQLLog() As Boolean
'
'    AllowToRecordSQLLog = mobjConnector.AllowToRecordSQLLog
'End Property
'
'
'Public Property Get CurrentSheet() As Excel.Worksheet
'
'    Set CurrentSheet = mobjAdoRecordsetSheetExpander.IExpandAdoRecordsetOnSheet_CurrentSheet
'End Property
'
'
'Public Property Get CurrentBook() As Excel.Workbook
'
'    If Not Me.CurrentSheet Is Nothing Then
'
'        Set CurrentBook = Me.CurrentSheet.Parent
'    Else
'        Set CurrentBook = Nothing
'    End If
'End Property
'
'
''///////////////////////////////////////////////
''/// Properties
''///////////////////////////////////////////////
''''
''''
''''
'Public Property Get DataTableSheetFormattingInterface() As IDataTableSheetFormatter
'
'    Set DataTableSheetFormattingInterface = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface
'End Property
'
'
''///////////////////////////////////////////////
''/// Properties - connoted ADOConnectionSetting
''///////////////////////////////////////////////
'Public Property Get ConnectSetting() As ADOConnectionSetting
'
'    Set ConnectSetting = mobjConnector.ConnectSetting
'End Property
'
''''
'''' unit is second. [s]
''''
'Public Property Let CommandTimeout(ByVal vintTimeout As Long)
'
'    mobjConnector.CommandTimeout = vintTimeout
'End Property
'Public Property Get CommandTimeout() As Long
'
'    CommandTimeout = mobjConnector.CommandTimeout
'End Property
'
''///////////////////////////////////////////////
''/// Implemented operations
''///////////////////////////////////////////////
''''
''''
''''
'Private Function IOutputBookPath_GetPreservedOutputBookPath() As String
'
'    IOutputBookPath_GetPreservedOutputBookPath = GetOracleConnectingPrearrangedLogBookPath()
'End Function
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''**---------------------------------------------
''** set ADO connection string all parameters directly
''**---------------------------------------------
''''
'''' set ODBC connection parameters by a registered Data Source Name (DSN)
''''
'Public Sub SetODBCParametersByDSN(ByVal vstrDSN As String, ByVal vstrUID As String, ByVal vstrPWD As String)
'
'    mobjConnector.SetODBCParametersByDSN vstrDSN, vstrUID, vstrPWD
'End Sub
'
''''
'''' set ODBC connection parameters without Data Source Name (DSN) setting
''''
'Public Sub SetODBCParametersWithoutDSN(ByVal vstrDriverName As String, ByVal vstrServerHostName As String, ByVal vstrNetworkServiceName As String, ByVal vstrUserid As String, ByVal vstrPassword As String, Optional ByVal vintPortNumber As Long = mintOracleDefaultPortNumber)
'
'    mobjConnector.SetODBCParametersWithoutDSN vstrDriverName, vstrServerHostName, vstrNetworkServiceName, vstrUserid, vstrPassword, vintPortNumber
'End Sub
'
'
''**---------------------------------------------
''** set ADO connection string parameters by a User-form interface
''**---------------------------------------------
''''
'''' Using a User-form, set DSN connection
''''
'''' <Argument>vstrSettingKeyName: Input - This is to be written in Windows-registry</Argument>
'''' <Argument>vstrDSN: Input</Argument>
'''' <Argument>vstrUID: Input</Argument>
'''' <Argument>vstrPWD: Input</Argument>
'Public Function IsAdoConnectionContinuedAfterOdbcConnectionDsnParametersByForm(ByVal vstrSettingKeyName As String, _
'        Optional ByVal vstrDSN As String = "", _
'        Optional ByVal vstrUID As String = "", _
'        Optional ByVal vstrPWD As String = "") As Boolean
'
'
'    IsAdoConnectionContinuedAfterOdbcConnectionDsnParametersByForm = mobjConnector.IsAdoConnectionContinuedAfterOdbcConnectionDsnParametersByForm(vstrSettingKeyName, vstrDSN, vstrUID, vstrPWD)
'End Function
'
''''
'''' Using a User-form, set DSN connection
''''
'''' <Argument>vstrSettingKeyName: Input - This is to be written in Windows-registry</Argument>
'''' <Argument>vstrDriverName: Input</Argument>
'''' <Argument>vstrServerHostName: Input</Argument>
'''' <Argument>vstrNetworkServiceName: Input</Argument>
'''' <Argument>vstrUserid: Input</Argument>
'''' <Argument>vstrPassword: Input</Argument>
'''' <Argument>vintPortNumber: Input</Argument>
'Public Function IsAdoConnectionContinuedAfterOdbcConnectionDsnlessParametersByForm(ByVal vstrSettingKeyName As String, _
'        Optional ByVal vstrDriverName As String = mstrDriverName, _
'        Optional ByVal vstrServerHostName As String = "", _
'        Optional ByVal vstrNetworkServiceName As String = "", _
'        Optional ByVal vstrUserid As String = "", _
'        Optional ByVal vstrPassword As String = "", _
'        Optional ByVal vintPortNumber As Long = mintPostgreSQLDefaultPortNumber) As Boolean
'
'
'    IsAdoConnectionContinuedAfterOdbcConnectionDsnlessParametersByForm = mobjConnector.IsAdoConnectionContinuedAfterOdbcConnectionDsnlessParametersByForm(vstrSettingKeyName, vstrDriverName, vstrServerHostName, vstrNetworkServiceName, vstrUserid, vstrPassword, vintPortNumber)
'End Function
'
''''
'''' Close ADO connection
''''
'Public Sub CloseConnection()
'
'    mobjConnector.CloseConnection
'End Sub
'
'
''''
''''
''''
'Public Sub CloseCacheOutputBookOfOpenByPresetModeProcess()
'
'    mobjAdoRecordsetSheetExpander.CloseCacheOutputBookOfOpenByPresetModeProcess
'End Sub
'
''''
''''
''''
'Public Sub CloseAll()
'
'    CloseConnection
'
'    CloseCacheOutputBookOfOpenByPresetModeProcess
'End Sub
'
''**---------------------------------------------
''** Output Recordset and logs to cells on sheet
''**---------------------------------------------
''''
'''' query SQL and output result to Excel sheet
''''
'Public Function OutputToSheetFrom(ByVal vstrSQL As String, _
'        ByVal vobjOutputSheet As Excel.Worksheet, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable) As ADODB.Recordset
'
'    Set OutputToSheetFrom = mobjAdoRecordsetSheetExpander.OutputToSheetFrom(vstrSQL, vobjOutputSheet, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType)
'End Function
'
'
''''
'''' query SQL and output result to the Existed Excel sheet
''''
'Public Function OutputInExistedBook(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable) As ADODB.Recordset
'
'
'    Set OutputInExistedBook = mobjAdoRecordsetSheetExpander.OutputInExistedBook(vstrSQL, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType)
'End Function
'
'
'Public Function OutputInAlreadyExistedBook(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable) As ADODB.Recordset
'
'
'    Set OutputInAlreadyExistedBook = mobjAdoRecordsetSheetExpander.OutputInAlreadyExistedBook(vstrSQL, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType)
'End Function
'
''''
''''
''''
'Public Function OutputInExistedBookByUnitTestMode(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable) As ADODB.Recordset
'
'
'    Set OutputInExistedBookByUnitTestMode = mobjAdoRecordsetSheetExpander.OutputInExistedBookByUnitTestMode(vstrSQL, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType)
'End Function
'
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Function OutputToSpecifiedBook(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable) As ADODB.Recordset
'
'
'    Set OutputToSpecifiedBook = mobjAdoRecordsetSheetExpander.OutputToSpecifiedBook(vstrSQL, vstrOutputBookPath, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType)
'End Function
'
'
'Public Function OutputToSpecifiedBookByUnitTestMode(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable) As ADODB.Recordset
'
'    Set OutputToSpecifiedBookByUnitTestMode = mobjAdoRecordsetSheetExpander.OutputToSpecifiedBookByUnitTestMode(vstrSQL, vstrOutputBookPath, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType)
'End Function
'
'
''''
'''' query SQL and output result to the already specified Excel sheet
''''
'Public Function OutputToAlreadySpecifiedBook(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable) As ADODB.Recordset
'
'    Set OutputToAlreadySpecifiedBook = mobjAdoRecordsetSheetExpander.OutputToAlreadySpecifiedBook(vstrSQL, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType)
'End Function
'
'
''''
''''
''''
'Public Sub SetOutputExcelBookPath(ByVal vstrOutputBookPath As String, _
'        Optional ByVal venmOutputRSetExcelBookOpenModeProcessFlag As OutputRSetExcelBookOpenModeProcessFlag = OutputRSetExcelBookOpenModeProcessFlag.OpenOutputBookByUnitTestMode)
'
'    mobjAdoRecordsetSheetExpander.SetOutputExcelBookPath vstrOutputBookPath, venmOutputRSetExcelBookOpenModeProcessFlag
'End Sub
'
''**---------------------------------------------
''** Excecute SQL command (UPDATE, INSERT, DELETE)
''**---------------------------------------------
''''
'''' command SQL and output result-log to Excel sheet
''''
'Public Sub OutputCommandLogToSheetFrom(ByVal vstrSQL As String, _
'        ByVal vobjOutputSheet As Excel.Worksheet, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable)
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogToSheetFrom vstrSQL, vobjOutputSheet, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType
'End Sub
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputCommandLogToSpecifiedBook(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogToSpecifiedBook vstrSQL, GetNewWorksheetAfterAllExistedSheets(vstrOutputBookPath), vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType
'End Sub
'
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputCommandLogToAlreadySpecifiedBook(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogToAlreadySpecifiedBook vstrSQL, vstrOutputBookPath, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType
'End Sub
'
'
'
'
'
'''--VBA_Code_File--<SqLiteAdoSheetExpander.cls>--
'VERSION 1.0 CLASS
'BEGIN
'  MultiUse = -1  'True
'END
'Attribute VB_Name = "SqLiteAdoSheetExpander"
'Attribute VB_GlobalNameSpace = False
'Attribute VB_Creatable = False
'Attribute VB_PredeclaredId = False
'Attribute VB_Exposed = False
''
''   output SQL result into Excel.Worksheet after connecting a SQLite database file by ADO ODBC interface
''   Probably, this capsuled class design will not be needed for almost VBA developers, because it is too complicated.
''   However the VBA programming interfaces will be united.
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on both ADO and Excel
''       Dependent on an installed SQLite ODBC driver
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Wed, 26/Jul/2023    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
'
''///////////////////////////////////////////////
''/// Interface implementations
''///////////////////////////////////////////////
'Implements IOutputBookPath
'
''///////////////////////////////////////////////
''/// Declarations
''///////////////////////////////////////////////
'Private mobjConnector As SqLiteAdoConnector: Private mitfConnector As IADOConnector
'
'Private mobjAdoRecordsetSheetExpander As AdoRSetSheetExpander
'
'
''///////////////////////////////////////////////
''/// Event Handlers
''///////////////////////////////////////////////
'Private Sub Class_Initialize()
'
'    Set mobjConnector = New SqLiteAdoConnector
'
'    Set mitfConnector = mobjConnector
'
'    Set mobjAdoRecordsetSheetExpander = New AdoRSetSheetExpander
'
'    With mobjAdoRecordsetSheetExpander
'
'        Set .ADOConnectorInterface = mobjConnector
'
'        Set .OutputBookPathInterface = Me
'
'        .DataTableSheetFormattingInterface.FieldTitleInteriorType = ColumnNameInteriorOfDefaultTypeSQLByDarkBrownFontLightSilverBgMeiryo
'    End With
'End Sub
'
''''
''''
''''
'Private Sub Class_Terminate()
'
'    mobjAdoRecordsetSheetExpander.CloseCacheOutputBookOfOpenByPresetModeProcess
'
'    Debug.Print "class AccDbAdoSheetExpander terminate -> " & mobjAdoRecordsetSheetExpander Is Nothing
'
'    Me.CloseConnection
'
'    Set mobjAdoRecordsetSheetExpander = Nothing
'
'    Set mobjConnector = Nothing
'End Sub
'
'
''///////////////////////////////////////////////
''/// Properties - connoted ADO connector
''///////////////////////////////////////////////
'Public Property Get SQLExecutionResult() As SQLResult
'
'    Set SQLExecutionResult = mobjConnector.SQLExecutionResult
'End Property
'
'Public Property Get IsSqlExecutionFailed() As Boolean
'
'    IsSqlExecutionFailed = mobjConnector.IsSqlExecutionFailed
'End Property
'
'Public Property Let AllowToRecordSQLLog(ByVal vblnAllowToRecord As Boolean)
'
'    mobjConnector.AllowToRecordSQLLog = vblnAllowToRecord
'End Property
'Public Property Get AllowToRecordSQLLog() As Boolean
'
'    AllowToRecordSQLLog = mobjConnector.AllowToRecordSQLLog
'End Property
'
'
'Public Property Get CurrentSheet() As Excel.Worksheet
'
'    Set CurrentSheet = mobjAdoRecordsetSheetExpander.IExpandAdoRecordsetOnSheet_CurrentSheet
'End Property
'
'
'Public Property Get CurrentBook() As Excel.Workbook
'
'    If Not Me.CurrentSheet Is Nothing Then
'
'        Set CurrentBook = Me.CurrentSheet.Parent
'    Else
'        Set CurrentBook = Nothing
'    End If
'End Property
'
'
''///////////////////////////////////////////////
''/// Properties
''///////////////////////////////////////////////
''''
''''
''''
'Public Property Get ADOConnectorInterface() As IADOConnector
'
'    Set ADOConnectorInterface = mitfConnector
'End Property
'
''''
''''
''''
'Public Property Get DataTableSheetFormattingInterface() As IDataTableSheetFormatter
'
'    Set DataTableSheetFormattingInterface = mobjAdoRecordsetSheetExpander.DataTableSheetFormattingInterface
'End Property
'
'
''///////////////////////////////////////////////
''/// Properties - connoted ADOConnectionSetting
''///////////////////////////////////////////////
'Public Property Get ConnectSetting() As ADOConnectionSetting
'
'    Set ConnectSetting = mobjConnector.ConnectSetting
'End Property
'
''''
'''' unit is second. [s]
''''
'Public Property Let CommandTimeout(ByVal vintTimeout As Long)
'
'    mobjConnector.CommandTimeout = vintTimeout
'End Property
'Public Property Get CommandTimeout() As Long
'
'    CommandTimeout = mobjConnector.CommandTimeout
'End Property
'
'
''///////////////////////////////////////////////
''/// Properties - SQLite database connection by an ODBC driver
''///////////////////////////////////////////////
'Public Property Get ConnectingFilePath() As String
'
'    ConnectingFilePath = mobjConnector.ConnectingFilePath
'End Property
'
'
''///////////////////////////////////////////////
''/// Implemented operations
''///////////////////////////////////////////////
''''
''''
''''
'Private Function IOutputBookPath_GetPreservedOutputBookPath() As String
'
'    IOutputBookPath_GetPreservedOutputBookPath = GetSqLiteConnectingPrearrangedLogBookPath()
'End Function
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''**---------------------------------------------
''** set ADO connection string all parameters directly
''**---------------------------------------------
''''
''''
''''
'Public Sub SetODBCConnectionByDSN(ByVal vstrDSN As String)
'
'    mobjConnector.SetODBCConnectionByDSN vstrDSN
'End Sub
'
''''
''''
''''
'Public Sub SetODBCParametersWithoutDSN(ByVal vstrDbFilePath As String)
'
'    mobjConnector.SetODBCParametersWithoutDSN vstrDbFilePath
'End Sub
'
'
''**---------------------------------------------
''** set ADO connection string parameters by a User-form interface
''**---------------------------------------------
''''
'''' Using a User-form, set DSN connection
''''
'''' <Argument>vstrSettingKeyName: Input - This is to be written in Windows-registry</Argument>
'''' <Argument>vstrDSN: Input</Argument>
'Public Function IsAdoConnectionContinuedAfterOdbcConnectionDsnParametersByForm(ByVal vstrSettingKeyName As String, _
'        Optional ByVal vstrDSN As String = "") As Boolean
'
'
'    IsAdoConnectionContinuedAfterOdbcConnectionDsnParametersByForm = mobjConnector.IsAdoConnectionContinuedAfterOdbcConnectionDsnParametersByForm(vstrSettingKeyName, vstrDSN)
'End Function
'
''''
'''' Using a User-form, set DSN connection
''''
'''' <Argument>vstrSettingKeyName: Input - This is to be written in Windows-registry</Argument>
'''' <Argument>vstrDbFilePath: Input</Argument>
'''' <Argument>vstrDriverName: Input</Argument>
'Public Function IsAdoConnectionContinuedAfterOdbcConnectionDsnlessParametersByForm(ByVal vstrSettingKeyName As String, _
'        Optional ByVal vstrDbFilePath As String = "", _
'        Optional ByVal vstrDriverName As String = "") As Boolean
'
'
'    IsAdoConnectionContinuedAfterOdbcConnectionDsnlessParametersByForm = mobjConnector.IsAdoConnectionContinuedAfterOdbcConnectionDsnlessParametersByForm(vstrSettingKeyName, vstrDbFilePath, vstrDriverName)
'End Function
'
'
''''
'''' Close ADO connection
''''
'Public Sub CloseConnection()
'
'    mobjConnector.CloseConnection
'End Sub
'
''''
''''
''''
'Public Sub CloseCacheOutputBookOfOpenByPresetModeProcess()
'
'    mobjAdoRecordsetSheetExpander.CloseCacheOutputBookOfOpenByPresetModeProcess
'End Sub
'
''''
''''
''''
'Public Sub CloseAll()
'
'    CloseConnection
'
'    CloseCacheOutputBookOfOpenByPresetModeProcess
'End Sub
'
''**---------------------------------------------
''** Output Recordset and logs to cells on sheet
''**---------------------------------------------
''''
'''' query SQL and output result to Excel sheet
''''
'Public Sub OutputToSheetFrom(ByVal vstrSQL As String, _
'        ByVal vobjOutputSheet As Excel.Worksheet, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'    mobjAdoRecordsetSheetExpander.OutputToSheetFrom vstrSQL, vobjOutputSheet, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedSqLitePathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
'''' query SQL and output result to the Existed Excel sheet
''''
'Public Sub OutputInExistedBook(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputInExistedBook vstrSQL, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedSqLitePathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
'Public Sub OutputInAlreadyExistedBook(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputInAlreadyExistedBook vstrSQL, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedSqLitePathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
''''
''''
'Public Sub OutputInExistedBookByUnitTestMode(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputInExistedBookByUnitTestMode vstrSQL, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedSqLitePathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputToSpecifiedBook(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputToSpecifiedBook vstrSQL, vstrOutputBookPath, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedSqLitePathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
'Public Sub OutputToSpecifiedBookByUnitTestMode(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'    mobjAdoRecordsetSheetExpander.OutputToSpecifiedBookByUnitTestMode vstrSQL, vstrOutputBookPath, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedSqLitePathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
'''' query SQL and output result to the already specified Excel sheet
''''
'Public Sub OutputToAlreadySpecifiedBook(ByVal vstrSQL As String, _
'        ByVal vstrNewSheetName As String, _
'        Optional ByVal venmExecutedAdoSqlQueryLogPositionType As ExecutedAdoSqlQueryLogPositionType = SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'    mobjAdoRecordsetSheetExpander.OutputToAlreadySpecifiedBook vstrSQL, vstrNewSheetName, venmExecutedAdoSqlQueryLogPositionType, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedSqLitePathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
''''
''''
'Public Sub SetOutputExcelBookPath(ByVal vstrOutputBookPath As String, _
'        Optional ByVal venmOutputRSetExcelBookOpenModeProcessFlag As OutputRSetExcelBookOpenModeProcessFlag = OutputRSetExcelBookOpenModeProcessFlag.OpenOutputBookByUnitTestMode, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag)
'
'    mobjAdoRecordsetSheetExpander.SetOutputExcelBookPath vstrOutputBookPath, venmOutputRSetExcelBookOpenModeProcessFlag
'End Sub
'
''**---------------------------------------------
''** Excecute SQL command (UPDATE, INSERT, DELETE)
''**---------------------------------------------
''''
'''' command SQL and output result-log to Excel sheet
''''
'Public Sub OutputCommandLogToSheetFrom(ByVal vstrSQL As String, _
'        ByVal vobjOutputSqlCommandLogSheet As Excel.Worksheet, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogToSheetFrom vstrSQL, vobjOutputSqlCommandLogSheet, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedSqLitePathInsertText(), vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputCommandLogToSpecifiedBook(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogToSpecifiedBook vstrSQL, vstrOutputBookPath, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedSqLitePathInsertText(), vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputCommandLogToAlreadySpecifiedBook(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogToAlreadySpecifiedBook vstrSQL, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedSqLitePathInsertText(), vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputCommandLogInAlreadyExistedBook(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogInAlreadyExistedBook vstrSQL, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedSqLitePathInsertText(), vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
'''' query SQL and output result to the specified Excel sheet
''''
'Public Sub OutputCommandLogToSpecifiedBookByUnitTestMode(ByVal vstrSQL As String, _
'        ByVal vstrOutputBookPath As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogToSpecifiedBookByUnitTestMode vstrSQL, vstrOutputBookPath, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedSqLitePathInsertText(), vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
''''
'''' query SQL and output result to the prepared Excel sheet
''''
'Public Sub OutputCommandLogInExistedBookByUnitTestMode(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag, _
'        Optional ByVal vstrNewSqlCommandLogSheetName As String = "", _
'        Optional ByVal vblnChangeVisibleExcelApplicationIfItIsImvisible As Boolean = True)
'
'
'    mobjAdoRecordsetSheetExpander.OutputCommandLogInExistedBookByUnitTestMode vstrSQL, venmShowUpAdoErrorOptionFlag, mfobjGetConnectedSqLitePathInsertText(), vstrNewSqlCommandLogSheetName, vblnChangeVisibleExcelApplicationIfItIsImvisible
'End Sub
'
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
''''
''''
''''
'Private Function mfobjGetConnectedSqLitePathInsertText() As Collection
'
'    Dim objInsertTexts As Collection
'
'    Set objInsertTexts = New Collection
'
'    With mobjConnector
'
'        objInsertTexts.Add "<File Name> " & GetFileNameFromPathByVbaDir(.ConnectingFilePath) & ", <Directory> " & GetParentDirectoryPathByVbaDir(.ConnectingFilePath)
'    End With
'
'    Set mfobjGetConnectedSqLitePathInsertText = objInsertTexts
'End Function
'
'
'''--VBA_Code_File--<SqlUTfSqLiteOdbcSheetExpander.bas>--
'Attribute VB_Name = "SqlUTfSqLiteOdbcSheetExpander"
''
''   Sanity test to output SQL results into Excel sheets for a SQLite database using ADO ODBC
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on both ADO and Excel
''       Dependent on an installed SQLite ODBC driver
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Wed, 26/Jul/2023    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Constants
''///////////////////////////////////////////////
'Private Const mstrTestTableName As String = "Test_Table"
'
'
''///////////////////////////////////////////////
''/// Control VBE windows
''///////////////////////////////////////////////
''**---------------------------------------------
''** Open relative VBE code-panes
''**---------------------------------------------
'Private Sub OpenVBECodePaneRelativeToSQLUTfSqLiteConnector()
'
'    ' Focusing on this procdure, please press [F5] key to start this. Therefore this scope is private.
'
'    On Error Resume Next ' If the 'ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma' isn't included in the VBA project, the following causes no syntax error.
'
'    Application.Run "ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma", "SqLiteAdoConnector,SqLiteAdoSheetExpander,UTfOperateSqLite3,OperateSqLite3,UTfAdoConnectSqLiteDb,ADOConStrOfOdbcSqLite,ADOConStrOfDsnlessSqLite"
'End Sub
'
'
''///////////////////////////////////////////////
''/// Sanity tests
''///////////////////////////////////////////////
''**---------------------------------------------
''** SqLiteAdoSheetExpander
''**---------------------------------------------
''''
'''' Create an in-line table from the only SELECT sql inside
''''
'Private Sub msubSanityTestToConnectByVirtualTableSqLiteAdoSheetExpander()
'
'    Dim strSQL As String
'
'    With New SqLiteAdoSheetExpander
'
'        .SetODBCParametersWithoutDSN PrepareSQLiteDbAfterCreateSampleSQLiteDbIfItDoesntExist("TestDbOfSqLiteAdoSheetExpander.sqlitedb")
'
'        .AllowToRecordSQLLog = True
'
'        'strSQL = "SELECT 1"
'
'        'strSQL = "SELECT 'ABC', 'DEF'"
'
'        strSQL = "SELECT 'ABC' AS A_Column, 'DEF' AS B_Column"
'
'        .OutputInExistedBookByUnitTestMode strSQL, "Test_ADO_VirtualTable", AppendingSqlQueryLogsInSheetCellsOnIndependentSheet
'
'        .CloseAll
'    End With
'End Sub
'
'
''''
'''' Create an in-line table from the only SELECT sql inside
''''
'Private Sub msubSanityTestToConfirmSqLiteVersion()
'
'    Dim strSQL As String
'
'    With New SqLiteAdoSheetExpander
'
'        .SetODBCParametersWithoutDSN PrepareSQLiteDbAfterCreateSampleSQLiteDbIfItDoesntExist("TestDbOfSqLiteAdoSheetExpander.sqlitedb")
'
'        .AllowToRecordSQLLog = True
'
'        strSQL = "SELECT sqlite_version()"
'
'        .OutputInExistedBookByUnitTestMode strSQL, "SqLiteVersion"
'
'        .CloseAll
'    End With
'End Sub
'
''''
''''
''''
'Private Sub msubOpenCmdWithSqLiteDbParentFolder()
'
'    OpenCmdWithSpecifyingBothWorkingDirectoryAndCmdWindowTitleText GetTemporarySqLiteDataBaseDir(), "Test SQLite db"
'End Sub
'
'
''''
''''
''''
'Private Sub msubSanityTestToGetTableList()
'
'    Dim strSQL As String
'
'    With New SqLiteAdoSheetExpander
'
'        With .DataTableSheetFormattingInterface
'
'            .RecordBordersType = RecordCellsGrayAll
'        End With
'
'        .SetODBCParametersWithoutDSN PrepareSQLiteDbAfterCreateSampleSQLiteDbIfItDoesntExist("TestDbOfSqLiteAdoSheetExpander.sqlitedb")
'
'        .AllowToRecordSQLLog = True
'
'
'        strSQL = "SELECT name FROM sqlite_master WHERE type = 'table'"
'
'        .OutputInExistedBookByUnitTestMode strSQL, "SqLiteTableList", SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable
'
'        .CloseAll
'    End With
'End Sub
'
'
''''
''''
''''
'Private Sub msubSanityTestToCreateTable()
'
'    Dim strSQL As String, strDbPath As String
'
'    strDbPath = GetTemporarySqLiteDataBaseDir() & "TestDb02OfSqLiteAdoSheetExpander.sqlitedb"
'
'    If FileExistsByVbaDir(strDbPath) Then VBA.Kill strDbPath
'
'    With New SqLiteAdoSheetExpander
'
'        .SetODBCParametersWithoutDSN strDbPath
'
'        .AllowToRecordSQLLog = True
'
'        ' strSQL = "CREATE TABLE " & mstrTestTableName & "(Col1, Col2, Col3)"
'
'        strSQL = "CREATE TABLE Test_Table(Col1, Col2, Col3)"
'
'        .OutputCommandLogInExistedBookByUnitTestMode strSQL
'
'        strSQL = "SELECT name FROM sqlite_master WHERE type = 'table'"
'
'        .OutputInExistedBookByUnitTestMode strSQL, "TableList", AppendingSqlQueryLogsInSheetCellsOnIndependentSheet
'
'        .CloseAll
'    End With
'End Sub
'
''''
'''' SQLite database sanity-test for SQL INSERT
''''
'Private Sub msubSanityTestToConnectToSqLiteBySqlInsertTable()
'
'    Dim strSQL As String
'
'    With New SqLiteAdoSheetExpander
'
'        .SetODBCParametersWithoutDSN GetTemporarySqLiteDataBaseDir() & "\" & "TestSimpleDbOfSqLiteAdoSheetExpander.sqlitedb"
'
'        .DataTableSheetFormattingInterface.RecordBordersType = RecordCellsGrayAll
'
'        .AllowToRecordSQLLog = True
'
'        PrepareTestTable4ColumnOfSqLiteTestingDataBaseForSqlTests .ADOConnectorInterface
'
'
'        strSQL = "INSERT INTO " & mstrTestTableName & " VALUES (13, 14, 15, 'Type3')"
'
'        .OutputCommandLogToSpecifiedBookByUnitTestMode strSQL, GetSqLiteConnectingPrearrangedLogBookPath()
'
'
'
'        strSQL = "SELECT * FROM Test_Table"
'
'        .OutputToSpecifiedBookByUnitTestMode strSQL, GetSqLiteConnectingPrearrangedLogBookPath(), "SqlResult", SqlQueryLogsByAutoShapesOnSheetWithOutputtingRecordsetDataTable
'
'        strSQL = "SELECT name FROM sqlite_master WHERE type = 'table'"
'
'        .OutputToSpecifiedBookByUnitTestMode strSQL, GetSqLiteConnectingPrearrangedLogBookPath(), "TableList", SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable
'
'        .CloseAll
'    End With
'End Sub
'
''''
'''' SQLite database sanity-test for SQL DELETE
''''
'Private Sub msubSanityTestToConnectToSqLiteBySqlDeleteTable()
'
'    Dim strSQL As String, strDbPath As String
'
'    strDbPath = GetTemporarySqLiteDataBaseDir() & "\" & "TestSimpleDbOfSqLiteAdoSheetExpander.sqlitedb"
'
'    If FileExistsByVbaDir(strDbPath) Then VBA.Kill strDbPath
'
'    With New SqLiteAdoSheetExpander
'
'        .SetODBCParametersWithoutDSN strDbPath
'
'        .DataTableSheetFormattingInterface.RecordBordersType = RecordCellsGrayAll
'
'        .AllowToRecordSQLLog = True
'
'        PrepareTestTable4ColumnOfSqLiteTestingDataBaseForSqlTests .ADOConnectorInterface
'
'
'        strSQL = "DELETE FROM " & mstrTestTableName & " WHERE ColText = 'Type1'"
'
'        .OutputCommandLogInExistedBookByUnitTestMode strSQL
'
'
'
'        strSQL = "SELECT * FROM Test_Table"
'
'        .OutputInExistedBookByUnitTestMode strSQL, "SqlResult", AppendingSqlQueryLogsInSheetCellsOnIndependentSheet
'
'        .CloseAll
'    End With
'End Sub
'
'
''''
'''' SQLite database sanity-test for SQL UPDATE
''''
'Private Sub msubSanityTestToConnectToSqLiteBySqlUpdateTable()
'
'    Dim strSQL As String, strDbPath As String
'
'    strDbPath = GetTemporarySqLiteDataBaseDir() & "\" & "TestSimpleDbOfSqLiteAdoSheetExpander.sqlitedb"
'
'    If FileExistsByVbaDir(strDbPath) Then VBA.Kill strDbPath
'
'    With New SqLiteAdoSheetExpander
'
'        .SetODBCParametersWithoutDSN strDbPath
'
'        .DataTableSheetFormattingInterface.RecordBordersType = RecordCellsGrayAll
'
'        .AllowToRecordSQLLog = True
'
'        PrepareTestTable4ColumnOfSqLiteTestingDataBaseForSqlTests .ADOConnectorInterface
'
'
'        strSQL = "UPDATE " & mstrTestTableName & " SET Col1 = 31, Col2 = 32 WHERE Col3 = 9 AND ColText = 'Type2'"
'
'        .OutputCommandLogToSpecifiedBook strSQL, GetSqLiteConnectingPrearrangedLogBookPath(), "SqlUpdate", SqlQueryLogsInSheetCellsWithOutputtingRecordsetDataTable
'
'
'
'        strSQL = "SELECT * FROM Test_Table"
'
'        .OutputToSpecifiedBook strSQL, GetSqLiteConnectingPrearrangedLogBookPath(), "SqlResult", SqlQueryLogsByAutoShapesOnSheetWithOutputtingRecordsetDataTable
'
'        .CloseCacheOutputBookOfOpenByPresetModeProcess
'    End With
'End Sub
'''--VBA_Code_File--<ExpandCodeOnWORDOut.bas>--
'Attribute VB_Name = "ExpandCodeOnWORDOut"
''
''   output VBA code as the Word document
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on Word
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Thu,  2/Jun/2022    Kalmclaeyd Tarclanus    Initial Creation
''
'
''///////////////////////////////////////////////
''/// Directives
''///////////////////////////////////////////////
'#Const HAS_REF = False
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Constants
''///////////////////////////////////////////////
'Private Const mstrCodePrintingTemplateDocumentBaseName As String = "CodePrints"
'
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''''
'''' close all opened documents and quit Word application
''''
'Public Sub CloseAllWordDocumentWithoutDisplayAleart()
'
'    #If HAS_REF Then
'        Dim objWordApplication As Word.Application, objDoc As Word.Document
'        Dim enmOldWdAlertLevel As Word.WdAlertLevel
'    #Else
'        Dim objWordApplication As Object, objDoc As Object
'        Dim enmOldWdAlertLevel As Long
'    #End If
'
'    Dim i As Long
'
'    Set objWordApplication = mfobjGetWordApplication()
'
'    enmOldWdAlertLevel = objWordApplication.DisplayAlerts
'
'    objWordApplication.DisplayAlerts = 0 'wdAlertsNone
'
'    On Error GoTo ErrHandler
'
'    With objWordApplication
'
'        For i = .Documents.Count To 1 Step -1
'
'            Set objDoc = .Documents.Item(i)
'
'            #If HAS_REF Then
'
'                objDoc.Close Word.WdSaveOptions.wdDoNotSaveChanges
'            #Else
'                objDoc.Close 0
'            #End If
'
'            Set objDoc = Nothing
'        Next
'    End With
'
'
'    On Error GoTo 0
'
'ErrHandler:
'    If Not objWordApplication Is Nothing Then
'
'        objWordApplication.DisplayAlerts = enmOldWdAlertLevel
'
'        objWordApplication.Quit
'    End If
'
'    Set objWordApplication = Nothing
'End Sub
'
'
''''
'''' Output Code into Word.Document from a specified .dotx template file
''''
'Public Sub LoadCodeToWordDocument(ByVal vstrVBComponentName As String)
'
'    Dim strCodePath As String, objVBProject As VBIDE.VBProject
'
'#If HAS_REF Then
'
'    Dim objWordApplication As Word.Application
'#Else
'    Dim objWordApplication As Object
'#End If
'
'    With GetCurrentOfficeFileObject()
'
'        Set objVBProject = .VBProject
'    End With
'
'    With mfobjOutputVBACodeFromVBProject(objVBProject, "", "", vstrVBComponentName)
'
'        strCodePath = .Item(1)
'    End With
'
'    Set objWordApplication = mfobjGetWordApplication()
'
'    msubCodeTextToDocx strCodePath, objWordApplication
'
'    ' display the created Word object
'    If Not objWordApplication.Visible Then
'
'        ' When the new WORD process is started, the default value of the Visible is false.
'        objWordApplication.Visible = True
'    End If
'
'    Set objWordApplication = Nothing
'End Sub
'
''''
''''
''''
'#If HAS_REF Then
'
'Public Function GetSimpleDocumentAfterLoadingTextFile(ByVal vstrTextFilePath As String) As Word.Document
'
'#Else
'Public Function GetSimpleDocumentAfterLoadingTextFile(ByVal vstrTextFilePath As String) As Object
'#End If
'
'    Set GetSimpleDocumentAfterLoadingTextFile = mfobjGetSimpleDocxAfterLoadingTextFile(vstrTextFilePath, mfobjGetWordApplication())
'End Function
'
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
''''
'''' If a WORD process has already created, this get the WORD object
''''
'#If HAS_REF Then
'
'Private Function mfobjGetWordApplication() As Word.Application
'
'    Dim objWordApplication As Word.Application
'#Else
'
'Private Function mfobjGetWordApplication() As Object
'
'    Dim objWordApplication As Object
'#End If
'
'    Set objWordApplication = Nothing
'
'    On Error Resume Next
'
'    ' If WORD process has already been stared, this will get WORD object, but the WORD doesn't exist in the loaded processes, this will be failed
'    Set objWordApplication = GetObject(Class:="Word.Application")
'
'    If Err.Number <> 0 Then
'
'        Set objWordApplication = CreateObject("Word.Application")
'    End If
'
'    On Error GoTo 0
'
'    Set mfobjGetWordApplication = objWordApplication
'End Function
'
'
''''
'''' transfer the code text file to a specified format docx file
''''
'#If HAS_REF Then
'
'Private Sub msubCodeTextToDocx(ByVal vstrCodeTextFilePath As String, ByVal vobjWordApplication As Word.Application)
'
'#Else
'
'Private Sub msubCodeTextToDocx(ByVal vstrCodeTextFilePath As String, ByVal vobjWordApplication As Object)     ' Word.Application
'#End If
'
'    Dim strTemplatePath As String, blnContinue As Boolean
'
'    #If HAS_REF Then
'        Dim objDocument As Word.Document, objParagraph As Word.Paragraph, objFS As Scripting.FileSystemObject
'
'        Set objFS = New Scripting.FileSystemObject
'    #Else
'        Dim objDocument As Object, objParagraph As Object, objFS As Object
'
'        Set objFS = CreateObject("Scripting.FileSystemObject")
'    #End If
'
'    strTemplatePath = GetOfficeTemplatesRootDir() & "\" & mstrCodePrintingTemplateDocumentBaseName & ".dotx"
'
'    blnContinue = True
'
'    With objFS
'
'        If Not .FileExists(strTemplatePath) Then
'
'            blnContinue = False
'
'            MsgBox "Not found Word template file for outputting codes", vbExclamation Or vbOKOnly, "Output codes as Word document"
'        End If
'    End With
'
'    If blnContinue Then
'
'        Set objDocument = mfobjGetNewWordDocument(vobjWordApplication, strTemplatePath)
'
'        Debug.Assert Not objDocument Is Nothing
'
'        msubLoadTextToDocument objDocument, vstrCodeTextFilePath, objFS
'
'        Set objParagraph = objDocument.Paragraphs.Item(1)
'
'        msubSetParagraphFormatForSoftwareCodes objParagraph.Range.ParagraphFormat
'
'        msubSetCodeFilePathToAllWordDocumentSections objDocument, vstrCodeTextFilePath, objFS
'
'        vobjWordApplication.ActivePrinter = "Microsoft Print to PDF"
'    End If
'End Sub
'
''''
''''
''''
'#If HAS_REF Then
'Private Function mfobjGetSimpleDocxAfterLoadingTextFile(ByVal vstrTextFilePath As String, ByVal vobjWordApplication As Word.Application) As Word.Document
'
'    Dim objDocument As Word.Document, objParagraph As Word.Paragraph, objFS As Scripting.FileSystemObject
'
'    Set objFS = New Scripting.FileSystemObject
'#Else
'Private Function mfobjGetSimpleDocxAfterLoadingTextFile(ByVal vstrTextFilePath As String, ByVal vobjWordApplication As Object) As Object
'
'    Dim objDocument As Object, objParagraph As Object, objFS As Object
'
'    Set objFS = CreateObject("Scripting.FileSystemObject")
'#End If
'
'    Set objDocument = mfobjGetNewWordDocument(vobjWordApplication)
'
'    msubLoadTextToDocument objDocument, vstrTextFilePath, objFS
'
'    msubSetCodeFilePathToAllWordDocumentSections objDocument, vstrTextFilePath, objFS
'
'    vobjWordApplication.ActivePrinter = "Microsoft Print to PDF"
'
'    Set mfobjGetSimpleDocxAfterLoadingTextFile = objDocument
'End Function
'
''''
''''
''''
'Private Sub msubLoadTextToDocument(ByRef robjDocument As Word.Document, ByRef rstrTextFilePath As String, ByRef robjFS As Scripting.FileSystemObject)
'
'    Dim objParagraph As Word.Paragraph
'
'    With robjFS.OpenTextFile(rstrTextFilePath)
'
'        Set objParagraph = robjDocument.Paragraphs.Item(1)
'
'        objParagraph.Range.Text = .ReadAll
'
'        .Close
'    End With
'End Sub
'
'
''''
''''
''''
'#If HAS_REF Then
'
'Private Sub msubSetCodeFilePathToAllWordDocumentSections(ByRef robjDocument As Word.Document, ByVal vstrCodeFilePath As String, ByRef robjFS As Scripting.FileSystemObject)
'
'    Dim objSection As Word.Section, objHeader As Word.HeaderFooter
'
'#Else
'
'Private Sub msubSetCodeFilePathToAllWordDocumentSections(ByRef robjDocument As Object, ByVal vstrCodeFilePath As String, ByRef robjFS As Object)
'
'    Dim objSection As Object, objHeader As Object
'
'#End If
'
'    With robjDocument
'
'        For Each objSection In .Sections
'
'            For Each objHeader In objSection.Headers
'
'                With objHeader.Range
'
'                    .ParagraphFormat.Alignment = 2  ' wdAlignParagraphRight
'
'                    With .Font
'
'                        .Name = "Arial"
'
'                        .UnderlineColor = &HFF000000  ' wdColorAutomatic
'
'                        .Underline = 1  ' wdUnderlineSingle
'                    End With
'
'                    .Text = robjFS.GetFileName(vstrCodeFilePath)
'                End With
'            Next
'        Next
'    End With
'End Sub
'
'
''''
''''
''''
'#If HAS_REF Then
'
'Private Sub msubSetParagraphFormatForSoftwareCodes(ByRef robjParagraphFormat As Word.ParagraphFormat)
'#Else
'Private Sub msubSetParagraphFormatForSoftwareCodes(ByRef robjParagraphFormat As Object)
'#End If
'
'    With robjParagraphFormat
'
'        .LineSpacingRule = wdLineSpaceExactly
'
'        .LineSpacing = 12
'    End With
'End Sub
'
'
''''
''''
''''
'#If HAS_REF Then
'
'Private Function mfobjGetNewWordDocument(ByVal vobjWordApplication As Word.Application, Optional ByVal vstrTemplateDocumentFilePath As String = "") As Word.Document
'
'#Else
'
'Private Function mfobjGetNewWordDocument(ByVal vobjWordApplication As Object, Optional ByVal vstrTemplateDocumentFilePath As String = "") As Object
'#End If
'
'#If HAS_REF Then
'
'    Dim objDocument As Word.Document, objProtectedViewWindow As Word.ProtectedViewWindow
'#Else
'    Dim objDocument As Object, objProtectedViewWindow As Object
'#End If
'
'    On Error Resume Next
'
'    Set objDocument = Nothing
'
'    If vstrTemplateDocumentFilePath <> "" Then
'
'        Set objDocument = vobjWordApplication.Documents.Add(vstrTemplateDocumentFilePath, False, 0)       ' WdNewDocumentType.wdNewBlankDocument = 0
'    Else
'        Set objDocument = vobjWordApplication.Documents.Add()
'    End If
'
'    If Err.Number <> 0 Then
'
'        Set objProtectedViewWindow = Nothing
'
'        Set objProtectedViewWindow = vobjWordApplication.ProtectedViewWindows.Item(1)
'
'        If Not objProtectedViewWindow Is Nothing Then
'
'            Set objDocument = objProtectedViewWindow.Edit()
'
'            If Not objDocument Is Nothing Then
'
'                Set objProtectedViewWindow = Nothing
'            End If
'        End If
'    End If
'
'    On Error GoTo 0
'
'    Set mfobjGetNewWordDocument = objDocument
'End Function
'
'
'
'
''///////////////////////////////////////////////
''/// Sanity tests
''///////////////////////////////////////////////
''''
'''' Sanity-test for the Code format docx creation
''''
'Public Sub msubSanityTestCodeToDocxFromCoreCode(Optional ByVal vstrClassifyingKey01 As String = "OfficeCommonCore", _
'        Optional ByVal vstrClassifyingKey02 As String = "", _
'        Optional ByVal vstrFilteringComponentNames As String = "")
'
'    Dim objCodePaths As Collection, varCodePath As Variant, strCodePath As String
'
'#If HAS_REF Then
'    Dim objWordApplication As Word.Application
'#Else
'    Dim objWordApplication As Object
'#End If
'
'    Set objCodePaths = GetComponentCodeFullPathsAtDefaultOnlyFileRepository(vstrClassifyingKey01, vstrClassifyingKey02, vstrFilteringComponentNames)
'
'    If Not objCodePaths Is Nothing Then
'
'        Set objWordApplication = mfobjGetWordApplication()
'
'        For Each varCodePath In objCodePaths
'
'            strCodePath = varCodePath
'
'            msubCodeTextToDocx strCodePath, objWordApplication
'        Next
'
'        ' display the created Word object
'        If Not objWordApplication.Visible Then
'
'            ' When the new WORD process is started, the default value of the Visible is false.
'            objWordApplication.Visible = True
'        End If
'    Else
'        MsgBox "Not searched VB component file-path", vbInformation Or vbOKOnly, "VBA codes to Word document process"
'    End If
'
'    Set objWordApplication = Nothing
'End Sub
'
''''
''''
''''
'Private Sub msubSanityTestToCodeToDocxAboudDumpModule()
'
'    LoadCodeToWordDocument "DumpModule"
'End Sub
'
'
''''
''''
''''
'Private Sub msubSanityTestToDumpOutputCodePaths()
'
'    msubDumpOutputCodePaths "", "", "DumpModule"
'End Sub
'
'
''''
''''
''''
'Private Sub msubDumpOutputCodePaths(ByVal vstrClassifyingKey01 As String, ByVal vstrClassifyingKey02 As String, ByVal vstrFilteringComponentNames As String)
'
'    Dim varFilePath As Variant
'
'    For Each varFilePath In mfobjGetOutputCodePaths(vstrClassifyingKey01, vstrClassifyingKey02, vstrFilteringComponentNames)
'
'        Debug.Print varFilePath
'    Next
'End Sub
'
''''
''''
''''
'Private Function mfobjOutputVBACodeFromVBProject(ByVal vobjVBProject As VBIDE.VBProject, ByVal vstrClassifyingKey01 As String, ByVal vstrClassifyingKey02 As String, ByVal vstrFilteringComponentNames As String) As Collection
'
'    Dim objFilePaths As Collection, objFS As Scripting.FileSystemObject, strDir As String
'    Dim varFilePath As Variant, objOutputFilePaths As Collection, objVBComponent As VBIDE.VBComponent, strComponentName As String
'
'
'    Set objFilePaths = mfobjGetOutputCodePaths(vstrClassifyingKey01, vstrClassifyingKey02, vstrFilteringComponentNames)
'
'    Set objOutputFilePaths = New Collection
'
'    Set objFS = New Scripting.FileSystemObject
'
'    With vobjVBProject
'
'        For Each varFilePath In objFilePaths
'
'            strComponentName = objFS.GetBaseName(varFilePath)
'
'            Set objVBComponent = Nothing
'
'            On Error Resume Next
'
'            Set objVBComponent = .VBComponents.Item(strComponentName)
'
'            On Error GoTo 0
'
'
'            If Not objVBComponent Is Nothing Then
'
'                With objFS
'
'                    strDir = objFS.GetParentFolderName(varFilePath)
'
'                    ForceToCreateDirectory strDir, objFS
'
'                    If .FileExists(varFilePath) Then
'
'                        .DeleteFile varFilePath, True
'                    End If
'                End With
'
'                objVBComponent.Export varFilePath
'
'                objOutputFilePaths.Add varFilePath
'            End If
'        Next
'    End With
'
'    Set mfobjOutputVBACodeFromVBProject = objOutputFilePaths
'End Function
'
'
''''
''''
''''
'Private Function mfobjGetOutputCodePaths(ByVal vstrClassifyingKey01 As String, ByVal vstrClassifyingKey02 As String, ByVal vstrFilteringComponentNames As String) As Collection
'
'    Dim strBaseDir As String, objComponentNameToRelativeFilePathDic As Scripting.Dictionary
'    Dim varComponentName As Variant
'    Dim strFilePath As String, objCol As Collection
'
'
'    Set objCol = New Collection
'
'    strBaseDir = GetTmpCompareRootPathOriginal()
'
'    Set objComponentNameToRelativeFilePathDic = GetComponentNameToRelativeFilePathDic(vstrClassifyingKey01, vstrClassifyingKey02, vstrFilteringComponentNames)
'
'    With objComponentNameToRelativeFilePathDic
'
'        For Each varComponentName In .Keys
'
'            strFilePath = strBaseDir & "\" & .Item(varComponentName)
'
'            objCol.Add strFilePath
'        Next
'    End With
'
'    Set mfobjGetOutputCodePaths = objCol
'End Function
'
'
''''
''''
''''
'Public Sub SanityTestCodeToDocxFromCoreCode()
'
'    Dim strSpecifiedComponentNames As String
'
'    'strSpecifiedComponentNames = ""
'    'strSpecifiedComponentNames = "DataTableSheetOut"
'
'    'strSpecifiedComponentNames = "DecorationSetterToXlSheet,DecorationGetterFromXlSheet,ADOSheetOut,DataTableSheetOut,LoadAndModifyModule"
'
'    'msubSanityTestCodeToDocxFromCoreCode "", "", strSpecifiedComponentNames
'
'
'
'    msubSanityTestCodeToDocxFromCoreCode "CoreCode,ADOAbstract,ADOODBC,AdoErrorHandlingTools,XlAdoConnectingTools", "LibGeneral,DevMManaging"
'
'    'msubSanityTestCodeToDocxFromCoreCode "CoreCode,IndependentCellParam", "LibGeneral", strSpecifiedComponentNames
'
'    'msubSanityTestCodeToDocxFromCoreCode "CoreCode,ADOODBC,XlAdoConnectingTools,MakeHyperLinks,ADOAbstract", "LIbGeneral"
'
'    'msubSanityTestCodeToDocxFromCoreCode "ADOAbstract", "LibGeneral"
'
'End Sub
'
''''
''''
''''
'Public Sub SanityTestCodeToDocxFromConverToVBS()
'
'    'msubSanityTestCodeToDocxFromCoreCode "", "DevMManaging"
'
'    msubSanityTestCodeToDocxFromCoreCode "", "", "DumpModule"
'End Sub

