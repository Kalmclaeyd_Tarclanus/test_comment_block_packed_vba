Attribute VB_Name = "Module3"
'
'   PackedExperimentalMetaVba [ 03 / 06 ] Self-extracting comment-block packed VBA source codes, which is generated automatically
'
'   Coding Conventions Note:
'       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
'       Almost recent integrated development environment(IDE) applications of various programming languages have various
'       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
'       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
'       some features are different. First, this editor has no powerful refactoring functions.
'       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
'       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
'       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
'       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
'       calculation codes, the notation is to be often near the application-Hungarian notation.
'       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
'       Although the lack of convenient refactoring tools of IDE is a weak point,
'       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
'
'   Count of packed modules:
'       70 / 393
'
'   Lack user-form sources:
'       UTestFormTextRClickClipBoard - CommonOfficeVBA\FormTools\UnitTest\UTestFormTextRClickClipBoard.frm
'       UInputPasswordBox - CommonOfficeVBA\FormTools\ManagePassword\UInputPasswordBox.frm
'       LogTextForm - CommonOfficeVBA\LoggingModules\LogTextForm\LogTextForm.frm
'       UProgressBarForm - CommonOfficeVBA\UserFormTools\UProgressBarForm.frm
'       UTestFormNoFormTop - CommonOfficeVBA\WindowControlModules\CommonControlHandlers\UnitTest\UTestFormNoFormTop.frm
'       UTestFormWithFormTop - CommonOfficeVBA\WindowControlModules\CommonControlHandlers\UnitTest\UTestFormWithFormTop.frm
'       UTestFormKeepStateReg - CommonOfficeVBA\RegistryAccess\UTestFormKeepStateReg.frm
'       CodePaneSwitchMultiLstForm - CommonOfficeVBA\DevelopmentAssist\SwitchCodePanes\CodePaneSwitchMultiLstForm.frm
'       CodePaneSwitchMultiLsvForm - CommonOfficeVBA\DevelopmentAssist\SwitchCodePanes\CodePaneSwitchMultiLsvForm.frm
'       CodePaneSwitchSingleLsvForm - CommonOfficeVBA\DevelopmentAssist\SwitchCodePanes\CodePaneSwitchSingleLsvForm.frm
'       USetAdoOleDbConStrForXlBook - CommonOfficeVBA\ADOModules\USetAdoOleDbConStrForXlBook.frm
'       USetAdoOdbcConStrForSqLite - CommonOfficeVBA\SQLiteConnector\USetAdoOdbcConStrForSqLite.frm
'       USetAdoOleDbConStrForAccdb - CommonOfficeVBA\ADOModules\MsAccessTool\USetAdoOleDbConStrForAccdb.frm
'       USetAdoOdbcConStrForPgSql - CommonOfficeVBA\PostgreSQLConnector\USetAdoOdbcConStrForPgSql.frm
'       USetAdoOdbcConStrForOracle - CommonOfficeVBA\OracleConnector\USetAdoOdbcConStrForOracle.frm
'       UErrorADOSQLForm - CommonOfficeVBA\ADOModules\AdoErrorHandling\UErrorADOSQLForm.frm
'
'   In this packed source files:
'       ADOConnectionUtilities.bas, ADOConStrOfDsn.cls, ADOConStrOfAceOleDb.cls, IADOConnectStrGenerator.cls, IADOConnector.cls
'       ADOConnectingInClasses.bas, ADOConnector.cls, DataTableADOIn.bas, UserFormToolsForAdoConStr.bas, FormStateToSetAdoConStr.cls
'       SimplePdAuthenticationAdo.bas, SimpleAdoConStrAuthentication.bas, ADOConStrToolsForExcelBook.bas, ADOConStrOfAceOleDbXl.cls, XlAdoConnector.cls
'       SqlAdoConnectingUTfXl.bas, CompareOnlySheetsByAdo.bas, UTfCompareOnlySheetsByAdo.bas, ADOConStrToolsForCSV.bas, ADOConStrOfAceOleDbCsv.cls
'       CsvAdoConnector.cls, CsvConnectingCommonPath.bas, SqlAdoConnectingUTfCsv.bas, OperateSqLite3.bas, ADOConStrToolsForSqLite.bas
'       ADOConStrOfDsnlessSqLite.cls, UTfAdoConnectSqLiteDb.bas, SqLiteAdoConnector.cls, SqlAdoConnectOdbcUTfSqLite.bas, ADOConStrOfOdbcSqLite.cls
'       UTfAdoConnectAccDb.bas, ADOConStrToolsForAccDb.bas, ADOConStrOfOleDbMsAccess.cls, AccDbAdoConnector.cls, SqlAdoConnectingUTfAccDb.bas
'       UTfDaoConnectAccDb.bas, ADOConStrToolsForPgSql.bas, ADOConStrOfDsnlessPgSql.cls, PgSqlCommonPath.bas, PgSqlDSNlessTest.bas
'       PgSqlOdbcConnector.cls, SqlAdoConnectOdbcUTfPgSql.bas, ADOConStrOfOdbcPgSql.cls, ConvertOraclePlSqlAndVba.bas, ADOConStrOfDsnlessOracle.cls
'       ADOConStrOfOleDbOracle.cls, ADOConStrToolsForOracle.bas, OracleOdbcConnector.cls, OracleOleDbConnector.cls, OracleCommonPath.bas
'       ADOConStrOfOdbcOracle.cls, SQLResult.cls, ErrorContentKeeper.cls, ErrorADOSQL.cls, ErrorLogSheetLocator.cls
'       ADOFailedInformation.bas, ImportAllVBACodes.bas, ExportAllVBACodes.bas, RemoveAllVBACodes.bas, UTfRemoveAllVBACodesForXl.bas
'       CodeVBAReferenceSetter.bas, UTfCodeVBAReferenceSetterForXl.bas, CodeBookPath.bas, ModifyExternalVBACode.bas, CodeVBAFunctions.bas
'       CompareVBProjects.bas, OperateVBIDEForProjectPass.bas, MetaDirectives.bas, LTfMetaDirectives.bas, UTfMetaDirectives.bas
'
'   Author:
'       Kalmclaeyd M. Tarclanus
'
'   License disclosure:
'       Copyright (c) 2024 Kalmclaeyd M. Tarclanus
'       Released under the MIT license
'       https://opensource.org/licenses/mit-license.php
'
'   Modification History:
'       Mon, 22/Jan/2024    Tarclanus-generator     Generated automatically
'
Option Explicit

'///////////////////////////////////////////////
'/// Directives
'///////////////////////////////////////////////
#Const HAS_REF = False

'///////////////////////////////////////////////
'/// Constants
'///////////////////////////////////////////////

Private Const mstrThisExtractionModuleName As String = "Module3"

Private Const mstrALoadingDirectionVBProjectName As String = "ConvergenceProj"

'**---------------------------------------------
'** Block preserved keyword definitions
'**---------------------------------------------
Private Const mstrBlockDelimiterOfCodesBegin As String = "'''--Begin the comment out codes block--"

Private Const mstrBlockDelimiterOfASourceFileBeginLeft As String = "'''--VBA_Code_File--<"

Private Const mstrBlockDelimiterOfASourceFileBeginRight As String = ">--"

'**---------------------------------------------
'** Temporary files paths
'**---------------------------------------------
Private Const mstrTmpSubDir As String = "TmpCBSubDirModule3"


'///////////////////////////////////////////////
'/// Sanity tests
'///////////////////////////////////////////////
'''
''' Extract all VBA source files from this file comment block and load these into the specified VBA project name Macro book
'''
Public Sub ExtractVbaCodesFrom_PackedExperimentalMetaVba03_InThisCommentBlock()

    Dim objBook As Excel.Workbook
    
    Set objBook = mfobjGetBookAfterLoadingVbaCodesFromThisVbaModuleCommentBlock()
End Sub


'''
'''
'''
Private Sub msubSanityTestToWriteSourceCodesFromThisVbaModuleCommentBlockToTemporaryDirectory()

    msubWriteSourceCodesFromThisVbaModuleCommentBlockToTemporaryDirectory
End Sub


'''
'''
'''
Private Sub msubSanityTestToGetFileNameToTextDicFromThisVbaModuleCommentBlock()

    Dim objDic As Scripting.Dictionary: Set objDic = mfobjGetFileNameToTextDicFromThisVbaModuleCommentBlock()

    'DebugDic objDic
End Sub

'''
'''
'''
Private Sub msubSanityTestToConfirmThisText()

    Debug.Print mfstrGetThisVbaModuleText()
End Sub



'///////////////////////////////////////////////
'/// Internal functions
'///////////////////////////////////////////////

'''
'''
'''
Private Function mfobjGetBookAfterLoadingVbaCodesFromThisVbaModuleCommentBlock(Optional ByVal vstrTmpVbaCodesSubDir As String = mstrTmpSubDir) As Excel.Workbook

    Dim objBook As Excel.Workbook
    
    Dim strTmpDir As String
   
#If HAS_REF Then

    Dim objFS As Scripting.FileSystemObject, objDir As Scripting.Folder, objFile As Scripting.File
    
    Dim objVBProject As VBIDE.VBProject
    
    Set objFS = New Scripting.FileSystemObject
#Else
    Dim objFS As Object, objDir As Object, objFile As Object
    
    Dim objVBProject As Object
    
    Set objFS = CreateObject("Scripting.FileSystemObject")
#End If
  
    msubWriteSourceCodesFromThisVbaModuleCommentBlockToTemporaryDirectory vstrTmpVbaCodesSubDir


    Set objBook = mfobjGetTargetWorkbookOfSpecifiedVBProjectName()

    Set objVBProject = objBook.VBProject

    strTmpDir = ThisWorkbook.Path & "\" & vstrTmpVbaCodesSubDir

    Set objDir = objFS.GetFolder(strTmpDir)

    For Each objFile In objDir.Files
    
        objVBProject.VBComponents.Import objFile.Path
    Next

    Set mfobjGetBookAfterLoadingVbaCodesFromThisVbaModuleCommentBlock = objBook
End Function

'''
'''
'''
Private Function mfobjGetTargetWorkbookOfSpecifiedVBProjectName() As Excel.Workbook

    Dim objBook As Excel.Workbook, objTargetBook As Excel.Workbook
    
#If HAS_REF Then

    Dim objVBProject As VBIDE.VBProject
#Else
    Dim objVBProject As Object
#End If

    Set objTargetBook = Nothing

    For Each objBook In ThisWorkbook.Application.Workbooks
    
        Set objVBProject = objBook.VBProject
        
        If StrComp(objVBProject.Name, mstrALoadingDirectionVBProjectName) = 0 Then
        
            Set objTargetBook = objBook
            
            Exit For
        End If
    Next
    
    If objTargetBook Is Nothing Then
    
        Set objTargetBook = ThisWorkbook.Application.Workbooks.Add()
        
        objTargetBook.VBProject.Name = mstrALoadingDirectionVBProjectName
    End If

    Set mfobjGetTargetWorkbookOfSpecifiedVBProjectName = objTargetBook
End Function


'''
'''
'''
Private Sub msubWriteSourceCodesFromThisVbaModuleCommentBlockToTemporaryDirectory(Optional ByVal vstrTmpVbaCodesSubDir As String = mstrTmpSubDir)

#If HAS_REF Then
    Dim objSourceFileNameToCodesDic As Scripting.Dictionary
    
    Dim objFS As Scripting.FileSystemObject, objFile As Scripting.File
    
    Set objFS = New Scripting.FileSystemObject
#Else
    Dim objSourceFileNameToCodesDic As Object
    
    Dim objFS As Object, objFile As Object
    
    Set objFS = CreateObject("Scripting.FileSystemObject")
#End If

    Dim varSourceFileName As Variant, strSourceFilePath As String
    Dim strTmpDir As String
    
    
    strTmpDir = ThisWorkbook.Path & "\" & vstrTmpVbaCodesSubDir
    
    With objFS
    
        If Not .FolderExists(strTmpDir) Then
        
            .CreateFolder strTmpDir
        Else
            ' delete all
            
            For Each objFile In .GetFolder(strTmpDir).Files
        
                objFile.Delete
            Next
        End If
    End With
    
    
    Set objSourceFileNameToCodesDic = mfobjGetFileNameToTextDicFromThisVbaModuleCommentBlock()

    With objSourceFileNameToCodesDic

        For Each varSourceFileName In .Keys
        
            strSourceFilePath = strTmpDir & "\" & varSourceFileName
        
            With objFS
            
                With .CreateTextFile(strSourceFilePath, True)
                
                    .Write objSourceFileNameToCodesDic.Item(varSourceFileName)
                
                    .Close
                End With
            End With
        Next
    End With
End Sub


'''
'''
'''
#If HAS_REF Then
Private Function mfobjGetFileNameToTextDicFromThisVbaModuleCommentBlock() As Scripting.Dictionary

    Dim objDic As Scripting.Dictionary: Set objDic = New Scripting.Dictionary
#Else
Private Function mfobjGetFileNameToTextDicFromThisVbaModuleCommentBlock() As Object

    Dim objDic As Object: Set objDic = CreateObject("Scripting.Dictionary")
#End If
    Dim varLineText As Variant
    Dim blnFoundBlockOfCodesBegin As Boolean
    Dim blnInASourceFileBlock As Boolean, strCurrentSourceFileName As String
    Dim intLeftPoint As Long, intRightPoint As Long, intChildSourceLineNumber As Long
    Dim strCurrentChildSourceText As String
    
    
    blnFoundBlockOfCodesBegin = False
    
    blnInASourceFileBlock = False
    
    strCurrentChildSourceText = ""

    For Each varLineText In Split(mfstrGetThisVbaModuleText(), vbNewLine)

        If Not blnFoundBlockOfCodesBegin Then
        
            If InStr(1, varLineText, "'") = 1 Then
            
                If InStr(1, varLineText, mstrBlockDelimiterOfCodesBegin) > 0 Then
                
                    blnFoundBlockOfCodesBegin = True
                End If
            End If
        End If

        If blnFoundBlockOfCodesBegin Then
        
            If Not blnInASourceFileBlock Then
            
                intLeftPoint = InStr(1, varLineText, mstrBlockDelimiterOfASourceFileBeginLeft)
                
                If intLeftPoint > 0 Then
                
                    intRightPoint = InStrRev(varLineText, mstrBlockDelimiterOfASourceFileBeginRight)
                    
                    strCurrentSourceFileName = Mid(varLineText, intLeftPoint + Len(mstrBlockDelimiterOfASourceFileBeginLeft), intRightPoint - (intLeftPoint + Len(mstrBlockDelimiterOfASourceFileBeginLeft)))
                    
                    intChildSourceLineNumber = 0
                    
                    blnInASourceFileBlock = True
                End If
            End If
            
            
            If blnInASourceFileBlock Then
            
                If intChildSourceLineNumber > 0 Then
                
                    intLeftPoint = InStr(1, varLineText, mstrBlockDelimiterOfASourceFileBeginLeft)
                    
                    If intLeftPoint > 0 Then
                    
                        ' save cache the current source text
                        If strCurrentChildSourceText <> "" Then
                        
                            objDic.Add strCurrentSourceFileName, strCurrentChildSourceText
                        End If
                    
                    
                        ' Prepare the new file
                        intRightPoint = InStrRev(varLineText, mstrBlockDelimiterOfASourceFileBeginRight)
                    
                        strCurrentSourceFileName = Mid(varLineText, intLeftPoint + Len(mstrBlockDelimiterOfASourceFileBeginLeft), intRightPoint - (intLeftPoint + Len(mstrBlockDelimiterOfASourceFileBeginLeft)))
                    
                        intChildSourceLineNumber = 0
                    
                        strCurrentChildSourceText = ""
                        
                        blnInASourceFileBlock = True
                    End If
                End If
                
                If intChildSourceLineNumber > 0 Then
                
                    ' confirm the line head character is the line comment char
                    If InStr(1, varLineText, "'") = 1 Then
                    
                        If strCurrentChildSourceText <> "" Then
                        
                            strCurrentChildSourceText = strCurrentChildSourceText & vbNewLine
                        End If
                    
                        ' get a source code line
                    
                        strCurrentChildSourceText = strCurrentChildSourceText & Right(varLineText, Len(varLineText) - 1)
                    Else
                        ' save cache the current source text
                        If strCurrentChildSourceText <> "" Then
                        
                            objDic.Add strCurrentSourceFileName, strCurrentChildSourceText
                        End If
                        
                        intChildSourceLineNumber = 0
                    
                        strCurrentChildSourceText = ""
                        
                        blnInASourceFileBlock = False
                    End If
                End If
            
                intChildSourceLineNumber = intChildSourceLineNumber + 1
            End If
        End If
    Next

    Set mfobjGetFileNameToTextDicFromThisVbaModuleCommentBlock = objDic
End Function


'''
'''
'''
Private Function mfstrGetThisVbaModuleText() As String

#If HAS_REF Then
    Dim objVBProject As VBIDE.VBProject, objVBComponent As VBIDE.VBComponent
#Else
    Dim objVBProject As Object, objVBComponent As Object
#End If
    Dim strText As String
    
    Set objVBProject = ThisWorkbook.VBProject
    
    Set objVBComponent = objVBProject.VBComponents.Item(mstrThisExtractionModuleName)

    With objVBComponent.CodeModule
    
        strText = .Lines(1, .CountOfLines)
    End With

    mfstrGetThisVbaModuleText = strText
End Function


'''--Begin the comment out codes block--
'''--VBA_Code_File--<ADOConnectionUtilities.bas>--
'Attribute VB_Name = "ADOConnectionUtilities"
''
''   ADO connection string generation and connection tests,
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on ADO
''       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access.
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Tue,  7/Jun/2022    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''**---------------------------------------------
''** general connection tests
''**---------------------------------------------
''''
'''' general ADO connection-string test
''''
'Public Function IsADOConnectionStringEnabled(ByVal vstrConnectionString As String) As Boolean
'
'    Dim blnRet As Boolean, objConnect As ADODB.Connection, intErrNumber As Long
'
'    blnRet = False
'
'    intErrNumber = 0
'
'    Set objConnect = New ADODB.Connection
'
'    On Error GoTo GetErrData
'
'    With objConnect
'
'        .ConnectionString = vstrConnectionString
'
'        .Open
'    End With
'
'GetErrData:
'
'    If Err.Number <> 0 Then
'
'        Debug.Print "Failed to connect a RDB by ADODB, Err.Number = " & Err.Number & ", Description - " & Err.Description
'    End If
'
'    intErrNumber = Err.Number
'
'    On Error GoTo Filnally
'
'Filnally:
'    If intErrNumber = 0 Then
'
'        blnRet = True
'
'        objConnect.Close
'    End If
'
'    Set objConnect = Nothing
'
'    IsADOConnectionStringEnabled = blnRet
'End Function
'
'
''''
''''
''''
'''' <Argument>DSN: Data Source Name, this is defined by ODBC (32 bit) administrator</Argument>
'''' <Argument>UID: User ID</Argument>
'''' <Argument>PWD: Password</Argument>
'Public Function DoesODBCDSNSettingExists(ByVal vstrDSN As String, ByVal vstrUID As String, ByVal vstrPWD As String) As Boolean
'
'    Dim strConnectionString As String
'
'    strConnectionString = "DSN=" & vstrDSN & ";UID=" & vstrUID & ";PWD=" & vstrPWD & ";"
'
'    DoesODBCDSNSettingExists = IsADOConnectionStringEnabled(strConnectionString)
'End Function
'
'
''**---------------------------------------------
''** for control USetAdoOdbcConStrForPgSql user-form, USetAdoOdbcConStrForOracle user-form
''**---------------------------------------------
''''
''''
''''
'Public Function GetInitialUserInputParametersDicForAdoDSNConnection(Optional ByVal venmOdbcConnectionDestinationRDBType As OdbcConnectionDestinationRDBType = OdbcConnectionDestinationRDBType.OdbcConnectionDestinationToPostgreSQL, _
'        Optional ByVal vstrDSN As String = "", _
'        Optional ByVal vstrUserName As String = "", _
'        Optional ByVal vstrPassword As String = "") As Scripting.Dictionary
'
'    Dim objDic As Scripting.Dictionary
'
'    Set objDic = New Scripting.Dictionary
'
'    With objDic
'
'        .Add "UseDSN", True
'
'        .Add "DSN", vstrDSN
'
'        If vstrUserName <> "" Then
'
'            .Add "UserName", vstrUserName
'        End If
'
'        If vstrPassword <> "" Then
'
'            .Add mstrTemporaryPdKey, vstrPassword
'        End If
'
'        .Add mstrAdoConnectionTypeAndDestinationKey, GetDescriptionOfOdbcConnectionDestinationRDBType(venmOdbcConnectionDestinationRDBType)
'    End With
'
'    Set GetInitialUserInputParametersDicForAdoDSNConnection = objDic
'End Function
'
''''
''''
''''
'Public Sub UpdateADOConStrOfDSNFromInputDic(ByRef robjADOConStrOfDsn As ADOConStrOfDsn, ByRef robjUserInputParametersDic As Scripting.Dictionary)
'
'    With robjUserInputParametersDic
'
'        robjADOConStrOfDsn.SetODBCConnectionWithDSN .Item("DSN"), .Item("UserName"), .Item(mstrTemporaryPdKey)
'    End With
'End Sub
'
'
''''
''''
''''
'Public Function GetADOConStrOfDSNFromInputDic(ByRef robjUserInputParametersDic As Scripting.Dictionary) As ADOConStrOfDsn
'
'    Dim objADOConStrOfDsn As ADOConStrOfDsn
'
'    Set objADOConStrOfDsn = New ADOConStrOfDsn
'
'    UpdateADOConStrOfDSNFromInputDic objADOConStrOfDsn, robjUserInputParametersDic
'
'    Set GetADOConStrOfDSNFromInputDic = objADOConStrOfDsn
'End Function
'
'''--VBA_Code_File--<ADOConStrOfDsn.cls>--
'VERSION 1.0 CLASS
'BEGIN
'  MultiUse = -1  'True
'END
'Attribute VB_Name = "ADOConStrOfDsn"
'Attribute VB_GlobalNameSpace = False
'Attribute VB_Creatable = False
'Attribute VB_PredeclaredId = False
'Attribute VB_Exposed = False
''
''   ADO connection string generator for general RDB with DSN
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on ADO
''       Dependent on ODBC Data-source-name setting (DSN) at this Windows
''       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access.
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Thu,  8/Jun/2023    Kalmclaeyd Tarclanus    Separated from ADOConnectStrGenerator
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Interface implementations
''///////////////////////////////////////////////
'Implements IADOConnectStrGenerator
'
''///////////////////////////////////////////////
''/// Declarations
''///////////////////////////////////////////////
''**---------------------------------------------
''** ODBC DSN connection
''**---------------------------------------------
'Private mstrDSN As String   ' Data Source Name
'
'Private mstrUID As String   ' User ID
'
'Private mstrPWD As String   ' Password
'
'
'' Additional information
'Private menmOdbcConnectionDestinationRDBType As OdbcConnectionDestinationRDBType
'
''///////////////////////////////////////////////
''/// Event Handlers
''///////////////////////////////////////////////
'Private Sub Class_Initialize()
'
'    ' about ODBC
'    mstrDSN = ""
'
'    ' Additional information
'    menmOdbcConnectionDestinationRDBType = OdbcConnectionDestinationToPostgreSQL
'End Sub
'
''///////////////////////////////////////////////
''/// Implemented operations
''///////////////////////////////////////////////
'Private Function IADOConnectStrGenerator_GetConnectionString() As String
'
'    IADOConnectStrGenerator_GetConnectionString = GetConnectionStringForODBCWithDSN()
'End Function
'
''///////////////////////////////////////////////
''/// Properties
''///////////////////////////////////////////////
''**---------------------------------------------
''** ODBC DSN connection
''**---------------------------------------------
''''
'''' read-only Data Source Name - ODBC
''''
'Public Property Get DSN() As String
'
'    DSN = mstrDSN
'End Property
'
''''
'''' read-only User Name - ODBC
''''
'Public Property Get UserName() As String
'
'    UserName = mstrUID
'End Property
'
''**---------------------------------------------
''** Additional information
''**---------------------------------------------
''''
''''
''''
'Public Property Get OdbcConnectionDestinationRDB() As OdbcConnectionDestinationRDBType
'
'    OdbcConnectionDestinationRDB = menmOdbcConnectionDestinationRDBType
'End Property
'
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''''
''''
''''
'Public Function GetConnectionString()
'
'    GetConnectionString = GetConnectionStringForODBCWithDSN()
'End Function
'
''''
'''' by ODBC DSN connection
''''
'Public Function GetConnectionStringForODBCWithDSN() As String
'
'    GetConnectionStringForODBCWithDSN = "DSN=" & mstrDSN & ";UID=" & mstrUID & ";PWD=" & mstrPWD & ";"
'End Function
'
''''
'''' using ODBC DataSource setting (DSN)
''''
'Public Sub SetODBCConnectionWithDSN(ByVal vstrDSN As String, ByVal vstrUID As String, ByVal vstrPWD As String)
'
'    mstrDSN = vstrDSN   ' Data Source Name
'
'    mstrUID = vstrUID   ' User ID
'
'    mstrPWD = vstrPWD   ' Password
'End Sub
'
''''
''''
''''
'Public Sub SetOdbcConnectionDestinationInformation(ByVal venmOdbcConnectionDestinationRDBType As OdbcConnectionDestinationRDBType)
'
'    menmOdbcConnectionDestinationRDBType = venmOdbcConnectionDestinationRDBType
'End Sub
'
'
'
'
'''--VBA_Code_File--<ADOConStrOfAceOleDb.cls>--
'VERSION 1.0 CLASS
'BEGIN
'  MultiUse = -1  'True
'END
'Attribute VB_Name = "ADOConStrOfAceOleDb"
'Attribute VB_GlobalNameSpace = False
'Attribute VB_Creatable = False
'Attribute VB_PredeclaredId = False
'Attribute VB_Exposed = False
''
''   ADO Ace OLE DB connection string generator for either Excel, CSV, and Microsoft Access DB
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on ADO
''       Dependent on ADOConnectStrGenerator.bas
''       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access.
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Thu,  8/Jun/2023    Kalmclaeyd Tarclanus    Separated from ADOConnectStrGenerator
''
'
'Option Explicit
'
'
''///////////////////////////////////////////////
''/// Declarations
''///////////////////////////////////////////////
''**---------------------------------------------
''** OLE DB connection
''**---------------------------------------------
'Private mstrProviderName As String    ' OLE DB provider name string
'
''**---------------------------------------------
''** Excel-book connection by ACCESS OLE-DB provider
''**---------------------------------------------
'Private mstrConnectingFilePath As String   ' Access DB path, Excel book path, csv file path
'
'
''///////////////////////////////////////////////
''/// Properties
''///////////////////////////////////////////////
''''
''''
''''
'Public Property Get OleDbProviderName() As String
'
'    OleDbProviderName = mstrProviderName
'End Property
'Public Property Let OleDbProviderName(ByVal vstrProviderName As String)
'
'    mstrProviderName = vstrProviderName
'End Property
'
''''
''''
''''
'Public Property Get ConnectingFilePath() As String
'
'    ConnectingFilePath = mstrConnectingFilePath
'End Property
'Public Property Let ConnectingFilePath(ByVal vstrConnectingFilePath As String)
'
'    mstrConnectingFilePath = vstrConnectingFilePath
'End Property
'
'
'''--VBA_Code_File--<IADOConnectStrGenerator.cls>--
'VERSION 1.0 CLASS
'BEGIN
'  MultiUse = -1  'True
'END
'Attribute VB_Name = "IADOConnectStrGenerator"
'Attribute VB_GlobalNameSpace = False
'Attribute VB_Creatable = False
'Attribute VB_PredeclaredId = False
'Attribute VB_Exposed = False
''
''   Interface class - interfaces ADO connection string generator classes
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access.
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Fri, 12/May/2023    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Operation interfaces
''///////////////////////////////////////////////
'
'Public Function GetConnectionString() As String: End Function
'
'''--VBA_Code_File--<IADOConnector.cls>--
'VERSION 1.0 CLASS
'BEGIN
'  MultiUse = -1  'True
'END
'Attribute VB_Name = "IADOConnector"
'Attribute VB_GlobalNameSpace = False
'Attribute VB_Creatable = False
'Attribute VB_PredeclaredId = False
'Attribute VB_Exposed = False
''
''   Interface class - interfaces of ADOConnector class
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on ADO
''       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access.
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Fri, 12/May/2023    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Property interfaces
''///////////////////////////////////////////////
'
'Public IsConnected As Boolean
'
'Public ADOConnection As ADODB.Connection
'
'Public SQLExecutionResult As SQLResult
'
'Public CommandTimeout As Long
'
'Public SuppressToShowUpSqlExecutionError As Boolean
'
'Public RdbConnectionInformation As RdbConnectionInformationFlag
'
''///////////////////////////////////////////////
''/// Operation interfaces
''///////////////////////////////////////////////
''''
'''' No use ADODB.Connection.BeginTrans
''''
'Public Function GetRecordset(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag) As ADODB.Recordset: End Function
'
''''
'''' use ADODB.Connection.BeginTrans, the .CommitTrans is to be executed in CloseConnection() operation
''''
'Public Function ExecuteSQL(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag) As ADODB.Recordset: End Sub
'
''''
'''' If the ExecuteSQL method had been used at least one time, this execute ADODB.Connect.CommitTrans()
''''
'Public Sub CommitTransaction(): End Sub
'
''''
'''' If the ExecuteSQL method had been used at least one time, this execute CommitTransaction before execute ADODB.Connect.Close()
''''
'Public Sub CloseConnection(): End Sub
'''--VBA_Code_File--<ADOConnectingInClasses.bas>--
'Attribute VB_Name = "ADOConnectingInClasses"
''
''   ADO connection support-processes for ADOConnector.cls
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on both ADO and Windows OS
''       Dependent on ADOConnector.cls, CurrentUserDomainUtility.bas
''       Dependent on interfaces of IADOConnectStrGenerator.cls, IADOConnector.cls
''       Dependent on ErrorADOSQL.cls, ErrorContentKeeper.cls
''       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access.
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Fri, 12/May/2023    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Private WinAPI Definitions
''///////////////////////////////////////////////
'#If VBA7 Then
'    ' For 64 bit version windows, declare timeGetTime
'    Private Declare PtrSafe Function timeGetTime Lib "winmm.dll" () As Long
'#Else
'    ' For 32 bit version windows, declare timeGetTime
'    Private Declare Function timeGetTime Lib "winmm.dll" () As Long
'#End If
'
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''**---------------------------------------------
''** ADO connecting for various provider
''**---------------------------------------------
''''
''''
''''
'Public Sub TryToConnectRDBProviderByAdoConnectionString(ByRef rblnIsConnected As Boolean, ByRef ritfConnection As IADOConnector, ByRef ritfADOConnectStrGenerator As IADOConnectStrGenerator)
'
'    rblnIsConnected = False
'
'    If Not ritfConnection.IsConnected Then
'
'        InitializeTextForADOParametersBeforeADOSQLExecution
'
'        Set ritfConnection.ADOConnection = New ADODB.Connection
'
'        With ritfConnection.ADOConnection
'
'            .ConnectionString = ritfADOConnectStrGenerator.GetConnectionString ' strConnection
'
'            If ritfConnection.CommandTimeout > 0 And .CommandTimeout <> ritfConnection.CommandTimeout Then
'
'                ' Set the ADODB.Connection.CommandTimeout property
'
'                .CommandTimeout = ritfConnection.CommandTimeout
'            End If
'
'            .Open
'        End With
'
'        ritfConnection.IsConnected = True
'    End If
'
'    rblnIsConnected = ritfConnection.IsConnected
'End Sub
'
'
''**---------------------------------------------
''** ADO Recordset sheet expansion support tools
''**---------------------------------------------
''''
''''
''''
'Public Sub DetectRecordsetRecordsCountState(ByRef rblnSupportedPropertyRecordCount As Boolean, ByRef robjSQLResult As SQLResult, ByRef robjRSet As ADODB.Recordset)
'
'    If robjRSet.State <> ADODB.ObjectStateEnum.adStateClosed Then
'
'        If robjRSet.RecordCount = -1 Then
'
'            rblnSupportedPropertyRecordCount = False
'        Else
'            rblnSupportedPropertyRecordCount = True
'
'            If Not robjRSet.EOF Then
'
'                robjSQLResult.TableRecordCount = robjRSet.RecordCount
'            End If
'        End If
'    Else
'        rblnSupportedPropertyRecordCount = False
'    End If
'End Sub
'
''''
''''
''''
'Public Sub GetFieldCountsAndStartToMeasureTimeOfExpandingToSheet(ByRef robjSQLResult As SQLResult, _
'        ByRef robjRSet As ADODB.Recordset, _
'        ByRef rintExcelSheetOutputTime1 As Long)
'
'    If robjRSet.State <> ADODB.ObjectStateEnum.adStateClosed Then
'
'        robjSQLResult.TableFieldCount = robjRSet.Fields.Count
'    End If
'
'    rintExcelSheetOutputTime1 = timeGetTime()
'End Sub
'
''''
''''
''''
'Public Sub GetMeasureTimeOfExpandedRecordsetToSheet(ByRef robjSQLResult As SQLResult, _
'        ByRef rintExcelSheetOutputTime1 As Long, _
'        ByRef rintExcelSheetOutputTime2 As Long)
'
'
'    rintExcelSheetOutputTime2 = timeGetTime()
'
'    robjSQLResult.ExcelSheetOutputElapsedTime = rintExcelSheetOutputTime2 - rintExcelSheetOutputTime1
'End Sub
'
''**---------------------------------------------
''** logging around SQL execution
''**---------------------------------------------
''''
''''
''''
'''' <Argument>robjSQLResult: Output</Argument>
'''' <Argument>robjLoadedADOConnectionSetting: Output</Argument>
'''' <Argument>rintT1: Output</Argument>
'''' <Argument>rblnAllowToRecordSQLLog: Input</Argument>
'''' <Argument>rstrSQL: Input</Argument>
'Public Sub SetupSQLResultLogDataObjectAndLoadedConnectSettingDataObjectBeforeExecuteSQL(ByRef robjSQLResult As SQLResult, _
'        ByRef robjLoadedADOConnectionSetting As LoadedADOConnectionSetting, _
'        ByRef rintT1 As Long, _
'        ByRef rblnAllowToRecordSQLLog As Boolean, _
'        ByRef rstrSQL As String)
'
'
'    If rblnAllowToRecordSQLLog Then
'
'        Set robjSQLResult = New SQLResult
'
'        Set robjLoadedADOConnectionSetting = New LoadedADOConnectionSetting
'
'        robjSQLResult.SQL = rstrSQL
'
'        rintT1 = timeGetTime()
'    Else
'        Set robjSQLResult = Nothing
'
'        Set robjLoadedADOConnectionSetting = Nothing
'    End If
'End Sub
'
'
''''
''''
''''
'''' <Argument>robjSQLResult: Output</Argument>
'''' <Argument>robjLoadedADOConnectionSetting: Output</Argument>
'''' <Argument>rintT1: Input</Argument>
'''' <Argument>rintT2: Output</Argument>
'''' <Argument>rblnAllowToRecordSQLLog: Input</Argument>
'''' <Argument>robjConnection: Input</Argument>
'''' <Argument>robjADOConnectionSetting: Input</Argument>
'''' <Argument>robjRSet: Input</Argument>
'''' <Argument>vintRecordsAffected: Input</Argument>
'Public Sub SetupSQLResultLogDataObjectAndLoadedConnectSettingDataObjectAfterExecuteSQL(ByRef robjSQLResult As SQLResult, _
'        ByRef robjLoadedADOConnectionSetting As LoadedADOConnectionSetting, _
'        ByRef rintT1 As Long, _
'        ByRef rintT2 As Long, _
'        ByRef rblnAllowToRecordSQLLog As Boolean, _
'        ByRef robjConnection As ADODB.Connection, _
'        ByRef robjADOConnectionSetting As ADOConnectionSetting, _
'        ByRef robjRSet As ADODB.Recordset, _
'        Optional ByVal vintRecordsAffected As Long = -1)
'
'
'    LoggingTimesAndOthersAfterExecuteSQL robjSQLResult, rintT1, rintT2, rblnAllowToRecordSQLLog, vintRecordsAffected
'
'    LoggingLoadedADOConnectionSettingAfterExecuteSQL robjSQLResult, robjLoadedADOConnectionSetting, rblnAllowToRecordSQLLog, robjConnection, robjADOConnectionSetting, robjRSet
'End Sub
'
''''
''''
''''
'Public Sub LoggingTimesAndOthersAfterExecuteSQL(ByRef robjSQLResult As SQLResult, _
'        ByRef rintT1 As Long, _
'        ByRef rintT2 As Long, _
'        ByRef rblnAllowToRecordSQLLog As Boolean, _
'        Optional ByVal vintRecordsAffected As Long = -1)
'
'
'    If rblnAllowToRecordSQLLog Then
'
'        rintT2 = timeGetTime()
'
'        With robjSQLResult
'
'            .SQLExeTime = Now()
'
'            .SQLExeElapsedTime = rintT2 - rintT1
'
'            If vintRecordsAffected >= 0 Then
'
'                .RecordsAffected = vintRecordsAffected
'            End If
'        End With
'
'        KeepSQLResultToSharedCurrentUserDomain robjSQLResult
'    End If
'End Sub
'
'
''''
''''
''''
'Public Sub LoggingLoadedADOConnectionSettingAfterExecuteSQL(ByRef robjSQLResult As SQLResult, _
'        ByRef robjLoadedADOConnectionSetting As LoadedADOConnectionSetting, _
'        ByRef rblnAllowToRecordSQLLog As Boolean, _
'        ByRef robjConnection As ADODB.Connection, _
'        ByRef robjADOConnectionSetting As ADOConnectionSetting, _
'        ByRef robjRSet As ADODB.Recordset)
'
'
'    If rblnAllowToRecordSQLLog Then
'
'        robjLoadedADOConnectionSetting.SetLoadedParameters robjConnection, robjADOConnectionSetting, robjRSet
'
'        robjSQLResult.SetSQLOptionalLog robjLoadedADOConnectionSetting
'    End If
'End Sub
'
''**---------------------------------------------
''** SQL command logging, UPDATE, INSERT, DELETE
''**---------------------------------------------
''''
''''
''''
'Public Sub DebugLoggingOfSQLCommand(ByRef rstrSQL As String, ByRef robjSQLResult As SQLResult, ByRef rintRecordsAffected As Long)
'
'    Dim udtCurrent As Date, strCurrentDateTime As String
'
'
'    udtCurrent = robjSQLResult.SQLExeTime
'
'    strCurrentDateTime = FormatDateTime(udtCurrent, vbLongDate) & ", " & FormatDateTime(udtCurrent, vbLongTime)
'
'    Select Case GetSQLSyntaxType(rstrSQL)
'
'        Case RdbSqlClassification.RdbSqlDeleteType
'
'            Debug.Print GetTextOfStrKeyAdoParametersCountOfSqlDelete() & ": " & CStr(rintRecordsAffected) & " - " & strCurrentDateTime
'
'            Debug.Print GetTextOfStrKeyAdoParametersDeletingElapsedTime() & ": " & Format(CDbl(robjSQLResult.SQLExeElapsedTime) / 1000, "0.000") & " " & GetTextOfStrKeyAdoParametersUnitSecond()
'
'        Case RdbSqlClassification.RdbSqlUpdateType
'
'            Debug.Print GetTextOfStrKeyAdoParametersCountOfSqlUpdate() & ": "; CStr(rintRecordsAffected) & " - " & strCurrentDateTime
'
'            Debug.Print GetTextOfStrKeyAdoParametersUpdatingElapsedTime() & ": " & Format(CDbl(robjSQLResult.SQLExeElapsedTime) / 1000, "0.000") & " " & GetTextOfStrKeyAdoParametersUnitSecond()
'
'        Case RdbSqlClassification.RdbSqlInsertType
'
'            Debug.Print GetTextOfStrKeyAdoParametersCountOfSqlInsert() & ": " & CStr(rintRecordsAffected) & " - " & strCurrentDateTime
'
'            Debug.Print GetTextOfStrKeyAdoParametersInsertingElapsedTime() & ": " & Format(CDbl(robjSQLResult.SQLExeElapsedTime) / 1000, "0.000") & " " & GetTextOfStrKeyAdoParametersUnitSecond()
'    End Select
'End Sub
'
'
''**---------------------------------------------
''** ADO SQL error trapping
''**---------------------------------------------
''''
'''' Catch the SQL execution error
''''
'Public Sub HandleAdoSQlError(ByRef robjSQLResult As SQLResult, _
'        ByRef rintT1 As Long, _
'        ByRef rintT2 As Long, _
'        ByRef rblnAllowToRecordSQLLog As Boolean, _
'        ByRef robjLoadedADOConnectionSetting As LoadedADOConnectionSetting, _
'        ByRef robjConnection As ADODB.Connection, _
'        ByRef robjADOConnectionSetting As ADOConnectionSetting, _
'        ByVal vobjErrorContent As ErrorContentKeeper, _
'        ByRef renmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag, _
'        Optional ByVal vobjRecordset As ADODB.Recordset = Nothing)
'
'    Dim objErrorADOSQL As ErrorADOSQL
'
'
'    Debug.Print "[Err.Source] :" & vobjErrorContent.Source
'
'
'    LoggingTimesAndOthersAfterExecuteSQL robjSQLResult, rintT1, rintT2, rblnAllowToRecordSQLLog
'
'    Set objErrorADOSQL = GetErrorAdoSqlObject(vobjErrorContent, robjSQLResult, robjLoadedADOConnectionSetting, robjConnection, robjADOConnectionSetting, vobjRecordset)
'
'    If (renmShowUpAdoErrorOptionFlag And SuppressToShowUpAdoSqlErrorByExcelSheetFlag) = 0 Then
'
'        ShowErrorADOExecutedSQLExcelSheetIfExcelIsInstalled objErrorADOSQL, rblnAllowToRecordSQLLog
'    End If
'
'    If (renmShowUpAdoErrorOptionFlag And SuppressToShowUpAdoSqlErrorByFormFlag) = 0 Then
'
'        ShowErrorADOExecutedSQLForm objErrorADOSQL
'    End If
'End Sub
''''
''''
''''
'Public Function GetErrorAdoSqlObject(ByRef robjErrorContent As ErrorContentKeeper, _
'        ByRef robjSQLResult As SQLResult, _
'        ByRef robjLoadedADOConnectionSetting As LoadedADOConnectionSetting, _
'        ByRef robjConnection As ADODB.Connection, _
'        ByRef robjADOConnectionSetting As ADOConnectionSetting, _
'        ByRef robjRSet As ADODB.Recordset) As ErrorADOSQL
'
'
'    Dim objErrorADOSQL As ErrorADOSQL
'
'    Set objErrorADOSQL = New ErrorADOSQL
'
'    robjLoadedADOConnectionSetting.SetLoadedParameters robjConnection, robjADOConnectionSetting, robjRSet
'
'    objErrorADOSQL.SetLogWithLoadedConnectSetting robjErrorContent, robjSQLResult, robjLoadedADOConnectionSetting
'
'    Set GetErrorAdoSqlObject = objErrorADOSQL
'End Function
'
''''
'''' Show ADO SQL error contents by a Excel sheet, if the Excel is installed and the GenerateADOSQLExecutionErrorLogSheet operation module is loaded.
''''
'Public Sub ShowErrorADOExecutedSQLExcelSheetIfExcelIsInstalled(ByRef robjErrorADOSQL As ErrorADOSQL, ByRef rblnAllowToRecordSQLLog As Boolean)
'
'    If rblnAllowToRecordSQLLog Then
'
'        'GenerateADOSQLExecutionErrorLogSheet robjErrorADOSQL
'
'        On Error Resume Next
'
'        ' When this VB project doesn't include GenerateADOSQLExecutionErrorLogSheet operation, the following will be ignored.
'
'        ' One of merits is that doesn't cause any VBA compile errors although the Excel hadn't been installed.
'
'        CallBackInterfaceBySingleArguments "GenerateADOSQLExecutionErrorLogSheet", robjErrorADOSQL
'
'        On Error GoTo 0
'    End If
'End Sub
'
''''
'''' Show ADO SQL error contents by user-form
''''
'Public Sub ShowErrorADOExecutedSQLForm(ByRef robjErrorADOSQL As ErrorADOSQL, Optional ByVal vblnSQLExecuteAsynchronous As Boolean = False)
'
'    Dim objErrorADOSQLForm As UErrorADOSQLForm: Set objErrorADOSQLForm = New UErrorADOSQLForm
'
'    With objErrorADOSQLForm
'
'        .SetAndShowForm robjErrorADOSQL, vblnSQLExecuteAsynchronous
'
'        .Show
'    End With
'End Sub
'
'
'''--VBA_Code_File--<ADOConnector.cls>--
'VERSION 1.0 CLASS
'BEGIN
'  MultiUse = -1  'True
'END
'Attribute VB_Name = "ADOConnector"
'Attribute VB_GlobalNameSpace = False
'Attribute VB_Creatable = False
'Attribute VB_PredeclaredId = False
'Attribute VB_Exposed = False
''
''   ADODB.Connection class wrapper
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Development Policy:
''       This class focuses on the visualization of both the ADO error-trapping and SQL execution error-trapping
''       LozalizationADOConnector doesn't support the error trapping visualization , logging of SQL results, and localizing captions of itself
''
''   Dependency Abstract:
''       Dependent on ADO
''       Dependent on SQLGeneral.bas
''       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access.
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Wed,  8/Jun/2022    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Interface implementations
''///////////////////////////////////////////////
'Implements IADOConnector
'
''///////////////////////////////////////////////
''/// Declarations
''///////////////////////////////////////////////
'
'Private mobjConnect As ADODB.Connection
'
'Private mblnIsBeganTransaction As Boolean
'
'Private mobjConnectSetting As ADOConnectionSetting
'
'Private mblnIsConnected As Boolean
'
'Private mblnAllowToRecordSQLLog As Boolean
'
'Private mobjSQLRes As SQLResult ' Logging
'
'Private mobjLoadedConnectSetting As LoadedADOConnectionSetting
'
'
'Private mblnSuppressToShowUpSqlExecutionError As Boolean
'
''**---------------------------------------------
''** SQL Error handling state variables
''**---------------------------------------------
'Private mintT1 As Long  ' mili-second
'
'Private mintT2 As Long  ' mili-second
'
'Private mblnSQLFailed As Boolean
'
'
''///////////////////////////////////////////////
''/// Event handlers
''///////////////////////////////////////////////
'Private Sub Class_Initialize()
'
'    mblnIsConnected = False
'
'    mblnAllowToRecordSQLLog = True
'
'    Set mobjConnectSetting = New ADOConnectionSetting   ' connection starting parameters
'
'    Set mobjLoadedConnectSetting = Nothing  ' log-data
'
'    mblnIsBeganTransaction = False
'
'
'    mblnSQLFailed = False
'
'    mblnSuppressToShowUpSqlExecutionError = False
'End Sub
'
'Private Sub Class_Terminate()
'
'    IADOConnector_CloseConnection
'
'    Set mobjConnect = Nothing
'
'    Set mobjConnectSetting = Nothing
'
'    Set mobjLoadedConnectSetting = Nothing
'End Sub
'
'
''///////////////////////////////////////////////
''/// Implemented properties
''///////////////////////////////////////////////
'Private Property Let IADOConnector_IsConnected(ByVal vblnIsConnected As Boolean)
'
'    mblnIsConnected = vblnIsConnected
'End Property
'Private Property Get IADOConnector_IsConnected() As Boolean
'
'    IADOConnector_IsConnected = mblnIsConnected
'End Property
'
'Private Property Set IADOConnector_ADOConnection(ByVal vobjADOConnection As ADODB.Connection)
'
'    Set mobjConnect = vobjADOConnection
'End Property
'Private Property Get IADOConnector_ADOConnection() As ADODB.Connection
'
'    Set IADOConnector_ADOConnection = mobjConnect
'End Property
'
'Private Property Set IADOConnector_SQLExecutionResult(ByVal vobjSQLResult As SQLResult)
'
'    Set mobjSQLRes = vobjSQLResult
'End Property
'Private Property Get IADOConnector_SQLExecutionResult() As SQLResult
'
'    Set IADOConnector_SQLExecutionResult = mobjSQLRes
'End Property
'
''''
'''' unit is second. [s]
''''
'Private Property Let IADOConnector_CommandTimeout(ByVal vintTimeout As Long)
'
'    Me.CommandTimeout = vintTimeout
'End Property
'Private Property Get IADOConnector_CommandTimeout() As Long
'
'    IADOConnector_CommandTimeout = Me.CommandTimeout
'End Property
'
'
'Private Property Get IADOConnector_SuppressToShowUpSqlExecutionError() As Boolean
'
'    IADOConnector_SuppressToShowUpSqlExecutionError = mblnSuppressToShowUpSqlExecutionError
'End Property
'Private Property Let IADOConnector_SuppressToShowUpSqlExecutionError(ByVal vblnSuppressToShowUpSqlExecutionError As Boolean)
'
'    mblnSuppressToShowUpSqlExecutionError = vblnSuppressToShowUpSqlExecutionError
'End Property
'
'Private Property Get IADOConnector_RdbConnectionInformation() As RdbConnectionInformationFlag
'
'    ' The following is imperfect, the perfect implementation is written in each individual ADO connecting class including this class
'
'    IADOConnector_RdbConnectionInformation = RdbConnectionInformationFlag.UseDatabaseMsAdoObjectToConnectRdbFlag
'End Property
'Private Property Let IADOConnector_RdbConnectionInformation(ByVal venmRdbConnectionInformationFlag As RdbConnectionInformationFlag)
'
'    ' Nothing to do
'End Property
'
''///////////////////////////////////////////////
''/// Properties
''///////////////////////////////////////////////
'Public Property Let IsConnected(ByVal vblnIsConnected As Boolean)
'
'    IADOConnector_IsConnected = vblnIsConnected
'End Property
'Public Property Get IsConnected() As Boolean
'
'    IsConnected = IADOConnector_IsConnected
'End Property
'
'Public Property Let AllowToRecordSQLLog(ByVal vblnAllowToRecordSQLLog As Boolean)
'
'    mblnAllowToRecordSQLLog = vblnAllowToRecordSQLLog
'End Property
'Public Property Get AllowToRecordSQLLog() As Boolean
'
'    AllowToRecordSQLLog = mblnAllowToRecordSQLLog
'End Property
'
'
'Public Property Set ADOConnection(ByVal vobjADOConnection As ADODB.Connection)
'
'    Set IADOConnector_ADOConnection = vobjADOConnection
'End Property
'Public Property Get ADOConnection() As ADODB.Connection
'
'    Set ADOConnection = IADOConnector_ADOConnection
'End Property
'
'
'Public Property Set SQLExecutionResult(ByVal vobjSQLResult As SQLResult)
'
'    Set mobjSQLRes = vobjSQLResult
'End Property
'Public Property Get SQLExecutionResult() As SQLResult
'
'    Set SQLExecutionResult = mobjSQLRes
'End Property
'
'Public Property Get IsSqlExecutionFailed() As Boolean
'
'    IsSqlExecutionFailed = mblnSQLFailed
'End Property
'
''///////////////////////////////////////////////
''/// Properties - connoted ADOConnectionSetting
''///////////////////////////////////////////////
'Public Property Get ConnectSetting() As ADOConnectionSetting
'
'    Set ConnectSetting = mobjConnectSetting
'End Property
'
''''
'''' unit is second. [s]
''''
'Public Property Let CommandTimeout(ByVal vintTimeout As Long)
'
'    mobjConnectSetting.CommandTimeout = vintTimeout
'End Property
'Public Property Get CommandTimeout() As Long
'
'    CommandTimeout = mobjConnectSetting.CommandTimeout
'End Property
'
''''
'''' read-only loaded ConnectSetting()
''''
'Public Property Get LoadedConnectSetting() As LoadedADOConnectionSetting
'
'    Set LoadedConnectSetting = mobjLoadedConnectSetting
'End Property
'
'
''///////////////////////////////////////////////
''/// Implemented operations
''///////////////////////////////////////////////
''''
'''' get recordset object by the SELECT SQL
''''
'Private Function IADOConnector_GetRecordset(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag) As ADODB.Recordset
'
'    Dim objRSet As ADODB.Recordset
'
'
'    Set objRSet = Nothing
'
'    mblnSQLFailed = False
'
'    If mblnIsConnected Then
'
'        SetupSQLResultLogDataObjectAndLoadedConnectSettingDataObjectBeforeExecuteSQL mobjSQLRes, mobjLoadedConnectSetting, mintT1, mblnAllowToRecordSQLLog, vstrSQL
'
'        Set objRSet = New ADODB.Recordset
'
'        With objRSet
'
'            .Source = vstrSQL: .ActiveConnection = mobjConnect
'
'            If mobjConnect.State <> ADODB.ObjectStateEnum.adStateOpen Then
'
'                Debug.Print "Attention: current ADODB.Conection state - " & GetADOEnumLiteralFromObjectStateEnum(mobjConnect.State)
'            End If
'
'            On Error Resume Next
'
'            .Open
'
'            If Err.Number <> 0 Then
'
'                msubCreateErrorContentAndHandleItWithShowingErrorOptions Err, mblnSuppressToShowUpSqlExecutionError, venmShowUpAdoErrorOptionFlag, "<ADOConnector.GetRecordset>"
'
'                mblnSQLFailed = True: Err.Clear: On Error GoTo 0
'            End If
'
'            'msubConfirmCurrentConnectionConditionsByDebugPrint mobjConnect, objRSet
'
'            On Error GoTo 0
'
'        End With
'
'        If Not mblnSQLFailed And mblnAllowToRecordSQLLog Then
'
'            SetupSQLResultLogDataObjectAndLoadedConnectSettingDataObjectAfterExecuteSQL mobjSQLRes, mobjLoadedConnectSetting, mintT1, mintT2, mblnAllowToRecordSQLLog, mobjConnect, mobjConnectSetting, objRSet
'        End If
'    End If
'
'    Set IADOConnector_GetRecordset = objRSet
'End Function
'
''''
'''' input SQL, such as INSERT, UPDATE, DELETE
''''
'Private Function IADOConnector_ExecuteSQL(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag) As ADODB.Recordset
'
'    Dim intRecordsAffected As Long, udtCurrent As Date, strCurrentDateTime As String ', objErrorContent As ErrorContentKeeper
'    Dim objCommand As ADODB.Command, objRSet As ADODB.Recordset
'
'    Set objRSet = Nothing
'
'    If mblnIsConnected Then
'
'        mblnSQLFailed = False
'
'        With mobjConnect
'
'            SetupSQLResultLogDataObjectAndLoadedConnectSettingDataObjectBeforeExecuteSQL mobjSQLRes, mobjLoadedConnectSetting, mintT1, mblnAllowToRecordSQLLog, vstrSQL
'
'            If Not mblnIsBeganTransaction Then
'
'                .BeginTrans
'
'                mblnIsBeganTransaction = True
'            End If
'
'            intRecordsAffected = -1
'
'            On Error Resume Next
'
'            Set objRSet = .Execute(vstrSQL, intRecordsAffected)
'
'            If Err.Number <> 0 Then
'
'                msubCreateErrorContentAndHandleItWithShowingErrorOptions Err, mblnSuppressToShowUpSqlExecutionError, venmShowUpAdoErrorOptionFlag, "<ADOConnector.ExecuteSQL>"
'
'                mblnSQLFailed = True: Err.Clear: On Error GoTo 0
'            End If
'
'            'msubConfirmCurrentConnectionConditionsByDebugPrint mobjConnect, objRSet
'
'            On Error GoTo 0
'
'            If Me.AllowToRecordSQLLog Then
'
'                SetupSQLResultLogDataObjectAndLoadedConnectSettingDataObjectAfterExecuteSQL mobjSQLRes, mobjLoadedConnectSetting, mintT1, mintT2, mblnAllowToRecordSQLLog, mobjConnect, mobjConnectSetting, objRSet, intRecordsAffected
'
'                DebugLoggingOfSQLCommand vstrSQL, mobjSQLRes, intRecordsAffected
'            End If
'        End With
'    End If
'
'    Set IADOConnector_ExecuteSQL = objRSet
'End Function
'
''''
''''
''''
'Private Sub msubConfirmCurrentConnectionConditionsByDebugPrint(ByRef robjConnect As ADODB.Connection, ByRef robjRSet As ADODB.Recordset)
'
'    Dim objCommand As ADODB.Command
'
'    If Not robjConnect Is Nothing Then
'
'        With robjConnect
'
'            Debug.Print "ADODB.Connection CursorLocation : " & GetADOEnumLiteralFromCursorLocation(.CursorLocation)
'        End With
'    End If
'
'    If Not robjRSet Is Nothing Then
'
'        With robjRSet
'
'            If .State <> ADODB.ObjectStateEnum.adStateClosed Then
'
'                Debug.Print "Recordset CursorType : " & GetADOEnumLiteralFromRecordsetCursorType(.CursorType)
'
'                Debug.Print "Recordset LockType : " & GetADOEnumLiteralFromRecordsetLockType(.LockType)
'
'                If Not .ActiveCommand Is Nothing Then
'
'                    Set objCommand = .ActiveCommand
'
'                    Debug.Print "Recordset CommandType : " & GetADOEnumLiteralFromCommandType(objCommand.CommandType)
'                End If
'            End If
'        End With
'    End If
'End Sub
'
'
''''
'''' If the ExecuteSQL method had been used at least one time, this execute ADODB.Connect.CommitTrans()
''''
'Private Sub IADOConnector_CommitTransaction()
'
'    If Not mobjConnect Is Nothing Then
'
'        If mblnIsConnected Then
'
'            If mblnIsBeganTransaction Then
'
'                With mobjConnect
'
'                    If .State <> ADODB.ObjectStateEnum.adStateClosed Then
'
'                        .CommitTrans
'
'                        mblnIsBeganTransaction = False
'                    End If
'                End With
'            End If
'        End If
'    End If
'End Sub
'
''''
'''' If the ExecuteSQL method had been used at least one time, this execute CommitTransaction before execute ADODB.Connect.Close()
''''
'Private Sub IADOConnector_CloseConnection()
'
'    If Not mobjConnect Is Nothing Then
'
'        If mblnIsConnected Then
'
'            On Error Resume Next
'
'            IADOConnector_CommitTransaction
'
'            On Error GoTo 0
'
'            With mobjConnect
'
'                If .State <> ADODB.ObjectStateEnum.adStateClosed Then
'
'                    On Error Resume Next
'
'                    .Close
'
'                    On Error GoTo 0
'                End If
'            End With
'
'            mblnIsConnected = False
'        End If
'    End If
'End Sub
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''''
'''' get recordset object by the Select SQL
''''
'Public Function GetRecordset(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag) As ADODB.Recordset
'
'    Set GetRecordset = IADOConnector_GetRecordset(vstrSQL, venmShowUpAdoErrorOptionFlag)
'End Function
'
'
''''
'''' input SQL, such as INSERT, UPDATE, DELETE
''''
'Public Function ExecuteSQL(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag) As ADODB.Recordset
'
'    Set ExecuteSQL = IADOConnector_ExecuteSQL(vstrSQL, venmShowUpAdoErrorOptionFlag)
'End Function
'
''''
'''' Close ADO connection
''''
'Public Sub CloseConnection()
'
'    IADOConnector_CloseConnection
'End Sub
'
'
''///////////////////////////////////////////////
''/// Internal Functions
''///////////////////////////////////////////////
'
''''
''''
''''
'Private Sub msubCreateErrorContentAndHandleItWithShowingErrorOptions(ByRef robjErr As Object, _
'        ByVal vblnSuppressToShowUpSqlExecutionError As Boolean, _
'        ByRef renmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag, _
'        Optional ByVal vstrAdditionalMetaDataForSource As String = "")
'
'    If Not vblnSuppressToShowUpSqlExecutionError Then
'
'        If (renmShowUpAdoErrorOptionFlag And SuppressToShowUpAdoSqlErrorFlag) = 0 Then
'
'            msubCreateErrorContentAndHandleIt robjErr, renmShowUpAdoErrorOptionFlag, "<ADOConnector.ExecuteSQL>"
'        Else
'            If (renmShowUpAdoErrorOptionFlag And SuppressToShowUpAdoSqlErrorByDebugPrintFlag) = 0 Then
'
'                Debug.Print "ADODB.Connection error: " & robjErr.Description
'            End If
'        End If
'    Else
'        If (renmShowUpAdoErrorOptionFlag And SuppressToShowUpAdoSqlErrorByDebugPrintFlag) = 0 Then
'
'            Debug.Print "ADODB.Connection error: " & robjErr.Description
'        End If
'    End If
'End Sub
'
''''
''''
''''
'Private Sub msubCreateErrorContentAndHandleIt(ByRef robjErr As Object, _
'        ByRef renmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag, _
'        Optional ByVal vstrAdditionalMetaDataForSource As String = "")
'
'    Dim objErrorContent As ErrorContentKeeper
'
'    Set objErrorContent = New ErrorContentKeeper
'
'    objErrorContent.CopyFrom robjErr, vstrAdditionalMetaDataForSource
'
'    msubSQLErrorHandling objErrorContent, renmShowUpAdoErrorOptionFlag, Nothing
'End Sub
'
'
'
''''
'''' Catch the SQL execution error
''''
'Private Sub msubSQLErrorHandling(ByVal vobjErrorContent As ErrorContentKeeper, _
'        ByRef renmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag, _
'        Optional ByVal vobjRecordset As ADODB.Recordset = Nothing)
'
'    HandleAdoSQlError mobjSQLRes, mintT1, mintT2, mblnAllowToRecordSQLLog, mobjLoadedConnectSetting, mobjConnect, mobjConnectSetting, vobjErrorContent, renmShowUpAdoErrorOptionFlag, vobjRecordset
'End Sub
'
'
'
'''--VBA_Code_File--<DataTableADOIn.bas>--
'Attribute VB_Name = "DataTableADOIn"
''
''   get data-table collection object or dictionary object from ADODB.Recordset which either SQL query or SQL cammand is used without any SQL error.
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on ADO
''       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Thu,  4/May/2023    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''''
'''' return Collection(Of String) from Recordset instance after SQL SELECT
''''
'Public Function GetTableCollectionFromRSet(ByVal vobjRSet As ADODB.Recordset) As Collection
'
'    Dim objCol As Collection, objChildCol As Collection, intColumn As Long
'
'    Set objCol = New Collection
'
'    With vobjRSet
'
'        If .State <> ADODB.ObjectStateEnum.adStateClosed Then
'
'            If .Fields.Count = 1 Then
'
'                Do While Not .EOF
'
'                    objCol.Add .Fields.Item(0).Value
'
'                    .MoveNext
'                Loop
'            Else
'                Do While Not .EOF
'
'                    Set objChildCol = New Collection
'
'                    For intColumn = 0 To .Fields.Count - 1
'
'                        objChildCol.Add .Fields.Item(intColumn).Value
'                    Next
'
'                    objCol.Add objChildCol
'
'                    .MoveNext
'                Loop
'            End If
'        Else
'            Debug.Print "The ADODB.Recordset has been already closed at the GetTableCollectionFromRSet function."
'        End If
'    End With
'
'    Set GetTableCollectionFromRSet = objCol
'End Function
'
'
''''
'''' return Collection(Of Date) from Recordset instance after SQL SELECT
''''
'Public Function GetTableCollectionAsDateFromRSet(ByVal vobjRSet As ADODB.Recordset) As Collection
'
'    Dim objCol As Collection, objChildCol As Collection, intColumn As Long, varValue As Variant
'
'    Set objCol = New Collection
'
'    With vobjRSet
'
'        If .State <> ADODB.ObjectStateEnum.adStateClosed Then
'
'            If .Fields.Count = 1 Then
'
'                Do While Not .EOF
'
'                    varValue = .Fields.Item(0).Value
'
'                    If IsDate(varValue) Then
'
'                        objCol.Add CDate(varValue)
'                    Else
'
'                        objCol.Add varValue
'                    End If
'
'                    .MoveNext
'                Loop
'            Else
'                Do While Not .EOF
'
'                    Set objChildCol = New Collection
'
'                    For intColumn = 0 To .Fields.Count - 1
'
'                        varValue = .Fields.Item(intColumn).Value
'
'                        If IsDate(varValue) Then
'
'                            objChildCol.Add CDate(varValue)
'                        Else
'
'                            objChildCol.Add varValue
'                        End If
'                        'objChildCol.Add .Fields.Item(intColumn).Value
'                    Next
'
'                    objCol.Add objChildCol
'
'                    .MoveNext
'                Loop
'            End If
'        Else
'            Debug.Print "The ADODB.Recordset has been already closed at the GetTableCollectionAsDateFromRSet function."
'        End If
'    End With
'
'    Set GetTableCollectionAsDateFromRSet = objCol
'End Function
'
''''
''''
''''
'''' <Argument>vobjRSet: a Recordset instance after SQL SELECT execution</Argument>
'''' <Argument>vblnOmitDuplicatedKeys: Default is false, </Argument>
'''' <Return>Dictionary(Of String, String) from a Recordset instance after SQL SELECT execution</Return>
'Public Function GetTableDictionaryFromRSet(ByVal vobjRSet As ADODB.Recordset, Optional ByVal vblnOmitDuplicatedKeys As Boolean = False) As Scripting.Dictionary
'
'    Dim objDic As Scripting.Dictionary
'
'    Set objDic = New Scripting.Dictionary
'
'    StoreTableIntoDictionaryFromRSet objDic, vobjRSet, vblnOmitDuplicatedKeys
'
'    Set GetTableDictionaryFromRSet = objDic
'End Function
'
'
''''
''''
''''
'''' <Argument>robjDic: Output Dictionary(Of Key, Value) or Dictionary(Of Key, Collection(Of Value)) - key is 1st column value of Recordset</Argument>
'''' <Argument>vobjRSet: Input - a Recordset instance after SQL SELECT execution, which has to include more than one column</Argument>
'''' <Argument>vblnOmitDuplicatedKeys: Input - Default is false, </Argument>
'Public Sub StoreTableIntoDictionaryFromRSet(ByRef robjDic As Scripting.Dictionary, ByVal vobjRSet As ADODB.Recordset, Optional ByVal vblnOmitDuplicatedKeys As Boolean = False)
'
'    Dim objRowCol As Collection, intColumn As Long
'    Dim varKey As Variant
'
'    If robjDic Is Nothing Then Set robjDic = New Scripting.Dictionary
'
'    With vobjRSet
'
'        If .State <> ADODB.ObjectStateEnum.adStateClosed Then
'
'            If .Fields.Count > 2 Then
'
'                Do While Not .EOF
'
'                    Set objRowCol = New Collection
'
'                    For intColumn = 1 To .Fields.Count - 1
'
'                        objRowCol.Add .Fields.Item(intColumn).Value  ' From the second column on, these column values are stored to Collection
'                    Next
'
'                    If vblnOmitDuplicatedKeys Then
'
'                        With robjDic
'
'                            varKey = vobjRSet.Fields.Item(0)    ' The first column value is key
'
'                            If Not .Exists(varKey) Then
'
'                                .Add varKey, objRowCol
'                            End If
'                        End With
'                    Else
'                        robjDic.Add .Fields.Item(0).Value, objRowCol ' The first column value is key
'                    End If
'
'                    .MoveNext
'                Loop
'            Else
'                Do While Not .EOF
'
'                    If vblnOmitDuplicatedKeys Then
'
'                        With robjDic
'
'                            varKey = vobjRSet.Fields.Item(0)
'
'                            If Not .Exists(varKey) Then
'
'                                .Add varKey, vobjRSet.Fields.Item(1).Value
'                            End If
'                        End With
'                    Else
'                        robjDic.Add .Fields.Item(0).Value, .Fields.Item(1).Value
'                    End If
'
'                    .MoveNext
'                Loop
'            End If
'        Else
'            Debug.Print "The ADODB.Recordset has been already closed at the StoreTableIntoDictionaryFromRSet function."
'        End If
'    End With
'End Sub
'
'
''''
''''
''''
'''' <Argument>vobjRSet: a Recordset instance after SQL SELECT execution</Argument>
'''' <Argument>vblnOmitDuplicatedKeys: Default is false, </Argument>
'''' <Return>Dictionary(Of 1stKey, Dictionary(Of 2ndKey, Value)) or Dictionary(Of 1stKey, Dictionary(Of 2ndKey, Collection(Of Value))) from a Recordset instance after SQL SELECT execution</Return>
'Public Function GetTableOneNestedDictionaryFromRSet(ByVal vobjRSet As ADODB.Recordset, Optional ByVal vblnOmitDuplicatedKeys As Boolean = False) As Scripting.Dictionary
'
'    Dim objDic As Scripting.Dictionary
'
'    Set objDic = New Scripting.Dictionary
'
'    StoreTableIntoOneNestedDictionaryFromRSet objDic, vobjRSet, vblnOmitDuplicatedKeys
'
'    Set GetTableOneNestedDictionaryFromRSet = objDic
'End Function
'
'
''''
''''
''''
'''' <Argument>robjDic: Output Dictionary(Of 1stKey, Dictionary(Of 2ndKey, Value)) or Dictionary(Of 1stKey, Dictionary(Of 2ndKey, Collection(Of Value))) - key is 1st column value of Recordset</Argument>
'''' <Argument>vobjRSet: Input - a Recordset instance after SQL SELECT execution, which has to include more than one column</Argument>
'''' <Argument>vblnOmitDuplicatedKeys: Input - Default is false, </Argument>
'Public Sub StoreTableIntoOneNestedDictionaryFromRSet(ByRef robjDic As Scripting.Dictionary, ByVal vobjRSet As ADODB.Recordset, Optional ByVal vblnOmitAfterSecondDuplicatedKeys As Boolean = False)
'
'    Dim objChildDic As Scripting.Dictionary, objRowCol As Collection, intColumn As Long
'    Dim var1stKey As Variant, var2ndKey As Variant, varValue As Variant
'
'    If robjDic Is Nothing Then Set robjDic = New Scripting.Dictionary
'
'    With vobjRSet
'
'        If .State <> ADODB.ObjectStateEnum.adStateClosed Then
'
'            If .Fields.Count > 3 Then
'
'                Do While Not .EOF
'
'                    Set objRowCol = New Collection
'
'                    For intColumn = 2 To .Fields.Count - 1
'
'                        objRowCol.Add .Fields.Item(intColumn).Value  ' From the third column on, these column values are stored to Collection
'                    Next
'
'                    var1stKey = .Fields.Item(0)
'
'                    var2ndKey = .Fields.Item(1)
'
'                    With robjDic
'
'                        If Not .Exists(var1stKey) Then
'
'                            Set objChildDic = New Scripting.Dictionary
'
'                            .Add var1stKey, objChildDic
'                        Else
'
'                            Set objChildDic = .Item(var1stKey)
'                        End If
'                    End With
'
'                    If vblnOmitAfterSecondDuplicatedKeys Then
'
'                        With objChildDic
'
'                            If Not .Exists(var2ndKey) Then
'
'                                .Add var2ndKey, objRowCol
'                            End If
'                        End With
'                    Else
'                        objChildDic.Add var2ndKey, objRowCol ' The first column value is key
'                    End If
'
'                    .MoveNext
'                Loop
'            Else
'                Do While Not .EOF
'
'                    var1stKey = .Fields.Item(0)
'
'                    var2ndKey = .Fields.Item(1)
'
'                    varValue = .Fields.Item(2)
'
'                    With robjDic
'
'                        If Not .Exists(var1stKey) Then
'
'                            Set objChildDic = New Scripting.Dictionary
'
'                            .Add var1stKey, objChildDic
'                        Else
'
'                            Set objChildDic = .Item(var1stKey)
'                        End If
'                    End With
'
'                    If vblnOmitAfterSecondDuplicatedKeys Then
'
'                        With objChildDic
'
'                            If Not .Exists(var2ndKey) Then
'
'                                .Add var2ndKey, varValue
'                            End If
'                        End With
'                    Else
'                        objChildDic.Add var2ndKey, varValue
'                    End If
'
'                    .MoveNext
'                Loop
'            End If
'        Else
'            Debug.Print "The ADODB.Recordset has been already closed at the StoreTableIntoOneNestedDictionaryFromRSet function."
'        End If
'    End With
'End Sub
'
'
''''
''''
''''
'''' <Argument>vobjRSet: a Recordset instance after SQL SELECT execution</Argument>
'''' <Argument>vblnOmitDuplicatedKeys: Default is false, </Argument>
'''' <Return>Dictionary(Of 1stKey, Dictionary(Of 2ndKey, Value)) or Dictionary(Of 1stKey, Dictionary(Of 2ndKey, Collection(Of Value))) from a Recordset instance after SQL SELECT execution</Return>
'Public Function GetTableOneNestedDictionaryFromRSetWithSpecialConversion(ByVal vobjRSet As ADODB.Recordset, Optional ByVal vblnOmitDuplicatedKeys As Boolean = False) As Scripting.Dictionary
'
'    Dim objDic As Scripting.Dictionary
'
'    Set objDic = New Scripting.Dictionary
'
'    StoreTableIntoOneNestedDictionaryFromRSetWithSpecialConversion objDic, vobjRSet, vblnOmitDuplicatedKeys
'
'    Set GetTableOneNestedDictionaryFromRSetWithSpecialConversion = objDic
'End Function
'
'
''''
''''
''''
'''' <Argument>robjDic: Output Dictionary(Of 1stKey, Dictionary(Of 2ndKey, Value)) or Dictionary(Of 1stKey, Dictionary(Of 2ndKey, Collection(Of Value))) - key is 1st column value of Recordset</Argument>
'''' <Argument>vobjRSet: Input - a Recordset instance after SQL SELECT execution, which has to include more than one column</Argument>
'''' <Argument>vblnOmitDuplicatedKeys: Input - Default is false, </Argument>
'Public Sub StoreTableIntoOneNestedDictionaryFromRSetWithSpecialConversion(ByRef robjDic As Scripting.Dictionary, _
'        ByVal vobjRSet As ADODB.Recordset, _
'        Optional ByVal vblnOmitAfterSecondDuplicatedKeys As Boolean = False)
'
'    Dim objChildDic As Scripting.Dictionary, objRowCol As Collection, intColumn As Long
'    Dim var1stValue As Variant, var2ndKey As Variant, varValue As Variant
'
'    Dim strCurrent1stKey As Variant
'
'    If robjDic Is Nothing Then Set robjDic = New Scripting.Dictionary
'
'    strCurrent1stKey = ""
'
'    With vobjRSet
'
'        If .State <> ADODB.ObjectStateEnum.adStateClosed Then
'
'            If .Fields.Count > 3 Then
'
'                Do While Not .EOF
'
'                    Set objRowCol = New Collection
'
'                    For intColumn = 2 To .Fields.Count - 1
'
'                        objRowCol.Add .Fields.Item(intColumn).Value  ' From the third column on, these column values are stored to Collection
'                    Next
'
'                    var1stValue = .Fields.Item(0)
'
'                    If var1stValue <> "" Then
'
'                        strCurrent1stKey = var1stValue
'                    End If
'
'                    var2ndKey = .Fields.Item(1)
'
'                    With robjDic
'
'                        If Not .Exists(strCurrent1stKey) Then
'
'                            Set objChildDic = New Scripting.Dictionary
'
'                            .Add strCurrent1stKey, objChildDic
'                        Else
'
'                            Set objChildDic = .Item(strCurrent1stKey)
'                        End If
'                    End With
'
'                    If vblnOmitAfterSecondDuplicatedKeys Then
'
'                        With objChildDic
'
'                            If Not .Exists(var2ndKey) Then
'
'                                .Add var2ndKey, objRowCol
'                            End If
'                        End With
'                    Else
'                        objChildDic.Add var2ndKey, objRowCol ' The first column value is key
'                    End If
'
'                    .MoveNext
'                Loop
'            Else
'                Do While Not .EOF
'
'                    var1stValue = .Fields.Item(0)
'
'                    If var1stValue <> "" Then
'
'                        strCurrent1stKey = var1stValue
'                    End If
'
'                    var2ndKey = .Fields.Item(1)
'
'                    varValue = .Fields.Item(2)
'
'                    With robjDic
'
'                        If Not .Exists(strCurrent1stKey) Then
'
'                            Set objChildDic = New Scripting.Dictionary
'
'                            .Add strCurrent1stKey, objChildDic
'                        Else
'                            Set objChildDic = .Item(strCurrent1stKey)
'                        End If
'                    End With
'
'                    If vblnOmitAfterSecondDuplicatedKeys Then
'
'                        With objChildDic
'
'                            If Not .Exists(var2ndKey) Then
'
'                                .Add var2ndKey, varValue
'                            End If
'                        End With
'                    Else
'                        objChildDic.Add var2ndKey, varValue
'                    End If
'
'                    .MoveNext
'                Loop
'            End If
'        Else
'            Debug.Print "The ADODB.Recordset has been already closed at the StoreTableIntoOneNestedDictionaryFromRSetWithSpecialConversion function."
'        End If
'    End With
'End Sub
'
'
'
'
''''
'''' return Dictionary(Of String, Boolean) from Recordset instance after SQL SELECT
''''
'''' Not 0 (for example 1) to True, 0 to False
''''
'Public Function GetTableDictionaryAsValuesAreBooleanFromRSet(ByVal vobjRSet As ADODB.Recordset, Optional ByVal vblnOmitDuplicatedKeys As Boolean = False) As Scripting.Dictionary
'
'    Dim objDic As Scripting.Dictionary, objRowCol As Collection, intColumn As Long
'    Dim varKey As Variant, blnValue As Boolean
'
'    Set objDic = New Scripting.Dictionary
'
'    With vobjRSet
'
'        If .State <> ADODB.ObjectStateEnum.adStateClosed Then
'
'            If .Fields.Count > 2 Then
'
'                Do While Not .EOF
'
'                    Set objRowCol = New Collection
'
'                    For intColumn = 1 To .Fields.Count - 1
'
'                        blnValue = False
'
'                        If Not IsObject(.Fields.Item(intColumn).Value) Then
'
'                            If IsNumeric(.Fields.Item(intColumn).Value) Then
'
'                                If .Fields.Item(intColumn).Value <> 0 Then
'
'                                    blnValue = True
'                                End If
'                            End If
'                        End If
'
'                        objRowCol.Add blnValue
'                    Next
'
'                    If vblnOmitDuplicatedKeys Then
'
'                        With objDic
'
'                            varKey = vobjRSet.Fields.Item(0)
'
'                            If Not .Exists(varKey) Then
'
'                                .Add varKey, objRowCol
'                            End If
'                        End With
'                    Else
'                        objDic.Add .Fields.Item(0).Value, objRowCol
'                    End If
'
'                    .MoveNext
'                Loop
'            Else
'                Do While Not .EOF
'
'                    If vblnOmitDuplicatedKeys Then
'
'                        With objDic
'
'                            varKey = vobjRSet.Fields.Item(0)
'
'                            If Not .Exists(varKey) Then
'
'                                blnValue = False
'
'                                If Not IsObject(.Fields.Item(1).Value) Then
'
'                                    If IsNumeric(.Fields.Item(1).Value) Then
'
'                                        If .Fields.Item(1).Value <> 0 Then
'                                            blnValue = True
'                                        End If
'
'                                    End If
'                                End If
'
'                                .Add varKey, blnValue
'
'                            End If
'                        End With
'                    Else
'                        blnValue = False
'
'                        If Not IsObject(.Fields.Item(1).Value) Then
'
'                            If IsNumeric(.Fields.Item(1).Value) Then
'
'                                If .Fields.Item(1).Value <> 0 Then
'
'                                    blnValue = True
'                                End If
'
'                            End If
'                        End If
'
'                        objDic.Add .Fields.Item(0).Value, blnValue
'                    End If
'
'                    .MoveNext
'                Loop
'            End If
'        Else
'            Debug.Print "The ADODB.Recordset has been already closed at the GetTableDictionaryAsValuesAreBooleanFromRSet function."
'        End If
'    End With
'
'    Set GetTableDictionaryFromRSet = objDic
'End Function
'
'
''''
''''
''''
'Public Sub GetTableDictionaryAndRemainedCollectionFromRSet(ByRef robjDTDic As Scripting.Dictionary, ByRef robjRemainedDTCol As Collection, ByVal vobjRSet As ADODB.Recordset, Optional ByVal vblnAddKeyValueForFirstItemAboutMoreThanOneItem As Boolean = False)
'
'    Dim objKeyToColDic As Scripting.Dictionary, objValuesCol As Collection
'    Dim objRowCol As Collection, intColumn As Long
'    Dim varKey As Variant, varItem As Variant, objChildCol As Collection, varRowItem As Variant
'
'    Set objKeyToColDic = New Scripting.Dictionary
'
'    ' serve the index 0 of Recordset.Fields.Item as dictionary key first key to values col
'    With vobjRSet
'
'        If .State <> ADODB.ObjectStateEnum.adStateClosed Then
'
'            If .Fields.Count > 2 Then
'
'                Do While Not .EOF
'
'                    varKey = .Fields.Item(0).Value  ' The first column is key
'
'                    Set objRowCol = New Collection
'
'                    For intColumn = 1 To .Fields.Count - 1
'
'                        objRowCol.Add .Fields.Item(intColumn).Value ' From the second column on, these column are stored to Collection
'                    Next
'
'                    With objKeyToColDic
'
'                        If Not .Exists(varKey) Then
'
'                            Set objValuesCol = New Collection
'
'                            objValuesCol.Add objRowCol
'
'                            .Add varKey, objValuesCol
'                        Else
'                            Set objValuesCol = .Item(varKey)
'
'                            objValuesCol.Add objRowCol
'
'                            Set .Item(varKey) = objValuesCol
'                        End If
'                    End With
'
'                    .MoveNext
'                Loop
'            Else
'                Do While Not .EOF
'
'                    varKey = .Fields.Item(0).Value  ' The first column is key
'                    varItem = .Fields.Item(1).Value ' The second column is value
'
'                    With objKeyToColDic
'
'                        If Not .Exists(varKey) Then
'
'                            Set objValuesCol = New Collection
'
'                            objValuesCol.Add varItem
'
'                            .Add varKey, objValuesCol
'                        Else
'                            Set objValuesCol = .Item(varKey)
'
'                            objValuesCol.Add varItem
'
'                            Set .Item(varKey) = objValuesCol
'                        End If
'
'                    End With
'
'                    .MoveNext
'                Loop
'            End If
'        Else
'            Debug.Print "The ADODB.Recordset has been already closed at the GetTableDictionaryAndRemainedCollectionFromRSet function."
'        End If
'    End With
'
'
'    Set robjDTDic = New Scripting.Dictionary
'
'    Set robjRemainedDTCol = New Collection
'
'    ' separate the first key values-col for a main dictionay and a remained col
'    With objKeyToColDic
'
'        For Each varKey In .Keys
'
'            Set objValuesCol = .Item(varKey)
'
'            If objValuesCol.Count = 1 Then
'
'                If IsObject(objValuesCol.Item(1)) Then
'
'                    Set varItem = objValuesCol.Item(1)
'                Else
'                    varItem = objValuesCol.Item(1)
'                End If
'
'                robjDTDic.Add varKey, varItem
'            Else
'                For Each varItem In objValuesCol
'
'                    Set objChildCol = New Collection
'
'                    objChildCol.Add varKey
'
'                    If IsObject(varItem) Then
'
'                        Set objRowCol = varItem
'
'                        For Each varRowItem In objRowCol
'
'                            objChildCol.Add varRowItem
'                        Next
'                    Else
'                        objChildCol.Add varRowItem
'                    End If
'
'                    robjRemainedDTCol.Add objChildCol
'                Next
'
'                If vblnAddKeyValueForFirstItemAboutMoreThanOneItem Then
'
'                    If Not robjDTDic.Exists(varKey) Then
'
'                        robjDTDic.Add varKey, objValuesCol.Item(1)
'                    End If
'                End If
'            End If
'        Next
'    End With
'End Sub
'
'
'''--VBA_Code_File--<UserFormToolsForAdoConStr.bas>--
'Attribute VB_Name = "UserFormToolsForAdoConStr"
''
''   User form support functions for setting ADO Connection string parameters form
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on ADO
''       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Thu, 20/Jul/2023    Kalmclaeyd Tarclanus    Initial creation
''
'
'Option Explicit
'
'
''///////////////////////////////////////////////
''/// Constants
''///////////////////////////////////////////////
'Private Const mstrModuleName As String = "UserFormToolsForAdoConStr"
'
'
''///////////////////////////////////////////////
''/// Declarations
''///////////////////////////////////////////////
'
'
'
''**---------------------------------------------
''** Key-Value cache preparation for UserFormToolsForAdoConStr
''**---------------------------------------------
'Private mobjStrKeyValueUserFormToolsForAdoConStrDic As Scripting.Dictionary
'
'Private mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized As Boolean   ' Uninitialized boolean value is false in VBA specification.
'
'
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''**---------------------------------------------
''** About Ms Access OLE DB ComboBox
''**---------------------------------------------
''''
''''
''''
'Public Function InitializeCombBoxOfAceOleDb(ByRef rcboUsingAccdbOleDb As MSForms.ComboBox) As Scripting.Dictionary
'
'    Dim varAccessOLEDBProviderType As Variant, enmAccessOLEDBProviderType As AccessOLEDBProviderType, objEnumValueToListIndexDic As Scripting.Dictionary, i As Long
'
'    With rcboUsingAccdbOleDb
'
'        .Clear
'
'        Set objEnumValueToListIndexDic = New Scripting.Dictionary
'
'        i = 0
'
'        For Each varAccessOLEDBProviderType In GetAccessOLEDBProviderTypeEnums()
'
'            enmAccessOLEDBProviderType = varAccessOLEDBProviderType
'
'            .AddItem GetAccessOLEDBProviderNameFromEnm(enmAccessOLEDBProviderType)
'
'            objEnumValueToListIndexDic.Add enmAccessOLEDBProviderType, i
'
'            i = i + 1
'        Next
'
'        .ListIndex = objEnumValueToListIndexDic.Item(AccessOLEDBProviderType.AceOLEDB_12P0)
'    End With
'
'    Set InitializeCombBoxOfAceOleDb = objEnumValueToListIndexDic
'End Function
'
''''
''''
''''
'Public Function InitializeCombBoxOfAdoIMEXMode(ByRef rcboIMEXMode As MSForms.ComboBox) As Scripting.Dictionary
'
'    Dim varIMEXMode As Variant, enmIMEXMode As IMEXMode, objEnumValueToListIndexDic As Scripting.Dictionary, i As Long
'
'    With rcboIMEXMode
'
'        .Clear
'
'        Set objEnumValueToListIndexDic = New Scripting.Dictionary
'
'        i = 0
'
'        For Each varIMEXMode In GetAdoIMEXModeEnums()
'
'            enmIMEXMode = varIMEXMode
'
'            .AddItem GetAdoIMEXModeNameFromEnm(enmIMEXMode)
'
'            objEnumValueToListIndexDic.Add enmIMEXMode, i
'
'            i = i + 1
'        Next
'
'        .ListIndex = objEnumValueToListIndexDic.Item(IMEXMode.NoUseIMEXMode)
'    End With
'
'    Set InitializeCombBoxOfAdoIMEXMode = objEnumValueToListIndexDic
'End Function
'
'
''**---------------------------------------------
''** About initializing UI
''**---------------------------------------------
''''
'''' For PostgreSQL, Oracle
''''
'Public Sub InitializeSavePasswordTemporaryCacheCheckBox(ByRef rchkSavePasswordTemporaryCache As MSForms.CheckBox)
'
'    If IsSaveAdoConStrPasswordInTemporaryCacheFunctionEnabled() Then
'
'        rchkSavePasswordTemporaryCache.Value = GetAllowToSaveAdoConStrPasswordInTemporaryCache()
'    Else
'        With rchkSavePasswordTemporaryCache
'
'            .Value = False
'
'            .Enabled = False
'        End With
'    End If
'End Sub
'
''''
'''' For Ms Access
''''
'Public Sub InitializeSavePasswordTemporaryCacheCheckBoxForMsAccess(ByRef rchkSavePasswordTemporaryCache As MSForms.CheckBox)
'
'    If IsSaveMsAccessDbLockFilePasswordInTemporaryCacheFunctionEnabled() Then
'
'        rchkSavePasswordTemporaryCache.Value = GetAllowToSaveAdoConStrPasswordInTemporaryCache()
'    Else
'        With rchkSavePasswordTemporaryCache
'
'            .Value = False
'
'            .Enabled = False
'        End With
'    End If
'End Sub
'
''''
'''' For Ms Excel
''''
'Public Sub InitializeSavePasswordTemporaryCacheCheckBoxForMsExcel(ByRef rchkSavePasswordTemporaryCache As MSForms.CheckBox)
'
'    If IsSaveMsExcelLockFilePasswordInTemporaryCacheFunctionEnabled() Then
'
'        rchkSavePasswordTemporaryCache.Value = GetAllowToSaveAdoConStrPasswordInTemporaryCache()
'    Else
'        With rchkSavePasswordTemporaryCache
'
'            .Value = False
'
'            .Enabled = False
'        End With
'    End If
'End Sub
'
''''
''''
''''
'Public Sub InitializeFixedSettingNameModeForEachRDB(ByRef robjSettingKeyNameToUserInputParametersDic As Scripting.Dictionary, _
'        ByRef rcboConStrCacheName As MSForms.ComboBox, _
'        ByRef robjFormStateToSetAdoConStr As FormStateToSetAdoConStr, _
'        ByVal venmOleDbOrOdbcAdoConnectionDestinationRDBType As OleDbOrOdbcAdoConnectionDestinationRDBType, _
'        ByRef rblnFixedSettingNameMode As Boolean, _
'        ByRef rcmdOK As MSForms.CommandButton)
'
'
'    If rblnFixedSettingNameMode Then
'
'        With rcboConStrCacheName
'
'            .AddItem robjFormStateToSetAdoConStr.SettingKeyName
'
'            .ListIndex = 0
'
'            .Locked = True
'        End With
'    Else
'        SetupAndInitializeCboSettingKeyNameUI robjSettingKeyNameToUserInputParametersDic, rcboConStrCacheName, robjFormStateToSetAdoConStr, venmOleDbOrOdbcAdoConnectionDestinationRDBType
'
'        If rcboConStrCacheName.ListCount > 0 Or rcboConStrCacheName.Text <> "" Then
'
'            rcmdOK.Enabled = True
'        Else
'            rcmdOK.Enabled = False
'        End If
'    End If
'End Sub
'
'
'
'
''''
''''
''''
'''' <Argument>robjSettingKeyNameToUserInputParametersDic: Output</Argument>
'''' <Argument>rcboConStrCacheName: Output</Argument>
'''' <Argument>robjFormStateToSetAdoConStr: Input</Argument>
'''' <Argument>venmOleDbOrOdbcAdoConnectionDestinationRDBType: Input</Argument>
'Public Sub SetupAndInitializeCboSettingKeyNameUI(ByRef robjSettingKeyNameToUserInputParametersDic As Scripting.Dictionary, _
'        ByRef rcboConStrCacheName As MSForms.ComboBox, _
'        ByRef robjFormStateToSetAdoConStr As FormStateToSetAdoConStr, _
'        ByVal venmOleDbOrOdbcAdoConnectionDestinationRDBType As OleDbOrOdbcAdoConnectionDestinationRDBType)
'
'
'    Dim varSettingKeyName As Variant
'
'    If robjSettingKeyNameToUserInputParametersDic Is Nothing Then
'
'        Set robjSettingKeyNameToUserInputParametersDic = GetFilteredOneNestedDicOfAllAdoConnectionStringInfoFromReadingWinRegChiledKeysAndValuesOfEachKey(venmOleDbOrOdbcAdoConnectionDestinationRDBType)
'    End If
'
'    If robjFormStateToSetAdoConStr.SettingKeyName <> "" Then
'
'        With robjSettingKeyNameToUserInputParametersDic
'
'            If Not .Exists(robjFormStateToSetAdoConStr.SettingKeyName) Then
'
'                .Add robjFormStateToSetAdoConStr.SettingKeyName, robjFormStateToSetAdoConStr.UserInputParametersDic
'            End If
'        End With
'    End If
'
'    SortDictionaryAboutKeys robjSettingKeyNameToUserInputParametersDic
'
'
'    With rcboConStrCacheName
'
'        .Text = ""
'
'        .Clear
'    End With
'
'    With robjSettingKeyNameToUserInputParametersDic
'
'        For Each varSettingKeyName In .Keys
'
'            With rcboConStrCacheName
'
'                .AddItem varSettingKeyName
'            End With
'        Next
'
'        With rcboConStrCacheName
'
'            If .ListCount > 0 Then
'
'                If robjFormStateToSetAdoConStr.SettingKeyName <> "" Then
'
'                    .Value = robjFormStateToSetAdoConStr.SettingKeyName
'                Else
'                    .ListIndex = 0
'                End If
'            End If
'        End With
'    End With
'End Sub
'
''''
''''
''''
'''' <Argument>rcboControl: Input</Argument>
'''' <Argument>robjSettingKeyNameToUserInputParametersDic: Input - one-nested Dictionary(Of String, Dictionary(Of String, Variant))</Argument>
'''' <Return>Dictionary(Of String, Variant)</Return>
'Public Function GetCurrentUserInputParametersDicFromComboBoxAndCacheDic(ByRef rcboControl As MSForms.ComboBox, _
'        ByRef robjSettingKeyNameToUserInputParametersDic As Scripting.Dictionary) As Scripting.Dictionary
'
'
'    Dim strSettingKeyName As String, objCurrentUserInputParametersDic As Scripting.Dictionary
'
'    strSettingKeyName = GetCurrentValueOfComboBoxWithTextEditable(rcboControl)
'
'    With robjSettingKeyNameToUserInputParametersDic
'
'        If .Exists(strSettingKeyName) Then
'
'            Set objCurrentUserInputParametersDic = .Item(strSettingKeyName)
'        Else
'            Set objCurrentUserInputParametersDic = New Scripting.Dictionary
'        End If
'    End With
'
'    Set GetCurrentUserInputParametersDicFromComboBoxAndCacheDic = objCurrentUserInputParametersDic
'End Function
'
'
''''
''''
''''
'Public Function GetCurrentValueOfComboBoxWithTextEditable(ByRef rcboControl As MSForms.ComboBox) As String
'
'    Dim strValue As String
'
'    strValue = ""
'
'    With rcboControl
'
'        If .ListCount > 0 Then
'
'            strValue = .List(.ListIndex)
'
'        ElseIf .Text <> "" Then
'
'            strValue = .Text
'        End If
'    End With
'
'    GetCurrentValueOfComboBoxWithTextEditable = strValue
'End Function
'
'
'
'
''**---------------------------------------------
''** About UI localization
''**---------------------------------------------
''''
''''
''''
'Public Sub LocalizeUIControlOfAdoConStrInfoForAdoConStrLblConStrCacheName(ByRef robjAdoSettingCacheName As MSForms.Label, ByRef robjAdoConnectionStringFrame As MSForms.Frame)
'
'    robjAdoSettingCacheName.Caption = GetTextOfStrKeyUserFormToolsForAdoConStrLblConStrCacheName()
'
'    robjAdoConnectionStringFrame.Caption = GetTextOfStrKeyUserFormToolsForAdoConStrFraAdoConnectionParameters()
'End Sub
'
''''
''''
''''
'Public Sub LocalizeUIControlOfAdoConStrInfoForAdoConStrFraDsnParameters(ByRef robjEnablingDsnFrame As MSForms.Frame, _
'        ByRef robjUseDsnOption As MSForms.OptionButton, _
'        ByRef robjNoUseDsnOption As MSForms.OptionButton, _
'        ByRef robjUseDsnFrame As MSForms.Frame, _
'        ByRef robjLabelOfDSN As MSForms.Label)
'
'    robjEnablingDsnFrame.Caption = GetTextOfStrKeyUserFormToolsForAdoConStrFraDsnType()
'
'    robjUseDsnOption.Caption = GetTextOfStrKeyUserFormToolsForAdoConStrOptUseDsn()
'
'    robjNoUseDsnOption.Caption = GetTextOfStrKeyUserFormToolsForAdoConStrOptDsnless()
'
'    robjUseDsnFrame.Caption = GetTextOfStrKeyUserFormToolsForAdoConStrFraDsnParameters()
'
'    robjLabelOfDSN.Caption = GetTextOfStrKeyUserFormToolsForAdoConStrLblDataSourceName()
'End Sub
'
'
''''
''''
''''
'Public Sub LocalizeUIControlOfAdoConStrInfoForAdoConStrFraConnectionStringInfoCachingOption(ByRef robjFrame As MSForms.Frame, _
'        ByRef robjCheckBoxOfSaveParametersInRegistoryExceptForPassword As MSForms.CheckBox, _
'        ByRef robjCheckBoxOfSavePasswordTemporaryCache As MSForms.CheckBox)
'
'
'    robjCheckBoxOfSavePasswordTemporaryCache.Caption = GetTextOfStrKeyUserFormToolsForAdoConStrChkSavePasswordTemporaryCache()
'
'    LocalizeUIControlOfAdoConStrInfoForSaveParametersInRegistoryExceptForPassword robjFrame, robjCheckBoxOfSaveParametersInRegistoryExceptForPassword
'End Sub
'
'
''''
''''
''''
'Public Sub LocalizeUIControlOfAdoConStrInfoForSaveParametersInRegistoryExceptForPassword(ByRef robjFrame As MSForms.Frame, _
'        ByRef robjCheckBoxOfSaveParametersInRegistoryExceptForPassword As MSForms.CheckBox)
'
'    robjFrame.Caption = GetTextOfStrKeyUserFormToolsForAdoConStrFraConnectionStringInfoCachingOption()
'
'    robjCheckBoxOfSaveParametersInRegistoryExceptForPassword.Caption = GetTextOfStrKeyUserFormToolsForAdoConStrChkSaveParametersInRegistoryExceptForPassword()
'End Sub
'
'
'
''**---------------------------------------------
''** Key-Value cache preparation for UserFormToolsForAdoConStr
''**---------------------------------------------
''''
'''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_ODBC_POSTGRESQL_FORM_TITLE
''''
'''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
'Public Function GetTextOfStrKeyUserFormToolsForAdoConStrOdbcPostgresqlFormTitle() As String
'
'    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then
'
'        msubInitializeTextForUserFormToolsForAdoConStr
'    End If
'
'    GetTextOfStrKeyUserFormToolsForAdoConStrOdbcPostgresqlFormTitle = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_ODBC_POSTGRESQL_FORM_TITLE")
'End Function
'
''''
'''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_ODBC_ORACLE_FORM_TITLE
''''
'''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
'Public Function GetTextOfStrKeyUserFormToolsForAdoConStrOdbcOracleFormTitle() As String
'
'    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then
'
'        msubInitializeTextForUserFormToolsForAdoConStr
'    End If
'
'    GetTextOfStrKeyUserFormToolsForAdoConStrOdbcOracleFormTitle = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_ODBC_ORACLE_FORM_TITLE")
'End Function
'
''''
'''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_ODBC_SQLITE3_FORM_TITLE
''''
'''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
'Public Function GetTextOfStrKeyUserFormToolsForAdoConStrOdbcSqlite3FormTitle() As String
'
'    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then
'
'        msubInitializeTextForUserFormToolsForAdoConStr
'    End If
'
'    GetTextOfStrKeyUserFormToolsForAdoConStrOdbcSqlite3FormTitle = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_ODBC_SQLITE3_FORM_TITLE")
'End Function
'
''''
'''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_OLEDB_MS_ACCESS_FORM_TITLE
''''
'''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
'Public Function GetTextOfStrKeyUserFormToolsForAdoConStrOledbMsAccessFormTitle() As String
'
'    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then
'
'        msubInitializeTextForUserFormToolsForAdoConStr
'    End If
'
'    GetTextOfStrKeyUserFormToolsForAdoConStrOledbMsAccessFormTitle = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_OLEDB_MS_ACCESS_FORM_TITLE")
'End Function
'
''''
'''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_OLEDB_MS_EXCEL_FORM_TITLE
''''
'''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
'Public Function GetTextOfStrKeyUserFormToolsForAdoConStrOledbMsExcelFormTitle() As String
'
'    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then
'
'        msubInitializeTextForUserFormToolsForAdoConStr
'    End If
'
'    GetTextOfStrKeyUserFormToolsForAdoConStrOledbMsExcelFormTitle = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_OLEDB_MS_EXCEL_FORM_TITLE")
'End Function
'
''''
'''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_CON_STR_CACHE_NAME
''''
'''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
'Public Function GetTextOfStrKeyUserFormToolsForAdoConStrLblConStrCacheName() As String
'
'    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then
'
'        msubInitializeTextForUserFormToolsForAdoConStr
'    End If
'
'    GetTextOfStrKeyUserFormToolsForAdoConStrLblConStrCacheName = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_CON_STR_CACHE_NAME")
'End Function
'
''''
'''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_FRA_ADO_CONNECTION_PARAMETERS
''''
'''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
'Public Function GetTextOfStrKeyUserFormToolsForAdoConStrFraAdoConnectionParameters() As String
'
'    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then
'
'        msubInitializeTextForUserFormToolsForAdoConStr
'    End If
'
'    GetTextOfStrKeyUserFormToolsForAdoConStrFraAdoConnectionParameters = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_FRA_ADO_CONNECTION_PARAMETERS")
'End Function
'
''''
'''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_FRA_DSN_TYPE
''''
'''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
'Public Function GetTextOfStrKeyUserFormToolsForAdoConStrFraDsnType() As String
'
'    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then
'
'        msubInitializeTextForUserFormToolsForAdoConStr
'    End If
'
'    GetTextOfStrKeyUserFormToolsForAdoConStrFraDsnType = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_FRA_DSN_TYPE")
'End Function
'
''''
'''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_OPT_USE_DSN
''''
'''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
'Public Function GetTextOfStrKeyUserFormToolsForAdoConStrOptUseDsn() As String
'
'    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then
'
'        msubInitializeTextForUserFormToolsForAdoConStr
'    End If
'
'    GetTextOfStrKeyUserFormToolsForAdoConStrOptUseDsn = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_OPT_USE_DSN")
'End Function
'
''''
'''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_OPT_DSNLESS
''''
'''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
'Public Function GetTextOfStrKeyUserFormToolsForAdoConStrOptDsnless() As String
'
'    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then
'
'        msubInitializeTextForUserFormToolsForAdoConStr
'    End If
'
'    GetTextOfStrKeyUserFormToolsForAdoConStrOptDsnless = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_OPT_DSNLESS")
'End Function
'
''''
'''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_FRA_DSN_PARAMETERS
''''
'''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
'Public Function GetTextOfStrKeyUserFormToolsForAdoConStrFraDsnParameters() As String
'
'    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then
'
'        msubInitializeTextForUserFormToolsForAdoConStr
'    End If
'
'    GetTextOfStrKeyUserFormToolsForAdoConStrFraDsnParameters = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_FRA_DSN_PARAMETERS")
'End Function
'
''''
'''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_DATA_SOURCE_NAME
''''
'''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
'Public Function GetTextOfStrKeyUserFormToolsForAdoConStrLblDataSourceName() As String
'
'    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then
'
'        msubInitializeTextForUserFormToolsForAdoConStr
'    End If
'
'    GetTextOfStrKeyUserFormToolsForAdoConStrLblDataSourceName = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_DATA_SOURCE_NAME")
'End Function
'
''''
'''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_FRA_DSNLESS_PGSQL_PARAMETERS
''''
'''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
'Public Function GetTextOfStrKeyUserFormToolsForAdoConStrFraDsnlessPgsqlParameters() As String
'
'    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then
'
'        msubInitializeTextForUserFormToolsForAdoConStr
'    End If
'
'    GetTextOfStrKeyUserFormToolsForAdoConStrFraDsnlessPgsqlParameters = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_FRA_DSNLESS_PGSQL_PARAMETERS")
'End Function
'
''''
'''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_FRA_DSNLESS_ORA_SQL_PARAMETERS
''''
'''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
'Public Function GetTextOfStrKeyUserFormToolsForAdoConStrFraDsnlessOraSqlParameters() As String
'
'    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then
'
'        msubInitializeTextForUserFormToolsForAdoConStr
'    End If
'
'    GetTextOfStrKeyUserFormToolsForAdoConStrFraDsnlessOraSqlParameters = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_FRA_DSNLESS_ORA_SQL_PARAMETERS")
'End Function
'
''''
'''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_FRA_DSNLESS_SQLITE_SQL_PARAMETERS
''''
'''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
'Public Function GetTextOfStrKeyUserFormToolsForAdoConStrFraDsnlessSqliteSqlParameters() As String
'
'    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then
'
'        msubInitializeTextForUserFormToolsForAdoConStr
'    End If
'
'    GetTextOfStrKeyUserFormToolsForAdoConStrFraDsnlessSqliteSqlParameters = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_FRA_DSNLESS_SQLITE_SQL_PARAMETERS")
'End Function
'
''''
'''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_ODBC_DRIVER
''''
'''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
'Public Function GetTextOfStrKeyUserFormToolsForAdoConStrLblOdbcDriver() As String
'
'    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then
'
'        msubInitializeTextForUserFormToolsForAdoConStr
'    End If
'
'    GetTextOfStrKeyUserFormToolsForAdoConStrLblOdbcDriver = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_ODBC_DRIVER")
'End Function
'
''''
'''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_SERVER_HOST
''''
'''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
'Public Function GetTextOfStrKeyUserFormToolsForAdoConStrLblServerHost() As String
'
'    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then
'
'        msubInitializeTextForUserFormToolsForAdoConStr
'    End If
'
'    GetTextOfStrKeyUserFormToolsForAdoConStrLblServerHost = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_SERVER_HOST")
'End Function
'
''''
'''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_DATABASE_NAME
''''
'''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
'Public Function GetTextOfStrKeyUserFormToolsForAdoConStrLblDatabaseName() As String
'
'    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then
'
'        msubInitializeTextForUserFormToolsForAdoConStr
'    End If
'
'    GetTextOfStrKeyUserFormToolsForAdoConStrLblDatabaseName = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_DATABASE_NAME")
'End Function
'
''''
'''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_NETWORK_SERVICE_NAME
''''
'''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
'Public Function GetTextOfStrKeyUserFormToolsForAdoConStrLblNetworkServiceName() As String
'
'    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then
'
'        msubInitializeTextForUserFormToolsForAdoConStr
'    End If
'
'    GetTextOfStrKeyUserFormToolsForAdoConStrLblNetworkServiceName = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_NETWORK_SERVICE_NAME")
'End Function
'
''''
'''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_PORT_NO
''''
'''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
'Public Function GetTextOfStrKeyUserFormToolsForAdoConStrLblPortNo() As String
'
'    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then
'
'        msubInitializeTextForUserFormToolsForAdoConStr
'    End If
'
'    GetTextOfStrKeyUserFormToolsForAdoConStrLblPortNo = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_PORT_NO")
'End Function
'
''''
'''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_USING_OLE_DB
''''
'''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
'Public Function GetTextOfStrKeyUserFormToolsForAdoConStrLblUsingOleDb() As String
'
'    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then
'
'        msubInitializeTextForUserFormToolsForAdoConStr
'    End If
'
'    GetTextOfStrKeyUserFormToolsForAdoConStrLblUsingOleDb = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_USING_OLE_DB")
'End Function
'
''''
'''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_FULL_PATH_OF_EXCEL_BOOK
''''
'''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
'Public Function GetTextOfStrKeyUserFormToolsForAdoConStrLblFullPathOfExcelBook() As String
'
'    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then
'
'        msubInitializeTextForUserFormToolsForAdoConStr
'    End If
'
'    GetTextOfStrKeyUserFormToolsForAdoConStrLblFullPathOfExcelBook = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_FULL_PATH_OF_EXCEL_BOOK")
'End Function
'
''''
'''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_FULL_PATH_OF_ACCESS_DB
''''
'''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
'Public Function GetTextOfStrKeyUserFormToolsForAdoConStrLblFullPathOfAccessDb() As String
'
'    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then
'
'        msubInitializeTextForUserFormToolsForAdoConStr
'    End If
'
'    GetTextOfStrKeyUserFormToolsForAdoConStrLblFullPathOfAccessDb = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_FULL_PATH_OF_ACCESS_DB")
'End Function
'
''''
'''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_FULL_PATH_OF_SQLITE_DB
''''
'''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
'Public Function GetTextOfStrKeyUserFormToolsForAdoConStrLblFullPathOfSqliteDb() As String
'
'    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then
'
'        msubInitializeTextForUserFormToolsForAdoConStr
'    End If
'
'    GetTextOfStrKeyUserFormToolsForAdoConStrLblFullPathOfSqliteDb = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_FULL_PATH_OF_SQLITE_DB")
'End Function
'
''''
'''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_CMD_BROWSE
''''
'''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
'Public Function GetTextOfStrKeyUserFormToolsForAdoConStrCmdBrowse() As String
'
'    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then
'
'        msubInitializeTextForUserFormToolsForAdoConStr
'    End If
'
'    GetTextOfStrKeyUserFormToolsForAdoConStrCmdBrowse = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_CMD_BROWSE")
'End Function
'
''''
'''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_USER_NAME
''''
'''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
'Public Function GetTextOfStrKeyUserFormToolsForAdoConStrLblUserName() As String
'
'    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then
'
'        msubInitializeTextForUserFormToolsForAdoConStr
'    End If
'
'    GetTextOfStrKeyUserFormToolsForAdoConStrLblUserName = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_USER_NAME")
'End Function
'
''''
'''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_USER_PASSWORD
''''
'''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
'Public Function GetTextOfStrKeyUserFormToolsForAdoConStrLblUserPassword() As String
'
'    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then
'
'        msubInitializeTextForUserFormToolsForAdoConStr
'    End If
'
'    GetTextOfStrKeyUserFormToolsForAdoConStrLblUserPassword = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_USER_PASSWORD")
'End Function
'
''''
'''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_PASSWORD_FOR_EXCEL_BOOK
''''
'''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
'Public Function GetTextOfStrKeyUserFormToolsForAdoConStrLblPasswordForExcelBook() As String
'
'    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then
'
'        msubInitializeTextForUserFormToolsForAdoConStr
'    End If
'
'    GetTextOfStrKeyUserFormToolsForAdoConStrLblPasswordForExcelBook = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_PASSWORD_FOR_EXCEL_BOOK")
'End Function
'
''''
'''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_PASSWORD_FOR_ACCESS_JET_DB_PASSWORD
''''
'''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
'Public Function GetTextOfStrKeyUserFormToolsForAdoConStrLblPasswordForAccessJetDbPassword() As String
'
'    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then
'
'        msubInitializeTextForUserFormToolsForAdoConStr
'    End If
'
'    GetTextOfStrKeyUserFormToolsForAdoConStrLblPasswordForAccessJetDbPassword = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_PASSWORD_FOR_ACCESS_JET_DB_PASSWORD")
'End Function
'
''''
'''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_CMD_ADO_CONNECTION_TEST
''''
'''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
'Public Function GetTextOfStrKeyUserFormToolsForAdoConStrCmdAdoConnectionTest() As String
'
'    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then
'
'        msubInitializeTextForUserFormToolsForAdoConStr
'    End If
'
'    GetTextOfStrKeyUserFormToolsForAdoConStrCmdAdoConnectionTest = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_CMD_ADO_CONNECTION_TEST")
'End Function
'
''''
'''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_CHK_READ_ONLY_ADO_CONNECTION
''''
'''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
'Public Function GetTextOfStrKeyUserFormToolsForAdoConStrChkReadOnlyAdoConnection() As String
'
'    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then
'
'        msubInitializeTextForUserFormToolsForAdoConStr
'    End If
'
'    GetTextOfStrKeyUserFormToolsForAdoConStrChkReadOnlyAdoConnection = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_CHK_READ_ONLY_ADO_CONNECTION")
'End Function
'
''''
'''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_CHK_HEADER_COLUMN_TITLES_EXIST
''''
'''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
'Public Function GetTextOfStrKeyUserFormToolsForAdoConStrChkHeaderColumnTitlesExist() As String
'
'    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then
'
'        msubInitializeTextForUserFormToolsForAdoConStr
'    End If
'
'    GetTextOfStrKeyUserFormToolsForAdoConStrChkHeaderColumnTitlesExist = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_CHK_HEADER_COLUMN_TITLES_EXIST")
'End Function
'
''''
'''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_IMEX_MODE
''''
'''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
'Public Function GetTextOfStrKeyUserFormToolsForAdoConStrLblImexMode() As String
'
'    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then
'
'        msubInitializeTextForUserFormToolsForAdoConStr
'    End If
'
'    GetTextOfStrKeyUserFormToolsForAdoConStrLblImexMode = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_IMEX_MODE")
'End Function
'
''''
'''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_FRA_CONNECTION_STRING_INFO_CACHING_OPTION
''''
'''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
'Public Function GetTextOfStrKeyUserFormToolsForAdoConStrFraConnectionStringInfoCachingOption() As String
'
'    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then
'
'        msubInitializeTextForUserFormToolsForAdoConStr
'    End If
'
'    GetTextOfStrKeyUserFormToolsForAdoConStrFraConnectionStringInfoCachingOption = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_FRA_CONNECTION_STRING_INFO_CACHING_OPTION")
'End Function
'
''''
'''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_CHK_SAVE_PARAMETERS_IN_REGISTORY_EXCEPT_FOR_PASSWORD
''''
'''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
'Public Function GetTextOfStrKeyUserFormToolsForAdoConStrChkSaveParametersInRegistoryExceptForPassword() As String
'
'    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then
'
'        msubInitializeTextForUserFormToolsForAdoConStr
'    End If
'
'    GetTextOfStrKeyUserFormToolsForAdoConStrChkSaveParametersInRegistoryExceptForPassword = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_CHK_SAVE_PARAMETERS_IN_REGISTORY_EXCEPT_FOR_PASSWORD")
'End Function
'
''''
'''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_CHK_SAVE_PASSWORD_TEMPORARY_CACHE
''''
'''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
'Public Function GetTextOfStrKeyUserFormToolsForAdoConStrChkSavePasswordTemporaryCache() As String
'
'    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then
'
'        msubInitializeTextForUserFormToolsForAdoConStr
'    End If
'
'    GetTextOfStrKeyUserFormToolsForAdoConStrChkSavePasswordTemporaryCache = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_CHK_SAVE_PASSWORD_TEMPORARY_CACHE")
'End Function
'
''''
'''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TOOLTIP_CHK_SAVE_PASSWORD_TEMPORARY_CACHE
''''
'''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
'Public Function GetTextOfStrKeyUserFormToolsForAdoConStrTooltipChkSavePasswordTemporaryCache() As String
'
'    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then
'
'        msubInitializeTextForUserFormToolsForAdoConStr
'    End If
'
'    GetTextOfStrKeyUserFormToolsForAdoConStrTooltipChkSavePasswordTemporaryCache = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TOOLTIP_CHK_SAVE_PASSWORD_TEMPORARY_CACHE")
'End Function
'
''''
'''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_CSV_ADO_OLE_DB_CONNECTION_TEST
''''
'''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
'Public Function GetTextOfStrKeyUserFormToolsForAdoConStrTitleCsvAdoOleDbConnectionTest() As String
'
'    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then
'
'        msubInitializeTextForUserFormToolsForAdoConStr
'    End If
'
'    GetTextOfStrKeyUserFormToolsForAdoConStrTitleCsvAdoOleDbConnectionTest = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_CSV_ADO_OLE_DB_CONNECTION_TEST")
'End Function
'
''''
'''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_MS_ACCESS_DB_ADO_OLE_DB_CONNECTION_TEST
''''
'''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
'Public Function GetTextOfStrKeyUserFormToolsForAdoConStrTitleMsAccessDbAdoOleDbConnectionTest() As String
'
'    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then
'
'        msubInitializeTextForUserFormToolsForAdoConStr
'    End If
'
'    GetTextOfStrKeyUserFormToolsForAdoConStrTitleMsAccessDbAdoOleDbConnectionTest = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_MS_ACCESS_DB_ADO_OLE_DB_CONNECTION_TEST")
'End Function
'
''''
'''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_MS_EXCEL_SHEET_ADO_OLE_DB_CONNECTION_TEST
''''
'''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
'Public Function GetTextOfStrKeyUserFormToolsForAdoConStrTitleMsExcelSheetAdoOleDbConnectionTest() As String
'
'    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then
'
'        msubInitializeTextForUserFormToolsForAdoConStr
'    End If
'
'    GetTextOfStrKeyUserFormToolsForAdoConStrTitleMsExcelSheetAdoOleDbConnectionTest = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_MS_EXCEL_SHEET_ADO_OLE_DB_CONNECTION_TEST")
'End Function
'
''''
'''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_POSTGRESQL_ADO_ODBC_CONNECTION_TEST
''''
'''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
'Public Function GetTextOfStrKeyUserFormToolsForAdoConStrTitlePostgresqlAdoOdbcConnectionTest() As String
'
'    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then
'
'        msubInitializeTextForUserFormToolsForAdoConStr
'    End If
'
'    GetTextOfStrKeyUserFormToolsForAdoConStrTitlePostgresqlAdoOdbcConnectionTest = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_POSTGRESQL_ADO_ODBC_CONNECTION_TEST")
'End Function
'
''''
'''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_ORACLE_ADO_ODBC_CONNECTION_TEST
''''
'''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
'Public Function GetTextOfStrKeyUserFormToolsForAdoConStrTitleOracleAdoOdbcConnectionTest() As String
'
'    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then
'
'        msubInitializeTextForUserFormToolsForAdoConStr
'    End If
'
'    GetTextOfStrKeyUserFormToolsForAdoConStrTitleOracleAdoOdbcConnectionTest = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_ORACLE_ADO_ODBC_CONNECTION_TEST")
'End Function
'
''''
'''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_SQLITE_ADO_ODBC_CONNECTION_TEST
''''
'''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
'Public Function GetTextOfStrKeyUserFormToolsForAdoConStrTitleSqliteAdoOdbcConnectionTest() As String
'
'    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then
'
'        msubInitializeTextForUserFormToolsForAdoConStr
'    End If
'
'    GetTextOfStrKeyUserFormToolsForAdoConStrTitleSqliteAdoOdbcConnectionTest = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_SQLITE_ADO_ODBC_CONNECTION_TEST")
'End Function
'
''''
'''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_MES_ADO_CONNECTION_SUCCESS
''''
'''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
'Public Function GetTextOfStrKeyUserFormToolsForAdoConStrMesAdoConnectionSuccess() As String
'
'    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then
'
'        msubInitializeTextForUserFormToolsForAdoConStr
'    End If
'
'    GetTextOfStrKeyUserFormToolsForAdoConStrMesAdoConnectionSuccess = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_MES_ADO_CONNECTION_SUCCESS")
'End Function
'
''''
'''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_MES_ADO_CONNECTION_FAILED
''''
'''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
'Public Function GetTextOfStrKeyUserFormToolsForAdoConStrMesAdoConnectionFailed() As String
'
'    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then
'
'        msubInitializeTextForUserFormToolsForAdoConStr
'    End If
'
'    GetTextOfStrKeyUserFormToolsForAdoConStrMesAdoConnectionFailed = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_MES_ADO_CONNECTION_FAILED")
'End Function
'
''''
'''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_SELECT_SQLITE_DATABASE_FILE
''''
'''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
'Public Function GetTextOfStrKeyUserFormToolsForAdoConStrTitleSelectSqliteDatabaseFile() As String
'
'    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then
'
'        msubInitializeTextForUserFormToolsForAdoConStr
'    End If
'
'    GetTextOfStrKeyUserFormToolsForAdoConStrTitleSelectSqliteDatabaseFile = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_SELECT_SQLITE_DATABASE_FILE")
'End Function
'
''''
'''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_DATABASE_SQLITE
''''
'''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
'Public Function GetTextOfStrKeyUserFormToolsForAdoConStrDatabaseSqlite() As String
'
'    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then
'
'        msubInitializeTextForUserFormToolsForAdoConStr
'    End If
'
'    GetTextOfStrKeyUserFormToolsForAdoConStrDatabaseSqlite = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_DATABASE_SQLITE")
'End Function
'
''''
'''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_SELECT_MS_ACCESS_DATABASE_FILE
''''
'''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
'Public Function GetTextOfStrKeyUserFormToolsForAdoConStrTitleSelectMsAccessDatabaseFile() As String
'
'    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then
'
'        msubInitializeTextForUserFormToolsForAdoConStr
'    End If
'
'    GetTextOfStrKeyUserFormToolsForAdoConStrTitleSelectMsAccessDatabaseFile = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_SELECT_MS_ACCESS_DATABASE_FILE")
'End Function
'
''''
'''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_DATABASE_MS_ACCESS
''''
'''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
'Public Function GetTextOfStrKeyUserFormToolsForAdoConStrDatabaseMsAccess() As String
'
'    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then
'
'        msubInitializeTextForUserFormToolsForAdoConStr
'    End If
'
'    GetTextOfStrKeyUserFormToolsForAdoConStrDatabaseMsAccess = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_DATABASE_MS_ACCESS")
'End Function
'
''''
'''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_SELECT_MS_EXCEL_BOOK_FILE
''''
'''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
'Public Function GetTextOfStrKeyUserFormToolsForAdoConStrTitleSelectMsExcelBookFile() As String
'
'    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then
'
'        msubInitializeTextForUserFormToolsForAdoConStr
'    End If
'
'    GetTextOfStrKeyUserFormToolsForAdoConStrTitleSelectMsExcelBookFile = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_SELECT_MS_EXCEL_BOOK_FILE")
'End Function
'
''''
'''' get string Value from STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_MS_EXCEL_BOOK
''''
'''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
'Public Function GetTextOfStrKeyUserFormToolsForAdoConStrMsExcelBook() As String
'
'    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then
'
'        msubInitializeTextForUserFormToolsForAdoConStr
'    End If
'
'    GetTextOfStrKeyUserFormToolsForAdoConStrMsExcelBook = mobjStrKeyValueUserFormToolsForAdoConStrDic.Item("STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_MS_EXCEL_BOOK")
'End Function
'
''''
'''' Initialize Key-Values which values
''''
'''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
'Private Sub msubInitializeTextForUserFormToolsForAdoConStr()
'
'    Dim objKeyToTextDic As Scripting.Dictionary
'
'    If Not mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized Then
'
'        Set objKeyToTextDic = GetKeyToTextForLocalizingEachModule(mstrModuleName)
'
'        If objKeyToTextDic Is Nothing Then
'
'            ' If the Key-Value table file is lost, then the default setting is used
'
'            Set objKeyToTextDic = New Scripting.Dictionary
'
'            Select Case GetCurrentUICaptionLanguageType()
'
'                Case CaptionLanguageType.UIJapaneseCaptions
'
'                    AddStringKeyValueForUserFormToolsForAdoConStrByJapaneseValues objKeyToTextDic
'                Case Else
'
'                    AddStringKeyValueForUserFormToolsForAdoConStrByEnglishValues objKeyToTextDic
'            End Select
'        End If
'
'        mblnIsStrKeyValueUserFormToolsForAdoConStrDicInitialized = True
'    End If
'
'    Set mobjStrKeyValueUserFormToolsForAdoConStrDic = objKeyToTextDic
'End Sub
'
''''
'''' Add Key-Values which values are in English for UserFormToolsForAdoConStr key-values cache
''''
'''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
'Private Sub AddStringKeyValueForUserFormToolsForAdoConStrByEnglishValues(ByRef robjDic As Scripting.Dictionary)
'
'    With robjDic
'
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_ODBC_POSTGRESQL_FORM_TITLE", "ADO connection string setting for ODBC PostgreSQL"
'
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_ODBC_ORACLE_FORM_TITLE", "ADO connection string setting for ODBC Oracle"
'
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_ODBC_SQLITE3_FORM_TITLE", "ADO connection string setting for ODBC SQLite"
'
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_OLEDB_MS_ACCESS_FORM_TITLE", "ADO connection string setting for OLEDB to MS Access DB"
'
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_OLEDB_MS_EXCEL_FORM_TITLE", "ADO connection string setting for OLEDB to MS Excel sheet"
'
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_CON_STR_CACHE_NAME", "Connection setting local name key"
'
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_FRA_ADO_CONNECTION_PARAMETERS", "ADO connection parameters"
'
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_FRA_DSN_TYPE", "Whether use DSN or not"
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_OPT_USE_DSN", "Use DSN"
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_OPT_DSNLESS", "DSNless"
'
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_FRA_DSN_PARAMETERS", "DSN parameters"
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_DATA_SOURCE_NAME", "Data source name(DSN)"
'
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_FRA_DSNLESS_PGSQL_PARAMETERS", "DSNless PosgreSQL ODBC settings"
'
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_FRA_DSNLESS_ORA_SQL_PARAMETERS", "DSNless Oracle ODBC settings"
'
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_FRA_DSNLESS_SQLITE_SQL_PARAMETERS", "DSNless SQLite ODBC settings"
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_ODBC_DRIVER", "ODBC driver"
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_SERVER_HOST", "Server host name"
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_DATABASE_NAME", "Data base name"
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_NETWORK_SERVICE_NAME", "Network service name"
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_PORT_NO", "Port Number"
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_USING_OLE_DB", "Using OLE DB interface"
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_FULL_PATH_OF_EXCEL_BOOK", "Path of Excel book"
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_FULL_PATH_OF_ACCESS_DB", "Path of Access DB"
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_FULL_PATH_OF_SQLITE_DB", "Path of SQLite DB"
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_CMD_BROWSE", "Browse"
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_USER_NAME", "User name"
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_USER_PASSWORD", "User password"
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_PASSWORD_FOR_EXCEL_BOOK", "Excel book password"
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_PASSWORD_FOR_ACCESS_JET_DB_PASSWORD", "Access JET DB password"
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_CMD_ADO_CONNECTION_TEST", "ADO connection test"
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_CHK_READ_ONLY_ADO_CONNECTION", "Read only ADO connection"
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_CHK_HEADER_COLUMN_TITLES_EXIST", "Header column titles exist"
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_IMEX_MODE", "IMEX mode"
'
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_FRA_CONNECTION_STRING_INFO_CACHING_OPTION", "Connection string information caching option"
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_CHK_SAVE_PARAMETERS_IN_REGISTORY_EXCEPT_FOR_PASSWORD", "Save parameters in Windows-registry except for password"
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_CHK_SAVE_PASSWORD_TEMPORARY_CACHE", "Save password temporary cache variable"
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TOOLTIP_CHK_SAVE_PASSWORD_TEMPORARY_CACHE", "The cache password is to be discarded when the Application is closed"
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_CSV_ADO_OLE_DB_CONNECTION_TEST", "Csv ADO OLE DB connection test"
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_MS_ACCESS_DB_ADO_OLE_DB_CONNECTION_TEST", "Microsoft Access database ADO OLE DB connection test"
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_MS_EXCEL_SHEET_ADO_OLE_DB_CONNECTION_TEST", "Microsoft Excel sheet ADO OLE DB connection test"
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_POSTGRESQL_ADO_ODBC_CONNECTION_TEST", "PostgreSQL ADO ODBC connection test"
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_ORACLE_ADO_ODBC_CONNECTION_TEST", "Oracle ADO ODBC connection test"
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_SQLITE_ADO_ODBC_CONNECTION_TEST", "SQLite ADO ODBC connection test"
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_MES_ADO_CONNECTION_SUCCESS", "ADO connection had been settled."
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_MES_ADO_CONNECTION_FAILED", "ADO connection was failed."
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_SELECT_SQLITE_DATABASE_FILE", "Select a SQLite database file"
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_DATABASE_SQLITE", "SQLite database"
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_SELECT_MS_ACCESS_DATABASE_FILE", "Select a Microsoft Access database file"
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_DATABASE_MS_ACCESS", "Access database"
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_SELECT_MS_EXCEL_BOOK_FILE", "Select a Excel file"
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_MS_EXCEL_BOOK", "Excel book"
'    End With
'End Sub
'
''''
'''' Add Key-Values which values are in Japanese for UserFormToolsForAdoConStr key-values cache
''''
'''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
'Private Sub AddStringKeyValueForUserFormToolsForAdoConStrByJapaneseValues(ByRef robjDic As Scripting.Dictionary)
'
'    With robjDic
'
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_ODBC_POSTGRESQL_FORM_TITLE", "ADO接続文字列設定 - ODBC - PostgreSQL"
'
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_ODBC_ORACLE_FORM_TITLE", "ADO接続文字列設定 - ODBC - Oracle"
'
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_ODBC_SQLITE3_FORM_TITLE", "ADO接続文字列設定 - ODBC - SQLite"
'
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_OLEDB_MS_ACCESS_FORM_TITLE", "ADO接続文字列設定 - OLEDB - MS Access DB"
'
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_OLEDB_MS_EXCEL_FORM_TITLE", "ADO接続文字列設定 - OLEDB - MS Excel sheet"
'
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_CON_STR_CACHE_NAME", "接続設定名キー"
'
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_FRA_ADO_CONNECTION_PARAMETERS", "ADO接続パラメータ"
'
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_FRA_DSN_TYPE", "DSN設定 使用・使用しない"
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_OPT_USE_DSN", "DSN使用"
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_OPT_DSNLESS", "DSN無し"
'
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_FRA_DSN_PARAMETERS", "DSN項目"
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_DATA_SOURCE_NAME", "データソース名(DSN)"
'
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_FRA_DSNLESS_PGSQL_PARAMETERS", "DSN無し PostgreSQL ODBC設定"
'
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_FRA_DSNLESS_ORA_SQL_PARAMETERS", "DSN無し Oracle ODBC設定"
'
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_FRA_DSNLESS_SQLITE_SQL_PARAMETERS", "DSN無し SQLite ODBC設定"
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_ODBC_DRIVER", "ODBCドライバ"
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_SERVER_HOST", "サーバホスト名"
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_DATABASE_NAME", "データベース名"
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_NETWORK_SERVICE_NAME", "ネットワークサービス名"
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_PORT_NO", "ポート番号"
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_USING_OLE_DB", "使用するOLE DB"
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_FULL_PATH_OF_EXCEL_BOOK", "Excelブックパス"
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_FULL_PATH_OF_ACCESS_DB", "Accessデータベースパス"
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_FULL_PATH_OF_SQLITE_DB", "SQLiteデータベースパス"
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_CMD_BROWSE", "開く"
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_USER_NAME", "ユーザ名"
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_USER_PASSWORD", "ユーザパスワード"
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_PASSWORD_FOR_EXCEL_BOOK", "Excelブックパスワード"
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_PASSWORD_FOR_ACCESS_JET_DB_PASSWORD", "Access JETデータベースパスワード"
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_CMD_ADO_CONNECTION_TEST", "ADO接続テスト"
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_CHK_READ_ONLY_ADO_CONNECTION", "読み取り専用ADO接続"
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_CHK_HEADER_COLUMN_TITLES_EXIST", "列見出し有り"
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_IMEX_MODE", "IMEXモード"
'
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_FRA_CONNECTION_STRING_INFO_CACHING_OPTION", "接続設定情報保存オプション"
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_CHK_SAVE_PARAMETERS_IN_REGISTORY_EXCEPT_FOR_PASSWORD", "パスワードを除いて接続情報をレジストリに保存"
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_CHK_SAVE_PASSWORD_TEMPORARY_CACHE", "パスワードを一時的に保存"
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TOOLTIP_CHK_SAVE_PASSWORD_TEMPORARY_CACHE", "一時的に保存されたパスワードは、アプリケーション終了時に消されます"
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_CSV_ADO_OLE_DB_CONNECTION_TEST", "Csv ADO OLE DB接続テスト"
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_MS_ACCESS_DB_ADO_OLE_DB_CONNECTION_TEST", "Microsoft Accessデータベース ADO OLE DB接続テスト"
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_MS_EXCEL_SHEET_ADO_OLE_DB_CONNECTION_TEST", "Microsoft Excelシート ADO OLE DB接続テスト"
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_POSTGRESQL_ADO_ODBC_CONNECTION_TEST", "PostgreSQLデータベース ADO ODBC接続テスト"
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_ORACLE_ADO_ODBC_CONNECTION_TEST", "Oracleデータベース ADO ODBC接続テスト"
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_SQLITE_ADO_ODBC_CONNECTION_TEST", "SQLiteデータベース ADO ODBC接続テスト"
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_MES_ADO_CONNECTION_SUCCESS", "ADO接続しました。"
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_MES_ADO_CONNECTION_FAILED", "ADO接続は失敗しました。"
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_SELECT_SQLITE_DATABASE_FILE", "SQLiteデータベースファイルを選択してください"
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_DATABASE_SQLITE", "SQLiteデータベース"
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_SELECT_MS_ACCESS_DATABASE_FILE", "Accessデータベースファイルを選択してください"
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_DATABASE_MS_ACCESS", "Accessデータベース"
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_SELECT_MS_EXCEL_BOOK_FILE", "Excelブックファイルを選択してください"
'        .Add "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_MS_EXCEL_BOOK", "Excelブック"
'    End With
'End Sub
'
''''
'''' Remove Keys for UserFormToolsForAdoConStr key-values cache
''''
'''' automatically-added for UserFormToolsForAdoConStr string-key-value management for standard module and class module
'Private Sub RemoveKeysOfKeyValueUserFormToolsForAdoConStr(ByRef robjDic As Scripting.Dictionary)
'
'    With robjDic
'
'        If .Exists(STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_ODBC_POSTGRESQL_FORM_TITLE) Then   ' Check only the first key, otherwise omit items
'
'            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_ODBC_POSTGRESQL_FORM_TITLE"
'
'            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_ODBC_ORACLE_FORM_TITLE"
'
'            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_ODBC_SQLITE3_FORM_TITLE"
'
'            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_OLEDB_MS_ACCESS_FORM_TITLE"
'
'            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_OLEDB_MS_EXCEL_FORM_TITLE"
'
'            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_CON_STR_CACHE_NAME"
'
'            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_FRA_ADO_CONNECTION_PARAMETERS"
'
'            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_FRA_DSN_TYPE"
'            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_OPT_USE_DSN"
'            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_OPT_DSNLESS"
'
'            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_FRA_DSN_PARAMETERS"
'            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_DATA_SOURCE_NAME"
'
'            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_FRA_DSNLESS_PGSQL_PARAMETERS"
'
'            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_FRA_DSNLESS_ORA_SQL_PARAMETERS"
'
'            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_FRA_DSNLESS_SQLITE_SQL_PARAMETERS"
'            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_ODBC_DRIVER"
'            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_SERVER_HOST"
'            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_DATABASE_NAME"
'            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_NETWORK_SERVICE_NAME"
'            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_PORT_NO"
'            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_USING_OLE_DB"
'            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_FULL_PATH_OF_EXCEL_BOOK"
'            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_FULL_PATH_OF_ACCESS_DB"
'            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_FULL_PATH_OF_SQLITE_DB"
'            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_CMD_BROWSE"
'            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_USER_NAME"
'            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_USER_PASSWORD"
'            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_PASSWORD_FOR_EXCEL_BOOK"
'            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_PASSWORD_FOR_ACCESS_JET_DB_PASSWORD"
'            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_CMD_ADO_CONNECTION_TEST"
'            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_CHK_READ_ONLY_ADO_CONNECTION"
'            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_CHK_HEADER_COLUMN_TITLES_EXIST"
'            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_LBL_IMEX_MODE"
'
'            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_FRA_CONNECTION_STRING_INFO_CACHING_OPTION"
'            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_CHK_SAVE_PARAMETERS_IN_REGISTORY_EXCEPT_FOR_PASSWORD"
'            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_CHK_SAVE_PASSWORD_TEMPORARY_CACHE"
'            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TOOLTIP_CHK_SAVE_PASSWORD_TEMPORARY_CACHE"
'            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_CSV_ADO_OLE_DB_CONNECTION_TEST"
'            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_MS_ACCESS_DB_ADO_OLE_DB_CONNECTION_TEST"
'            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_MS_EXCEL_SHEET_ADO_OLE_DB_CONNECTION_TEST"
'            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_POSTGRESQL_ADO_ODBC_CONNECTION_TEST"
'            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_ORACLE_ADO_ODBC_CONNECTION_TEST"
'            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_SQLITE_ADO_ODBC_CONNECTION_TEST"
'            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_MES_ADO_CONNECTION_SUCCESS"
'            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_MES_ADO_CONNECTION_FAILED"
'            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_SELECT_SQLITE_DATABASE_FILE"
'            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_DATABASE_SQLITE"
'            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_SELECT_MS_ACCESS_DATABASE_FILE"
'            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_DATABASE_MS_ACCESS"
'            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_TITLE_SELECT_MS_EXCEL_BOOK_FILE"
'            .Remove "STR_KEY_USER_FORM_TOOLS_FOR_ADO_CON_STR_MS_EXCEL_BOOK"
'        End If
'    End With
'End Sub
'
'
'''--VBA_Code_File--<FormStateToSetAdoConStr.cls>--
'VERSION 1.0 CLASS
'BEGIN
'  MultiUse = -1  'True
'END
'Attribute VB_Name = "FormStateToSetAdoConStr"
'Attribute VB_GlobalNameSpace = False
'Attribute VB_Creatable = False
'Attribute VB_PredeclaredId = False
'Attribute VB_Exposed = False
''
''   Form state control data object for ADO connection string parameters
''   This is referred from USetAdoOdbcConStrForSqLite.frm, USetAdoOleDbConStrForAccdb.frm, USetAdoOleDbConStrForXlBook.frm, USetAdoOdbcConStrForPgSql.frm
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on ADO
''       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Tue, 18/Jul/2023    Kalmclaeyd Tarclanus    Initial creation
''
'
'Option Explicit
'
'
''///////////////////////////////////////////////
''/// Declarations
''///////////////////////////////////////////////
'
'Private mstrSettingKeyName As String
'
'Private mobjUserInputParametersDic As Scripting.Dictionary
'
'
'Private mblnIsRequestedToCancelAdoConnection As Boolean
'
''///////////////////////////////////////////////
''/// Event handlers
''///////////////////////////////////////////////
''''
''''
''''
'Private Sub Class_Initialize()
'
'    mblnIsRequestedToCancelAdoConnection = False
'End Sub
'
''///////////////////////////////////////////////
''/// Properties
''///////////////////////////////////////////////
''''
''''
''''
'Public Property Get UserInputParametersDic() As Scripting.Dictionary
'
'    Set UserInputParametersDic = mobjUserInputParametersDic
'End Property
'Public Property Set UserInputParametersDic(ByVal vobjUserInputParametersDic As Scripting.Dictionary)
'
'    Set mobjUserInputParametersDic = vobjUserInputParametersDic
'End Property
'
''''
''''
''''
'Public Property Get SettingKeyName() As String
'
'    SettingKeyName = mstrSettingKeyName
'End Property
'Public Property Let SettingKeyName(ByVal vstrSettingKeyName As String)
'
'    mstrSettingKeyName = vstrSettingKeyName
'End Property
'
''''
''''
''''
'Public Property Get IsRequestedToCancelAdoConnection() As Boolean
'
'    IsRequestedToCancelAdoConnection = mblnIsRequestedToCancelAdoConnection
'End Property
'Public Property Let IsRequestedToCancelAdoConnection(ByVal vblnIsRequestedToCancelAdoConnection As Boolean)
'
'    mblnIsRequestedToCancelAdoConnection = vblnIsRequestedToCancelAdoConnection
'End Property
'
'
'''--VBA_Code_File--<SimplePdAuthenticationAdo.bas>--
'Attribute VB_Name = "SimplePdAuthenticationAdo"
''
''   Simply password keeper for some type RDB (relational data-base)
''   If users feel some risks for keeping passwords in VB project variable cache, they can remove this VB code module from this VB project without any VBA syntax error.
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on ADO
''       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Thu,  6/Jul/2023    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Declarations
''///////////////////////////////////////////////
'
'Private mobjConnectionInfoToPasswordInfoDic As Scripting.Dictionary ' Dictionary(Of String[file path], String[password])
'
'
''///////////////////////////////////////////////
''/// Enumerations
''///////////////////////////////////////////////
''''
''''
''''
'Public Enum RDBInfoCacheType
'
'    PostgreSQLInfoCache = 1
'
'    SQLiteInfoCache = 2
'
'    OracleInfoCache = 4
'
'    DSNConnectionCache = 10
'End Enum
'
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''**---------------------------------------------
''** ADO connection info setting password keeper control
''**---------------------------------------------
''''
'''' This is called from Application.Run
''''
'Public Function IsAdoPdAuthenticationCachingEnabled() As Boolean
'
'    IsAdoPdAuthenticationCachingEnabled = True
'End Function
'
'
'
''**---------------------------------------------
''** Cache RDB user password temporarily
''**---------------------------------------------
''''
''''
''''
'Public Sub ClearCacheTemporaryRdbUserPasswords()
'
'    If Not mobjConnectionInfoToPasswordInfoDic Is Nothing Then
'
'        mobjConnectionInfoToPasswordInfoDic.RemoveAll
'
'        Set mobjConnectionInfoToPasswordInfoDic = Nothing
'    End If
'End Sub
'
''''
''''
''''
'Public Function GetRdbUserPasswordByAdoConStrPartKey(ByRef rstrAdoConStrPartKey As String) As String
'
'    Dim strCachePassword As String
'
'    strCachePassword = ""
'
'    If Not mobjConnectionInfoToPasswordInfoDic Is Nothing Then
'
'        With mobjConnectionInfoToPasswordInfoDic
'
'            If .Exists(rstrAdoConStrPartKey) Then
'
'                strCachePassword = .Item(rstrAdoConStrPartKey)
'            End If
'        End With
'    End If
'
'    GetRdbUserPasswordByAdoConStrPartKey = strCachePassword
'End Function
'
''''
'''' Serve a ADO connection supported RDB password
''''
'Public Sub StoreRdbUserPasswordByAdoConStrPartKey(ByRef rstrAdoConStrPartKey As String, ByRef rstrPassword As String)
'
'    If mobjConnectionInfoToPasswordInfoDic Is Nothing Then Set mobjConnectionInfoToPasswordInfoDic = New Scripting.Dictionary
'
'    With mobjConnectionInfoToPasswordInfoDic
'
'        If Not .Exists(rstrAdoConStrPartKey) Then
'
'            .Add rstrAdoConStrPartKey, rstrPassword
'        Else
'            .Item(rstrAdoConStrPartKey) = rstrPassword
'        End If
'    End With
'End Sub
'
'''--VBA_Code_File--<SimpleAdoConStrAuthentication.bas>--
'Attribute VB_Name = "SimpleAdoConStrAuthentication"
''
''   ADO connection string save and load between a Dictionary object and a Windows-registry key values except for any passwords
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on both ADO and Windows OS
''       Dependent on ConvertDicToWinReg.bas, WinRegGeneral.bas
''       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Thu, 13/Jul/2023    Kalmclaeyd Tarclanus    Initial creation
''
'
'Option Explicit
'
'
''///////////////////////////////////////////////
''/// Constants
''///////////////////////////////////////////////
''**---------------------------------------------
''** Public constants
''**---------------------------------------------
'Public Const mstrTemporaryPdKey As String = "Password"
'
'Public Const mstrAdoConnectionTypeAndDestinationKey As String = "ConnectTypeAndDestination"
'
'Public Const mstrAdoDbFilePathKey As String = "DbFilePath"
'
''**---------------------------------------------
''** Internal private constants
''**---------------------------------------------
'Private Const mstrModuleKey As String = "SimpleAdoConStrAuthentication"   ' 1st part of registry sub-key
'
'Private Const mstrSubjectCachingAdoConStrParamsKey As String = "CachingAdoConStrParams"   ' 2nd part of registry sub-key
'
'Private Const mstrModuleNameAndSubjectKeyForAdoConStrCachingMode As String = mstrModuleKey & "\" & mstrSubjectCachingAdoConStrParamsKey
'
'
'Private Const mstrWinRegValueNameOfAllowToSaveAdoConnectionParams As String = "AllowToSaveAdoConnectionParams"
'
'Private Const mstrWinRegValueNameOfAllowToSaveAdoConStrPasswordTemporarily As String = "AllowToSaveAdoConStrPdTemporarily"
'
'Private Const mstrWinRegValueNameOfAllowToSaveMsExcelLockFilePasswordTemporarily As String = "AllowToSaveMsExcelLockFilePdTemporarily"
'
'Private Const mstrWinRegValueNameOfAllowToSaveMsAccessDbLockFilePasswordTemporarily As String = "AllowToSaveMsAccessDbLockFilePdTemporarily"
'
'
'Private Const mstrRegKeySecureOfficeCommonVBAAdoConStr As String = "HKEY_CURRENT_USER\Software\VBASecurePosition\OfficeCommonVBA\AdoConStrSettings"
'
''///////////////////////////////////////////////
''/// Declarations
''///////////////////////////////////////////////
''**---------------------------------------------
''** ADO connection info setting keeper control
''**---------------------------------------------
'Private mblnSaveParametersInRegistoryExceptForPassword As Boolean
'
'Private mblnIsTheValue_SaveParametersInRegistoryExceptForPassword_Initialized As Boolean
'
'
''**---------------------------------------------
''** About ADO connection password caching
''**---------------------------------------------
'
'Private mblnSaveAdoConStrPasswordInTemporaryCache As Boolean
'
'Private mblnIsTheValue_SaveAdoConStrPasswordInTemporaryCache_Initialized As Boolean
'
'Private mblnIsTheFunction_SaveAdoConStrPasswordInTemporaryCache_Enabled As Boolean
'
'
'
'Private mblnSaveMsExcelFileLockPasswordInTemporaryCache As Boolean
'
'Private mblnIsTheValue_SaveMsExcelFileLockPasswordInTemporaryCache_Initialized As Boolean
'
'Private mblnIsTheFunction_SaveMsExcelFileLockPasswordInTemporaryCache_Enabled As Boolean
'
'
'
'Private mblnSaveMsAccessDbFileLockPasswordInTemporaryCache As Boolean
'
'Private mblnIsTheValue_SaveMsAccessDbFileLockPasswordInTemporaryCache_Initialized As Boolean
'
'Private mblnIsTheFunction_SaveMsAccessDbFileLockPasswordInTemporaryCache_Enabled As Boolean
'
'
''**---------------------------------------------
''** About confirmed no password lock file-paths
''**---------------------------------------------
'' cache
'Private mobjNoPasswordLockFilePathDic As Scripting.Dictionary
'
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''**---------------------------------------------
''** ADO connection info setting keeper control
''**---------------------------------------------
''''
''''
''''
'Public Sub SetAdoConnectionSaveParametersInRegistoryExceptForPassword(ByVal vblnSaveParametersInRegistoryExceptForPassword As Boolean)
'
'    If mblnSaveParametersInRegistoryExceptForPassword <> vblnSaveParametersInRegistoryExceptForPassword Then
'
'        mblnSaveParametersInRegistoryExceptForPassword = vblnSaveParametersInRegistoryExceptForPassword
'
'        WriteCurUserRegBoolByModuleNameAndSubjectSubKey mstrModuleNameAndSubjectKeyForAdoConStrCachingMode, mstrWinRegValueNameOfAllowToSaveAdoConnectionParams, vblnSaveParametersInRegistoryExceptForPassword
'    End If
'End Sub
'Public Function GetAdoConnectionSaveParametersInRegistoryExceptForPassword() As Boolean
'
'    Dim blnReadValue As Boolean
'
'    If Not mblnIsTheValue_SaveParametersInRegistoryExceptForPassword_Initialized Then
'
'        mblnSaveParametersInRegistoryExceptForPassword = True
'
'        If IsCurUserRegBoolAboutAllowToSaveAdoConnectionParams(blnReadValue) Then
'
'            mblnSaveParametersInRegistoryExceptForPassword = blnReadValue
'        End If
'
'        mblnIsTheValue_SaveParametersInRegistoryExceptForPassword_Initialized = True
'    End If
'
'    GetAdoConnectionSaveParametersInRegistoryExceptForPassword = mblnSaveParametersInRegistoryExceptForPassword
'End Function
'
''''
''''
''''
'Public Function IsCurUserRegBoolAboutAllowToSaveAdoConnectionParams(ByRef rblnReadValue As Boolean) As Boolean
'
'    Dim blnIsValueRead As Boolean
'
'    blnIsValueRead = False
'
'    If IsCurUserRegBoolReadFromModuleNameAndSubjectSubKey(rblnReadValue, mstrModuleNameAndSubjectKeyForAdoConStrCachingMode, mstrWinRegValueNameOfAllowToSaveAdoConnectionParams) Then
'
'        blnIsValueRead = True
'    End If
'
'    IsCurUserRegBoolAboutAllowToSaveAdoConnectionParams = blnIsValueRead
'End Function
'
'
''**---------------------------------------------
''** About ADO connection password caching
''**---------------------------------------------
''''
''''
''''
'''' <Argument>robjUserInputParametersDic: Input-Output</Argument>
'''' <Return>Boolean</Return>
'Public Function IsOpeningFormNeededAfterTryToGetAdoConnectionPasswordFromTemporaryCache(ByRef robjUserInputParametersDic As Scripting.Dictionary) As Boolean
'
'    Dim blnIsOpeningFormNeeded As Boolean, strPassword As String, strConStrInfoKey As String, enmOleDbOrOdbcAdoConnectionDestinationRDBType As OleDbOrOdbcAdoConnectionDestinationRDBType
'
'    blnIsOpeningFormNeeded = True
'
'    If GetAllowToSaveAdoConStrPasswordInTemporaryCache() Then
'
'        If IsSaveAdoConStrPasswordInTemporaryCacheFunctionEnabled() Then
'
'            ' If the SimplePdAuthenticationAdo.bas had been removed in this VB project, the following doesn't causes a VBA syntax error
'
'            strPassword = ""
'
'            enmOleDbOrOdbcAdoConnectionDestinationRDBType = GetEnumOfOleDbOrOdbcAdoConnectionDestinationRDBTypeFromDescription(robjUserInputParametersDic.Item(mstrAdoConnectionTypeAndDestinationKey))
'
'            Select Case enmOleDbOrOdbcAdoConnectionDestinationRDBType
'
'                Case OleDbOrOdbcAdoConnectionDestinationRDBType.AdoOleDbAndMsExcel
'
'                    strPassword = GetStringFromCallBackInterfaceAndOneStringArgument("GetLockingBookPasswordByPath", robjUserInputParametersDic.Item(mstrAdoDbFilePathKey))
'
'                Case OleDbOrOdbcAdoConnectionDestinationRDBType.AdoOleDbAndMsAccessDb
'
'                    strPassword = GetStringFromCallBackInterfaceAndOneStringArgument("GetLockingAccdbPasswordByPath", robjUserInputParametersDic.Item(mstrAdoDbFilePathKey))
'                Case Else
'
'                    strConStrInfoKey = GetAdoConStrKeyFromUserInputParametersDic(robjUserInputParametersDic)
'
'                    Debug.Print "At temporary Pd reading [ConStrInfoKey] - " & strConStrInfoKey
'
'                    'strPassword = GetRdbUserPasswordByAdoConStrPartKey(strConStrInfoKey)
'
'                    strPassword = GetStringFromCallBackInterfaceAndOneStringArgument("GetRdbUserPasswordByAdoConStrPartKey", GetAdoConStrKeyFromUserInputParametersDic(robjUserInputParametersDic))
'            End Select
'
'            If strPassword <> "" Then
'
'                robjUserInputParametersDic.Add mstrTemporaryPdKey, strPassword
'
'                blnIsOpeningFormNeeded = False
'            End If
'        End If
'    End If
'
'    IsOpeningFormNeededAfterTryToGetAdoConnectionPasswordFromTemporaryCache = blnIsOpeningFormNeeded
'End Function
'
'
''''
'''' This is applied to ODBC connection for both PostgreSQL, Oracle
''''
'Public Function IsSaveAdoConStrPasswordInTemporaryCacheFunctionEnabled() As Boolean
'
'    Dim blnIsToolVBModuleExisted As Boolean, blnReadValue As Boolean
'
'    If Not mblnIsTheValue_SaveAdoConStrPasswordInTemporaryCache_Initialized Then
'
'        ' confirm the SimplePdAuthenticationAdo.bas exists
'
'        blnIsToolVBModuleExisted = False
'
'        On Error Resume Next
'
'        blnIsToolVBModuleExisted = Application.Run("IsAdoPdAuthenticationCachingEnabled")
'
'        On Error GoTo 0
'
'        mblnIsTheFunction_SaveAdoConStrPasswordInTemporaryCache_Enabled = blnIsToolVBModuleExisted
'
'        If blnIsToolVBModuleExisted Then
'
'            ' read an enabled value from registry
'
'            blnReadValue = False
'
'            If IsCurUserRegBoolAboutAllowToSaveAdoConStrPasswordTemporarily(blnReadValue) Then
'
'                mblnSaveAdoConStrPasswordInTemporaryCache = blnReadValue
'            End If
'        End If
'
'        mblnIsTheValue_SaveAdoConStrPasswordInTemporaryCache_Initialized = True
'    End If
'
'    IsSaveAdoConStrPasswordInTemporaryCacheFunctionEnabled = mblnIsTheFunction_SaveAdoConStrPasswordInTemporaryCache_Enabled
'End Function
'
''''
''''
''''
'Public Function IsCurUserRegBoolAboutAllowToSaveAdoConStrPasswordTemporarily(ByRef rblnReadValue As Boolean) As Boolean
'
'    Dim blnIsValueRead As Boolean
'
'    blnIsValueRead = False
'
'    If IsCurUserRegBoolReadFromModuleNameAndSubjectSubKey(rblnReadValue, mstrModuleNameAndSubjectKeyForAdoConStrCachingMode, mstrWinRegValueNameOfAllowToSaveAdoConStrPasswordTemporarily) Then
'
'        blnIsValueRead = True
'    End If
'
'    IsCurUserRegBoolAboutAllowToSaveAdoConStrPasswordTemporarily = blnIsValueRead
'End Function
'
'
''''
''''
''''
'Public Sub SetAllowToSaveAdoConStrPasswordInTemporaryCache(ByVal vblnSaveAdoConStrPasswordInTemporaryCache As Boolean)
'
'    If IsSaveAdoConStrPasswordInTemporaryCacheFunctionEnabled() Then
'
'        If mblnSaveAdoConStrPasswordInTemporaryCache <> vblnSaveAdoConStrPasswordInTemporaryCache Then
'
'            mblnSaveAdoConStrPasswordInTemporaryCache = vblnSaveAdoConStrPasswordInTemporaryCache
'
'            WriteCurUserRegBoolByModuleNameAndSubjectSubKey mstrModuleNameAndSubjectKeyForAdoConStrCachingMode, mstrWinRegValueNameOfAllowToSaveAdoConStrPasswordTemporarily, vblnSaveAdoConStrPasswordInTemporaryCache
'        End If
'    End If
'End Sub
'Public Function GetAllowToSaveAdoConStrPasswordInTemporaryCache() As Boolean
'
'    Dim blnAllowToSave As Boolean, blnReadValue As Boolean
'
'    blnAllowToSave = False
'
'    If IsSaveAdoConStrPasswordInTemporaryCacheFunctionEnabled() Then
'
'        blnReadValue = False
'
'        If IsCurUserRegBoolAboutAllowToSaveAdoConStrPasswordTemporarily(blnReadValue) Then
'
'            mblnSaveAdoConStrPasswordInTemporaryCache = blnReadValue
'        End If
'
'        blnAllowToSave = mblnSaveAdoConStrPasswordInTemporaryCache
'    End If
'
'    GetAllowToSaveAdoConStrPasswordInTemporaryCache = blnAllowToSave
'End Function
'
''**---------------------------------------------
''** About Microsoft Excel file lock password caching
''**---------------------------------------------
''''
'''' This is applied to Excel book OLE DB connection
''''
'Public Function IsSaveMsExcelLockFilePasswordInTemporaryCacheFunctionEnabled() As Boolean
'
'    Dim blnIsToolVBModuleExisted As Boolean, blnReadValue As Boolean
'
'    If Not mblnIsTheValue_SaveMsExcelFileLockPasswordInTemporaryCache_Initialized Then
'
'        ' confirm the SimplePdAuthenticationAdo.bas exists
'
'        blnIsToolVBModuleExisted = False
'
'        On Error Resume Next
'
'        blnIsToolVBModuleExisted = Application.Run("IsMsExcelFilePdCachingEnabled")
'
'        On Error GoTo 0
'
'        mblnIsTheFunction_SaveMsExcelFileLockPasswordInTemporaryCache_Enabled = blnIsToolVBModuleExisted
'
'        If blnIsToolVBModuleExisted Then
'
'            ' read an enabled value from registry
'
'            blnReadValue = False
'
'            If IsCurUserRegBoolReadFromModuleNameAndSubjectSubKey(blnReadValue, mstrModuleNameAndSubjectKeyForAdoConStrCachingMode, mstrWinRegValueNameOfAllowToSaveMsExcelLockFilePasswordTemporarily) Then
'
'                mblnSaveMsExcelFileLockPasswordInTemporaryCache = blnReadValue
'            End If
'        End If
'
'        mblnIsTheValue_SaveMsExcelFileLockPasswordInTemporaryCache_Initialized = True
'    End If
'
'    IsSaveMsExcelLockFilePasswordInTemporaryCacheFunctionEnabled = mblnIsTheFunction_SaveMsExcelFileLockPasswordInTemporaryCache_Enabled
'End Function
'
'
''''
''''
''''
'Public Sub SetAllowToSaveMsExcelLockFilePasswordInTemporaryCache(ByVal vblnSaveMsExcelLockFilePasswordInTemporaryCache As Boolean)
'
'    If IsSaveMsExcelLockFilePasswordInTemporaryCacheFunctionEnabled() Then
'
'        If mblnSaveMsExcelLockFilePasswordInTemporaryCache <> vblnSaveMsExcelLockFilePasswordInTemporaryCache Then
'
'            mblnSaveMsExcelLockFilePasswordInTemporaryCache = vblnSaveMsExcelLockFilePasswordInTemporaryCache
'
'            WriteCurUserRegBoolByModuleNameAndSubjectSubKey mstrModuleNameAndSubjectKeyForAdoConStrCachingMode, mstrWinRegValueNameOfAllowToSaveMsExcelLockFilePasswordTemporarily, vblnSaveMsExcelLockFilePasswordInTemporaryCache
'        End If
'    End If
'End Sub
'Public Function GetAllowToSaveMsExcelLockFilePasswordInTemporaryCache() As Boolean
'
'    Dim blnAllowToSave As Boolean
'
'    blnAllowToSave = False
'
'    If IsSaveMsExcelLockFilePasswordInTemporaryCacheFunctionEnabled() Then
'
'        blnAllowToSave = mblnSaveMsExcelLockFilePasswordInTemporaryCache
'    End If
'
'    GetAllowToSaveMsExcelLockFilePasswordInTemporaryCache = blnAllowToSave
'End Function
'
'
'
''**---------------------------------------------
''** About Microsoft Access database file lock password caching
''**---------------------------------------------
''''
'''' This is applied to Access database OLE DB connection
''''
'Public Function IsSaveMsAccessDbLockFilePasswordInTemporaryCacheFunctionEnabled() As Boolean
'
'    Dim blnIsToolVBModuleExisted As Boolean, blnReadValue As Boolean
'
'    If Not mblnIsTheValue_SaveMsAccessDbFileLockPasswordInTemporaryCache_Initialized Then
'
'        ' confirm the SimplePdAuthenticationAdo.bas exists
'
'        blnIsToolVBModuleExisted = False
'
'        On Error Resume Next
'
'        blnIsToolVBModuleExisted = Application.Run("IsMsAccessDbFilePdCachingEnabled")
'
'        On Error GoTo 0
'
'        mblnIsTheFunction_SaveMsAccessDbFileLockPasswordInTemporaryCache_Enabled = blnIsToolVBModuleExisted
'
'        If blnIsToolVBModuleExisted Then
'
'            ' read an enabled value from registry
'
'            blnReadValue = False
'
'            If IsCurUserRegBoolReadFromModuleNameAndSubjectSubKey(blnReadValue, mstrModuleNameAndSubjectKeyForAdoConStrCachingMode, mstrWinRegValueNameOfAllowToSaveMsAccessDbLockFilePasswordTemporarily) Then
'
'                mblnSaveMsAccessDbFileLockPasswordInTemporaryCache = blnReadValue
'            End If
'        End If
'
'        mblnIsTheValue_SaveMsAccessDbFileLockPasswordInTemporaryCache_Initialized = True
'    End If
'
'    IsSaveMsAccessDbLockFilePasswordInTemporaryCacheFunctionEnabled = mblnIsTheFunction_SaveMsAccessDbFileLockPasswordInTemporaryCache_Enabled
'End Function
'
'
''''
''''
''''
'Public Sub SetAllowToSaveMsAccessDbLockFilePasswordInTemporaryCache(ByVal vblnSaveMsAccessDbLockFilePasswordInTemporaryCache As Boolean)
'
'    If IsSaveMsAccessDbLockFilePasswordInTemporaryCacheFunctionEnabled() Then
'
'        If mblnSaveMsAccessDbLockFilePasswordInTemporaryCache <> vblnSaveMsAccessDbLockFilePasswordInTemporaryCache Then
'
'            mblnSaveMsAccessDbLockFilePasswordInTemporaryCache = vblnSaveMsAccessDbLockFilePasswordInTemporaryCache
'
'            WriteCurUserRegBoolByModuleNameAndSubjectSubKey mstrModuleNameAndSubjectKeyForAdoConStrCachingMode, mstrWinRegValueNameOfAllowToSaveMsAccessDbLockFilePasswordTemporarily, vblnSaveMsAccessDbLockFilePasswordInTemporaryCache
'        End If
'    End If
'End Sub
'Public Function GetAllowToSaveMsAccessDbLockFilePasswordInTemporaryCache() As Boolean
'
'    Dim blnAllowToSave As Boolean
'
'    blnAllowToSave = False
'
'    If IsSaveMsAccessDbLockFilePasswordInTemporaryCacheFunctionEnabled() Then
'
'        blnAllowToSave = mblnSaveMsAccessDbLockFilePasswordInTemporaryCache
'    End If
'
'    GetAllowToSaveMsAccessDbLockFilePasswordInTemporaryCache = blnAllowToSave
'End Function
'
''**---------------------------------------------
''** Write or read from Windows-regisrty using FormStateToSetAdoConStr object
''**---------------------------------------------
''''
''''
''''
'Public Function GetFormStateToSetAdoConStrByDefaultParams(ByRef robjUserInputParametersDic As Scripting.Dictionary, ByRef rstrSettingKeyName As String) As FormStateToSetAdoConStr
'
'    Dim objFormStateToSetAdoConStr As FormStateToSetAdoConStr
'
'    Set objFormStateToSetAdoConStr = New FormStateToSetAdoConStr
'
'    With objFormStateToSetAdoConStr
'
'        Set .UserInputParametersDic = robjUserInputParametersDic
'
'        .SettingKeyName = rstrSettingKeyName
'    End With
'
'    Set GetFormStateToSetAdoConStrByDefaultParams = objFormStateToSetAdoConStr
'End Function
'
''''
''''
''''
'''' <Argument>rblnIsRequestedToCancelAdoConnection: Output</Argument>
'''' <Argument>robjUserInputParametersDic: Output</Argument>
'''' <Argument>robjFormStateToSetAdoConStr: Input</Argument>
'Public Sub AfterProcessAdoConStrFormSetting(ByRef rblnIsRequestedToCancelAdoConnection As Boolean, _
'        ByRef robjUserInputParametersDic As Scripting.Dictionary, _
'        ByRef robjFormStateToSetAdoConStr As FormStateToSetAdoConStr)
'
'    If Not robjFormStateToSetAdoConStr.IsRequestedToCancelAdoConnection Then
'
'        If GetAllowToSaveAdoConStrPasswordInTemporaryCache() Then
'
'            UpdateWinRegKeyValuesOfAdoConnectionStringInfoFromAdoConStrFormStateDic robjFormStateToSetAdoConStr
'        End If
'
'        Set robjUserInputParametersDic = robjFormStateToSetAdoConStr.UserInputParametersDic
'    Else
'        rblnIsRequestedToCancelAdoConnection = True
'    End If
'End Sub
'
'
''''
'''' write ADO connection string information to a Windows-registry
''''
'''' <Argument>robjFormStateToSetAdoConStr: Input</Argument>
'Public Sub UpdateWinRegKeyValuesOfAdoConnectionStringInfoFromAdoConStrFormStateDic(ByRef robjFormStateToSetAdoConStr As FormStateToSetAdoConStr)
'
'    With robjFormStateToSetAdoConStr
'
'        msubManagePasswordOfUserInputParametersDic .UserInputParametersDic
'
'        msubUpdateWinRegKeyValuesBySpecifyingChildSubKeyNameFromAdoConStrInfoDic .SettingKeyName, .UserInputParametersDic
'    End With
'End Sub
'
''''
''''
''''
'Private Sub msubUpdateWinRegKeyValuesBySpecifyingChildSubKeyNameFromAdoConStrInfoDic(ByRef rstrSettingKeyName As String, ByRef robjUserInputParametersDic As Scripting.Dictionary)
'
'    Dim objDic As Scripting.Dictionary
'
'    Set objDic = GetFilteredDicExceptForSpecifiedPluralKeys(robjUserInputParametersDic, mstrTemporaryPdKey)
'
'    UpdateWinRegKeyValuesBySpecifyingChildSubKeyNameFromDic mstrRegKeySecureOfficeCommonVBAAdoConStr, rstrSettingKeyName, objDic
'End Sub
'
'
''**---------------------------------------------
''** Write or read from Windows-regisrty
''**---------------------------------------------
''''
'''' write ADO connection string information to a Windows-registry
''''
'''' <Argument>rstrSettingKeyName: Input - This is same as Windows-registry child sub-key name</Argument>
'''' <Argument>robjUserInputParametersDic: Input</Argument>
'Public Sub UpdateWinRegKeyValuesOfAdoConnectionStringInfoFromDic(ByRef rstrSettingKeyName As String, ByRef robjUserInputParametersDic As Scripting.Dictionary)
'
'    UpdateWinRegKeyValuesBySpecifyingChildSubKeyNameFromDic mstrRegKeySecureOfficeCommonVBAAdoConStr, rstrSettingKeyName, robjUserInputParametersDic
'End Sub
'
''''
'''' delete ADO connection string information from a Windows-registry
''''
'''' <Argument>rstrSettingKeyName: Input - This is same as Windows-registry child sub-key name</Argument>
'Public Sub DeleteWinRegKeyValuesOfAdoConnectionStringInfo(ByRef rstrSettingKeyName As String)
'
'    DeleteWinRegKeyValuesBySpecifyingChildSubKeyName mstrRegKeySecureOfficeCommonVBAAdoConStr, rstrSettingKeyName
'End Sub
'
''''
'''' read ADO connection string information as a Dictionary object
''''
'''' <Argument>rstrSettingKeyName: Input - This is same as Windows-registry child sub-key name</Argument>
'''' <Return>Dictionary(Of String[ValueName], Variant[Value])</Return>
'Public Function GetDicOfAdoConnectionStringInfoFromReadingWinRegValues(ByRef rstrSettingKeyName As String) As Scripting.Dictionary
'
'    Set GetDicOfAdoConnectionStringInfoFromReadingWinRegValues = GetDicBySpecifyingChildSubKeyNameFromReadingWinRegValues(mstrRegKeySecureOfficeCommonVBAAdoConStr, rstrSettingKeyName)
'End Function
'
''''
''''
''''
'Public Function GetFilteredOneNestedDicOfAllAdoConnectionStringInfoFromReadingWinRegChiledKeysAndValuesOfEachKey(ByVal venmOleDbOrOdbcAdoConnectionDestinationRDBType As OleDbOrOdbcAdoConnectionDestinationRDBType) As Scripting.Dictionary
'
'    Dim objFilteredOneNestedDic As Scripting.Dictionary, varSettingKeyName As Variant, objChildDic As Scripting.Dictionary
'
'    Set objFilteredOneNestedDic = New Scripting.Dictionary
'
'    With GetOneNestedDicOfAllAdoConnectionStringInfoFromReadingWinRegChiledKeysAndValuesOfEachKey()
'
'        For Each varSettingKeyName In .Keys
'
'            Set objChildDic = .Item(varSettingKeyName)
'
'            With objChildDic
'
'                If .Exists(mstrAdoConnectionTypeAndDestinationKey) Then
'
'                    If GetEnumOfOleDbOrOdbcAdoConnectionDestinationRDBTypeFromDescription(.Item(mstrAdoConnectionTypeAndDestinationKey)) = venmOleDbOrOdbcAdoConnectionDestinationRDBType Then
'
'                        objFilteredOneNestedDic.Add varSettingKeyName, objChildDic
'                    End If
'                End If
'            End With
'        Next
'    End With
'
'    Set GetFilteredOneNestedDicOfAllAdoConnectionStringInfoFromReadingWinRegChiledKeysAndValuesOfEachKey = objFilteredOneNestedDic
'End Function
'
'
'
''''
'''' read ADO current all connection settings in the current user Windows-registry
''''
'''' <Return>Dictionary(Of String[ChildSubKeyName], Dictionary(Of String[ValueName], Variant[Value]))</Return>
'Public Function GetOneNestedDicOfAllAdoConnectionStringInfoFromReadingWinRegChiledKeysAndValuesOfEachKey() As Scripting.Dictionary
'
'    Set GetOneNestedDicOfAllAdoConnectionStringInfoFromReadingWinRegChiledKeysAndValuesOfEachKey = GetOneNestedDicFromReadingWinRegChiledKeysAndValuesOfEachKey(mstrRegKeySecureOfficeCommonVBAAdoConStr)
'End Function
'
'
''''
''''
''''
'Public Function GetAdoConStrKeyFromUserInputParametersDic(ByRef robjUserInputParametersDic As Scripting.Dictionary) As String
'
'    Dim strConStrPartKey As String, varKey As Variant, strKey As String
'
'    strConStrPartKey = ""
'
'    With robjUserInputParametersDic
'
'        For Each varKey In .Keys
'
'            strKey = varKey
'
'            Select Case strKey
'
'                Case mstrAdoConnectionTypeAndDestinationKey, mstrTemporaryPdKey
'
'                    ' nothing to do
'
'                Case Else
'
'                    If strConStrPartKey <> "" Then
'
'                        strConStrPartKey = strConStrPartKey & ";"
'                    End If
'
'                    strConStrPartKey = strConStrPartKey & .Item(strKey)
'            End Select
'        Next
'    End With
'
'    GetAdoConStrKeyFromUserInputParametersDic = strConStrPartKey
'End Function
'
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
''**---------------------------------------------
''** Write or read from Windows-regisrty using FormStateToSetAdoConStr object
''**---------------------------------------------
''''
''''
''''
'Private Sub msubManagePasswordOfUserInputParametersDic(ByRef robjUserInputParametersDic As Scripting.Dictionary)
'
'    Dim enmOleDbOrOdbcAdoConnectionDestinationRDBType As OleDbOrOdbcAdoConnectionDestinationRDBType, strPassword As String, strConStrInfoKey As String, strPath As String
'
'    With robjUserInputParametersDic
'
'        If .Exists(mstrTemporaryPdKey) Then
'
'            If GetAllowToSaveAdoConStrPasswordInTemporaryCache() Then
'
'                ' Temporarily cache password
'
'                strPassword = .Item(mstrTemporaryPdKey)
'
'                enmOleDbOrOdbcAdoConnectionDestinationRDBType = GetEnumOfOleDbOrOdbcAdoConnectionDestinationRDBTypeFromDescription(.Item(mstrAdoConnectionTypeAndDestinationKey))
'
'                Select Case enmOleDbOrOdbcAdoConnectionDestinationRDBType
'
'                    Case OleDbOrOdbcAdoConnectionDestinationRDBType.AdoOleDbAndMsExcel
'
'                        If IsSaveMsExcelLockFilePasswordInTemporaryCacheFunctionEnabled() Then
'
'                            ' If the SimplePdAuthentication.bas had been removed in this VB project, the following doesn't causes a VBA syntax error
'
'                            strPath = .Item(mstrAdoDbFilePathKey)
'
'                            CallBackInterfaceByTwoStringArguments "StorePasswordOfBookTemporarily", strPath, strPassword
'                        End If
'
'                    Case OleDbOrOdbcAdoConnectionDestinationRDBType.AdoOleDbAndMsAccessDb
'
'                        If IsSaveMsAccessDbLockFilePasswordInTemporaryCacheFunctionEnabled() Then
'
'                            ' If the SimplePdAuthentication.bas had been removed in this VB project, the following doesn't causes a VBA syntax error
'
'                            strPath = .Item(mstrAdoDbFilePathKey)
'
'                            CallBackInterfaceByTwoStringArguments "StorePasswordOfAccdbTemporarily", strPath, strPassword
'                        End If
'
'                    Case Else
'
'                        If IsSaveAdoConStrPasswordInTemporaryCacheFunctionEnabled() Then
'
'                            ' If the SimplePdAuthenticationAdo.bas had been removed in this VB project, the following doesn't causes a VBA syntax error
'
'                            strConStrInfoKey = GetAdoConStrKeyFromUserInputParametersDic(robjUserInputParametersDic)
'
'                            Debug.Print "At temporary Pd writing [ConStrInfoKey] - " & strConStrInfoKey
'
'                            'StoreRdbUserPasswordByAdoConStrPartKey strConStrInfoKey, strPassword
'
'                            CallBackInterfaceByTwoStringArguments "StoreRdbUserPasswordByAdoConStrPartKey", GetAdoConStrKeyFromUserInputParametersDic(robjUserInputParametersDic), strPassword
'                        End If
'                End Select
'            End If
'        End If
'    End With
'End Sub
'
'
''**---------------------------------------------
''** View the current ADO connection settings in the current user Windows registry
''**---------------------------------------------
''''
''''
''''
'Private Sub OutputWinRegValuesOneNestedDicOfAllAdoConnectionStringInfoInTheCurrentUserWinRegistryKeyToBook()
'
'    Dim strBookPath As String, objOneNestedDic As Scripting.Dictionary
'
'    Set objOneNestedDic = GetOneNestedDicOfAllAdoConnectionStringInfoFromReadingWinRegChiledKeysAndValuesOfEachKey()
'
'    If objOneNestedDic.Count > 0 Then
'
'        strBookPath = GetReadWinRegValuesTemporaryDirectoryPath() & "\AdoConnectionSettingWindowsRegistryCache.xlsx"
'
'        OutputWinRegValuesOneNestedDicInSpecifiedWinRegistryKeyToBook mstrRegKeySecureOfficeCommonVBAAdoConStr, objOneNestedDic, strBookPath
'    Else
'        MsgBox "The current ADO connection string info has not existed yet.", vbExclamation Or vbOKOnly, "Current user Win-resgistory cache"
'    End If
'End Sub
'
'''--VBA_Code_File--<ADOConStrToolsForExcelBook.bas>--
'Attribute VB_Name = "ADOConStrToolsForExcelBook"
''
''   Generate ADODB connection string to a Excel book file
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on ADOConStrOfAceOleDbXl.cls
''       Despite this is dependent on ADO, it is a common codes for localizing various captions
''       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Tue,  7/Feb/2023    Kalmclaeyd Tarclanus    Separated from ADOConnectStrGenerator.cls
''
'
'Option Explicit
'
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''**---------------------------------------------
''** for control USetAdoOleDbConStrForAccdb user-form
''**---------------------------------------------
''''
''''
''''
'Public Function GetInitialUserInputParametersDicForAdoOleDbConnectionOfMsExcelSheet(Optional ByVal vstrBookPath As String = "", _
'        Optional ByVal venmIMEXMode As IMEXMode = IMEXMode.NoUseIMEXMode, _
'        Optional ByVal vblnHeaderFieldTitleExists As Boolean = True, _
'        Optional ByVal vblnBookReadOnly As Boolean = False, _
'        Optional ByVal venmAccessOLEDBProviderType As AccessOLEDBProviderType = AccessOLEDBProviderType.AceOLEDB_12P0, _
'        Optional ByVal vstrBookLockPassword As String = "") As Scripting.Dictionary
'
'    Dim objDic As Scripting.Dictionary
'
'    Set objDic = New Scripting.Dictionary
'
'    With objDic
'
'        .Add mstrAdoDbFilePathKey, vstrBookPath
'
'        .Add "OLE_DB_Provider", GetAccessOLEDBProviderNameFromEnm(venmAccessOLEDBProviderType)
'
'        .Add "HeaderColumnTitleExists", vblnHeaderFieldTitleExists
'
'        .Add "BookReadOnly", vblnBookReadOnly
'
'        If vstrBookLockPassword <> "" Then
'
'            .Add mstrTemporaryPdKey, vstrBookLockPassword
'        End If
'
'        If venmIMEXMode <> NoUseIMEXMode Then
'
'            .Add "IMEX", venmIMEXMode
'        End If
'
'        .Add mstrAdoConnectionTypeAndDestinationKey, GetDescriptionOfOleDbOrOdbcAdoConnectionDestinationRDBType(AdoOleDbAndMsExcel)
'    End With
'
'    Set GetInitialUserInputParametersDicForAdoOleDbConnectionOfMsExcelSheet = objDic
'End Function
'
'
''''
''''
''''
'Public Sub UpdateADOConStrOfAccessOleDbToExcelSheetFromInputDic(ByRef robjADOConStrOfAceOledbXl As ADOConStrOfAceOleDbXl, ByRef robjUserInputParametersDic As Scripting.Dictionary)
'
'    Dim strKey As String, enmIMEXMode As IMEXMode, blnHeaderFieldTitleExists As Boolean, blnBookReadOnly As Boolean, enmAccessOLEDBProviderType As AccessOLEDBProviderType
'
'    With robjUserInputParametersDic
'
'        strKey = "OLE_DB_Provider"
'
'        enmAccessOLEDBProviderType = AceOLEDB_12P0
'
'        If .Exists(strKey) Then
'
'            enmAccessOLEDBProviderType = GetAccessOLEDBProviderEnmFromName(.Item(strKey))
'        End If
'
'
'        enmIMEXMode = NoUseIMEXMode
'
'        strKey = "IMEX"
'
'        If .Exists(strKey) Then
'
'            enmIMEXMode = GetAdoIMEXModeEnmFromName(.Item(strKey))
'        End If
'
'
'        blnHeaderFieldTitleExists = True
'
'        strKey = "HeaderColumnTitleExists"
'
'        If .Exists(strKey) Then
'
'            blnHeaderFieldTitleExists = CBool(.Item(strKey))
'        End If
'
'        blnBookReadOnly = False
'
'        strKey = "BookReadOnly"
'
'        If .Exists(strKey) Then
'
'            blnBookReadOnly = CBool(.Item(strKey))
'        End If
'
'        robjADOConStrOfAceOledbXl.SetExcelBookConnectionInDetails .Item(mstrAdoDbFilePathKey), enmIMEXMode, blnHeaderFieldTitleExists, blnBookReadOnly, enmAccessOLEDBProviderType
'    End With
'End Sub
'
'
''''
''''
''''
'Public Function GetADOConStrOfAccessOleDbToExcelSheetFromInputDic(ByRef robjUserInputParametersDic As Scripting.Dictionary) As ADOConStrOfAceOleDbXl
'
'    Dim objADOConStrOfAceOledbXl As ADOConStrOfAceOleDbXl
'
'    Set objADOConStrOfAceOledbXl = New ADOConStrOfAceOleDbXl
'
'    UpdateADOConStrOfAccessOleDbToExcelSheetFromInputDic objADOConStrOfAceOledbXl, robjUserInputParametersDic
'
'    Set GetADOConStrOfAccessOleDbToExcelSheetFromInputDic = objADOConStrOfAceOledbXl
'End Function
'
'
'
''**---------------------------------------------
''** ADO connection string parameters solution interfaces by each special form
''**---------------------------------------------
''''
''''
''''
'''' <Argument>rblnIsRequestedToCancelAdoConnection: Output</Argument>
'''' <Argument>robjADOConStrOfAceOledbXl: Output</Argument>
'''' <Argument>vstrSettingKeyName: Input</Argument>
'''' <Argument>vstrBookPath: Input</Argument>
'''' <Argument>venmIMEXMode: Input</Argument>
'''' <Argument>vblnHeaderFieldTitleExists: Input</Argument>
'''' <Argument>vblnBookReadOnly: Input</Argument>
'''' <Argument>venmAccessOLEDBProviderType: Input</Argument>
'''' <Argument>vstrBookLockPassword: Input</Argument>
'Public Sub SetOleDbConnectionParametersByAccessOleDbToExcelSheetForm(ByRef rblnIsRequestedToCancelAdoConnection As Boolean, _
'        ByRef robjADOConStrOfAceOledbXl As ADOConStrOfAceOleDbXl, _
'        ByVal vstrSettingKeyName As String, _
'        Optional ByVal vstrBookPath As String = "", _
'        Optional ByVal venmIMEXMode As IMEXMode = IMEXMode.NoUseIMEXMode, _
'        Optional ByVal vblnHeaderFieldTitleExists As Boolean = True, _
'        Optional ByVal vblnBookReadOnly As Boolean = False, _
'        Optional ByVal venmAccessOLEDBProviderType As AccessOLEDBProviderType = AccessOLEDBProviderType.AceOLEDB_12P0, _
'        Optional ByVal vstrBookLockPassword As String = "", _
'        Optional ByVal vblnAllowToOpenAdoConnectingExcelBookWhenItIsLockedByPassword As Boolean = False)
'
'    Dim objUserInputParametersDic As Scripting.Dictionary, strPassword As String, blnIsOpeningFormNeeded As Boolean
'
'    rblnIsRequestedToCancelAdoConnection = False
'
'    blnIsOpeningFormNeeded = IsOpeningFormNeededAfterLoadFromUserInputParametersDicFirstCacheAndSecondFunctionInputsOfAccessOleDbToExcelSheet(objUserInputParametersDic, vstrSettingKeyName, vstrBookPath, venmIMEXMode, vblnHeaderFieldTitleExists, vblnBookReadOnly, venmAccessOLEDBProviderType, vstrBookLockPassword)
'
'    If blnIsOpeningFormNeeded Then
'
'        msubSetUserInputParametersDicOfAccessOleDbToExcelSheetByOpeningFormWithFixedSettingKeyName rblnIsRequestedToCancelAdoConnection, objUserInputParametersDic, vstrSettingKeyName, vblnAllowToOpenAdoConnectingExcelBookWhenItIsLockedByPassword
'    Else
'        If vblnAllowToOpenAdoConnectingExcelBookWhenItIsLockedByPassword Then
'
'            msubOpenPasswordLockedExcelBookIfItIsNeeded objUserInputParametersDic
'        End If
'    End If
'
'    If Not rblnIsRequestedToCancelAdoConnection Then
'
'        UpdateADOConStrOfAccessOleDbToExcelSheetFromInputDic robjADOConStrOfAceOledbXl, objUserInputParametersDic
'    End If
'End Sub
'
'
'
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
''''
''''
''''
'''' <Argument>rblnIsRequestedToCancelAdoConnection: Output</Argument>
'''' <Argument>robjUserInputParametersDic: Input - Output</Argument>
'''' <Argument>rstrSettingKeyName: Input</Argument>
'Private Sub msubSetUserInputParametersDicOfAccessOleDbToExcelSheetByOpeningFormWithFixedSettingKeyName(ByRef rblnIsRequestedToCancelAdoConnection As Boolean, _
'        ByRef robjUserInputParametersDic As Scripting.Dictionary, _
'        ByRef rstrSettingKeyName As String, _
'        Optional ByVal vblnAllowToOpenAdoConnectingExcelBookWhenItIsLockedByPassword As Boolean = False)
'
'
'    Dim objFormStateToSetAdoConStr As FormStateToSetAdoConStr, objUSetAdoOleDbConStrForXlBook As USetAdoOleDbConStrForXlBook
'
'    Set objFormStateToSetAdoConStr = GetFormStateToSetAdoConStrByDefaultParams(robjUserInputParametersDic, rstrSettingKeyName)
'
'    Set objUSetAdoOleDbConStrForXlBook = New USetAdoOleDbConStrForXlBook
'
'    With objUSetAdoOleDbConStrForXlBook
'
'        .SetInitialFormStateToSetAdoConStr objFormStateToSetAdoConStr, True
'
'        .Show vbModal
'    End With
'
'    If vblnAllowToOpenAdoConnectingExcelBookWhenItIsLockedByPassword Then
'
'        msubOpenPasswordLockedExcelBookIfItIsNeeded robjUserInputParametersDic
'    End If
'
'    AfterProcessAdoConStrFormSetting rblnIsRequestedToCancelAdoConnection, robjUserInputParametersDic, objFormStateToSetAdoConStr
'End Sub
'
''''
''''
''''
'Private Sub msubOpenPasswordLockedExcelBookIfItIsNeeded(ByRef robjUserInputParametersDic As Scripting.Dictionary)
'
'    Dim objBook As Excel.Workbook
'
'    With robjUserInputParametersDic
'
'        If .Exists(mstrTemporaryPdKey) Then
'
'            If .Item(mstrTemporaryPdKey) <> "" Then
'
'                Set objBook = GetWorkbook(.Item(mstrAdoDbFilePathKey), False, vstrPassword:=.Item(mstrTemporaryPdKey))
'            End If
'        End If
'    End With
'End Sub
'
'
''''
''''
''''
'''' <Argument>robjUserInputParametersDic: Output</Argument>
'''' <Argument>vstrSettingKeyName: Input</Argument>
'''' <Argument>vstrBookPath: Input</Argument>
'''' <Argument>venmIMEXMode: Input</Argument>
'''' <Argument>vblnHeaderFieldTitleExists: Input</Argument>
'''' <Argument>vblnBookReadOnly: Input</Argument>
'''' <Argument>venmAccessOLEDBProviderType: Input</Argument>
'''' <Argument>vstrBookLockPassword: Input</Argument>
'Private Function IsOpeningFormNeededAfterLoadFromUserInputParametersDicFirstCacheAndSecondFunctionInputsOfAccessOleDbToExcelSheet(ByRef robjUserInputParametersDic As Scripting.Dictionary, _
'        ByVal vstrSettingKeyName As String, _
'        Optional ByVal vstrBookPath As String = "", _
'        Optional ByVal venmIMEXMode As IMEXMode = IMEXMode.NoUseIMEXMode, _
'        Optional ByVal vblnHeaderFieldTitleExists As Boolean = True, _
'        Optional ByVal vblnBookReadOnly As Boolean = False, _
'        Optional ByVal venmAccessOLEDBProviderType As AccessOLEDBProviderType = AccessOLEDBProviderType.AceOLEDB_12P0, _
'        Optional ByVal vstrBookLockPassword As String = "") As Boolean
'
'    Dim blnIsOpeningFormNeeded As Boolean
'
'    blnIsOpeningFormNeeded = True
'
'    ' Try to get parameters from Win-registry cache
'
'    Set robjUserInputParametersDic = GetDicOfAdoConnectionStringInfoFromReadingWinRegValues(vstrSettingKeyName)
'
'    Select Case True
'
'        Case robjUserInputParametersDic Is Nothing, robjUserInputParametersDic.Count = 0
'
'            blnIsOpeningFormNeeded = IsOpeningFormNeededAfterInitializingUserInputPrametersDicOfAccessOleDbToExcelSheet(robjUserInputParametersDic, vstrBookPath, venmIMEXMode, vblnHeaderFieldTitleExists, vblnBookReadOnly, venmAccessOLEDBProviderType, vstrBookLockPassword)
'        Case Else
'            blnIsOpeningFormNeeded = IsOpeningFormNeededAfterTryToGetAdoConnectionPasswordFromTemporaryCache(robjUserInputParametersDic)
'    End Select
'
'    IsOpeningFormNeededAfterLoadFromUserInputParametersDicFirstCacheAndSecondFunctionInputsOfAccessOleDbToExcelSheet = blnIsOpeningFormNeeded
'End Function
'
'
'
''''
''''
''''
'''' <Argument>robjUserInputParametersDic: Output</Argument>
'''' <Argument>vstrBookPath: Input</Argument>
'''' <Argument>venmIMEXMode: Input</Argument>
'''' <Argument>vblnHeaderFieldTitleExists: Input</Argument>
'''' <Argument>vblnBookReadOnly: Input</Argument>
'''' <Argument>venmAccessOLEDBProviderType: Input</Argument>
'''' <Argument>vstrBookLockPassword: Input</Argument>
'Private Function IsOpeningFormNeededAfterInitializingUserInputPrametersDicOfAccessOleDbToExcelSheet(ByRef robjUserInputParametersDic As Scripting.Dictionary, _
'        Optional ByVal vstrBookPath As String = "", _
'        Optional ByVal venmIMEXMode As IMEXMode = IMEXMode.NoUseIMEXMode, _
'        Optional ByVal vblnHeaderFieldTitleExists As Boolean = True, _
'        Optional ByVal vblnBookReadOnly As Boolean = False, _
'        Optional ByVal venmAccessOLEDBProviderType As AccessOLEDBProviderType = AccessOLEDBProviderType.AceOLEDB_12P0, _
'        Optional ByVal vstrBookLockPassword As String = "") As Boolean
'
'
'    Dim blnIsOpeningFormNeeded As Boolean, objBook As Excel.Workbook
'
'    blnIsOpeningFormNeeded = True
'
'    Set robjUserInputParametersDic = GetInitialUserInputParametersDicForAdoOleDbConnectionOfMsExcelSheet(vstrBookPath, venmIMEXMode, vblnHeaderFieldTitleExists, vblnBookReadOnly, venmAccessOLEDBProviderType, vstrBookLockPassword)
'
'    If vstrBookLockPassword <> "" And vstrBookPath <> "" Then
'
'        ' When the Excel book file is locked by a password, the ADO connection is able after the Excel book is opened
'
'        Set objBook = GetWorkbook(vstrBookPath, False, vstrPassword:=vstrBookLockPassword)
'    End If
'
'    If IsADOConnectionStringEnabled(GetADOConStrOfAccessOleDbToExcelSheetFromInputDic(robjUserInputParametersDic).GetConnectionString()) Then
'
'        blnIsOpeningFormNeeded = False
'    End If
'
'    IsOpeningFormNeededAfterInitializingUserInputPrametersDicOfAccessOleDbToExcelSheet = blnIsOpeningFormNeeded
'End Function
'
'
''///////////////////////////////////////////////
''/// Sanity tests
''///////////////////////////////////////////////
''''
'''' delete testing SettingKeyName from the registory
''''
'Private Sub msubSanityTestToDeleteTestingSettingKeyNamesForConnectingToAccessOleDbWithExcelSheet()
'
'    DeleteWinRegKeyValuesOfAdoConnectionStringInfo "TestAccOleDbToExcelKey"
'
'    DeleteWinRegKeyValuesOfAdoConnectionStringInfo "TestAccOleDbToExcelWithPasswordKey"
'End Sub
'
'
''''
''''
''''
'Private Sub msubSanityTestToOpenUSetAdoOleDbConStrForAccessOleDbWithExcelSheet()
'
'    Dim objUserInputParametersDic As Scripting.Dictionary, blnIsRequestedToCancelAdoConnection As Boolean
'
'    Set objUserInputParametersDic = GetInitialUserInputParametersDicForAdoOleDbConnectionOfMsExcelSheet()
'
'    msubSetUserInputParametersDicOfAccessOleDbToExcelSheetByOpeningFormWithFixedSettingKeyName blnIsRequestedToCancelAdoConnection, objUserInputParametersDic, "TestAccOleDbToExcelKey"
'
'    If Not blnIsRequestedToCancelAdoConnection Then
'
'        If IsADOConnectionStringEnabled(GetADOConStrOfAccessOleDbToExcelSheetFromInputDic(objUserInputParametersDic).GetConnectionString()) Then
'
'            Debug.Print "Connected to Excel sheet by Ms Access OLE DB using ADO"
'        End If
'    End If
'End Sub
'''--VBA_Code_File--<ADOConStrOfAceOleDbXl.cls>--
'VERSION 1.0 CLASS
'BEGIN
'  MultiUse = -1  'True
'END
'Attribute VB_Name = "ADOConStrOfAceOleDbXl"
'Attribute VB_GlobalNameSpace = False
'Attribute VB_Creatable = False
'Attribute VB_PredeclaredId = False
'Attribute VB_Exposed = False
''
''   ADO connection string generator for Access OLEDB connection with Excel.Worksheet
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Development Policy:
''       ADOConnector focus on the visualization of both the ADO error-trapping and SQL data-base error-trapping
''       LozalizationADOConnector doesn't support the error trapping visualization , logging of SQL results, and localizing captions of itself
''
''   Dependency Abstract:
''       Dependent on ADO
''       Dependent on Microsoft Office Access Data Base Engine OLE DB Provider,
''       which should have been the installed when the Microsoft Office Excel software has been installed.
''       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access.
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Thu,  9/Jun/2022    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Interface implementations
''///////////////////////////////////////////////
'Implements IADOConnectStrGenerator
'
''///////////////////////////////////////////////
''/// Declarations
''///////////////////////////////////////////////
'Private mobjCoreGenerator As ADOConStrOfAceOleDb
'
'
'Private menmAccessOLEDBProviderType As AccessOLEDBProviderType
'
'Private menmExcelBookSpecType As ExcelBookSpecType
'
'Private menmIMEXMode As IMEXMode
'
'Private mblnHeaderFieldTitleExists As Boolean
'
'Private mblnFileReadonly As Boolean
'
'Private mblnForceToUseAceOLEDBMacro As Boolean
'
'
''///////////////////////////////////////////////
''/// Event Handlers
''///////////////////////////////////////////////
'Private Sub Class_Initialize()
'
'    Set mobjCoreGenerator = New ADOConStrOfAceOleDb
'
'    mblnForceToUseAceOLEDBMacro = False
'End Sub
'
'
''///////////////////////////////////////////////
''/// Implemented operations
''///////////////////////////////////////////////
'Private Function IADOConnectStrGenerator_GetConnectionString() As String
'
'    IADOConnectStrGenerator_GetConnectionString = GetConnectionStringForXlBook()
'End Function
'
''///////////////////////////////////////////////
''/// Properties
''///////////////////////////////////////////////
''**---------------------------------------------
''** OLE DB connection
''**---------------------------------------------
''''
'''' read-only Provider Name for OLE DB
''''
'Public Property Get ProviderName() As String
'
'    ProviderName = mobjCoreGenerator.OleDbProviderName
'End Property
'
''**---------------------------------------------
''** Excel-book connection
''**---------------------------------------------
''''
'''' read-only Excel book path or ACCESS accdb path
''''
'Public Property Get ConnectingFilePath() As String
'
'    ConnectingFilePath = mobjCoreGenerator.ConnectingFilePath
'End Property
'
'Public Property Get BookReadOnly() As Boolean
'
'    BookReadOnly = mblnFileReadonly
'End Property
'
'Public Property Get HeaderFieldTitleExists() As Boolean
'
'    HeaderFieldTitleExists = mblnHeaderFieldTitleExists
'End Property
'
'Public Property Get IMEX() As IMEXMode
'
'    IMEX = menmIMEXMode
'End Property
'
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''''
''''
''''
'Public Function GetConnectionString() As String
'
'    GetConnectionString = IADOConnectStrGenerator_GetConnectionString
'End Function
'
''''
''''
''''
'Public Function GetConnectionStringForXlBook() As String
'
'    GetConnectionStringForXlBook = GetADODBConnectionOleDbStringToXlBook(mobjCoreGenerator.ConnectingFilePath, menmAccessOLEDBProviderType, menmExcelBookSpecType, mblnHeaderFieldTitleExists, mblnFileReadonly, menmIMEXMode)
'End Function
'
'
''**---------------------------------------------
''** Set ADODB Access OLE DB parameters directly
''**---------------------------------------------
''''
''''
''''
'Public Sub SetExcelBookConnection(ByVal vstrBookPath As String, _
'        ByVal venmIMEXMode As IMEXMode, _
'        Optional ByVal vblnHeaderFieldTitleExists As Boolean = True, _
'        Optional ByVal vblnBookReadOnly As Boolean = False, _
'        Optional ByVal vblnIsConnectedToOffice95 As Boolean = False)
'
'
'    mobjCoreGenerator.ConnectingFilePath = vstrBookPath
'
'    If mblnForceToUseAceOLEDBMacro Then
'
'        menmAccessOLEDBProviderType = AceOLEDB_12P0 ' for Excel2007 or lator
'
'        menmExcelBookSpecType = XlBookFileType12P0Macro
'    Else
'        Select Case LCase(GetExtensionNameByVbaDir(vstrBookPath))
'
'            Case "xls"
'
'                If vblnIsConnectedToOffice95 Then
'
'                    menmAccessOLEDBProviderType = JetOLEDB_4P0 ' for Excel97
'
'                    menmExcelBookSpecType = XlBookFileType5P0   ' for Excel95
'                Else
'                    menmAccessOLEDBProviderType = JetOLEDB_4P0
'
'                    menmExcelBookSpecType = XlBookFileType8P0   ' for Excel97, Excel2000, Excel2002
'                End If
'
'            Case "xlsx" ' book xml
'
'                menmAccessOLEDBProviderType = AceOLEDB_12P0 ' for Excel2007 or lator
'
'                menmExcelBookSpecType = XlBookFileType12P0Xml
'
'            Case "xlsb" ' book binary
'
'                menmAccessOLEDBProviderType = AceOLEDB_12P0 ' for Excel2007 or lator
'
'                menmExcelBookSpecType = XlBookFileType12P0B
'
'            Case "xlsm" ' book macro
'
'                menmAccessOLEDBProviderType = AceOLEDB_12P0 ' for Excel2007 or lator
'
'                menmExcelBookSpecType = XlBookFileType12P0Macro
'            Case Else
'
'                Debug.Print "!!! Atention - The Excel book might not be saved as a new file yet"
'
'                menmAccessOLEDBProviderType = AceOLEDB_12P0 ' for Excel2007 or lator
'
'                menmExcelBookSpecType = XlBookFileType12P0Xml
'        End Select
'    End If
'
'    mobjCoreGenerator.OleDbProviderName = GetAccessOLEDBProviderDescriptionFromEnm(menmAccessOLEDBProviderType)
'
'    menmIMEXMode = venmIMEXMode
'
'    mblnHeaderFieldTitleExists = vblnHeaderFieldTitleExists
'
'    mblnFileReadonly = vblnBookReadOnly
'End Sub
'
'
''''
''''
''''
'Public Sub SetExcelBookConnectionInDetails(ByVal vstrBookPath As String, _
'        ByVal venmIMEXMode As IMEXMode, _
'        Optional ByVal vblnHeaderFieldTitleExists As Boolean = True, _
'        Optional ByVal vblnBookReadOnly As Boolean = False, _
'        Optional ByVal venmAccessOLEDBProviderType As AccessOLEDBProviderType = AccessOLEDBProviderType.AceOLEDB_12P0, _
'        Optional ByVal vblnIsConnectedToOffice95 As Boolean = False)
'
'    With mobjCoreGenerator
'
'        .ConnectingFilePath = vstrBookPath
'
'        menmAccessOLEDBProviderType = venmAccessOLEDBProviderType
'
'        .OleDbProviderName = GetAccessOLEDBProviderDescriptionFromEnm(menmAccessOLEDBProviderType)
'    End With
'
'    Select Case LCase(GetExtensionNameByVbaDir(vstrBookPath))
'
'        Case "xls"
'
'            If vblnIsConnectedToOffice95 Then
'
'                menmExcelBookSpecType = XlBookFileType5P0   ' for Excel95
'            Else
'                menmExcelBookSpecType = XlBookFileType8P0   ' for Excel97, Excel2000, Excel2002
'            End If
'
'        Case "xlsx" ' book xml
'
'            menmExcelBookSpecType = XlBookFileType12P0Xml
'
'        Case "xlsb" ' book binary
'
'            menmExcelBookSpecType = XlBookFileType12P0B
'
'        Case "xlsm" ' book macro
'
'            menmExcelBookSpecType = XlBookFileType12P0Macro
'        Case Else
'
'
'    End Select
'
'    menmIMEXMode = venmIMEXMode
'
'    mblnHeaderFieldTitleExists = vblnHeaderFieldTitleExists
'
'    mblnFileReadonly = vblnBookReadOnly
'End Sub
'
'
''**---------------------------------------------
''** By a User-form, set ADODB ODBC parameters
''**---------------------------------------------
''''
''''
''''
'''' <Argument>vstrSettingKeyName: Input</Argument>
'''' <Argument>vstrBookPath: Input</Argument>
'''' <Argument>venmIMEXMode: Input</Argument>
'''' <Argument>vblnHeaderFieldTitleExists: Input</Argument>
'''' <Argument>vblnBookReadOnly: Input</Argument>
'''' <Argument>venmAccessOLEDBProviderType: Input</Argument>
'''' <Argument>vstrBookLockPassword: Input</Argument>
'''' <Return>Boolean</Return>
'Public Function IsAdoConnectionContinuedAfterAccessOleDbToExcelSheetConnectionParametersByForm(ByVal vstrSettingKeyName As String, _
'        Optional ByVal vstrBookPath As String = "", _
'        Optional ByVal venmIMEXMode As IMEXMode = IMEXMode.NoUseIMEXMode, _
'        Optional ByVal vblnHeaderFieldTitleExists As Boolean = True, _
'        Optional ByVal vblnBookReadOnly As Boolean = False, _
'        Optional ByVal venmAccessOLEDBProviderType As AccessOLEDBProviderType = AccessOLEDBProviderType.AceOLEDB_12P0, _
'        Optional ByVal vstrBookLockPassword As String = "") As Boolean
'
'    Dim blnIsRequestedToCancelAdoConnection As Boolean
'
'    SetOleDbConnectionParametersByAccessOleDbToExcelSheetForm blnIsRequestedToCancelAdoConnection, Me, vstrSettingKeyName, vstrBookPath, venmIMEXMode, vblnHeaderFieldTitleExists, vblnBookReadOnly, venmAccessOLEDBProviderType, vstrBookLockPassword, True
'
'    IsAdoConnectionContinuedAfterAccessOleDbToExcelSheetConnectionParametersByForm = Not blnIsRequestedToCancelAdoConnection
'End Function
'
'''--VBA_Code_File--<XlAdoConnector.cls>--
'VERSION 1.0 CLASS
'BEGIN
'  MultiUse = -1  'True
'END
'Attribute VB_Name = "XlAdoConnector"
'Attribute VB_GlobalNameSpace = False
'Attribute VB_Creatable = False
'Attribute VB_PredeclaredId = False
'Attribute VB_Exposed = False
''
''   connect to Excel.Workbook by ADO-DB interface
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on ADO
''       Dependent on Microsoft Office Access Data Base Engine OLE DB Provider,
''       which should have been the installed when the Microsoft Office Excel software has been installed.
''       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access.
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Thu,  9/Jun/2022    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Directives
''///////////////////////////////////////////////
'#Const HAS_EXCEL_REF = False
'
'
''///////////////////////////////////////////////
''/// Interface implementations
''///////////////////////////////////////////////
'Implements IADOConnector
'
''///////////////////////////////////////////////
''/// Declarations
''///////////////////////////////////////////////
'Private mobjConnector As ADOConnector: Private mitfConnector As IADOConnector
'
'Private mobjConnectStrGenerator As ADOConStrOfAceOleDbXl
'
'
''///////////////////////////////////////////////
''/// Event Handlers
''///////////////////////////////////////////////
'Private Sub Class_Initialize()
'
'    Set mobjConnector = New ADOConnector: Set mitfConnector = mobjConnector
'
'    Set mobjConnectStrGenerator = New ADOConStrOfAceOleDbXl
'End Sub
'
'Private Sub Class_Terminate()
'
'    Me.CloseConnection
'
'    Set mitfConnector = Nothing
'
'    Set mobjConnector = Nothing
'
'    Set mobjConnectStrGenerator = Nothing
'End Sub
'
''///////////////////////////////////////////////
''/// Properties
''///////////////////////////////////////////////
''''
''''
''''
'Public Property Get ADOConnectorInterface() As IADOConnector
'
'    Set ADOConnectorInterface = mitfConnector
'End Property
'
''///////////////////////////////////////////////
''/// Properties - connoted ADO connector
''///////////////////////////////////////////////
'Public Property Let AllowToRecordSQLLog(ByVal vblnAllowToRecord As Boolean)
'
'    mobjConnector.AllowToRecordSQLLog = vblnAllowToRecord
'End Property
'Public Property Get AllowToRecordSQLLog() As Boolean
'
'    AllowToRecordSQLLog = mobjConnector.AllowToRecordSQLLog
'End Property
'
'
'Public Property Get SQLExecutionResult() As SQLResult
'
'    Set SQLExecutionResult = IADOConnector_SQLExecutionResult
'End Property
'
'Public Property Get IsSqlExecutionFailed() As Boolean
'
'    IsSqlExecutionFailed = mobjConnector.IsSqlExecutionFailed
'End Property
'
''///////////////////////////////////////////////
''/// Implemented properties
''///////////////////////////////////////////////
'Private Property Let IADOConnector_IsConnected(ByVal vblnIsConnected As Boolean)
'
'    If vblnIsConnected Then
'
'        mitfConnector.IsConnected = mfblnConnect()
'    Else
'        mobjConnector.CloseConnection
'
'        mitfConnector.IsConnected = mobjConnector.IsConnected
'    End If
'End Property
'Private Property Get IADOConnector_IsConnected() As Boolean
'
'    IADOConnector_IsConnected = mfblnConnect()
'End Property
'
'Private Property Set IADOConnector_ADOConnection(ByVal vobjADOConnection As ADODB.Connection)
'
'    Set mitfConnector.ADOConnection = vobjADOConnection
'End Property
'Private Property Get IADOConnector_ADOConnection() As ADODB.Connection
'
'    Set IADOConnector_ADOConnection = mitfConnector.ADOConnection
'End Property
'
'Private Property Set IADOConnector_SQLExecutionResult(ByVal vobjSQLResult As SQLResult)
'
'    Set mitfConnector.SQLExecutionResult = vobjSQLResult
'End Property
'Private Property Get IADOConnector_SQLExecutionResult() As SQLResult
'
'    Set IADOConnector_SQLExecutionResult = mitfConnector.SQLExecutionResult
'End Property
'
'
'Private Property Get IADOConnector_CommandTimeout() As Long
'
'    IADOConnector_CommandTimeout = mobjConnector.CommandTimeout
'End Property
'Private Property Let IADOConnector_CommandTimeout(ByVal vintTimeout As Long)
'
'    mobjConnector.CommandTimeout = vintTimeout
'End Property
'
'Private Property Get IADOConnector_SuppressToShowUpSqlExecutionError() As Boolean
'
'    IADOConnector_SuppressToShowUpSqlExecutionError = mitfConnector.SuppressToShowUpSqlExecutionError
'End Property
'Private Property Let IADOConnector_SuppressToShowUpSqlExecutionError(ByVal vblnSuppressToShowUpSqlExecutionError As Boolean)
'
'    mitfConnector.SuppressToShowUpSqlExecutionError = vblnSuppressToShowUpSqlExecutionError
'End Property
'
'Private Property Get IADOConnector_RdbConnectionInformation() As RdbConnectionInformationFlag
'
'    IADOConnector_RdbConnectionInformation = AdoOleDbToMsExcelBookAsRdbFlag
'End Property
'Private Property Let IADOConnector_RdbConnectionInformation(ByVal venmRdbConnectionInformationFlag As RdbConnectionInformationFlag)
'
'    ' Nothing to do
'End Property
'
''///////////////////////////////////////////////
''/// Properties - connoted ADOConnectionSetting
''///////////////////////////////////////////////
'Public Property Get ConnectSetting() As ADOConnectionSetting
'
'    Set ConnectSetting = mobjConnector.ConnectSetting
'End Property
'
''''
'''' unit is second. [s]
''''
'Public Property Let CommandTimeout(ByVal vintTimeout As Long)
'
'    IADOConnector_CommandTimeout = vintTimeout
'End Property
'Public Property Get CommandTimeout() As Long
'
'    CommandTimeout = IADOConnector_CommandTimeout
'End Property
'
'
'Public Property Get SuppressToShowUpSqlExecutionError() As Boolean
'
'    SuppressToShowUpSqlExecutionError = IADOConnector_SuppressToShowUpSqlExecutionError
'End Property
'Public Property Let SuppressToShowUpSqlExecutionError(ByVal vblnSuppressToShowUpSqlExecutionError As Boolean)
'
'    IADOConnector_SuppressToShowUpSqlExecutionError = vblnSuppressToShowUpSqlExecutionError
'End Property
'
''///////////////////////////////////////////////
''/// Properties - Excel-book connection by ACCESS OLE-DB provider
''///////////////////////////////////////////////
'Public Property Get ConnectingFilePath() As String
'
'    ConnectingFilePath = mobjConnectStrGenerator.ConnectingFilePath
'End Property
'
''''
'''' when the load Excel sheet doesn't have the field header title text, this is False
''''
'Public Property Get HeaderFieldTitleExists() As Boolean
'
'    HeaderFieldTitleExists = mobjConnectStrGenerator.HeaderFieldTitleExists
'End Property
'
'Public Property Get BookReadOnly() As Boolean
'
'    BookReadOnly = mobjConnectStrGenerator.BookReadOnly
'End Property
'
'Public Property Get IMEX() As IMEXMode
'
'    IMEX = mobjConnectStrGenerator.IMEX
'End Property
'
'
''///////////////////////////////////////////////
''/// Implemented operations
''///////////////////////////////////////////////
''''
'''' overrides
''''
'Private Function IADOConnector_GetRecordset(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag) As ADODB.Recordset
'
'    Set IADOConnector_GetRecordset = Me.GetRecordset(vstrSQL, venmShowUpAdoErrorOptionFlag)
'End Function
'
''''
'''' In the case of Excel connector, this is not almost used; overrides
''''
'Private Function IADOConnector_ExecuteSQL(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag) As ADODB.Recordset
'
'    Set IADOConnector_ExecuteSQL = ExecuteSQL(vstrSQL, venmShowUpAdoErrorOptionFlag)
'End Function
'
''''
'''' If the ExecuteSQL method had been used at least one time, this execute ADODB.Connect.CommitTrans()
''''
'Private Sub IADOConnector_CommitTransaction()
'
'    mitfConnector.CommitTransaction
'End Sub
'
''''
'''' If the ExecuteSQL method had been used at least one time, this execute CommitTransaction before execute ADODB.Connect.Close()
''''
'Private Sub IADOConnector_CloseConnection()
'
'    Me.CloseConnection
'End Sub
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''**---------------------------------------------
''** set ADO connection string all parameters directly
''**---------------------------------------------
''''
'''' set input Excel book path
''''
'Public Sub SetInputExcelBookPath(ByVal vstrBookPath As String, _
'        Optional ByVal venmIMEXMode As IMEXMode = IMEXMode.NoUseIMEXMode, _
'        Optional ByVal vblnHeaderFieldTitleExists As Boolean = True, _
'        Optional ByVal vblnBookReadOnly As Boolean = False, _
'        Optional ByVal vstrLockBookPassword As String = "", _
'        Optional ByVal vblnIsConnectedToOffice95 As Boolean = False)
'
'#If HAS_EXCEL_REF Then
'
'    Dim objBook As Excel.Workbook
'#Else
'    Dim objBook As Object
'#End If
'
'    With mobjConnectStrGenerator
'
'        If .ConnectingFilePath <> "" And .ConnectingFilePath <> vstrBookPath Then
'
'            Me.CloseConnection
'        End If
'
'        .SetExcelBookConnection vstrBookPath, venmIMEXMode, vblnHeaderFieldTitleExists, vblnBookReadOnly, vblnIsConnectedToOffice95
'    End With
'
'    If vstrLockBookPassword <> "" Then
'
'        ' If to connect Excel book is locked by the password, then the ADO connecting needs the opened state in a Excel application
'
'        Set objBook = OpenPasswordLockedWorkbookByInterface(vstrBookPath, vstrLockBookPassword)
'    End If
'End Sub
'
''**---------------------------------------------
''** set ADO connection string parameters by a User-form interface
''**---------------------------------------------
''''
''''
''''
'''' <Argument>vstrSettingKeyName: Input</Argument>
'''' <Argument>vstrBookPath: Input</Argument>
'''' <Argument>venmIMEXMode: Input</Argument>
'''' <Argument>vblnHeaderFieldTitleExists: Input</Argument>
'''' <Argument>vblnBookReadOnly: Input</Argument>
'''' <Argument>venmAccessOLEDBProviderType: Input</Argument>
'''' <Argument>vstrBookLockPassword: Input</Argument>
'''' <Return>Boolean</Return>
'Public Function IsAdoConnectionContinuedAfterAccessOleDbConnectionParametersByForm(ByVal vstrSettingKeyName As String, _
'        Optional ByVal vstrBookPath As String = "", _
'        Optional ByVal venmIMEXMode As IMEXMode = IMEXMode.NoUseIMEXMode, _
'        Optional ByVal vblnHeaderFieldTitleExists As Boolean = True, _
'        Optional ByVal vblnBookReadOnly As Boolean = False, _
'        Optional ByVal venmAccessOLEDBProviderType As AccessOLEDBProviderType = AccessOLEDBProviderType.AceOLEDB_12P0, _
'        Optional ByVal vstrBookLockPassword As String = "")
'
'
'    IsAdoConnectionContinuedAfterAccessOleDbConnectionParametersByForm = mobjConnectStrGenerator.IsAdoConnectionContinuedAfterAccessOleDbToExcelSheetConnectionParametersByForm(vstrSettingKeyName, vstrBookPath, venmIMEXMode, vblnHeaderFieldTitleExists, vblnBookReadOnly, venmAccessOLEDBProviderType, vstrBookLockPassword)
'End Function
'
'
''''
'''' Close ADO connection
''''
'Public Sub CloseConnection()
'
'    mobjConnector.CloseConnection
'End Sub
'
'
''**---------------------------------------------
''** Use SQL after the ADO connected
''**---------------------------------------------
''''
'''' get recordset object by the Select SQL
''''
'Public Function GetRecordset(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag) As ADODB.Recordset
'
'    If mfblnConnect() Then
'
'        Set GetRecordset = mobjConnector.GetRecordset(vstrSQL, venmShowUpAdoErrorOptionFlag)
'    End If
'End Function
'
''''
'''' In the case of Excel connector, this is not almost used. - input SQL, such as INSERT, UPDATE, DELETE
''''
'Public Function ExecuteSQL(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag) As ADODB.Recordset
'
'    Dim objRSet As ADODB.Recordset
'
'    Set objRSet = Nothing
'
'    If mfblnConnect() Then
'
'        Set objRSet = mobjConnector.ExecuteSQL(vstrSQL, venmShowUpAdoErrorOptionFlag)
'    End If
'
'    Set ExecuteSQL = objRSet
'End Function
'
''''
''''
''''
'Public Function IsConnected() As Boolean
'
'    IsConnected = mfblnConnect()
'End Function
'
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
''''
'''' connect Excel sheet by ADO
''''
'Private Function mfblnConnect() As Boolean
'
'    Dim blnIsConnected As Boolean
'
'    TryToConnectRDBProviderByAdoConnectionString blnIsConnected, mobjConnector, mobjConnectStrGenerator
'
'    mfblnConnect = blnIsConnected
'End Function
'
'
'
'''--VBA_Code_File--<SqlAdoConnectingUTfXl.bas>--
'Attribute VB_Name = "SqlAdoConnectingUTfXl"
''
''   Sanity tests to connecting a Excel sheets in a directory using ADO OLE DB
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on ADO
''       Dependent on Microsoft Office Access Data Base Engine OLE DB Provider,
''       which should have been the installed when the Microsoft Office Excel software has been installed.
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Fri,  4/Aug/2023    Kalmclaeyd Tarclanus    Initial creation
''
'
'Option Explicit
'
'
''///////////////////////////////////////////////
''/// Sanity tests
''///////////////////////////////////////////////
''**---------------------------------------------
''** XlAdoConnector
''**---------------------------------------------
''''
'''' Create an in-line table from the only SELECT sql inside
''''
'Private Sub msubSanityTestToConnectExcelSheetAndOutputRecordsetToImmediateWiondowByVirtualTableXlConnector()
'
'    Dim strSQL As String
'
'    With New XlAdoConnector
'
'        .SetInputExcelBookPath GetXlConnectingPrearrangedLogBookPath()
'
'        .AllowToRecordSQLLog = True
'
'        strSQL = "SELECT 'ABC' AS A_Column, 'DEF' AS B_Column"
'
'        DebugCol GetTableCollectionFromRSet(.GetRecordset(strSQL))
'    End With
'End Sub
'
'
''''
'''' show UErrorADOSQLForm tests
''''
'Private Sub msubSanityTestToShowErrorTrapFormUsingXlAdoConnectorByIllegalSQL()
'
'    Dim strSQL As String, objRSet As ADODB.Recordset
'
'    With New XlAdoConnector
'
'        .SetInputExcelBookPath GetXlConnectingPrearrangedLogBookPath()
'
'        strSQL = "(SELECT 'ABC', 'DEF') UNION ALL (SELECT 'GHI', 'JKL')" ' This causes a error. It seems that UNION ALL is not applicable....
'
'        'strSQL = "(SELECT 'ABC' AS A_Column, 'DEF' AS B_Column) UNION ALL (SELECT 'GHI', 'JKL')" ' Error occurs
'
'        DebugCol GetTableCollectionFromRSet(.GetRecordset(strSQL))
'    End With
'End Sub
'
'
'
'
'''--VBA_Code_File--<CompareOnlySheetsByAdo.bas>--
'Attribute VB_Name = "CompareOnlySheetsByAdo"
''
''   compare two Excel book about only Worksheet table data, which are not locked by password, using ADODB.connection
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on ADO
''       Dependent on XlAdoConnector.bas, VariantTypeConversion.bas
''       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Tue,  4/Jun/2023    Kalmclaeyd Tarclanus    Separated from CodeAnalysisVBA.bas
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''**---------------------------------------------
''** Using ADO, compare Excel worksheets between two files without opening
''**---------------------------------------------
''''
'''' compare two books only sheets, which book is not locked by password
''''
'''' This doesn't check auto-shapes on each Worksheet.
''''
'Public Function IsDifferentPartBetweenTwoBookSheetsUsingAdo(ByRef rstrBook01Path As String, _
'        ByRef rstrBook02Path As String, _
'        ByVal vstrSheetNamesDelimitedByComma As String) As Boolean
'
'    Dim strSQL As String, varSheetName As Variant, strSheetName As String, objRSet As ADODB.Recordset
'    Dim objDT01Col As Collection, objDT02Col As Collection, blnIsDifferent As Boolean
'
'
'    blnIsDifferent = False
'
'    If vstrSheetNamesDelimitedByComma = "" Then
'
'        Debug.Print "!!! Attention: The comparison cannot be able because there is no sheet name information."
'    End If
'
'    For Each varSheetName In Split(vstrSheetNamesDelimitedByComma, ",")
'
'        strSheetName = varSheetName
'
'        strSQL = "SELECT * FROM [" & strSheetName & "$]"
'
'        Set objDT01Col = Nothing: Set objDT02Col = Nothing
'
'        With New XlAdoConnector
'
'            .SuppressToShowUpSqlExecutionError = True
'
'            .SetInputExcelBookPath rstrBook01Path, vblnBookReadOnly:=True
'
'            Set objRSet = .GetRecordset(strSQL)
'
'            Set objDT01Col = GetTableCollectionFromRSet(objRSet)
'
'
'            .CloseConnection
'
'            .SetInputExcelBookPath rstrBook02Path, vblnBookReadOnly:=True
'
'            Set objRSet = .GetRecordset(strSQL)
'
'            Set objDT02Col = GetTableCollectionFromRSet(objRSet)
'        End With
'
'        If Not objDT01Col Is Nothing And Not objDT02Col Is Nothing Then
'
'            If AreTwoCountOfColumnsOfDTColsExactlyMatched(objDT01Col, objDT02Col) Then
'
'                If AreTwoDTColsExactlyMatched(objDT01Col, objDT02Col) Then
'
'                    ' matched
'                Else
'                    blnIsDifferent = True
'                End If
'            Else
'                blnIsDifferent = True
'            End If
'
'        ElseIf objDT01Col Is Nothing And objDT02Col Is Nothing Then
'
'            ' Unknown
'        Else
'            blnIsDifferent = True
'        End If
'
'        If blnIsDifferent Then
'
'            Exit For
'        End If
'    Next
'
'    IsDifferentPartBetweenTwoBookSheetsUsingAdo = blnIsDifferent
'End Function
'
'
''**---------------------------------------------
''** Update Excel Book files based on any Worksheet table differences
''**---------------------------------------------
''''
'''' Ref. UpdateCopyFilesWithoutDuplicated in FileSysSyncCopy.bas
''''
'Public Function UpdateCopyExcelBookFilesOnlyWhenThereAreWorksheetsDifferences(ByVal vstrParentDirectoryPath As String, _
'        ByVal vstrSourcePathPart As String, _
'        ByVal vstrDestinationPathPart As String, _
'        ByVal vstrSheetNamesDelimitedByComma As String, _
'        Optional ByVal vstrRawWildCardsByDelimitedChar As String = "*.xlsx,*xls", _
'        Optional ByVal vblnShowWarningMessageBoxWhenFileSystemDriveTroubles As Boolean = True) As Collection
'
'
'    Dim objBookPathes As Collection, objCopiedFilePaths As Collection
'
'
'    Set objCopiedFilePaths = New Collection
'
'    If AreBothDrivesReady(vstrSourcePathPart, vstrDestinationPathPart, vblnShowWarningMessageBoxWhenFileSystemDriveTroubles) Then
'
'        ' use VBA.Dir
'        Set objBookPathes = GetFilePathsUsingVbaDir(vstrParentDirectoryPath, vstrRawWildCardsByDelimitedChar)
'
'        Set objCopiedFilePaths = CopyFilesWithKeepingDirectoryStructureOnlyWhenThereAreWorksheetsDifferences(objLogDTCol, objBookPathes, vstrSourcePathPart, vstrDestinationPathPart, vstrSheetNamesDelimitedByComma)
'    End If
'
'    Set UpdateCopyExcelBookFilesOnlyWhenThereAreWorksheetsDifferences = objCopiedFilePaths
'End Function
'
'
''''
'''' ref. CopyFilesWithKeepingDirectoryStructure in FileSysCopyProgressBar.bas
''''
'''' <Argument>robjLogDTCol: Output - loggings when checking diff</Argument>
'''' <Argument>vobjBookPaths: Input - Excel book full paths</Argument>
'''' <Argument>vstrSourceParentDirectoryPathPart: Input</Argument>
'''' <Argument>vstrDestinationParentDirectoryPathPart: Input</Argument>
'''' <Argument>vstrSheetNamesDelimitedByComma: Input - checking sheet names delimited by comma character</Argument>
'''' <Return>Collection: copied file paths</Return>
'Public Function CopyFilesWithKeepingDirectoryStructureOnlyWhenThereAreWorksheetsDifferences(ByRef robjLogDTCol As Collection, _
'        ByVal vobjBookPaths As Collection, _
'        ByVal vstrSourceParentDirectoryPathPart As String, _
'        ByVal vstrDestinationParentDirectoryPathPart As String, _
'        ByVal vstrSheetNamesDelimitedByComma As String) As Collection
'
'
'    Dim objCopiedFilePaths As Collection, objLogDTCol As Collection, objDoCopySrcToDstDic As Scripting.Dictionary, objDoCreateShortcutDstToTargetPathDic As Scripting.Dictionary
'
'    Set objLogDTCol = GetListsOfCopyFilesWithKeepingDirectoryStructureOnlyWhenThereAreWorksheetsDifferences(objDoCopySrcToDstDic, objDoCreateShortcutDstToTargetPathDic, vobjBookPaths, vstrSourceParentDirectoryPathPart, vstrDestinationParentDirectoryPathPart)
'
'    Set objCopiedFilePaths = CopyFilesWithSimpleProgressBar(robjDoCopySrcToDstDic, New Scripting.FileSystemObject)
'
'    Set robjLogDTCol = objLogDTCol
'
'    Set CopyFilesWithKeepingDirectoryStructureOnlyWhenThereAreWorksheetsDifferences = objCopiedFilePaths
'End Function
'
'
''''
'''' ref. GetListsOfCopyFilesWithKeepingDirectoryStructure in FileSysSyncCopy.bas
''''
'''' <Argument>robjDoCopySrcToDstDic: Output</Argument>
'''' <Argument>vobjBookPaths: Input</Argument>
'''' <Argument>vstrSourceParentDirectoryPathPart: Input</Argument>
'''' <Argument>vstrDestinationParentDirectoryPathPart: Input</Argument>
'''' <Argument>vstrSheetNamesDelimitedByComma: Input - checking sheet names delimited by comma character</Argument>
'''' <Return>Collection</Return>
'Public Function GetListsOfCopyFilesWithKeepingDirectoryStructureOnlyWhenThereAreWorksheetsDifferences(ByRef robjDoCopySrcToDstDic As Scripting.Dictionary, _
'        ByVal vobjBookPaths As Collection, _
'        ByVal vstrSourceParentDirectoryPathPart As String, _
'        ByVal vstrDestinationParentDirectoryPathPart As String, _
'        ByVal vstrSheetNamesDelimitedByComma As String) As Collection
'
'
'    Dim objFS As Scripting.FileSystemObject
'    Dim varPath As Variant, strPath As String, strDestinationDir As String, strDestinationPath As String
'
'    Dim blnDoCopy As Boolean, objLogRowCol As Collection, objLogDTCol As Collection
'
'    Dim enmFileCopySituation As FileSyncCopySituation, varCompoundKey As Variant, objDoCopySrcToDstDic As Scripting.Dictionary
'
'
'    Set objFS = New Scripting.FileSystemObject
'
'    Set objLogDTCol = New Collection
'
'    For Each varPath In vobjBookPaths
'
'        strPath = varPath
'
'        If InStr(1, strPath, vstrSourceParentDirectoryPathPart) > 0 Then
'
'            With objFS
'                strDestinationDir = .GetParentFolderName(Replace(strPath, vstrSourceParentDirectoryPathPart, vstrDestinationParentDirectoryPathPart))
'
'                strDestinationPath = strDestinationDir & "\" & .GetFileName(strSourcePath)
'
'                blnDoCopy = mfblnIsNeedToCopyOnlyWhenThereAreWorksheetsDifferences(enmFileCopySituation, objLogRowCol, strSourcePath, strDestinationPath, vstrSheetNamesDelimitedByComma, objFS)
'
'                If Not objLogRowCol Is Nothing Then
'
'                    objLogDTCol.Add objLogRowCol
'                End If
'            End With
'
'            If blnDoCopy Then
'
'                ForceToCreateDirectory strDestinationDir, objFS
'
'                varCompoundKey = strSourcePath & ";" & CStr(enmFileCopySituation)
'
'                objDoCopySrcToDstDic.Add varCompoundKey, strDestinationDir & "\"
'            End If
'        End If
'    Next
'
'    Set robjDoCopySrcToDstDic = objDoCopySrcToDstDic
'
'    Set GetListsOfCopyFilesWithKeepingDirectoryStructureOnlyWhenThereAreWorksheetsDifferences = objLogDTCol
'End Function
'
'
''''
'''' ref. mfblnIsNeedToCopy in FileSysSyncCopy.bas
''''
'Private Function mfblnIsNeedToCopyOnlyWhenThereAreWorksheetsDifferences(ByRef renmFileCopySituation As FileSyncCopySituation, _
'        ByRef robjLogRowCol As Collection, _
'        ByVal vstrSourcePath As String, _
'        ByVal vstrDestinationPath As String, _
'        ByVal vstrSheetNamesDelimitedByComma As String, _
'        ByVal vobjFS As Scripting.FileSystemObject) As Boolean
'
'
'    Dim blnDoCopy As Boolean, intSecondDiff As Long, intDayDiff As Long
'
'
'    Const intEpsilonOfSecond As Long = 5    ' When the Window copy any file in special combination of an internal HDD and an external HDD, each file timestamp are always shifted between one and two about each file timestamp.
'
'    blnDoCopy = False
'
'    With vobjFS
'
'        If .FileExists(vstrDestinationPath) Then
'
'            Set robjLogRowCol = Nothing
'
'            ' compare only data-tables on sheets. the auto-shapes on each sheet and modified-last-date and office-file-built-in properties are ignored
'
'            If Not IsDifferentPartBetweenTwoBookSheetsUsingAdo(vstrSourcePath, vstrDestinationPath, vstrSheetNamesDelimitedByComma) Then
'
'                blnDoCopy = False
'            Else
'                blnDoCopy = True
'
'                renmFileCopySituation = UpdateFileToFileSystemSync
'            End If
'
'            'msubLogOfComparingDateLastModifiedBetweenSourceAndDestination robjLogRowCol, objSourceFile, objDestinationFile, blnDoCopy, intSecondDiff, intDayDiff
'        Else
'            blnDoCopy = True
'
'            renmFileCopySituation = CopyFileNewToFileSystemSync
'        End If
'    End With
'
'    mfblnIsNeedToCopyOnlyWhenThereAreWorksheetsDifferences = blnDoCopy
'End Function
'
'
'''--VBA_Code_File--<UTfCompareOnlySheetsByAdo.bas>--
'Attribute VB_Name = "UTfCompareOnlySheetsByAdo"
''
''   Sanity tests to compare two Excel book about only Worksheet table data
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on both ADO and Excel
''       Dependent on XlAdoConnector.bas, VariantTypeConversion.bas
''       Dependent on UTfDecorationSetterToXlSheet.bas
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Sat, 29/Jul/2023    Kalmclaeyd Tarclanus    Initial creation
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Sanity tests
''///////////////////////////////////////////////
''**---------------------------------------------
''** About double sheets
''**---------------------------------------------
'Private Sub msubSanityTestToIsDifferentPartBetweenTwoBookSheetsWithTwoSheetsUsingAdo01()
'
'    Dim strBookPath01 As String, strBookPath02 As String, str1stSheetName As String, str2ndSheetName As String
'    Dim strSheetNamesDelimitedByComma As String
'
'    'msubGetTestingTwoExcelBooksWhichHasTwoSheet strBookPath01, strBookPath02, str1stSheetName, str2ndSheetName, "3, 4, 1, 1", "8, 12, 1, 1", "3, 4, 1, 1", "8, 12, 1, 1", "TwoSheetsTestBook01.xlsx"
'
'    msubGetTestingTwoExcelBooksWhichHasTwoSheet strBookPath01, strBookPath02, str1stSheetName, str2ndSheetName, "3, 4, 1, 1", "8, 12, 5, 7", "3, 4, 1, 1", "8, 12, 1, 1", "TwoSheetsTestBook01.xlsx"
'
'
'    strSheetNamesDelimitedByComma = str1stSheetName & "," & str2ndSheetName
'
'    If IsDifferentPartBetweenTwoBookSheetsUsingAdo(strBookPath01, strBookPath02, strSheetNamesDelimitedByComma) Then
'
'        Debug.Assert False
'    Else
'        Debug.Print "The two books are same for each specified sheets" & " - The sanity test is passed"
'    End If
'End Sub
'
'Private Sub msubSanityTestToIsDifferentPartBetweenTwoBookSheetsWithTwoSheetsUsingAdo02()
'
'    Dim strBookPath01 As String, strBookPath02 As String, str1stSheetName As String, str2ndSheetName As String
'    Dim strSheetNamesDelimitedByComma As String
'
'    msubGetTestingTwoExcelBooksWhichHasTwoSheet strBookPath01, strBookPath02, str1stSheetName, str2ndSheetName, "3, 4, 1, 1", "8, 11, 1, 1", "3, 4, 1, 1", "8, 12, 1, 1", "TwoSheetsTestDifferentBook02.xlsx"
'
'    strSheetNamesDelimitedByComma = str1stSheetName & "," & str2ndSheetName
'
'    If IsDifferentPartBetweenTwoBookSheetsUsingAdo(strBookPath01, strBookPath02, strSheetNamesDelimitedByComma) Then
'
'        Debug.Print "The two books are different" & " - The sanity test is passed"
'    Else
'        Debug.Assert False
'    End If
'End Sub
'
''**---------------------------------------------
''** About single sheet
''**---------------------------------------------
''''
''''
''''
'Private Sub msubSanityTestToIsDifferentPartBetweenTwoBookSheetsUsingAdo01()
'
'    Dim strBookPath01 As String, strBookPath02 As String, strSheetName As String
'
'
'    'msubGetTestingTwoExcelBooks strBookPath01, strBookPath02, strSheetName, "3, 4, 1, 1", "3, 4, 1, 1", "TestDataBook01"
'
'    msubGetTestingTwoExcelBooks strBookPath01, strBookPath02, strSheetName, "8, 9, 1, 1", "8, 9, 1, 1", "TestDataBook01.xlsx"
'
'    If IsDifferentPartBetweenTwoBookSheetsUsingAdo(strBookPath01, strBookPath02, strSheetName) Then
'
'        Debug.Assert False
'    Else
'        Debug.Print "The two books are same" & " - The sanity test is passed"
'    End If
'End Sub
'
'Private Sub msubSanityTestToIsDifferentPartBetweenTwoBookSheetsUsingAdo02()
'
'    Dim strBookPath01 As String, strBookPath02 As String, strSheetName As String
'
'
'    msubGetTestingTwoExcelBooks strBookPath01, strBookPath02, strSheetName, "3, 4, 1, 1", "3, 5, 1, 1", "TestDifferentDataBook02.xlsx"
'
'    If IsDifferentPartBetweenTwoBookSheetsUsingAdo(strBookPath01, strBookPath02, strSheetName) Then
'
'        Debug.Print "The two books are different" & " - The sanity test is passed"
'    Else
'        Debug.Assert False
'    End If
'End Sub
'
'
''''
''''
''''
'Private Sub msubSanityTestToIsDifferentPartBetweenTwoBookSheetsUsingAdo03()
'
'    Dim strBookPath01 As String, strBookPath02 As String, strSheetName As String
'
'
'    msubGetTestingTwoExcelBooks strBookPath01, strBookPath02, strSheetName, "3, 4, 1, 1", "3, 4, 3, 3", "TestDifferentDataBook03.xlsx"
'
'    If IsDifferentPartBetweenTwoBookSheetsUsingAdo(strBookPath01, strBookPath02, strSheetName) Then
'
'        Debug.Assert False
'    Else
'        Debug.Print "The two books are same, but the top-left of table is different." & " - The sanity test is passed"
'    End If
'End Sub
'
''''
''''
''''
'Private Sub msubSanityTestToIsDifferentPartBetweenTwoBookSheetsUsingAdo04()
'
'    Dim strBookPath01 As String, strBookPath02 As String, strSheetName As String
'
'    msubGetTestingTwoExcelBooks strBookPath01, strBookPath02, strSheetName, "3, 4, 1, 1", "3, 4, 1, 1", "TestDifferentDataBook04", "", "2;2;105"
'
'    If IsDifferentPartBetweenTwoBookSheetsUsingAdo(strBookPath01, strBookPath02, strSheetName) Then
'
'        Debug.Print "The two books are different" & " - The sanity test has been passed"
'    Else
'        Debug.Assert False
'    End If
'End Sub
'
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
''''
''''
''''
'Private Sub msubExploreTestDir()
'
'    ExploreParentFolderWithSelectingWhenOpenedThatIsNothing mfstrGetTemporaryBookDir("SudDir01")
'End Sub
'
'
''''
''''
''''
'Private Sub msubGetTestingTwoExcelBooks(ByRef rstrBookPath01 As String, _
'        ByRef rstrBookPath02 As String, _
'        ByRef rstrSheetName As String, _
'        ByVal vstrDataTableParamsDelimitedByComma01 As String, _
'        ByVal vstrDataTableParamsDelimitedByComma02 As String, _
'        ByVal vstrBookFileName As String, _
'        Optional ByVal vstrAfterChangeSheetValueInfo1stRowIndex2ndColumnIndex3rdValueByDelimitedSemicolon01 As String = "", _
'        Optional ByVal vstrAfterChangeSheetValueInfo1stRowIndex2ndColumnIndex3rdValueByDelimitedSemicolon02 As String = "")
'
'
'    msubGetTestingExcelBookOfOneSheet rstrBookPath01, rstrSheetName, "SudDir01", vstrDataTableParamsDelimitedByComma01, vstrBookFileName, vstrAfterChangeSheetValueInfo1stRowIndex2ndColumnIndex3rdValueByDelimitedSemicolon01
'
'    msubGetTestingExcelBookOfOneSheet rstrBookPath02, rstrSheetName, "SudDir02", vstrDataTableParamsDelimitedByComma02, vstrBookFileName, vstrAfterChangeSheetValueInfo1stRowIndex2ndColumnIndex3rdValueByDelimitedSemicolon02
'End Sub
'
''''
''''
''''
'Private Sub msubGetTestingTwoExcelBooksWhichHasTwoSheet(ByRef rstrBookPath01 As String, _
'        ByRef rstrBookPath02 As String, _
'        ByRef rstr1stSheetName As String, _
'        ByRef rstr2ndSheetName As String, _
'        ByVal vstr1stDataTableParamsDelimitedByComma01 As String, _
'        ByVal vstr2ndDataTableParamsDelimitedByComma01 As String, _
'        ByVal vstr1stDataTableParamsDelimitedByComma02 As String, _
'        ByVal vstr2ndDataTableParamsDelimitedByComma02 As String, _
'        ByVal vstrBookFileName As String)
'
'
'    msubGetTestingExcelBookOfTwoSheets rstrBookPath01, rstr1stSheetName, rstr2ndSheetName, "SudDir01", vstr1stDataTableParamsDelimitedByComma01, vstr2ndDataTableParamsDelimitedByComma01, vstrBookFileName
'
'    msubGetTestingExcelBookOfTwoSheets rstrBookPath02, rstr1stSheetName, rstr2ndSheetName, "SudDir02", vstr1stDataTableParamsDelimitedByComma02, vstr2ndDataTableParamsDelimitedByComma02, vstrBookFileName
'End Sub
'
''''
''''
''''
'Private Sub msubGetTestingExcelBookOfOneSheet(ByRef rstrBookPath As String, _
'        ByRef rstrSheetName As String, _
'        ByVal vstrSubDirectoryName As String, _
'        ByVal vstrDataTableParamsDelimitedByComma As String, _
'        ByVal vstrBookFileName As String, _
'        Optional ByVal vstrAfterChangeSheetValueInfo1stRowIndex2ndColumnIndex3rdValueByDelimitedSemicolon As String = "")
'
'    Dim objBook As Excel.Workbook, strSheetName As String
'
'    strSheetName = "TestName"
'
'
'    rstrBookPath = mfstrGetTestBookPath(vstrSubDirectoryName, vstrBookFileName)
'
'    Set objBook = GetWorkbookAndPrepareForUnitTest(rstrBookPath)
'
'    GenerateSimpleTableSheetForSpecifiedBookWithStringParams objBook, strSheetName, vstrDataTableParamsDelimitedByComma, vstrAfterChangeSheetValueInfo1stRowIndex2ndColumnIndex3rdValueByDelimitedSemicolon
'
'    DeleteDummySheetWhenItExists objBook
'
'    ForceToSaveAsBookWithoutDisplayAlert objBook, rstrBookPath
'
'    objBook.Close
'
'    rstrSheetName = strSheetName
'End Sub
'
''''
''''
''''
'Private Sub msubGetTestingExcelBookOfTwoSheets(ByRef rstrBookPath As String, _
'        ByRef rstr1stSheetName As String, _
'        ByRef rstr2ndSheetName As String, _
'        ByVal vstrSubDirectoryName As String, _
'        ByVal vstr1stDataTableParamsDelimitedByComma As String, _
'        ByVal vstr2ndDataTableParamsDelimitedByComma As String, _
'        ByVal vstrBookFileName As String)
'
'    Dim objBook As Excel.Workbook
'
'    rstr1stSheetName = "TestName01"
'
'    rstr2ndSheetName = "TestName02"
'
'
'    rstrBookPath = mfstrGetTestBookPath(vstrSubDirectoryName, vstrBookFileName)
'
'    Set objBook = GetWorkbookAndPrepareForUnitTest(rstrBookPath)
'
'    GenerateSimpleTableSheetForSpecifiedBookWithStringParams objBook, rstr1stSheetName, vstr1stDataTableParamsDelimitedByComma
'
'    GenerateSimpleTableSheetForSpecifiedBookWithStringParams objBook, rstr2ndSheetName, vstr2ndDataTableParamsDelimitedByComma
'
'    DeleteDummySheetWhenItExists objBook
'
'    ForceToSaveAsBookWithoutDisplayAlert objBook, rstrBookPath
'
'    objBook.Close
'End Sub
'
'
''''
''''
''''
'Private Function mfstrGetTestBookPath(ByVal vstrSubDirectoryName As String, ByVal vstrBookName As String) As String
'
'    Dim strDir As String
'
'    strDir = mfstrGetTemporaryBookDir(vstrSubDirectoryName)
'
'    mfstrGetTestBookPath = strDir & "\" & vstrBookName
'End Function
'
''''
''''
''''
'Private Function mfstrGetTemporaryBookDir(ByVal vstrSubDirectoryName As String) As String
'
'    Dim strDir As String
'
'    strDir = GetTemporaryDevelopmentVBARootDir() & "\OutputBooks\AdoBookCompare\" & vstrSubDirectoryName
'
'    ForceToCreateDirectoryByVbaDir strDir
'
'    mfstrGetTemporaryBookDir = strDir
'End Function
'
'''--VBA_Code_File--<ADOConStrToolsForCSV.bas>--
'Attribute VB_Name = "ADOConStrToolsForCSV"
''
''   Generate ADODB connection string to CSV files
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access.
''       Dependent on ADO
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Tue,  7/Feb/2023    Kalmclaeyd Tarclanus    Separated from ADOConnectStrGenerator
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''''
'''' CSV files connection
''''
'Public Function GetADODBConnectionOleDbStringForCSVFile(ByVal vstrDirectoryPath As String, _
'        Optional ByVal venmAccessOLEDBProviderType As AccessOLEDBProviderType = AccessOLEDBProviderType.AceOLEDB_12P0, _
'        Optional ByVal vblnHeaderFieldTitleExists As Boolean = True) As String
'
'    Dim strConnectionString As String
'    Dim strProvider As String, strExtendedProperties As String
'
'    strProvider = GetAccessOLEDBProviderNameFromEnm(venmAccessOLEDBProviderType)
'
'    Select Case venmAccessOLEDBProviderType
'
'        Case AccessOLEDBProviderType.AceOLEDB_12P0, AccessOLEDBProviderType.AceOLEDB_16P0
'
'            strExtendedProperties = "Text;HDR="
'
'            If vblnHeaderFieldTitleExists Then
'
'                strExtendedProperties = strExtendedProperties & "Yes;"
'            Else
'                strExtendedProperties = strExtendedProperties & "No;"
'            End If
'
'            strExtendedProperties = strExtendedProperties & "FMT=Delimited;"
'
'            strConnectionString = "Provider=" & strProvider & ";" & _
'                            "Data Source=" & vstrDirectoryPath & "\;Extended Properties=""" & strExtendedProperties & """"
'
'        Case AccessOLEDBProviderType.JetOLEDB_4P0, AccessOLEDBProviderType.JetOLEDB_3P51
'
'            strExtendedProperties = "Text;HDR="
'
'            If vblnHeaderFieldTitleExists Then
'
'                strExtendedProperties = strExtendedProperties & "YES;"
'            Else
'                strExtendedProperties = strExtendedProperties & "NO;"
'            End If
'
'            strExtendedProperties = strExtendedProperties & ";FMT=Delimited"
'
'            strConnectionString = "Provider=" & strProvider & ";" & _
'                            "Data Source=" & vstrDirectoryPath & "\;Extended Properties=""" & strExtendedProperties & """"
'
'    End Select
'
'    GetADODBConnectionOleDbStringForCSVFile = strConnectionString
'End Function
'''--VBA_Code_File--<ADOConStrOfAceOleDbCsv.cls>--
'VERSION 1.0 CLASS
'BEGIN
'  MultiUse = -1  'True
'END
'Attribute VB_Name = "ADOConStrOfAceOleDbCsv"
'Attribute VB_GlobalNameSpace = False
'Attribute VB_Creatable = False
'Attribute VB_PredeclaredId = False
'Attribute VB_Exposed = False
''
''   ADO connection string generator for ADO-DB connection with CSV files
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on ADO
''       Dependent on Microsoft Office Access Data Base Engine OLE DB Provider,
''       which should have been the installed when the Microsoft Office Excel software has been installed.
''       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access.
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Fri, 10/Jun/2022    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Interface implementations
''///////////////////////////////////////////////
'Implements IADOConnectStrGenerator
'
''///////////////////////////////////////////////
''/// Declarations
''///////////////////////////////////////////////
'Private mobjCoreGenerator As ADOConStrOfAceOleDb
'
'
'Private menmAccessOLEDBProviderType As AccessOLEDBProviderType
'
'Private mstrDirectoryPath As String
'
'Private mblnHeaderFieldTitleExists As Boolean
'
''///////////////////////////////////////////////
''/// Event Handlers
''///////////////////////////////////////////////
'Private Sub Class_Initialize()
'
'    Set mobjCoreGenerator = New ADOConStrOfAceOleDb
'End Sub
'
'
''///////////////////////////////////////////////
''/// Implemented operations
''///////////////////////////////////////////////
'Private Function IADOConnectStrGenerator_GetConnectionString() As String
'
'    IADOConnectStrGenerator_GetConnectionString = GetConnectionStringForCSVFile
'End Function
''///////////////////////////////////////////////
''/// Properties
''///////////////////////////////////////////////
''**---------------------------------------------
''** CSV directory connection by ACCESS OLE-DB provider
''**---------------------------------------------
'Public Property Get ConnectingFilePath() As String
'
'    ConnectingFilePath = mobjCoreGenerator.ConnectingFilePath
'End Property
'
''''
'''' read-only
''''
'Public Property Get ConnectingDirectoryPath() As String
'
'    ConnectingDirectoryPath = mstrDirectoryPath
'End Property
'
'Public Property Get HeaderFieldTitleExists() As Boolean
'
'    HeaderFieldTitleExists = mblnHeaderFieldTitleExists
'End Property
'
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''''
''''
''''
'Public Function GetConnectionString() As String
'
'    GetConnectionString = IADOConnectStrGenerator_GetConnectionString()
'End Function
'
''''
'''' CSV files connection
''''
'Public Function GetConnectionStringForCSVFile() As String
'
'    GetConnectionStringForCSVFile = GetADODBConnectionOleDbStringForCSVFile(mstrDirectoryPath, menmAccessOLEDBProviderType, mblnHeaderFieldTitleExists)
'End Function
'
''''
'''' connect to CSV directory from a CSV file
''''
'Public Sub SetCsvFileConnection(ByVal vstrCSVFilePath As String, _
'        Optional vblnHeaderFieldTitleExists As Boolean = True, _
'        Optional ByVal venmAccessOLEDBProviderType As AccessOLEDBProviderType = AccessOLEDBProviderType.AceOLEDB_12P0)
'
'    mobjCoreGenerator.ConnectingFilePath = vstrCSVFilePath
'
'    With New Scripting.FileSystemObject
'
'        SetCsvDirectoryConnection .GetParentFolderName(vstrCSVFilePath), vblnHeaderFieldTitleExists, venmAccessOLEDBProviderType
'    End With
'End Sub
'
''''
'''' connect to CSV directory
''''
'Public Sub SetCsvDirectoryConnection(ByVal vstrCsvDirectoryPath As String, _
'        Optional vblnHeaderFieldTitleExists As Boolean = True, _
'        Optional ByVal venmAccessOLEDBProviderType As AccessOLEDBProviderType = AccessOLEDBProviderType.AceOLEDB_12P0)
'
'    mstrDirectoryPath = vstrCsvDirectoryPath
'
'    mblnHeaderFieldTitleExists = vblnHeaderFieldTitleExists
'
'    menmAccessOLEDBProviderType = venmAccessOLEDBProviderType
'
'    mobjCoreGenerator.OleDbProviderName = GetAccessOLEDBProviderDescriptionFromEnm(menmAccessOLEDBProviderType)
'End Sub
'
'''--VBA_Code_File--<CsvAdoConnector.cls>--
'VERSION 1.0 CLASS
'BEGIN
'  MultiUse = -1  'True
'END
'Attribute VB_Name = "CsvAdoConnector"
'Attribute VB_GlobalNameSpace = False
'Attribute VB_Creatable = False
'Attribute VB_PredeclaredId = False
'Attribute VB_Exposed = False
''
''   connect CSV files by ADO-DB interface
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on ADO
''       Dependent on Microsoft Office Access Data Base Engine OLE DB Provider,
''       which should have been the installed when the Microsoft Office Excel software has been installed.
''       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access.
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Thu,  9/Jun/2022    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Interface implementations
''///////////////////////////////////////////////
'Implements IADOConnector
'
''///////////////////////////////////////////////
''/// Declarations
''///////////////////////////////////////////////
'Private mobjConnector As ADOConnector: Private mitfConnector As IADOConnector
'
'Private mobjConnectStrGenerator As ADOConStrOfAceOleDbCsv
'
'
''///////////////////////////////////////////////
''/// Event Handlers
''///////////////////////////////////////////////
'Private Sub Class_Initialize()
'
'    Set mobjConnector = New ADOConnector: Set mitfConnector = mobjConnector
'
'    Set mobjConnectStrGenerator = New ADOConStrOfAceOleDbCsv
'End Sub
'
'Private Sub Class_Terminate()
'
'    Me.CloseConnection
'
'    Set mitfConnector = Nothing
'
'    Set mobjConnector = Nothing
'
'    Set mobjConnectStrGenerator = Nothing
'End Sub
'
''///////////////////////////////////////////////
''/// Properties
''///////////////////////////////////////////////
''''
''''
''''
'Public Property Get ADOConnectorInterface() As IADOConnector
'
'    Set ADOConnectorInterface = Me
'End Property
'
''///////////////////////////////////////////////
''/// Properties - connoted ADO connector
''///////////////////////////////////////////////
'Public Property Let AllowToRecordSQLLog(ByVal vblnAllowToRecord As Boolean)
'
'    mobjConnector.AllowToRecordSQLLog = vblnAllowToRecord
'End Property
'Public Property Get AllowToRecordSQLLog() As Boolean
'
'    AllowToRecordSQLLog = mobjConnector.AllowToRecordSQLLog
'End Property
'
'
'Public Property Get SQLExecutionResult() As SQLResult
'
'    Set SQLExecutionResult = IADOConnector_SQLExecutionResult
'End Property
'
'Public Property Get IsSqlExecutionFailed() As Boolean
'
'    IsSqlExecutionFailed = mobjConnector.IsSqlExecutionFailed
'End Property
'
''///////////////////////////////////////////////
''/// Implemented properties
''///////////////////////////////////////////////
'Private Property Let IADOConnector_IsConnected(ByVal vblnIsConnected As Boolean)
'
'    If vblnIsConnected Then
'
'        mitfConnector.IsConnected = mfblnConnect()
'    Else
'        mobjConnector.CloseConnection
'
'        mitfConnector.IsConnected = mobjConnector.IsConnected
'    End If
'End Property
'Private Property Get IADOConnector_IsConnected() As Boolean
'
'    IADOConnector_IsConnected = mfblnConnect()
'End Property
'
'Private Property Set IADOConnector_ADOConnection(ByVal vobjADOConnection As ADODB.Connection)
'
'    Set mitfConnector.ADOConnection = vobjADOConnection
'End Property
'Private Property Get IADOConnector_ADOConnection() As ADODB.Connection
'
'    Set IADOConnector_ADOConnection = mitfConnector.ADOConnection
'End Property
'
'Private Property Set IADOConnector_SQLExecutionResult(ByVal vobjSQLResult As SQLResult)
'
'    Set mitfConnector.SQLExecutionResult = vobjSQLResult
'End Property
'Private Property Get IADOConnector_SQLExecutionResult() As SQLResult
'
'    Set IADOConnector_SQLExecutionResult = mitfConnector.SQLExecutionResult
'End Property
'
'
'Private Property Get IADOConnector_CommandTimeout() As Long
'
'    IADOConnector_CommandTimeout = mobjConnector.CommandTimeout
'End Property
'Private Property Let IADOConnector_CommandTimeout(ByVal vintTimeout As Long)
'
'    mobjConnector.CommandTimeout = vintTimeout
'End Property
'
'Private Property Get IADOConnector_SuppressToShowUpSqlExecutionError() As Boolean
'
'    IADOConnector_SuppressToShowUpSqlExecutionError = mitfConnector.SuppressToShowUpSqlExecutionError
'End Property
'Private Property Let IADOConnector_SuppressToShowUpSqlExecutionError(ByVal vblnSuppressToShowUpSqlExecutionError As Boolean)
'
'    mitfConnector.SuppressToShowUpSqlExecutionError = vblnSuppressToShowUpSqlExecutionError
'End Property
'
'Private Property Get IADOConnector_RdbConnectionInformation() As RdbConnectionInformationFlag
'
'    IADOConnector_RdbConnectionInformation = AdoOleDbToCsvFileAsRdbFlag
'End Property
'Private Property Let IADOConnector_RdbConnectionInformation(ByVal venmRdbConnectionInformationFlag As RdbConnectionInformationFlag)
'
'    ' Nothing to do
'End Property
'
''///////////////////////////////////////////////
''/// Properties - connoted ADOConnectionSetting
''///////////////////////////////////////////////
'Public Property Get ConnectSetting() As ADOConnectionSetting
'
'    Set ConnectSetting = mobjConnector.ConnectSetting
'End Property
'
''''
'''' unit is second. [s]
''''
'Public Property Let CommandTimeout(ByVal vintTimeout As Long)
'
'    IADOConnector_CommandTimeout = vintTimeout
'End Property
'Public Property Get CommandTimeout() As Long
'
'    CommandTimeout = IADOConnector_CommandTimeout
'End Property
'
'Public Property Get SuppressToShowUpSqlExecutionError() As Boolean
'
'    SuppressToShowUpSqlExecutionError = IADOConnector_SuppressToShowUpSqlExecutionError
'End Property
'Public Property Let SuppressToShowUpSqlExecutionError(ByVal vblnSuppressToShowUpSqlExecutionError As Boolean)
'
'    IADOConnector_SuppressToShowUpSqlExecutionError = vblnSuppressToShowUpSqlExecutionError
'End Property
'
''///////////////////////////////////////////////
''/// Properties - CSV files connection by ACCESS OLE-DB provider
''///////////////////////////////////////////////
''''
'''' connecting CSV file path
''''
'Public Property Get ConnectingFilePath() As String
'
'    ConnectingFilePath = mobjConnectStrGenerator.ConnectingFilePath
'End Property
'
''''
'''' connecting directory path, which includes CSV files
''''
'Public Property Get ConnectingDirectoryPath() As String
'
'    ConnectingDirectoryPath = mobjConnectStrGenerator.ConnectingDirectoryPath
'End Property
'
''''
'''' when the load Excel sheet doesn't have the field header title text, this is False
''''
'Public Property Get HeaderFieldTitleExists() As Boolean
'
'    HeaderFieldTitleExists = mobjConnectStrGenerator.HeaderFieldTitleExists
'End Property
'
'
''///////////////////////////////////////////////
''/// Implemented operations
''///////////////////////////////////////////////
''''
'''' overrides
''''
'Public Function IADOConnector_GetRecordset(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag) As ADODB.Recordset
'
'    Set IADOConnector_GetRecordset = Me.GetRecordset(vstrSQL, venmShowUpAdoErrorOptionFlag)
'End Function
'
''''
''''
''''
'Private Function IADOConnector_ExecuteSQL(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag) As ADODB.Recordset
'
'    Set IADOConnector_ExecuteSQL = ExecuteSQL(vstrSQL, venmShowUpAdoErrorOptionFlag)
'End Function
'
''''
'''' If the ExecuteSQL method had been used at least one time, this execute ADODB.Connect.CommitTrans()
''''
'Private Sub IADOConnector_CommitTransaction()
'
'    mitfConnector.CommitTransaction
'End Sub
'
''''
'''' If the ExecuteSQL method had been used at least one time, this execute CommitTransaction before execute ADODB.Connect.Close()
''''
'Private Sub IADOConnector_CloseConnection()
'
'    Me.CloseConnection
'End Sub
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
'''
''' set input CSV directory path form a CSV file path
'''
'Public Sub SetInputCsvFileConnection(ByVal vstrCSVFilePath As String, Optional vblnHeaderFieldTitleExists As Boolean = True, Optional ByVal venmAccessOLEDBProviderType As AccessOLEDBProviderType = AccessOLEDBProviderType.AceOLEDB_12P0)
'
'    With mobjConnectStrGenerator
'
'        If .ConnectingFilePath <> "" And .ConnectingFilePath <> vstrCSVFilePath Then
'
'            Me.CloseConnection
'        End If
'    End With
'
'    mobjConnectStrGenerator.SetCsvFileConnection vstrCSVFilePath, vblnHeaderFieldTitleExists, venmAccessOLEDBProviderType
'End Sub
'
''''
'''' set input directory path, which includes CSV files
''''
'Public Sub SetInputCsvDirectoryConnection(ByVal vstrCsvDirectoryPath As String, Optional vblnHeaderFieldTitleExists As Boolean = True, Optional ByVal venmAccessOLEDBProviderType As AccessOLEDBProviderType = AccessOLEDBProviderType.AceOLEDB_12P0)
'
'    With mobjConnectStrGenerator
'
'        If .ConnectingDirectoryPath <> "" And .ConnectingDirectoryPath <> vstrCsvDirectoryPath Then
'
'            Me.CloseConnection
'        End If
'    End With
'
'    mobjConnectStrGenerator.SetCsvDirectoryConnection vstrCsvDirectoryPath, vblnHeaderFieldTitleExists, venmAccessOLEDBProviderType
'End Sub
'
''''
'''' Close ADO connection
''''
'Public Sub CloseConnection()
'
'    mobjConnector.CloseConnection
'End Sub
'
''''
'''' get recordset object by the Select SQL
''''
'Public Function GetRecordset(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag) As ADODB.Recordset
'
'    If mfblnConnect() Then
'
'        Set GetRecordset = mobjConnector.GetRecordset(vstrSQL, venmShowUpAdoErrorOptionFlag)
'    End If
'End Function
'
''''
'''' input SQL, such as INSERT, UPDATE, DELETE
''''
'Public Function ExecuteSQL(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag) As ADODB.Recordset
'
'    Dim objRSet As ADODB.Recordset
'
'    Set objRSet = Nothing
'
'    If mfblnConnect() Then
'
'        Set objRSet = mobjConnector.ExecuteSQL(vstrSQL, venmShowUpAdoErrorOptionFlag)
'    End If
'
'    Set ExecuteSQL = objRSet
'End Function
'
'
''''
''''
''''
'Public Function IsConnected() As Boolean
'
'    IsConnected = mfblnConnect()
'End Function
'
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
''''
'''' connect directory including CSV files by ADO
''''
'Private Function mfblnConnect() As Boolean
'
'    Dim blnIsConnected As Boolean
'
'    TryToConnectRDBProviderByAdoConnectionString blnIsConnected, mobjConnector, mobjConnectStrGenerator
'
'    mfblnConnect = blnIsConnected
'End Function
'
'
'
'
'''--VBA_Code_File--<CsvConnectingCommonPath.bas>--
'Attribute VB_Name = "CsvConnectingCommonPath"
''
''   CSV ADO common utitlies
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Fri, 10/Jun/2022    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
'
''///////////////////////////////////////////////
''/// Constants
''///////////////////////////////////////////////
'Private Const mstrModuleKey As String = "CsvConnectingCommonPath"   ' 1st part of registry sub-key
'
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''''
''''
''''
'Public Function GetCSVTableNameLeteral(ByVal vstrCSVFilePath As String) As String
'
'    Dim strTableName As String
'
'    With New Scripting.FileSystemObject
'
'        strTableName = "[" & .GetFileName(vstrCSVFilePath) & "]"
'    End With
'
'    GetCSVTableNameLeteral = strTableName
'End Function
'
'
''///////////////////////////////////////////////
''/// Operations - a UNC path portability solution
''///////////////////////////////////////////////
''**---------------------------------------------
''** solute path generaly
''**---------------------------------------------
''''
'''' solute directory path from registry CURRENT_USER
''''
'Public Function GetCsvAdoConnectingGeneralLogBookPath() As String
'
'    GetCsvAdoConnectingGeneralLogBookPath = GetCurrentBookOutputDir() & "\SQLTests\CsvAdoConnectingUnittests.xlsx"
'End Function
'
'
'''--VBA_Code_File--<SqlAdoConnectingUTfCsv.bas>--
'Attribute VB_Name = "SqlAdoConnectingUTfCsv"
''
''   Sanity tests to connecting a CSV files directory in a directory using ADO OLE DB
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on ADO
''       Dependent on Microsoft Office Access Data Base Engine OLE DB Provider,
''       which should have been the installed when the Microsoft Office Excel software has been installed.
''       Dependent on UTfCreateDataTable.bas
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Fri,  4/Aug/2023    Kalmclaeyd Tarclanus    Initial creation
''
'
'Option Explicit
'
'
''///////////////////////////////////////////////
''/// Sanity tests
''///////////////////////////////////////////////
''**---------------------------------------------
''** CsvAdoConnector
''**---------------------------------------------
''''
''''
''''
'Public Sub msubSanityTestToConnectCsvAndOutputRecordsetToImmediateWiondowByASampleTable()
'
'    Dim strInputCSVPath As String, strSQL As String
'
'    strInputCSVPath = GetCSVPathAfterCheckFileExistenceOrCreateWhenItDoesntExist(BasicSampleDT, 15)
'
'    With New CsvAdoConnector
'
'        .SetInputCsvFileConnection strInputCSVPath
'
'        strSQL = "SELECT * FROM " & GetCSVTableNameLeteral(strInputCSVPath)
'
'        DebugCol GetTableCollectionFromRSet(.GetRecordset(strSQL))
'    End With
'End Sub
'
'
''''
'''' Create an in-line table from the only SELECT sql inside
''''
'Private Sub msubSanityTestToConnectCsvAndOutputRecordsetToImmediateWiondowByVirtualTable()
'
'    Dim strSQL As String, strInputCSVPath As String
'
'    strInputCSVPath = GetCSVPathAfterCheckFileExistenceOrCreateWhenItDoesntExist(BasicSampleDT, 15)
'
'    With New CsvAdoConnector
'
'        .SetInputCsvFileConnection strInputCSVPath
'
'        .AllowToRecordSQLLog = True
'
'        strSQL = "SELECT 'ABC' AS A_Column, 'DEF' AS B_Column"
'
'        ' Confirm the Recordset of the virtual-table in the SQL without reading CSV file.
'
'        DebugCol GetTableCollectionFromRSet(.GetRecordset(strSQL))
'    End With
'End Sub
'
'''--VBA_Code_File--<OperateSqLite3.bas>--
'Attribute VB_Name = "OperateSqLite3"
''
''   Utilities for serve SQLite3 at Windows
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on SQLite3
''       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Tue, 11/Jul/2023    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
'
''///////////////////////////////////////////////
''/// Constants
''///////////////////////////////////////////////
'Private Const mstrModuleKey As String = "OperateSqLite3"   ' 1st part of registry sub-key
'
'Private Const mstrSubjectInstalledSqLite3PathKey As String = "SQLite3"   ' 2nd part of registry sub-key, recommend local hard drive
'
''///////////////////////////////////////////////
''/// Declarations
''///////////////////////////////////////////////
''**---------------------------------------------
''** Check whether the sqlite3.exe is installed or not
''**---------------------------------------------
'Private mblnIsSQLite3InstallationChecked As Boolean    ' VBA default value is false
'
'Private mblnIsSQLite3InstalledInThisComputer As Boolean                ' VBA default value is false
'
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''''
''''
''''
'Public Function CreateSqLiteDatabaseByCmd(ByVal vstrDbPath As String, Optional ByVal vblnAllowToDeleteAllTablesIfItAlreadyExisted As Boolean = False) As String
'
'    Dim strDbFileName As String, strDir As String, strSqLiteCommand As String, strCmdCommand As String, strStdOut As String
'
'    strStdOut = ""
'
'    If Not FileExistsByVbaDir(vstrDbPath) Or vblnAllowToDeleteAllTablesIfItAlreadyExisted Then
'
'        GetParentDirectoryPathToArgByVbaDir strDir, vstrDbPath
'
'        GetFileNameToArgFromPathByVbaDir strDbFileName, vstrDbPath
'
'        If Not DirectoryExistsByVbaDir(strDir) Then
'
'            ' The following uses FileSystemObject
'            ForceToCreateDirectory strDir
'        End If
'
'
'        If vblnAllowToDeleteAllTablesIfItAlreadyExisted Then
'
'            strSqLiteCommand = ".open --new " & strDbFileName
'        Else
'            strSqLiteCommand = ".open " & strDbFileName
'        End If
'
'        strCmdCommand = "echo " & strSqLiteCommand & " | " & mfstrGetSqLite3Path()
'
'        strStdOut = ExecuteWinCmd(strCmdCommand, strDir)
'    End If
'
'
'    CreateSqLiteDatabaseByCmd = strStdOut
'End Function
'
''''
''''
''''
'Public Function ExecuteSqlInSqLiteDatabaseByCmd(ByVal vstrSQL As String, ByVal vstrDbPath As String) As String
'
'    Dim strDir As String, strDbFileName As String, strCmdCommand As String, strStdOut As String
'
'    strDir = GetParentDirectoryPathByVbaDir(vstrDbPath)
'
'    strDbFileName = GetFileNameFromPathByVbaDir(vstrDbPath)
'
'    strCmdCommand = "echo " & vstrSQL & " | " & mfstrGetSqLite3Path() & " " & strDbFileName
'
'    strStdOut = ExecuteWinCmd(strCmdCommand, strDir)
'
'    ExecuteSqlInSqLiteDatabaseByCmd = strStdOut
'End Function
'
'
''**---------------------------------------------
''** solute path generaly
''**---------------------------------------------
''''
'''' search path from registry CURRENT_USER
''''
'Private Function mfstrGetSqLite3Path() As String
'
'    Dim objSubPaths As Collection, strTargetPath As String, strModuleNameAndSubjectKey As String
'
'
'    strModuleNameAndSubjectKey = mstrModuleKey & "\" & mstrSubjectInstalledSqLite3PathKey
'
'    strTargetPath = GetUNCPathToCurUserReg(strModuleNameAndSubjectKey)
'
'    If Not mblnIsSQLite3InstallationChecked Then
'
'        If strTargetPath = "" Then
'
'            strTargetPath = SearchSpecifiedFileFromCurrentHardDrivesAndSetRegistryIfFound(strModuleNameAndSubjectKey, "sqlite3.exe", "ODBC Driver")
'        End If
'
'        If strTargetPath <> "" Then
'
'             mblnIsSQLite3InstalledInThisComputer = True
'
'             SetUNCPathToCurUserReg strModuleNameAndSubjectKey, strTargetPath
'        End If
'
'        mblnIsSQLite3InstallationChecked = True
'    End If
'
'    mfstrGetSqLite3Path = strTargetPath
'End Function
'
'
''''
'''' Individual-Set-Registy - AIP Label control
''''
'Public Sub ISRInInstalledSqLite3PathOfGetSqLite3Path(ByVal vstrTargetPath As String)
'
'    SetUNCPathToCurUserReg mstrModuleKey & "\" & mstrSubjectInstalledSqLite3PathKey, vstrTargetPath
'End Sub
'Public Sub IDelRInInstalledSqLite3PathOfGetSqLite3Path()
'
'    DeleteUNCPathAllToCurUserReg mstrModuleKey & "\" & mstrSubjectInstalledSqLite3PathKey
'End Sub
'
'
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''**---------------------------------------------
''** About SQLite relative path
''**---------------------------------------------
''''
'''' About 'sqlite3.exe' installed path
''''
'Private Sub msubSanityTestToGetSqLite3Path()
'
'    Debug.Print mfstrGetSqLite3Path()
'End Sub
'
'''--VBA_Code_File--<ADOConStrToolsForSqLite.bas>--
'Attribute VB_Name = "ADOConStrToolsForSqLite"
''
''   Generate ADODB connection string to a SQLite data base
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on ADO
''       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Tue, 20/Jul/2023    Kalmclaeyd Tarclanus    Initial creation
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Constants
''///////////////////////////////////////////////
'Private Const mstrSqLite3OdbcDriverName As String = "SQLite3 ODBC Driver"
'
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''''
''''
''''
'Public Function GetADODBConnectionOdbcStringToSqLite3Db(ByVal vstrDbPath As String) As String
'
'    Dim strConnectionString As String, strProvider As String
'
'    strConnectionString = "DRIVER=" & mstrSqLite3OdbcDriverName & ";Database=" & vstrDbPath & ";"
'
'    GetADODBConnectionOdbcStringToSqLite3Db = strConnectionString
'End Function
'
'
''**---------------------------------------------
''** for control USetAdoOdbcConStrForPgSql user-form
''**---------------------------------------------
''''
''''
''''
'Public Function GetInitialUserInputParametersDicForAdoDSNlessConnectionOfSqLite(Optional ByVal vstrDbFilePath As String = "", _
'        Optional ByVal vstrDriverName As String = mstrSqLite3OdbcDriverName) As Scripting.Dictionary
'
'    Dim objDic As Scripting.Dictionary
'
'    Set objDic = New Scripting.Dictionary
'
'    With objDic
'
'        .Add "UseDSN", False
'
'        .Add "DriverName", vstrDriverName
'
'        .Add mstrAdoDbFilePathKey, vstrDbFilePath
'
'        .Add mstrAdoConnectionTypeAndDestinationKey, GetDescriptionOfOdbcConnectionDestinationRDBType(OdbcConnectionDestinationToSQLite3)
'    End With
'
'    Set GetInitialUserInputParametersDicForAdoDSNlessConnectionOfSqLite = objDic
'End Function
'
'
''''
''''
''''
'Public Sub UpdateADOConStrOfOdbcSqLiteFromInputDic(ByRef robjADOConStrOfOdbcSqLite As ADOConStrOfOdbcSqLite, ByRef robjUserInputParametersDic As Scripting.Dictionary)
'
'    Dim blnUseDSN As Boolean
'
'    blnUseDSN = robjUserInputParametersDic.Item("UseDSN")
'
'    robjADOConStrOfOdbcSqLite.UseDSN = blnUseDSN
'
'    If blnUseDSN Then
'
'        UpdateADOConStrOfDSNFromInputDic robjADOConStrOfOdbcSqLite.DsnAdoConStrGenerator, robjUserInputParametersDic
'    Else
'        UpdateADOConStrOfDsnlessSqLiteFromInputDic robjADOConStrOfOdbcSqLite.DsnlessAdoConStrGenerator, robjUserInputParametersDic
'    End If
'End Sub
'
''''
''''
''''
'Public Sub UpdateADOConStrOfDsnlessSqLiteFromInputDic(ByRef robjADOConStrOfDsnlessSqLite As ADOConStrOfDsnlessSqLite, ByRef robjUserInputParametersDic As Scripting.Dictionary)
'
'    With robjUserInputParametersDic
'
'        robjADOConStrOfDsnlessSqLite.SetSQLite3OdbcConnectionWithoutDSN .Item(mstrAdoDbFilePathKey)
'    End With
'End Sub
'
''''
''''
''''
'Public Function GetADOConStrOfDsnlessSqLiteFromInputDic(ByRef robjUserInputParametersDic As Scripting.Dictionary) As ADOConStrOfDsnlessSqLite
'
'    Dim objADOConStrOfDsnlessSqLite As ADOConStrOfDsnlessSqLite
'
'    Set objADOConStrOfDsnlessSqLite = New ADOConStrOfDsnlessSqLite
'
'    UpdateADOConStrOfDsnlessSqLiteFromInputDic objADOConStrOfDsnlessSqLite, robjUserInputParametersDic
'
'    Set GetADOConStrOfDsnlessSqLiteFromInputDic = objADOConStrOfDsnlessSqLite
'End Function
'
'
''''
''''
''''
'Public Function GetADOConnectionOdbcSqLiteStringFromInputDic(ByRef robjUserInputParametersDic As Scripting.Dictionary) As String
'
'    Dim strConnection As String, blnUseDSN As Boolean
'
'    With robjUserInputParametersDic
'
'        blnUseDSN = .Item("UseDSN")
'
'        If blnUseDSN Then
'
'            strConnection = GetADOConStrOfDSNFromInputDic(robjUserInputParametersDic).GetConnectionString()
'        Else
'            strConnection = GetADOConStrOfDsnlessSqLiteFromInputDic(robjUserInputParametersDic).GetConnectionString()
'        End If
'    End With
'
'    GetADOConnectionOdbcSqLiteStringFromInputDic = strConnection
'End Function
'
''**---------------------------------------------
''** ADO connection string parameters solution interfaces by each special form
''**---------------------------------------------
''''
''''
''''
'''' <Argument>rblnIsRequestedToCancelAdoConnection: Output</Argument>
'''' <Argument>robjADOConStrOfOdbcSqLite: Output</Argument>
'''' <Argument>vstrSettingKeyName: Input</Argument>
'''' <Argument>vstrDriverName: Input</Argument>
'''' <Argument>vstrDbFilePath: Input</Argument>
'''' <Argument>vstrDSN: Input</Argument>
'Public Sub SetOdbcConnectionParametersBySqLiteForm(ByRef rblnIsRequestedToCancelAdoConnection As Boolean, _
'        ByRef robjADOConStrOfOdbcSqLite As ADOConStrOfOdbcSqLite, _
'        ByVal vstrSettingKeyName As String, _
'        Optional ByVal vstrDriverName As String = mstrSqLite3OdbcDriverName, _
'        Optional ByVal vstrDbFilePath As String = "", _
'        Optional ByVal vstrDSN As String = "")
'
'    Dim objUserInputParametersDic As Scripting.Dictionary, blnIsOpeningFormNeeded As Boolean
'
'    rblnIsRequestedToCancelAdoConnection = False
'
'    blnIsOpeningFormNeeded = IsOpeningFormNeededAfterLoadFromUserInputParametersDicFirstCacheAndSecondFunctionInputsOfSqLite(objUserInputParametersDic, vstrSettingKeyName, vstrDriverName, vstrDbFilePath, vstrDSN)
'
'    If blnIsOpeningFormNeeded Then
'
'        msubSetUserInputParametersDicOfSqLiteByOpeningFormWithFixedSettingKeyName rblnIsRequestedToCancelAdoConnection, objUserInputParametersDic, vstrSettingKeyName
'    End If
'
'    If Not rblnIsRequestedToCancelAdoConnection Then
'
'        UpdateADOConStrOfOdbcSqLiteFromInputDic robjADOConStrOfOdbcSqLite, objUserInputParametersDic
'    End If
'End Sub
'
''''
''''
''''
'''' <Argument>robjUserInputParametersDic: Output</Argument>
'''' <Argument>vstrSettingKeyName: Input</Argument>
'''' <Argument>vstrDriverName: Input</Argument>
'''' <Argument>vstrDbFilePath: Input</Argument>
'''' <Argument>vstrDSN: Input</Argument>
'Private Function IsOpeningFormNeededAfterLoadFromUserInputParametersDicFirstCacheAndSecondFunctionInputsOfSqLite(ByRef robjUserInputParametersDic As Scripting.Dictionary, _
'        ByVal vstrSettingKeyName As String, _
'        Optional ByVal vstrDriverName As String = mstrSqLite3OdbcDriverName, _
'        Optional ByVal vstrDbFilePath As String = "", _
'        Optional ByVal vstrDSN As String = "") As Boolean
'
'    Dim blnIsOpeningFormNeeded As Boolean
'
'    blnIsOpeningFormNeeded = True
'
'    ' Try to get parameters from Win-registry cache
'
'    Set robjUserInputParametersDic = GetDicOfAdoConnectionStringInfoFromReadingWinRegValues(vstrSettingKeyName)
'
'    Select Case True
'
'        Case robjUserInputParametersDic Is Nothing, robjUserInputParametersDic.Count = 0
'
'            blnIsOpeningFormNeeded = IsOpeningFormNeededAfterInitializingUserInputPrametersDicOfSqLite(robjUserInputParametersDic, vstrDriverName, vstrDbFilePath, vstrDSN)
'
'    End Select
'
'    IsOpeningFormNeededAfterLoadFromUserInputParametersDicFirstCacheAndSecondFunctionInputsOfSqLite = blnIsOpeningFormNeeded
'End Function
'
'
''''
''''
''''
'''' <Argument>robjUserInputParametersDic: Output</Argument>
'''' <Argument>vstrDriverName: Input</Argument>
'''' <Argument>vstrDbFilePath: Input</Argument>
'''' <Argument>vstrDSN: Input</Argument>
'Private Function IsOpeningFormNeededAfterInitializingUserInputPrametersDicOfSqLite(ByRef robjUserInputParametersDic As Scripting.Dictionary, _
'        Optional ByVal vstrDriverName As String = mstrSqLite3OdbcDriverName, _
'        Optional ByVal vstrDbFilePath As String = "", _
'        Optional ByVal vstrDSN As String = "") As Boolean
'
'
'    Dim blnIsOpeningFormNeeded As Boolean
'
'    blnIsOpeningFormNeeded = True
'
'    If vstrDSN <> "" Then
'
'        Set robjUserInputParametersDic = GetInitialUserInputParametersDicForAdoDSNConnection(OdbcConnectionDestinationToSQLite3, vstrDSN)
'    Else
'        Set robjUserInputParametersDic = GetInitialUserInputParametersDicForAdoDSNlessConnectionOfSqLite(vstrDbFilePath, vstrDriverName)
'    End If
'
'    If IsADOConnectionStringEnabled(GetADOConnectionOdbcSqLiteStringFromInputDic(robjUserInputParametersDic)) Then
'
'        If vstrDSN <> "" Or (vstrDbFilePath <> "" And FileExistsByVbaDir(vstrDbFilePath)) Then
'
'            blnIsOpeningFormNeeded = False
'        End If
'    End If
'
'    IsOpeningFormNeededAfterInitializingUserInputPrametersDicOfSqLite = blnIsOpeningFormNeeded
'End Function
'
'
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
''''
''''
''''
'''' <Argument>rblnIsRequestedToCancelAdoConnection: Output</Argument>
'''' <Argument>robjUserInputParametersDic: Input - Output</Argument>
'''' <Argument>rstrSettingKeyName: Input</Argument>
'Private Sub msubSetUserInputParametersDicOfSqLiteByOpeningFormWithFixedSettingKeyName(ByRef rblnIsRequestedToCancelAdoConnection As Boolean, _
'        ByRef robjUserInputParametersDic As Scripting.Dictionary, _
'        ByRef rstrSettingKeyName As String)
'
'
'    Dim objFormStateToSetAdoConStr As FormStateToSetAdoConStr, objUSetAdoOdbcConStrForSqLite As USetAdoOdbcConStrForSqLite
'
'    Set objFormStateToSetAdoConStr = GetFormStateToSetAdoConStrByDefaultParams(robjUserInputParametersDic, rstrSettingKeyName)
'
'    Set objUSetAdoOdbcConStrForSqLite = New USetAdoOdbcConStrForSqLite
'
'    With objUSetAdoOdbcConStrForSqLite
'
'        .SetInitialFormStateToSetAdoConStr objFormStateToSetAdoConStr, True
'
'        .Show vbModal
'    End With
'
'    AfterProcessAdoConStrFormSetting rblnIsRequestedToCancelAdoConnection, robjUserInputParametersDic, objFormStateToSetAdoConStr
'End Sub
'
'
'
''///////////////////////////////////////////////
''/// Sanity tests
''///////////////////////////////////////////////
''**---------------------------------------------
''** Object creation tests
''**---------------------------------------------
''''
''''
''''
'Private Sub msubSanityTestToCreateADOConStrOfOdbcSqLite()
'
'    Dim objADOConStrOfOdbcSqLite As ADOConStrOfOdbcSqLite, objADOConStrOfDsnlessSqLite As ADOConStrOfDsnlessSqLite
'
'    Set objADOConStrOfOdbcSqLite = New ADOConStrOfOdbcSqLite
'
'    Set objADOConStrOfDsnlessSqLite = New ADOConStrOfDsnlessSqLite
'End Sub
'
'
''**---------------------------------------------
''** USetAdoOdbcConStrForSqLite form tests
''**---------------------------------------------
''''
'''' delete testing SettingKeyName from the registory
''''
'Private Sub msubSanityTestToDeleteTestingSettingKeyNamesForConnectingToSqLite()
'
'    DeleteWinRegKeyValuesOfAdoConnectionStringInfo "TestSqLiteDSNlessKey"
'
'    DeleteWinRegKeyValuesOfAdoConnectionStringInfo "TestSqLiteDSNKey"
'End Sub
'
'
'
''''
''''
''''
'Private Sub msubSanityTestToOpenUSetAdoOdbcConStrForSqLiteForDSNless()
'
'    Dim objUserInputParametersDic As Scripting.Dictionary, blnIsRequestedToCancelAdoConnection As Boolean
'
'    Set objUserInputParametersDic = GetInitialUserInputParametersDicForAdoDSNlessConnectionOfSqLite()
'
'    msubSetUserInputParametersDicOfSqLiteByOpeningFormWithFixedSettingKeyName blnIsRequestedToCancelAdoConnection, objUserInputParametersDic, "TestSqLiteDSNlessKey"
'
'    If Not blnIsRequestedToCancelAdoConnection Then
'
'        If IsADOConnectionStringEnabled(GetADOConnectionOdbcSqLiteStringFromInputDic(objUserInputParametersDic)) Then
'
'            Debug.Print "Connected to SqLite by ADO"
'        End If
'    End If
'End Sub
'
''''
'''' DSN connection to PostgreSQL
''''
'Private Sub msubSanityTestToOpenUSetAdoOdbcConStrForSqLiteForDSN()
'
'    Dim objUserInputParametersDic As Scripting.Dictionary, blnIsRequestedToCancelAdoConnection As Boolean
'
'    Set objUserInputParametersDic = GetInitialUserInputParametersDicForAdoDSNConnection(OdbcConnectionDestinationToSQLite3)
'
'    msubSetUserInputParametersDicOfSqLiteByOpeningFormWithFixedSettingKeyName blnIsRequestedToCancelAdoConnection, objUserInputParametersDic, "TestSqLiteDSNKey"
'
'    If Not blnIsRequestedToCancelAdoConnection Then
'
'        If IsADOConnectionStringEnabled(GetADOConnectionOdbcSqLiteStringFromInputDic(objUserInputParametersDic)) Then
'
'            Debug.Print "Connected to SqLite by ADO"
'        End If
'    End If
'End Sub
'''--VBA_Code_File--<ADOConStrOfDsnlessSqLite.cls>--
'VERSION 1.0 CLASS
'BEGIN
'  MultiUse = -1  'True
'END
'Attribute VB_Name = "ADOConStrOfDsnlessSqLite"
'Attribute VB_GlobalNameSpace = False
'Attribute VB_Creatable = False
'Attribute VB_PredeclaredId = False
'Attribute VB_Exposed = False
''
''   ADO ODBC connection string generator for SQLite RDB without DSN
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on ADO
''       Dependent on SQLite3 ODBC driver at this Windows
''       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access.
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Wed, 12/Jul/2023    Kalmclaeyd Tarclanus    Initial creation
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Interface implementations
''///////////////////////////////////////////////
'Implements IADOConnectStrGenerator
'
''///////////////////////////////////////////////
''/// Constants
''///////////////////////////////////////////////
'Private Const mstrSqLite3OdbcDriverName As String = "SQLite3 ODBC Driver"
'
'
''///////////////////////////////////////////////
''/// Declarations
''///////////////////////////////////////////////
'Private mstrConnectingDbFilePath As String
'
'
''///////////////////////////////////////////////
''/// Implemented properties
''///////////////////////////////////////////////
'Private Function IADOConnectStrGenerator_GetConnectionString() As String
'
'    IADOConnectStrGenerator_GetConnectionString = GetADODBConnectionOdbcDsnlessStringToSqLite3Db()
'End Function
'
''///////////////////////////////////////////////
''/// Properties
''///////////////////////////////////////////////
''**---------------------------------------------
''** ODBC DSNless connection
''**---------------------------------------------
''''
'''' read-only Driver Name for ODBC
''''
'Public Property Get DriverName() As String
'
'    DriverName = mstrSqLite3OdbcDriverName
'End Property
'
''''
'''' read-only SQLite3 data base path
''''
'Public Property Get ConnectingFilePath() As String
'
'    ConnectingFilePath = mstrConnectingDbFilePath
'End Property
'
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''''
''''
''''
'Public Sub SetSQLite3OdbcConnectionWithoutDSN(ByVal vstrDbFilePath As String)
'
'    mstrConnectingDbFilePath = vstrDbFilePath
'End Sub
'
'
'Public Function GetConnectionString() As String
'
'    GetConnectionString = GetADODBConnectionOdbcDsnlessStringToSqLite3Db()
'End Function
'
''''
''''
''''
'Public Function GetADODBConnectionOdbcDsnlessStringToSqLite3Db() As String
'
'    GetADODBConnectionOdbcDsnlessStringToSqLite3Db = GetADODBConnectionOdbcStringToSqLite3Db(mstrConnectingDbFilePath)
'End Function
'
'
'''--VBA_Code_File--<UTfAdoConnectSqLiteDb.bas>--
'Attribute VB_Name = "UTfAdoConnectSqLiteDb"
''
''   Sanity tests to connect SQLite3 database by ADO
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on both ADO and SQLite3
''       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Tue, 11/Jul/2023    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
'
''///////////////////////////////////////////////
''/// Sanity tests
''///////////////////////////////////////////////
''''
''''
''''
'Private Sub msubSanityTestToConnectSqLite3DbByAdo()
'
'    Dim strDbPath As String, strConnectionString As String
'
'    strDbPath = GetTemporarySqLiteDataBaseDir() & "\TestSqLiteSample.db"
'
'    CreateSqLiteDatabaseByCmd strDbPath
'
'    strConnectionString = "DRIVER=SQLite3 ODBC Driver;Database=" & strDbPath & ";"
'
'    If IsADOConnectionStringEnabled(strConnectionString) Then
'
'        Debug.Print "ADODB connection is enabled for SQLite3 data-base"
'    Else
'        Debug.Print "Failed to SQLite3 data-base ADODB connection, probably, the SQLite3 ODBC driver has not been isntalled yet."
'    End If
'End Sub
'
'
''''
''''
''''
'Private Sub msubSanityTestToGetPathAfterCreateSampleSqLiteDbByCmd()
'
'    CreateSqLiteDbSampleTableByUsingCmd "TestSqLiteSample.db"
'
'    ExploreParentFolderWhenOpenedThatIsNothing GetTemporarySqLiteDataBaseDir()
'End Sub
'
'
'
''''
'''' The following is to be failed because the ADOX.Catalog.Create interface is not supported at SQLite3 ODBC Driver
''''
'Private Sub msubSanityTestToGetPathAfterCreateSampleSqLiteDbByADOX()
'
'    Dim strPath As String
'
'    strPath = GetPathAfterCreateSampleSqLiteDbByADOX("TestSqLiteSample.db")
'
'    ExploreParentFolderWhenOpenedThatIsNothing strPath
'End Sub
'
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''''
''''
''''
'Public Sub OpenTestingSQLite3Cmd()
'
'    OpenCmdWithSpecifyingBothWorkingDirectoryAndCmdWindowTitleText GetTemporarySqLiteDataBaseDir(), "SQLite3 testing directory"
'End Sub
'
''''
''''
''''
'Public Function PrepareSQLiteDbAfterCreateSampleSQLiteDbIfItDoesntExist(ByRef rstrDbFileName As String) As String
'
'    Dim strDbPath As String
'
'    strDbPath = GetTemporarySqLiteDataBaseDir() & "\" & rstrDbFileName
'
'    If Not FileExistsByVbaDir(strDbPath) Then
'
'        CreateSqLiteDbSampleTableByUsingCmd rstrDbFileName
'    End If
'
'    PrepareSQLiteDbAfterCreateSampleSQLiteDbIfItDoesntExist = strDbPath
'End Function
'
'
''''
''''
''''
'Public Function CreateSqLiteDbSampleTableByUsingCmd(ByRef rstrDbFileName As String) As String
'
'    Dim strDbFileName As String, strDbPath As String, strStdOut As String
'
'    strDbFileName = rstrDbFileName
'
'    strDbPath = GetTemporarySqLiteDataBaseDir() & "\" & strDbFileName
'
'    If FileExistsByVbaDir(strDbPath) Then
'
'        VBA.Kill strDbPath
'    End If
'
'    'strStdOut = ExecuteWinCmd("echo .open --new " & rstrDbFileName & " | sqlite3", GetTemporarySqLiteDataBaseDir())
'
'    strStdOut = CreateSqLiteDatabaseByCmd(strDbPath)
'
'    Debug.Print strStdOut
'
'    CreateSqLiteDbSampleTableByUsingCmd = strDbPath
'End Function
'
'
''''
'''' This will be failed because the ADOX.Catalog.Create interface is not supported at SQLite3 ODBC Driver
''''
'Public Function GetPathAfterCreateSampleSqLiteDbByADOX(ByVal vstrDbName As String) As String
'
'    Dim strDbPath As String, strConnectionString As String
'
'
'    strDbPath = GetTemporarySqLiteDataBaseDir() & "\" & vstrDbName
'
'    If FileExistsByVbaDir(strDbPath) Then
'
'        VBA.Kill strDbPath
'    End If
'
'    strConnectionString = "DRIVER=SQLite3 ODBC Driver;Database=" & strDbPath & ";"
'
'    msubCreateSqLiteDbSampleTableByUsingSQL strConnectionString
'
'    GetPathAfterCreateSampleSqLiteDbByADOX = strDbPath
'End Function
'
'
''''
''''
''''
'Public Function GetTemporarySqLiteDataBaseDir() As String
'
'    Dim strDir As String
'
'    strDir = GetTemporaryDevelopmentVBARootDir() & "\DataBases\SQLite"
'
'    ForceToCreateDirectory strDir
'
'    GetTemporarySqLiteDataBaseDir = strDir
'End Function
'
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
''''
'''' This will be failed because the ADOX.Catalog.Create interface is not supported at SQLite3 ODBC Driver
''''
'Private Sub msubCreateSqLiteDbSampleTableByUsingSQL(ByRef rstrConnectionString As String)
'
'    Dim strTableName As String, strTableFieldTypes As String, strSQL As String
'
'    strTableName = "TestTable"
'
'    strTableFieldTypes = "氏名 varchar(20),身長 float"
'
'
'    With New ADOX.Catalog
'
'        ' create SQLite3 data-base
'
'        ' The following are to be failed because the .Create interface doesn't supported at 'SQLite3 ODBC Driver'
'
'        With .Create(rstrConnectionString)
'
'            strSQL = "create table " & strTableName & " (" & strTableFieldTypes & ");"
'
'            ' create a table
'            .Execute strSQL
'
'            .BeginTrans
'
'            strSQL = "insert into " & strTableName & " (氏名, 身長)  values('Suzuki data', 174.6);": .Execute strSQL
'
'            strSQL = "insert into " & strTableName & " (氏名, 身長)  values('Takahashi data', 166.2);": .Execute strSQL
'
'            strSQL = "insert into " & strTableName & " (氏名, 身長)  values('Tanaka data', 151.4);": .Execute strSQL
'
'            .CommitTrans
'
'            .Close
'        End With
'    End With
'End Sub
'
'''--VBA_Code_File--<SqLiteAdoConnector.cls>--
'VERSION 1.0 CLASS
'BEGIN
'  MultiUse = -1  'True
'END
'Attribute VB_Name = "SqLiteAdoConnector"
'Attribute VB_GlobalNameSpace = False
'Attribute VB_Creatable = False
'Attribute VB_PredeclaredId = False
'Attribute VB_Exposed = False
''
''   connect to SQLite3 database by ADO ODBC interface
''   Probably, this capsuled class design will not be needed for almost VBA developers, because it is too complicated.
''   However the VBA programming interfaces will be united.
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on ADO
''       Dependent on an installed SQLite ODBC driver
''       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access.
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Wed, 26/Jul/2023    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Interface implementations
''///////////////////////////////////////////////
'Implements IADOConnector
'
''///////////////////////////////////////////////
''/// Constants
''///////////////////////////////////////////////
'Private Const mstrSqLite3OdbcDriverName As String = "SQLite3 ODBC Driver"
'
''///////////////////////////////////////////////
''/// Declarations
''///////////////////////////////////////////////
'Private mobjConnector As ADOConnector: Private mitfConnector As IADOConnector
'
'Private mobjConnectStrGenerator As ADOConStrOfOdbcSqLite
'
''///////////////////////////////////////////////
''/// Event Handlers
''///////////////////////////////////////////////
'Private Sub Class_Initialize()
'
'    Set mobjConnector = New ADOConnector: Set mitfConnector = mobjConnector
'
'    Set mobjConnectStrGenerator = New ADOConStrOfOdbcSqLite
'End Sub
'
'Private Sub Class_Terminate()
'
'    Me.CloseConnection
'
'    Set mitfConnector = Nothing
'
'    Set mobjConnector = Nothing
'
'    Set mobjConnectStrGenerator = Nothing
'End Sub
'
'
''///////////////////////////////////////////////
''/// Properties
''///////////////////////////////////////////////
''''
''''
''''
'Public Property Get ADOConnectorInterface() As IADOConnector
'
'    Set ADOConnectorInterface = Me
'End Property
'
''///////////////////////////////////////////////
''/// Properties - connoted ADO connector
''///////////////////////////////////////////////
'Public Property Let AllowToRecordSQLLog(ByVal vblnAllowToRecord As Boolean)
'
'    mobjConnector.AllowToRecordSQLLog = vblnAllowToRecord
'End Property
'Public Property Get AllowToRecordSQLLog() As Boolean
'
'    AllowToRecordSQLLog = mobjConnector.AllowToRecordSQLLog
'End Property
'
'
'Public Property Get SQLExecutionResult() As SQLResult
'
'    Set SQLExecutionResult = IADOConnector_SQLExecutionResult
'End Property
'
'Public Property Get IsSqlExecutionFailed() As Boolean
'
'    IsSqlExecutionFailed = mobjConnector.IsSqlExecutionFailed
'End Property
'
''///////////////////////////////////////////////
''/// Implemented properties
''///////////////////////////////////////////////
'Private Property Let IADOConnector_IsConnected(ByVal vblnIsConnected As Boolean)
'
'    If vblnIsConnected Then
'
'        mitfConnector.IsConnected = mfblnConnect()
'    Else
'        mobjConnector.CloseConnection
'
'        mitfConnector.IsConnected = mobjConnector.IsConnected
'    End If
'End Property
'Private Property Get IADOConnector_IsConnected() As Boolean
'
'    IADOConnector_IsConnected = mfblnConnect()
'End Property
'
'Private Property Set IADOConnector_ADOConnection(ByVal vobjADOConnection As ADODB.Connection)
'
'    Set mitfConnector.ADOConnection = vobjADOConnection
'End Property
'Private Property Get IADOConnector_ADOConnection() As ADODB.Connection
'
'    Set IADOConnector_ADOConnection = mitfConnector.ADOConnection
'End Property
'
'Private Property Set IADOConnector_SQLExecutionResult(ByVal vobjSQLResult As SQLResult)
'
'    Set mitfConnector.SQLExecutionResult = vobjSQLResult
'End Property
'Private Property Get IADOConnector_SQLExecutionResult() As SQLResult
'
'    Set IADOConnector_SQLExecutionResult = mitfConnector.SQLExecutionResult
'End Property
'
'
'Private Property Get IADOConnector_CommandTimeout() As Long
'
'    IADOConnector_CommandTimeout = mobjConnector.CommandTimeout
'End Property
'Private Property Let IADOConnector_CommandTimeout(ByVal vintTimeout As Long)
'
'    mobjConnector.CommandTimeout = vintTimeout
'End Property
'
'Private Property Get IADOConnector_SuppressToShowUpSqlExecutionError() As Boolean
'
'    IADOConnector_SuppressToShowUpSqlExecutionError = mitfConnector.SuppressToShowUpSqlExecutionError
'End Property
'Private Property Let IADOConnector_SuppressToShowUpSqlExecutionError(ByVal vblnSuppressToShowUpSqlExecutionError As Boolean)
'
'    mitfConnector.SuppressToShowUpSqlExecutionError = vblnSuppressToShowUpSqlExecutionError
'End Property
'
'Private Property Get IADOConnector_RdbConnectionInformation() As RdbConnectionInformationFlag
'
'    IADOConnector_RdbConnectionInformation = AdoOdbcToSqLiteRdbFlag
'End Property
'Private Property Let IADOConnector_RdbConnectionInformation(ByVal venmRdbConnectionInformationFlag As RdbConnectionInformationFlag)
'
'    ' Nothing to do
'End Property
'
''///////////////////////////////////////////////
''/// Properties - connoted ADOConnectionSetting
''///////////////////////////////////////////////
'Public Property Get ConnectSetting() As ADOConnectionSetting
'
'    Set ConnectSetting = mobjConnector.ConnectSetting
'End Property
'
''''
'''' unit is second. [s]
''''
'Public Property Let CommandTimeout(ByVal vintTimeout As Long)
'
'    IADOConnector_CommandTimeout = vintTimeout
'End Property
'Public Property Get CommandTimeout() As Long
'
'    CommandTimeout = IADOConnector_CommandTimeout
'End Property
'
'
'Public Property Get SuppressToShowUpSqlExecutionError() As Boolean
'
'    SuppressToShowUpSqlExecutionError = IADOConnector_SuppressToShowUpSqlExecutionError
'End Property
'Public Property Let SuppressToShowUpSqlExecutionError(ByVal vblnSuppressToShowUpSqlExecutionError As Boolean)
'
'    IADOConnector_SuppressToShowUpSqlExecutionError = vblnSuppressToShowUpSqlExecutionError
'End Property
'
''///////////////////////////////////////////////
''/// Properties - Access Db connection by ACCESS OLE-DB provider
''///////////////////////////////////////////////
'Public Property Get ConnectingFilePath() As String
'
'    ConnectingFilePath = mobjConnectStrGenerator.ConnectingFilePath
'End Property
'
'
''///////////////////////////////////////////////
''/// Implemented operations
''///////////////////////////////////////////////
''''
'''' overrides
''''
'Private Function IADOConnector_GetRecordset(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag) As ADODB.Recordset
'
'    Set IADOConnector_GetRecordset = Me.GetRecordset(vstrSQL, venmShowUpAdoErrorOptionFlag)
'End Function
'
''''
''''
''''
'Private Function IADOConnector_ExecuteSQL(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag) As ADODB.Recordset
'
'    Set IADOConnector_ExecuteSQL = ExecuteSQL(vstrSQL, venmShowUpAdoErrorOptionFlag)
'End Function
'
''''
'''' If the ExecuteSQL method had been used at least one time, this execute ADODB.Connect.CommitTrans()
''''
'Private Sub IADOConnector_CommitTransaction()
'
'    mitfConnector.CommitTransaction
'End Sub
'
''''
'''' If the ExecuteSQL method had been used at least one time, this execute CommitTransaction before execute ADODB.Connect.Close()
''''
'Private Sub IADOConnector_CloseConnection()
'
'    Me.CloseConnection
'End Sub
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''**---------------------------------------------
''** set ADO connection string all parameters directly
''**---------------------------------------------
''''
''''
''''
'Public Sub SetODBCConnectionByDSN(ByVal vstrDSN As String)
'
'    With mobjConnectStrGenerator
'
'        If .DsnAdoConStrGenerator.DSN <> vstrDSN Then
'
'            CloseConnection
'        End If
'
'        .SetODBCConnectionWithDSN vstrDSN
'    End With
'End Sub
'
''''
'''' set ODBC connection parameters without Data Source Name (DSN) setting
''''
'Public Sub SetODBCParametersWithoutDSN(ByVal vstrDbFilePath As String)
'
'    With mobjConnectStrGenerator
'
'        If .ConnectingFilePath <> "" And .ConnectingFilePath <> vstrDbFilePath Then
'
'            CloseConnection
'        End If
'
'        .SetSQLite3OdbcConnectionWithoutDSN vstrDbFilePath
'    End With
'End Sub
'
''**---------------------------------------------
''** set ADO connection string parameters by a User-form interface
''**---------------------------------------------
''''
'''' Using a User-form, set DSN connection
''''
'''' <Argument>vstrSettingKeyName: Input - This is to be written in Windows-registry</Argument>
'''' <Argument>vstrDSN: Input</Argument>
'Public Function IsAdoConnectionContinuedAfterOdbcConnectionDsnParametersByForm(ByVal vstrSettingKeyName As String, _
'        Optional ByVal vstrDSN As String = "") As Boolean
'
'
'    IsAdoConnectionContinuedAfterOdbcConnectionDsnParametersByForm = mobjConnectStrGenerator.IsAdoConnectionContinuedAfterSqLiteOdbcConnectionDsnParametersByForm(vstrSettingKeyName, vstrDSN)
'End Function
'
''''
'''' Using a User-form, set DSN connection
''''
'''' <Argument>vstrSettingKeyName: Input - This is to be written in Windows-registry</Argument>
'''' <Argument>vstrDbFilePath: Input</Argument>
'''' <Argument>vstrDriverName: Input</Argument>
'Public Function IsAdoConnectionContinuedAfterOdbcConnectionDsnlessParametersByForm(ByVal vstrSettingKeyName As String, _
'        Optional ByVal vstrDbFilePath As String = "", _
'        Optional ByVal vstrDriverName As String = mstrSqLite3OdbcDriverName) As Boolean
'
'
'    IsAdoConnectionContinuedAfterOdbcConnectionDsnlessParametersByForm = mobjConnectStrGenerator.IsAdoConnectionContinuedAfterSqLiteOdbcConnectionDsnlessParametersByForm(vstrSettingKeyName, vstrDbFilePath, vstrDriverName)
'End Function
'
'
''''
'''' Close ADO connection
''''
'Public Sub CloseConnection()
'
'    mobjConnector.CloseConnection
'End Sub
'
'
''**---------------------------------------------
''** Use SQL after the ADO connected
''**---------------------------------------------
''''
'''' get recordset object by the Select SQL
''''
'Public Function GetRecordset(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag) As ADODB.Recordset
'
'    If mfblnConnect() Then
'
'        Set GetRecordset = mobjConnector.GetRecordset(vstrSQL, venmShowUpAdoErrorOptionFlag)
'    End If
'End Function
'
''''
''''
''''
'Public Function ExecuteSQL(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag) As ADODB.Recordset
'
'    Dim objRSet As ADODB.Recordset
'
'    Set objRSet = Nothing
'
'    If mfblnConnect() Then
'
'        Set objRSet = mobjConnector.ExecuteSQL(vstrSQL, venmShowUpAdoErrorOptionFlag)
'    End If
'
'    Set ExecuteSQL = objRSet
'End Function
'
''''
''''
''''
'Public Function IsConnected() As Boolean
'
'    IsConnected = mfblnConnect()
'End Function
'
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
''''
'''' connect Excel sheet by ADO
''''
'Private Function mfblnConnect() As Boolean
'
'    Dim blnIsConnected As Boolean
'
'    TryToConnectRDBProviderByAdoConnectionString blnIsConnected, mobjConnector, mobjConnectStrGenerator
'
'    mfblnConnect = blnIsConnected
'End Function
'
'
'
'''--VBA_Code_File--<SqlAdoConnectOdbcUTfSqLite.bas>--
'Attribute VB_Name = "SqlAdoConnectOdbcUTfSqLite"
''
''   Sanity test to output SQL results into Excel sheets for a SQLite database using ADO ODBC
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on both ADO and Excel
''       Dependent on an installed SQLite ODBC driver
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Fri,  4/Aug/2023    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Sanity tests
''///////////////////////////////////////////////
''**---------------------------------------------
''** SqLiteAdoConnector
''**---------------------------------------------
''''
'''' Create an in-line table from the only SELECT sql inside
''''
'Private Sub msubSanityTestToConnectByVirtualTableSqLiteAdoConnector()
'
'    Dim strSQL As String
'
'    With New SqLiteAdoConnector
'
'        .SetODBCParametersWithoutDSN PrepareSQLiteDbAfterCreateSampleSQLiteDbIfItDoesntExist("TestDbOfSqLiteAdoConnectorVirtual.sqlitedb")
'
'        .AllowToRecordSQLLog = True
'
'        strSQL = "SELECT 'ABC' AS A_Column, 'DEF' AS B_Column"
'
'        DebugCol GetTableCollectionFromRSet(.GetRecordset(strSQL))
'    End With
'End Sub
'
''''
'''' create sample Table about SELECT
''''
'Private Sub msubSanityTestToConnectToSampleTable01BySqLiteAdoConnector()
'
'    Dim strSQL As String, strDbPath As String
'
'
'    strDbPath = CreateSqLiteDbSampleTableByUsingCmd("TestDb01OfSqLiteAdoConnector.sqlitedb")
'
'    strSQL = "CREATE TABLE Test_Table(Col1, Col2, Col3); INSERT INTO Test_Table VALUES (1, 2, 3); INSERT INTO Test_Table VALUES (4, 5, 6); INSERT INTO Test_Table VALUES (7, 8, 9); INSERT INTO Test_Table VALUES (10, 11, 12)"
'
'    ' execute SQL by cmd.exe
'
'    ExecuteSqlInSqLiteDatabaseByCmd strSQL, strDbPath
'
'    With New SqLiteAdoConnector
'
'        .SetODBCParametersWithoutDSN strDbPath
'
'        .AllowToRecordSQLLog = True
'
'        strSQL = "SELECT * FROM Test_Table"
'
'        DebugCol GetTableCollectionFromRSet(.GetRecordset(strSQL))
'    End With
'End Sub
'
'
''''
'''' create sample Table about CREATE TABLE
''''
'''' If the SqLite database file doesn't exist, the SQLite3 ODBC Driver support the ADO ODBC connection. (The SQLite3 database system doesn't need to execute The ADOX.Catalog.Create())
''''
'Private Sub msubSanityTestToConnectToSampleTableFromNoDbFileBySqLiteAdoConnector()
'
'    Dim strSQL As String, strDbPath As String
'
'    strDbPath = GetTemporarySqLiteDataBaseDir() & "\TestDbFromNoExistedOfSqLiteAdoConnector.sqlitedb"
'
'    If FileExistsByVbaDir(strDbPath) Then
'
'        VBA.Kill strDbPath
'    End If
'
'    With New SqLiteAdoConnector
'
'        .SetODBCParametersWithoutDSN strDbPath
'
'        .AllowToRecordSQLLog = True
'
'        strSQL = "CREATE TABLE Test_Table(Col1, Col2, Col3)"
'
'        .ExecuteSQL strSQL
'
'        ' The following causes a ODBC error
''        strSQL = "INSERT INTO Test_Table VALUES (1, 2, 3); INSERT INTO Test_Table VALUES (4, 5, 6)"
''
''        .ExecuteSQL strSQL
'
'        strSQL = "INSERT INTO Test_Table VALUES (1, 2, 3)"
'
'        .ExecuteSQL strSQL
'
'        strSQL = "INSERT INTO Test_Table VALUES (4, 5, 6)"
'
'        .ExecuteSQL strSQL
'
'        strSQL = "INSERT INTO Test_Table VALUES (7, 8, 9)"
'
'        .ExecuteSQL strSQL
'
'        .CloseConnection
'
'        strSQL = "SELECT * FROM Test_Table"
'
'        DebugCol GetTableCollectionFromRSet(.GetRecordset(strSQL))
'    End With
'End Sub
'
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''''
''''
''''
'Public Sub PrepareTestTable4ColumnOfSqLiteTestingDataBaseForSqlTestsWithUsingDropTable(ByRef ritfSqLiteConnector As IADOConnector)
'
'    Dim strSQL As String
'
'    With ritfSqLiteConnector
'
'        strSQL = "DROP TABLE Test_Table"
'
'        On Error Resume Next: .ExecuteSQL strSQL, SuppressToShowUpAdoSqlErrorFlag: On Error GoTo 0
'
'        strSQL = "CREATE TABLE Test_Table(Col1, Col2, Col3, ColText)" ' "create table Test_Table (Col1 integer, Col2 integer, Col3 integer, ColText varchar)"
'
'        .ExecuteSQL strSQL, SuppressToShowUpAdoSqlErrorByExcelSheetFlag
'
'        msubAddTableDataFor04Columns ritfSqLiteConnector
'
'        .CommitTransaction
'    End With
'End Sub
'
''''
''''
''''
'Public Sub PrepareTestTable4ColumnOfSqLiteTestingDataBaseForSqlTests(ByRef ritfSqLiteConnector As IADOConnector)
'
'    Dim strSQL As String
'
'    With ritfSqLiteConnector
'
'        strSQL = "CREATE TABLE Test_Table(Col1, Col2, Col3, ColText)"
'
'        On Error Resume Next: .ExecuteSQL strSQL, SuppressToShowUpAdoSqlErrorFlag: On Error GoTo 0
'
'        strSQL = "DELETE FROM Test_Table"
'
'        .ExecuteSQL strSQL, SuppressToShowUpAdoSqlErrorByExcelSheetFlag
'
'        msubAddTableDataFor04Columns ritfSqLiteConnector
'
'        .CommitTransaction
'    End With
'End Sub
'
''''
''''
''''
'Private Sub msubAddTableDataFor04Columns(ByRef ritfSqLiteConnector As IADOConnector)
'
'    Dim strSQL As String
'
'    With ritfSqLiteConnector
'
'        strSQL = "INSERT INTO Test_Table values (1, 2, 3, 'Type1');"
'
'        .ExecuteSQL strSQL
'
'        strSQL = "INSERT INTO Test_Table values (4, 5, 6, 'Type1');"
'
'        .ExecuteSQL strSQL
'
'        strSQL = "INSERT INTO Test_Table values (7, 8, 9, 'Type2');"
'
'        .ExecuteSQL strSQL
'
'        strSQL = "INSERT INTO Test_Table values (10, 11, 12, 'Type2');"
'
'        .ExecuteSQL strSQL
'    End With
'End Sub
'
'
''''
''''
''''
'Public Sub PrepareTestTable3ColumnOfSqLiteTestingDataBaseForSqlTests(ByRef ritfSqLiteConnector As IADOConnector)
'
'    Dim strSQL As String
'
'    With ritfSqLiteConnector
'
'        strSQL = "DROP TABLE Test_Table"
'
'        On Error Resume Next: .ExecuteSQL strSQL, SuppressToShowUpAdoSqlErrorFlag: On Error GoTo 0
'
'        strSQL = "CREATE TABLE Test_Table(Col1, Col2, Col3)"
'
'        .ExecuteSQL strSQL, SuppressToShowUpAdoSqlErrorByExcelSheetFlag
'
'        strSQL = "INSERT INTO Test_Table VALUES (1, 2, 3)"
'
'        .ExecuteSQL strSQL
'
'        strSQL = "INSERT INTO Test_Table VALUES (4, 5, 6)"
'
'        .ExecuteSQL strSQL
'
'        strSQL = "INSERT INTO Test_Table VALUES (7, 8, 9)"
'
'        .ExecuteSQL strSQL
'
'        strSQL = "INSERT INTO Test_Table VALUES (10, 11, 12)"
'
'        .ExecuteSQL strSQL
'
'        .CommitTransaction
'    End With
'End Sub
'
''''
'''' create sample Table about INSERT
''''
'Private Sub msubSanityTestToConnectToSampleTable02BySqLiteAdoConnector()
'
'    Dim strSQL As String, strDbPath As String
'
'
'    strDbPath = CreateSqLiteDbSampleTableByUsingCmd("TestDb02OfSqLiteAdoConnector.sqlitedb")
'
'    strSQL = "CREATE TABLE Test_Table(Col1, Col2, Col3); INSERT INTO Test_Table VALUES (1, 2, 3); INSERT INTO Test_Table VALUES (4, 5, 6); INSERT INTO Test_Table VALUES (7, 8, 9); INSERT INTO Test_Table VALUES (10, 11, 12)"
'
'    ' execute SQL by cmd.exe
'
'    ExecuteSqlInSqLiteDatabaseByCmd strSQL, strDbPath
'
'    With New SqLiteAdoConnector
'
'        .SetODBCParametersWithoutDSN strDbPath
'
'        .AllowToRecordSQLLog = True
'
'        strSQL = "INSERT INTO Test_Table VALUES (15, 16, 17)"
'
'        .ExecuteSQL strSQL
'
'        'DebugCol GetTableCollectionFromRSet(.GetRecordset(strSQL))
'    End With
'
'
'    strSQL = "SELECT * FROM Test_Table"
'
'    Debug.Print ExecuteSqlInSqLiteDatabaseByCmd(strSQL, strDbPath)
'End Sub
'
'
''''
'''' create sample Table about UPDATE
''''
'Private Sub msubSanityTestToConnectToSampleTable03BySqLiteAdoConnector()
'
'    Dim strSQL As String, strDbPath As String
'
'    strDbPath = CreateSqLiteDbSampleTableByUsingCmd("TestDb03OfSqLiteAdoConnector.sqlitedb")
'
'    strSQL = "CREATE TABLE Test_Table(Col1, Col2, Col3); INSERT INTO Test_Table VALUES (1, 2, 3); INSERT INTO Test_Table VALUES (4, 5, 6); INSERT INTO Test_Table VALUES (7, 8, 9); INSERT INTO Test_Table VALUES (10, 11, 12)"
'
'    ' execute SQL by cmd.exe
'
'    ExecuteSqlInSqLiteDatabaseByCmd strSQL, strDbPath
'
'    With New SqLiteAdoConnector
'
'        .SetODBCParametersWithoutDSN strDbPath
'
'        .AllowToRecordSQLLog = True
'
'        strSQL = "UPDATE Test_Table SET Col1 = 31, Col2 = 32 WHERE Col1 = 4 AND Col2 = 5 AND Col3 = 6"
'
'        .ExecuteSQL strSQL
'
'        'DebugCol GetTableCollectionFromRSet(.GetRecordset(strSQL))
'    End With
'
'
'    strSQL = "SELECT * FROM Test_Table"
'
'    Debug.Print ExecuteSqlInSqLiteDatabaseByCmd(strSQL, strDbPath)
'End Sub
'
'
''''
'''' create sample Table about DELETE
''''
'Private Sub msubSanityTestToConnectToSampleTable04BySqLiteAdoConnector()
'
'    Dim strSQL As String, strDbPath As String
'
'    strDbPath = CreateSqLiteDbSampleTableByUsingCmd("TestDb04OfSqLiteAdoConnector.sqlitedb")
'
'    strSQL = "CREATE TABLE Test_Table(Col1, Col2, Col3); INSERT INTO Test_Table VALUES (1, 2, 3); INSERT INTO Test_Table VALUES (4, 5, 6); INSERT INTO Test_Table VALUES (7, 8, 9); INSERT INTO Test_Table VALUES (10, 11, 12)"
'
'    ' execute SQL by cmd.exe
'
'    ExecuteSqlInSqLiteDatabaseByCmd strSQL, strDbPath
'
'    With New SqLiteAdoConnector
'
'        .SetODBCParametersWithoutDSN strDbPath
'
'        .AllowToRecordSQLLog = True
'
'        strSQL = "DELETE FROM Test_Table WHERE Col1 = 4 AND Col2 = 5 AND Col3 = 6"
'
'        .ExecuteSQL strSQL
'
'        .CloseConnection
'
'        strSQL = "SELECT * FROM Test_Table"
'
'        DebugCol GetTableCollectionFromRSet(.GetRecordset(strSQL))
'    End With
'End Sub
'
'
''''
'''' create sample Table about DELETE 02
''''
'Private Sub msubSanityTestToConnectToSampleTable05BySqLiteAdoConnector()
'
'    Dim strSQL As String, strDbPath As String, objRSet As ADODB.Recordset
'
'    Dim objConnector As SqLiteAdoConnector
'
'
'    Set objConnector = New SqLiteAdoConnector
'
'    With objConnector
'
'        .SetODBCParametersWithoutDSN GetTemporarySqLiteDataBaseDir() & "\" & "TestDb05OfSqLiteAdoConnector.sqlitedb"
'
'        .AllowToRecordSQLLog = True
'
'        PrepareTestTable3ColumnOfSqLiteTestingDataBaseForSqlTests objConnector
'
'
'        strSQL = "SELECT * FROM Test_Table"
'
'        DebugCol GetTableCollectionFromRSet(.GetRecordset(strSQL))
'
'
'        strSQL = "DELETE FROM Test_Table WHERE Col1 = 4 AND Col2 = 5 AND Col3 = 6"
'
'        .ExecuteSQL strSQL
'
'        .CloseConnection
'
'        strSQL = "SELECT * FROM Test_Table"
'
'        Set objRSet = .GetRecordset(strSQL)
'
'        DebugCol GetTableCollectionFromRSet(objRSet)
'    End With
'End Sub
'
'
''''
''''
''''
'Private Sub msubSanityTestToConnectToSampleTable05AndListUpRecordsetProperties()
'
'    Dim strSQL As String, strDbPath As String, objRSet As ADODB.Recordset
'    Dim objProperty As ADODB.Property
'
'    With New SqLiteAdoConnector
'
'        .SetODBCParametersWithoutDSN GetTemporarySqLiteDataBaseDir() & "\" & "TestDb05OfSqLiteAdoConnector.sqlitedb"
'
'        .AllowToRecordSQLLog = True
'
'        PrepareTestTable3ColumnOfSqLiteTestingDataBaseForSqlTests .ADOConnectorInterface
'
'        strSQL = "SELECT * FROM Test_Table"
'
'        Set objRSet = .GetRecordset(strSQL)
'
'        With objRSet
'
'            'Debug.Print TypeName(.DataSource)
'
'            For Each objProperty In .Properties
'
'                With objProperty
'
'                    Debug.Print "ADO Recordset property [Name]: " & .Name & ", [Type]: " & CStr(.Type) & ", [Value]: " & CStr(.Value)
'                End With
'            Next
'        End With
'
'        DebugCol GetTableCollectionFromRSet(objRSet)
'    End With
'End Sub
'
'
''''
'''' create sample Table
''''
'Private Sub msubSanityTestToConnectToSampleTableBySqLiteAdoConnectorWithADOConnectorInterfaceWithUsingDropTable()
'
'    Dim strSQL As String, strDbPath As String, objRSet As ADODB.Recordset
'
'    Dim objConnector As SqLiteAdoConnector
'
'
'    Set objConnector = New SqLiteAdoConnector
'
'    With objConnector
'
'        .SetODBCParametersWithoutDSN GetTemporarySqLiteDataBaseDir() & "\" & "TestDb05OfSqLiteAdoConnector.sqlitedb"
'
'        .AllowToRecordSQLLog = True
'
'        PrepareTestTable4ColumnOfSqLiteTestingDataBaseForSqlTestsWithUsingDropTable objConnector
'
'        PrepareTestTable4ColumnOfSqLiteTestingDataBaseForSqlTestsWithUsingDropTable objConnector
'
'        strSQL = "SELECT * FROM Test_Table"
'
'        DebugCol GetTableCollectionFromRSet(.GetRecordset(strSQL))
'    End With
'End Sub
'
'
''''
'''' create sample Table
''''
'Private Sub msubSanityTestToConnectToSampleTableBySqLiteAdoConnectorWithADOConnectorInterface()
'
'    Dim strSQL As String, strDbPath As String, objRSet As ADODB.Recordset
'
'    Dim objConnector As SqLiteAdoConnector
'
'
'    Set objConnector = New SqLiteAdoConnector
'
'    With objConnector
'
'        .SetODBCParametersWithoutDSN GetTemporarySqLiteDataBaseDir() & "\" & "TestDb05OfSqLiteAdoConnector.sqlitedb"
'
'        .AllowToRecordSQLLog = True
'
'        PrepareTestTable4ColumnOfSqLiteTestingDataBaseForSqlTests objConnector
'
'        PrepareTestTable4ColumnOfSqLiteTestingDataBaseForSqlTests objConnector
'
'        strSQL = "SELECT * FROM Test_Table"
'
'        DebugCol GetTableCollectionFromRSet(.GetRecordset(strSQL))
'    End With
'End Sub
'''--VBA_Code_File--<ADOConStrOfOdbcSqLite.cls>--
'VERSION 1.0 CLASS
'BEGIN
'  MultiUse = -1  'True
'END
'Attribute VB_Name = "ADOConStrOfOdbcSqLite"
'Attribute VB_GlobalNameSpace = False
'Attribute VB_Creatable = False
'Attribute VB_PredeclaredId = False
'Attribute VB_Exposed = False
''
''   ADO ODBC connection string generator for SQLite3 RDB for both DSN or DSNless
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on ADO
''       Dependent on SQLite3 ODBC driver at this Windows
''       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access.
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Wed, 12/Jul/2023    Kalmclaeyd Tarclanus    Initial creation
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Interface implementations
''///////////////////////////////////////////////
'Implements IADOConnectStrGenerator
'
''///////////////////////////////////////////////
''/// Constants
''///////////////////////////////////////////////
'Private Const mstrSqLite3OdbcDriverName As String = "SQLite3 ODBC Driver"
'
''///////////////////////////////////////////////
''/// Declarations
''///////////////////////////////////////////////
'Private mobjADOConStrOfDsn As ADOConStrOfDsn
'
'Private mobjADOConStrOfDsnlessSqLite As ADOConStrOfDsnlessSqLite
'
'Private mblnUseDSN As Boolean
'
''///////////////////////////////////////////////
''/// Event handlers
''///////////////////////////////////////////////
''''
''''
''''
'Private Sub Class_Initialize()
'
'    Set mobjADOConStrOfDsn = New ADOConStrOfDsn
'
'    mobjADOConStrOfDsn.SetOdbcConnectionDestinationInformation OdbcConnectionDestinationToSQLite3
'
'    Set mobjADOConStrOfDsnlessSqLite = New ADOConStrOfDsnlessSqLite
'End Sub
'
''///////////////////////////////////////////////
''/// Properties
''///////////////////////////////////////////////
''''
''''
''''
'Public Property Get DsnAdoConStrGenerator() As ADOConStrOfDsn
'
'    Set DsnAdoConStrGenerator = mobjADOConStrOfDsn
'End Property
'
''''
''''
''''
'Public Property Get DsnlessAdoConStrGenerator() As ADOConStrOfDsnlessSqLite
'
'    Set DsnlessAdoConStrGenerator = mobjADOConStrOfDsnlessSqLite
'End Property
'
''''
''''
''''
'Public Property Get UseDSN() As Boolean
'
'    UseDSN = mblnUseDSN
'End Property
'Public Property Let UseDSN(ByVal vblnUseDSN As Boolean)
'
'    mblnUseDSN = vblnUseDSN
'End Property
'
''''
'''' read-only SQLite database path
''''
'Public Property Get ConnectingFilePath() As String
'
'    ConnectingFilePath = mobjADOConStrOfDsnlessSqLite.ConnectingFilePath
'End Property
'
''///////////////////////////////////////////////
''/// Implemented operations
''///////////////////////////////////////////////
'Private Function IADOConnectStrGenerator_GetConnectionString() As String
'
'    If mblnUseDSN Then
'
'        IADOConnectStrGenerator_GetConnectionString = mobjADOConStrOfDsn.GetConnectionString
'    Else
'        IADOConnectStrGenerator_GetConnectionString = mobjADOConStrOfDsnlessSqLite.GetConnectionString
'    End If
'End Function
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''''
''''
''''
'Public Function GetConnectionString() As String
'
'    GetConnectionString = IADOConnectStrGenerator_GetConnectionString()
'End Function
'
''**---------------------------------------------
''** Set ADODB ODBC parameters directly
''**---------------------------------------------
''''
'''' using ODBC DataSource setting (DSN)
''''
'Public Sub SetODBCConnectionWithDSN(ByVal vstrDSN As String)
'
'    mobjADOConStrOfDsn.SetODBCConnectionWithDSN vstrDSN, "", ""
'
'    mblnUseDSN = True
'End Sub
'
''''
''''
''''
'Public Sub SetSQLite3OdbcConnectionWithoutDSN(ByVal vstrDbFilePath As String)
'
'    mobjADOConStrOfDsnlessSqLite.SetSQLite3OdbcConnectionWithoutDSN vstrDbFilePath
'
'    mblnUseDSN = False
'End Sub
'
'
''**---------------------------------------------
''** By a User-form, set ADODB ODBC parameters
''**---------------------------------------------
''''
'''' use SimpleAdoConStrAuthentication.bas
''''
'''' <Argument>vstrSettingKeyName: Input - This is to be written in Windows-registry</Argument>
'''' <Argument>vstrDSN: Input</Argument>
'Public Function IsAdoConnectionContinuedAfterSqLiteOdbcConnectionDsnParametersByForm(ByVal vstrSettingKeyName As String, _
'        Optional ByVal vstrDSN As String = "") As Boolean
'
'    Dim blnIsRequestedToCancelAdoConnection As Boolean
'
'    mblnUseDSN = True
'
'    SetOdbcConnectionParametersBySqLiteForm blnIsRequestedToCancelAdoConnection, Me, vstrSettingKeyName, , , vstrDSN
'
'    IsAdoConnectionContinuedAfterSqLiteOdbcConnectionDsnParametersByForm = Not blnIsRequestedToCancelAdoConnection
'End Function
'
''''
'''' DSNless connection, with referring SimpleAdoConStrAuthentication.bas
''''
'''' <Argument>vstrSettingKeyName: Input - This is to be written in Windows-registry</Argument>
'''' <Argument>vstrDbFilePath: Input</Argument>
'''' <Argument>vstrDriverName: Input</Argument>
'Public Function IsAdoConnectionContinuedAfterSqLiteOdbcConnectionDsnlessParametersByForm(ByVal vstrSettingKeyName As String, _
'        Optional ByVal vstrDbFilePath As String = "", _
'        Optional ByVal vstrDriverName As String = mstrSqLite3OdbcDriverName) As Boolean
'
'
'    Dim blnIsRequestedToCancelAdoConnection As Boolean
'
'    mblnUseDSN = False
'
'    SetOdbcConnectionParametersBySqLiteForm blnIsRequestedToCancelAdoConnection, Me, vstrSettingKeyName, vstrDriverName, vstrDbFilePath
'
'    IsAdoConnectionContinuedAfterSqLiteOdbcConnectionDsnlessParametersByForm = Not blnIsRequestedToCancelAdoConnection
'End Function
'
'
'
'''--VBA_Code_File--<UTfAdoConnectAccDb.bas>--
'Attribute VB_Name = "UTfAdoConnectAccDb"
''
''   Serving Microsoft Access db serving tests by ADO without Access application
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on ADO
''       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Mon, 15/May/2023    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Directives
''///////////////////////////////////////////////
'#Const HAS_REF = True   ' About ADODB, ADOX
'
''///////////////////////////////////////////////
''/// Sanity tests
''///////////////////////////////////////////////
''''
''''
''''
'Private Sub msubSanityTestToCreateAccessDB()
'
'    GetPathAfterCreateSampleAccDb "TestFromVba01.accdb"
'End Sub
'
''''
'''' use SQL
''''
'Private Sub msubSanityTestToCreateAccessDBBySQL()
'
'    Dim strDbName As String, strDbPath As String, strConnectionString As String
'
'
'    strDbName = "TestFromVba01.accdb"
'
'    strDbPath = GetTemporaryAccessDataBaseFilesDirectoryPath() & "\" & strDbName
'
'    With New Scripting.FileSystemObject
'
'        If .FileExists(strDbPath) Then
'
'            .DeleteFile strDbPath
'        End If
'    End With
'
'    strConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & strDbPath & ";"
'
'    msubCreateAccdbSampleTableByUsingSQL strConnectionString
'End Sub
'
'
''''
'''' Whenever this VBA is the 32 bit mode, no error occurs
''''
'Private Sub msubSanityTestToCreateAccessDBWithMDB()
'
'    Dim strDbName As String, strDbPath As String, strConnectionString As String
'
'#If HAS_REF Then
'
'    Dim objConnection As ADODB.Connection, objCatalog As ADOX.Catalog, strSQL As String
'#Else
'    Dim objConnection As Object, objCatalog As Object, strSQL As String
'#End If
'
'
'    strDbName = "TestFromVbs01.mdb"
'
'    strDbPath = GetTemporaryAccessDataBaseFilesDirectoryPath() & "\" & strDbName
'
'    With New Scripting.FileSystemObject
'
'        If .FileExists(strDbPath) Then
'
'            .DeleteFile strDbPath
'        End If
'    End With
'
'    strConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & strDbPath & ";"
'
'    ' If X64 mode VBA, the following code causes an error.
'
'    msubCreateSampleTableByUsingRecordset strConnectionString
'End Sub
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''''
''''
''''
'Private Sub ExploreParentFolderOfTemporaryAccessDb()
'
'    ExploreParentFolderWhenOpenedThatIsNothing GetTemporaryAccessDataBaseFilesDirectoryPath()
'End Sub
'
'
'
''''
''''
''''
'Public Function PrepareAccessDbAfterCreateSampleAccDbIfItDoesntExist(ByVal vstrDbName As String) As String
'
'    Dim strDbPath As String
'
'    strDbPath = GetTemporaryAccessDataBaseFilesDirectoryPath() & "\" & vstrDbName
'
'    If Not FileExistsByVbaDir(strDbPath) Then
'
'        GetPathAfterCreateSampleAccDb vstrDbName
'    End If
'
'    PrepareAccessDbAfterCreateSampleAccDbIfItDoesntExist = strDbPath
'End Function
'
'
''''
''''
''''
'Public Function GetPathAfterCreateSampleAccDb(ByVal vstrDbName As String) As String
'
'    Dim strDbPath As String, strConnectionString As String
'
'    strDbPath = GetTemporaryAccessDataBaseFilesDirectoryPath() & "\" & vstrDbName
'
'    If FileExistsByVbaDir(strDbPath) Then
'
'        VBA.Kill strDbPath
'    End If
'
'    strConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & strDbPath & ";"
'
'    msubCreateSampleTableByUsingRecordset strConnectionString
'
'    GetPathAfterCreateSampleAccDb = strDbPath
'End Function
'
''''
''''
''''
'Public Function GetTemporaryAccessDataBaseFilesDirectoryPath() As String
'
'    Dim strDir As String
'
'    strDir = GetTemporaryDevelopmentVBARootDir() & "\DataBases"
'
'    ForceToCreateDirectory strDir
'
'    GetTemporaryAccessDataBaseFilesDirectoryPath = strDir
'End Function
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
''''
''''
''''
'Private Sub msubCreateSampleTableByUsingRecordset(ByRef rstrConnectionString As String)
'
'    Dim strTableName As String, strTableFieldTypes As String, strSQL As String
'
'#If HAS_REF Then
'
'    Dim objConnection As ADODB.Connection
'#Else
'    Dim objConnection As Object
'#End If
'
'    strTableName = "TestTable"
'
'    strTableFieldTypes = "氏名 varchar(20),身長 float"
'
'
'    With New ADOX.Catalog
'
'        ' create Access data-base
'
'        ' This cause an error in x64 bit mode VBA
'
'        Set objConnection = .Create(rstrConnectionString)
'    End With
'
'    strSQL = "create table " & strTableName & " (" & strTableFieldTypes & ");"
'
'    ' create a table
'    objConnection.Execute strSQL
'
'
'    strSQL = "select * from " & strTableName & ";"
'
'    'With CreateObject("ADODB.Recordset")
'
'    With New ADODB.Recordset
'
'        ' adLockPessimistic := 2, ADODB.CommandTypeEnum.adCmdText := 1
'
'        .Open strSQL, objConnection, adOpenForwardOnly, ADODB.LockTypeEnum.adLockPessimistic, adCmdText
'
'        ' add some records by ADODB.Recordset interface
'
'        .AddNew
'
'        .Fields("氏名").Value = "鈴木 出伊多": .Fields("身長").Value = 174.6
'
'        '.Update
'
'        .AddNew
'
'        .Fields(0).Value = "高橋 出伊多": .Fields(1).Value = 166.2
'
'        '.Update
'
'        .AddNew Array("氏名", "身長"), Array("田中 出伊多", 151.4)
'
'        .Update
'
'        .Close
'    End With
'
'    objConnection.Close
'End Sub
'
''''
''''
''''
'Private Sub msubCreateAccdbSampleTableByUsingSQL(ByRef rstrConnectionString As String)
'
'    Dim strTableName As String, strTableFieldTypes As String, strSQL As String
'
'    strTableName = "TestTable"
'
'    strTableFieldTypes = "氏名 varchar(20),身長 float"
'
'
'    With New ADOX.Catalog
'
'        ' create Access data-base
'
'        ' This cause an error in x64 bit mode VBA
'
'        With .Create(rstrConnectionString)
'
'            strSQL = "create table " & strTableName & " (" & strTableFieldTypes & ");"
'
'            ' create a table
'            .Execute strSQL
'
'            .BeginTrans
'
'            strSQL = "insert into " & strTableName & " (氏名, 身長)  values('鈴木 出伊多', 174.6);"
'            .Execute strSQL
'
'            strSQL = "insert into " & strTableName & " (氏名, 身長)  values('高橋 出伊多', 166.2);"
'            .Execute strSQL
'
'            strSQL = "insert into " & strTableName & " (氏名, 身長)  values('田中 出伊多', 151.4);"
'            .Execute strSQL
'
'            .CommitTrans
'
'            .Close
'        End With
'    End With
'End Sub
'
'''--VBA_Code_File--<ADOConStrToolsForAccDb.bas>--
'Attribute VB_Name = "ADOConStrToolsForAccDb"
''
''   Generate ADODB connection string to a Ace (.accdb) data base
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on ADO
''       Dependent on Microsoft Office Access Data Base Engine OLE DB Provider,
''       which should have been the installed when the Microsoft Office Excel software has been installed.
''       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Tue, 16/May/2023    Kalmclaeyd Tarclanus    Initial creation
''
'
'Option Explicit
'
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''''
''''
''''
'Public Function GetADODBConnectionOleDbStringToAccDb(ByVal vstrDbPath As String, _
'        Optional ByVal vstrOldTypeDbPassword As String = "", _
'        Optional ByVal venmAccessOLEDBProviderType As AccessOLEDBProviderType = AccessOLEDBProviderType.AceOLEDB_12P0) As String
'
'
'    Dim strConnectionString As String, strProvider As String
'
'    strProvider = GetAccessOLEDBProviderNameFromEnm(venmAccessOLEDBProviderType)
'
'    strConnectionString = "Provider=" & strProvider & ";Data Source=" & vstrDbPath & ";"
'
'    If vstrOldTypeDbPassword <> "" Then
'
'        strConnectionString = strConnectionString & "Jet OLEDB:Database Password=" & vstrOldTypeDbPassword & ";"
'    End If
'
'    GetADODBConnectionOleDbStringToAccDb = strConnectionString
'End Function
'
'
''**---------------------------------------------
''** for control USetAdoOleDbConStrForAccdb user-form
''**---------------------------------------------
''''
''''
''''
'Public Function GetInitialUserInputParametersDicForAdoOleDbConnectionOfMsAccessDb(Optional ByVal vstrDbPath As String = "", _
'        Optional ByVal vstrOldTypeDbPassword As String = "", _
'        Optional ByVal venmAccessOLEDBProviderType As AccessOLEDBProviderType = AccessOLEDBProviderType.AceOLEDB_12P0) As Scripting.Dictionary
'
'    Dim objDic As Scripting.Dictionary
'
'    Set objDic = New Scripting.Dictionary
'
'    With objDic
'
'        .Add mstrAdoDbFilePathKey, vstrDbPath
'
'        .Add "OLE_DB_Provider", GetAccessOLEDBProviderNameFromEnm(venmAccessOLEDBProviderType)
'
'        If vstrOldTypeDbPassword <> "" Then
'
'            .Add mstrTemporaryPdKey, vstrOldTypeDbPassword
'        End If
'
'        .Add mstrAdoConnectionTypeAndDestinationKey, GetDescriptionOfOleDbOrOdbcAdoConnectionDestinationRDBType(AdoOleDbAndMsAccessDb)
'    End With
'
'    Set GetInitialUserInputParametersDicForAdoOleDbConnectionOfMsAccessDb = objDic
'End Function
'
''''
''''
''''
'Public Sub UpdateADOConStrOfAccessOleDbToMsAccessDbFromInputDic(ByRef robjADOConStrOfOledbMsAccess As ADOConStrOfOleDbMsAccess, ByRef robjUserInputParametersDic As Scripting.Dictionary)
'
'    Dim strKey As String, strOldTypeDbPassword As String, enmAccessOLEDBProviderType As AccessOLEDBProviderType
'
'    With robjUserInputParametersDic
'
'        strKey = "OLE_DB_Provider"
'
'        enmAccessOLEDBProviderType = AceOLEDB_12P0
'
'        If .Exists(strKey) Then
'
'            enmAccessOLEDBProviderType = GetAccessOLEDBProviderEnmFromName(.Item(strKey))
'        End If
'
'        strOldTypeDbPassword = ""
'
'        If .Exists(mstrTemporaryPdKey) Then
'
'            strOldTypeDbPassword = .Item(mstrTemporaryPdKey)
'        End If
'
'        robjADOConStrOfOledbMsAccess.SetMsAccessConnection .Item(mstrAdoDbFilePathKey), strOldTypeDbPassword, enmAccessOLEDBProviderType
'    End With
'End Sub
'
''''
''''
''''
'Public Function GetADOConStrOfAccessOleDbToMsAccessDbFromInputDic(ByRef robjUserInputParametersDic As Scripting.Dictionary) As ADOConStrOfOleDbMsAccess
'
'    Dim objADOConStrOfOleDbMsAccess As ADOConStrOfOleDbMsAccess
'
'    Set objADOConStrOfOleDbMsAccess = New ADOConStrOfOleDbMsAccess
'
'    UpdateADOConStrOfAccessOleDbToMsAccessDbFromInputDic objADOConStrOfOleDbMsAccess, robjUserInputParametersDic
'
'    Set GetADOConStrOfAccessOleDbToMsAccessDbFromInputDic = objADOConStrOfOleDbMsAccess
'End Function
'
'
'
''**---------------------------------------------
''** ADO connection string parameters solution interfaces by each special form
''**---------------------------------------------
''''
''''
''''
'''' <Argument>rblnIsRequestedToCancelAdoConnection: Output</Argument>
'''' <Argument>robjADOConStrOfOledbMsAccess: Output</Argument>
'''' <Argument>vstrSettingKeyName: Input</Argument>
'''' <Argument>vstrDbFilePath: Input</Argument>
'''' <Argument>venmAccessOLEDBProviderType: Input</Argument>
'''' <Argument>vstrOldTypeDbPassword: Input</Argument>
'Public Sub SetOleDbConnectionParametersByAccessOleDbToMsAccessDbForm(ByRef rblnIsRequestedToCancelAdoConnection As Boolean, _
'        ByRef robjADOConStrOfOledbMsAccess As ADOConStrOfOleDbMsAccess, _
'        ByVal vstrSettingKeyName As String, _
'        Optional ByVal vstrDbFilePath As String = "", _
'        Optional ByVal venmAccessOLEDBProviderType As AccessOLEDBProviderType = AccessOLEDBProviderType.AceOLEDB_12P0, _
'        Optional ByVal vstrOldTypeDbPassword As String = "")
'
'    Dim objUserInputParametersDic As Scripting.Dictionary, strPassword As String, blnIsOpeningFormNeeded As Boolean
'
'    rblnIsRequestedToCancelAdoConnection = False
'
'    blnIsOpeningFormNeeded = IsOpeningFormNeededAfterLoadFromUserInputParametersDicFirstCacheAndSecondFunctionInputsOfAccessOleDbToMsAccessDb(objUserInputParametersDic, vstrSettingKeyName, vstrDbFilePath, venmAccessOLEDBProviderType, vstrOldTypeDbPassword)
'
'    If blnIsOpeningFormNeeded Then
'
'        msubSetUserInputParametersDicOfAccessOleDbToMsAccessDbByOpeningFormWithFixedSettingKeyName rblnIsRequestedToCancelAdoConnection, objUserInputParametersDic, vstrSettingKeyName
'    End If
'
'    If Not rblnIsRequestedToCancelAdoConnection Then
'
'        UpdateADOConStrOfAccessOleDbToMsAccessDbFromInputDic robjADOConStrOfOledbMsAccess, objUserInputParametersDic
'    End If
'End Sub
'
'
'
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
''''
''''
''''
'''' <Argument>rblnIsRequestedToCancelAdoConnection: Output</Argument>
'''' <Argument>robjUserInputParametersDic: Input - Output</Argument>
'''' <Argument>rstrSettingKeyName: Input</Argument>
'Private Sub msubSetUserInputParametersDicOfAccessOleDbToMsAccessDbByOpeningFormWithFixedSettingKeyName(ByRef rblnIsRequestedToCancelAdoConnection As Boolean, _
'        ByRef robjUserInputParametersDic As Scripting.Dictionary, _
'        ByRef rstrSettingKeyName As String)
'
'
'    Dim objFormStateToSetAdoConStr As FormStateToSetAdoConStr, objUSetAdoOleDbConStrForAccdb As USetAdoOleDbConStrForAccdb
'
'    Set objFormStateToSetAdoConStr = GetFormStateToSetAdoConStrByDefaultParams(robjUserInputParametersDic, rstrSettingKeyName)
'
'    Set objUSetAdoOleDbConStrForAccdb = New USetAdoOleDbConStrForAccdb
'
'    With objUSetAdoOleDbConStrForAccdb
'
'        .SetInitialFormStateToSetAdoConStr objFormStateToSetAdoConStr, True
'
'        .Show vbModal
'    End With
'
'    AfterProcessAdoConStrFormSetting rblnIsRequestedToCancelAdoConnection, robjUserInputParametersDic, objFormStateToSetAdoConStr
'End Sub
'
''''
''''
''''
'''' <Argument>robjUserInputParametersDic: Output</Argument>
'''' <Argument>vstrSettingKeyName: Input</Argument>
'''' <Argument>vstrDbFilePath: Input</Argument>
'''' <Argument>venmAccessOLEDBProviderType: Input</Argument>
'''' <Argument>vstrOldTypeDbPassword: Input</Argument>
'Private Function IsOpeningFormNeededAfterLoadFromUserInputParametersDicFirstCacheAndSecondFunctionInputsOfAccessOleDbToMsAccessDb(ByRef robjUserInputParametersDic As Scripting.Dictionary, _
'        ByVal vstrSettingKeyName As String, _
'        Optional ByVal vstrDbFilePath As String = "", _
'        Optional ByVal venmAccessOLEDBProviderType As AccessOLEDBProviderType = AccessOLEDBProviderType.AceOLEDB_12P0, _
'        Optional ByVal vstrOldTypeDbPassword As String = "") As Boolean
'
'    Dim blnIsOpeningFormNeeded As Boolean
'
'    blnIsOpeningFormNeeded = True
'
'    ' Try to get parameters from Win-registry cache
'
'    Set robjUserInputParametersDic = GetDicOfAdoConnectionStringInfoFromReadingWinRegValues(vstrSettingKeyName)
'
'    Select Case True
'
'        Case robjUserInputParametersDic Is Nothing, robjUserInputParametersDic.Count = 0
'
'            blnIsOpeningFormNeeded = IsOpeningFormNeededAfterInitializingUserInputPrametersDicOfAccessOleDbToMsAccessDb(robjUserInputParametersDic, vstrDbFilePath, venmAccessOLEDBProviderType, vstrOldTypeDbPassword)
'        Case Else
'            blnIsOpeningFormNeeded = IsOpeningFormNeededAfterTryToGetAdoConnectionPasswordFromTemporaryCache(robjUserInputParametersDic)
'    End Select
'
'    IsOpeningFormNeededAfterLoadFromUserInputParametersDicFirstCacheAndSecondFunctionInputsOfAccessOleDbToMsAccessDb = blnIsOpeningFormNeeded
'End Function
'
'
'
''''
''''
''''
'''' <Argument>robjUserInputParametersDic: Output</Argument>
'''' <Argument>vstrDbFilePath: Input</Argument>
'''' <Argument>venmAccessOLEDBProviderType: Input</Argument>
'''' <Argument>vstrOldTypeDbPassword: Input</Argument>
'Private Function IsOpeningFormNeededAfterInitializingUserInputPrametersDicOfAccessOleDbToMsAccessDb(ByRef robjUserInputParametersDic As Scripting.Dictionary, _
'        Optional ByVal vstrDbFilePath As String = "", _
'        Optional ByVal venmAccessOLEDBProviderType As AccessOLEDBProviderType = AccessOLEDBProviderType.AceOLEDB_12P0, _
'        Optional ByVal vstrOldTypeDbPassword As String = "") As Boolean
'
'
'    Dim blnIsOpeningFormNeeded As Boolean
'
'    blnIsOpeningFormNeeded = True
'
'    Set robjUserInputParametersDic = GetInitialUserInputParametersDicForAdoOleDbConnectionOfMsAccessDb(vstrDbFilePath, vstrOldTypeDbPassword, venmAccessOLEDBProviderType)
'
'    If IsADOConnectionStringEnabled(GetADOConStrOfAccessOleDbToMsAccessDbFromInputDic(robjUserInputParametersDic).GetConnectionString()) Then
'
'        blnIsOpeningFormNeeded = False
'    End If
'
'    IsOpeningFormNeededAfterInitializingUserInputPrametersDicOfAccessOleDbToMsAccessDb = blnIsOpeningFormNeeded
'End Function
'
'
''///////////////////////////////////////////////
''/// Sanity tests
''///////////////////////////////////////////////
''''
'''' delete testing SettingKeyName from the registory
''''
'Private Sub msubSanityTestToDeleteTestingSettingKeyNamesForConnectingToAccessOleDbWithMsAccessDb()
'
'    DeleteWinRegKeyValuesOfAdoConnectionStringInfo "TestAccOleDbToAccessDbKey"
'
'    DeleteWinRegKeyValuesOfAdoConnectionStringInfo "TestAccOleDbToAccessDbWithPasswordKey"
'End Sub
'
'
''''
''''
''''
'Private Sub msubSanityTestToOpenUSetAdoOleDbConStrForAccessOleDbWithMsAccessDb()
'
'    Dim objUserInputParametersDic As Scripting.Dictionary, blnIsRequestedToCancelAdoConnection As Boolean
'
'    Set objUserInputParametersDic = GetInitialUserInputParametersDicForAdoOleDbConnectionOfMsAccessDb()
'
'    msubSetUserInputParametersDicOfAccessOleDbToMsAccessDbByOpeningFormWithFixedSettingKeyName blnIsRequestedToCancelAdoConnection, objUserInputParametersDic, "TestAccOleDbToAccessDbKey"
'
'    If Not blnIsRequestedToCancelAdoConnection Then
'
'        If IsADOConnectionStringEnabled(GetADOConStrOfAccessOleDbToMsAccessDbFromInputDic(objUserInputParametersDic).GetConnectionString()) Then
'
'            Debug.Print "Connected to Ms Access Db by Ms Access OLE DB using ADO"
'        End If
'    End If
'End Sub
'
'''--VBA_Code_File--<ADOConStrOfOleDbMsAccess.cls>--
'VERSION 1.0 CLASS
'BEGIN
'  MultiUse = -1  'True
'END
'Attribute VB_Name = "ADOConStrOfOleDbMsAccess"
'Attribute VB_GlobalNameSpace = False
'Attribute VB_Creatable = False
'Attribute VB_PredeclaredId = False
'Attribute VB_Exposed = False
''
''   ADO OLE DB connection and tools for Microsoft Access RDB
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on ADO
''       Dependent on Microsoft Access OLE DB provider, which should has been installed when the Excel has installed.
''       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access.
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Thu,  8/Jun/2023    Kalmclaeyd Tarclanus    Initial creation
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Interface implementations
''///////////////////////////////////////////////
'Implements IADOConnectStrGenerator
'
''///////////////////////////////////////////////
''/// Declarations
''///////////////////////////////////////////////
'Private mobjCoreGenerator As ADOConStrOfAceOleDb
'
'Private menmAccessOLEDBProviderType As AccessOLEDBProviderType
'
'Private mstrOldTypeDbPassword As String
'
''///////////////////////////////////////////////
''/// Event Handlers
''///////////////////////////////////////////////
'Private Sub Class_Initialize()
'
'    Set mobjCoreGenerator = New ADOConStrOfAceOleDb
'
'    mstrOldTypeDbPassword = ""
'End Sub
'
'
''///////////////////////////////////////////////
''/// Implemented operations
''///////////////////////////////////////////////
'Private Function IADOConnectStrGenerator_GetConnectionString() As String
'
'    IADOConnectStrGenerator_GetConnectionString = GetConnectionStringForMsAccess()
'End Function
'
''///////////////////////////////////////////////
''/// Properties
''///////////////////////////////////////////////
''**---------------------------------------------
''** OLE DB connection
''**---------------------------------------------
''''
'''' read-only Provider Name for OLE DB
''''
'Public Property Get ProviderName() As String
'
'    ProviderName = mobjCoreGenerator.OleDbProviderName
'End Property
'
''**---------------------------------------------
''** Ms Access (.accdb) connection
''**---------------------------------------------
''''
'''' read-only Excel book path or ACCESS accdb path
''''
'Public Property Get ConnectingFilePath() As String
'
'    ConnectingFilePath = mobjCoreGenerator.ConnectingFilePath
'End Property
'
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''''
''''
''''
'Public Function GetConnectionString()
'
'    GetConnectionString = IADOConnectStrGenerator_GetConnectionString()
'End Function
'
''''
''''
''''
'Public Function GetConnectionStringForMsAccess() As String
'
'    GetConnectionStringForMsAccess = GetADODBConnectionOleDbStringToAccDb(mobjCoreGenerator.ConnectingFilePath, mstrOldTypeDbPassword, menmAccessOLEDBProviderType)
'End Function
'
''**---------------------------------------------
''** Set ADODB Access OLE DB parameters directly
''**---------------------------------------------
''''
'''' connect to CSV directory
''''
'Public Sub SetMsAccessConnection(ByVal vstrDbPath As String, _
'        Optional ByVal vstrOldTypeDbPassword As String = "", _
'        Optional ByVal venmAccessOLEDBProviderType As AccessOLEDBProviderType = AccessOLEDBProviderType.AceOLEDB_12P0)
'
'    mobjCoreGenerator.ConnectingFilePath = vstrDbPath
'
'    menmAccessOLEDBProviderType = venmAccessOLEDBProviderType
'
'    mobjCoreGenerator.OleDbProviderName = GetAccessOLEDBProviderDescriptionFromEnm(menmAccessOLEDBProviderType)
'
'    mstrOldTypeDbPassword = vstrOldTypeDbPassword
'End Sub
'
''**---------------------------------------------
''** By a User-form, set ADODB ODBC parameters
''**---------------------------------------------
''''
''''
''''
'''' <Argument>vstrSettingKeyName: Input</Argument>
'''' <Argument>vstrDbPath: Input</Argument>
'''' <Argument>venmAccessOLEDBProviderType: Input</Argument>
'''' <Argument>vstrOldTypeDbPassword: Input</Argument>
'''' <Return>Boolean</Return>
'Public Function IsAdoConnectionContinuedAfterOleDbToAccessDbConnectionParametersByForm(ByVal vstrSettingKeyName As String, _
'        Optional ByVal vstrDbPath As String = "", _
'        Optional ByVal venmAccessOLEDBProviderType As AccessOLEDBProviderType = AccessOLEDBProviderType.AceOLEDB_12P0, _
'        Optional ByVal vstrOldTypeDbPassword As String = "") As Boolean
'
'    Dim blnIsRequestedToCancelAdoConnection As Boolean
'
'    SetOleDbConnectionParametersByAccessOleDbToMsAccessDbForm blnIsRequestedToCancelAdoConnection, Me, vstrSettingKeyName, vstrDbPath, venmAccessOLEDBProviderType, vstrOldTypeDbPassword
'
'    IsAdoConnectionContinuedAfterOleDbToAccessDbConnectionParametersByForm = Not blnIsRequestedToCancelAdoConnection
'End Function
'
'
'
'
'
'
'
'''--VBA_Code_File--<AccDbAdoConnector.cls>--
'VERSION 1.0 CLASS
'BEGIN
'  MultiUse = -1  'True
'END
'Attribute VB_Name = "AccDbAdoConnector"
'Attribute VB_GlobalNameSpace = False
'Attribute VB_Creatable = False
'Attribute VB_PredeclaredId = False
'Attribute VB_Exposed = False
''
''   connect to a Microsoft Access database by the ADO Access OLE DB interface
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on ADO
''       Dependent on Microsoft Office Access Data Base Engine OLE DB Provider,
''       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access.
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Wed, 26/Jul/2023    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Interface implementations
''///////////////////////////////////////////////
'Implements IADOConnector
'
''///////////////////////////////////////////////
''/// Declarations
''///////////////////////////////////////////////
'Private mobjConnector As ADOConnector: Private mitfConnector As IADOConnector
'
'Private mobjConnectStrGenerator As ADOConStrOfOleDbMsAccess
'
''///////////////////////////////////////////////
''/// Event Handlers
''///////////////////////////////////////////////
'Private Sub Class_Initialize()
'
'    Set mobjConnector = New ADOConnector: Set mitfConnector = mobjConnector
'
'    Set mobjConnectStrGenerator = New ADOConStrOfOleDbMsAccess
'End Sub
'
'Private Sub Class_Terminate()
'
'    Me.CloseConnection
'
'    Set mitfConnector = Nothing
'
'    Set mobjConnector = Nothing
'
'    Set mobjConnectStrGenerator = Nothing
'End Sub
'
''///////////////////////////////////////////////
''/// Properties
''///////////////////////////////////////////////
''''
''''
''''
'Public Property Get ADOConnectorInterface() As IADOConnector
'
'    Set ADOConnectorInterface = Me
'End Property
'
''///////////////////////////////////////////////
''/// Properties - connoted ADO connector
''///////////////////////////////////////////////
'Public Property Let AllowToRecordSQLLog(ByVal vblnAllowToRecord As Boolean)
'
'    mobjConnector.AllowToRecordSQLLog = vblnAllowToRecord
'End Property
'Public Property Get AllowToRecordSQLLog() As Boolean
'
'    AllowToRecordSQLLog = mobjConnector.AllowToRecordSQLLog
'End Property
'
'
'Public Property Get SQLExecutionResult() As SQLResult
'
'    Set SQLExecutionResult = IADOConnector_SQLExecutionResult
'End Property
'
'Public Property Get IsSqlExecutionFailed() As Boolean
'
'    IsSqlExecutionFailed = mobjConnector.IsSqlExecutionFailed
'End Property
'
''///////////////////////////////////////////////
''/// Implemented properties
''///////////////////////////////////////////////
'Private Property Let IADOConnector_IsConnected(ByVal vblnIsConnected As Boolean)
'
'    If vblnIsConnected Then
'
'        mitfConnector.IsConnected = mfblnConnect()
'    Else
'        mobjConnector.CloseConnection
'
'        mitfConnector.IsConnected = mobjConnector.IsConnected
'    End If
'End Property
'Private Property Get IADOConnector_IsConnected() As Boolean
'
'    IADOConnector_IsConnected = mfblnConnect()
'End Property
'
'Private Property Set IADOConnector_ADOConnection(ByVal vobjADOConnection As ADODB.Connection)
'
'    Set mitfConnector.ADOConnection = vobjADOConnection
'End Property
'Private Property Get IADOConnector_ADOConnection() As ADODB.Connection
'
'    Set IADOConnector_ADOConnection = mitfConnector.ADOConnection
'End Property
'
'
'
'Private Property Set IADOConnector_SQLExecutionResult(ByVal vobjSQLResult As SQLResult)
'
'    Set mitfConnector.SQLExecutionResult = vobjSQLResult
'End Property
'Private Property Get IADOConnector_SQLExecutionResult() As SQLResult
'
'    Set IADOConnector_SQLExecutionResult = mitfConnector.SQLExecutionResult
'End Property
'
'
'Private Property Get IADOConnector_CommandTimeout() As Long
'
'    IADOConnector_CommandTimeout = mobjConnector.CommandTimeout
'End Property
'Private Property Let IADOConnector_CommandTimeout(ByVal vintTimeout As Long)
'
'    mobjConnector.CommandTimeout = vintTimeout
'End Property
'
'Private Property Get IADOConnector_SuppressToShowUpSqlExecutionError() As Boolean
'
'    IADOConnector_SuppressToShowUpSqlExecutionError = mitfConnector.SuppressToShowUpSqlExecutionError
'End Property
'Private Property Let IADOConnector_SuppressToShowUpSqlExecutionError(ByVal vblnSuppressToShowUpSqlExecutionError As Boolean)
'
'    mitfConnector.SuppressToShowUpSqlExecutionError = vblnSuppressToShowUpSqlExecutionError
'End Property
'
'Private Property Get IADOConnector_RdbConnectionInformation() As RdbConnectionInformationFlag
'
'    IADOConnector_RdbConnectionInformation = AdoOleDbToMsAccessRdbFlag
'End Property
'Private Property Let IADOConnector_RdbConnectionInformation(ByVal venmRdbConnectionInformationFlag As RdbConnectionInformationFlag)
'
'    ' Nothing to do
'End Property
'
''///////////////////////////////////////////////
''/// Properties - connoted ADOConnectionSetting
''///////////////////////////////////////////////
'Public Property Get ConnectSetting() As ADOConnectionSetting
'
'    Set ConnectSetting = mobjConnector.ConnectSetting
'End Property
'
''''
'''' unit is second. [s]
''''
'Public Property Let CommandTimeout(ByVal vintTimeout As Long)
'
'    IADOConnector_CommandTimeout = vintTimeout
'End Property
'Public Property Get CommandTimeout() As Long
'
'    CommandTimeout = IADOConnector_CommandTimeout
'End Property
'
'
'Public Property Get SuppressToShowUpSqlExecutionError() As Boolean
'
'    SuppressToShowUpSqlExecutionError = IADOConnector_SuppressToShowUpSqlExecutionError
'End Property
'Public Property Let SuppressToShowUpSqlExecutionError(ByVal vblnSuppressToShowUpSqlExecutionError As Boolean)
'
'    IADOConnector_SuppressToShowUpSqlExecutionError = vblnSuppressToShowUpSqlExecutionError
'End Property
'
''///////////////////////////////////////////////
''/// Properties - Access Db connection by ACCESS OLE-DB provider
''///////////////////////////////////////////////
'Public Property Get ConnectingFilePath() As String
'
'    ConnectingFilePath = mobjConnectStrGenerator.ConnectingFilePath
'End Property
'
'
''///////////////////////////////////////////////
''/// Implemented operations
''///////////////////////////////////////////////
''''
'''' overrides
''''
'Private Function IADOConnector_GetRecordset(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag) As ADODB.Recordset
'
'    Set IADOConnector_GetRecordset = Me.GetRecordset(vstrSQL, venmShowUpAdoErrorOptionFlag)
'End Function
'
''''
''''
''''
'Private Function IADOConnector_ExecuteSQL(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag) As ADODB.Recordset
'
'    Set IADOConnector_ExecuteSQL = ExecuteSQL(vstrSQL, venmShowUpAdoErrorOptionFlag)
'End Function
'
''''
'''' If the ExecuteSQL method had been used at least one time, this execute ADODB.Connect.CommitTrans()
''''
'Private Sub IADOConnector_CommitTransaction()
'
'    mitfConnector.CommitTransaction
'End Sub
'
''''
'''' If the ExecuteSQL method had been used at least one time, this execute CommitTransaction before execute ADODB.Connect.Close()
''''
'Private Sub IADOConnector_CloseConnection()
'
'    Me.CloseConnection
'End Sub
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''**---------------------------------------------
''** set ADO connection string all parameters directly
''**---------------------------------------------
''''
''''
''''
'Public Sub SetMsAccessConnection(ByVal vstrDbPath As String, _
'        Optional ByVal vstrOldTypeDbPassword As String = "", _
'        Optional ByVal venmAccessOLEDBProviderType As AccessOLEDBProviderType = AccessOLEDBProviderType.AceOLEDB_12P0)
'
'    With mobjConnectStrGenerator
'
'        If .ConnectingFilePath <> "" And .ConnectingFilePath <> vstrDbPath Then
'
'            Me.CloseConnection
'        End If
'
'        .SetMsAccessConnection vstrDbPath, vstrOldTypeDbPassword, venmAccessOLEDBProviderType
'    End With
'End Sub
'
''**---------------------------------------------
''** set ADO connection string parameters by a User-form interface
''**---------------------------------------------
''''
''''
''''
'''' <Argument>vstrSettingKeyName: Input</Argument>
'''' <Argument>vstrDbPath: Input</Argument>
'''' <Argument>venmAccessOLEDBProviderType: Input</Argument>
'''' <Argument>vstrOldTypeDbPassword: Input</Argument>
'''' <Return>Boolean</Return>
'Public Function IsAdoConnectionContinuedAfterAccessOleDbConnectionParametersByForm(ByVal vstrSettingKeyName As String, _
'        Optional ByVal vstrDbPath As String = "", _
'        Optional ByVal venmAccessOLEDBProviderType As AccessOLEDBProviderType = AccessOLEDBProviderType.AceOLEDB_12P0, _
'        Optional ByVal vstrOldTypeDbPassword As String = "")
'
'
'    IsAdoConnectionContinuedAfterAccessOleDbConnectionParametersByForm = mobjConnectStrGenerator.IsAdoConnectionContinuedAfterOleDbToAccessDbConnectionParametersByForm(vstrSettingKeyName, vstrDbPath, venmAccessOLEDBProviderType, vstrOldTypeDbPassword)
'End Function
'
''''
'''' Close ADO connection
''''
'Public Sub CloseConnection()
'
'    mobjConnector.CloseConnection
'End Sub
'
'
''**---------------------------------------------
''** Use SQL after the ADO connected
''**---------------------------------------------
''''
'''' get recordset object by the Select SQL
''''
'Public Function GetRecordset(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag) As ADODB.Recordset
'
'    If mfblnConnect() Then
'
'        Set GetRecordset = mobjConnector.GetRecordset(vstrSQL, venmShowUpAdoErrorOptionFlag)
'    End If
'End Function
'
''''
'''' input SQL, such as INSERT, UPDATE, DELETE
''''
'Public Function ExecuteSQL(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag) As ADODB.Recordset
'
'    Dim objRSet As ADODB.Recordset
'
'    Set objRSet = Nothing
'
'    If mfblnConnect() Then
'
'        Set objRSet = mobjConnector.ExecuteSQL(vstrSQL, venmShowUpAdoErrorOptionFlag)
'    End If
'
'    Set ExecuteSQL = objRSet
'End Function
'
''''
''''
''''
'Public Function IsConnected() As Boolean
'
'    IsConnected = mfblnConnect()
'End Function
'
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
''''
'''' connect Excel sheet by ADO
''''
'Private Function mfblnConnect() As Boolean
'
'    Dim blnIsConnected As Boolean
'
'    TryToConnectRDBProviderByAdoConnectionString blnIsConnected, mobjConnector, mobjConnectStrGenerator
'
'    mfblnConnect = blnIsConnected
'End Function
'
'
'''--VBA_Code_File--<SqlAdoConnectingUTfAccDb.bas>--
'Attribute VB_Name = "SqlAdoConnectingUTfAccDb"
''
''   Sanity test to connect Access database using ADO OLE DB
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on ADO
''       Dependent on Microsoft Office Access Data Base Engine OLE DB Provider,
''       which should have been the installed when the Microsoft Office Excel software has been installed.
''       Dependent on UTfAdoConnectAccDb.bas
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Fri,  4/Aug/2023    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
'
''///////////////////////////////////////////////
''/// Sanity tests
''///////////////////////////////////////////////
''**---------------------------------------------
''** AccDbAdoConnector
''**---------------------------------------------
''''
'''' Create an in-line table from the only SELECT sql inside
''''
'Private Sub msubSanityTestToConnectToAccDbAdoConnectorWithSqlInlineVirtualTable()
'
'    Dim strSQL As String
'
'    With New AccDbAdoConnector
'
'        ' use ADOX
'
'        .SetMsAccessConnection PrepareAccessDbAfterCreateSampleAccDbIfItDoesntExist("TestDbOfAccDbAdoSheetExpander.accdb")
'
'        .AllowToRecordSQLLog = True
'
'        ' Confirm the Recordset of the virtual-table in the SQL without reading the Access database.
'
'        strSQL = "SELECT 'ABC' AS A_Column, 'DEF' AS B_Column"
'
'        DebugCol GetTableCollectionFromRSet(.GetRecordset(strSQL))
'    End With
'End Sub
'
''''
'''' SQL test INSERT INTO
''''
'Private Sub msubSanityTestToConnectToAccDbAdoConnectorWithSqlInsert()
'
'    GetAccDbPathAfterTestingToConnectToAccDbAdoConnectorWithSqlInsert
'End Sub
'
''''
'''' SQL test DELETE
''''
'Private Sub msubSanityTestToConnectToAccDbAdoConnectorWithSqlDelete()
'
'    Dim strDbPath As String, strSQL As String
'
'    strDbPath = GetAccDbPathAfterTestingToConnectToAccDbAdoConnectorWithSqlInsert()
'
'    With New AccDbAdoConnector
'
'        .SetMsAccessConnection strDbPath
'
'        strSQL = "DELETE FROM Test_Table WHERE ColText = 'Type2'"
'
'        .ExecuteSQL strSQL
'
'        .CloseConnection
'
'        strSQL = "SELECT * FROM Test_Table"
'
'        DebugCol GetTableCollectionFromRSet(.GetRecordset(strSQL))
'    End With
'End Sub
'
''''
'''' SQL test UPDATE
''''
'Private Sub msubSanityTestToConnectToAccDbAdoConnectorWithSqlUpdate()
'
'    Dim strDbPath As String, strSQL As String
'
'    strDbPath = GetAccDbPathAfterTestingToConnectToAccDbAdoConnectorWithSqlInsert()
'
'    With New AccDbAdoConnector
'
'        .SetMsAccessConnection strDbPath
'
'        strSQL = "UPDATE Test_Table SET Col1 = 31, Col2 = 32 WHERE Col3 = 9 AND ColText = 'Type2'"
'
'        .ExecuteSQL strSQL
'
'        .CloseConnection
'
'        strSQL = "SELECT * FROM Test_Table"
'
'        DebugCol GetTableCollectionFromRSet(.GetRecordset(strSQL))
'    End With
'End Sub
'
'
''''
'''' SQL test DROP TABLE
''''
'Public Sub msubSanityTestToConnectMsAccessDbAndDropTable()
'
'    Dim strSQL As String, strDbPath As String, objADOConStrOfOleDbMsAccess As ADOConStrOfOleDbMsAccess
'
'    strDbPath = GetTemporaryAccessDataBaseFilesDirectoryPath() & "\TestDb02OfAccDbAdoConnector.accdb"
'
'    If FileExistsByVbaDir(strDbPath) Then
'
'        VBA.Kill strDbPath
'    End If
'
'    Set objADOConStrOfOleDbMsAccess = New ADOConStrOfOleDbMsAccess
'
'    objADOConStrOfOleDbMsAccess.SetMsAccessConnection strDbPath
'
'    With New ADOX.Catalog
'
'        With .Create(objADOConStrOfOleDbMsAccess.GetConnectionString())
'
'            .Close
'        End With
'    End With
'
'    With New AccDbAdoConnector
'
'        .SetMsAccessConnection strDbPath
'
'
'        strSQL = "CREATE TABLE Test_Table (Col1 Integer, Col2 Integer, Col3 Integer, ColText varchar);"
'
'        .ExecuteSQL strSQL
'
'        .SuppressToShowUpSqlExecutionError = True
'
'        strSQL = "CREATE TABLE Test_Table (Col1 Integer, Col2 Integer, Col3 Integer, ColText varchar);"
'
'        On Error Resume Next
'
'        .ExecuteSQL strSQL
'
'        On Error GoTo 0
'
'        .SuppressToShowUpSqlExecutionError = False
'
'
'        strSQL = "DROP TABLE Test_Table"
'
'        .ExecuteSQL strSQL
'
'
''        strSQL = "DROP TABLE Test_Table"
''
''        .ExecuteSQL strSQL
'
'        .CloseConnection
'    End With
'End Sub
'
'
''**---------------------------------------------
''** About Microsoft Access MSysObjects
''**---------------------------------------------
''''
'''' At default .accdb file, MSysObjects is not able to access from ODBC outside Ms Access application
''''
'Private Sub msubSanityTestToConnectToAccDbAndGetTableListBySQL()
'
'    Dim strSQL As String, strDbPath As String
'
'    strDbPath = GetTemporaryAccessDataBaseFilesDirectoryPath() & "\TestDb01OfAccDbAdoConnector.accdb"
'
'    With New AccDbAdoConnector
'
'        .SetMsAccessConnection strDbPath
'
'        strSQL = "SELECT MSysObjects.Type, MSysObjects.Name, MSysObjects.Flags From MSysObjects WHERE MSysObjects.Type = 1 AND MSysObjects.Flags = 0 ORDER BY MSysObjects.Type, MSysObjects.Name;"
'
'        ' The following occurs an error of the access authority of MSysObjects
'
'        DebugCol GetTableCollectionAsDateFromRSet(.GetRecordset(strSQL))
'
'        If Err.Number <> 0 Then
'
'            Debug.Print Err.Description
'        End If
'
'        On Error GoTo 0
'    End With
'End Sub
'
''**---------------------------------------------
''** Ms Access ADODB.Connection tests
''**---------------------------------------------
''''
'''' SQL SELECT INTO
''''
'Private Sub msubSanityTestToConnectMsAccessDbAndSelectInto()
'
'    Dim strSQL As String, strDbPath As String, objADOConStrOfOleDbMsAccess As ADOConStrOfOleDbMsAccess, blnFailedToDeleteAccDb As Boolean
'    Dim strConnectionString As String
'    Dim objConnection As ADODB.Connection, objRSet As ADODB.Recordset, intRecordsAffected As Long
'
'    strDbPath = GetTemporaryAccessDataBaseFilesDirectoryPath() & "\TestDb01.accdb"
'
'    blnFailedToDeleteAccDb = False
'
'    If FileExistsByVbaDir(strDbPath) Then
'
'        On Error Resume Next
'
'        VBA.Kill strDbPath
'
'        If Err.Number <> 0 Then
'
'            blnFailedToDeleteAccDb = True
'        End If
'
'        On Error GoTo 0
'    End If
'
'    If Not blnFailedToDeleteAccDb Then
'
'        Set objConnection = mfobjGetConnectionAfterCreateSimpleAdoDbByADOX(strDbPath)
'    End If
'
'    With objConnection
'
'        .BeginTrans
'
'        If blnFailedToDeleteAccDb Then
'
'            strSQL = "DROP TABLE Test_Table"
'
'            On Error Resume Next: .Execute strSQL: On Error GoTo 0
'        End If
'
'        strSQL = "CREATE TABLE Test_Table (Col1 Integer, Col2 Integer, Col3 Integer, ColText varchar);"
'
'        .Execute strSQL
'
'        strSQL = "INSERT INTO Test_Table VALUES (1, 2, 3, 'Type1');"
'
'        .Execute strSQL
'
'        strSQL = "INSERT INTO Test_Table VALUES (4, 5, 6, 'Type1');"
'
'        .Execute strSQL
'
'        strSQL = "INSERT INTO Test_Table VALUES (7, 8, 9, 'Type2');"
'
'        .Execute strSQL
'
'        strSQL = "INSERT INTO Test_Table VALUES (10, 11, 12, 'Type2');"
'
'        .Execute strSQL
'
'        .CommitTrans
'
'        strSQL = "SELECT * FROM Test_Table"
'
'        DebugCol GetTableCollectionFromRSet(.Execute(strSQL))
'
'        ' Test SELECT INTO
'
'        .BeginTrans
'
'        strSQL = "SELECT * INTO Test_Table02 FROM Test_Table WHERE ColText = 'Type2'"
'
'        intRecordsAffected = -1
'
'        Set objRSet = .Execute(strSQL, intRecordsAffected)
'
'        If Not objRSet Is Nothing Then
'
'            Debug.Print "ADODB.Recordset.State: " & GetADOEnumLiteralFromObjectStateEnum(objRSet.State) & ", ADODB.Connection.State: " & GetADOEnumLiteralFromObjectStateEnum(.State)
'
'            If objRSet.State <> ADODB.ObjectStateEnum.adStateClosed Then
'
'                Debug.Print "ADODB.Recordset has been got and have some data"
'
'                Debug.Print "RecordCount: " & CStr(objRSet.RecordCount)
'
'                Debug.Print "Fields.Count: " & CStr(objRSet.Fields.Count)
'
'                DebugCol GetTableCollectionFromRSet(objRSet)
'            Else
'                Debug.Print "ADODB.Recordset has been got, but it has been closed."
'            End If
'        End If
'
'        If intRecordsAffected <> -1 Then
'
'            Debug.Print "After SELECT INTO, RecordsAffected: " & CStr(intRecordsAffected)
'        End If
'
'        .CommitTrans
'
'        strSQL = "SELECT * FROM Test_Table02"
'
'        DebugCol GetTableCollectionFromRSet(.Execute(strSQL))
'
'        .Close
'    End With
'End Sub
'
''''
''''
''''
'Private Function mfobjGetConnectionAfterCreateSimpleAdoDbByADOX(ByVal vstrDbPath As String, Optional ByVal vblnCloseAdoConnectBecauseOfNoUse As Boolean = False) As ADODB.Connection
'
'    Dim objADOConStrOfOleDbMsAccess As ADOConStrOfOleDbMsAccess, objConnection As ADODB.Connection
'
'    Set objADOConStrOfOleDbMsAccess = New ADOConStrOfOleDbMsAccess
'
'    objADOConStrOfOleDbMsAccess.SetMsAccessConnection vstrDbPath
'
'    With New ADOX.Catalog
'
'        Set objConnection = .Create(objADOConStrOfOleDbMsAccess.GetConnectionString())
'
'        If vblnCloseAdoConnectBecauseOfNoUse Then
'
'            objConnection.Close
'
'            Set objConnection = Nothing
'        End If
'    End With
'
'    Set mfobjGetConnectionAfterCreateSimpleAdoDbByADOX = objConnection
'End Function
'
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''''
''''
''''
'Public Function GetAccDbPathAfterTestingToConnectToAccDbAdoConnectorWithSqlInsert() As String
'
'    Dim strSQL As String, strDbPath As String, blnFailedToDeleteAccDb As Boolean
'
'    strDbPath = GetTemporaryAccessDataBaseFilesDirectoryPath() & "\TestDb01OfAccDbAdoConnector.accdb"
'
'    blnFailedToDeleteAccDb = False
'
'    If FileExistsByVbaDir(strDbPath) Then
'
'        On Error Resume Next
'
'        VBA.Kill strDbPath
'
'        If Err.Number <> 0 Then
'
'            blnFailedToDeleteAccDb = True
'        End If
'
'        On Error GoTo 0
'    End If
'
'    If Not blnFailedToDeleteAccDb Then
'
'        mfobjGetConnectionAfterCreateSimpleAdoDbByADOX strDbPath, True
'    End If
'
'    With New AccDbAdoConnector
'
'        .SetMsAccessConnection strDbPath
'
'        If blnFailedToDeleteAccDb Then
'
'            strSQL = "DROP TABLE Test_Table"
'
'            '.SuppressToShowUpSqlExecutionError = True
'
'            On Error Resume Next: .ExecuteSQL strSQL, SuppressToShowUpAdoSqlErrorFlag: On Error GoTo 0
'
'            '.SuppressToShowUpSqlExecutionError = False
'        End If
'
'        strSQL = "CREATE TABLE Test_Table (Col1 Integer, Col2 Integer, Col3 Integer, ColText varchar);"
'
'        On Error Resume Next: .ExecuteSQL strSQL, SuppressToShowUpAdoSqlErrorFlag: On Error GoTo 0
'
'        .ADOConnectorInterface.CommitTransaction
'
'        If .IsSqlExecutionFailed Then
'
'            strSQL = "DELETE FROM Test_Table"
'
'            .ExecuteSQL strSQL
'
'            .ADOConnectorInterface.CommitTransaction
'        End If
'
'        strSQL = "INSERT INTO Test_Table VALUES (1, 2, 3, 'Type1');"
'
'        .ExecuteSQL strSQL
'
'        strSQL = "INSERT INTO Test_Table VALUES (4, 5, 6, 'Type1');"
'
'        .ExecuteSQL strSQL
'
'        strSQL = "INSERT INTO Test_Table VALUES (7, 8, 9, 'Type2');"
'
'        .ExecuteSQL strSQL
'
'        strSQL = "INSERT INTO Test_Table VALUES (10, 11, 12, 'Type2');"
'
'        .ExecuteSQL strSQL
'
'        .CloseConnection
'
'        strSQL = "SELECT * FROM Test_Table"
'
'        DebugCol GetTableCollectionFromRSet(.GetRecordset(strSQL))
'
'        .CloseConnection
'    End With
'
'    GetAccDbPathAfterTestingToConnectToAccDbAdoConnectorWithSqlInsert = strDbPath
'End Function
'
'
'
'
'''--VBA_Code_File--<UTfDaoConnectAccDb.bas>--
'Attribute VB_Name = "UTfDaoConnectAccDb"
''
''   Serving Microsoft Access db serving tests by DAO
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on both DAO and Microsoft Access
''       Dependent on UTfAdoConnectAccDb.bas
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Tue, 25/Jul/2023    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
'
''///////////////////////////////////////////////
''/// Sanity tests
''///////////////////////////////////////////////
''**---------------------------------------------
''** USetAdoOleDbConStrForAccdb form tests
''**---------------------------------------------
''''
'''' delete testing SettingKeyName from the registory
''''
'Private Sub msubSanityTestToDeleteTestingSettingKeyNamesForConnectingToAccessOleDbWithAccessDb()
'
'    DeleteWinRegKeyValuesOfAdoConnectionStringInfo "TestAccOleDbToAccessDbKey"
'
'    DeleteWinRegKeyValuesOfAdoConnectionStringInfo "TestAccOleDbToAccessDbWithPasswordKey"
'End Sub
'
''''
'''' Access Db, which is not locked by any password.
''''
'Private Sub msubSanityTestToSetOdbcConnectionParametersByAccessOleDbToAccessDbForm()
'
'    Dim blnIsRequestedToCancelAdoConnection As Boolean, objADOConStrOfOleDbMsAccess As ADOConStrOfOleDbMsAccess
'    Dim strSettingKeyName As String, strDbPath As String
'
'
'    strSettingKeyName = "TestAccOleDbToAccessDbKey"
'
'    Set objADOConStrOfOleDbMsAccess = New ADOConStrOfOleDbMsAccess
'
'    strDbPath = mfstrPrepareAccessDbPathAfterCreateSampleAccDbIfItDoesntExist()
'
'    SetOleDbConnectionParametersByAccessOleDbToMsAccessDbForm blnIsRequestedToCancelAdoConnection, objADOConStrOfOleDbMsAccess, strSettingKeyName, strDbPath
'End Sub
'
''''
'''' Connect a password locked Access db
''''
'Private Sub msubSanityTestToSetOdbcConnectionParametersByAccessOleDbToAccessDbFormWithPasswordLock()
'
'    Dim blnIsRequestedToCancelAdoConnection As Boolean, objADOConStrOfOleDbMsAccess As ADOConStrOfOleDbMsAccess
'    Dim strSettingKeyName As String, strDbPath As String
'
'
'    strSettingKeyName = "TestAccOleDbToAccessDbWithPasswordKey"
'
'    Set objADOConStrOfOleDbMsAccess = New ADOConStrOfOleDbMsAccess
'
'    strDbPath = mfstrPreparePasswordLockedAccessDbPathAfterCreateSampleAccDbIfItDoesntExist("1234")
'
'    SetOleDbConnectionParametersByAccessOleDbToMsAccessDbForm blnIsRequestedToCancelAdoConnection, objADOConStrOfOleDbMsAccess, strSettingKeyName, strDbPath, vstrOldTypeDbPassword:="1234"
'End Sub
'
''''
'''' Connect a password locked Access db
''''
'Private Sub msubSanityTestToSetOdbcConnectionParametersByAccessOleDbToAccessDbFormWithPasswordLockByOpeningForm()
'
'    Dim blnIsRequestedToCancelAdoConnection As Boolean, objADOConStrOfOleDbMsAccess As ADOConStrOfOleDbMsAccess
'    Dim strSettingKeyName As String, strDbPath As String
'
'
'    strSettingKeyName = "TestAccOleDbToAccessDbWithPasswordKey"
'
'    Set objADOConStrOfOleDbMsAccess = New ADOConStrOfOleDbMsAccess
'
'    strDbPath = mfstrPreparePasswordLockedAccessDbPathAfterCreateSampleAccDbIfItDoesntExist("1234")
'
'    SetOleDbConnectionParametersByAccessOleDbToMsAccessDbForm blnIsRequestedToCancelAdoConnection, objADOConStrOfOleDbMsAccess, strSettingKeyName, strDbPath
'End Sub
'
'
''''
''''
''''
'Private Sub msubSanityTestToCreateAccessDB()
'
'    Dim strDbPath As String
'
'    strDbPath = mfstrPreparePasswordLockedAccessDbPathAfterCreateSampleAccDbIfItDoesntExist("1234")
'End Sub
'
''**---------------------------------------------
''** About DAO SQL
''**---------------------------------------------
''''
'''' Get the Microsoft Access Table list
''''
'Private Sub msubSanityTestToConnectToAccDbAndSqlByDao01()
'
'    Dim strDbPath As String, strSQL As String
'    Dim objRSet As DAO.Recordset, objTableDef As DAO.TableDef
'
'    strDbPath = GetTemporaryAccessDataBaseFilesDirectoryPath() & "\TestDb01OfAccDbAdoConnector.accdb"
'
'    With New DAO.DBEngine
'
'        With .OpenDatabase(strDbPath, False)
'
'            strSQL = "SELECT MSysObjects.Type, MSysObjects.Name, MSysObjects.Flags From MSysObjects WHERE MSysObjects.Type = 1 AND MSysObjects.Flags = 0 ORDER BY MSysObjects.Type, MSysObjects.Name;"
'
'            ' The following occurs an error of the access authority of MSysObjects
'
'            On Error Resume Next
'
'            Set objRSet = .OpenRecordset(strSQL)
'
'            If Err.Number <> 0 Then
'
'                Debug.Print Err.Description
'            End If
'
'            On Error GoTo 0
'
'            ' However, DAT.TableDef is able to use from Microsoft Excel VBA
'
'            For Each objTableDef In .TableDefs
'
'                Debug.Print objTableDef.Name
'            Next
'
'            .Close
'        End With
'    End With
'End Sub
'
'
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
''''
''''
''''
'Private Function mfstrPreparePasswordLockedAccessDbPathAfterCreateSampleAccDbIfItDoesntExist(ByRef rstrPassword As String) As String
'
'    Dim strDbPath As String
'
'    strDbPath = PrepareAccessDbAfterCreateSampleAccDbWithLockingPasswordIfItDoesntExist("TestFromVba02WithPassword.accdb", rstrPassword)
'
'    mfstrPreparePasswordLockedAccessDbPathAfterCreateSampleAccDbIfItDoesntExist = strDbPath
'End Function
'
''''
''''
''''
'Private Function mfstrPrepareAccessDbPathAfterCreateSampleAccDbIfItDoesntExist() As String
'
'    mfstrPrepareAccessDbPathAfterCreateSampleAccDbIfItDoesntExist = PrepareAccessDbAfterCreateSampleAccDbIfItDoesntExist("TestFromVba01.accdb")
'End Function
'
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''''
''''
''''
'Public Function PrepareAccessDbAfterCreateSampleAccDbWithLockingPasswordIfItDoesntExist(ByVal vstrDbName As String, ByVal vstrPassword As String) As String
'
'    Dim strDbPath As String
'
'    strDbPath = GetTemporaryAccessDataBaseFilesDirectoryPath() & "\" & vstrDbName
'
'    If Not FileExistsByVbaDir(strDbPath) Then
'
'        GetPathAfterCreateSampleAccDb vstrDbName
'
'        SetNewAccessPasswordUsingDao strDbPath, vstrPassword
'    End If
'
'    PrepareAccessDbAfterCreateSampleAccDbWithLockingPasswordIfItDoesntExist = strDbPath
'End Function
'
''''
'''' Dependent on DAO
''''
'Private Sub SetNewAccessPasswordUsingDao(ByRef rstrDbPath As String, ByRef rstrPassword As String)
'
'    With New DAO.DBEngine
'
'        With .OpenDatabase(rstrDbPath, True)
'
'            .NewPassword "", rstrPassword
'
'            .Close
'        End With
'    End With
'End Sub
'
''''
''''
''''
'Private Sub msubSanityTestToCreateDaoDBEngine()
'
'    Dim objDBEngine As DAO.DBEngine
'
'    On Error Resume Next
'
'    Set objDBEngine = CreateObject("DAO.DBEngine")
'
'    If Err.Number <> 0 Then
'
'        ' DAO.DBEngine is not supported at CreateObject
'
'        Debug.Print Err.Description
'    Else
'        Debug.Print TypeName(objDBEngine)
'
'        Debug.Assert False
'    End If
'End Sub
'
'
'
'''--VBA_Code_File--<ADOConStrToolsForPgSql.bas>--
'Attribute VB_Name = "ADOConStrToolsForPgSql"
''
''   Generate ADODB connection string to a PostgreSQL data base
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on ADO
''       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Tue, 16/May/2023    Kalmclaeyd Tarclanus    Initial creation
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Constants
''///////////////////////////////////////////////
'#If Win64 Then
'
'    Private Const mstrDriverName As String = "{PostgreSQL Unicode(x64)}"
'#Else
'    Private Const mstrDriverName As String = "{PostgreSQL Unicode}"
'#End If
'
'Private Const mintPostgreSQLDefaultPortNumber As Long = 5432
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''''
''''
''''
'Public Function GetAdoDbConnectionOdbcDSNLessStringToPostgreSQL(ByVal vstrDriverName As String, _
'        ByVal vstrServerHostName As String, _
'        ByVal vstrDatabaseName As String, _
'        ByVal vstrUID As String, _
'        ByVal vstrPWD As String, _
'        Optional ByVal vintPortNo As Long = mintPostgreSQLDefaultPortNumber) As String
'
'    Dim strConnectionString As String
'
'    strConnectionString = "DRIVER=" & vstrDriverName & ";" _
'                        & "SERVER=" & vstrServerHostName & ";" _
'                        & "DATABASE=" & vstrDatabaseName & ";" _
'                        & "UID=" & vstrUID & ";" _
'                        & "PWD=" & vstrPWD & ";" _
'                        & "Port=" & CStr(vintPortNo) & ";"
'
'    GetAdoDbConnectionOdbcDSNLessStringToPostgreSQL = strConnectionString
'End Function
''''
''''
''''
'Public Function GetAdoDbConnectionDSNLessStringToPostgreSQLWithFixedDriverName(ByVal vstrServerHostName As String, _
'        ByVal vstrDatabaseName As String, _
'        ByVal vstrUID As String, _
'        ByVal vstrPWD As String, _
'        Optional ByVal vintPortNo As Long = mintPostgreSQLDefaultPortNumber) As String
'
'
'    GetAdoDbConnectionDSNLessStringToPostgreSQLWithFixedDriverName = GetAdoDbConnectionOdbcDSNLessStringToPostgreSQL(mstrDriverName, vstrServerHostName, vstrDatabaseName, vstrUID, vstrPWD, vintPortNo)
'End Function
'
'
''**---------------------------------------------
''** About connecting to PostgreSQL
''**---------------------------------------------
''''
'''' DSN less ODBC Oracle DB connection test
''''
'''' <Argument>UID: User ID</Argument>
'''' <Argument>PWD: Password</Argument>
'Public Function DoesODBCPostgreSQLDSNLessSettingExists(ByVal vstrServerHostName As String, _
'        ByVal vstrDatabaseName As String, _
'        ByVal vstrUID As String, _
'        ByVal vstrPWD As String) As Boolean
'
'    Dim strConnectionString As String, blnIsConnected As Boolean, strLog As String
'
'    strConnectionString = GetAdoDbConnectionDSNLessStringToPostgreSQLWithFixedDriverName(vstrServerHostName, vstrDatabaseName, vstrUID, vstrPWD)
'
'    blnIsConnected = IsADOConnectionStringEnabled(strConnectionString)
'
'    If blnIsConnected Then
'
'        strLog = "Confirmed that the PostgreSQL [" & vstrDatabaseName & "] had been connected." & vbNewLine
'
'        strLog = strLog & vbTab & "Server-host: " & vstrServerHostName & ", user name: " & vstrUID
'
'        Debug.Print strLog
'    Else
'        strLog = "Failed to connect the PostgreSQL [" & vstrDatabaseName & "]" & vbNewLine
'
'        strLog = strLog & vbTab & "Server-host: " & vstrServerHostName & ", user name: " & vstrUID
'
'        Debug.Print strLog
'    End If
'
'
'    DoesODBCPostgreSQLDSNLessSettingExists = blnIsConnected
'End Function
'
''**---------------------------------------------
''** for control USetAdoOdbcConStrForPgSql user-form
''**---------------------------------------------
''''
''''
''''
'Public Function GetInitialUserInputParametersDicForAdoDSNlessConnectionOfPostgreSQL(Optional ByVal vstrDriverName As String = mstrDriverName, _
'        Optional ByVal vstrServerHostName As String = "", _
'        Optional ByVal vstrDatabaseName As String = "", _
'        Optional ByVal vstrUserid As String = "", _
'        Optional ByVal vstrPassword As String = "", _
'        Optional ByVal vintPortNumber As Long = mintPostgreSQLDefaultPortNumber) As Scripting.Dictionary
'
'    Dim objDic As Scripting.Dictionary
'
'    Set objDic = New Scripting.Dictionary
'
'    With objDic
'
'        .Add "UseDSN", False
'
'
'        .Add "DriverName", vstrDriverName
'
'        .Add "ServerHostName", vstrServerHostName
'
'        .Add "DataBaseName", vstrDatabaseName
'
'        .Add "PortNumber", vintPortNumber
'
'
'        .Add "UserName", vstrUserid
'
'        .Add mstrTemporaryPdKey, vstrPassword
'
'        .Add mstrAdoConnectionTypeAndDestinationKey, GetDescriptionOfOdbcConnectionDestinationRDBType(OdbcConnectionDestinationToPostgreSQL)
'    End With
'
'    Set GetInitialUserInputParametersDicForAdoDSNlessConnectionOfPostgreSQL = objDic
'End Function
'
''''
''''
''''
'Public Function GetADOConStrOfOdbcPgSqlFromInputDic(ByRef robjUserInputParametersDic As Scripting.Dictionary) As ADOConStrOfOdbcPgSql
'
'    Dim objADOConStrOfOdbcPgSql As ADOConStrOfOdbcPgSql
'
'    Set objADOConStrOfOdbcPgSql = New ADOConStrOfOdbcPgSql
'
'    UpdateADOConStrOfOdbcPgSqlFromInputDic objADOConStrOfOdbcPgSql, robjUserInputParametersDic
'
'    Set GetADOConStrOfOdbcPgSqlFromInputDic = objADOConStrOfOdbcPgSql
'End Function
'
'
''''
''''
''''
'Public Sub UpdateADOConStrOfOdbcPgSqlFromInputDic(ByRef robjADOConStrOfOdbcPgSql As ADOConStrOfOdbcPgSql, ByRef robjUserInputParametersDic As Scripting.Dictionary)
'
'    Dim blnUseDSN As Boolean
'
'    blnUseDSN = robjUserInputParametersDic.Item("UseDSN")
'
'    robjADOConStrOfOdbcPgSql.UseDSN = blnUseDSN
'
'    If blnUseDSN Then
'
'        UpdateADOConStrOfDSNFromInputDic robjADOConStrOfOdbcPgSql.DsnAdoConStrGenerator, robjUserInputParametersDic
'    Else
'        UpdateADOConStrOfDsnlessPgSqlFromInputDic robjADOConStrOfOdbcPgSql.DsnlessAdoConStrGenerator, robjUserInputParametersDic
'    End If
'End Sub
'
''''
''''
''''
'Public Sub UpdateADOConStrOfDsnlessPgSqlFromInputDic(ByRef robjADOConStrOfDsnlessPgSql As ADOConStrOfDsnlessPgSql, ByRef robjUserInputParametersDic As Scripting.Dictionary)
'
'    Dim strKey As String, intPortNo As Long
'
'    With robjUserInputParametersDic
'
'        strKey = "PortNumber"
'
'        intPortNo = 0
'
'        If .Exists(strKey) Then
'
'            intPortNo = .Item(strKey)
'        End If
'
'        If intPortNo > 0 Then
'
'            robjADOConStrOfDsnlessPgSql.SetPostgreSqlOdbcConnectionWithoutDSN .Item("DriverName"), .Item("ServerHostName"), .Item("DataBaseName"), .Item("UserName"), .Item(mstrTemporaryPdKey), intPortNo
'        Else
'            robjADOConStrOfDsnlessPgSql.SetPostgreSqlOdbcConnectionWithoutDSN .Item("DriverName"), .Item("ServerHostName"), .Item("DataBaseName"), .Item("UserName"), .Item(mstrTemporaryPdKey)
'        End If
'    End With
'End Sub
'
'
''''
''''
''''
'Public Function GetADOConStrOfDsnlessPgSqlFromInputDic(ByRef robjUserInputParametersDic As Scripting.Dictionary) As ADOConStrOfDsnlessPgSql
'
'    Dim objADOConStrOfDsnlessPgSql As ADOConStrOfDsnlessPgSql
'
'    Set objADOConStrOfDsnlessPgSql = New ADOConStrOfDsnlessPgSql
'
'    UpdateADOConStrOfDsnlessPgSqlFromInputDic objADOConStrOfDsnlessPgSql, robjUserInputParametersDic
'
'    Set GetADOConStrOfDsnlessPgSqlFromInputDic = objADOConStrOfDsnlessPgSql
'End Function
'
''''
''''
''''
'Public Function GetADOConnectionOdbcPgSqlStringFromInputDic(ByRef robjUserInputParametersDic As Scripting.Dictionary) As String
'
'    Dim strConnection As String, blnUseDSN As Boolean
'
'    With robjUserInputParametersDic
'
'        blnUseDSN = .Item("UseDSN")
'
'        If blnUseDSN Then
'
'            strConnection = GetADOConStrOfDSNFromInputDic(robjUserInputParametersDic).GetConnectionString()
'        Else
'            strConnection = GetADOConStrOfDsnlessPgSqlFromInputDic(robjUserInputParametersDic).GetConnectionString()
'        End If
'    End With
'
'    GetADOConnectionOdbcPgSqlStringFromInputDic = strConnection
'End Function
'
'
''**---------------------------------------------
''** ADO connection string parameters solution interfaces by each special form
''**---------------------------------------------
''''
''''
''''
'''' <Argument>rblnIsRequestedToCancelAdoConnection: Output</Argument>
'''' <Argument>robjADOConStrOfOdbcPgSql: Output</Argument>
'''' <Argument>vstrSettingKeyName: Input</Argument>
'''' <Argument>vstrDriverName: Input</Argument>
'''' <Argument>vstrServerHostName: Input</Argument>
'''' <Argument>vstrDatabaseName: Input</Argument>
'''' <Argument>vstrUserid: Input</Argument>
'''' <Argument>vstrPassword: Input</Argument>
'''' <Argument>vintPortNumber: Input</Argument>
'''' <Argument>vstrDSN: Input</Argument>
'Public Sub SetOdbcConnectionParametersByPostgreSqlForm(ByRef rblnIsRequestedToCancelAdoConnection As Boolean, _
'        ByRef robjADOConStrOfOdbcPgSql As ADOConStrOfOdbcPgSql, _
'        ByVal vstrSettingKeyName As String, _
'        Optional ByVal vstrDriverName As String = mstrDriverName, _
'        Optional ByVal vstrServerHostName As String = "", _
'        Optional ByVal vstrDatabaseName As String = "", _
'        Optional ByVal vstrUserid As String = "", _
'        Optional ByVal vstrPassword As String = "", _
'        Optional ByVal vintPortNumber As Long = mintPostgreSQLDefaultPortNumber, _
'        Optional ByVal vstrDSN As String = "")
'
'    Dim objUserInputParametersDic As Scripting.Dictionary, strPassword As String, blnIsOpeningFormNeeded As Boolean
'
'    rblnIsRequestedToCancelAdoConnection = False
'
'    blnIsOpeningFormNeeded = IsOpeningFormNeededAfterLoadFromUserInputParametersDicFirstCacheAndSecondFunctionInputsOfPostgreSql(objUserInputParametersDic, vstrSettingKeyName, vstrDriverName, vstrServerHostName, vstrDatabaseName, vstrUserid, vstrPassword, vintPortNumber, vstrDSN)
'
'    If blnIsOpeningFormNeeded Then
'
'        msubSetUserInputParametersDicOfPostgreSqlByOpeningFormWithFixedSettingKeyName rblnIsRequestedToCancelAdoConnection, objUserInputParametersDic, vstrSettingKeyName
'    End If
'
'    If Not rblnIsRequestedToCancelAdoConnection Then
'
'        UpdateADOConStrOfOdbcPgSqlFromInputDic robjADOConStrOfOdbcPgSql, objUserInputParametersDic
'    End If
'End Sub
'
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
''''
''''
''''
'''' <Argument>rblnIsRequestedToCancelAdoConnection: Output</Argument>
'''' <Argument>robjUserInputParametersDic: Input - Output</Argument>
'''' <Argument>rstrSettingKeyName: Input</Argument>
'Private Sub msubSetUserInputParametersDicOfPostgreSqlByOpeningFormWithFixedSettingKeyName(ByRef rblnIsRequestedToCancelAdoConnection As Boolean, _
'        ByRef robjUserInputParametersDic As Scripting.Dictionary, _
'        ByRef rstrSettingKeyName As String)
'
'
'    Dim objFormStateToSetAdoConStr As FormStateToSetAdoConStr, objUSetAdoOdbcConStrForPgSql As USetAdoOdbcConStrForPgSql
'
'    Set objFormStateToSetAdoConStr = GetFormStateToSetAdoConStrByDefaultParams(robjUserInputParametersDic, rstrSettingKeyName)
'
'    Set objUSetAdoOdbcConStrForPgSql = New USetAdoOdbcConStrForPgSql
'
'    With objUSetAdoOdbcConStrForPgSql
'
'        .SetInitialFormStateToSetAdoConStr objFormStateToSetAdoConStr, True
'
'        .Show vbModal
'    End With
'
'    AfterProcessAdoConStrFormSetting rblnIsRequestedToCancelAdoConnection, robjUserInputParametersDic, objFormStateToSetAdoConStr
'End Sub
'
'
''''
''''
''''
'''' <Argument>robjUserInputParametersDic: Output</Argument>
'''' <Argument>vstrSettingKeyName: Input</Argument>
'''' <Argument>vstrDriverName: Input</Argument>
'''' <Argument>vstrServerHostName: Input</Argument>
'''' <Argument>vstrDataBaseName: Input</Argument>
'''' <Argument>vstrUserid: Input</Argument>
'''' <Argument>vstrPassword: Input</Argument>
'''' <Argument>vintPortNumber: Input</Argument>
'''' <Argument>vstrDSN: Input</Argument>
'Private Function IsOpeningFormNeededAfterLoadFromUserInputParametersDicFirstCacheAndSecondFunctionInputsOfPostgreSql(ByRef robjUserInputParametersDic As Scripting.Dictionary, _
'        ByVal vstrSettingKeyName As String, _
'        Optional ByVal vstrDriverName As String = mstrDriverName, _
'        Optional ByVal vstrServerHostName As String = "", _
'        Optional ByVal vstrDatabaseName As String = "", _
'        Optional ByVal vstrUserid As String = "", _
'        Optional ByVal vstrPassword As String = "", _
'        Optional ByVal vintPortNumber As Long = mintPostgreSQLDefaultPortNumber, _
'        Optional ByVal vstrDSN As String = "") As Boolean
'
'    Dim blnIsOpeningFormNeeded As Boolean
'
'    blnIsOpeningFormNeeded = True
'
'    ' Try to get parameters from Win-registry cache
'
'    Set robjUserInputParametersDic = GetDicOfAdoConnectionStringInfoFromReadingWinRegValues(vstrSettingKeyName)
'
'    Select Case True
'
'        Case robjUserInputParametersDic Is Nothing, robjUserInputParametersDic.Count = 0
'
'            blnIsOpeningFormNeeded = IsOpeningFormNeededAfterInitializingUserInputPrametersDicOfPostgreSql(robjUserInputParametersDic, vstrDriverName, vstrServerHostName, vstrDatabaseName, vstrUserid, vstrPassword, vintPortNumber, vstrDSN)
'        Case Else
'            blnIsOpeningFormNeeded = IsOpeningFormNeededAfterTryToGetAdoConnectionPasswordFromTemporaryCache(robjUserInputParametersDic)
'    End Select
'
'    IsOpeningFormNeededAfterLoadFromUserInputParametersDicFirstCacheAndSecondFunctionInputsOfPostgreSql = blnIsOpeningFormNeeded
'End Function
'
'
''''
''''
''''
'''' <Argument>robjUserInputParametersDic: Output</Argument>
'''' <Argument>vstrDriverName: Input</Argument>
'''' <Argument>vstrServerHostName: Input</Argument>
'''' <Argument>vstrDataBaseName: Input</Argument>
'''' <Argument>vstrUserid: Input</Argument>
'''' <Argument>vstrPassword: Input</Argument>
'''' <Argument>vintPortNumber: Input</Argument>
'''' <Argument>vstrDSN: Input</Argument>
'Private Function IsOpeningFormNeededAfterInitializingUserInputPrametersDicOfPostgreSql(ByRef robjUserInputParametersDic As Scripting.Dictionary, _
'        Optional ByVal vstrDriverName As String = mstrDriverName, _
'        Optional ByVal vstrServerHostName As String = "", _
'        Optional ByVal vstrDatabaseName As String = "", _
'        Optional ByVal vstrUserid As String = "", _
'        Optional ByVal vstrPassword As String = "", _
'        Optional ByVal vintPortNumber As Long = mintPostgreSQLDefaultPortNumber, _
'        Optional ByVal vstrDSN As String = "") As Boolean
'
'
'    Dim blnIsOpeningFormNeeded As Boolean
'
'    blnIsOpeningFormNeeded = True
'
'    If vstrDSN <> "" Then
'
'        Set robjUserInputParametersDic = GetInitialUserInputParametersDicForAdoDSNConnection(OdbcConnectionDestinationToPostgreSQL, vstrDSN, vstrUserid, vstrPassword)
'    Else
'        Set robjUserInputParametersDic = GetInitialUserInputParametersDicForAdoDSNlessConnectionOfPostgreSQL(vstrDriverName, vstrServerHostName, vstrDatabaseName, vstrUserid, vstrPassword, vintPortNumber)
'    End If
'
'    If vstrPassword <> "" Then
'
'        If IsADOConnectionStringEnabled(GetADOConnectionOdbcPgSqlStringFromInputDic(robjUserInputParametersDic)) Then
'
'            blnIsOpeningFormNeeded = False
'        End If
'    End If
'
'    IsOpeningFormNeededAfterInitializingUserInputPrametersDicOfPostgreSql = blnIsOpeningFormNeeded
'End Function
'
'
'
''///////////////////////////////////////////////
''/// Sanity tests
''///////////////////////////////////////////////
''**---------------------------------------------
''** Object creation tests
''**---------------------------------------------
''''
''''
''''
'Private Sub msubSanityTestToCreateADOConStrOfOdbcPgSql()
'
'    Dim objADOConStrOfOdbcPgSql As ADOConStrOfOdbcPgSql, objADOConStrOfDsnlessPgSql As ADOConStrOfDsnlessPgSql
'
'    Set objADOConStrOfOdbcPgSql = New ADOConStrOfOdbcPgSql
'
'    Set objADOConStrOfDsnlessPgSql = New ADOConStrOfDsnlessPgSql
'End Sub
'
'
''**---------------------------------------------
''** USetAdoOdbcConStrForPgSql form tests
''**---------------------------------------------
''''
'''' delete testing SettingKeyName from the registory
''''
'Private Sub msubSanityTestToDeleteTestingSettingKeyNamesForConnectingToPgSql()
'
'    DeleteWinRegKeyValuesOfAdoConnectionStringInfo "TestPgSqlDSNlessKey"
'
'    DeleteWinRegKeyValuesOfAdoConnectionStringInfo "TestPgSqlDSNKey"
'End Sub
'
'
''''
''''
''''
'Private Sub msubSanityTestToOpenUSetAdoOdbcConStrForPgSqlForDSNless()
'
'    Dim objUserInputParametersDic As Scripting.Dictionary, blnIsRequestedToCancelAdoConnection As Boolean
'
'    Set objUserInputParametersDic = GetInitialUserInputParametersDicForAdoDSNlessConnectionOfPostgreSQL()
'
'    msubSetUserInputParametersDicOfPostgreSqlByOpeningFormWithFixedSettingKeyName blnIsRequestedToCancelAdoConnection, objUserInputParametersDic, "TestPgSqlDSNlessKey"
'
'    If Not blnIsRequestedToCancelAdoConnection Then
'
'        If IsADOConnectionStringEnabled(GetADOConStrOfOdbcPgSqlFromInputDic(objUserInputParametersDic).GetConnectionString()) Then
'
'            Debug.Print "Connected to PostgreSQL by ADO"
'        End If
'    End If
'End Sub
'
'
''''
'''' DSNless PostgreSQL connection
''''
'Private Sub msubSanityTestToOpenUSetAdoOdbcConStrForPgSqlForDSNlessWithLongNotations()
'
'
'    Dim objUSetAdoOdbcConStrForPgSql As USetAdoOdbcConStrForPgSql
'    Dim objFormStateToSetAdoConStr As FormStateToSetAdoConStr
'
'    Set objFormStateToSetAdoConStr = New FormStateToSetAdoConStr
'
'    With objFormStateToSetAdoConStr
'
'        Set .UserInputParametersDic = GetInitialUserInputParametersDicForAdoDSNlessConnectionOfPostgreSQL()       ' A document object of a properties form
'
'        .SettingKeyName = "TestPgSqlDSNlessKey"
'    End With
'
'    Set objUSetAdoOdbcConStrForPgSql = New USetAdoOdbcConStrForPgSql
'
'    With objUSetAdoOdbcConStrForPgSql
'
'        .SetInitialFormStateToSetAdoConStr objFormStateToSetAdoConStr, True
'
'        .Show vbModal
'    End With
'
'    If Not objFormStateToSetAdoConStr.IsRequestedToCancelAdoConnection Then
'
'        msubConnectionTestForOdbcPgSql objFormStateToSetAdoConStr, True
'    End If
'End Sub
'
'
''''
'''' DSN connection to PostgreSQL
''''
'Private Sub msubSanityTestToOpenUSetAdoOdbcConStrForPgSqlForDSN()
'
'    Dim objUserInputParametersDic As Scripting.Dictionary, blnIsRequestedToCancelAdoConnection As Boolean
'
'    Set objUserInputParametersDic = GetInitialUserInputParametersDicForAdoDSNConnection(OdbcConnectionDestinationToPostgreSQL)
'
'    msubSetUserInputParametersDicOfPostgreSqlByOpeningFormWithFixedSettingKeyName blnIsRequestedToCancelAdoConnection, objUserInputParametersDic, "TestPgSqlDSNKey"
'
'    If Not blnIsRequestedToCancelAdoConnection Then
'
'        If IsADOConnectionStringEnabled(GetADOConStrOfOdbcPgSqlFromInputDic(objUserInputParametersDic).GetConnectionString()) Then
'
'            Debug.Print "Connected to PostgreSQL by ADO"
'        End If
'    End If
'End Sub
'
''''
'''' Connection to PostgreSQL with Form
''''
'Private Sub msubSanityTestToOpenUSetAdoOdbcConStrForPgSqlFromSettingCache()
'
'
'    Dim objUSetAdoOdbcConStrForPgSql As USetAdoOdbcConStrForPgSql, objFormStateToSetAdoConStr As FormStateToSetAdoConStr
'
'
'    Set objFormStateToSetAdoConStr = New FormStateToSetAdoConStr
'
'    With objFormStateToSetAdoConStr
'
'        Set .UserInputParametersDic = GetInitialUserInputParametersDicForAdoDSNConnection(OdbcConnectionDestinationToPostgreSQL)       ' A document object of a properties form
'
'        .SettingKeyName = ""
'    End With
'
'    Set objUSetAdoOdbcConStrForPgSql = New USetAdoOdbcConStrForPgSql
'
'    With objUSetAdoOdbcConStrForPgSql
'
'        .SetInitialFormStateToSetAdoConStr objFormStateToSetAdoConStr, False
'
'        .Show vbModal
'    End With
'
'    If Not objFormStateToSetAdoConStr.IsRequestedToCancelAdoConnection Then
'
'        msubConnectionTestForOdbcPgSql objFormStateToSetAdoConStr, True
'    End If
'End Sub
'
'
'
''///////////////////////////////////////////////
''/// Internal functions for sanity tests
''///////////////////////////////////////////////
'
''''
''''
''''
'Private Sub msubConnectionTestForOdbcPgSql(ByRef robjFormStateToSetAdoConStr As FormStateToSetAdoConStr, _
'        Optional ByVal vblnAllowToWriteAdoConStrInfoSettingIntoWinRegistry As Boolean = False)
'
'    If IsADOConnectionStringEnabled(GetADOConStrOfOdbcPgSqlFromInputDic(robjFormStateToSetAdoConStr.UserInputParametersDic).GetConnectionString()) Then
'
'        Debug.Print "Connected to PostgreSQL by ADO"
'
'        If vblnAllowToWriteAdoConStrInfoSettingIntoWinRegistry Then
'
'            ' Write setting to Registory
'
'            If GetAllowToSaveAdoConStrPasswordInTemporaryCache() Then
'
'                UpdateWinRegKeyValuesOfAdoConnectionStringInfoFromAdoConStrFormStateDic robjFormStateToSetAdoConStr
'            End If
'        End If
'    End If
'End Sub
'
'''--VBA_Code_File--<ADOConStrOfDsnlessPgSql.cls>--
'VERSION 1.0 CLASS
'BEGIN
'  MultiUse = -1  'True
'END
'Attribute VB_Name = "ADOConStrOfDsnlessPgSql"
'Attribute VB_GlobalNameSpace = False
'Attribute VB_Creatable = False
'Attribute VB_PredeclaredId = False
'Attribute VB_Exposed = False
''
''   ADO ODBC connection string generator for PostgreSQL RDB without DSN
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on ADO
''       Dependent on both an installed PostgreSQL and PsqlOdbc driver at this Windows
''       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access.
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Thu,  8/Jun/2023    Kalmclaeyd Tarclanus    Separated from ADOConnectStrGenerator
''       Tue, 28/Nov/2023    Kalmclaeyd Tarclanus    Introduced DbConPgSql.cls
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Interface implementations
''///////////////////////////////////////////////
'Implements IADOConnectStrGenerator
'
''///////////////////////////////////////////////
''/// Declarations
''///////////////////////////////////////////////
'
'Private mobjDbConPgSql As DbConPgSql
'
''**---------------------------------------------
''** ODBC DSNless connection
''**---------------------------------------------
'Private mstrDriverName As String    ' ODBC driver name string
'
''///////////////////////////////////////////////
''/// Event handlers
''///////////////////////////////////////////////
'Private Sub Class_Initialize()
'
'    Set mobjDbConPgSql = New DbConPgSql
'End Sub
'
''///////////////////////////////////////////////
''/// Implemented operations
''///////////////////////////////////////////////
'Private Function IADOConnectStrGenerator_GetConnectionString() As String
'
'    IADOConnectStrGenerator_GetConnectionString = GetConnectionStringForPostgreSqlODBCWithoutDSN()
'End Function
''///////////////////////////////////////////////
''/// Properties
''///////////////////////////////////////////////
''''
'''' read-only User Name - ODBC
''''
'Public Property Get UserName() As String
'
'    UserName = mobjDbConPgSql.UserName
'End Property
'
''**---------------------------------------------
''** ODBC DSNless connection
''**---------------------------------------------
''''
'''' read-only Driver Name for ODBC
''''
'Public Property Get DriverName() As String
'
'    DriverName = mstrDriverName
'End Property
'
''''
'''' read-only RDB server host name or IP address
''''
'Public Property Get ServerHostName() As String
'
'    ServerHostName = mobjDbConPgSql.ServerHostName
'End Property
'
''''
'''' read-only PostgreSQL database name
''''
'Public Property Get DataBaseName() As String
'
'    DataBaseName = mobjDbConPgSql.DataBaseName
'End Property
'
''''
'''' read-only Port number
''''
'Public Property Get PortNo() As Long
'
'    PortNo = mobjDbConPgSql.PortNo
'End Property
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''''
'''' using PostgreSQL ODBC Driver connection string
''''
'Public Sub SetPostgreSqlOdbcConnectionWithoutDSN(ByVal vstrDriverName As String, _
'        ByVal vstrServerHostName As String, _
'        ByVal vstrDatabaseName As String, _
'        ByVal vstrUserid As String, _
'        ByVal vstrPassword As String, _
'        Optional ByVal vintPortNumber As Long = 5432)
'
'    mstrDriverName = vstrDriverName ' for PostgreSQL
'
'    mobjDbConPgSql.SetPostgreSqlConnectionParameters vstrServerHostName, vstrDatabaseName, vstrUserid, vstrPassword, vintPortNumber
'End Sub
'
''''
''''
''''
'Public Function GetConnectionString() As String
'
'    GetConnectionString = IADOConnectStrGenerator_GetConnectionString()
'End Function
'
''''
'''' For PostgreSQL, DSNless connection, which ConnectionString must request PortNo
''''
'Public Function GetConnectionStringForPostgreSqlODBCWithoutDSN() As String
'
'    GetConnectionStringForPostgreSqlODBCWithoutDSN = GetAdoDbConnectionOdbcDSNLessStringToPostgreSQL(mstrDriverName, mstrServerHostName, mstrDataBaseName, mstrUID, mstrPWD, mintPortNo)
'End Function
'
'
'
'''--VBA_Code_File--<PgSqlCommonPath.bas>--
'Attribute VB_Name = "PgSqlCommonPath"
''
''   utilities to solute relative ADO PostgreSQL connection
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on PgSqlOdbcConnector.cls
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Wed, 26/Jul/2023    Kalmclaeyd Tarclanus    Redesigned
''
'
'Option Explicit
'
'
''///////////////////////////////////////////////
''/// Constants
''///////////////////////////////////////////////
'Private Const mstrModuleKey As String = "PgSqlCommonPath"   ' 1st part of registry sub-key
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''**---------------------------------------------
''** Actual implementation of IOutputBookPath
''**---------------------------------------------
''''
'''' solute directory path from registry CURRENT_USER
''''
'Public Function GetPgSqlConnectingPrearrangedLogBookPath() As String
'
'    GetPgSqlConnectingPrearrangedLogBookPath = GetCurrentBookOutputDir() & "\SQLTests\PgSqlAdoConnectingUnittests.xlsx"
'End Function
'
'
''**---------------------------------------------
''** PostgreSQL exclusive SQL
''**---------------------------------------------
''''
'''' get sheet name and SQL for both the table info and the table columns
''''
'Public Sub GetPgSqlTableInfoSQLsAndSheetNames(ByRef rstrTableInfoSheetName As String, _
'        ByRef rstrTableInfoSQL As String, _
'        ByRef rstrTableColumnsInfoSheetName As String, _
'        ByRef rstrColumnsInfoSQL As String, _
'        ByVal vstrDatabaseName As String, _
'        ByVal vstrTableName As String, _
'        Optional ByVal vstrSchemaName As String = "")
'
'    rstrTableInfoSheetName = vstrTableName & "_Info"
'
'    rstrTableInfoSQL = "select relid AS id, schemaname AS schema_name, relname AS table_name from pg_stat_user_tables where relname = '" & vstrTableName & "' ORDER BY relname"
'
'    rstrTableColumnsInfoSheetName = vstrTableName & "_Columns"
'
'    rstrColumnsInfoSQL = Me.GetSQLForPgSqlColumnsInformation(vstrDatabaseName, vstrTableName, vstrSchemaName)
'End Sub
'
'
''''
'''' get column information getting SQL for PostgreSQL
''''
'Public Function GetSQLForPgSqlColumnsInformation(ByVal vstrDatabaseName As String, _
'        ByVal vstrTableName As String, _
'        Optional ByVal vstrSchemaName As String = "") As String
'
'    Dim strSQL As String, strSQLSelect As String
'
'
'    strSQLSelect = "ordinal_position as Cln_ID, column_name AS 列名, data_type as Data_type, character_maximum_length as size"
'
'    If vstrSchemaName = "" Then
'
'        strSQL = "select " & strSQLSelect & " from information_schema.columns where table_catalog = '" & vstrDatabaseName & "' and table_name = '" & vstrTableName & "'"
'    Else
'        strSQL = "select " & strSQLSelect & " from information_schema.columns where table_catalog = '" & vstrDatabaseName & "' and table_schema = '" & vstrSchemaName & "' and table_name = '" & vstrTableName & "'"
'    End If
'
'    GetSQLForPgSqlColumnsInformation = strSQL
'End Function
'
'''--VBA_Code_File--<PgSqlDSNlessTest.bas>--
'Attribute VB_Name = "PgSqlDSNlessTest"
''
''   Simple connection test
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on an installed PostgreSQL for Windows X64
''       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Mon, 19/Jun/2023    Kalmclaeyd Tarclanus    Corrected
''
'
'Option Explicit
'
'
''///////////////////////////////////////////////
''/// Sanity tests
''///////////////////////////////////////////////
''''
'''' test code for ODBC connection error trapping
''''
'Private Sub msubSanityTestPostgreSQLODBCDSNlessErrorTrap()
'
'    ' for default connection setting soon after the PostgreSQL typcal configuration is installed in this local computer.
'
'    Debug.Print "ODBC PostgreSQL without DSN Setting: " & DoesODBCPostgreSQLDSNLessSettingExists("LocalHost", "postgres", "postgres", "postgres")
'End Sub
'''--VBA_Code_File--<PgSqlOdbcConnector.cls>--
'VERSION 1.0 CLASS
'BEGIN
'  MultiUse = -1  'True
'END
'Attribute VB_Name = "PgSqlOdbcConnector"
'Attribute VB_GlobalNameSpace = False
'Attribute VB_Creatable = False
'Attribute VB_PredeclaredId = False
'Attribute VB_Exposed = False
''
''   ADO ODBC connection and tools for PostgreSQL RDB
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on ADO
''       Dependent on both PsqlOdbc driver at this Windows and an PostgreSQL database server in either a network somewhere or the local-host
''       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access.
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Thu,  8/Jun/2023    Kalmclaeyd Tarclanus    Started to redesign
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Interface implementations
''///////////////////////////////////////////////
'Implements IADOConnector
'
''///////////////////////////////////////////////
''/// Constants
''///////////////////////////////////////////////
'#If Win64 Then
'
'    Private Const mstrDriverName As String = "{PostgreSQL Unicode(x64)}"
'#Else
'    Private Const mstrDriverName As String = "{PostgreSQL Unicode}"
'#End If
'
'Private Const mintPostgreSQLDefaultPortNumber As Long = 5432
'
'
''///////////////////////////////////////////////
''/// Declarations
''///////////////////////////////////////////////
'Private mobjConnector As ADOConnector: Private mitfConnector As IADOConnector
'
'Private mobjConnectStrGenerator As ADOConStrOfOdbcPgSql
'
''///////////////////////////////////////////////
''/// Event Handlers
''///////////////////////////////////////////////
'Private Sub Class_Initialize()
'
'    Set mobjConnector = New ADOConnector: Set mitfConnector = mobjConnector
'
'    Set mobjConnectStrGenerator = New ADOConStrOfOdbcPgSql
'End Sub
'
'Private Sub Class_Terminate()
'
'    Me.CloseConnection
'
'    Set mitfConnector = Nothing
'
'    Set mobjConnector = Nothing
'
'    Set mobjConnectStrGenerator = Nothing
'End Sub
'
''///////////////////////////////////////////////
''/// Properties
''///////////////////////////////////////////////
''''
''''
''''
'Public Property Get ADOConnectorInterface() As IADOConnector
'
'    Set ADOConnectorInterface = mitfConnector
'End Property
'
''///////////////////////////////////////////////
''/// Properties - connoted ADO connector
''///////////////////////////////////////////////
'Public Property Let AllowToRecordSQLLog(ByVal vblnAllowToRecord As Boolean)
'
'    mobjConnector.AllowToRecordSQLLog = vblnAllowToRecord
'End Property
'Public Property Get AllowToRecordSQLLog() As Boolean
'
'    AllowToRecordSQLLog = mobjConnector.AllowToRecordSQLLog
'End Property
'
'
'Public Property Get SQLExecutionResult() As SQLResult
'
'    Set SQLExecutionResult = IADOConnector_SQLExecutionResult
'End Property
'
'Public Property Get IsSqlExecutionFailed() As Boolean
'
'    IsSqlExecutionFailed = mobjConnector.IsSqlExecutionFailed
'End Property
'
''///////////////////////////////////////////////
''/// Implemented properties
''///////////////////////////////////////////////
'Private Property Let IADOConnector_IsConnected(ByVal vblnIsConnected As Boolean)
'
'    If vblnIsConnected Then
'
'        mitfConnector.IsConnected = mfblnConnect()
'    Else
'        mobjConnector.CloseConnection
'
'        mitfConnector.IsConnected = mobjConnector.IsConnected
'    End If
'End Property
'Private Property Get IADOConnector_IsConnected() As Boolean
'
'    IADOConnector_IsConnected = mfblnConnect()
'End Property
'
'Private Property Set IADOConnector_ADOConnection(ByVal vobjADOConnection As ADODB.Connection)
'
'    Set mitfConnector.ADOConnection = vobjADOConnection
'End Property
'Private Property Get IADOConnector_ADOConnection() As ADODB.Connection
'
'    Set IADOConnector_ADOConnection = mitfConnector.ADOConnection
'End Property
'
'Private Property Set IADOConnector_SQLExecutionResult(ByVal vobjSQLResult As SQLResult)
'
'    Set mitfConnector.SQLExecutionResult = vobjSQLResult
'End Property
'Private Property Get IADOConnector_SQLExecutionResult() As SQLResult
'
'    Set IADOConnector_SQLExecutionResult = mitfConnector.SQLExecutionResult
'End Property
'
'
'Private Property Get IADOConnector_CommandTimeout() As Long
'
'    IADOConnector_CommandTimeout = mobjConnector.CommandTimeout
'End Property
'Private Property Let IADOConnector_CommandTimeout(ByVal vintTimeout As Long)
'
'    mobjConnector.CommandTimeout = vintTimeout
'End Property
'
'Private Property Get IADOConnector_SuppressToShowUpSqlExecutionError() As Boolean
'
'    IADOConnector_SuppressToShowUpSqlExecutionError = mitfConnector.SuppressToShowUpSqlExecutionError
'End Property
'Private Property Let IADOConnector_SuppressToShowUpSqlExecutionError(ByVal vblnSuppressToShowUpSqlExecutionError As Boolean)
'
'    mitfConnector.SuppressToShowUpSqlExecutionError = vblnSuppressToShowUpSqlExecutionError
'End Property
'
'Private Property Get IADOConnector_RdbConnectionInformation() As RdbConnectionInformationFlag
'
'    IADOConnector_RdbConnectionInformation = AdoOdbcToPgSqlRdbFlag
'End Property
'Private Property Let IADOConnector_RdbConnectionInformation(ByVal venmRdbConnectionInformationFlag As RdbConnectionInformationFlag)
'
'    ' Nothing to do
'End Property
'
''///////////////////////////////////////////////
''/// Properties - connoted ADOConnectionSetting
''///////////////////////////////////////////////
'Public Property Get ConnectSetting() As ADOConnectionSetting
'
'    Set ConnectSetting = mobjConnector.ConnectSetting
'End Property
'
''''
'''' unit is second. [s]
''''
'Public Property Let CommandTimeout(ByVal vintTimeout As Long)
'
'    IADOConnector_CommandTimeout = vintTimeout
'End Property
'Public Property Get CommandTimeout() As Long
'
'    CommandTimeout = IADOConnector_CommandTimeout
'End Property
'
'
'Public Property Get SuppressToShowUpSqlExecutionError() As Boolean
'
'    SuppressToShowUpSqlExecutionError = IADOConnector_SuppressToShowUpSqlExecutionError
'End Property
'Public Property Let SuppressToShowUpSqlExecutionError(ByVal vblnSuppressToShowUpSqlExecutionError As Boolean)
'
'    IADOConnector_SuppressToShowUpSqlExecutionError = vblnSuppressToShowUpSqlExecutionError
'End Property
'
'
''///////////////////////////////////////////////
''/// Implemented operations
''///////////////////////////////////////////////
''''
'''' overrides
''''
'Private Function IADOConnector_GetRecordset(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag) As ADODB.Recordset
'
'    Set IADOConnector_GetRecordset = Me.GetRecordset(vstrSQL, venmShowUpAdoErrorOptionFlag)
'End Function
'
''''
''''
''''
'Private Function IADOConnector_ExecuteSQL(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag) As ADODB.Recordset
'
'    Set IADOConnector_ExecuteSQL = ExecuteSQL(vstrSQL, venmShowUpAdoErrorOptionFlag)
'End Function
'
''''
'''' If the ExecuteSQL method had been used at least one time, this execute ADODB.Connect.CommitTrans()
''''
'Private Sub IADOConnector_CommitTransaction()
'
'    mitfConnector.CommitTransaction
'End Sub
'
''''
'''' If the ExecuteSQL method had been used at least one time, this execute CommitTransaction before execute ADODB.Connect.Close()
''''
'Private Sub IADOConnector_CloseConnection()
'
'    Me.CloseConnection
'End Sub
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''**---------------------------------------------
''** set ADO connection string all parameters directly
''**---------------------------------------------
''''
'''' set ODBC connection parameters by a registered Data Source Name (DSN)
''''
'Public Sub SetODBCParametersByDSN(ByVal vstrDSN As String, ByVal vstrUID As String, ByVal vstrPWD As String)
'
'    mobjConnectStrGenerator.SetODBCConnectionWithDSN vstrDSN, vstrUID, vstrPWD
'End Sub
'
''''
'''' set ODBC connection parameters without Data Source Name (DSN) setting
''''
'Public Sub SetODBCParametersWithoutDSN(ByVal vstrDriverName As String, ByVal vstrServerHostName As String, ByVal vstrDatabaseName As String, ByVal vstrUserid As String, ByVal vstrPassword As String, Optional ByVal vintPortNumber As Long = mintPostgreSQLDefaultPortNumber)
'
'    mobjConnectStrGenerator.SetPostgreSqlOdbcConnectionWithoutDSN vstrDriverName, vstrServerHostName, vstrDatabaseName, vstrUserid, vstrPassword, vintPortNumber
'End Sub
'
''**---------------------------------------------
''** set ADO connection string parameters by a User-form interface
''**---------------------------------------------
''''
'''' Using a User-form, set DSN connection
''''
'''' <Argument>vstrSettingKeyName: Input - This is to be written in Windows-registry</Argument>
'''' <Argument>vstrDSN: Input</Argument>
'''' <Argument>vstrUID: Input</Argument>
'''' <Argument>vstrPWD: Input</Argument>
'Public Function IsAdoConnectionContinuedAfterOdbcConnectionDsnParametersByForm(ByVal vstrSettingKeyName As String, _
'        Optional ByVal vstrDSN As String = "", _
'        Optional ByVal vstrUID As String = "", _
'        Optional ByVal vstrPWD As String = "") As Boolean
'
'
'    IsAdoConnectionContinuedAfterOdbcConnectionDsnParametersByForm = mobjConnectStrGenerator.IsAdoConnectionContinuedAfterPgSqlOdbcConnectionDsnParametersByForm(vstrSettingKeyName, vstrDSN, vstrUID, vstrPWD)
'End Function
'
''''
'''' Using a User-form, set DSN connection
''''
'''' <Argument>vstrSettingKeyName: Input - This is to be written in Windows-registry</Argument>
'''' <Argument>vstrDriverName: Input</Argument>
'''' <Argument>vstrServerHostName: Input</Argument>
'''' <Argument>vstrDatabaseName: Input</Argument>
'''' <Argument>vstrUserid: Input</Argument>
'''' <Argument>vstrPassword: Input</Argument>
'''' <Argument>vintPortNumber: Input</Argument>
'Public Function IsAdoConnectionContinuedAfterOdbcConnectionDsnlessParametersByForm(ByVal vstrSettingKeyName As String, _
'        Optional ByVal vstrDriverName As String = mstrDriverName, _
'        Optional ByVal vstrServerHostName As String = "", _
'        Optional ByVal vstrDatabaseName As String = "", _
'        Optional ByVal vstrUserid As String = "", _
'        Optional ByVal vstrPassword As String = "", _
'        Optional ByVal vintPortNumber As Long = mintPostgreSQLDefaultPortNumber) As Boolean
'
'
'    IsAdoConnectionContinuedAfterOdbcConnectionDsnlessParametersByForm = mobjConnectStrGenerator.IsAdoConnectionContinuedAfterPgSqlOdbcConnectionDsnlessParametersByForm(vstrSettingKeyName, vstrDriverName, vstrServerHostName, vstrDatabaseName, vstrUserid, vstrPassword, vintPortNumber)
'End Function
'
''''
'''' Close ADO connection
''''
'Public Sub CloseConnection()
'
'    mobjConnector.CloseConnection
'End Sub
'
''**---------------------------------------------
''** Use SQL after the ADO connected
''**---------------------------------------------
''''
'''' get recordset object by the Select SQL
''''
'Public Function GetRecordset(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag) As ADODB.Recordset
'
'    If mfblnConnect() Then
'
'        Set GetRecordset = mobjConnector.GetRecordset(vstrSQL, venmShowUpAdoErrorOptionFlag)
'    End If
'End Function
'
''''
'''' input SQL, such as INSERT, UPDATE, DELETE
''''
'Public Function ExecuteSQL(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag) As ADODB.Recordset
'
'    Dim objRSet As ADODB.Recordset
'
'    Set objRSet = Nothing
'
'    If mfblnConnect() Then
'
'        Set objRSet = mobjConnector.ExecuteSQL(vstrSQL, venmShowUpAdoErrorOptionFlag)
'    End If
'
'    Set ExecuteSQL = objRSet
'End Function
'
'
''''
''''
''''
'Public Function IsConnected() As Boolean
'
'    IsConnected = mfblnConnect()
'End Function
'
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
''''
'''' connect Excel sheet by ADO
''''
'Private Function mfblnConnect() As Boolean
'
'    Dim blnIsConnected As Boolean
'
'    TryToConnectRDBProviderByAdoConnectionString blnIsConnected, mobjConnector, mobjConnectStrGenerator
'
'    mfblnConnect = blnIsConnected
'End Function
'''--VBA_Code_File--<SqlAdoConnectOdbcUTfPgSql.bas>--
'Attribute VB_Name = "SqlAdoConnectOdbcUTfPgSql"
''
''   Sanity test to connect a PostgreSQL database using ADO ODBC
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on ADO
''       Dependent on both PsqlOdbc driver at this Windows and an PostgreSQL database server in either a network somewhere or the local-host
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Fri,  4/Aug/2023    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Constants
''///////////////////////////////////////////////
'#If Win64 Then
'
'    Private Const mstrDriverName As String = "{PostgreSQL Unicode(x64)}"
'#Else
'    Private Const mstrDriverName As String = "{PostgreSQL Unicode}"
'#End If
'
'Private Const mintPostgreSQLDefaultPortNumber As Long = 5432
'
''///////////////////////////////////////////////
''/// Sanity tests
''///////////////////////////////////////////////
''**---------------------------------------------
''** PgSqlOdbcConnector
''**---------------------------------------------
''''
'''' Create an in-line table from the only SELECT sql inside
''''
'Private Sub msubSanityTestToConnectByVirtualTablePgSqlOdbcConnector()
'
'    Dim strSQL As String
'
'    With New PgSqlOdbcConnector
'
'        ' for default connection setting soon after the PostgreSQL typcal configuration is installed in this local computer.
'
'        .SetODBCParametersWithoutDSN mstrDriverName, "LocalHost", "postgres", "postgres", "postgres"
'
'        .AllowToRecordSQLLog = True
'
'        strSQL = "select 'ABC' as A_Column, 'DEF' as B_Column"
'
'        ' Confirm the Recordset of the virtual-table in the SQL without reading CSV file.
'
'        DebugCol GetTableCollectionFromRSet(.GetRecordset(strSQL))
'    End With
'End Sub
'
''**---------------------------------------------
''** Test dependent on outside VB module
''**---------------------------------------------
''''
'''' table list
''''
'Private Sub msubSanityTestToConnectToPgSqlByTableInfo()
'
'    Dim strSQL As String, objConnector As PgSqlOdbcConnector
'
'    ' You must implement 'GetCurrentThisSytemTestingPgSqlOdbcConnector' function before you execute the following.
'
'    Set objConnector = mfobjGetPgSqlOdbcConnectorWhichIncludesConnectionInfo()
'
'    If Not objConnector Is Nothing Then
'
'        With objConnector
'
'            strSQL = "select schemaname, tablename, tableowner from pg_tables"
'
'            DebugCol GetTableCollectionFromRSet(.GetRecordset(strSQL))
'
'            .CloseConnection
'        End With
'    Else
'        Debug.Print "Attention!!!: the 'GetCurrentThisSytemTestingPgSqlOdbcConnector' function hasn't been implemented yet."
'    End With
'End Sub
'
'
''**---------------------------------------------
''** Simple SQL command tests
''**---------------------------------------------
''''
'''' SQL Test for create table
''''
'Private Sub msubSanityTestToConnectToPgSqlBySqlCreateTable()
'
'    Dim strSQL As String, objConnector As PgSqlOdbcConnector
'
'    ' You must implement 'GetCurrentThisSytemTestingPgSqlOdbcConnector' function before you execute the following.
'
'    Set objConnector = mfobjGetPgSqlOdbcConnectorWhichIncludesConnectionInfo()
'
'    If Not objConnector Is Nothing Then
'
'        With objConnector
'
'            strSQL = "create table Test_Table (Col1 integer, Col2 integer, Col3 integer, ColText varchar)"
'
'            On Error Resume Next: .ExecuteSQL strSQL, SuppressToShowUpAdoSqlErrorFlag: On Error GoTo 0
'
'            .CloseConnection
'        End With
'    Else
'        Debug.Print "Attention!!!: the 'GetCurrentThisSytemTestingPgSqlOdbcConnector' function hasn't been implemented yet."
'    End If
'End Sub
'
''''
'''' SQL Test for drop table
''''
'Private Sub msubSanityTestToConnectToPgSqlBySqlDropTable()
'
'    Dim strSQL As String, objConnector As PgSqlOdbcConnector
'
'    ' You must implement 'GetCurrentThisSytemTestingPgSqlOdbcConnector' function before you execute the following.
'
'    Set objConnector = mfobjGetPgSqlOdbcConnectorWhichIncludesConnectionInfo()
'
'    If Not objConnector Is Nothing Then
'
'        With objConnector
'
'            strSQL = "drop table Test_Table"
'
'            On Error Resume Next: .ExecuteSQL strSQL, SuppressToShowUpAdoSqlErrorFlag: On Error GoTo 0
'
'            .CloseConnection
'        End With
'    Else
'        Debug.Print "Attention!!!: the 'GetCurrentThisSytemTestingPgSqlOdbcConnector' function hasn't been implemented yet."
'    End If
'End Sub
'
''''
'''' SQL Test for insert
''''
'Private Sub msubSanityTestToConnectToPgSqlBySqlInsert()
'
'    Dim strSQL As String, objConnector As PgSqlOdbcConnector
'
'    ' You must implement 'GetCurrentThisSytemTestingPgSqlOdbcConnector' function before you execute the following.
'
'    Set objConnector = mfobjGetPgSqlOdbcConnectorAfterPrepareTestTableOfPostgreSQLTestingDataBaseForSqlTests()
'
'    If Not objConnector Is Nothing Then
'
'        With objConnector
'
'            strSQL = "insert into Test_Table values (13, 15, 16, 'Type3')"
'
'            .ExecuteSQL strSQL
'
'            .CloseConnection
'
'            strSQL = "select * from Test_Table"
'
'            DebugCol GetTableCollectionFromRSet(.GetRecordset(strSQL))
'        End With
'    Else
'        Debug.Print "Attention!!!: the 'GetCurrentThisSytemTestingPgSqlOdbcConnector' function hasn't been implemented yet."
'    End If
'End Sub
'
''''
'''' SQL Test for delete
''''
'Private Sub msubSanityTestToConnectToPgSqlBySqlDelete()
'
'    Dim strSQL As String, objConnector As PgSqlOdbcConnector
'
'    ' You must implement 'GetCurrentThisSytemTestingPgSqlOdbcConnector' function before you execute the following.
'
'    Set objConnector = mfobjGetPgSqlOdbcConnectorAfterPrepareTestTableOfPostgreSQLTestingDataBaseForSqlTests()
'
'    If Not objConnector Is Nothing Then
'
'        With objConnector
'
'            strSQL = "delete from Test_Table where ColText = 'Type2'"
'
'            .ExecuteSQL strSQL
'
'            .CloseConnection
'
'            strSQL = "select * from Test_Table"
'
'            DebugCol GetTableCollectionFromRSet(.GetRecordset(strSQL))
'        End With
'    Else
'        Debug.Print "Attention!!!: the 'GetCurrentThisSytemTestingPgSqlOdbcConnector' function hasn't been implemented yet."
'    End If
'End Sub
'
''''
'''' SQL Test for update
''''
'Private Sub msubSanityTestToConnectToPgSqlBySqlUpdate()
'
'    Dim strSQL As String, objConnector As PgSqlOdbcConnector
'
'    ' You must implement 'GetCurrentThisSytemTestingPgSqlOdbcConnector' function before you execute the following.
'
'    Set objConnector = mfobjGetPgSqlOdbcConnectorAfterPrepareTestTableOfPostgreSQLTestingDataBaseForSqlTests()
'
'    If Not objConnector Is Nothing Then
'
'        With objConnector
'
'            strSQL = "update Test_Table set Col1 = 51, Col2 = 52 where Col3 = 9 and ColText = 'Type2'"
'
'            .ExecuteSQL strSQL
'
'            .CloseConnection
'
'            strSQL = "select * from Test_Table"
'
'            DebugCol GetTableCollectionFromRSet(.GetRecordset(strSQL))
'        End With
'    Else
'        Debug.Print "Attention!!!: the 'GetCurrentThisSytemTestingPgSqlOdbcConnector' function hasn't been implemented yet."
'    End If
'End Sub
'
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
''''
''''
''''
'Private Function mfobjGetPgSqlOdbcConnectorAfterPrepareTestTableOfPostgreSQLTestingDataBaseForSqlTests() As PgSqlOdbcConnector
'
'    Dim strSQL As String, objConnector As PgSqlOdbcConnector
'
'    ' You must implement 'GetCurrentThisSytemTestingPgSqlOdbcConnector' function before you execute the following.
'
'    Set objConnector = mfobjGetPgSqlOdbcConnectorWhichIncludesConnectionInfo()
'
'    If Not objConnector Is Nothing Then
'
'        PrepareTestTableOfPostgreSQLTestingDataBaseForSqlTests objConnector
'    Else
'        Debug.Print "Attention!!!: the 'GetCurrentThisSytemTestingPgSqlOdbcConnector' function hasn't been implemented yet."
'    End If
'
'    Set mfobjGetPgSqlOdbcConnectorAfterPrepareTestTableOfPostgreSQLTestingDataBaseForSqlTests = objConnector
'End Function
'
'
'
''''
'''' You must prepare the 'GetCurrentThisSytemTestingPgSqlOdbcConnector' public function outside this VB code module before execute this module sanity tests
''''
'Private Function mfobjGetPgSqlOdbcConnectorWhichIncludesConnectionInfo() As PgSqlOdbcConnector
'
'    Dim objPgSqlOdbcConnector As PgSqlOdbcConnector
'
'    On Error Resume Next
'
'    Set objPgSqlOdbcConnector = Application.Run("GetCurrentThisSytemTestingPgSqlOdbcConnector")
'
'    On Error GoTo 0
'
'    Set mfobjGetPgSqlOdbcConnectorWhichIncludesConnectionInfo = objPgSqlOdbcConnector
'End Function
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''''
''''
''''
'Public Sub PrepareTestTableOfPostgreSQLTestingDataBaseForSqlTests(ByRef ritfPgSqlConnector As IADOConnector)
'
'    Dim strSQL As String
'
'    With ritfPgSqlConnector
'
'        strSQL = "create table Test_Table (Col1 integer, Col2 integer, Col3 integer, ColText varchar)"
'
'        On Error Resume Next: .ExecuteSQL strSQL, SuppressToShowUpAdoSqlErrorFlag: On Error GoTo 0
'
'        strSQL = "truncate table Test_Table"
'
'        .ExecuteSQL strSQL, SuppressToShowUpAdoSqlErrorByExcelSheetFlag
'
'        strSQL = "insert into Test_Table values (1, 2, 3, 'Type1');"
'
'        .ExecuteSQL strSQL
'
'        strSQL = "insert into Test_Table values (4, 5, 6, 'Type1');"
'
'        .ExecuteSQL strSQL
'
'        strSQL = "insert into Test_Table values (7, 8, 9, 'Type2');"
'
'        .ExecuteSQL strSQL
'
'        strSQL = "insert into Test_Table values (10, 11, 12, 'Type2');"
'
'        .ExecuteSQL strSQL
'
'        .CommitTransaction
'    End With
'End Sub
'
'
'
'''--VBA_Code_File--<ADOConStrOfOdbcPgSql.cls>--
'VERSION 1.0 CLASS
'BEGIN
'  MultiUse = -1  'True
'END
'Attribute VB_Name = "ADOConStrOfOdbcPgSql"
'Attribute VB_GlobalNameSpace = False
'Attribute VB_Creatable = False
'Attribute VB_PredeclaredId = False
'Attribute VB_Exposed = False
''
''   ADO ODBC connection string generator for PostgreSQL RDB for both DSN or DSNless
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on ADO
''       Dependent on psqlODBC at this Windows
''       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access.
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Wed, 12/Jul/2023    Kalmclaeyd Tarclanus    Initial creation
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Interface implementations
''///////////////////////////////////////////////
'Implements IADOConnectStrGenerator
'
''///////////////////////////////////////////////
''/// Constants
''///////////////////////////////////////////////
'#If Win64 Then
'
'    Private Const mstrDriverName As String = "{PostgreSQL Unicode(x64)}"
'#Else
'    Private Const mstrDriverName As String = "{PostgreSQL Unicode}"
'#End If
'
'Private Const mintPostgreSQLDefaultPortNumber As Long = 5432
'
''///////////////////////////////////////////////
''/// Declarations
''///////////////////////////////////////////////
'Private mobjADOConStrOfDsn As ADOConStrOfDsn
'
'Private mobjADOConStrOfDsnlessPgSql As ADOConStrOfDsnlessPgSql
'
'Private mblnUseDSN As Boolean
'
'
''///////////////////////////////////////////////
''/// Event handlers
''///////////////////////////////////////////////
''''
''''
''''
'Private Sub Class_Initialize()
'
'    Set mobjADOConStrOfDsn = New ADOConStrOfDsn
'
'    mobjADOConStrOfDsn.SetOdbcConnectionDestinationInformation OdbcConnectionDestinationToPostgreSQL
'
'    Set mobjADOConStrOfDsnlessPgSql = New ADOConStrOfDsnlessPgSql
'End Sub
'
'
''///////////////////////////////////////////////
''/// Properties
''///////////////////////////////////////////////
''''
''''
''''
'Public Property Get DsnAdoConStrGenerator() As ADOConStrOfDsn
'
'    Set DsnAdoConStrGenerator = mobjADOConStrOfDsn
'End Property
'
''''
''''
''''
'Public Property Get DsnlessAdoConStrGenerator() As ADOConStrOfDsnlessPgSql
'
'    Set DsnlessAdoConStrGenerator = mobjADOConStrOfDsnlessPgSql
'End Property
'
''''
''''
''''
'Public Property Get UseDSN() As Boolean
'
'    UseDSN = mblnUseDSN
'End Property
'Public Property Let UseDSN(ByVal vblnUseDSN As Boolean)
'
'    mblnUseDSN = vblnUseDSN
'End Property
'
'
''///////////////////////////////////////////////
''/// Implemented operations
''///////////////////////////////////////////////
'Private Function IADOConnectStrGenerator_GetConnectionString() As String
'
'    If mblnUseDSN Then
'
'        IADOConnectStrGenerator_GetConnectionString = mobjADOConStrOfDsn.GetConnectionString
'    Else
'        IADOConnectStrGenerator_GetConnectionString = mobjADOConStrOfDsnlessPgSql.GetConnectionString
'    End If
'End Function
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''''
''''
''''
'Public Function GetConnectionString() As String
'
'    GetConnectionString = IADOConnectStrGenerator_GetConnectionString()
'End Function
'
''**---------------------------------------------
''** Set ADODB ODBC parameters directly
''**---------------------------------------------
''''
'''' using ODBC DataSource setting (DSN)
''''
'Public Sub SetODBCConnectionWithDSN(ByVal vstrDSN As String, ByVal vstrUID As String, ByVal vstrPWD As String)
'
'    mobjADOConStrOfDsn.SetODBCConnectionWithDSN vstrDSN, vstrUID, vstrPWD
'
'    mblnUseDSN = True
'End Sub
'
''''
''''
''''
'Public Sub SetPostgreSqlOdbcConnectionWithoutDSN(ByVal vstrDriverName As String, _
'        ByVal vstrServerHostName As String, _
'        ByVal vstrDatabaseName As String, _
'        ByVal vstrUserid As String, _
'        ByVal vstrPassword As String, _
'        Optional ByVal vintPortNumber As Long = mintPostgreSQLDefaultPortNumber)
'
'    mobjADOConStrOfDsnlessPgSql.SetPostgreSqlOdbcConnectionWithoutDSN vstrDriverName, vstrServerHostName, vstrDatabaseName, vstrUserid, vstrPassword, vintPortNumber
'
'    mblnUseDSN = False
'End Sub
'
'
''**---------------------------------------------
''** By a User-form, set ADODB ODBC parameters
''**---------------------------------------------
''''
'''' use SimpleAdoConStrAuthentication.bas
''''
'''' <Argument>vstrSettingKeyName: Input - This is to be written in Windows-registry</Argument>
'''' <Argument>vstrDSN: Input</Argument>
'''' <Argument>vstrUID: Input</Argument>
'''' <Argument>vstrPWD: Input</Argument>
'Public Function IsAdoConnectionContinuedAfterPgSqlOdbcConnectionDsnParametersByForm(ByVal vstrSettingKeyName As String, _
'        Optional ByVal vstrDSN As String = "", _
'        Optional ByVal vstrUID As String = "", _
'        Optional ByVal vstrPWD As String = "") As Boolean
'
'    Dim blnIsRequestedToCancelAdoConnection As Boolean
'
'    mblnUseDSN = True
'
'    SetOdbcConnectionParametersByPostgreSqlForm blnIsRequestedToCancelAdoConnection, Me, vstrSettingKeyName, "", "", "", vstrUID, vstrPWD, , vstrDSN
'
'    IsAdoConnectionContinuedAfterPgSqlOdbcConnectionDsnParametersByForm = Not blnIsRequestedToCancelAdoConnection
'End Function
'
''''
'''' DSNless connection, with referring SimpleAdoConStrAuthentication.bas
''''
'''' <Argument>vstrSettingKeyName: Input - This is to be written in Windows-registry</Argument>
'''' <Argument>vstrDriverName: Input</Argument>
'''' <Argument>vstrServerHostName: Input</Argument>
'''' <Argument>vstrDatabaseName: Input</Argument>
'''' <Argument>vstrUserid: Input</Argument>
'''' <Argument>vstrPassword: Input</Argument>
'''' <Argument>vintPortNumber: Input</Argument>
'Public Function IsAdoConnectionContinuedAfterPgSqlOdbcConnectionDsnlessParametersByForm(ByVal vstrSettingKeyName As String, _
'        Optional ByVal vstrDriverName As String = mstrDriverName, _
'        Optional ByVal vstrServerHostName As String = "", _
'        Optional ByVal vstrDatabaseName As String = "", _
'        Optional ByVal vstrUserid As String = "", _
'        Optional ByVal vstrPassword As String = "", _
'        Optional ByVal vintPortNumber As Long = mintPostgreSQLDefaultPortNumber) As Boolean
'
'
'    Dim blnIsRequestedToCancelAdoConnection As Boolean
'
'    mblnUseDSN = False
'
'    SetOdbcConnectionParametersByPostgreSqlForm blnIsRequestedToCancelAdoConnection, Me, vstrSettingKeyName, vstrDriverName, vstrServerHostName, vstrDatabaseName, vstrUserid, vstrPassword, vintPortNumber
'
'    IsAdoConnectionContinuedAfterPgSqlOdbcConnectionDsnlessParametersByForm = Not blnIsRequestedToCancelAdoConnection
'End Function
'
'''--VBA_Code_File--<ConvertOraclePlSqlAndVba.bas>--
'Attribute VB_Name = "ConvertOraclePlSqlAndVba"
''
''   About Oracle PL/SQL string simple conversions
''
''   It has no enough functions yet.
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Oracle PL/SQL sources
''       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access.
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2024 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Sun, 21/Jan/2024    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''''
''''
''''
'Public Function FilterOraPlSqlForADO(ByRef rstrSQL As String) As String
'
'    Dim strSQL As String, varLine As Variant, strLine As String
'    Dim intP1 As Long, strOutSql As String
'
'    Const strLineComment As String = "--"
'
'    Const strTabReplacing As String = "    "
'
'
'    strOutSql = ""
'
'    For Each varLine In Split(rstrSQL, vbNewLine)
'
'        ' About PL/SQL line comment
'
'        intP1 = InStr(1, varLine, strLineComment)
'
'        If intP1 > 0 Then
'
'            strLine = Left(varLine, intP1 - 1)
'        Else
'            strLine = varLine
'        End If
'
'        ' About tab
'        strLine = Replace(strLine, vbTab, strTabReplacing)
'
'        If strOutSql <> "" Then
'
'            strOutSql = strOutSql & vbNewLine
'        End If
'
'        strOutSql = strOutSql & strLine
'    Next
'
'    FilterOraPlSqlForADO = strOutSql
'End Function
'
'
'
''///////////////////////////////////////////////
''/// Sanity tests
''///////////////////////////////////////////////
'''--VBA_Code_File--<ADOConStrOfDsnlessOracle.cls>--
'VERSION 1.0 CLASS
'BEGIN
'  MultiUse = -1  'True
'END
'Attribute VB_Name = "ADOConStrOfDsnlessOracle"
'Attribute VB_GlobalNameSpace = False
'Attribute VB_Creatable = False
'Attribute VB_PredeclaredId = False
'Attribute VB_Exposed = False
''
''   ADO connection string generator for Oracle RDB without DSN
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on ADO
''       Dependent on both an installed Oracle and Oracle ODBC driver at this Windows
''       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access.
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   Modification History:
''       Thu,  8/Jun/2023    Kalmclaeyd Tarclanus    Separated from ADOConnectStrGenerator
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Interface implementations
''///////////////////////////////////////////////
'Implements IADOConnectStrGenerator
'
''///////////////////////////////////////////////
''/// Declarations
''///////////////////////////////////////////////
'Private mstrUID As String   ' User ID
'
'Private mstrPWD As String   ' Password
'
''**---------------------------------------------
''** ODBC DSNless connection
''**---------------------------------------------
'Private mstrDriverName As String    ' ODBC driver name string
'
'Private mstrServerHostName As String
'
'Private mstrNetworkServiceName As String    ' Network service name, for Oracle server
'
'Private mintPortNo As Long  ' connection TCP/IP port number, it is ordinary omitted, for Oracle 1521, for PostgreSQL 5432
'
'
''///////////////////////////////////////////////
''/// Implemented operations
''///////////////////////////////////////////////
'Private Function IADOConnectStrGenerator_GetConnectionString() As String
'
'    IADOConnectStrGenerator_GetConnectionString = GetConnectionStringForOracleODBCWithoutDSN()
'End Function
''///////////////////////////////////////////////
''/// Properties
''///////////////////////////////////////////////
''''
'''' read-only User Name - ODBC
''''
'Public Property Get UserName() As String
'
'    UserName = mstrUID
'End Property
'
''**---------------------------------------------
''** ODBC DSNless connection
''**---------------------------------------------
''''
'''' read-only Driver Name for ODBC
''''
'Public Property Get DriverName() As String
'
'    DriverName = mstrDriverName
'End Property
'
''''
'''' read-only RDB server host name or IP address
''''
'Public Property Get ServerHostName() As String
'
'    ServerHostName = mstrServerHostName
'End Property
'
'
''''
'''' read-only Oracle network-service name
''''
'Public Property Get NetworkServiceName() As String
'
'    NetworkServiceName = mstrNetworkServiceName
'End Property
'
''''
'''' read-only Port number
''''
'Public Property Get PortNo() As Long
'
'    PortNo = mintPortNo
'End Property
'
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''''
'''' using Oracle ODBC Driver connection string
''''
'Public Sub SetOracleOdbcConnectionWithoutDSN(ByVal vstrDriverName As String, _
'        ByVal vstrServerHostName As String, _
'        ByVal vstrNetworkServiceName As String, _
'        ByVal vstrUserid As String, _
'        ByVal vstrPassword As String, _
'        Optional ByVal vintPortNumber As Long = 1521)
'
'
'    mstrDriverName = vstrDriverName ' for Oracle
'
'    mstrServerHostName = vstrServerHostName
'
'    mstrNetworkServiceName = vstrNetworkServiceName
'
'
'    mstrUID = vstrUserid   ' User ID
'
'    mstrPWD = vstrPassword   ' Password
'
'    mintPortNo = vintPortNo
'End Sub
'
''''
''''
''''
'Public Function GetConnectionString()
'
'    GetConnectionString = IADOConnectStrGenerator_GetConnectionString()
'End Function
'
''''
'''' For Oracle, DSNless connection
''''
'Public Function GetConnectionStringForOracleODBCWithoutDSN() As String
'
'    GetConnectionStringForOracleODBCWithoutDSN = GetAdoDbConnectionOdbcDSNLessStringToOracle(mstrDriverName, mstrServerHostName, mstrNetworkServiceName, mstrUID, mstrPWD)
'
''    GetConnectionStringForOracleODBCWithoutDSN = "DRIVER=" & mstrDriverName & _
''            ";SERVER=" & mstrServerHostName & _
''            ";DBQ=" & mstrNetworkServiceName & _
''            ";UID=" & mstrUID & ";PWD=" & mstrPWD & ";"
'End Function
'
'
'
'''--VBA_Code_File--<ADOConStrOfOleDbOracle.cls>--
'VERSION 1.0 CLASS
'BEGIN
'  MultiUse = -1  'True
'END
'Attribute VB_Name = "ADOConStrOfOleDbOracle"
'Attribute VB_GlobalNameSpace = False
'Attribute VB_Creatable = False
'Attribute VB_PredeclaredId = False
'Attribute VB_Exposed = False
''
''   ADO connection string generator for Oracle RDB with Oracle OLE DB interface
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on ADO
''       Dependent on both an installed Oracle and Oracle OLE DB provider at this Windows
''       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access.
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   Modification History:
''       Thu,  8/Jun/2023    Kalmclaeyd Tarclanus    Separated from ADOConnectStrGenerator
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Interface implementations
''///////////////////////////////////////////////
'Implements IADOConnectStrGenerator
'
''///////////////////////////////////////////////
''/// Declarations
''///////////////////////////////////////////////
'Private mstrUID As String   ' User ID
'
'Private mstrPWD As String   ' Password
'
'Private mstrProviderName As String    ' OLE DB provider name string
'
'Private mstrServerHostName As String
'
'Private mstrNetworkServiceName As String    ' Network service name, for Oracle server
'
'Private mintPortNo As Long  ' connection TCP/IP port number, it is ordinary omitted, for Oracle 1521, for PostgreSQL 5432
'
'
''///////////////////////////////////////////////
''/// Implemented operations
''///////////////////////////////////////////////
'Private Function IADOConnectStrGenerator_GetConnectionString() As String
'
'    IADOConnectStrGenerator_GetConnectionString = GetConnectionStringForOracleOleDb()
'End Function
''///////////////////////////////////////////////
''/// Properties
''///////////////////////////////////////////////
''''
'''' read-only User Name - ODBC
''''
'Public Property Get UserName() As String
'
'    UserName = mstrUID
'End Property
'
''**---------------------------------------------
''** OLE DB connection
''**---------------------------------------------
''''
'''' read-only Provider Name for OLE DB
''''
'Public Property Get ProviderName() As String
'
'    ProviderName = mstrProviderName
'End Property
'
''''
'''' read-only RDB server host name or IP address
''''
'Public Property Get ServerHostName() As String
'
'    ServerHostName = mstrServerHostName
'End Property
'
''''
'''' read-only Oracle network-service name
''''
'Public Property Get NetworkServiceName() As String
'
'    NetworkServiceName = mstrNetworkServiceName
'End Property
'
''''
'''' read-only Port number
''''
'Public Property Get PortNo() As Long
'
'    PortNo = mintPortNo
'End Property
'
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''''
'''' For Oracle OLE DB connection
''''
'Public Function GetConnectionStringForOracleOleDb() As String
'
'    GetConnectionStringForOracleOleDb = GetAdoDbConnectionOleDbStringToOracle(mstrProviderName, mstrServerHostName, mintPortNo, mstrNetworkServiceName, mstrUID, mstrPWD)
'End Function
'
''''
'''' using Oracle OLE DB provider connection string
''''
'Public Sub SetOracleOleDbConnection(ByVal vstrProviderName As String, ByVal vstrServerHostName As String, ByVal vstrNetworkServiceName As String, ByVal vstrUserid As String, ByVal vstrPassword As String, Optional ByVal vintPortNumber As Long = 1521)
'
'    mstrProviderName = vstrProviderName ' for Oracle
'
'    mstrServerHostName = vstrServerHostName
'
'    mstrNetworkServiceName = vstrNetworkServiceName
'
'    mstrUID = vstrUserid   ' User ID
'
'    mstrPWD = vstrPassword   ' Password
'
'    mintPortNo = vintPortNumber
'End Sub
'
'
'''--VBA_Code_File--<ADOConStrToolsForOracle.bas>--
'Attribute VB_Name = "ADOConStrToolsForOracle"
''
''   Generate ADODB connection string to a Oracle data base
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on ADO
''       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   Modification History:
''       Tue, 16/May/2023    Kalmclaeyd Tarclanus    Separated from ADOConnectionUtilities.bas
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Constants
''///////////////////////////////////////////////
'Private Const mintOracleDefaultPortNumber As Long = 1521
'
'#If Win64 Then
'
'    Private Const mstrDriverName As String = "{Oracle in OraClient11g_home1}"    ' it is dependent on the installed ODBC drivers
'#Else
'    Private Const mstrDriverName As String = "{Oracle in OraClient10g_home1}"
'#End If
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''**---------------------------------------------
''** By Oracle OLE DB provider
''**---------------------------------------------
''''
'''' use the OraOLEDB.Oracle provider
''''
'Public Function GetAdoDbConnectionOraOleDbString(ByVal vstrDataSource As String, ByVal vstrUID As String, ByVal vstrPWD As String) As String
'
'    Dim strConnectionString As String
'
'    strConnectionString = "Provider=OraOLEDB.Oracle;"
'
'    strConnectionString = strConnectionString & "Data Source=" & vstrDataSource & ";"
'
'    strConnectionString = strConnectionString & "User ID=" & vstrUID & ";"
'
'    strConnectionString = strConnectionString & "Password=" & vstrPWD
'
'    GetAdoDbConnectionOraOleDbString = strConnectionString
'End Function
'
'
''''
'''' use a Oracle RDB OLEDB provider
''''
'Public Function GetAdoDbConnectionOleDbStringToOracleWithFixedProviderName(ByVal vstrServerHostName As String, _
'        ByVal vstrPortNo As String, _
'        ByVal vstrNetworkServiceName As String, _
'        ByVal vstrUID As String, _
'        ByVal vstrPWD As String) As String
'
'    Dim strConnectionString As String
'
'    Const strProvider As String = "OraOLEDB.Oracle"
'
'    strConnectionString = GetAdoDbConnectionOleDbStringToOracle(strProvider, vstrServerHostName, vstrPortNo, vstrNetworkServiceName, vstrUID, vstrPWD)
'
'    GetAdoDbConnectionOleDbStringToOracleWithFixedProviderName = strConnectionString
'End Function
'
'
''''
'''' use a Oracle RDB OLEDB provider
''''
'Public Function GetAdoDbConnectionOleDbStringToOracle(ByVal vstrProviderName As String, _
'        ByVal vstrServerHostName As String, _
'        ByVal vstrPortNo As String, _
'        ByVal vstrNetworkServiceName As String, _
'        ByVal vstrUID As String, _
'        ByVal vstrPWD As String) As String
'
'    Dim strConnectionString As String
'
'    strConnectionString = "Provider=" & vstrProviderName _
'                        & ";Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)" _
'                        & "(HOST=" & vstrServerHostName & ")" _
'                        & "(PORT=" & vstrPortNo & "))" _
'                        & "(CONNECT_DATA=" _
'                        & "(SERVICE_NAME=" & vstrNetworkServiceName & ")))" _
'                        & ";User ID=" & vstrUID _
'                        & ";PASSWORD=" & vstrPWD
'
'    GetAdoDbConnectionOleDbStringToOracle = strConnectionString
'End Function
'
''**---------------------------------------------
''** By Oracle ODBC driver
''**---------------------------------------------
''''
'''' use a Oracle RDB ODBC driver
''''
'Public Function GetAdoDbConnectionOdbcDSNLessStringToOracle(ByVal vstrDriverName As String, _
'        ByVal vstrServerHostName As String, _
'        ByVal vstrNetworkServiceName As String, _
'        ByVal vstrUID As String, _
'        ByVal vstrPWD As String) As String
'
'    Dim strConnectionString As String
'
'    strConnectionString = "DRIVER=" & vstrDriverName & ";" _
'                        & "SERVER=" & vstrServerHostName & ";" _
'                        & "DBQ=" & vstrNetworkServiceName & ";" _
'                        & "UID=" & vstrUID & ";" _
'                        & "PWD=" & vstrPWD & ";"
'
'    GetAdoDbConnectionOdbcDSNLessStringToOracle = strConnectionString
'End Function
'
'
''''
'''' use a Oracle RDB ODBC driver
''''
'Public Function GetAdoDbConnectionOdbcDSNLessStringToOracleWithPresetDriverName(ByVal vstrServerHostName As String, _
'        ByVal vstrNetworkServiceName As String, _
'        ByVal vstrUID As String, _
'        ByVal vstrPWD As String) As String
'
'    Dim strConnectionString As String
'
'    strConnectionString = GetAdoDbConnectionOdbcDSNLessStringToOracle(mstrDriverName, vstrServerHostName, vstrNetworkServiceName, vstrUID, vstrPWD)
'
'    GetAdoDbConnectionOdbcDSNLessStringToOracleWithPresetDriverName = strConnectionString
'End Function
'
'
''**---------------------------------------------
''** About connecting to Oracle
''**---------------------------------------------
''''
'''' DSN less ODBC Oracle DB connection test
''''
'''' <Argument>UID: User ID</Argument>
'''' <Argument>PWD: Password</Argument>
'Public Function DoesOdbcOracleDSNLessSettingExists(ByVal vstrServerHostName As String, ByVal vstrNetworkServiceName As String, ByVal vstrUID As String, ByVal vstrPWD As String) As Boolean
'
'    Dim strConnectionString As String
'
'    strConnectionString = GetAdoDbConnectionOdbcDSNLessStringToOracle(vstrServerHostName, vstrNetworkServiceName, vstrUID, vstrPWD)
'
'    DoesOdbcOracleDSNLessSettingExists = IsADOConnectionStringEnabled(strConnectionString)
'End Function
'
'
''''
'''' OLE-DB oracle DB connection test
''''
'Public Function DoesOLEDBOracleSettingExists(ByVal vstrServerHostName As String, _
'        ByVal vstrPortNo As String, _
'        ByVal vstrNetworkServiceName As String, _
'        ByVal vstrUID As String, _
'        ByVal vstrPWD As String) As Boolean
'
'    Dim strConnectionString As String
'
'    strConnectionString = GetAdoDbConnectionOleDbStringToOracleWithFixedProviderName(vstrServerHostName, vstrPortNo, vstrNetworkServiceName, vstrUID, vstrPWD)
'
'    DoesOLEDBOracleSettingExists = IsADOConnectionStringEnabled(strConnectionString)
'End Function
'
'
''**---------------------------------------------
''** Find a proper Oracle ODBC driver name
''**---------------------------------------------
''''
''''
''''
'Public Function GetProperOracleDriverName(ByVal vobjDriverNameCandidates As Collection, ByVal vstrServerHostName As String, ByVal vstrNetworkServiceName As String, ByVal vstrUID As String, ByVal vstrPWD) As String
'
'    Dim varDriverName As Variant, strDriverName As String
'    Dim strConnectionString As String, strProperDriverName As String
'
'    strProperDriverName = ""
'
'    For Each varDriverName In vobjDriverNameCandidates
'
'        strDriverName = varDriverName
'
'        strConnectionString = GetAdoODBCConnectionStringForOracle(strDriverName, vstrServerHostName, vstrNetworkServiceName, vstrUID, vstrPWD)
'
'        If IsADOConnectionStringEnabled(strConnectionString) Then
'
'            strProperDriverName = strDriverName
'        End If
'    Next
'
'    GetProperOracleDriverName = strProperDriverName
'End Function
'
'
''''
'''' Return the candidate names of Oracle driver installed
''''
'Public Function GetDefaultOracleODBCDriverNameList() As Collection
'
'    Dim objODBCDriverNames As Collection
'
'#If Win64 Then
'
'    Const strDefaultDriverNames As String = "{Oracle in OraClient11g_home1},{Oracle in OraClient11g_home2},{Oracle in OraClient11g_home3}"  ' it is dependent on the installed ODBC drivers
'#Else
'
'    Const strDefaultDriverNames As String = "{Oracle in OraClient10g_home1},{Oracle in OraClient10g_home2},{Oracle in OraClient10g_home3}"  ' it is dependent on the installed ODBC drivers
'#End If
'
'    Set objODBCDriverNames = GetColFromLineDelimitedChar(strDefaultDriverNames)
'
'    Set GetDefaultOracleODBCDriverNameList = objODBCDriverNames
'End Function
'
''''
''''
''''
'Public Function GetProperOracleDriverNameFromPreservedODBCDriverNames(ByVal vstrServerHostName As String, ByVal vstrNetworkServiceName As String, ByVal vstrUID As String, ByVal vstrPWD As String) As String
'
'    GetProperOracleDriverNameFromPreservedODBCDriverNames = GetProperOracleDriverName(GetDefaultOracleODBCDriverNameList(), vstrServerHostName, vstrNetworkServiceName, vstrUID, vstrPWD)
'End Function
'
'
''**---------------------------------------------
''** for control USetAdoOdbcConStrForOracle user-form
''**---------------------------------------------
''''
''''
''''
'Public Function GetInitialUserInputParametersDicForAdoDSNlessConnectionOfOracle(Optional ByVal vstrDriverName As String = "", _
'        Optional ByVal vstrServerHostName As String = "", _
'        Optional ByVal vstrNetworkServiceName As String = "", _
'        Optional ByVal vstrUserid As String = "", _
'        Optional ByVal vstrPassword As String = "", _
'        Optional ByVal vintPortNumber As Long = mintOracleDefaultPortNumber) As Scripting.Dictionary
'
'    Dim objDic As Scripting.Dictionary
'
'    Set objDic = New Scripting.Dictionary
'
'    With objDic
'
'        .Add "UseDSN", False
'
'
'        .Add "DriverName", vstrDriverName
'
'        .Add "ServerHostName", vstrServerHostName
'
'        .Add "NetworkServiceName", vstrNetworkServiceName
'
'        .Add "PortNumber", vintPortNumber
'
'
'        .Add "UserName", vstrUserid
'
'        .Add mstrTemporaryPdKey, vstrPassword
'
'        .Add mstrAdoConnectionTypeAndDestinationKey, GetDescriptionOfOdbcConnectionDestinationRDBType(OdbcConnectionDestinationToOracle)
'    End With
'
'    Set GetInitialUserInputParametersDicForAdoDSNlessConnectionOfOracle = objDic
'End Function
'
''''
''''
''''
'Public Function GetADOConStrOfOdbcOracleFromInputDic(ByRef robjUserInputParametersDic As Scripting.Dictionary) As ADOConStrOfOdbcOracle
'
'    Dim objADOConStrOfOdbcOracle As ADOConStrOfOdbcOracle
'
'    Set objADOConStrOfOdbcOracle = New ADOConStrOfOdbcOracle
'
'    UpdateADOConStrOfOdbcOracleFromInputDic objADOConStrOfOdbcOracle, robjUserInputParametersDic
'
'    Set GetADOConStrOfOdbcOracleFromInputDic = objADOConStrOfOdbcOracle
'End Function
'
'
''''
''''
''''
'Public Sub UpdateADOConStrOfOdbcOracleFromInputDic(ByRef robjADOConStrOfOdbcOracle As ADOConStrOfOdbcOracle, ByRef robjUserInputParametersDic As Scripting.Dictionary)
'
'    Dim blnUseDSN As Boolean
'
'    blnUseDSN = robjUserInputParametersDic.Item("UseDSN")
'
'    robjADOConStrOfOdbcOracle.UseDSN = blnUseDSN
'
'    If blnUseDSN Then
'
'        UpdateADOConStrOfDSNFromInputDic robjADOConStrOfOdbcOracle.DsnAdoConStrGenerator, robjUserInputParametersDic
'    Else
'        UpdateADOConStrOfDsnlessOracleFromInputDic robjADOConStrOfOdbcOracle.DsnlessAdoConStrGenerator, robjUserInputParametersDic
'    End If
'End Sub
'
''''
''''
''''
'Public Sub UpdateADOConStrOfDsnlessOracleFromInputDic(ByRef robjADOConStrOfDsnlessOracle As ADOConStrOfDsnlessOracle, ByRef robjUserInputParametersDic As Scripting.Dictionary)
'
'    Dim strKey As String, intPortNo As Long
'
'    With robjUserInputParametersDic
'
'        strKey = "PortNumber"
'
'        intPortNo = 0
'
'        If .Exists(strKey) Then
'
'            intPortNo = .Item(strKey)
'        End If
'
'        If intPortNo > 0 Then
'
'            robjADOConStrOfDsnlessOracle.SetOracleOdbcConnectionWithoutDSN .Item("DriverName"), .Item("ServerHostName"), .Item("NetworkServiceName"), .Item("UserName"), .Item(mstrTemporaryPdKey), intPortNo
'        Else
'            robjADOConStrOfDsnlessOracle.SetOracleOdbcConnectionWithoutDSN .Item("DriverName"), .Item("ServerHostName"), .Item("NetworkServiceName"), .Item("UserName"), .Item(mstrTemporaryPdKey)
'        End If
'    End With
'End Sub
'
'
''''
''''
''''
'Public Function GetADOConStrOfDsnlessOracleFromInputDic(ByRef robjUserInputParametersDic As Scripting.Dictionary) As ADOConStrOfDsnlessOracle
'
'    Dim objADOConStrOfDsnlessOracle As ADOConStrOfDsnlessOracle
'
'    Set objADOConStrOfDsnlessOracle = New ADOConStrOfDsnlessOracle
'
'    UpdateADOConStrOfDsnlessOracleFromInputDic objADOConStrOfDsnlessOracle, robjUserInputParametersDic
'
'    Set GetADOConStrOfDsnlessOracleFromInputDic = objADOConStrOfDsnlessOracle
'End Function
'
''''
''''
''''
'Public Function GetADOConnectionOdbcOracleStringFromInputDic(ByRef robjUserInputParametersDic As Scripting.Dictionary) As String
'
'    Dim strConnection As String, blnUseDSN As Boolean
'
'    With robjUserInputParametersDic
'
'        blnUseDSN = .Item("UseDSN")
'
'        If blnUseDSN Then
'
'            strConnection = GetADOConStrOfDSNFromInputDic(robjUserInputParametersDic).GetConnectionString()
'        Else
'            strConnection = GetADOConStrOfDsnlessOracleFromInputDic(robjUserInputParametersDic).GetConnectionString()
'        End If
'    End With
'
'    GetADOConnectionOdbcOracleStringFromInputDic = strConnection
'End Function
'
'
'
''**---------------------------------------------
''** ADO connection string parameters solution interfaces by each special form
''**---------------------------------------------
''''
''''
''''
'''' <Argument>rblnIsRequestedToCancelAdoConnection: Output</Argument>
'''' <Argument>robjADOConStrOfOdbcOracle: Output</Argument>
'''' <Argument>vstrSettingKeyName: Input</Argument>
'''' <Argument>vstrDriverName: Input</Argument>
'''' <Argument>vstrServerHostName: Input</Argument>
'''' <Argument>vstrNetworkServiceName: Input</Argument>
'''' <Argument>vstrUserid: Input</Argument>
'''' <Argument>vstrPassword: Input</Argument>
'''' <Argument>vintPortNumber: Input</Argument>
'''' <Argument>vstrDSN: Input</Argument>
'Public Sub SetOdbcConnectionParametersByOracleForm(ByRef rblnIsRequestedToCancelAdoConnection As Boolean, _
'        ByRef robjADOConStrOfOdbcOracle As ADOConStrOfOdbcOracle, _
'        ByVal vstrSettingKeyName As String, _
'        Optional ByVal vstrDriverName As String = "", _
'        Optional ByVal vstrServerHostName As String = "", _
'        Optional ByVal vstrNetworkServiceName As String = "", _
'        Optional ByVal vstrUserid As String = "", _
'        Optional ByVal vstrPassword As String = "", _
'        Optional ByVal vintPortNumber As Long = mintOracleDefaultPortNumber, _
'        Optional ByVal vstrDSN As String = "")
'
'    Dim objUserInputParametersDic As Scripting.Dictionary, strPassword As String, blnIsOpeningFormNeeded As Boolean
'
'    rblnIsRequestedToCancelAdoConnection = False
'
'    blnIsOpeningFormNeeded = IsOpeningFormNeededAfterLoadFromUserInputParametersDicFirstCacheAndSecondFunctionInputsOfOracle(objUserInputParametersDic, vstrSettingKeyName, vstrDriverName, vstrServerHostName, vstrNetworkServiceName, vstrUserid, vstrPassword, vintPortNumber, vstrDSN)
'
'    If blnIsOpeningFormNeeded Then
'
'        msubSetUserInputParametersDicOfOdbcOracleByOpeningFormWithFixedSettingKeyName rblnIsRequestedToCancelAdoConnection, objUserInputParametersDic, vstrSettingKeyName
'    End If
'
'    If Not rblnIsRequestedToCancelAdoConnection Then
'
'        UpdateADOConStrOfOdbcOracleFromInputDic robjADOConStrOfOdbcOracle, objUserInputParametersDic
'    End If
'End Sub
'
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
''''
''''
''''
'''' <Argument>rblnIsRequestedToCancelAdoConnection: Output</Argument>
'''' <Argument>robjUserInputParametersDic: Input - Output</Argument>
'''' <Argument>rstrSettingKeyName: Input</Argument>
'Private Sub msubSetUserInputParametersDicOfOdbcOracleByOpeningFormWithFixedSettingKeyName(ByRef rblnIsRequestedToCancelAdoConnection As Boolean, _
'        ByRef robjUserInputParametersDic As Scripting.Dictionary, _
'        ByRef rstrSettingKeyName As String)
'
'
'    Dim objFormStateToSetAdoConStr As FormStateToSetAdoConStr, objUSetAdoOdbcConStrForOracle As USetAdoOdbcConStrForOracle
'
'    Set objFormStateToSetAdoConStr = GetFormStateToSetAdoConStrByDefaultParams(robjUserInputParametersDic, rstrSettingKeyName)
'
'    Set objUSetAdoOdbcConStrForOracle = New USetAdoOdbcConStrForOracle
'
'    With objUSetAdoOdbcConStrForOracle
'
'        .SetInitialFormStateToSetAdoConStr objFormStateToSetAdoConStr, True
'
'        .Show vbModal
'    End With
'
'    AfterProcessAdoConStrFormSetting rblnIsRequestedToCancelAdoConnection, robjUserInputParametersDic, objFormStateToSetAdoConStr
'End Sub
'
'
''''
''''
''''
'''' <Argument>robjUserInputParametersDic: Output</Argument>
'''' <Argument>vstrSettingKeyName: Input</Argument>
'''' <Argument>vstrDriverName: Input</Argument>
'''' <Argument>vstrServerHostName: Input</Argument>
'''' <Argument>vstrNetworkServiceName: Input</Argument>
'''' <Argument>vstrUserid: Input</Argument>
'''' <Argument>vstrPassword: Input</Argument>
'''' <Argument>vintPortNumber: Input</Argument>
'''' <Argument>vstrDSN: Input</Argument>
'Private Function IsOpeningFormNeededAfterLoadFromUserInputParametersDicFirstCacheAndSecondFunctionInputsOfOracle(ByRef robjUserInputParametersDic As Scripting.Dictionary, _
'        ByVal vstrSettingKeyName As String, _
'        Optional ByVal vstrDriverName As String = "", _
'        Optional ByVal vstrServerHostName As String = "", _
'        Optional ByVal vstrNetworkServiceName As String = "", _
'        Optional ByVal vstrUserid As String = "", _
'        Optional ByVal vstrPassword As String = "", _
'        Optional ByVal vintPortNumber As Long = mintOracleDefaultPortNumber, _
'        Optional ByVal vstrDSN As String = "") As Boolean
'
'    Dim blnIsOpeningFormNeeded As Boolean
'
'    blnIsOpeningFormNeeded = True
'
'    ' Try to get parameters from Win-registry cache
'
'    Set robjUserInputParametersDic = GetDicOfAdoConnectionStringInfoFromReadingWinRegValues(vstrSettingKeyName)
'
'    Select Case True
'
'        Case robjUserInputParametersDic Is Nothing, robjUserInputParametersDic.Count = 0
'
'            blnIsOpeningFormNeeded = IsOpeningFormNeededAfterInitializingUserInputPrametersDicOfOracle(robjUserInputParametersDic, vstrDriverName, vstrServerHostName, vstrNetworkServiceName, vstrUserid, vstrPassword, vintPortNumber, vstrDSN)
'        Case Else
'            blnIsOpeningFormNeeded = IsOpeningFormNeededAfterTryToGetAdoConnectionPasswordFromTemporaryCache(robjUserInputParametersDic)
'    End Select
'
'    IsOpeningFormNeededAfterLoadFromUserInputParametersDicFirstCacheAndSecondFunctionInputsOfOracle = blnIsOpeningFormNeeded
'End Function
'
'
''''
''''
''''
'''' <Argument>robjUserInputParametersDic: Output</Argument>
'''' <Argument>vstrDriverName: Input</Argument>
'''' <Argument>vstrServerHostName: Input</Argument>
'''' <Argument>vstrNetworkServiceName: Input</Argument>
'''' <Argument>vstrUserid: Input</Argument>
'''' <Argument>vstrPassword: Input</Argument>
'''' <Argument>vintPortNumber: Input</Argument>
'''' <Argument>vstrDSN: Input</Argument>
'Private Function IsOpeningFormNeededAfterInitializingUserInputPrametersDicOfOracle(ByRef robjUserInputParametersDic As Scripting.Dictionary, _
'        Optional ByVal vstrDriverName As String = "", _
'        Optional ByVal vstrServerHostName As String = "", _
'        Optional ByVal vstrNetworkServiceName As String = "", _
'        Optional ByVal vstrUserid As String = "", _
'        Optional ByVal vstrPassword As String = "", _
'        Optional ByVal vintPortNumber As Long = mintOracleDefaultPortNumber, _
'        Optional ByVal vstrDSN As String = "") As Boolean
'
'
'    Dim blnIsOpeningFormNeeded As Boolean
'
'    blnIsOpeningFormNeeded = True
'
'    If vstrDSN <> "" Then
'
'        Set robjUserInputParametersDic = GetInitialUserInputParametersDicForAdoDSNConnection(OdbcConnectionDestinationToOracle, vstrDSN, vstrUserid, vstrPassword)
'    Else
'        Set robjUserInputParametersDic = GetInitialUserInputParametersDicForAdoDSNlessConnectionOfOracle(vstrDriverName, vstrServerHostName, vstrNetworkServiceName, vstrUserid, vstrPassword, vintPortNumber)
'    End If
'
'    If vstrPassword <> "" Then
'
'        If IsADOConnectionStringEnabled(GetADOConnectionOdbcOracleStringFromInputDic(robjUserInputParametersDic)) Then
'
'            blnIsOpeningFormNeeded = False
'        End If
'    End If
'
'    IsOpeningFormNeededAfterInitializingUserInputPrametersDicOfOracle = blnIsOpeningFormNeeded
'End Function
'
'
'
''///////////////////////////////////////////////
''/// Sanity tests
''///////////////////////////////////////////////
''**---------------------------------------------
''** Object creation tests
''**---------------------------------------------
''''
''''
''''
'Private Sub msubSanityTestToCreateADOConStrOfOdbcOracle()
'
'    Dim objADOConStrOfOdbcOracle As ADOConStrOfOdbcOracle, objADOConStrOfDsnlessOracle As ADOConStrOfDsnlessOracle
'
'    Set objADOConStrOfOdbcOracle = New ADOConStrOfOdbcOracle
'
'    Set objADOConStrOfDsnlessOracle = New ADOConStrOfDsnlessOracle
'End Sub
'
'
''**---------------------------------------------
''** USetAdoOdbcConStrForOracle form tests
''**---------------------------------------------
''''
'''' delete testing SettingKeyName from the registory
''''
'Private Sub msubSanityTestToDeleteTestingSettingKeyNamesForConnectingToOracle()
'
'    DeleteWinRegKeyValuesOfAdoConnectionStringInfo "TestOracleDSNlessKey"
'
'    DeleteWinRegKeyValuesOfAdoConnectionStringInfo "TestOracleDSNKey"
'End Sub
'
'
''''
''''
''''
'Private Sub msubSanityTestToOpenUSetAdoOdbcConStrForOracleForDSNless()
'
'    Dim objUserInputParametersDic As Scripting.Dictionary, blnIsRequestedToCancelAdoConnection As Boolean
'
'    Set objUserInputParametersDic = GetInitialUserInputParametersDicForAdoDSNlessConnectionOfOracle()
'
'    msubSetUserInputParametersDicOfOdbcOracleByOpeningFormWithFixedSettingKeyName blnIsRequestedToCancelAdoConnection, objUserInputParametersDic, "TestOracleDSNlessKey"
'
'    If Not blnIsRequestedToCancelAdoConnection Then
'
'        If IsADOConnectionStringEnabled(GetADOConStrOfOdbcOracleFromInputDic(objUserInputParametersDic).GetConnectionString()) Then
'
'            Debug.Print "Connected to Oracle by ADO"
'        End If
'    End If
'End Sub
'
'
''''
'''' DSNless Oracle connection
''''
'Private Sub msubSanityTestToOpenUSetAdoOdbcConStrForOracleForDSNlessWithLongNotations()
'
'
'    Dim objUSetAdoOdbcConStrForOracle As USetAdoOdbcConStrForOracle
'    Dim objFormStateToSetAdoConStr As FormStateToSetAdoConStr
'
'    Set objFormStateToSetAdoConStr = New FormStateToSetAdoConStr
'
'    With objFormStateToSetAdoConStr
'
'        Set .UserInputParametersDic = GetInitialUserInputParametersDicForAdoDSNlessConnectionOfOracle()       ' A document object of a properties form
'
'        .SettingKeyName = "TestOracleDSNlessKey"
'    End With
'
'    Set objUSetAdoOdbcConStrForOracle = New USetAdoOdbcConStrForOracle
'
'    With objUSetAdoOdbcConStrForOracle
'
'        .SetInitialFormStateToSetAdoConStr objFormStateToSetAdoConStr, True
'
'        .Show vbModal
'    End With
'
'    If Not objFormStateToSetAdoConStr.IsRequestedToCancelAdoConnection Then
'
'        msubConnectionTestForOdbcOracle objFormStateToSetAdoConStr, True
'    End If
'End Sub
'
'
''''
'''' DSN connection to Oracle
''''
'Private Sub msubSanityTestToOpenUSetAdoOdbcConStrForOracleForDSN()
'
'    Dim objUserInputParametersDic As Scripting.Dictionary, blnIsRequestedToCancelAdoConnection As Boolean
'
'    Set objUserInputParametersDic = GetInitialUserInputParametersDicForAdoDSNConnection(OdbcConnectionDestinationToOracle)
'
'    msubSetUserInputParametersDicOfOdbcOracleByOpeningFormWithFixedSettingKeyName blnIsRequestedToCancelAdoConnection, objUserInputParametersDic, "TestOracleDSNKey"
'
'    If Not blnIsRequestedToCancelAdoConnection Then
'
'        If IsADOConnectionStringEnabled(GetADOConStrOfOdbcOracleFromInputDic(objUserInputParametersDic).GetConnectionString()) Then
'
'            Debug.Print "Connected to Oracle by ADO"
'        End If
'    End If
'End Sub
'
''''
'''' Connection to Oracle with Form
''''
'Private Sub msubSanityTestToOpenUSetAdoOdbcConStrForOracleFromSettingCache()
'
'
'    Dim objUSetAdoOdbcConStrForOracle As USetAdoOdbcConStrForOracle, objFormStateToSetAdoConStr As FormStateToSetAdoConStr
'
'
'    Set objFormStateToSetAdoConStr = New FormStateToSetAdoConStr
'
'    With objFormStateToSetAdoConStr
'
'        Set .UserInputParametersDic = GetInitialUserInputParametersDicForAdoDSNConnection(OdbcConnectionDestinationToOracle)       ' A document object of a properties form
'
'        .SettingKeyName = ""
'    End With
'
'    Set objUSetAdoOdbcConStrForOracle = New USetAdoOdbcConStrForOracle
'
'    With objUSetAdoOdbcConStrForOracle
'
'        .SetInitialFormStateToSetAdoConStr objFormStateToSetAdoConStr, False
'
'        .Show vbModal
'    End With
'
'    If Not objFormStateToSetAdoConStr.IsRequestedToCancelAdoConnection Then
'
'        msubConnectionTestForOdbcOracle objFormStateToSetAdoConStr, True
'    End If
'End Sub
'
'
'
''///////////////////////////////////////////////
''/// Internal functions for sanity tests
''///////////////////////////////////////////////
'
''''
''''
''''
'Private Sub msubConnectionTestForOdbcOracle(ByRef robjFormStateToSetAdoConStr As FormStateToSetAdoConStr, _
'        Optional ByVal vblnAllowToWriteAdoConStrInfoSettingIntoWinRegistry As Boolean = False)
'
'    If IsADOConnectionStringEnabled(GetADOConStrOfOdbcOracleFromInputDic(robjFormStateToSetAdoConStr.UserInputParametersDic).GetConnectionString()) Then
'
'        Debug.Print "Connected to Oracle by ADO"
'
'        If vblnAllowToWriteAdoConStrInfoSettingIntoWinRegistry Then
'
'            ' Write setting to Registory
'
'            If GetAllowToSaveAdoConStrPasswordInTemporaryCache() Then
'
'                UpdateWinRegKeyValuesOfAdoConnectionStringInfoFromAdoConStrFormStateDic robjFormStateToSetAdoConStr
'            End If
'        End If
'    End If
'End Sub
'
'''--VBA_Code_File--<OracleOdbcConnector.cls>--
'VERSION 1.0 CLASS
'BEGIN
'  MultiUse = -1  'True
'END
'Attribute VB_Name = "OracleOdbcConnector"
'Attribute VB_GlobalNameSpace = False
'Attribute VB_Creatable = False
'Attribute VB_PredeclaredId = False
'Attribute VB_Exposed = False
''
''   ADO ODBC connection and tools for Oracle RDB
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on ADO
''       Dependent on both Oracle ODBC driver at this Windows and an Oracle database server in a network somewhere
''       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access.
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   Modification History:
''       Wed, 26/Jul/2023    Kalmclaeyd Tarclanus    Initial creation
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Interface implementations
''///////////////////////////////////////////////
'Implements IADOConnector
'
''///////////////////////////////////////////////
''/// Declarations
''///////////////////////////////////////////////
'
'Private mobjConnector As ADOConnector: Private mitfConnector As IADOConnector
'
'Private mobjConnectStrGenerator As ADOConStrOfOdbcOracle
'
'
''///////////////////////////////////////////////
''/// Event Handlers
''///////////////////////////////////////////////
'Private Sub Class_Initialize()
'
'    Set mobjConnector = New ADOConnector: Set mitfConnector = mobjConnector
'
'    Set mobjConnectStrGenerator = New ADOConStrOfOdbcOracle
'End Sub
'
'Private Sub Class_Terminate()
'
'    Me.CloseConnection
'
'    Set mitfConnector = Nothing
'
'    Set mobjConnector = Nothing
'
'    Set mobjConnectStrGenerator = Nothing
'End Sub
'
'
''///////////////////////////////////////////////
''/// Properties - connoted ADO connector
''///////////////////////////////////////////////
'Public Property Let AllowToRecordSQLLog(ByVal vblnAllowToRecord As Boolean)
'
'    mobjConnector.AllowToRecordSQLLog = vblnAllowToRecord
'End Property
'Public Property Get AllowToRecordSQLLog() As Boolean
'
'    AllowToRecordSQLLog = mobjConnector.AllowToRecordSQLLog
'End Property
'
'
'Public Property Get SQLExecutionResult() As SQLResult
'
'    Set SQLExecutionResult = IADOConnector_SQLExecutionResult
'End Property
'
'Public Property Get IsSqlExecutionFailed() As Boolean
'
'    IsSqlExecutionFailed = mobjConnector.IsSqlExecutionFailed
'End Property
'
''///////////////////////////////////////////////
''/// Implemented properties
''///////////////////////////////////////////////
'Private Property Let IADOConnector_IsConnected(ByVal vblnIsConnected As Boolean)
'
'    If vblnIsConnected Then
'
'        mitfConnector.IsConnected = mfblnConnect()
'    Else
'        mobjConnector.CloseConnection
'
'        mitfConnector.IsConnected = mobjConnector.IsConnected
'    End If
'End Property
'Private Property Get IADOConnector_IsConnected() As Boolean
'
'    IADOConnector_IsConnected = mfblnConnect()
'End Property
'
'Private Property Set IADOConnector_ADOConnection(ByVal vobjADOConnection As ADODB.Connection)
'
'    Set mitfConnector.ADOConnection = vobjADOConnection
'End Property
'Private Property Get IADOConnector_ADOConnection() As ADODB.Connection
'
'    Set IADOConnector_ADOConnection = mitfConnector.ADOConnection
'End Property
'
'Private Property Set IADOConnector_SQLExecutionResult(ByVal vobjSQLResult As SQLResult)
'
'    Set mitfConnector.SQLExecutionResult = vobjSQLResult
'End Property
'Private Property Get IADOConnector_SQLExecutionResult() As SQLResult
'
'    Set IADOConnector_SQLExecutionResult = mitfConnector.SQLExecutionResult
'End Property
'
'
'Private Property Get IADOConnector_CommandTimeout() As Long
'
'    IADOConnector_CommandTimeout = mobjConnector.CommandTimeout
'End Property
'Private Property Let IADOConnector_CommandTimeout(ByVal vintTimeout As Long)
'
'    mobjConnector.CommandTimeout = vintTimeout
'End Property
'
'Private Property Get IADOConnector_SuppressToShowUpSqlExecutionError() As Boolean
'
'    IADOConnector_SuppressToShowUpSqlExecutionError = mitfConnector.SuppressToShowUpSqlExecutionError
'End Property
'Private Property Let IADOConnector_SuppressToShowUpSqlExecutionError(ByVal vblnSuppressToShowUpSqlExecutionError As Boolean)
'
'    mitfConnector.SuppressToShowUpSqlExecutionError = vblnSuppressToShowUpSqlExecutionError
'End Property
'
'Private Property Get IADOConnector_RdbConnectionInformation() As RdbConnectionInformationFlag
'
'    IADOConnector_RdbConnectionInformation = AdoOdbcToOracleRdbFlag
'End Property
'Private Property Let IADOConnector_RdbConnectionInformation(ByVal venmRdbConnectionInformationFlag As RdbConnectionInformationFlag)
'
'    ' Nothing to do
'End Property
'
''///////////////////////////////////////////////
''/// Properties - connoted ADOConnectionSetting
''///////////////////////////////////////////////
'Public Property Get ConnectSetting() As ADOConnectionSetting
'
'    Set ConnectSetting = mobjConnector.ConnectSetting
'End Property
'
''''
'''' unit is second. [s]
''''
'Public Property Let CommandTimeout(ByVal vintTimeout As Long)
'
'    IADOConnector_CommandTimeout = vintTimeout
'End Property
'Public Property Get CommandTimeout() As Long
'
'    CommandTimeout = IADOConnector_CommandTimeout
'End Property
'
'
'Public Property Get SuppressToShowUpSqlExecutionError() As Boolean
'
'    SuppressToShowUpSqlExecutionError = IADOConnector_SuppressToShowUpSqlExecutionError
'End Property
'Public Property Let SuppressToShowUpSqlExecutionError(ByVal vblnSuppressToShowUpSqlExecutionError As Boolean)
'
'    IADOConnector_SuppressToShowUpSqlExecutionError = vblnSuppressToShowUpSqlExecutionError
'End Property
'
'
''///////////////////////////////////////////////
''/// Implemented operations
''///////////////////////////////////////////////
''''
'''' overrides
''''
'Private Function IADOConnector_GetRecordset(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag) As ADODB.Recordset
'
'    Set IADOConnector_GetRecordset = Me.GetRecordset(vstrSQL, venmShowUpAdoErrorOptionFlag)
'End Function
'
''''
''''
''''
'Private Function IADOConnector_ExecuteSQL(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag) As ADODB.Recordset
'
'    Set IADOConnector_ExecuteSQL = ExecuteSQL(vstrSQL, venmShowUpAdoErrorOptionFlag)
'End Function
'
''''
'''' If the ExecuteSQL method had been used at least one time, this execute ADODB.Connect.CommitTrans()
''''
'Private Sub IADOConnector_CommitTransaction()
'
'    mitfConnector.CommitTransaction
'End Sub
'
''''
'''' If the ExecuteSQL method had been used at least one time, this execute CommitTransaction before execute ADODB.Connect.Close()
''''
'Private Sub IADOConnector_CloseConnection()
'
'    Me.CloseConnection
'End Sub
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''**---------------------------------------------
''** set ADO connection string all parameters directly
''**---------------------------------------------
''''
'''' set ODBC connection parameters by a registered Data Source Name (DSN)
''''
'Public Sub SetODBCParametersByDSN(ByVal vstrDSN As String, ByVal vstrUID As String, ByVal vstrPWD As String)
'
'    mobjConnectStrGenerator.SetODBCConnectionWithDSN vstrDSN, vstrUID, vstrPWD
'End Sub
'
''''
'''' set ODBC connection parameters without Data Source Name (DSN) setting
''''
'Public Sub SetODBCParametersWithoutDSN(ByVal vstrDriverName As String, ByVal vstrServerHostName As String, ByVal vstrNetworkServiceName As String, ByVal vstrUserid As String, ByVal vstrPassword As String, Optional ByVal vintPortNumber As Long = 1521)
'
'    mobjConnectStrGenerator.SetOracleOdbcConnectionWithoutDSN vstrDriverName, vstrServerHostName, vstrNetworkServiceName, vstrUserid, vstrPassword, vintPortNumber
'End Sub
'
''**---------------------------------------------
''** set ADO connection string parameters by a User-form interface
''**---------------------------------------------
''''
'''' Using a User-form, set DSN connection
''''
'''' <Argument>vstrSettingKeyName: Input - This is to be written in Windows-registry<Argument>
'''' <Argument>vstrDSN: Input<Argument>
'''' <Argument>vstrUID: Input<Argument>
'''' <Argument>vstrPWD: Input<Argument>
'Public Function IsAdoConnectionContinuedAfterOdbcConnectionDsnParametersByForm(ByVal vstrSettingKeyName As String, _
'        Optional ByVal vstrDSN As String = "", _
'        Optional ByVal vstrUID As String = "", _
'        Optional ByVal vstrPWD As String = "") As Boolean
'
'
'    IsAdoConnectionContinuedAfterOdbcConnectionDsnParametersByForm = mobjConnectStrGenerator.IsAdoConnectionContinuedAfterOracleOdbcConnectionDsnParametersByForm(vstrSettingKeyName, vstrDSN, vstrUID, vstrPWD)
'End Function
'
''''
'''' Using a User-form, set DSN connection
''''
'''' <Argument>vstrSettingKeyName: Input - This is to be written in Windows-registry<Argument>
'''' <Argument>vstrDriverName: Input<Argument>
'''' <Argument>vstrServerHostName: Input<Argument>
'''' <Argument>vstrNetworkServiceName: Input<Argument>
'''' <Argument>vstrUserid: Input</Argument>
'''' <Argument>vstrPassword: Input</Argument>
'''' <Argument>vintPortNumber: Input</Argument>
'Public Function IsAdoConnectionContinuedAfterOdbcConnectionDsnlessParametersByForm(ByVal vstrSettingKeyName As String, _
'        Optional ByVal vstrDriverName As String = "", _
'        Optional ByVal vstrServerHostName As String = "", _
'        Optional ByVal vstrNetworkServiceName As String = "", _
'        Optional ByVal vstrUserid As String = "", _
'        Optional ByVal vstrPassword As String = "", _
'        Optional ByVal vintPortNumber As Long = 5432) As Boolean
'
'
'    IsAdoConnectionContinuedAfterOdbcConnectionDsnlessParametersByForm = mobjConnectStrGenerator.IsAdoConnectionContinuedAfterOracleOdbcConnectionDsnlessParametersByForm(vstrSettingKeyName, vstrDriverName, vstrServerHostName, vstrNetworkServiceName, vstrUserid, vstrPassword, vintPortNumber)
'End Function
'
'
''**---------------------------------------------
''** Use SQL after the ADO connected
''**---------------------------------------------
''''
'''' get recordset object by the Select SQL
''''
'Public Function GetRecordset(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag) As ADODB.Recordset
'
'    If mfblnConnect() Then
'
'        Set GetRecordset = mobjConnector.GetRecordset(vstrSQL, venmShowUpAdoErrorOptionFlag)
'    End If
'End Function
'
''''
'''' input SQL, such as INSERT, UPDATE, DELETE
''''
'Public Function ExecuteSQL(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag) As ADODB.Recordset
'
'    Dim objRSet As ADODB.Recordset
'
'    Set objRSet = Nothing
'
'    If mfblnConnect() Then
'
'        Set objRSet = mobjConnector.ExecuteSQL(vstrSQL, venmShowUpAdoErrorOptionFlag)
'    End If
'
'    Set ExecuteSQL = objRSet
'End Function
'
''''
''''
''''
'Public Function IsConnected() As Boolean
'
'    IsConnected = mfblnConnect()
'End Function
'
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
''''
'''' connect Excel sheet by ADO
''''
'Private Function mfblnConnect() As Boolean
'
'    Dim blnIsConnected As Boolean
'
'    TryToConnectRDBProviderByAdoConnectionString blnIsConnected, mobjConnector, mobjConnectStrGenerator
'
'    mfblnConnect = blnIsConnected
'End Function
'''--VBA_Code_File--<OracleOleDbConnector.cls>--
'VERSION 1.0 CLASS
'BEGIN
'  MultiUse = -1  'True
'END
'Attribute VB_Name = "OracleOleDbConnector"
'Attribute VB_GlobalNameSpace = False
'Attribute VB_Creatable = False
'Attribute VB_PredeclaredId = False
'Attribute VB_Exposed = False
''
''   connect to a Oracle database by the ADO Oracle OLE DB interface
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on ADO
''       Dependent on an installed Oracle OLE DB Provider,
''       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access.
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   Modification History:
''       Wed, 26/Jul/2023    Kalmclaeyd Tarclanus    Redesigned
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Interface implementations
''///////////////////////////////////////////////
'Implements IADOConnector
'
''///////////////////////////////////////////////
''/// Declarations
''///////////////////////////////////////////////
'Private mobjConnector As ADOConnector: Private mitfConnector As IADOConnector
'
'Private mobjConnectStrGenerator As ADOConStrOfOleDbOracle
'
'
''///////////////////////////////////////////////
''/// Event Handlers
''///////////////////////////////////////////////
'Private Sub Class_Initialize()
'
'    Set mobjConnector = New ADOConnector: Set mitfConnector = mobjConnector
'
'    Set mobjConnectStrGenerator = New ADOConStrOfOleDbOracle
'End Sub
'
'Private Sub Class_Terminate()
'
'    Me.CloseConnection
'
'    Set mitfConnector = Nothing
'
'    Set mobjConnector = Nothing
'
'    Set mobjConnectStrGenerator = Nothing
'End Sub
'
'
''///////////////////////////////////////////////
''/// Properties - connoted ADO connector
''///////////////////////////////////////////////
'Public Property Let AllowToRecordSQLLog(ByVal vblnAllowToRecord As Boolean)
'
'    mobjConnector.AllowToRecordSQLLog = vblnAllowToRecord
'End Property
'Public Property Get AllowToRecordSQLLog() As Boolean
'
'    AllowToRecordSQLLog = mobjConnector.AllowToRecordSQLLog
'End Property
'
'
'Public Property Get SQLExecutionResult() As SQLResult
'
'    Set SQLExecutionResult = IADOConnector_SQLExecutionResult
'End Property
'
'Public Property Get IsSqlExecutionFailed() As Boolean
'
'    IsSqlExecutionFailed = mobjConnector.IsSqlExecutionFailed
'End Property
'
''///////////////////////////////////////////////
''/// Implemented properties
''///////////////////////////////////////////////
'Private Property Let IADOConnector_IsConnected(ByVal vblnIsConnected As Boolean)
'
'    If vblnIsConnected Then
'
'        mitfConnector.IsConnected = mfblnConnect()
'    Else
'        mobjConnector.CloseConnection
'
'        mitfConnector.IsConnected = mobjConnector.IsConnected
'    End If
'End Property
'Private Property Get IADOConnector_IsConnected() As Boolean
'
'    IADOConnector_IsConnected = mfblnConnect()
'End Property
'
'Private Property Set IADOConnector_ADOConnection(ByVal vobjADOConnection As ADODB.Connection)
'
'    Set mitfConnector.ADOConnection = vobjADOConnection
'End Property
'Private Property Get IADOConnector_ADOConnection() As ADODB.Connection
'
'    Set IADOConnector_ADOConnection = mitfConnector.ADOConnection
'End Property
'
'Private Property Set IADOConnector_SQLExecutionResult(ByVal vobjSQLResult As SQLResult)
'
'    Set mitfConnector.SQLExecutionResult = vobjSQLResult
'End Property
'Private Property Get IADOConnector_SQLExecutionResult() As SQLResult
'
'    Set IADOConnector_SQLExecutionResult = mitfConnector.SQLExecutionResult
'End Property
'
'
'Private Property Get IADOConnector_CommandTimeout() As Long
'
'    IADOConnector_CommandTimeout = mobjConnector.CommandTimeout
'End Property
'Private Property Let IADOConnector_CommandTimeout(ByVal vintTimeout As Long)
'
'    mobjConnector.CommandTimeout = vintTimeout
'End Property
'
'Private Property Get IADOConnector_SuppressToShowUpSqlExecutionError() As Boolean
'
'    IADOConnector_SuppressToShowUpSqlExecutionError = mitfConnector.SuppressToShowUpSqlExecutionError
'End Property
'Private Property Let IADOConnector_SuppressToShowUpSqlExecutionError(ByVal vblnSuppressToShowUpSqlExecutionError As Boolean)
'
'    mitfConnector.SuppressToShowUpSqlExecutionError = vblnSuppressToShowUpSqlExecutionError
'End Property
'
'Private Property Get IADOConnector_RdbConnectionInformation() As RdbConnectionInformationFlag
'
'    IADOConnector_RdbConnectionInformation = AdoOleDbToOracleRdbFlag
'End Property
'Private Property Let IADOConnector_RdbConnectionInformation(ByVal venmRdbConnectionInformationFlag As RdbConnectionInformationFlag)
'
'    ' Nothing to do
'End Property
'
''///////////////////////////////////////////////
''/// Properties - connoted ADOConnectionSetting
''///////////////////////////////////////////////
'Public Property Get ConnectSetting() As ADOConnectionSetting
'
'    Set ConnectSetting = mobjConnector.ConnectSetting
'End Property
'
''''
'''' unit is second. [s]
''''
'Public Property Let CommandTimeout(ByVal vintTimeout As Long)
'
'    IADOConnector_CommandTimeout = vintTimeout
'End Property
'Public Property Get CommandTimeout() As Long
'
'    CommandTimeout = IADOConnector_CommandTimeout
'End Property
'
'
'Public Property Get SuppressToShowUpSqlExecutionError() As Boolean
'
'    SuppressToShowUpSqlExecutionError = IADOConnector_SuppressToShowUpSqlExecutionError
'End Property
'Public Property Let SuppressToShowUpSqlExecutionError(ByVal vblnSuppressToShowUpSqlExecutionError As Boolean)
'
'    IADOConnector_SuppressToShowUpSqlExecutionError = vblnSuppressToShowUpSqlExecutionError
'End Property
'
''///////////////////////////////////////////////
''/// Implemented operations
''///////////////////////////////////////////////
''''
'''' overrides
''''
'Private Function IADOConnector_GetRecordset(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag) As ADODB.Recordset
'
'    Set IADOConnector_GetRecordset = Me.GetRecordset(vstrSQL, venmShowUpAdoErrorOptionFlag)
'End Function
'
''''
''''
''''
'Private Function IADOConnector_ExecuteSQL(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag) As ADODB.Recordset
'
'    Set IADOConnector_ExecuteSQL = ExecuteSQL(vstrSQL, venmShowUpAdoErrorOptionFlag)
'End Function
'
''''
'''' If the ExecuteSQL method had been used at least one time, this execute ADODB.Connect.CommitTrans()
''''
'Private Sub IADOConnector_CommitTransaction()
'
'    mitfConnector.CommitTransaction
'End Sub
'
''''
'''' If the ExecuteSQL method had been used at least one time, this execute CommitTransaction before execute ADODB.Connect.Close()
''''
'Private Sub IADOConnector_CloseConnection()
'
'    Me.CloseConnection
'End Sub
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''**---------------------------------------------
''** set ADO connection string all parameters directly
''**---------------------------------------------
''''
'''' set OLE DB connection parameters
''''
'Public Sub SetOLEDBConnection(ByVal vstrProviderName As String, _
'        ByVal vstrServerHostName As String, _
'        ByVal vstrNetworkServiceName As String, _
'        ByVal vstrUserid As String, _
'        ByVal vstrPassword As String, _
'        Optional ByVal vintPortNumber As Long = 1521)
'
'    mobjConnectStrGenerator.SetOracleOleDbConnection vstrProviderName, vstrServerHostName, vstrNetworkServiceName, vstrUserid, vstrPassword, vintPortNumber
'End Sub
'
''**---------------------------------------------
''** Use SQL after the ADO connected
''**---------------------------------------------
''''
'''' get recordset object by the Select SQL
''''
'Public Function GetRecordset(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag) As ADODB.Recordset
'
'    If mfblnConnect() Then
'
'        Set GetRecordset = mobjConnector.GetRecordset(vstrSQL, venmShowUpAdoErrorOptionFlag)
'    End If
'End Function
'
''''
'''' input SQL, such as INSERT, UPDATE, DELETE
''''
'Public Function ExecuteSQL(ByVal vstrSQL As String, _
'        Optional ByVal venmShowUpAdoErrorOptionFlag As ShowUpAdoErrorOptionFlag = ShowUpAdoErrorOptionFlag.NoControlToShowUpAdoSqlErrorFlag) As ADODB.Recordset
'
'    Dim objRSet As ADODB.Recordset
'
'    Set objRSet = Nothing
'
'    If mfblnConnect() Then
'
'        Set objRSet = mobjConnector.ExecuteSQL(vstrSQL, venmShowUpAdoErrorOptionFlag)
'    End If
'
'    Set ExecuteSQL = objRSet
'End Function
'
''''
''''
''''
'Public Function IsConnected() As Boolean
'
'    IsConnected = mfblnConnect()
'End Function
'
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
''''
'''' connect Excel sheet by ADO
''''
'Private Function mfblnConnect() As Boolean
'
'    Dim blnIsConnected As Boolean
'
'    TryToConnectRDBProviderByAdoConnectionString blnIsConnected, mobjConnector, mobjConnectStrGenerator
'
'    mfblnConnect = blnIsConnected
'End Function
'
'
'
'''--VBA_Code_File--<OracleCommonPath.bas>--
'Attribute VB_Name = "OracleCommonPath"
''
''   Simple connection test
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on an installed Oracle client for Windows X64
''       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   Modification History:
''       Mon, 19/Jun/2023    Kalmclaeyd Tarclanus    Corrected
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Constants
''///////////////////////////////////////////////
'Private Const mstrModuleKey As String = "OracleCommonPath"   ' 1st part of registry sub-key
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''**---------------------------------------------
''** Actual implementation of IOutputBookPath
''**---------------------------------------------
''''
'''' solute directory path from registry CURRENT_USER
''''
'Public Function GetOracleConnectingPrearrangedLogBookPath() As String
'
'    GetOracleConnectingPrearrangedLogBookPath = GetCurrentBookOutputDir() & "\SQLTests\OracleAdoConnectingUnittests.xlsx"
'End Function
'
'
'''--VBA_Code_File--<ADOConStrOfOdbcOracle.cls>--
'VERSION 1.0 CLASS
'BEGIN
'  MultiUse = -1  'True
'END
'Attribute VB_Name = "ADOConStrOfOdbcOracle"
'Attribute VB_GlobalNameSpace = False
'Attribute VB_Creatable = False
'Attribute VB_PredeclaredId = False
'Attribute VB_Exposed = False
''
''   ADO ODBC connection string generator for Oracle RDB for both DSN or DSNless
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on ADO
''       Dependent on Oracle ODBC driver such as {Oracle in OraClient12g_home1} at this Windows
''       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access.
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   Modification History:
''       Wed, 12/Jul/2023    Kalmclaeyd Tarclanus    Initial creation
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Constants
''///////////////////////////////////////////////
'Private Const mintOracleDefaultPortNumber As Long = 1521
'
'#If Win64 Then
'
'    Private Const mstrDriverName As String = "{Oracle in OraClient11g_home1}"    ' it is dependent on the installed ODBC drivers
'#Else
'    Private Const mstrDriverName As String = "{Oracle in OraClient10g_home1}"
'#End If
'
''///////////////////////////////////////////////
''/// Interface implementations
''///////////////////////////////////////////////
'Implements IADOConnectStrGenerator
'
''///////////////////////////////////////////////
''/// Declarations
''///////////////////////////////////////////////
'Private mobjADOConStrOfDsn As ADOConStrOfDsn
'
'Private mobjADOConStrOfDsnlessOracle As ADOConStrOfDsnlessOracle
'
'Private mblnUseDSN As Boolean
'
'
''///////////////////////////////////////////////
''/// Event handlers
''///////////////////////////////////////////////
''''
''''
''''
'Private Sub Class_Initialize()
'
'    Set mobjADOConStrOfDsn = New ADOConStrOfDsn
'
'    mobjADOConStrOfDsn.SetOdbcConnectionDestinationInformation OdbcConnectionDestinationToOracle
'
'    Set mobjADOConStrOfDsnlessOracle = New ADOConStrOfDsnlessOracle
'End Sub
'
'
''///////////////////////////////////////////////
''/// Implemented operations
''///////////////////////////////////////////////
'Private Function IADOConnectStrGenerator_GetConnectionString() As String
'
'    If mblnUseDSN Then
'
'        IADOConnectStrGenerator_GetConnectionString = mobjADOConStrOfDsn.GetConnectionString
'    Else
'        IADOConnectStrGenerator_GetConnectionString = mobjADOConStrOfDsnlessOracle.GetConnectionString
'    End If
'End Function
'
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''''
''''
''''
'Public Function GetConnectionString() As String
'
'    GetConnectionString = IADOConnectStrGenerator_GetConnectionString()
'End Function
'
''**---------------------------------------------
''** Set ADODB ODBC parameters directly
''**---------------------------------------------
''''
'''' using ODBC DataSource setting (DSN)
''''
'Public Sub SetODBCConnectionWithDSN(ByVal vstrDSN As String, ByVal vstrUID As String, ByVal vstrPWD As String)
'
'    mobjADOConStrOfDsn.SetODBCConnectionWithDSN vstrDSN, vstrUID, vstrPWD
'
'    mblnUseDSN = True
'End Sub
'
''''
''''
''''
'Public Sub SetOracleOdbcConnectionWithoutDSN(ByVal vstrDriverName As String, _
'        ByVal vstrServerHostName As String, _
'        ByVal vstrNetworkServiceName As String, _
'        ByVal vstrUserid As String, _
'        ByVal vstrPassword As String, _
'        Optional ByVal vintPortNumber As Long = mintOracleDefaultPortNumber)
'
'    mobjADOConStrOfDsnlessOracle.SetOracleOdbcConnectionWithoutDSN vstrDriverName, vstrServerHostName, vstrNetworkServiceName, vstrUserid, vstrPassword, vintPortNumber
'
'    mblnUseDSN = False
'End Sub
'
'
''**---------------------------------------------
''** By a User-form, set ADODB ODBC parameters
''**---------------------------------------------
''''
'''' use SimpleAdoConStrAuthentication.bas
''''
'''' <Argument>vstrSettingKeyName: Input - This is to be written in Windows-registry</Argument>
'''' <Argument>vstrDSN: Input</Argument>
'''' <Argument>vstrUID: Input</Argument>
'''' <Argument>vstrPWD: Input</Argument>
'Public Function IsAdoConnectionContinuedAfterOracleOdbcConnectionDsnParametersByForm(ByVal vstrSettingKeyName As String, _
'        Optional ByVal vstrDSN As String = "", _
'        Optional ByVal vstrUID As String = "", _
'        Optional ByVal vstrPWD As String = "") As Boolean
'
'    Dim blnIsRequestedToCancelAdoConnection As Boolean
'
'    mblnUseDSN = True
'
'    SetOdbcConnectionParametersByOracleForm blnIsRequestedToCancelAdoConnection, Me, vstrSettingKeyName, "", "", "", vstrUID, vstrPWD, , vstrDSN
'
'    IsAdoConnectionContinuedAfterOracleOdbcConnectionDsnParametersByForm = Not blnIsRequestedToCancelAdoConnection
'End Function
'
'
''''
'''' DSNless connection, with referring SimpleAdoConStrAuthentication.bas
''''
'''' <Argument>vstrSettingKeyName: Input - This is to be written in Windows-registry</Argument>
'''' <Argument>vstrDriverName: Input</Argument>
'''' <Argument>vstrServerHostName: Input</Argument>
'''' <Argument>vstrNetworkServiceName: Input</Argument>
'''' <Argument>vstrUserid: Input</Argument>
'''' <Argument>vstrPassword: Input</Argument>
'''' <Argument>vintPortNumber: Input</Argument>
'Public Function IsAdoConnectionContinuedAfterOracleOdbcConnectionDsnlessParametersByForm(ByVal vstrSettingKeyName As String, _
'        Optional ByVal vstrDriverName As String = mstrDriverName, _
'        Optional ByVal vstrServerHostName As String = "", _
'        Optional ByVal vstrNetworkServiceName As String = "", _
'        Optional ByVal vstrUserid As String = "", _
'        Optional ByVal vstrPassword As String = "", _
'        Optional ByVal vintPortNumber As Long = mintOracleDefaultPortNumber) As Boolean
'
'
'    Dim blnIsRequestedToCancelAdoConnection As Boolean
'
'    mblnUseDSN = False
'
'    SetOdbcConnectionParametersByOracleForm blnIsRequestedToCancelAdoConnection, Me, vstrSettingKeyName, vstrDriverName, vstrServerHostName, vstrNetworkServiceName, vstrUserid, vstrPassword, vintPortNumber
'
'    IsAdoConnectionContinuedAfterOracleOdbcConnectionDsnlessParametersByForm = Not blnIsRequestedToCancelAdoConnection
'End Function
'
'
'''--VBA_Code_File--<SQLResult.cls>--
'VERSION 1.0 CLASS
'BEGIN
'  MultiUse = -1  'True
'END
'Attribute VB_Name = "SQLResult"
'Attribute VB_GlobalNameSpace = False
'Attribute VB_Creatable = False
'Attribute VB_PredeclaredId = False
'Attribute VB_Exposed = False
''
''   ADO SQL result simple data class
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on ADO
''       Dependent on CurrentUserDomainUtility.bas
''       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access.
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Wed,  1/Jun/2022    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Declarations
''///////////////////////////////////////////////
''**---------------------------------------------
''** Variables as properties
''**---------------------------------------------
'Public SQLExeElapsedTime As Long  ' mili-second,  SheetOut.bls, OracleOdbcConnector, XlAdoSheetExpander
'
'Public TableRecordCount As Long
'
'Public TableFieldCount As Long
'
'Public SQLExeTime As Date
'
'Public ExcelSheetOutputElapsedTime As Long  ' mili-second
'
''**---------------------------------------------
''** Private variables
''**---------------------------------------------
'Private mintRecordsAffected As Long  ' this is used when the SQL is UPDATE, DELETE, INSERT
'
'Private mstrSQL As String
'
'Private menmSqlOptionalLogFlagOfWorksheet As SqlOptionalLogFlagOfWorksheet
'
'Private mobjLoadedADOConnectionSetting As LoadedADOConnectionSetting
'
'Private mobjCurrentUserDomain As CurrentUserDomain
'
''///////////////////////////////////////////////
''/// Event Handlers
''///////////////////////////////////////////////
'Private Sub Class_Initialize()
'
'    mintRecordsAffected = -1
'
'    menmSqlOptionalLogFlagOfWorksheet = 0
'End Sub
'
'
'
''///////////////////////////////////////////////
''/// Properties
''///////////////////////////////////////////////
'
'Public Property Let SQL(ByVal vstrSQL As String)
'
'    mstrSQL = vstrSQL
'End Property
'Public Property Get SQL() As String
'
'    SQL = mstrSQL
'End Property
'
'
'Public Property Let RecordsAffected(ByVal vintRecordsAffected As Long)
'
'    If vintRecordsAffected >= 0 Then
'
'        mintRecordsAffected = vintRecordsAffected
'    End If
'End Property
'Public Property Get RecordsAffected() As Long
'
'    RecordsAffected = mintRecordsAffected
'End Property
'
''''
'''' read-only SqlOptionalLogFlagOfWorksheet
''''
'Public Property Get SQLOptionalLog() As SqlOptionalLogFlagOfWorksheet
'
'    SQLOptionalLog = menmSqlOptionalLogFlagOfWorksheet
'End Property
'
''''
'''' read-only LoadedADOConnectionSetting
''''
'Public Property Get LoadedADOConnectSetting() As LoadedADOConnectionSetting
'
'    Set LoadedADOConnectSetting = mobjLoadedADOConnectionSetting
'End Property
'
''''
'''' read-only CurrentUserDomain
''''
'Public Property Get CurrentUserInfo() As CurrentUserDomain
'
'    Set CurrentUserInfo = mobjCurrentUserDomain
'End Property
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''''
''''
''''
'Public Sub SetSQLOptionalLog(ByVal vobjLoadedADOConnectionSetting As LoadedADOConnectionSetting)
'
'    Set mobjLoadedADOConnectionSetting = vobjLoadedADOConnectionSetting
'
'    menmSqlOptionalLogFlagOfWorksheet = menmSqlOptionalLogFlagOfWorksheet Or SqlOptionalLogFlagOfWorksheet.LogOfAdoConnectSettingOnSheet
'End Sub
'
''''
''''
''''
'Public Sub SetCurrentUserLog(ByVal vobjCurrentUserDomain As CurrentUserDomain)
'
'    Set mobjCurrentUserDomain = vobjCurrentUserDomain
'
'    ' insert exception code
'    With mobjCurrentUserDomain
'
'        If mblnFEATURE_FLAG_PREVENT_SPECIAL_CLIENT_USER_LOG Then
'
'            Exit Sub
'        End If
'    End With
'
'    menmSqlOptionalLogFlagOfWorksheet = menmSqlOptionalLogFlagOfWorksheet Or SqlOptionalLogFlagOfWorksheet.LogOfCurrentUserComputerDomainOnSheet
'End Sub
'''--VBA_Code_File--<ErrorContentKeeper.cls>--
'VERSION 1.0 CLASS
'BEGIN
'  MultiUse = -1  'True
'END
'Attribute VB_Name = "ErrorContentKeeper"
'Attribute VB_GlobalNameSpace = False
'Attribute VB_Creatable = False
'Attribute VB_PredeclaredId = False
'Attribute VB_Exposed = False
''
''   general VBA error data-clas
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access.
''       Independent on ADO
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Wed,  8/Jun/2022    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Declarations
''///////////////////////////////////////////////
'Private mintNumber As Long
'Private mstrDescription As String
'Private mstrSource As String
'
'
''///////////////////////////////////////////////
''/// Event Handlers
''///////////////////////////////////////////////
'Private Sub Class_Initialize()
'
'    mintNumber = vbObjectError
'
'    mstrDescription = ""
'
'    mstrSource = ""
'End Sub
'
''///////////////////////////////////////////////
''/// Properties
''///////////////////////////////////////////////
''''
'''' error number
''''
'Public Property Get Number() As Long
'
'    Number = mintNumber
'End Property
'Public Property Let Number(ByVal vintNumber As Long)
'
'    mintNumber = vintNumber
'End Property
'
''''
'''' error description
''''
'Public Property Get Description() As String
'
'    Description = mstrDescription
'End Property
'Public Property Let Description(ByVal vstrDescription As String)
'
'    mstrDescription = vstrDescription
'End Property
'
''''
'''' error source
''''
'Public Property Get Source() As String
'
'    Source = mstrSource
'End Property
'Public Property Let Source(ByVal vstrSource As String)
'
'    mstrSource = vstrSource
'End Property
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
'
'Public Sub CopyFrom(ByVal vobjErr As Object, Optional vstrAdditionalMetaDataForSource As String = "")
'
'    With vobjErr
'
'        Me.Number = .Number
'
'        Me.Description = .Description
'
'        On Error Resume Next
'
'        Me.Source = .Source
'
'        If vstrAdditionalMetaDataForSource <> "" Then
'
'            If Me.Source = "" Then
'
'                Me.Source = vstrAdditionalMetaDataForSource
'            Else
'                Me.Source = vstrAdditionalMetaDataForSource & ", " & Me.Source
'            End If
'        End If
'    End With
'End Sub
'''--VBA_Code_File--<ErrorADOSQL.cls>--
'VERSION 1.0 CLASS
'BEGIN
'  MultiUse = -1  'True
'END
'Attribute VB_Name = "ErrorADOSQL"
'Attribute VB_GlobalNameSpace = False
'Attribute VB_Creatable = False
'Attribute VB_PredeclaredId = False
'Attribute VB_Exposed = False
''
''   ADO SQL error data-class
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access.
''       Dependent on ADO
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Wed,  8/Jun/2022    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Declarations
''///////////////////////////////////////////////
''**---------------------------------------------
''** an additional information
''**---------------------------------------------
'Private mudtLoggedDate As Date
'
'Private mobjLoadedConnectionSetting As LoadedADOConnectionSetting
'
'Private mobjSQLResult As SQLResult
'
'Private mblnIsRecordsetExists As Boolean
'
''**---------------------------------------------
''** an error contents
''**---------------------------------------------
'Private mintErrorNumber As Long
'
'Private mstrErrorDescription As String
'
'Private mstrErrorSource As String
'
'
''///////////////////////////////////////////////
''/// Event Handlers
''///////////////////////////////////////////////
'Private Sub Class_Initialize()
'
'    mblnIsRecordsetExists = False
'
'    mstrErrorSource = ""
'End Sub
'
'Private Sub Class_Terminate()
'
'    Set mobjLoadedConnectionSetting = Nothing
'
'    Set mobjSQLResult = Nothing
'End Sub
'
''///////////////////////////////////////////////
''/// Properties
''///////////////////////////////////////////////
''**---------------------------------------------
''** an error contents
''**---------------------------------------------
''''
'''' read-only error number
''''
'Public Property Get ErrorNumber() As Long
'
'    ErrorNumber = mintErrorNumber
'End Property
'
''''
'''' read-only error description
''''
'Public Property Get ErrorDescription() As String
'
'    ErrorDescription = mstrErrorDescription
'End Property
'
''''
'''' read-only error source
''''
'Public Property Get ErrorSource() As String
'
'    ErrorSource = mstrErrorSource
'End Property
'
''**---------------------------------------------
''** an additional information
''**---------------------------------------------
''''
'''' read-only SQL result
''''
'Public Property Get ADOSQLResult() As SQLResult
'
'    Set ADOSQLResult = mobjSQLResult
'End Property
'
''''
'''' read-only, this is false when the Recordset is nothing
''''
'Public Property Get IsRecordsetExists() As Boolean
'
'    IsRecordsetExists = mobjLoadedConnectionSetting.IsRecordsetExists
'End Property
'
''''
'''' read-only Loaded-ConnectionSetting object
''''
'Public Property Get LoadedConnectionSetting() As LoadedADOConnectionSetting
'
'    Set LoadedConnectionSetting = mobjLoadedConnectionSetting
'End Property
'
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''''
''''
''''
'Public Sub SetLog(ByVal vobjErr As Object, _
'        ByVal vobjSQLResult As SQLResult, _
'        ByVal vobjConnection As ADODB.Connection, _
'        ByVal vobjOpeningConnectionSetting As ADOConnectionSetting, _
'        Optional ByVal vobjRecordset As ADODB.Recordset = Nothing)
'
'
'    msubSetErrorAndResult vobjErr, vobjSQLResult
'
'    Set mobjLoadedConnectionSetting = New LoadedADOConnectionSetting
'
'    mobjLoadedConnectionSetting.SetLoadedParameters vobjConnection, vobjOpeningConnectionSetting, vobjRecordset
'End Sub
'
'
'Public Sub SetLogWithLoadedConnectSetting(ByVal vobjErr As Object, _
'        ByVal vobjSQLResult As SQLResult, _
'        ByVal vobjLoadedConnectSetting As LoadedADOConnectionSetting)
'
'
'    msubSetErrorAndResult vobjErr, vobjSQLResult
'
'    Set mobjLoadedConnectionSetting = vobjLoadedConnectSetting
'End Sub
'
'
''''
''''
''''
'Public Function GetErrorSummary() As String
'
'    Dim strMessage As String
'
'    strMessage = ""
'
'    strMessage = strMessage & "[Number] : " & CStr(Me.ErrorNumber) & vbCrLf
'
'    If Me.ErrorSource <> "" Then
'
'        strMessage = strMessage & "[Source] : " & Me.ErrorSource & vbCrLf & vbCrLf
'    End If
'
'    strMessage = strMessage & Me.ErrorDescription
'
'    GetErrorSummary = strMessage
'End Function
'
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
''''
''''
''''
'Private Sub msubSetErrorAndResult(ByVal vobjErr As Object, ByVal vobjSQLResult As SQLResult)
'
'    mudtLoggedDate = Now()
'
'    With vobjErr    ' As ADODB.Error...
'
'        mintErrorNumber = CLng(.Number)
'
'        mstrErrorDescription = .Description
'
'        mstrErrorSource = .Source
'    End With
'
'    Set mobjSQLResult = vobjSQLResult
'End Sub
'
'
'''--VBA_Code_File--<ErrorLogSheetLocator.cls>--
'VERSION 1.0 CLASS
'BEGIN
'  MultiUse = -1  'True
'END
'Attribute VB_Name = "ErrorLogSheetLocator"
'Attribute VB_GlobalNameSpace = False
'Attribute VB_Creatable = False
'Attribute VB_PredeclaredId = False
'Attribute VB_Exposed = False
''
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on Excel
''       Independent on ADO
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Fri, 12/May/2023    Kalmclaeyd Tarclanus    Improved
''
'
'Option Explicit
'
'
''///////////////////////////////////////////////
''/// Declarations
''///////////////////////////////////////////////
'
'Private mstrErrorBookPath As String
'
'Private mobjToLogSheet As Excel.Worksheet
'
'Private mblnAllowToAddLogToOutputingSheet As Boolean
'
''///////////////////////////////////////////////
''/// Event Handlers
''///////////////////////////////////////////////
'Private Sub Class_Initialize()
'
'    mstrErrorBookPath = ""
'
'    mblnAllowToAddLogToOutputingSheet = True
'End Sub
'
'
''///////////////////////////////////////////////
''/// Properties
''///////////////////////////////////////////////
'Public Property Get AllowToAddLogToOutputingSheet() As Boolean
'
'    AllowToAddLogToOutputingSheet = mblnAllowToAddLogToOutputingSheet
'End Property
'Public Property Let AllowToAddLogToOutputingSheet(ByVal vblnAllowToAddLogToOutputingSheet As Boolean)
'
'    mblnAllowToAddLogToOutputingSheet = vblnAllowToAddLogToOutputingSheet
'End Property
'
''''
'''' read-only error log output sheet in order to write log
''''
'Public Property Get ToLogSheet() As Excel.Worksheet
'
'    Set ToLogSheet = mobjToLogSheet
'End Property
'
''''
'''' read-only error-log default Excel sheet name
''''
'Public Property Get DefaultErrorLogSheetName() As String
'
'    DefaultErrorLogSheetName = mfstrGetDefaultSheetName()
'End Property
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''''
''''
''''
'Public Function GetToLogSheetByDefaultSetting() As Excel.Worksheet
'
'    Dim objBook As Excel.Workbook
'
'    With New Scripting.FileSystemObject
'
'        If .FileExists(mfstrGetDefaultFilePath()) Then
'
'            Set objBook = GetWorkbook(mfstrGetDefaultFilePath(), False)
'
'            With objBook.Worksheets
'
'                Set mobjToLogSheet = .Item(.Count)
'            End With
'        Else
'            SetDefaultErrorLog
'        End If
'    End With
'
'    Set GetToLogSheetByDefaultSetting = mobjToLogSheet
'End Function
'
'
''''
''''
''''
'Public Sub SetDefaultErrorLog()
'
'    Dim objLogSheet As Excel.Worksheet
'
'    Set objLogSheet = GetNewWorksheetAfterAllExistedSheets(mfstrGetDefaultFilePath())
'
'    With objLogSheet
'
'        .Name = FindNewSheetNameWhenAlreadyExist(mfstrGetDefaultSheetName(), GetWorkbookIfItExists(mfstrGetDefaultFilePath()))
'
'        DeleteDefaultSheet .Parent
'
'        DeleteDummySheetWhenItExists .Parent
'    End With
'
'    Set mobjToLogSheet = objLogSheet
'End Sub
'
''''
''''
''''
'Public Sub DeleteErrorLogsForDefaultPathBook()
'
'    Dim objBook As Excel.Workbook
'
'    Set objBook = GetWorkbook(mfstrGetDefaultFilePath())
'
'    DeleteSheetsWithoutDummySheet objBook
'
'    SetDefaultErrorLog
'End Sub
'
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
''''
''''
''''
'Private Function mfstrGetDefaultDirectoryPath() As String
'
'    Dim strMyDocumentsDir As String
'
'    'With New IWshRuntimeLibrary.WshShell
'
'    With CreateObject("WScript.Shell")
'
'        strMyDocumentsDir = .SpecialFolders("MyDocuments")
'    End With
'
'    mfstrGetDefaultDirectoryPath = strMyDocumentsDir
'End Function
'
''''
'''' Excel book file name
''''
'Private Function mfstrGetDefaultFileName() As String
'
'    mfstrGetDefaultFileName = "ErrLog.xlsx"
'End Function
'
''''
'''' error log file path
''''
'Private Function mfstrGetDefaultFilePath() As String
'
'    mfstrGetDefaultFilePath = mfstrGetDefaultDirectoryPath() & "\" & mfstrGetDefaultFileName
'End Function
'
'
''''
'''' sheet name
''''
'Private Function mfstrGetDefaultSheetName() As String
'
'    mfstrGetDefaultSheetName = "ErrLog"
'End Function
'''--VBA_Code_File--<ADOFailedInformation.bas>--
'Attribute VB_Name = "ADOFailedInformation"
''
''   ADO error information localized texts, which are refered in UErrorADOSQLForm form object
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on ADO
''       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access.
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Tue,  7/Feb/2023    Kalmclaeyd Tarclanus    Separated from ADOParameters
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Constants
''///////////////////////////////////////////////
'
'Private Const mstrModuleName As String = "ADOFailedInformation"
'
''**---------------------------------------------
''** Key-Value cache preparation for ADOFailedInformation
''**---------------------------------------------
'Private mobjStrKeyValueADOFailedInformationDic As Scripting.Dictionary
'
'Private mblnIsStrKeyValueADOFailedInformationDicInitialized As Boolean   ' Uninitialized boolean value is false in VBA specification.
'
'
''///////////////////////////////////////////////
''/// Declarations
''///////////////////////////////////////////////
''**---------------------------------------------
''** get description for each enumeration (Japanese)
''**---------------------------------------------
'Public Function GetADOEnumDescriptionJPNFromCursorLocation(ByVal venmCursorLocationEnum As ADODB.CursorLocationEnum) As String
'
'    Dim strDescription As String
'
'    Select Case venmCursorLocationEnum
'
'        Case ADODB.CursorLocationEnum.adUseServer
'
'            strDescription = GetTextOfStrKeyAdoFailedInfoCursorLocationEnumUseServer()
'
'        Case ADODB.CursorLocationEnum.adUseClient
'
'            strDescription = GetTextOfStrKeyAdoFailedInfoCursorLocationEnumUseClient()
'    End Select
'
'    GetADOEnumDescriptionJPNFromCursorLocation = strDescription
'End Function
'
'Public Function GetADOEnumDescriptionJPNFromCommandType(ByVal venmCommandTypeEnum As ADODB.CommandTypeEnum) As String
'
'    Dim strDescription As String
'
'    Select Case venmCommandTypeEnum
'
'        Case ADODB.CommandTypeEnum.adCmdUnknown
'
'            strDescription = GetTextOfStrKeyAdoFailedInfoCommandTypeEnumUnknown()
'
'        Case ADODB.CommandTypeEnum.adCmdText
'
'            strDescription = GetTextOfStrKeyAdoFailedInfoCommandTypeEnumText()
'
'        Case ADODB.CommandTypeEnum.adCmdTable
'
'            strDescription = GetTextOfStrKeyAdoFailedInfoCommandTypeEnumTable()
'
'        Case ADODB.CommandTypeEnum.adCmdTableDirect
'
'            strDescription = GetTextOfStrKeyAdoFailedInfoCommandTypeEnumTableDirect()
'
'        Case ADODB.CommandTypeEnum.adCmdStoredProc
'
'            strDescription = GetTextOfStrKeyAdoFailedInfoCommandTypeEnumStoredProcedure()
'
'        Case ADODB.CommandTypeEnum.adCmdFile
'
'            strDescription = GetTextOfStrKeyAdoFailedInfoCommandTypeEnumFile()
'    End Select
'
'    GetADOEnumDescriptionJPNFromCommandType = strDescription
'End Function
'
''''
'''' this evaluate individual flag
''''
'Public Function GetADOEnumDescriptionJPNFromExecuteOption(ByVal venmExecutionOptionEnum As ADOR.ExecuteOptionEnum) As String
'
'    Dim strDescription As String
'
'    Select Case venmExecutionOptionEnum
'
'        Case ADOR.ExecuteOptionEnum.adAsyncExecute
'
'            strDescription = GetTextOfStrKeyAdoFailedInfoExecuteOptionEnumAsyncExecute()
'
'        Case ADOR.ExecuteOptionEnum.adAsyncFetch
'
'            strDescription = GetTextOfStrKeyAdoFailedInfoExecuteOptionEnumAsyncFetch()
'
'        Case ADOR.ExecuteOptionEnum.adAsyncFetchNonBlocking
'
'            strDescription = GetTextOfStrKeyAdoFailedInfoExecuteOptionEnumAsyncFetchNonBlocking()
'
'        Case ADOR.ExecuteOptionEnum.adExecuteNoRecords
'
'            strDescription = GetTextOfStrKeyAdoFailedInfoExecuteOptionEnumExecuteNoRecords()
'
'        Case ADOR.ExecuteOptionEnum.adExecuteRecord
'
'            strDescription = GetTextOfStrKeyAdoFailedInfoExecuteOptionEnumExecuteRecord()
'
'        Case ADOR.ExecuteOptionEnum.adExecuteStream
'
'            strDescription = GetTextOfStrKeyAdoFailedInfoExecuteOptionEnumExecuteStream()
'
'        Case ADOR.ExecuteOptionEnum.adOptionUnspecified
'
'            strDescription = GetTextOfStrKeyAdoFailedInfoExecuteOptionEnumUnspecified()
'    End Select
'
'    GetADOEnumDescriptionJPNFromExecuteOption = strDescription
'End Function
'
'
'Public Function GetADOEnumDescriptionJPNFromRecordsetCursorType(ByVal venmCursorTypeEnum As ADOR.CursorTypeEnum) As String
'
'    Dim strDescription As String
'
'    Select Case venmCursorTypeEnum
'
'        Case ADOR.CursorTypeEnum.adOpenForwardOnly
'
'            strDescription = GetTextOfStrKeyAdoFailedInfoCursorTypeEnumOpenForwardOnly()
'
'        Case ADOR.CursorTypeEnum.adOpenKeyset
'
'            strDescription = GetTextOfStrKeyAdoFailedInfoCursorTypeEnumOpenKeyset()
'
'        Case ADOR.CursorTypeEnum.adOpenDynamic
'
'            strDescription = GetTextOfStrKeyAdoFailedInfoCursorTypeEnumOpenDynamic()
'
'        Case ADOR.CursorTypeEnum.adOpenStatic
'
'            strDescription = GetTextOfStrKeyAdoFailedInfoCursorTypeEnumOpenStatic()
'
'        Case ADOR.CursorTypeEnum.adOpenUnspecified
'
'            strDescription = GetTextOfStrKeyAdoFailedInfoCursorTypeEnumOpenUnspecified()
'    End Select
'
'    GetADOEnumDescriptionJPNFromRecordsetCursorType = strDescription
'End Function
'
'
'Public Function GetADOEnumDescriptionJPNFromRecordsetLockType(ByVal venmLockTypeEnum As ADOR.LockTypeEnum) As String
'
'    Dim strDescription As String
'
'    Select Case venmLockTypeEnum
'
'        Case ADOR.LockTypeEnum.adLockReadOnly
'
'            strDescription = GetTextOfStrKeyAdoFailedInfoLockTypeEnumLockReadOnly()
'
'        Case ADOR.LockTypeEnum.adLockPessimistic
'
'            strDescription = GetTextOfStrKeyAdoFailedInfoLockTypeEnumLockPessimistic()
'
'        Case ADOR.LockTypeEnum.adLockOptimistic
'
'            strDescription = GetTextOfStrKeyAdoFailedInfoLockTypeEnumLockOptimistic()
'
'        Case ADOR.LockTypeEnum.adLockBatchOptimistic
'
'            strDescription = GetTextOfStrKeyAdoFailedInfoLockTypeEnumLockBatchOptimistic()
'
'        Case ADOR.LockTypeEnum.adLockUnspecified
'
'            strDescription = GetTextOfStrKeyAdoFailedInfoLockTypeEnumLockUnspecified()
'    End Select
'
'    GetADOEnumDescriptionJPNFromRecordsetLockType = strDescription
'End Function
'
'
''**---------------------------------------------
''** get flag literals for each enumeration
''**---------------------------------------------
'
'Public Function GetADOEnumAllLiteralsFromExecuteOption(ByVal venmExecutionOptionEnum As ADOR.ExecuteOptionEnum) As String
'
'    Dim varFlag As Variant
'    Dim enmFlag As ADOR.ExecuteOptionEnum
'    Dim i As Long
'
'    Dim strLiterals As String, objCol As Collection
'
'    Set objCol = GetFilteredEnumForExecuteOptionFlag(venmExecutionOptionEnum)
'
'    i = 1
'
'    strLiterals = ""
'
'    For Each varFlag In objCol
'
'        enmFlag = varFlag
'
'        strLiterals = strLiterals & GetADOEnumLiteralFromExecuteOption(enmFlag)
'
'        If i < objCol.Count Then
'
'            strLiterals = strLiterals & vbCrLf
'        End If
'
'        i = i + 1
'    Next
'
'    GetADOEnumAllLiteralsFromExecuteOption = strLiterals
'End Function
'
'
'Public Function GetADOEnumAllDescriptionsJPNFromExecuteOption(ByVal venmExecutionOptionEnum As ADOR.ExecuteOptionEnum) As String
'
'    Dim varFlag As Variant
'    Dim enmFlag As ADOR.ExecuteOptionEnum
'    Dim i As Long
'
'    Dim strDescriptions As String, objCol As Collection
'
'
'    Set objCol = GetFilteredEnumForExecuteOptionFlag(venmExecutionOptionEnum)
'
'    i = 1
'    strDescriptions = ""
'
'    For Each varFlag In objCol
'
'        enmFlag = varFlag
'
'        strDescriptions = strDescriptions & GetADOEnumDescriptionJPNFromExecuteOption(enmFlag)
'
'        If i < objCol.Count Then
'
'            strDescriptions = strDescriptions & vbCrLf
'        End If
'
'        i = i + 1
'    Next
'
'    GetADOEnumAllDescriptionsJPNFromExecuteOption = strDescriptions
'End Function
'
'
'Public Function GetFilteredEnumForExecuteOptionFlag(ByVal venmExecutionOptionEnum As ADOR.ExecuteOptionEnum) As Collection
'
'    Dim objFilteredCol As Collection
'    Dim varFlag As Variant
'    Dim enmFlag As ADOR.ExecuteOptionEnum
'
'    Set objFilteredCol = New Collection
'
'    If venmExecutionOptionEnum = adOptionUnspecified Then
'
'        objFilteredCol.Add venmExecutionOptionEnum
'    Else
'        For Each varFlag In GetAllEnumForExecuteOptionFlag()
'
'            enmFlag = varFlag
'
'            If (venmExecutionOptionEnum And enmFlag) = enmFlag Then
'
'                objFilteredCol.Add enmFlag
'            End If
'        Next
'    End If
'
'    Set GetFilteredEnumForExecuteOptionFlag = objFilteredCol
'End Function
'
'Public Function GetAllEnumForExecuteOptionFlag() As Collection
'
'    Dim objAllCol As Collection
'
'    Set objAllCol = New Collection
'
'    With objAllCol
'
'        .Add ADOR.ExecuteOptionEnum.adAsyncExecute
'
'        .Add ADOR.ExecuteOptionEnum.adAsyncFetch
'
'        .Add ADOR.ExecuteOptionEnum.adAsyncFetchNonBlocking
'
'        .Add ADOR.ExecuteOptionEnum.adExecuteNoRecords
'
'        .Add ADOR.ExecuteOptionEnum.adExecuteRecord
'
'        .Add ADOR.ExecuteOptionEnum.adExecuteStream
'
'        .Add ADOR.ExecuteOptionEnum.adOptionUnspecified
'    End With
'
'    Set GetAllEnumForExecuteOptionFlag = objAllCol
'End Function
'
'
'
''**---------------------------------------------
''** Key-Value cache preparation for ADOFailedInformation
''**---------------------------------------------
''''
'''' get string Value from STR_KEY_ADO_FAILED_INFO_CURSOR_LOCATION_ENUM_USE_SERVER
''''
'''' automatically-added for ADOFailedInformation string-key-value management for standard module and class module
'Private Function GetTextOfStrKeyAdoFailedInfoCursorLocationEnumUseServer() As String
'
'    If Not mblnIsStrKeyValueADOFailedInformationDicInitialized Then
'
'        msubInitializeTextForADOFailedInformation
'    End If
'
'    GetTextOfStrKeyAdoFailedInfoCursorLocationEnumUseServer = mobjStrKeyValueADOFailedInformationDic.Item("STR_KEY_ADO_FAILED_INFO_CURSOR_LOCATION_ENUM_USE_SERVER")
'End Function
'
''''
'''' get string Value from STR_KEY_ADO_FAILED_INFO_CURSOR_LOCATION_ENUM_USE_CLIENT
''''
'''' automatically-added for ADOFailedInformation string-key-value management for standard module and class module
'Private Function GetTextOfStrKeyAdoFailedInfoCursorLocationEnumUseClient() As String
'
'    If Not mblnIsStrKeyValueADOFailedInformationDicInitialized Then
'
'        msubInitializeTextForADOFailedInformation
'    End If
'
'    GetTextOfStrKeyAdoFailedInfoCursorLocationEnumUseClient = mobjStrKeyValueADOFailedInformationDic.Item("STR_KEY_ADO_FAILED_INFO_CURSOR_LOCATION_ENUM_USE_CLIENT")
'End Function
'
''''
'''' get string Value from STR_KEY_ADO_FAILED_INFO_COMMAND_TYPE_ENUM_UNKNOWN
''''
'''' automatically-added for ADOFailedInformation string-key-value management for standard module and class module
'Private Function GetTextOfStrKeyAdoFailedInfoCommandTypeEnumUnknown() As String
'
'    If Not mblnIsStrKeyValueADOFailedInformationDicInitialized Then
'
'        msubInitializeTextForADOFailedInformation
'    End If
'
'    GetTextOfStrKeyAdoFailedInfoCommandTypeEnumUnknown = mobjStrKeyValueADOFailedInformationDic.Item("STR_KEY_ADO_FAILED_INFO_COMMAND_TYPE_ENUM_UNKNOWN")
'End Function
'
''''
'''' get string Value from STR_KEY_ADO_FAILED_INFO_COMMAND_TYPE_ENUM_TEXT
''''
'''' automatically-added for ADOFailedInformation string-key-value management for standard module and class module
'Private Function GetTextOfStrKeyAdoFailedInfoCommandTypeEnumText() As String
'
'    If Not mblnIsStrKeyValueADOFailedInformationDicInitialized Then
'
'        msubInitializeTextForADOFailedInformation
'    End If
'
'    GetTextOfStrKeyAdoFailedInfoCommandTypeEnumText = mobjStrKeyValueADOFailedInformationDic.Item("STR_KEY_ADO_FAILED_INFO_COMMAND_TYPE_ENUM_TEXT")
'End Function
'
''''
'''' get string Value from STR_KEY_ADO_FAILED_INFO_COMMAND_TYPE_ENUM_TABLE
''''
'''' automatically-added for ADOFailedInformation string-key-value management for standard module and class module
'Private Function GetTextOfStrKeyAdoFailedInfoCommandTypeEnumTable() As String
'
'    If Not mblnIsStrKeyValueADOFailedInformationDicInitialized Then
'
'        msubInitializeTextForADOFailedInformation
'    End If
'
'    GetTextOfStrKeyAdoFailedInfoCommandTypeEnumTable = mobjStrKeyValueADOFailedInformationDic.Item("STR_KEY_ADO_FAILED_INFO_COMMAND_TYPE_ENUM_TABLE")
'End Function
'
''''
'''' get string Value from STR_KEY_ADO_FAILED_INFO_COMMAND_TYPE_ENUM_TABLE_DIRECT
''''
'''' automatically-added for ADOFailedInformation string-key-value management for standard module and class module
'Private Function GetTextOfStrKeyAdoFailedInfoCommandTypeEnumTableDirect() As String
'
'    If Not mblnIsStrKeyValueADOFailedInformationDicInitialized Then
'
'        msubInitializeTextForADOFailedInformation
'    End If
'
'    GetTextOfStrKeyAdoFailedInfoCommandTypeEnumTableDirect = mobjStrKeyValueADOFailedInformationDic.Item("STR_KEY_ADO_FAILED_INFO_COMMAND_TYPE_ENUM_TABLE_DIRECT")
'End Function
'
''''
'''' get string Value from STR_KEY_ADO_FAILED_INFO_COMMAND_TYPE_ENUM_FILE
''''
'''' automatically-added for ADOFailedInformation string-key-value management for standard module and class module
'Private Function GetTextOfStrKeyAdoFailedInfoCommandTypeEnumFile() As String
'
'    If Not mblnIsStrKeyValueADOFailedInformationDicInitialized Then
'
'        msubInitializeTextForADOFailedInformation
'    End If
'
'    GetTextOfStrKeyAdoFailedInfoCommandTypeEnumFile = mobjStrKeyValueADOFailedInformationDic.Item("STR_KEY_ADO_FAILED_INFO_COMMAND_TYPE_ENUM_FILE")
'End Function
'
''''
'''' get string Value from STR_KEY_ADO_FAILED_INFO_COMMAND_TYPE_ENUM_STORED_PROCEDURE
''''
'''' automatically-added for ADOFailedInformation string-key-value management for standard module and class module
'Private Function GetTextOfStrKeyAdoFailedInfoCommandTypeEnumStoredProcedure() As String
'
'    If Not mblnIsStrKeyValueADOFailedInformationDicInitialized Then
'
'        msubInitializeTextForADOFailedInformation
'    End If
'
'    GetTextOfStrKeyAdoFailedInfoCommandTypeEnumStoredProcedure = mobjStrKeyValueADOFailedInformationDic.Item("STR_KEY_ADO_FAILED_INFO_COMMAND_TYPE_ENUM_STORED_PROCEDURE")
'End Function
'
''''
'''' get string Value from STR_KEY_ADO_FAILED_INFO_EXECUTE_OPTION_ENUM_ASYNC_EXECUTE
''''
'''' automatically-added for ADOFailedInformation string-key-value management for standard module and class module
'Private Function GetTextOfStrKeyAdoFailedInfoExecuteOptionEnumAsyncExecute() As String
'
'    If Not mblnIsStrKeyValueADOFailedInformationDicInitialized Then
'
'        msubInitializeTextForADOFailedInformation
'    End If
'
'    GetTextOfStrKeyAdoFailedInfoExecuteOptionEnumAsyncExecute = mobjStrKeyValueADOFailedInformationDic.Item("STR_KEY_ADO_FAILED_INFO_EXECUTE_OPTION_ENUM_ASYNC_EXECUTE")
'End Function
'
''''
'''' get string Value from STR_KEY_ADO_FAILED_INFO_EXECUTE_OPTION_ENUM_ASYNC_FETCH
''''
'''' automatically-added for ADOFailedInformation string-key-value management for standard module and class module
'Private Function GetTextOfStrKeyAdoFailedInfoExecuteOptionEnumAsyncFetch() As String
'
'    If Not mblnIsStrKeyValueADOFailedInformationDicInitialized Then
'
'        msubInitializeTextForADOFailedInformation
'    End If
'
'    GetTextOfStrKeyAdoFailedInfoExecuteOptionEnumAsyncFetch = mobjStrKeyValueADOFailedInformationDic.Item("STR_KEY_ADO_FAILED_INFO_EXECUTE_OPTION_ENUM_ASYNC_FETCH")
'End Function
'
''''
'''' get string Value from STR_KEY_ADO_FAILED_INFO_EXECUTE_OPTION_ENUM_ASYNC_FETCH_NON_BLOCKING
''''
'''' automatically-added for ADOFailedInformation string-key-value management for standard module and class module
'Private Function GetTextOfStrKeyAdoFailedInfoExecuteOptionEnumAsyncFetchNonBlocking() As String
'
'    If Not mblnIsStrKeyValueADOFailedInformationDicInitialized Then
'
'        msubInitializeTextForADOFailedInformation
'    End If
'
'    GetTextOfStrKeyAdoFailedInfoExecuteOptionEnumAsyncFetchNonBlocking = mobjStrKeyValueADOFailedInformationDic.Item("STR_KEY_ADO_FAILED_INFO_EXECUTE_OPTION_ENUM_ASYNC_FETCH_NON_BLOCKING")
'End Function
'
''''
'''' get string Value from STR_KEY_ADO_FAILED_INFO_EXECUTE_OPTION_ENUM_EXECUTE_NO_RECORDS
''''
'''' automatically-added for ADOFailedInformation string-key-value management for standard module and class module
'Private Function GetTextOfStrKeyAdoFailedInfoExecuteOptionEnumExecuteNoRecords() As String
'
'    If Not mblnIsStrKeyValueADOFailedInformationDicInitialized Then
'
'        msubInitializeTextForADOFailedInformation
'    End If
'
'    GetTextOfStrKeyAdoFailedInfoExecuteOptionEnumExecuteNoRecords = mobjStrKeyValueADOFailedInformationDic.Item("STR_KEY_ADO_FAILED_INFO_EXECUTE_OPTION_ENUM_EXECUTE_NO_RECORDS")
'End Function
'
''''
'''' get string Value from STR_KEY_ADO_FAILED_INFO_EXECUTE_OPTION_ENUM_EXECUTE_RECORD
''''
'''' automatically-added for ADOFailedInformation string-key-value management for standard module and class module
'Private Function GetTextOfStrKeyAdoFailedInfoExecuteOptionEnumExecuteRecord() As String
'
'    If Not mblnIsStrKeyValueADOFailedInformationDicInitialized Then
'
'        msubInitializeTextForADOFailedInformation
'    End If
'
'    GetTextOfStrKeyAdoFailedInfoExecuteOptionEnumExecuteRecord = mobjStrKeyValueADOFailedInformationDic.Item("STR_KEY_ADO_FAILED_INFO_EXECUTE_OPTION_ENUM_EXECUTE_RECORD")
'End Function
'
''''
'''' get string Value from STR_KEY_ADO_FAILED_INFO_EXECUTE_OPTION_ENUM_EXECUTE_STREAM
''''
'''' automatically-added for ADOFailedInformation string-key-value management for standard module and class module
'Private Function GetTextOfStrKeyAdoFailedInfoExecuteOptionEnumExecuteStream() As String
'
'    If Not mblnIsStrKeyValueADOFailedInformationDicInitialized Then
'
'        msubInitializeTextForADOFailedInformation
'    End If
'
'    GetTextOfStrKeyAdoFailedInfoExecuteOptionEnumExecuteStream = mobjStrKeyValueADOFailedInformationDic.Item("STR_KEY_ADO_FAILED_INFO_EXECUTE_OPTION_ENUM_EXECUTE_STREAM")
'End Function
'
''''
'''' get string Value from STR_KEY_ADO_FAILED_INFO_EXECUTE_OPTION_ENUM_UNSPECIFIED
''''
'''' automatically-added for ADOFailedInformation string-key-value management for standard module and class module
'Private Function GetTextOfStrKeyAdoFailedInfoExecuteOptionEnumUnspecified() As String
'
'    If Not mblnIsStrKeyValueADOFailedInformationDicInitialized Then
'
'        msubInitializeTextForADOFailedInformation
'    End If
'
'    GetTextOfStrKeyAdoFailedInfoExecuteOptionEnumUnspecified = mobjStrKeyValueADOFailedInformationDic.Item("STR_KEY_ADO_FAILED_INFO_EXECUTE_OPTION_ENUM_UNSPECIFIED")
'End Function
'
''''
'''' get string Value from STR_KEY_ADO_FAILED_INFO_CURSOR_TYPE_ENUM_OPEN_FORWARD_ONLY
''''
'''' automatically-added for ADOFailedInformation string-key-value management for standard module and class module
'Private Function GetTextOfStrKeyAdoFailedInfoCursorTypeEnumOpenForwardOnly() As String
'
'    If Not mblnIsStrKeyValueADOFailedInformationDicInitialized Then
'
'        msubInitializeTextForADOFailedInformation
'    End If
'
'    GetTextOfStrKeyAdoFailedInfoCursorTypeEnumOpenForwardOnly = mobjStrKeyValueADOFailedInformationDic.Item("STR_KEY_ADO_FAILED_INFO_CURSOR_TYPE_ENUM_OPEN_FORWARD_ONLY")
'End Function
'
''''
'''' get string Value from STR_KEY_ADO_FAILED_INFO_CURSOR_TYPE_ENUM_OPEN_KEYSET
''''
'''' automatically-added for ADOFailedInformation string-key-value management for standard module and class module
'Private Function GetTextOfStrKeyAdoFailedInfoCursorTypeEnumOpenKeyset() As String
'
'    If Not mblnIsStrKeyValueADOFailedInformationDicInitialized Then
'
'        msubInitializeTextForADOFailedInformation
'    End If
'
'    GetTextOfStrKeyAdoFailedInfoCursorTypeEnumOpenKeyset = mobjStrKeyValueADOFailedInformationDic.Item("STR_KEY_ADO_FAILED_INFO_CURSOR_TYPE_ENUM_OPEN_KEYSET")
'End Function
'
''''
'''' get string Value from STR_KEY_ADO_FAILED_INFO_CURSOR_TYPE_ENUM_OPEN_DYNAMIC
''''
'''' automatically-added for ADOFailedInformation string-key-value management for standard module and class module
'Private Function GetTextOfStrKeyAdoFailedInfoCursorTypeEnumOpenDynamic() As String
'
'    If Not mblnIsStrKeyValueADOFailedInformationDicInitialized Then
'
'        msubInitializeTextForADOFailedInformation
'    End If
'
'    GetTextOfStrKeyAdoFailedInfoCursorTypeEnumOpenDynamic = mobjStrKeyValueADOFailedInformationDic.Item("STR_KEY_ADO_FAILED_INFO_CURSOR_TYPE_ENUM_OPEN_DYNAMIC")
'End Function
'
''''
'''' get string Value from STR_KEY_ADO_FAILED_INFO_CURSOR_TYPE_ENUM_OPEN_STATIC
''''
'''' automatically-added for ADOFailedInformation string-key-value management for standard module and class module
'Private Function GetTextOfStrKeyAdoFailedInfoCursorTypeEnumOpenStatic() As String
'
'    If Not mblnIsStrKeyValueADOFailedInformationDicInitialized Then
'
'        msubInitializeTextForADOFailedInformation
'    End If
'
'    GetTextOfStrKeyAdoFailedInfoCursorTypeEnumOpenStatic = mobjStrKeyValueADOFailedInformationDic.Item("STR_KEY_ADO_FAILED_INFO_CURSOR_TYPE_ENUM_OPEN_STATIC")
'End Function
'
''''
'''' get string Value from STR_KEY_ADO_FAILED_INFO_CURSOR_TYPE_ENUM_OPEN_UNSPECIFIED
''''
'''' automatically-added for ADOFailedInformation string-key-value management for standard module and class module
'Private Function GetTextOfStrKeyAdoFailedInfoCursorTypeEnumOpenUnspecified() As String
'
'    If Not mblnIsStrKeyValueADOFailedInformationDicInitialized Then
'
'        msubInitializeTextForADOFailedInformation
'    End If
'
'    GetTextOfStrKeyAdoFailedInfoCursorTypeEnumOpenUnspecified = mobjStrKeyValueADOFailedInformationDic.Item("STR_KEY_ADO_FAILED_INFO_CURSOR_TYPE_ENUM_OPEN_UNSPECIFIED")
'End Function
'
''''
'''' get string Value from STR_KEY_ADO_FAILED_INFO_LOCK_TYPE_ENUM_LOCK_READ_ONLY
''''
'''' automatically-added for ADOFailedInformation string-key-value management for standard module and class module
'Private Function GetTextOfStrKeyAdoFailedInfoLockTypeEnumLockReadOnly() As String
'
'    If Not mblnIsStrKeyValueADOFailedInformationDicInitialized Then
'
'        msubInitializeTextForADOFailedInformation
'    End If
'
'    GetTextOfStrKeyAdoFailedInfoLockTypeEnumLockReadOnly = mobjStrKeyValueADOFailedInformationDic.Item("STR_KEY_ADO_FAILED_INFO_LOCK_TYPE_ENUM_LOCK_READ_ONLY")
'End Function
'
''''
'''' get string Value from STR_KEY_ADO_FAILED_INFO_LOCK_TYPE_ENUM_LOCK_PESSIMISTIC
''''
'''' automatically-added for ADOFailedInformation string-key-value management for standard module and class module
'Private Function GetTextOfStrKeyAdoFailedInfoLockTypeEnumLockPessimistic() As String
'
'    If Not mblnIsStrKeyValueADOFailedInformationDicInitialized Then
'
'        msubInitializeTextForADOFailedInformation
'    End If
'
'    GetTextOfStrKeyAdoFailedInfoLockTypeEnumLockPessimistic = mobjStrKeyValueADOFailedInformationDic.Item("STR_KEY_ADO_FAILED_INFO_LOCK_TYPE_ENUM_LOCK_PESSIMISTIC")
'End Function
'
''''
'''' get string Value from STR_KEY_ADO_FAILED_INFO_LOCK_TYPE_ENUM_LOCK_OPTIMISTIC
''''
'''' automatically-added for ADOFailedInformation string-key-value management for standard module and class module
'Private Function GetTextOfStrKeyAdoFailedInfoLockTypeEnumLockOptimistic() As String
'
'    If Not mblnIsStrKeyValueADOFailedInformationDicInitialized Then
'
'        msubInitializeTextForADOFailedInformation
'    End If
'
'    GetTextOfStrKeyAdoFailedInfoLockTypeEnumLockOptimistic = mobjStrKeyValueADOFailedInformationDic.Item("STR_KEY_ADO_FAILED_INFO_LOCK_TYPE_ENUM_LOCK_OPTIMISTIC")
'End Function
'
''''
'''' get string Value from STR_KEY_ADO_FAILED_INFO_LOCK_TYPE_ENUM_LOCK_BATCH_OPTIMISTIC
''''
'''' automatically-added for ADOFailedInformation string-key-value management for standard module and class module
'Private Function GetTextOfStrKeyAdoFailedInfoLockTypeEnumLockBatchOptimistic() As String
'
'    If Not mblnIsStrKeyValueADOFailedInformationDicInitialized Then
'
'        msubInitializeTextForADOFailedInformation
'    End If
'
'    GetTextOfStrKeyAdoFailedInfoLockTypeEnumLockBatchOptimistic = mobjStrKeyValueADOFailedInformationDic.Item("STR_KEY_ADO_FAILED_INFO_LOCK_TYPE_ENUM_LOCK_BATCH_OPTIMISTIC")
'End Function
'
''''
'''' get string Value from STR_KEY_ADO_FAILED_INFO_LOCK_TYPE_ENUM_LOCK_UNSPECIFIED
''''
'''' automatically-added for ADOFailedInformation string-key-value management for standard module and class module
'Private Function GetTextOfStrKeyAdoFailedInfoLockTypeEnumLockUnspecified() As String
'
'    If Not mblnIsStrKeyValueADOFailedInformationDicInitialized Then
'
'        msubInitializeTextForADOFailedInformation
'    End If
'
'    GetTextOfStrKeyAdoFailedInfoLockTypeEnumLockUnspecified = mobjStrKeyValueADOFailedInformationDic.Item("STR_KEY_ADO_FAILED_INFO_LOCK_TYPE_ENUM_LOCK_UNSPECIFIED")
'End Function
'
''''
'''' Initialize Key-Values which values
''''
'''' automatically-added for ADOFailedInformation string-key-value management for standard module and class module
'Private Sub msubInitializeTextForADOFailedInformation()
'
'    Dim objKeyToTextDic As Scripting.Dictionary
'
'    If Not mblnIsStrKeyValueADOFailedInformationDicInitialized Then
'
'        Set objKeyToTextDic = GetKeyToTextForLocalizingEachModule(mstrModuleName)
'
'        If objKeyToTextDic Is Nothing Then
'
'            ' If the Key-Value table file is lost, then the default setting is used
'
'            Set objKeyToTextDic = New Scripting.Dictionary
'
'            Select Case GetCurrentUICaptionLanguageType()
'
'                Case CaptionLanguageType.UIJapaneseCaptions
'
'                    AddStringKeyValueForADOFailedInformationByJapaneseValues objKeyToTextDic
'                Case Else
'
'                    AddStringKeyValueForADOFailedInformationByEnglishValues objKeyToTextDic
'            End Select
'        End If
'
'        mblnIsStrKeyValueADOFailedInformationDicInitialized = True
'    End If
'
'    Set mobjStrKeyValueADOFailedInformationDic = objKeyToTextDic
'End Sub
'
''''
'''' Add Key-Values which values are in English for ADOFailedInformation key-values cache
''''
'''' automatically-added for ADOFailedInformation string-key-value management for standard module and class module
'Private Sub AddStringKeyValueForADOFailedInformationByEnglishValues(ByRef robjDic As Scripting.Dictionary)
'
'    With robjDic
'
'        .Add "STR_KEY_ADO_FAILED_INFO_CURSOR_LOCATION_ENUM_USE_SERVER", "Default. Uses cursors supplied by the data provider or driver. These cursors are sometimes very flexible and allow for additional sensitivity to changes others make to the data source. However, some features of the The Microsoft Cursor Service for OLE DB, such as disassociated"
'        .Add "STR_KEY_ADO_FAILED_INFO_CURSOR_LOCATION_ENUM_USE_CLIENT", "Uses client-side cursors supplied by a local cursor library. Local cursor services often will allow many features that driver-supplied cursors may not, so using this setting may provide an advantage with respect to features that will be enabled. For backward compatibility, the synonym adUseClientBatch is also supported."
'        .Add "STR_KEY_ADO_FAILED_INFO_COMMAND_TYPE_ENUM_UNKNOWN", "Default. Indicates that the type of command in the CommandText property is not known. When the type of command is not known, ADO will make several attempts to interpret the CommandText"
'        .Add "STR_KEY_ADO_FAILED_INFO_COMMAND_TYPE_ENUM_TEXT", "Evaluates CommandText as a textual definition of a command or stored procedure call."
'        .Add "STR_KEY_ADO_FAILED_INFO_COMMAND_TYPE_ENUM_TABLE", "Evaluates CommandText as a table name whose columns are all returned by an internally generated SQL query."
'        .Add "STR_KEY_ADO_FAILED_INFO_COMMAND_TYPE_ENUM_TABLE_DIRECT", "Evaluates CommandText as a table name whose columns are all returned. Used with Recordset.Open or Requery only. To use the Seek method, the Recordset must be opened with adCmdTableDirect."
'        .Add "STR_KEY_ADO_FAILED_INFO_COMMAND_TYPE_ENUM_FILE", "Evaluates CommandText as the file name of a persistently stored Recordset. Used with Recordset.Open or Requery only."
'        .Add "STR_KEY_ADO_FAILED_INFO_COMMAND_TYPE_ENUM_STORED_PROCEDURE", "Evaluates CommandText as a stored procedure name."
'        .Add "STR_KEY_ADO_FAILED_INFO_EXECUTE_OPTION_ENUM_ASYNC_EXECUTE", "Indicates that the command should execute asynchronously. This value cannot be combined with the CommandTypeEnum value adCmdTableDirect."
'        .Add "STR_KEY_ADO_FAILED_INFO_EXECUTE_OPTION_ENUM_ASYNC_FETCH", "Indicates that the remaining rows after the initial quantity specified in the CacheSize property should be retrieved asynchronously."
'        .Add "STR_KEY_ADO_FAILED_INFO_EXECUTE_OPTION_ENUM_ASYNC_FETCH_NON_BLOCKING", "Indicates that the main thread never blocks while retrieving. If the requested row has not been retrieved, the current row automatically moves to the end of the file."
'        .Add "STR_KEY_ADO_FAILED_INFO_EXECUTE_OPTION_ENUM_EXECUTE_NO_RECORDS", "Indicates that the command text is a command or stored procedure that does not return rows (for example, a command that only inserts data). If any rows are retrieved, they are discarded and not returned."
'        .Add "STR_KEY_ADO_FAILED_INFO_EXECUTE_OPTION_ENUM_EXECUTE_RECORD", "Indicates that the CommandText is a command or stored procedure that returns a single row which should be returned as a Record object."
'        .Add "STR_KEY_ADO_FAILED_INFO_EXECUTE_OPTION_ENUM_EXECUTE_STREAM", "Indicates that the results of a command execution should be returned as a stream. adExecuteStream can only be passed as an optional parameter to the Command Execute method."
'        .Add "STR_KEY_ADO_FAILED_INFO_EXECUTE_OPTION_ENUM_UNSPECIFIED", "Indicates that the command is unspecified."
'        .Add "STR_KEY_ADO_FAILED_INFO_CURSOR_TYPE_ENUM_OPEN_FORWARD_ONLY", "Default. Uses a forward-only cursor. Identical to a static cursor, except that you can only scroll forward through records. This improves performance when you need to make only one pass through a Recordset."
'        .Add "STR_KEY_ADO_FAILED_INFO_CURSOR_TYPE_ENUM_OPEN_KEYSET", "Uses a keyset cursor. Like a dynamic cursor, except that you can't see records that other users add, although records that other users delete are inaccessible from your Recordset. Data changes by other users are still visible."
'        .Add "STR_KEY_ADO_FAILED_INFO_CURSOR_TYPE_ENUM_OPEN_DYNAMIC", "Uses a dynamic cursor. Additions, changes, and deletions by other users are visible, and all types of movement through the Recordset are allowed, except for bookmarks, if the provider doesn't support them."
'        .Add "STR_KEY_ADO_FAILED_INFO_CURSOR_TYPE_ENUM_OPEN_STATIC", "Uses a static cursor, which is a static copy of a set of records that you can use to find data or generate reports. Additions, changes, or deletions by other users are not visible."
'        .Add "STR_KEY_ADO_FAILED_INFO_CURSOR_TYPE_ENUM_OPEN_UNSPECIFIED", "Does not specify the type of cursor."
'        .Add "STR_KEY_ADO_FAILED_INFO_LOCK_TYPE_ENUM_LOCK_READ_ONLY", "Indicates read-only records. You cannot alter the data."
'        .Add "STR_KEY_ADO_FAILED_INFO_LOCK_TYPE_ENUM_LOCK_PESSIMISTIC", "Indicates pessimistic locking, record by record. The provider does what is necessary to ensure successful editing of the records, usually by locking records at the data source immediately after editing."
'        .Add "STR_KEY_ADO_FAILED_INFO_LOCK_TYPE_ENUM_LOCK_OPTIMISTIC", "Indicates optimistic locking, record by record. The provider uses optimistic locking, locking records only when you call the Update method."
'        .Add "STR_KEY_ADO_FAILED_INFO_LOCK_TYPE_ENUM_LOCK_BATCH_OPTIMISTIC", "Indicates optimistic batch updates. Required for batch update mode."
'        .Add "STR_KEY_ADO_FAILED_INFO_LOCK_TYPE_ENUM_LOCK_UNSPECIFIED", "Does not specify a type of lock. For clones, the clone is created with the same lock type as the original."
'    End With
'End Sub
'
''''
'''' Add Key-Values which values are in Japanese for ADOFailedInformation key-values cache
''''
'''' automatically-added for ADOFailedInformation string-key-value management for standard module and class module
'Private Sub AddStringKeyValueForADOFailedInformationByJapaneseValues(ByRef robjDic As Scripting.Dictionary)
'
'    With robjDic
'
'        .Add "STR_KEY_ADO_FAILED_INFO_CURSOR_LOCATION_ENUM_USE_SERVER", "既定値 - サーバのカーソルを使用"
'        .Add "STR_KEY_ADO_FAILED_INFO_CURSOR_LOCATION_ENUM_USE_CLIENT", "ADOはCursor Serviceを起動し、取得したデータをクライアント側にコピー、また､そのデータによってRecordCountプロパティにレコード数が設定されます。"
'        .Add "STR_KEY_ADO_FAILED_INFO_COMMAND_TYPE_ENUM_UNKNOWN", "既定値. CommandTextプロパティの中のコマンドの種類が不明"
'        .Add "STR_KEY_ADO_FAILED_INFO_COMMAND_TYPE_ENUM_TEXT", "CommandTextをテキストによるコマンドの定義, SQL文やストアドプロシージャとして評価"
'        .Add "STR_KEY_ADO_FAILED_INFO_COMMAND_TYPE_ENUM_TABLE", "CommandTextをテーブル名として評価"
'        .Add "STR_KEY_ADO_FAILED_INFO_COMMAND_TYPE_ENUM_TABLE_DIRECT", "すべての列が返されたテーブル名としてCommandTextを評価します。 レコードセットで使用されます。 OpenまたはRequeryのみ。 Seekメソッドを使用するには、レコードセットをadCmdTableDirectで開く必要があります。"
'        .Add "STR_KEY_ADO_FAILED_INFO_COMMAND_TYPE_ENUM_FILE", "永続的に格納されたレコードセットのファイル名としてCommandTextを評価します。 レコードセットで使用されます。OpenまたはRequeryのみ。"
'        .Add "STR_KEY_ADO_FAILED_INFO_COMMAND_TYPE_ENUM_STORED_PROCEDURE", "CommandTextをストアド プロシージャ名として評価"
'        .Add "STR_KEY_ADO_FAILED_INFO_EXECUTE_OPTION_ENUM_ASYNC_EXECUTE", "コマンドを非同期的に実行する。 CommandTypeEnum値adCmdTableDirectと組み合わせることはできません。"
'        .Add "STR_KEY_ADO_FAILED_INFO_EXECUTE_OPTION_ENUM_ASYNC_FETCH", "CacheSizeプロパティで指定された初期数量の後の残りの行を非同期に取得する"
'        .Add "STR_KEY_ADO_FAILED_INFO_EXECUTE_OPTION_ENUM_ASYNC_FETCH_NON_BLOCKING", "メインスレッドが取得中にブロックされないことを示します。 要求された行が取得されていない場合は、現在の行が自動的にファイルの末尾に移動"
'        .Add "STR_KEY_ADO_FAILED_INFO_EXECUTE_OPTION_ENUM_EXECUTE_NO_RECORDS", "adExecuteNoRecordsは、コマンドまたは接続の Executeメソッドにオプションのパラメーターとしてのみ渡す。コマンドテキストが、行を返さないコマンドまたはストアドプロシージャ (たとえば、データを挿入するコマンド) であることを示します。 取得された行は破棄され、返されません。"
'        .Add "STR_KEY_ADO_FAILED_INFO_EXECUTE_OPTION_ENUM_EXECUTE_RECORD", "CommandTextが、レコードオブジェクトとして返される1つの行を返すコマンドまたはストアドプロシージャであることを示します。"
'        .Add "STR_KEY_ADO_FAILED_INFO_EXECUTE_OPTION_ENUM_EXECUTE_STREAM", "adExecuteStreamはコマンドの Executeメソッドにオプションのパラメーターとしてのみ渡すことができます。コマンドの実行結果をストリームとして返します"
'        .Add "STR_KEY_ADO_FAILED_INFO_EXECUTE_OPTION_ENUM_UNSPECIFIED", "コマンドの起動の指定オプション無し"
'        .Add "STR_KEY_ADO_FAILED_INFO_CURSOR_TYPE_ENUM_OPEN_FORWARD_ONLY", "既定値. 順方向専用カーソルを使用します。 レコードをスクロールするだけでスクロールできる点を除いて、静的カーソルと同じです。 これにより、レコードセットのパススルーを1回だけ行う必要がある場合にパフォーマンスが向上します。"
'        .Add "STR_KEY_ADO_FAILED_INFO_CURSOR_TYPE_ENUM_OPEN_KEYSET", "キーセットカーソルを使用します。 動的カーソルと同様に、他のユーザーが追加したレコードは表示されませんが、他のユーザーが削除したレコードにはレコードセットからアクセスできません。 他のユーザーによるデータの変更は引き続き表示されます。"
'        .Add "STR_KEY_ADO_FAILED_INFO_CURSOR_TYPE_ENUM_OPEN_DYNAMIC", "動的カーソルを使用します。 他のユーザーによる追加、変更、および削除が表示され、プロバイダーがサポートしていない場合は、ブックマークを除くすべての種類の移動が許可されます。"
'        .Add "STR_KEY_ADO_FAILED_INFO_CURSOR_TYPE_ENUM_OPEN_STATIC", "静的カーソルを使用します。これは、データの検索やレポートの生成に使用できる一連のレコードの静的なコピーです。 他のユーザーによる追加、変更、または削除は表示されません。"
'        .Add "STR_KEY_ADO_FAILED_INFO_CURSOR_TYPE_ENUM_OPEN_UNSPECIFIED", "カーソルの種類を指定しません。"
'        .Add "STR_KEY_ADO_FAILED_INFO_LOCK_TYPE_ENUM_LOCK_READ_ONLY", "読み取り専用レコードを示します。 データを変更することはできません。"
'        .Add "STR_KEY_ADO_FAILED_INFO_LOCK_TYPE_ENUM_LOCK_PESSIMISTIC", "レコード単位の排他的ロック。プロバイダーは、レコードを正常に編集するために必要な処理を実行。通常は、編集するとすぐデータソースのレコードをロック。"
'        .Add "STR_KEY_ADO_FAILED_INFO_LOCK_TYPE_ENUM_LOCK_OPTIMISTIC", "レコード単位の共有的ロック。プロバイダーは共有的ロックを使用し、 Updateメソッドを呼び出したときにのみレコードをロックします。"
'        .Add "STR_KEY_ADO_FAILED_INFO_LOCK_TYPE_ENUM_LOCK_BATCH_OPTIMISTIC", "共有的バッチ更新を示します。 バッチ更新モードの場合のみ指定できます。"
'        .Add "STR_KEY_ADO_FAILED_INFO_LOCK_TYPE_ENUM_LOCK_UNSPECIFIED", "ロックの種類を指定していません。 複製の場合、複製は元と同じロックの種類で作成されます。"
'    End With
'End Sub
'
''''
'''' Remove Keys for ADOFailedInformation key-values cache
''''
'''' automatically-added for ADOFailedInformation string-key-value management for standard module and class module
'Private Sub RemoveKeysOfKeyValueADOFailedInformation(ByRef robjDic As Scripting.Dictionary)
'
'    With robjDic
'
'        If .Exists(STR_KEY_ADO_FAILED_INFO_CURSOR_LOCATION_ENUM_USE_SERVER) Then   ' Check only the first key, otherwise omit items
'
'            .Remove "STR_KEY_ADO_FAILED_INFO_CURSOR_LOCATION_ENUM_USE_SERVER"
'            .Remove "STR_KEY_ADO_FAILED_INFO_CURSOR_LOCATION_ENUM_USE_CLIENT"
'            .Remove "STR_KEY_ADO_FAILED_INFO_COMMAND_TYPE_ENUM_UNKNOWN"
'            .Remove "STR_KEY_ADO_FAILED_INFO_COMMAND_TYPE_ENUM_TEXT"
'            .Remove "STR_KEY_ADO_FAILED_INFO_COMMAND_TYPE_ENUM_TABLE"
'            .Remove "STR_KEY_ADO_FAILED_INFO_COMMAND_TYPE_ENUM_TABLE_DIRECT"
'            .Remove "STR_KEY_ADO_FAILED_INFO_COMMAND_TYPE_ENUM_FILE"
'            .Remove "STR_KEY_ADO_FAILED_INFO_COMMAND_TYPE_ENUM_STORED_PROCEDURE"
'            .Remove "STR_KEY_ADO_FAILED_INFO_EXECUTE_OPTION_ENUM_ASYNC_EXECUTE"
'            .Remove "STR_KEY_ADO_FAILED_INFO_EXECUTE_OPTION_ENUM_ASYNC_FETCH"
'            .Remove "STR_KEY_ADO_FAILED_INFO_EXECUTE_OPTION_ENUM_ASYNC_FETCH_NON_BLOCKING"
'            .Remove "STR_KEY_ADO_FAILED_INFO_EXECUTE_OPTION_ENUM_EXECUTE_NO_RECORDS"
'            .Remove "STR_KEY_ADO_FAILED_INFO_EXECUTE_OPTION_ENUM_EXECUTE_RECORD"
'            .Remove "STR_KEY_ADO_FAILED_INFO_EXECUTE_OPTION_ENUM_EXECUTE_STREAM"
'            .Remove "STR_KEY_ADO_FAILED_INFO_EXECUTE_OPTION_ENUM_UNSPECIFIED"
'            .Remove "STR_KEY_ADO_FAILED_INFO_CURSOR_TYPE_ENUM_OPEN_FORWARD_ONLY"
'            .Remove "STR_KEY_ADO_FAILED_INFO_CURSOR_TYPE_ENUM_OPEN_KEYSET"
'            .Remove "STR_KEY_ADO_FAILED_INFO_CURSOR_TYPE_ENUM_OPEN_DYNAMIC"
'            .Remove "STR_KEY_ADO_FAILED_INFO_CURSOR_TYPE_ENUM_OPEN_STATIC"
'            .Remove "STR_KEY_ADO_FAILED_INFO_CURSOR_TYPE_ENUM_OPEN_UNSPECIFIED"
'            .Remove "STR_KEY_ADO_FAILED_INFO_LOCK_TYPE_ENUM_LOCK_READ_ONLY"
'            .Remove "STR_KEY_ADO_FAILED_INFO_LOCK_TYPE_ENUM_LOCK_PESSIMISTIC"
'            .Remove "STR_KEY_ADO_FAILED_INFO_LOCK_TYPE_ENUM_LOCK_OPTIMISTIC"
'            .Remove "STR_KEY_ADO_FAILED_INFO_LOCK_TYPE_ENUM_LOCK_BATCH_OPTIMISTIC"
'            .Remove "STR_KEY_ADO_FAILED_INFO_LOCK_TYPE_ENUM_LOCK_UNSPECIFIED"
'        End If
'    End With
'End Sub
'
'
'
'''--VBA_Code_File--<ImportAllVBACodes.bas>--
'Attribute VB_Name = "ImportAllVBACodes"
''
''   read this VBA project all source codes from the specified directory
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Development Policy:
''       Use some duplicated codes from outside VBA code modules for keeping this module independency.
''
''   Dependency Abstract:
''       Dependent on VBIDE
''       This is an independent code from all other modules
''       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access, or ADO
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Mon, 23/May/2022    Kalmclaeyd Tarclanus    Initial Creation
''       Mon, 13/Feb/2023    Kalmclaeyd Tarclanus    Supported the caption localization for both English and Japanese.
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Directives
''///////////////////////////////////////////////
'#Const HAS_REF = False   ' If you get the code-snipet support, you need to change HAS_REF to True
'
'
''///////////////////////////////////////////////
''/// Constants
''///////////////////////////////////////////////
'
'Private Const mstrModuleName As String = "ImportAllVBACodes"   ' 1st part of registry sub-key
'
''///////////////////////////////////////////////
''/// Declarations
''///////////////////////////////////////////////
''**---------------------------------------------
''** Key-Value cache preparation for ImportAllVBACodes
''**---------------------------------------------
'Private mobjStrKeyValueImportAllVBACodesDic As Object
'
'Private mblnIsStrKeyValueImportAllVBACodesDicInitialized As Boolean   ' Uninitialized boolean value is false in VBA specification.
'
'
''///////////////////////////////////////////////
''/// Sanity tests
''///////////////////////////////////////////////
''''
''''
''''
'Private Sub msubSanityTestToLoadAllVBACodesFromDirectoryWithUserSelected()
'
'    Dim objApplicationObjects As Object
'
'    Select Case Trim(Replace(LCase(Application.Name), "microsoft", ""))
'
'        Case "excel"
'
'            Dim objBook As Object
'
'            Set objApplicationObjects = VBA.CallByName(Application, "Workbooks", VbGet)
'
'            Set objBook = VBA.CallByName(objApplicationObjects, "Add", VbMethod)
'
'            LoadAllVBACodesFromDirectoryWithUserSelected objBook, True
'
'        Case "word"
'
'            Dim objDocument As Object
'
'            Set objApplicationObjects = VBA.CallByName(Application, "Documents", VbGet)
'
'            Set objDocument = VBA.CallByName(objApplicationObjects, "Add", VbMethod)
'
'            LoadAllVBACodesFromDirectoryWithUserSelected objDocument, True
'
'        Case "powerpoint"
'
'            Dim objPresentation As Object
'
'            Set objApplicationObjects = VBA.CallByName(Application, "Presentations", VbGet)
'
'            Set objPresentation = VBA.CallByName(objApplicationObjects, "Add", VbMethod)
'
'            LoadAllVBACodesFromDirectoryWithUserSelected objPresentation, True
'    End Select
'End Sub
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''''
''''
''''
'Public Sub LoadAllVBACodesFromDirectory(ByVal vstrVBACodesDirectoryPath As String, ByVal vobjOfficeFile As Object, Optional ByVal vblnForceToImport As Boolean = False, Optional ByVal vblnAllowToImportFromSubDirectory As Boolean = True, Optional ByVal vblnAllowToCreateLog As Boolean = True)
'
'    Dim intImportedCount As Long, objImportedFilesCol As Collection
'
'#If HAS_REF Then
'    Dim objFS As Scripting.FileSystemObject
'
'    Set objFS = New Scripting.FileSystemObject
'#Else
'    Dim objFS As Object
'
'    Set objFS = CreateObject("Scripting.FileSystemObject")
'#End If
'
'
'    msubImportAllModulesFromDirectory intImportedCount, objImportedFilesCol, vstrVBACodesDirectoryPath, vobjOfficeFile.VBProject, objFS, vblnForceToImport
'
'    msubCheckImportedResultAndLogging intImportedCount, objImportedFilesCol, vstrVBACodesDirectoryPath, vobjOfficeFile, vblnAllowToCreateLog
'End Sub
'
'
''''
'''' using folder-dialog, load all VBA source files
''''
'Public Sub LoadAllVBACodesFromDirectoryWithUserSelected(ByVal vobjOfficeFile As Object, Optional ByVal vblnForceToImport As Boolean = False)
'
'    Dim strDirPath As String
'
'    With Application.FileDialog(msoFileDialogFolderPicker)
'
'        .Title = GetTextOfStrKeyImportAllVbaCodesSelectDirectoryThatIncludesVbaSource()
'
'        .AllowMultiSelect = False
'
'        If .Show = -1 Then
'
'            strDirPath = .SelectedItems(1)
'
'            LoadAllVBACodesFromDirectory strDirPath, vobjOfficeFile, vblnForceToImport
'        Else
'
'            Debug.Print GetTextOfStrKeyImportAllVbaCodesCanceledSelectingDirectory() & "..."
'        End If
'    End With
'End Sub
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
''''
''''
'''' This is a recursive call function.
''''
'#If HAS_REF Then
'Private Sub msubImportAllModulesFromDirectory(ByRef rintImportedCount As Long, ByRef robjImportedFilesCol As Collection, ByVal vstrVBACodesDirectoryPath As String, ByVal vobjVBProject As VBIDE.VBProject, ByVal vobjFS As Scripting.FileSystemObject, Optional ByVal vblnForceToImport As Boolean = False, Optional ByVal vblnAllowToImportFromSubDirectory As Boolean = True)
'#Else
'Private Sub msubImportAllModulesFromDirectory(ByRef rintImportedCount As Long, ByRef robjImportedFilesCol As Collection, ByVal vstrVBACodesDirectoryPath As String, ByVal vobjVBProject As Object, ByVal vobjFS As Object, Optional ByVal vblnForceToImport As Boolean = False, Optional ByVal vblnAllowToImportFromSubDirectory As Boolean = True)
'#End If
'    Dim strFileName As String, strSourcePath As String, strSourceFileBaseName As String
'
'#If HAS_REF Then
'    Dim objDir As Scripting.Folder, objFile As Scripting.File
'    Dim objSubDir As Scripting.Folder
'#Else
'    Dim objDir As Object, objFile As Object
'    Dim objComponent As Object, objSubDir As Object
'#End If
'
'    With vobjFS
'
'        Set objDir = .GetFolder(vstrVBACodesDirectoryPath)
'
'        If Not objDir Is Nothing Then
'
'            For Each objFile In objDir.Files
'
'                Select Case LCase(.GetExtensionName(objFile.Name))
'                    Case "bas", "cls", "frm"
'
'                        rintImportedCount = rintImportedCount + 1
'
'                        strSourcePath = objFile.Path
'
'                        msubImportVBACodeWithLogging robjImportedFilesCol, vobjVBProject, vobjFS, objFile.Path, vblnForceToImport
'                End Select
'            Next
'
'            If vblnAllowToImportFromSubDirectory Then
'
'                For Each objSubDir In objDir.SubFolders
'
'                    ' recursive call
'                    msubImportAllModulesFromDirectory rintImportedCount, robjImportedFilesCol, objSubDir.Path, vobjVBProject, vobjFS, vblnForceToImport, vblnAllowToImportFromSubDirectory
'                Next
'            End If
'        End If
'    End With
'End Sub
'
'
''''
''''
''''
'#If HAS_REF Then
'Private Sub msubImportVBACodeWithLogging(ByRef robjImportedFilesCol As Collection, ByVal vobjVBProject As VBIDE.VBProject, ByVal vobjFS As Scripting.FileSystemObject, ByRef rstrSourcePath As String, ByVal vblnForceToImport As Boolean)
'
'    Dim objComponent As VBIDE.VBComponent
'#Else
'Private Sub msubImportVBACodeWithLogging(ByRef robjImportedFilesCol As Collection, ByVal vobjVBProject As Object, ByVal vobjFS As Object, ByRef rstrSourcePath As String, ByVal vblnForceToImport As Boolean)
'
'    Dim objComponent As Object
'#End If
'
'    With vobjVBProject.VBComponents
'
'        On Error Resume Next
'
'        Set objComponent = Nothing
'
'        Set objComponent = .Item(vobjFS.GetBaseName(rstrSourcePath))
'
'        On Error GoTo 0
'
'        If objComponent Is Nothing Then
'
'            .Import rstrSourcePath
'
'            If robjImportedFilesCol Is Nothing Then Set robjImportedFilesCol = New Collection
'
'            robjImportedFilesCol.Add vobjFS.GetFileName(rstrSourcePath)
'        Else
'            Debug.Print vobjVBProject.Name & " has already have " & vobjFS.GetBaseName(rstrSourcePath)
'
'            If vblnForceToImport Then
'
'                .Remove objComponent
'
'                Set objComponent = Nothing
'
'                .Import rstrSourcePath
'
'                If robjImportedFilesCol Is Nothing Then Set robjImportedFilesCol = New Collection
'
'                robjImportedFilesCol.Add vobjFS.GetFileName(rstrSourcePath)
'            End If
'        End If
'    End With
'End Sub
'
'
'
''''
''''
''''
'Private Sub msubCheckImportedResultAndLogging(ByVal vintImportedCount As Long, ByVal vobjImportedFilesCol As Collection, ByVal vstrVBACodesDirectoryPath As String, ByVal vobjOfficeFile As Object, Optional ByVal vblnAllowToCreateLog As Boolean = True)
'
'    Dim strMsgBoxTitle As String
'
'#If HAS_REF Then
'    Dim objFS As Scripting.FileSystemObject
'#Else
'    Dim objFS As Object
'#End If
'
'    If vintImportedCount = 0 Then
'
'        Set objFS = CreateObject("Scripting.FileSystemObject")
'
'        strMsgBoxTitle = GetTextOfStrKeyImportAllVbaCodesImportSourceFilesInto() & " " & vobjOfficeFile.Name
'
'        MsgBox "Path: " & vstrVBACodesDirectoryPath & vbNewLine & GetTextOfStrKeyImportAllVbaCodesNoVbaSourceCode(), vbInformation + vbOKOnly, strMsgBoxTitle
'    Else
'        If vobjImportedFilesCol Is Nothing Then
'
'            Set objFS = CreateObject("Scripting.FileSystemObject")
'
'            strMsgBoxTitle = GetTextOfStrKeyImportAllVbaCodesImportSourceFilesInto() & " " & vobjOfficeFile.Name
'
'            MsgBox "Path: " & vstrVBACodesDirectoryPath & vbNewLine & GetTextOfStrKeyImportAllVbaCodesFailedToImportBecauseExisted(), vbExclamation + vbOKOnly, strMsgBoxTitle
'        Else
'            If vblnAllowToCreateLog Then
'
'                msubWriteOfficeFileBuiltInPropertyLog vstrVBACodesDirectoryPath, vobjOfficeFile, vobjImportedFilesCol
'            End If
'        End If
'    End If
'End Sub
'
'
''''
''''
''''
'Private Sub msubWriteOfficeFileBuiltInPropertyLog(ByVal vstrDirectoryPath As String, ByVal vobjOfficeFile As Object, ByVal vobjImportedFilesCol As Collection)
'
'#If HAS_REF Then
'    Dim objNetwork As WshNetwork
'#Else
'    Dim objNetwork As Object
'#End If
'
'    With vobjOfficeFile.BuiltinDocumentProperties
'
'        .Item("Subject").Value = "Automatic VBA Importing"
'
'        Set objNetwork = CreateObject("WScript.Network")
'
'        .Item("Author").Value = "Auto-imported by " & objNetwork.UserName
'
'        .Item("Comments").Value = mfstrGetLogOfImportingVBACodes(vstrDirectoryPath, vobjOfficeFile, vobjImportedFilesCol)
'
''        .Item("Company").Value = Empty  ' Company
''        .Item("Manager").Value = Empty  ' Manager
'    End With
'End Sub
'
'
''''
''''
''''
'Private Function mfstrGetLogOfImportingVBACodes(ByVal vstrVBACodesDirectoryPath As String, ByVal vobjOfficeFile As Object, ByVal vobjImportedFilesCol As Collection) As String
'
'    Dim strLog As String, varFileName As Variant
'
'
'    strLog = GetTextOfStrKeyImportAllVbaCodesLogImportingFunctionModuleFileName() & ": " & mstrModuleName & vbNewLine & vbNewLine
'
'    'strLog = strLog & "Office file name: " & vobjOfficeFile.Name & vbNewLine & vbNewLine
'
'    strLog = strLog & GetTextOfStrKeyImportAllVbaCodesLogProcessedDateTime() & ": " & Format(Now(), "yyyy/mm/dd hh:mm:ss") & vbNewLine
'
'    With CreateObject("WScript.Network")
'
'        strLog = strLog & GetTextOfStrKeyImportAllVbaCodesLogCurrentUserName() & ": " & .UserName & vbNewLine
'
'        strLog = strLog & GetTextOfStrKeyImportAllVbaCodesLogCurrentComputerName() & ": " & .ComputerName & vbNewLine
'    End With
'
'    strLog = strLog & vbNewLine
'
'    strLog = strLog & GetTextOfStrKeyImportAllVbaCodesLogImportingSourceDirectory() & ": " & vstrVBACodesDirectoryPath & vbNewLine
'
'    strLog = strLog & vbNewLine
'
'    strLog = strLog & GetTextOfStrKeyImportAllVbaCodesLogCountOfImportedFiles() & ": " & CStr(vobjImportedFilesCol.Count) & vbNewLine
'
'    strLog = strLog & vbNewLine
'
'    strLog = strLog & GetTextOfStrKeyImportAllVbaCodesLogImportedList() & ":" & vbNewLine
'
'    For Each varFileName In vobjImportedFilesCol
'
'        strLog = strLog & vbTab & CStr(varFileName) & vbNewLine
'    Next
'
'    mfstrGetLogOfImportingVBACodes = strLog
'End Function
'
'
''''
'''' Using GetObject function, which code can be used from Word, PowerPoint, Access
''''
'''' In Word, Access,or Outlook, 'ThisWorkbook' object name can not be used.
''''
'Private Function GetMsExcelWorkbook(ByVal vstrBookPath As String) As Object
'
'    Dim objBook As Object
'
'    On Error Resume Next
'
'    Set objBook = Nothing
'
'    Set objBook = GetObject(vstrBookPath)
'
'    On Error GoTo 0
'
'    Set GetMsExcelWorkbook = objBook
'End Function
'
'
''**---------------------------------------------
''** Key-Value cache preparation for ImportAllVBACodes
''**---------------------------------------------
''''
'''' get string Value from STR_KEY_IMPORT_ALL_VBA_CODES_SELECT_DIRECTORY_THAT_INCLUDES_VBA_SOURCE
''''
'''' automatically-added for ImportAllVBACodes string-key-value management for standard module and class module
'Private Function GetTextOfStrKeyImportAllVbaCodesSelectDirectoryThatIncludesVbaSource() As String
'
'    If Not mblnIsStrKeyValueImportAllVBACodesDicInitialized Then
'
'        msubInitializeTextForImportAllVBACodes
'    End If
'
'    GetTextOfStrKeyImportAllVbaCodesSelectDirectoryThatIncludesVbaSource = mobjStrKeyValueImportAllVBACodesDic.Item("STR_KEY_IMPORT_ALL_VBA_CODES_SELECT_DIRECTORY_THAT_INCLUDES_VBA_SOURCE")
'End Function
'
''''
'''' get string Value from STR_KEY_IMPORT_ALL_VBA_CODES_CANCELED_SELECTING_DIRECTORY
''''
'''' automatically-added for ImportAllVBACodes string-key-value management for standard module and class module
'Private Function GetTextOfStrKeyImportAllVbaCodesCanceledSelectingDirectory() As String
'
'    If Not mblnIsStrKeyValueImportAllVBACodesDicInitialized Then
'
'        msubInitializeTextForImportAllVBACodes
'    End If
'
'    GetTextOfStrKeyImportAllVbaCodesCanceledSelectingDirectory = mobjStrKeyValueImportAllVBACodesDic.Item("STR_KEY_IMPORT_ALL_VBA_CODES_CANCELED_SELECTING_DIRECTORY")
'End Function
'
''''
'''' get string Value from STR_KEY_IMPORT_ALL_VBA_CODES_IMPORT_SOURCE_FILES_INTO
''''
'''' automatically-added for ImportAllVBACodes string-key-value management for standard module and class module
'Private Function GetTextOfStrKeyImportAllVbaCodesImportSourceFilesInto() As String
'
'    If Not mblnIsStrKeyValueImportAllVBACodesDicInitialized Then
'
'        msubInitializeTextForImportAllVBACodes
'    End If
'
'    GetTextOfStrKeyImportAllVbaCodesImportSourceFilesInto = mobjStrKeyValueImportAllVBACodesDic.Item("STR_KEY_IMPORT_ALL_VBA_CODES_IMPORT_SOURCE_FILES_INTO")
'End Function
'
''''
'''' get string Value from STR_KEY_IMPORT_ALL_VBA_CODES_NO_VBA_SOURCE_CODE
''''
'''' automatically-added for ImportAllVBACodes string-key-value management for standard module and class module
'Private Function GetTextOfStrKeyImportAllVbaCodesNoVbaSourceCode() As String
'
'    If Not mblnIsStrKeyValueImportAllVBACodesDicInitialized Then
'
'        msubInitializeTextForImportAllVBACodes
'    End If
'
'    GetTextOfStrKeyImportAllVbaCodesNoVbaSourceCode = mobjStrKeyValueImportAllVBACodesDic.Item("STR_KEY_IMPORT_ALL_VBA_CODES_NO_VBA_SOURCE_CODE")
'End Function
'
''''
'''' get string Value from STR_KEY_IMPORT_ALL_VBA_CODES_FAILED_TO_IMPORT_BECAUSE_EXISTED
''''
'''' automatically-added for ImportAllVBACodes string-key-value management for standard module and class module
'Private Function GetTextOfStrKeyImportAllVbaCodesFailedToImportBecauseExisted() As String
'
'    If Not mblnIsStrKeyValueImportAllVBACodesDicInitialized Then
'
'        msubInitializeTextForImportAllVBACodes
'    End If
'
'    GetTextOfStrKeyImportAllVbaCodesFailedToImportBecauseExisted = mobjStrKeyValueImportAllVBACodesDic.Item("STR_KEY_IMPORT_ALL_VBA_CODES_FAILED_TO_IMPORT_BECAUSE_EXISTED")
'End Function
'
''''
'''' get string Value from STR_KEY_IMPORT_ALL_VBA_CODES_LOG_IMPORTING_FUNCTION_MODULE_FILE_NAME
''''
'''' automatically-added for ImportAllVBACodes string-key-value management for standard module and class module
'Private Function GetTextOfStrKeyImportAllVbaCodesLogImportingFunctionModuleFileName() As String
'
'    If Not mblnIsStrKeyValueImportAllVBACodesDicInitialized Then
'
'        msubInitializeTextForImportAllVBACodes
'    End If
'
'    GetTextOfStrKeyImportAllVbaCodesLogImportingFunctionModuleFileName = mobjStrKeyValueImportAllVBACodesDic.Item("STR_KEY_IMPORT_ALL_VBA_CODES_LOG_IMPORTING_FUNCTION_MODULE_FILE_NAME")
'End Function
'
''''
'''' get string Value from STR_KEY_IMPORT_ALL_VBA_CODES_LOG_PROCESSED_DATE_TIME
''''
'''' automatically-added for ImportAllVBACodes string-key-value management for standard module and class module
'Private Function GetTextOfStrKeyImportAllVbaCodesLogProcessedDateTime() As String
'
'    If Not mblnIsStrKeyValueImportAllVBACodesDicInitialized Then
'
'        msubInitializeTextForImportAllVBACodes
'    End If
'
'    GetTextOfStrKeyImportAllVbaCodesLogProcessedDateTime = mobjStrKeyValueImportAllVBACodesDic.Item("STR_KEY_IMPORT_ALL_VBA_CODES_LOG_PROCESSED_DATE_TIME")
'End Function
'
''''
'''' get string Value from STR_KEY_IMPORT_ALL_VBA_CODES_LOG_IMPORTING_SOURCE_DIRECTORY
''''
'''' automatically-added for ImportAllVBACodes string-key-value management for standard module and class module
'Private Function GetTextOfStrKeyImportAllVbaCodesLogImportingSourceDirectory() As String
'
'    If Not mblnIsStrKeyValueImportAllVBACodesDicInitialized Then
'
'        msubInitializeTextForImportAllVBACodes
'    End If
'
'    GetTextOfStrKeyImportAllVbaCodesLogImportingSourceDirectory = mobjStrKeyValueImportAllVBACodesDic.Item("STR_KEY_IMPORT_ALL_VBA_CODES_LOG_IMPORTING_SOURCE_DIRECTORY")
'End Function
'
'
''''
'''' get string Value from STR_KEY_IMPORT_ALL_VBA_CODES_LOG_COUNT_OF_IMPORTED_FILES
''''
'''' automatically-added for ImportAllVBACodes string-key-value management for standard module and class module
'Private Function GetTextOfStrKeyImportAllVbaCodesLogCountOfImportedFiles() As String
'
'    If Not mblnIsStrKeyValueImportAllVBACodesDicInitialized Then
'
'        msubInitializeTextForImportAllVBACodes
'    End If
'
'    GetTextOfStrKeyImportAllVbaCodesLogCountOfImportedFiles = mobjStrKeyValueImportAllVBACodesDic.Item("STR_KEY_IMPORT_ALL_VBA_CODES_LOG_COUNT_OF_IMPORTED_FILES")
'End Function
'
''''
'''' get string Value from STR_KEY_IMPORT_ALL_VBA_CODES_LOG_IMPORTED_LIST
''''
'''' automatically-added for ImportAllVBACodes string-key-value management for standard module and class module
'Private Function GetTextOfStrKeyImportAllVbaCodesLogImportedList() As String
'
'    If Not mblnIsStrKeyValueImportAllVBACodesDicInitialized Then
'
'        msubInitializeTextForImportAllVBACodes
'    End If
'
'    GetTextOfStrKeyImportAllVbaCodesLogImportedList = mobjStrKeyValueImportAllVBACodesDic.Item("STR_KEY_IMPORT_ALL_VBA_CODES_LOG_IMPORTED_LIST")
'End Function
'
''''
'''' get string Value from STR_KEY_IMPORT_ALL_VBA_CODES_LOG_CURRENT_USER_NAME
''''
'''' automatically-added for ImportAllVBACodes string-key-value management for standard module and class module
'Private Function GetTextOfStrKeyImportAllVbaCodesLogCurrentUserName() As String
'
'    If Not mblnIsStrKeyValueImportAllVBACodesDicInitialized Then
'
'        msubInitializeTextForImportAllVBACodes
'    End If
'
'    GetTextOfStrKeyImportAllVbaCodesLogCurrentUserName = mobjStrKeyValueImportAllVBACodesDic.Item("STR_KEY_IMPORT_ALL_VBA_CODES_LOG_CURRENT_USER_NAME")
'End Function
'
''''
'''' get string Value from STR_KEY_IMPORT_ALL_VBA_CODES_LOG_CURRENT_COMPUTER_NAME
''''
'''' automatically-added for ImportAllVBACodes string-key-value management for standard module and class module
'Private Function GetTextOfStrKeyImportAllVbaCodesLogCurrentComputerName() As String
'
'    If Not mblnIsStrKeyValueImportAllVBACodesDicInitialized Then
'
'        msubInitializeTextForImportAllVBACodes
'    End If
'
'    GetTextOfStrKeyImportAllVbaCodesLogCurrentComputerName = mobjStrKeyValueImportAllVBACodesDic.Item("STR_KEY_IMPORT_ALL_VBA_CODES_LOG_CURRENT_COMPUTER_NAME")
'End Function
'
''''
'''' Initialize Key-Values which values. Avoiding to use LocalizationADOConnector instance.
''''
'''' automatically-added for ImportAllVBACodes string-key-value management for standard module and class module
'Private Sub msubInitializeTextForImportAllVBACodes()
'
'    Dim objKeyToTextDic As Object ' Scripting.Dictionary
'
'    If Not mblnIsStrKeyValueImportAllVBACodesDicInitialized Then
'
'        Set objKeyToTextDic = CreateObject("Scripting.Dictionary") ' Scripting.Dictionary
'
'        Select Case Application.LanguageSettings.LanguageID(msoLanguageIDUI)
'
'            Case 1041 ' Japanese environment
'
'                AddStringKeyValueForImportAllVBACodesByJapaneseValues objKeyToTextDic
'            Case Else
'
'                AddStringKeyValueForImportAllVBACodesByEnglishValues objKeyToTextDic
'        End Select
'        mblnIsStrKeyValueImportAllVBACodesDicInitialized = True
'    End If
'
'    Set mobjStrKeyValueImportAllVBACodesDic = objKeyToTextDic
'End Sub
'
''''
'''' Add Key-Values which values are in English for ImportAllVBACodes key-values cache
''''
'''' automatically-added for ImportAllVBACodes string-key-value management for standard module and class module
'Private Sub AddStringKeyValueForImportAllVBACodesByEnglishValues(ByRef robjDic As Object)
'
'    With robjDic
'
'        .Add "STR_KEY_IMPORT_ALL_VBA_CODES_SELECT_DIRECTORY_THAT_INCLUDES_VBA_SOURCE", "Select a directory which includes VBA source files"
'        .Add "STR_KEY_IMPORT_ALL_VBA_CODES_CANCELED_SELECTING_DIRECTORY", "Canceled selecting directory"
'        .Add "STR_KEY_IMPORT_ALL_VBA_CODES_IMPORT_SOURCE_FILES_INTO", "Import source files into"
'        .Add "STR_KEY_IMPORT_ALL_VBA_CODES_NO_VBA_SOURCE_CODE", "No VBA source codes in specified directory"
'        .Add "STR_KEY_IMPORT_ALL_VBA_CODES_FAILED_TO_IMPORT_BECAUSE_EXISTED", "Failed to import, because the VBA souce module files have already existed in the file"
'        .Add "STR_KEY_IMPORT_ALL_VBA_CODES_LOG_IMPORTING_FUNCTION_MODULE_FILE_NAME", "This Importing Function Module File Name"
'        .Add "STR_KEY_IMPORT_ALL_VBA_CODES_LOG_PROCESSED_DATE_TIME", "Processed date-time"
'        .Add "STR_KEY_IMPORT_ALL_VBA_CODES_LOG_IMPORTING_SOURCE_DIRECTORY", "Importing source directory path"
'        .Add "STR_KEY_IMPORT_ALL_VBA_CODES_LOG_COUNT_OF_IMPORTED_FILES", "Count of imported VBA module files"
'        .Add "STR_KEY_IMPORT_ALL_VBA_CODES_LOG_IMPORTED_LIST", "Imported List"
'        .Add "STR_KEY_IMPORT_ALL_VBA_CODES_LOG_CURRENT_USER_NAME", "Current user name"
'        .Add "STR_KEY_IMPORT_ALL_VBA_CODES_LOG_CURRENT_COMPUTER_NAME", "Current computer name"
'    End With
'End Sub
'
''''
'''' Add Key-Values which values are in Japanese for ImportAllVBACodes key-values cache
''''
'''' automatically-added for ImportAllVBACodes string-key-value management for standard module and class module
'Private Sub AddStringKeyValueForImportAllVBACodesByJapaneseValues(ByRef robjDic As Object)
'
'    With robjDic
'
'        .Add "STR_KEY_IMPORT_ALL_VBA_CODES_SELECT_DIRECTORY_THAT_INCLUDES_VBA_SOURCE", "VBAコードが含まれるディレクトリを選択してください"
'        .Add "STR_KEY_IMPORT_ALL_VBA_CODES_CANCELED_SELECTING_DIRECTORY", "ディレクトリの選択はキャンセルされました"
'        .Add "STR_KEY_IMPORT_ALL_VBA_CODES_IMPORT_SOURCE_FILES_INTO", "VBAソースファイルの読み込み -"
'        .Add "STR_KEY_IMPORT_ALL_VBA_CODES_NO_VBA_SOURCE_CODE", "読込対象のモジュール(ソースコード)がありません"
'        .Add "STR_KEY_IMPORT_ALL_VBA_CODES_FAILED_TO_IMPORT_BECAUSE_EXISTED", "既にモジュール(ソースコード)ファイルがVBProjectに含まれていたため、読み込みされませんでした。"
'        .Add "STR_KEY_IMPORT_ALL_VBA_CODES_LOG_IMPORTING_FUNCTION_MODULE_FILE_NAME", "読み込み機能のモジュール名"
'        .Add "STR_KEY_IMPORT_ALL_VBA_CODES_LOG_PROCESSED_DATE_TIME", "読み込み日時"
'        .Add "STR_KEY_IMPORT_ALL_VBA_CODES_LOG_IMPORTING_SOURCE_DIRECTORY", "読み込み元ディレクトリパス"
'        .Add "STR_KEY_IMPORT_ALL_VBA_CODES_LOG_COUNT_OF_IMPORTED_FILES", "読み込んだファイル数"
'        .Add "STR_KEY_IMPORT_ALL_VBA_CODES_LOG_IMPORTED_LIST", "読み込んだリスト"
'        .Add "STR_KEY_IMPORT_ALL_VBA_CODES_LOG_CURRENT_USER_NAME", "ユーザー名"
'        .Add "STR_KEY_IMPORT_ALL_VBA_CODES_LOG_CURRENT_COMPUTER_NAME", "コンピューター名"
'    End With
'End Sub
'
''''
'''' Remove Keys for ImportAllVBACodes key-values cache
''''
'''' automatically-added for ImportAllVBACodes string-key-value management for standard module and class module
'Private Sub RemoveKeysOfKeyValueImportAllVBACodes(ByRef robjDic As Object)
'
'    With robjDic
'
'        If .Exists(STR_KEY_IMPORT_ALL_VBA_CODES_SELECT_DIRECTORY_THAT_INCLUDES_VBA_SOURCE) Then   ' Check only the first key, otherwise omit items
'
'            .Remove "STR_KEY_IMPORT_ALL_VBA_CODES_SELECT_DIRECTORY_THAT_INCLUDES_VBA_SOURCE"
'            .Remove "STR_KEY_IMPORT_ALL_VBA_CODES_CANCELED_SELECTING_DIRECTORY"
'            .Remove "STR_KEY_IMPORT_ALL_VBA_CODES_IMPORT_SOURCE_FILES_INTO"
'            .Remove "STR_KEY_IMPORT_ALL_VBA_CODES_NO_VBA_SOURCE_CODE"
'            .Remove "STR_KEY_IMPORT_ALL_VBA_CODES_FAILED_TO_IMPORT_BECAUSE_EXISTED"
'            .Remove "STR_KEY_IMPORT_ALL_VBA_CODES_LOG_IMPORTING_FUNCTION_MODULE_FILE_NAME"
'            .Remove "STR_KEY_IMPORT_ALL_VBA_CODES_LOG_PROCESSED_DATE_TIME"
'            .Remove "STR_KEY_IMPORT_ALL_VBA_CODES_LOG_IMPORTING_SOURCE_DIRECTORY"
'            .Remove "STR_KEY_IMPORT_ALL_VBA_CODES_LOG_COUNT_OF_IMPORTED_FILES"
'            .Remove "STR_KEY_IMPORT_ALL_VBA_CODES_LOG_IMPORTED_LIST"
'            .Remove "STR_KEY_IMPORT_ALL_VBA_CODES_LOG_CURRENT_USER_NAME"
'            .Remove "STR_KEY_IMPORT_ALL_VBA_CODES_LOG_CURRENT_COMPUTER_NAME"
'        End If
'    End With
'End Sub
'
'
'''--VBA_Code_File--<ExportAllVBACodes.bas>--
'Attribute VB_Name = "ExportAllVBACodes"
''
''   save this VBA project all source codes into the specified directory
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Development Policy:
''       Use some duplicated codes from outside VBA code modules for keeping this module independency.
''
''   Dependency Abstract:
''       Dependent on VBIDE, Scripting.Dictionary
''       This is an independent code from all other modules
''       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access, or ADO
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Mon, 23/May/2022    Kalmclaeyd Tarclanus    Initial Creation
''       Mon, 13/Feb/2023    Kalmclaeyd Tarclanus    Supported the caption localization for both English and Japanese.
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Directives
''///////////////////////////////////////////////
'
'#Const HAS_REF = True   ' If you want to get the code-snipet support, you need to change HAS_REF into True
'
'
''///////////////////////////////////////////////
''/// Constants
''///////////////////////////////////////////////
'
'Private Const mstrModuleName As String = "ExportAllVBACodes"   ' 1st part of registry sub-key
'
'Private Const mstrLogFileBaseName As String = "OutputVBACodesLog"
'
''///////////////////////////////////////////////
''/// Declarations
''///////////////////////////////////////////////
''**---------------------------------------------
''** Key-Value cache preparation for ExportAllVBACodes
''**---------------------------------------------
'Private mobjStrKeyValueExportAllVBACodesDic As Object
'
'Private mblnIsStrKeyValueExportAllVBACodesDicInitialized As Boolean   ' Uninitialized boolean value is false in VBA specification.
'
'
''///////////////////////////////////////////////
''/// Sanity tests
''///////////////////////////////////////////////
''''
'''' local dpendent sanity test
''''
'Private Sub msubSanityTestToSaveCodesOfOpenedListViewTest()
'
'    SaveCodesOfOpenedOfficeFile "ListViewTest"
'End Sub
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''''
'''' Attention! use duplicated codes for keeping this module independency
''''
'Public Sub SaveCodesOfSelectedOfficeFile()
'
'    Dim strOfficeFilePath As String, strOutputDir As String
'
'#If HAS_REF Then
'    Dim objVBProject As VBIDE.VBProject
'#Else
'    Dim objVBProject As Object
'#End If
'
'    strOfficeFilePath = GetMacroEnableOfficeFilePathFromFileDialog()
'
'    On Error Resume Next
'
'    Set objVBProject = GetObject(strOfficeFilePath).VBProject
'
'    On Error GoTo 0
'
'    If Not objVBProject Is Nothing Then
'        'Debug.Print objVBProject.Name
'
'        strOutputDir = GetOutputCodesDirPath(objVBProject)
'
'        SaveAllVBAProjectCodesWithoutObjectModule strOutputDir, objVBProject, True
'    End If
'End Sub
'
'
''''
''''
''''
'Public Sub SaveCodesOfOpenedOfficeFile(ByVal vstrVBProjectName As String)
'
'#If HAS_REF Then
'    Dim objVBProject As VBIDE.VBProject
'#Else
'    Dim objVBProject As Object
'#End If
'
'    Dim strOutputDir As String
'
'    Set objVBProject = FindVBProjectFromVBE(vstrVBProjectName)
'
'    If Not objVBProject Is Nothing Then
'
'        strOutputDir = GetOutputCodesDirPath(objVBProject)
'
'        SaveAllVBAProjectCodesWithoutObjectModule strOutputDir, objVBProject, True
'    End If
'End Sub
'
''''
''''
''''
'#If HAS_REF Then
'Private Function FindVBProjectFromVBE(ByVal vstrVBProjectName As String) As VBIDE.VBProject
'
'    Dim objVBProject As VBIDE.VBProject, objFoundVBProject As VBIDE.VBProject
'#Else
'Private Function FindVBProjectFromVBE(ByVal vstrVBProjectName As String) As Object
'
'    Dim objVBProject As Object, objFoundVBProject As Object
'
'#End If
'    Set objFoundVBProject = Nothing
'
'    For Each objVBProject In Application.VBE.VBProjects
'
'        If StrComp(objVBProject.Name, vstrVBProjectName) = 0 Then
'
'            Set objFoundVBProject = objVBProject
'
'            Exit For
'        End If
'    Next
'
'    Set FindVBProjectFromVBE = objFoundVBProject
'End Function
'
'
''''
''''
''''
'#If HAS_REF Then
'
'Private Function GetOutputCodesDirPath(ByVal vobjVBProject As VBIDE.VBProject) As String
'
'#Else
'Private Function GetOutputCodesDirPath(ByVal vobjVBProject As Object) As String
'#End If
'
'    Dim strDir As String
'
'    With New Scripting.FileSystemObject
'
'        strDir = mfstrGetCurrentOfficeFileDirectoryPath() & "\GenerateVBACodes"
'
'        If Not .FolderExists(strDir) Then
'
'            .CreateFolder strDir
'        End If
'
'        strDir = strDir & "\Exported" & vobjVBProject.Name
'
'        If Not .FolderExists(strDir) Then
'
'            .CreateFolder strDir
'        End If
'    End With
'
'    GetOutputCodesDirPath = strDir
'End Function
'
'
'
'
''''
'''' Attention! use duplicated codes for keeping this module independency
''''
'Private Function GetMacroEnableOfficeFilePathFromFileDialog() As String
'
'    Dim strPath As String
'
'    strPath = ""
'
'    With Application.FileDialog(msoFileDialogFilePicker)
'
'        With .Filters
'
'            .Clear
'
'            .Add GetTextOfStrKeyExportAllVbaCodesFileOfExcelWordPowerpoint(), "*.xlsm;*.xls;*.docm;*.doc;*.pptm;*.ppt"
'        End With
'
'        .InitialFileName = mfstrGetCurrentOfficeFileDirectoryPath()
'
'        .Title = GetTextOfStrKeyExportAllVbaCodesSelectOfficeFileThatIncludesVba()
'
'        .AllowMultiSelect = False
'
'        If .Show() Then
'
'            strPath = .SelectedItems(1)
'        End If
'    End With
'
'    GetMacroEnableOfficeFilePathFromFileDialog = strPath
'End Function
'
'
''''
'''' Attention! use duplicated codes for keeping this module independency
''''
'Private Function mfstrGetCurrentOfficeFileDirectoryPath() As String
'
'    mfstrGetCurrentOfficeFileDirectoryPath = mfobjGetCurrentOfficeFile().Path
'End Function
'
''''
'''' Attention! use duplicated codes for keeping this module independency
''''
'Private Function mfobjGetCurrentOfficeFile() As Object
'
'    Dim objOfficeObject As Object
'
'    Set objOfficeObject = Nothing
'
'    Select Case Trim(Replace(LCase(Application.Name), "microsoft", ""))
'
'        Case "excel"
'
'            On Error Resume Next
'
'            Set objOfficeObject = VBA.CallByName(Application, "ThisWorkbook", VbGet) ' Application.ThisWorkbook
'
'            On Error GoTo 0
'
'            If objOfficeObject Is Nothing Then
'
'                Set objOfficeObject = VBA.CallByName(Application, "ActiveWorkbook", VbGet)  ' Excel.Workbook
'            End If
'
'        Case "word"
'
'            On Error Resume Next
'
'            Set objOfficeObject = VBA.CallByName(Application, "ThisDocument", VbGet)
'
'            On Error GoTo 0
'
'            If objOfficeObject Is Nothing Then
'
'                Set objOfficeObject = VBA.CallByName(Application, "ActiveDocument", VbGet)  ' Word.Document
'            End If
'
'        Case "powerpoint"
'
'            Set objOfficeObject = VBA.CallByName(Application, "ActivePresentation", VbGet)  ' PowerPoint.Presentation
'    End Select
'
'    Set mfobjGetCurrentOfficeFile = objOfficeObject
'End Function
'
''''
''''
''''
'#If HAS_REF Then
'
'Public Sub SaveAllVBAProjectCodesWithoutObjectModule(ByVal vstrDirectoryPath As String, ByVal vobjVBProject As VBIDE.VBProject, Optional ByVal vblnForceToExport As Boolean = False, Optional ByVal vblnAllowToCreateLog As Boolean = True, Optional ByVal vstrLogOutputDirectoryPath As String = "")
'
'#Else
'
'Public Sub SaveAllVBAProjectCodesWithoutObjectModule(ByVal vstrDirectoryPath As String, ByVal vobjVBProject As Object, Optional ByVal vblnForceToExport As Boolean = False, Optional ByVal vblnAllowToCreateLog As Boolean = True, Optional ByVal vstrLogOutputDirectoryPath As String = "")
'#End If
'    Dim objExportedFilesCol As Collection, intExportedCount As Long
'
'    msubExportAllModules intExportedCount, objExportedFilesCol, vstrDirectoryPath, vobjVBProject, vblnForceToExport
'
'    msubCheckExportedResultAndLogging intExportedCount, objExportedFilesCol, vstrDirectoryPath, vobjVBProject, vblnAllowToCreateLog, vstrLogOutputDirectoryPath
'End Sub
'
''''
''''
''''
'Public Sub ClearAllFilesBeforeSaveAllVBAProjectCodes(ByVal vstrDirectoryPath As String)
'
'
'#If HAS_REF Then
'    Dim objDir As Scripting.Folder, objFile As Scripting.File, objFS As Scripting.FileSystemObject
'
'    Set objFS = New Scripting.FileSystemObject
'#Else
'    Dim objDir As Object, objFile As Object, objFS As Object
'
'    Set objFS = CreateObject("Scripting.FileSystemObject")
'#End If
'
'    With objFS
'
'        If .FolderExists(vstrDirectoryPath) Then
'
'            Set objDir = .GetFolder(vstrDirectoryPath)
'
'            For Each objFile In objDir.Files
'
'                Select Case LCase(.GetExtensionName(objFile.Name))
'
'                    Case "bas", "cls", "frm", "frx"
'
'                        .DeleteFile objFile.Path, True
'
'                    Case "txt"
'
'                        If StrComp(LCase(objFile.Name), LCase(mstrLogFileBaseName) & ".txt") = 0 Then
'
'                            .DeleteFile objFile.Path, True
'                        End If
'                End Select
'            Next
'        End If
'    End With
'End Sub
'
'
''///////////////////////////////////////////////
''/// Internal Functions
''///////////////////////////////////////////////
''''
'''' Attention! use duplicated codes for keeping this module independency
''''
'Private Sub msubExportAllModules(ByRef rintExportedCount As Long, ByRef robjExportedFilesCol As Collection, ByVal vstrDirectoryPath As String, ByVal vobjVBProject As VBIDE.VBProject, Optional ByVal vblnForceToExport As Boolean = False)
'
'#If HAS_REF Then
'    Dim objDir As Scripting.Folder, objFile As Scripting.File
'    Dim objComponent As VBIDE.VBComponent, objFS As Scripting.FileSystemObject
'
'    Set objFS = New Scripting.FileSystemObject
'#Else
'    Dim objDir As Object, objFile As Object
'    Dim objComponent As Object, objFS As Object
'
'    Set objFS = CreateObject("Scripting.FileSystemObject")
'#End If
'
'    Dim strFileName As String, strDestinationPath As String, strDestinationFileName As String
'    Dim objExportedFilesCol As Collection, intExportedCount As Long
'
'    intExportedCount = 0
'
'    With objFS
'
'        Set objDir = .GetFolder(vstrDirectoryPath)
'
'        If Not objDir Is Nothing Then
'
'            For Each objComponent In vobjVBProject.VBComponents
'
'                If vbext_ct_Document <> objComponent.Type Then  ' Exclude ThisWorkbook class module and Worksheet class module
'
'                    intExportedCount = intExportedCount + 1
'
'                    With objComponent
'
'                        strDestinationFileName = .Name & mfstrGetExtensionFromComponentType(.Type)
'                    End With
'
'                    strDestinationPath = vstrDirectoryPath & "\" & strDestinationFileName
'
'                    If .FileExists(strDestinationPath) Then
'
'                        If vblnForceToExport Then
'
'                            .DeleteFile strDestinationPath, True
'
'                            objComponent.Export strDestinationPath
'
'                            If objExportedFilesCol Is Nothing Then Set objExportedFilesCol = New Collection
'
'                            objExportedFilesCol.Add strDestinationFileName
'                        End If
'                    Else
'                        ExportVBComponentWithCreatingDirectory objComponent, strDestinationPath, objFS
'
'                        If objExportedFilesCol Is Nothing Then Set objExportedFilesCol = New Collection
'
'                        objExportedFilesCol.Add strDestinationFileName
'                    End If
'
'                End If
'            Next
'        End If
'
'    End With
'
'    ' Outputs
'    rintExportedCount = intExportedCount
'
'    Set robjExportedFilesCol = objExportedFilesCol
'End Sub
'
'
''''
''''
''''
'#If HAS_REF Then
'
'Private Sub msubCheckExportedResultAndLogging(ByVal vintExportedCount As Long, ByVal vobjExportedFilesCol As Collection, ByVal vstrDirectoryPath As String, ByVal vobjVBProject As VBIDE.VBProject, Optional ByVal vblnAllowToCreateLog As Boolean = True, Optional ByVal vstrLogOutputDirectoryPath As String = "")
'
'#Else
'Private Sub msubCheckExportedResultAndLogging(ByVal vintExportedCount As Long, ByVal vobjExportedFilesCol As Collection, ByVal vstrDirectoryPath As String, ByVal vobjVBProject As Object, Optional ByVal vblnAllowToCreateLog As Boolean = True, Optional ByVal vstrLogOutputDirectoryPath As String = "")
'#End If
'
'    Dim strLogFilePath As String, strMsgBoxTitle As String, strLogDir As String
'
'#If HAS_REF Then
'    Dim objFS As Scripting.FileSystemObject
'
'    Set objFS = New Scripting.FileSystemObject
'#Else
'    Dim objFS As Object
'
'    Set objFS = CreateObject("Scripting.FileSystemObject")
'#End If
'
'    If vintExportedCount = 0 Then
'
'        strMsgBoxTitle = GetTextOfStrKeyExportAllVbaCodesExportSourceFilesFrom() & " " & objFS.GetFileName(vobjVBProject.fileName)
'
'        MsgBox "Path: " & vobjVBProject.fileName & vbNewLine & GetTextOfStrKeyExportAllVbaCodesNoVbaSourceCode(), vbInformation Or vbOKOnly, strMsgBoxTitle
'    Else
'        If vobjExportedFilesCol Is Nothing Then
'
'            strMsgBoxTitle = GetTextOfStrKeyExportAllVbaCodesExportSourceFilesFrom() & " " & objFS.GetFileName(vobjVBProject.fileName)
'
'            MsgBox "Path: " & vobjVBProject.fileName & vbNewLine & GetTextOfStrKeyExportAllVbaCodesFailedToExportBecauseExisted(), vbExclamation Or vbOKOnly, strMsgBoxTitle
'        Else
'            If vblnAllowToCreateLog Then
'
'                If vstrLogOutputDirectoryPath <> "" Then
'
'                    strLogDir = vstrLogOutputDirectoryPath
'                Else
'                    strLogDir = vstrDirectoryPath
'                End If
'
'                msubExportLogByText strLogDir, vobjVBProject, vobjExportedFilesCol
'
'                strLogFilePath = strLogDir & "\" & mstrLogFileBaseName & ".txt"
'
'                If vstrLogOutputDirectoryPath <> "" Then
'
'                    msubOpenLogFileBySomeTextEditor strLogFilePath, False
'
'                    ' open the Git repository root directory path
'                    msubOpenWinExplorerOfDirectoryPath vstrDirectoryPath
'                Else
'                    ' logging with opening the exporting log output directory path
'                    msubOpenLogFileBySomeTextEditor strLogFilePath
'                End If
'            End If
'        End If
'    End If
'End Sub
'
'
''''
''''
''''
'Private Sub msubOpenWinExplorerOfDirectoryPath(ByVal vstrDirectoryPath As String)
'
'    Const strWinExploreOutsideOperationInThisVBProject As String = "ExploreFolderWhenOpenedThatIsNothing"
'
'    On Error Resume Next
'
'    ' If this project doesn't have "ExploreFolderWhenOpenedThatIsNothing", then execute a substitute method
'
'    Application.Run strWinExploreOutsideOperationInThisVBProject, vstrDirectoryPath
'
'    If Err.Number <> 0 Then
'
'#If HAS_REF Then
'
'        Dim objShell As Shell32.Shell
'
'        Set objShell = New Shell32.Shell
'#Else
'        Dim objShell As Object
'
'        Set objShell = CreateObject("Shell.Application")
'#End If
'        With objShell
'
'            .Explore vstrDirectoryPath & "\"
'        End With
'    Else
'        Debug.Print "Called " & strWinExploreOutsideOperationInThisVBProject & " without any errors."
'    End If
'
'    On Error GoTo 0
'End Sub
'
''''
''''
''''
'Private Sub msubOpenLogFileBySomeTextEditor(ByVal vstrLogFilePath As String, Optional ByVal vblnAllowToOpenDirectoryByExplorer As Boolean = True)
'
'    Const strOpenSomeTextEditorOutsideOperationInThisVBProject As String = "OpenTextBySomeSelectedTextEditorFromWshShell"
'
'    On Error Resume Next
'
'    ' If this project doesn't have "OpenTextBySomeSelectedTextEditorFromWshShell", then execute a substitute method
'
'    Application.Run strOpenSomeTextEditorOutsideOperationInThisVBProject, vstrLogFilePath, True
'
'    If Err.Number <> 0 Then
'
'        msubOpenLogFileByNotepad vstrLogFilePath, vblnAllowToOpenDirectoryByExplorer
'    Else
'        Debug.Print "Called " & strOpenSomeTextEditorOutsideOperationInThisVBProject & " without any errors."
'
'        If vblnAllowToOpenDirectoryByExplorer Then
'
'            msubOpenWinExplorerOfDirectoryPath CreateObject("Scripting.FileSystemObject").GetParentFolderName(vstrLogFilePath)
'        End If
'    End If
'
'    On Error GoTo 0
'End Sub
'
'
''''
'''' Attention! use duplicated codes for keeping this module independency
''''
'Private Sub msubOpenLogFileByNotepad(ByVal vstrLogFilePath As String, Optional ByVal vblnAllowToOpenDirectoryByExplorer As Boolean = True)
'
'    Dim strNotepadPath As String, strCmd As String
'
'#If HAS_REF Then
'    Dim objWshWhell As IWshRuntimeLibrary.WshShell, objShell As Shell32.Shell
'
'    Set objWshWhell = New IWshRuntimeLibrary.WshShell
'#Else
'    Dim objWshWhell As Object, objShell As Object
'
'    Set objWshWhell = CreateObject("Wscript.Shell")
'#End If
'
'    strNotepadPath = "%SystemRoot%\System32\notepad.exe"
'
'    strCmd = strNotepadPath & " """ & vstrLogFilePath & """"
'
'    With objWshWhell
'
'        .Run strCmd, 1
'    End With
'
'    If vblnAllowToOpenDirectoryByExplorer Then
'
'        msubOpenWinExplorerOfDirectoryPath CreateObject("Scripting.FileSystemObject").GetParentFolderName(vstrLogFilePath)
'    End If
'End Sub
'
'
'
''''
''''
''''
'#If HAS_REF Then
'Private Sub msubExportLogByText(ByVal vstrDirectoryPath As String, ByVal vobjVBProject As VBIDE.VBProject, ByVal vobjExportedFilesCol As Collection)
'
'#Else
'Private Sub msubExportLogByText(ByVal vstrDirectoryPath As String, ByVal vobjVBProject As Object, ByVal vobjExportedFilesCol As Collection)
'#End If
'    Dim strLogPath As String
'
'    strLogPath = vstrDirectoryPath & "\" & mstrLogFileBaseName & ".txt"
'
'#If HAS_REF Then
'    Dim objFS As Scripting.FileSystemObject
'
'    Set objFS = New Scripting.FileSystemObject
'#Else
'    Dim objFS As Object
'
'    Set objFS = CreateObject("Scripting.FileSystemObject")
'#End If
'
'    With objFS
'
'        With .CreateTextFile(strLogPath, True)
'
'            .WriteLine mfstrGetLogOfExportingVBACodes(vstrDirectoryPath, vobjVBProject, vobjExportedFilesCol)
'
'            .Close
'        End With
'    End With
'End Sub
'
''''
''''
''''
'#If HAS_REF Then
'
'Private Function mfstrGetLogOfExportingVBACodes(ByVal vstrDirectoryPath As String, ByVal vobjVBProject As VBIDE.VBProject, ByVal vobjExportedFilesCol As Collection) As String
'
'#Else
'Private Function mfstrGetLogOfExportingVBACodes(ByVal vstrDirectoryPath As String, ByVal vobjVBProject As Object, ByVal vobjExportedFilesCol As Collection) As String
'#End If
'    Dim strLog As String, varFileName As Variant
'
'
'    strLog = GetTextOfStrKeyExportAllVbaCodesLogExportingFunctionModuleFileName() & ": " & mstrModuleName & vbNewLine & vbNewLine
'
'    strLog = strLog & GetTextOfStrKeyExportAllVbaCodesLogProcessedDateTime() & ": " & Format(Now(), "ddd, yyyy/mm/dd hh:mm:ss") & vbNewLine
'
'    With CreateObject("WScript.Network")
'
'        strLog = strLog & GetTextOfStrKeyExportAllVbaCodesLogCurrentUserName() & ": " & .UserName & vbNewLine
'
'        strLog = strLog & GetTextOfStrKeyExportAllVbaCodesLogCurrentComputerName() & ": " & .ComputerName & vbNewLine
'    End With
'
'    strLog = strLog & vbNewLine
'
'    strLog = strLog & GetTextOfStrKeyExportAllVbaCodesLogCountOfExportedFiles() & ": " & CStr(vobjExportedFilesCol.Count) & vbNewLine
'
'    strLog = strLog & vbNewLine
'
'    strLog = strLog & GetTextOfStrKeyExportAllVbaCodesLogExportedList() & ":" & vbNewLine
'
'    For Each varFileName In vobjExportedFilesCol
'
'        strLog = strLog & vbTab & CStr(varFileName) & vbNewLine
'    Next
'
'    mfstrGetLogOfExportingVBACodes = strLog
'End Function
'
'
'
''''
'''' get extension name
''''
'''' Attention! use duplicated codes for keeping this module independency
''''
'Private Function mfstrGetExtensionFromComponentType(ByVal venmComponentType As Variant) As String
'
'    Dim strExtension As String
'
'    strExtension = ""
'
'    Select Case venmComponentType
'
'        Case vbext_ct_StdModule     ' 1
'
'            strExtension = ".bas"
'        Case vbext_ct_ClassModule   ' 2
'
'            strExtension = ".cls"
'        Case vbext_ct_MSForm        ' 3
'
'            strExtension = ".frm"
'        Case vbext_ct_ActiveXDesigner   ' 11
'
'            strExtension = ".cls"
'        Case vbext_ct_Document      ' 100
'
'            strExtension = ".cls"   ' Worksheet object class module, Workbook object class module
'    End Select
'
'    mfstrGetExtensionFromComponentType = strExtension
'End Function
'
'
''**---------------------------------------------
''** Key-Value cache preparation for ExportAllVBACodes
''**---------------------------------------------
''''
'''' get string Value from STR_KEY_EXPORT_ALL_VBA_CODES_SELECT_OFFICE_FILE_THAT_INCLUDES_VBA
''''
'''' automatically-added for ExportAllVBACodes string-key-value management for standard module and class module
'Private Function GetTextOfStrKeyExportAllVbaCodesSelectOfficeFileThatIncludesVba() As String
'
'    If Not mblnIsStrKeyValueExportAllVBACodesDicInitialized Then
'
'        msubInitializeTextForExportAllVBACodes
'    End If
'
'    GetTextOfStrKeyExportAllVbaCodesSelectOfficeFileThatIncludesVba = mobjStrKeyValueExportAllVBACodesDic.Item("STR_KEY_EXPORT_ALL_VBA_CODES_SELECT_OFFICE_FILE_THAT_INCLUDES_VBA")
'End Function
'
''''
'''' get string Value from STR_KEY_EXPORT_ALL_VBA_CODES_FILE_OF_EXCEL_WORD_POWERPOINT
''''
'''' automatically-added for ExportAllVBACodes string-key-value management for standard module and class module
'Private Function GetTextOfStrKeyExportAllVbaCodesFileOfExcelWordPowerpoint() As String
'
'    If Not mblnIsStrKeyValueExportAllVBACodesDicInitialized Then
'
'        msubInitializeTextForExportAllVBACodes
'    End If
'
'    GetTextOfStrKeyExportAllVbaCodesFileOfExcelWordPowerpoint = mobjStrKeyValueExportAllVBACodesDic.Item("STR_KEY_EXPORT_ALL_VBA_CODES_FILE_OF_EXCEL_WORD_POWERPOINT")
'End Function
'
''''
'''' get string Value from STR_KEY_EXPORT_ALL_VBA_CODES_EXPORT_SOURCE_FILES_FROM
''''
'''' automatically-added for ExportAllVBACodes string-key-value management for standard module and class module
'Private Function GetTextOfStrKeyExportAllVbaCodesExportSourceFilesFrom() As String
'
'    If Not mblnIsStrKeyValueExportAllVBACodesDicInitialized Then
'
'        msubInitializeTextForExportAllVBACodes
'    End If
'
'    GetTextOfStrKeyExportAllVbaCodesExportSourceFilesFrom = mobjStrKeyValueExportAllVBACodesDic.Item("STR_KEY_EXPORT_ALL_VBA_CODES_EXPORT_SOURCE_FILES_FROM")
'End Function
'
''''
'''' get string Value from STR_KEY_EXPORT_ALL_VBA_CODES_NO_VBA_SOURCE_CODE
''''
'''' automatically-added for ExportAllVBACodes string-key-value management for standard module and class module
'Private Function GetTextOfStrKeyExportAllVbaCodesNoVbaSourceCode() As String
'
'    If Not mblnIsStrKeyValueExportAllVBACodesDicInitialized Then
'
'        msubInitializeTextForExportAllVBACodes
'    End If
'
'    GetTextOfStrKeyExportAllVbaCodesNoVbaSourceCode = mobjStrKeyValueExportAllVBACodesDic.Item("STR_KEY_EXPORT_ALL_VBA_CODES_NO_VBA_SOURCE_CODE")
'End Function
'
''''
'''' get string Value from STR_KEY_EXPORT_ALL_VBA_CODES_FAILED_TO_EXPORT_BECAUSE_EXISTED
''''
'''' automatically-added for ExportAllVBACodes string-key-value management for standard module and class module
'Private Function GetTextOfStrKeyExportAllVbaCodesFailedToExportBecauseExisted() As String
'
'    If Not mblnIsStrKeyValueExportAllVBACodesDicInitialized Then
'
'        msubInitializeTextForExportAllVBACodes
'    End If
'
'    GetTextOfStrKeyExportAllVbaCodesFailedToExportBecauseExisted = mobjStrKeyValueExportAllVBACodesDic.Item("STR_KEY_EXPORT_ALL_VBA_CODES_FAILED_TO_EXPORT_BECAUSE_EXISTED")
'End Function
'
''''
'''' get string Value from STR_KEY_EXPORT_ALL_VBA_CODES_LOG_EXPORTING_FUNCTION_MODULE_FILE_NAME
''''
'''' automatically-added for ExportAllVBACodes string-key-value management for standard module and class module
'Private Function GetTextOfStrKeyExportAllVbaCodesLogExportingFunctionModuleFileName() As String
'
'    If Not mblnIsStrKeyValueExportAllVBACodesDicInitialized Then
'
'        msubInitializeTextForExportAllVBACodes
'    End If
'
'    GetTextOfStrKeyExportAllVbaCodesLogExportingFunctionModuleFileName = mobjStrKeyValueExportAllVBACodesDic.Item("STR_KEY_EXPORT_ALL_VBA_CODES_LOG_EXPORTING_FUNCTION_MODULE_FILE_NAME")
'End Function
'
''''
'''' get string Value from STR_KEY_EXPORT_ALL_VBA_CODES_LOG_PROCESSED_DATE_TIME
''''
'''' automatically-added for ExportAllVBACodes string-key-value management for standard module and class module
'Private Function GetTextOfStrKeyExportAllVbaCodesLogProcessedDateTime() As String
'
'    If Not mblnIsStrKeyValueExportAllVBACodesDicInitialized Then
'
'        msubInitializeTextForExportAllVBACodes
'    End If
'
'    GetTextOfStrKeyExportAllVbaCodesLogProcessedDateTime = mobjStrKeyValueExportAllVBACodesDic.Item("STR_KEY_EXPORT_ALL_VBA_CODES_LOG_PROCESSED_DATE_TIME")
'End Function
'
''''
'''' get string Value from STR_KEY_EXPORT_ALL_VBA_CODES_LOG_COUNT_OF_EXPORTED_FILES
''''
'''' automatically-added for ExportAllVBACodes string-key-value management for standard module and class module
'Private Function GetTextOfStrKeyExportAllVbaCodesLogCountOfExportedFiles() As String
'
'    If Not mblnIsStrKeyValueExportAllVBACodesDicInitialized Then
'
'        msubInitializeTextForExportAllVBACodes
'    End If
'
'    GetTextOfStrKeyExportAllVbaCodesLogCountOfExportedFiles = mobjStrKeyValueExportAllVBACodesDic.Item("STR_KEY_EXPORT_ALL_VBA_CODES_LOG_COUNT_OF_EXPORTED_FILES")
'End Function
'
''''
'''' get string Value from STR_KEY_EXPORT_ALL_VBA_CODES_LOG_EXPORTED_LIST
''''
'''' automatically-added for ExportAllVBACodes string-key-value management for standard module and class module
'Private Function GetTextOfStrKeyExportAllVbaCodesLogExportedList() As String
'
'    If Not mblnIsStrKeyValueExportAllVBACodesDicInitialized Then
'
'        msubInitializeTextForExportAllVBACodes
'    End If
'
'    GetTextOfStrKeyExportAllVbaCodesLogExportedList = mobjStrKeyValueExportAllVBACodesDic.Item("STR_KEY_EXPORT_ALL_VBA_CODES_LOG_EXPORTED_LIST")
'End Function
'
''''
'''' get string Value from STR_KEY_EXPORT_ALL_VBA_CODES_LOG_CURRENT_USER_NAME
''''
'''' automatically-added for ExportAllVBACodes string-key-value management for standard module and class module
'Private Function GetTextOfStrKeyExportAllVbaCodesLogCurrentUserName() As String
'
'    If Not mblnIsStrKeyValueExportAllVBACodesDicInitialized Then
'
'        msubInitializeTextForExportAllVBACodes
'    End If
'
'    GetTextOfStrKeyExportAllVbaCodesLogCurrentUserName = mobjStrKeyValueExportAllVBACodesDic.Item("STR_KEY_EXPORT_ALL_VBA_CODES_LOG_CURRENT_USER_NAME")
'End Function
'
''''
'''' get string Value from STR_KEY_EXPORT_ALL_VBA_CODES_LOG_CURRENT_COMPUTER_NAME
''''
'''' automatically-added for ExportAllVBACodes string-key-value management for standard module and class module
'Private Function GetTextOfStrKeyExportAllVbaCodesLogCurrentComputerName() As String
'
'    If Not mblnIsStrKeyValueExportAllVBACodesDicInitialized Then
'
'        msubInitializeTextForExportAllVBACodes
'    End If
'
'    GetTextOfStrKeyExportAllVbaCodesLogCurrentComputerName = mobjStrKeyValueExportAllVBACodesDic.Item("STR_KEY_EXPORT_ALL_VBA_CODES_LOG_CURRENT_COMPUTER_NAME")
'End Function
'
''''
'''' Initialize Key-Values which values. Avoiding to use LocalizationADOConnector instance.
''''
'''' automatically-added for ExportAllVBACodes string-key-value management for standard module and class module
'Private Sub msubInitializeTextForExportAllVBACodes()
'
'    Dim objKeyToTextDic As Object ' Scripting.Dictionary
'
'    If Not mblnIsStrKeyValueExportAllVBACodesDicInitialized Then
'
'        Set objKeyToTextDic = CreateObject("Scripting.Dictionary") ' Scripting.Dictionary
'
'        Select Case Application.LanguageSettings.LanguageID(msoLanguageIDUI)
'
'            Case 1041 ' Japanese environment
'
'                AddStringKeyValueForExportAllVBACodesByJapaneseValues objKeyToTextDic
'            Case Else
'
'                AddStringKeyValueForExportAllVBACodesByEnglishValues objKeyToTextDic
'        End Select
'        mblnIsStrKeyValueExportAllVBACodesDicInitialized = True
'    End If
'
'    Set mobjStrKeyValueExportAllVBACodesDic = objKeyToTextDic
'End Sub
'
''''
'''' Add Key-Values which values are in English for ExportAllVBACodes key-values cache
''''
'''' automatically-added for ExportAllVBACodes string-key-value management for standard module and class module
'Private Sub AddStringKeyValueForExportAllVBACodesByEnglishValues(ByRef robjDic As Object)
'
'    With robjDic
'
'        .Add "STR_KEY_EXPORT_ALL_VBA_CODES_SELECT_OFFICE_FILE_THAT_INCLUDES_VBA", "Select a Office file which includes VBA codes"
'        .Add "STR_KEY_EXPORT_ALL_VBA_CODES_FILE_OF_EXCEL_WORD_POWERPOINT", "Office file of Excel, Word, or PowerPoint"
'        .Add "STR_KEY_EXPORT_ALL_VBA_CODES_EXPORT_SOURCE_FILES_FROM", "Export source files from"
'        .Add "STR_KEY_EXPORT_ALL_VBA_CODES_NO_VBA_SOURCE_CODE", "This office file has no VBA source code modules without object modules"
'        .Add "STR_KEY_EXPORT_ALL_VBA_CODES_FAILED_TO_EXPORT_BECAUSE_EXISTED", "Failed to export, because the VBA souce files have already existed in the specified directory"
'        .Add "STR_KEY_EXPORT_ALL_VBA_CODES_LOG_EXPORTING_FUNCTION_MODULE_FILE_NAME", "This Exporting Function Module File Name"
'        .Add "STR_KEY_EXPORT_ALL_VBA_CODES_LOG_PROCESSED_DATE_TIME", "Processed date-time"
'        .Add "STR_KEY_EXPORT_ALL_VBA_CODES_LOG_COUNT_OF_EXPORTED_FILES", "Count of exported VBA module files"
'        .Add "STR_KEY_EXPORT_ALL_VBA_CODES_LOG_EXPORTED_LIST", "Exported List"
'        .Add "STR_KEY_EXPORT_ALL_VBA_CODES_LOG_CURRENT_USER_NAME", "Current user name"
'        .Add "STR_KEY_EXPORT_ALL_VBA_CODES_LOG_CURRENT_COMPUTER_NAME", "Current computer name"
'    End With
'End Sub
'
''''
'''' Add Key-Values which values are in Japanese for ExportAllVBACodes key-values cache
''''
'''' automatically-added for ExportAllVBACodes string-key-value management for standard module and class module
'Private Sub AddStringKeyValueForExportAllVBACodesByJapaneseValues(ByRef robjDic As Object)
'
'    With robjDic
'
'        .Add "STR_KEY_EXPORT_ALL_VBA_CODES_SELECT_OFFICE_FILE_THAT_INCLUDES_VBA", "VBAコードが含まれるファイルを選択してください"
'        .Add "STR_KEY_EXPORT_ALL_VBA_CODES_FILE_OF_EXCEL_WORD_POWERPOINT", "Excelファイル、Wordファイル、PowerPointファイル"
'        .Add "STR_KEY_EXPORT_ALL_VBA_CODES_EXPORT_SOURCE_FILES_FROM", "エクスポートファイル -"
'        .Add "STR_KEY_EXPORT_ALL_VBA_CODES_NO_VBA_SOURCE_CODE", "オブジェクトモジュールコードを除いてVBAソースコードがありませんでした"
'        .Add "STR_KEY_EXPORT_ALL_VBA_CODES_FAILED_TO_EXPORT_BECAUSE_EXISTED", "VBAソースコードの出力に失敗しました。既に同名ファイルがあります。"
'        .Add "STR_KEY_EXPORT_ALL_VBA_CODES_LOG_EXPORTING_FUNCTION_MODULE_FILE_NAME", "エクスポート機能のモジュール名"
'        .Add "STR_KEY_EXPORT_ALL_VBA_CODES_LOG_PROCESSED_DATE_TIME", "エクスポート日時"
'        .Add "STR_KEY_EXPORT_ALL_VBA_CODES_LOG_COUNT_OF_EXPORTED_FILES", "エクスポートしたファイル数"
'        .Add "STR_KEY_EXPORT_ALL_VBA_CODES_LOG_EXPORTED_LIST", "エクスポートしたリスト"
'        .Add "STR_KEY_EXPORT_ALL_VBA_CODES_LOG_CURRENT_USER_NAME", "ユーザー名"
'        .Add "STR_KEY_EXPORT_ALL_VBA_CODES_LOG_CURRENT_COMPUTER_NAME", "コンピューター名"
'    End With
'End Sub
'
''''
'''' Remove Keys for ExportAllVBACodes key-values cache
''''
'''' automatically-added for ExportAllVBACodes string-key-value management for standard module and class module
'Private Sub RemoveKeysOfKeyValueExportAllVBACodes(ByRef robjDic As Object)
'
'    With robjDic
'
'        If .Exists(STR_KEY_EXPORT_ALL_VBA_CODES_SELECT_OFFICE_FILE_THAT_INCLUDES_VBA) Then   ' Check only the first key, otherwise omit items
'
'            .Remove "STR_KEY_EXPORT_ALL_VBA_CODES_SELECT_OFFICE_FILE_THAT_INCLUDES_VBA"
'            .Remove "STR_KEY_EXPORT_ALL_VBA_CODES_FILE_OF_EXCEL_WORD_POWERPOINT"
'            .Remove "STR_KEY_EXPORT_ALL_VBA_CODES_EXPORT_SOURCE_FILES_FROM"
'            .Remove "STR_KEY_EXPORT_ALL_VBA_CODES_NO_VBA_SOURCE_CODE"
'            .Remove "STR_KEY_EXPORT_ALL_VBA_CODES_FAILED_TO_EXPORT_BECAUSE_EXISTED"
'            .Remove "STR_KEY_EXPORT_ALL_VBA_CODES_LOG_EXPORTING_FUNCTION_MODULE_FILE_NAME"
'            .Remove "STR_KEY_EXPORT_ALL_VBA_CODES_LOG_PROCESSED_DATE_TIME"
'            .Remove "STR_KEY_EXPORT_ALL_VBA_CODES_LOG_COUNT_OF_EXPORTED_FILES"
'            .Remove "STR_KEY_EXPORT_ALL_VBA_CODES_LOG_EXPORTED_LIST"
'            .Remove "STR_KEY_EXPORT_ALL_VBA_CODES_LOG_CURRENT_USER_NAME"
'            .Remove "STR_KEY_EXPORT_ALL_VBA_CODES_LOG_CURRENT_COMPUTER_NAME"
'        End If
'    End With
'End Sub
'
'
'
'''--VBA_Code_File--<RemoveAllVBACodes.bas>--
'Attribute VB_Name = "RemoveAllVBACodes"
''
''   remove this VBA project all source codes without this
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Development Policy:
''       Use some duplicated codes from outside VBA code modules for keeping this module independency.
''
''   Dependency Abstract:
''       Dependent on VBIDE
''       This is an independent code from all other modules
''       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access, or ADO
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Thu, 26/May/2022    Kalmclaeyd Tarclanus    Initial Creation
''       Mon, 13/Feb/2023    Kalmclaeyd Tarclanus    Supported the caption localization for both English and Japanese.
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Directives
''///////////////////////////////////////////////
'#Const HAS_REF = False   ' If you want to the code-snipet support, you need to change HAS_REF into True
'
''///////////////////////////////////////////////
''/// Constants
''///////////////////////////////////////////////
'
'Private Const mstrModuleName As String = "RemoveAllVBACodes"   ' 1st part of registry sub-key
'
''///////////////////////////////////////////////
''/// Declarations
''///////////////////////////////////////////////
''**---------------------------------------------
''** Key-Value cache preparation for RemoveAllVBACodes
''**---------------------------------------------
'Private mobjStrKeyValueRemoveAllVBACodesDic As Object
'
'Private mblnIsStrKeyValueRemoveAllVBACodesDicInitialized As Boolean   ' Uninitialized boolean value is false in VBA specification.
'
'
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''''
'''' For example, you can remove all PowerPoint VBA codes.
'''' But, this prevents deleting this current VBA project codes from some users wrong operation
''''
'''' Note: Outlook has no VBIDE.VBProject interface, so the method Import and Export and Remove cannot be used in Outlook.
''''
'Public Sub RemoveAllVBACodesWhenThisIsNotActiveApplication(ByVal vobjOfficeFile As Object)
'
'    Dim strFilePath As String
'
'    Select Case TypeName(vobjOfficeFile)
'
'        Case "Workbook", "Document", "Presentation"
'
'            If Not mfobjGetCurrentFile() Is vobjOfficeFile Then
'
'                msubRemoveAllVBACodesWithoutThisModule vobjOfficeFile
'
'            End If
'
'        Case "DataBase"
'
'            ' The VBProject of the Access data-base is not supported to operate
'    End Select
'End Sub
'
'
''''
'''' Attention! use duplicated codes for keeping this module independency
''''
'Private Function mfobjGetCurrentFile() As Object
'
'    Dim objOfficeObject As Object
'
'    Select Case LCase(Trim(Replace(Application.Name, "Microsoft", "")))
'
'        Case "excel"
'
'            On Error Resume Next
'
'            Set objOfficeObject = VBA.CallByName(Application, "ThisWorkbook", VbGet) ' Application.ThisWorkbook
'
'            On Error GoTo 0
'
'            If objOfficeObject Is Nothing Then
'
'                Set objOfficeObject = VBA.CallByName(Application, "ActiveWorkbook", VbGet)  ' Excel.Workbook
'            End If
'
'        Case "word"
'
'            On Error Resume Next
'
'            Set objOfficeObject = VBA.CallByName(Application, "ThisDocument", VbGet)
'
'            On Error GoTo 0
'
'            If objOfficeObject Is Nothing Then
'
'                Set objOfficeObject = VBA.CallByName(Application, "ActiveDocument", VbGet)  ' Word.Document
'            End If
'
'        Case "powerpoint"
'
'            Set objOfficeObject = VBA.CallByName(Application, "ActivePresentation", VbGet)  ' PowerPoint.Presentation
'    End Select
'
'    Set mfobjGetCurrentFile = objOfficeObject
'End Function
'
'
'
''///////////////////////////////////////////////
''/// Internal Functions
''///////////////////////////////////////////////
''''
''''
''''
'Private Sub msubRemoveAllVBACodesWithoutThisModule(ByVal vobjOfficeFile As Object)
'
'    Dim objRemovedFilesCol As Collection, strToRemoveFileName As String, strLogPath As String
'
'#If HAS_REF Then
'    Dim objVBComponent As VBIDE.VBComponent, objVBProject As VBIDE.VBProject
'
'    Set objVBProject = vobjOfficeFile.VBProject
'#Else
'    Dim objVBComponent As Object, objVBProject As Object
'
'    Set objVBProject = vobjOfficeFile.VBProject
'#End If
'
'    If Not objVBProject Is Nothing Then
'
'        With objVBProject
'
'            For Each objVBComponent In .VBComponents
'
'                If objVBComponent.Type <> vbext_ct_Document Then
'
'                    strToRemoveFileName = objVBComponent.Name & mfstrGetExtensionFromComponentType(objVBComponent.Type)
'
'                    .VBComponents.Remove objVBComponent
'
'                    If objRemovedFilesCol Is Nothing Then Set objRemovedFilesCol = New Collection
'
'                    objRemovedFilesCol.Add strToRemoveFileName
'                End If
'            Next
'
'        End With
'    End If
'
'
'    If Not objRemovedFilesCol Is Nothing Then
'
'        msubWriteLogAboutRemovedFiles vobjOfficeFile, objRemovedFilesCol
'
'        strLogPath = vobjOfficeFile.Path & "\RomovedVBACodesLog.txt"
'
'        msubOpenLogFileByNotepad strLogPath
'    End If
'End Sub
'
'
''''
'''' Attention! use duplicated codes for keeping this module independency
''''
'Private Sub msubOpenLogFileByNotepad(ByVal vstrLogFilePath As String, Optional ByVal vblnAllowToOpenDirectoryByExplorer As Boolean = True)
'
'    Dim strNotepadPath As String, strCmd As String
'
'#If HAS_REF Then
'    Dim objWshWhell As IWshRuntimeLibrary.WshShell, objShell As Shell32.Shell
'
'    Set objWshWhell = New IWshRuntimeLibrary.WshShell
'#Else
'    Dim objWshWhell As Object, objShell As Object
'
'    Set objWshWhell = CreateObject("Wscript.Shell")
'#End If
'
'    strNotepadPath = "%SystemRoot%\System32\notepad.exe"
'
'    strCmd = strNotepadPath & " """ & vstrLogFilePath & """"
'
'    With objWshWhell
'
'        .Run strCmd, 1
'    End With
'
'    If vblnAllowToOpenDirectoryByExplorer Then
'
'#If HAS_REF Then
'
'        Set objShell = New Shell32.Shell
'#Else
'        Set objShell = CreateObject("Shell.Application")
'#End If
'
'        With objShell  ' At VBScript, then With CreateObject("Shell.Application")
'
'            .Explore CreateObject("Scripting.FileSystemObject").GetParentFolderName(vstrLogFilePath)
'        End With
'    End If
'End Sub
'
'
''''
''''
''''
'Private Sub msubWriteLogAboutRemovedFiles(ByVal vobjOfficeFile As Object, ByVal vobjRemovedFilesCol As Collection)
'
'    Dim strLogPath As String
'
'#If HAS_REF Then
'    Dim objFS As Scripting.FileSystemObject
'
'    Set objFS = New Scripting.FileSystemObject
'#Else
'    Dim objFS As Object
'
'    Set objFS = CreateObject("Scripting.FileSystemObject")
'#End If
'
'    strLogPath = vobjOfficeFile.Path & "\RomovedVBACodesLog.txt"
'
'    With objFS
'        With .CreateTextFile(strLogPath, True)
'
'            .WriteLine mfstrGetLogOfRemovingVBACodes(vobjOfficeFile, vobjRemovedFilesCol)
'
'            .Close
'        End With
'    End With
'End Sub
'
'
''''
''''
''''
'
'Private Function mfstrGetLogOfRemovingVBACodes(ByVal vobjOfficeFile As Object, ByVal vobjRemovedFilesCol As Collection) As String
'
'    Dim strLog As String, varFileName As Variant
'
'
'    strLog = GetTextOfStrKeyRemoveAllVbaCodesLogRemovingFunctionModuleFileName() & ": " & mstrModuleName & vbNewLine & vbNewLine
'
'    strLog = strLog & GetTextOfStrKeyRemoveAllVbaCodesLogProcessedDateTime() & ": " & Format(Now(), "yyyy/mm/dd hh:mm:ss") & vbNewLine
'
'    With CreateObject("WScript.Network")
'
'        strLog = strLog & GetTextOfStrKeyRemoveAllVbaCodesLogCurrentUserName() & ": " & .UserName & vbNewLine
'        strLog = strLog & GetTextOfStrKeyRemoveAllVbaCodesLogCurrentComputerName() & ": " & .ComputerName & vbNewLine
'
'    End With
'
'    strLog = strLog & vbNewLine
'
'    strLog = strLog & GetTextOfStrKeyRemoveAllVbaCodesLogCountOfRemovedFiles() & ": " & CStr(vobjRemovedFilesCol.Count) & vbNewLine
'
'    strLog = strLog & vbNewLine
'
'    strLog = strLog & GetTextOfStrKeyRemoveAllVbaCodesLogRemovedList() & ":" & vbNewLine
'
'    For Each varFileName In vobjRemovedFilesCol
'
'        strLog = strLog & vbTab & CStr(varFileName) & vbNewLine
'    Next
'
'
'    mfstrGetLogOfRemovingVBACodes = strLog
'End Function
'
'
''''
'''' get extension name
'''' <Duplicated function for keeping this module independency>
''''
'#If HAS_REF Then
'Private Function mfstrGetExtensionFromComponentType(ByVal venmComponentType As VBIDE.vbext_ComponentType) As String
'#Else
'Private Function mfstrGetExtensionFromComponentType(ByVal venmComponentType As Variant) As String
'#End If
'    Dim strExtension As String
'
'    strExtension = ""
'
'    Select Case venmComponentType
'
'        Case vbext_ct_StdModule     ' 1
'
'            strExtension = ".bas"
'        Case vbext_ct_ClassModule   ' 2
'
'            strExtension = ".cls"
'        Case vbext_ct_MSForm        ' 3
'
'            strExtension = ".frm"
'        Case vbext_ct_ActiveXDesigner   ' 11
'
'            strExtension = ".cls"
'        Case vbext_ct_Document      ' 100
'
'            strExtension = ".cls"   ' Worksheet object class module, Workbook object class module
'    End Select
'
'    mfstrGetExtensionFromComponentType = strExtension
'End Function
'
'
'
''**---------------------------------------------
''** Key-Value cache preparation for RemoveAllVBACodes
''**---------------------------------------------
''''
'''' get string Value from STR_KEY_REMOVE_ALL_VBA_CODES_LOG_REMOVING_FUNCTION_MODULE_FILE_NAME
''''
'''' automatically-added for RemoveAllVBACodes string-key-value management for standard module and class module
'Private Function GetTextOfStrKeyRemoveAllVbaCodesLogRemovingFunctionModuleFileName() As String
'
'    If Not mblnIsStrKeyValueRemoveAllVBACodesDicInitialized Then
'
'        msubInitializeTextForRemoveAllVBACodes
'    End If
'
'    GetTextOfStrKeyRemoveAllVbaCodesLogRemovingFunctionModuleFileName = mobjStrKeyValueRemoveAllVBACodesDic.Item("STR_KEY_REMOVE_ALL_VBA_CODES_LOG_REMOVING_FUNCTION_MODULE_FILE_NAME")
'End Function
'
''''
'''' get string Value from STR_KEY_REMOVE_ALL_VBA_CODES_LOG_PROCESSED_DATE_TIME
''''
'''' automatically-added for RemoveAllVBACodes string-key-value management for standard module and class module
'Private Function GetTextOfStrKeyRemoveAllVbaCodesLogProcessedDateTime() As String
'
'    If Not mblnIsStrKeyValueRemoveAllVBACodesDicInitialized Then
'
'        msubInitializeTextForRemoveAllVBACodes
'    End If
'
'    GetTextOfStrKeyRemoveAllVbaCodesLogProcessedDateTime = mobjStrKeyValueRemoveAllVBACodesDic.Item("STR_KEY_REMOVE_ALL_VBA_CODES_LOG_PROCESSED_DATE_TIME")
'End Function
'
''''
'''' get string Value from STR_KEY_REMOVE_ALL_VBA_CODES_LOG_COUNT_OF_REMOVED_FILES
''''
'''' automatically-added for RemoveAllVBACodes string-key-value management for standard module and class module
'Private Function GetTextOfStrKeyRemoveAllVbaCodesLogCountOfRemovedFiles() As String
'
'    If Not mblnIsStrKeyValueRemoveAllVBACodesDicInitialized Then
'
'        msubInitializeTextForRemoveAllVBACodes
'    End If
'
'    GetTextOfStrKeyRemoveAllVbaCodesLogCountOfRemovedFiles = mobjStrKeyValueRemoveAllVBACodesDic.Item("STR_KEY_REMOVE_ALL_VBA_CODES_LOG_COUNT_OF_REMOVED_FILES")
'End Function
'
''''
'''' get string Value from STR_KEY_REMOVE_ALL_VBA_CODES_LOG_REMOVED_LIST
''''
'''' automatically-added for RemoveAllVBACodes string-key-value management for standard module and class module
'Private Function GetTextOfStrKeyRemoveAllVbaCodesLogRemovedList() As String
'
'    If Not mblnIsStrKeyValueRemoveAllVBACodesDicInitialized Then
'
'        msubInitializeTextForRemoveAllVBACodes
'    End If
'
'    GetTextOfStrKeyRemoveAllVbaCodesLogRemovedList = mobjStrKeyValueRemoveAllVBACodesDic.Item("STR_KEY_REMOVE_ALL_VBA_CODES_LOG_REMOVED_LIST")
'End Function
'
''''
'''' get string Value from STR_KEY_REMOVE_ALL_VBA_CODES_LOG_CURRENT_USER_NAME
''''
'''' automatically-added for RemoveAllVBACodes string-key-value management for standard module and class module
'Private Function GetTextOfStrKeyRemoveAllVbaCodesLogCurrentUserName() As String
'
'    If Not mblnIsStrKeyValueRemoveAllVBACodesDicInitialized Then
'
'        msubInitializeTextForRemoveAllVBACodes
'    End If
'
'    GetTextOfStrKeyRemoveAllVbaCodesLogCurrentUserName = mobjStrKeyValueRemoveAllVBACodesDic.Item("STR_KEY_REMOVE_ALL_VBA_CODES_LOG_CURRENT_USER_NAME")
'End Function
'
''''
'''' get string Value from STR_KEY_REMOVE_ALL_VBA_CODES_LOG_CURRENT_COMPUTER_NAME
''''
'''' automatically-added for RemoveAllVBACodes string-key-value management for standard module and class module
'Private Function GetTextOfStrKeyRemoveAllVbaCodesLogCurrentComputerName() As String
'
'    If Not mblnIsStrKeyValueRemoveAllVBACodesDicInitialized Then
'
'        msubInitializeTextForRemoveAllVBACodes
'    End If
'
'    GetTextOfStrKeyRemoveAllVbaCodesLogCurrentComputerName = mobjStrKeyValueRemoveAllVBACodesDic.Item("STR_KEY_REMOVE_ALL_VBA_CODES_LOG_CURRENT_COMPUTER_NAME")
'End Function
'
''''
'''' Initialize Key-Values which values. Avoiding to use LocalizationADOConnector instance.
''''
'''' automatically-added for RemoveAllVBACodes string-key-value management for standard module and class module
'Private Sub msubInitializeTextForRemoveAllVBACodes()
'
'    Dim objKeyToTextDic As Object ' Scripting.Dictionary
'
'    If Not mblnIsStrKeyValueRemoveAllVBACodesDicInitialized Then
'
'        Set objKeyToTextDic = CreateObject("Scripting.Dictionary") ' Scripting.Dictionary
'
'        Select Case Application.LanguageSettings.LanguageID(msoLanguageIDUI)
'
'            Case 1041 ' Japanese environment
'
'                AddStringKeyValueForRemoveAllVBACodesByJapaneseValues objKeyToTextDic
'            Case Else
'
'                AddStringKeyValueForRemoveAllVBACodesByEnglishValues objKeyToTextDic
'        End Select
'        mblnIsStrKeyValueRemoveAllVBACodesDicInitialized = True
'    End If
'
'    Set mobjStrKeyValueRemoveAllVBACodesDic = objKeyToTextDic
'End Sub
'
''''
'''' Add Key-Values which values are in English for RemoveAllVBACodes key-values cache
''''
'''' automatically-added for RemoveAllVBACodes string-key-value management for standard module and class module
'Private Sub AddStringKeyValueForRemoveAllVBACodesByEnglishValues(ByRef robjDic As Object)
'
'    With robjDic
'
'        .Add "STR_KEY_REMOVE_ALL_VBA_CODES_LOG_REMOVING_FUNCTION_MODULE_FILE_NAME", "This Removing Function Module File Name"
'        .Add "STR_KEY_REMOVE_ALL_VBA_CODES_LOG_PROCESSED_DATE_TIME", "Processed date-time"
'        .Add "STR_KEY_REMOVE_ALL_VBA_CODES_LOG_COUNT_OF_REMOVED_FILES", "Count of removed files"
'        .Add "STR_KEY_REMOVE_ALL_VBA_CODES_LOG_REMOVED_LIST", "Removed module list"
'        .Add "STR_KEY_REMOVE_ALL_VBA_CODES_LOG_CURRENT_USER_NAME", "Current user name"
'        .Add "STR_KEY_REMOVE_ALL_VBA_CODES_LOG_CURRENT_COMPUTER_NAME", "Current computer name"
'    End With
'End Sub
'
''''
'''' Add Key-Values which values are in Japanese for RemoveAllVBACodes key-values cache
''''
'''' automatically-added for RemoveAllVBACodes string-key-value management for standard module and class module
'Private Sub AddStringKeyValueForRemoveAllVBACodesByJapaneseValues(ByRef robjDic As Object)
'
'    With robjDic
'
'        .Add "STR_KEY_REMOVE_ALL_VBA_CODES_LOG_REMOVING_FUNCTION_MODULE_FILE_NAME", "VBA削除機能のモジュール名"
'        .Add "STR_KEY_REMOVE_ALL_VBA_CODES_LOG_PROCESSED_DATE_TIME", "削除日時"
'        .Add "STR_KEY_REMOVE_ALL_VBA_CODES_LOG_COUNT_OF_REMOVED_FILES", "削除したモジュール数"
'        .Add "STR_KEY_REMOVE_ALL_VBA_CODES_LOG_REMOVED_LIST", "削除したモジュールリスト"
'        .Add "STR_KEY_REMOVE_ALL_VBA_CODES_LOG_CURRENT_USER_NAME", "ユーザー名"
'        .Add "STR_KEY_REMOVE_ALL_VBA_CODES_LOG_CURRENT_COMPUTER_NAME", "コンピューター名"
'    End With
'End Sub
'
''''
'''' Remove Keys for RemoveAllVBACodes key-values cache
''''
'''' automatically-added for RemoveAllVBACodes string-key-value management for standard module and class module
'Private Sub RemoveKeysOfKeyValueRemoveAllVBACodes(ByRef robjDic As Object)
'
'    With robjDic
'
'        If .Exists(STR_KEY_REMOVE_ALL_VBA_CODES_LOG_REMOVING_FUNCTION_MODULE_FILE_NAME) Then   ' Check only the first key, otherwise omit items
'
'            .Remove "STR_KEY_REMOVE_ALL_VBA_CODES_LOG_REMOVING_FUNCTION_MODULE_FILE_NAME"
'            .Remove "STR_KEY_REMOVE_ALL_VBA_CODES_LOG_PROCESSED_DATE_TIME"
'            .Remove "STR_KEY_REMOVE_ALL_VBA_CODES_LOG_COUNT_OF_REMOVED_FILES"
'            .Remove "STR_KEY_REMOVE_ALL_VBA_CODES_LOG_REMOVED_LIST"
'            .Remove "STR_KEY_REMOVE_ALL_VBA_CODES_LOG_CURRENT_USER_NAME"
'            .Remove "STR_KEY_REMOVE_ALL_VBA_CODES_LOG_CURRENT_COMPUTER_NAME"
'        End If
'    End With
'End Sub
'
'
'
'''--VBA_Code_File--<UTfRemoveAllVBACodesForXl.bas>--
'Attribute VB_Name = "UTfRemoveAllVBACodesForXl"
''
''   Sanity-tests for RemoveAllVBA.bas, ExportAllVBA.bas, and ImportAllVBA.bas. This test is dependent on the many other Excel-VBA codes
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on both Excel and VBIDE
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Fri, 27/May/2022    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
'
''///////////////////////////////////////////////
''/// Constants
''///////////////////////////////////////////////
'
'Private Const mstrCommonVBAGenerationSubDirectoryName As String = "GenerateVBACodes"
'
'Private Const mstrCommonTemporarySourceExportedDirectoryName As String = "TemporaryExportedCodes"
'
''///////////////////////////////////////////////
''/// Control VBE windows
''///////////////////////////////////////////////
''**---------------------------------------------
''** Open relative VBE code-panes
''**---------------------------------------------
'Private Sub OpenVBECodePaneRelativeToExportAllVBACodes()
'
'    ' Focusing on this procdure, please press [F5] key to start this. Therefore this scope is private.
'
'    On Error Resume Next ' ' If the 'ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma' isn't included in the VBA project, the following causes no syntax error.
'
'    Application.Run "ShowIDECodePaneFromModuleNamesOfThisVBProjectByModuleNamesDelimitedComma", "ExportAllVBACodes,ImportAllVBACodes,RemoveAllVBACodes"
'End Sub
'
'
''///////////////////////////////////////////////
''/// Sanity tests
''///////////////////////////////////////////////
''''
''''
''''
'Public Sub SanityTestToExportAndImportAndRemoveAllCodes()
'
'    msubSanityTestToClearFilesBeforeExportCodes
'
'    msubSanityTestToExportCodes
'
'    msubSanityTestToImportCodesWithCreatingNewBook
'
'    msubSanityTestToRemovingVBACodes
'End Sub
'
'
''***--------------------------------------------
''*** Excel dependent Export all VBA codes test
''***--------------------------------------------
''''
''''
''''
'Private Sub msubSanityTestToExportCodes()
'
'    Dim strTmpDir As String
'
'    strTmpDir = GetTemporaryCodeDirectoryPath()
'
'    SaveAllVBAProjectCodesWithoutObjectModule strTmpDir, mfobjGetCurrentOfficeFile().VBProject
'End Sub
'
''''
'''' clear VB code files and the specified log text file
''''
'Private Sub msubSanityTestToClearFilesBeforeExportCodes()
'
'    Dim strTmpDir As String
'
'    strTmpDir = GetTemporaryCodeDirectoryPath()
'
'    ClearAllFilesBeforeSaveAllVBAProjectCodes strTmpDir
'End Sub
'
'
'
''***--------------------------------------------
''*** Excel dependent Remove all VBA codes test
''***--------------------------------------------
''''
''''
''''
'Private Sub msubSanityTestToRemovingVBACodes()
'
'    Dim objBook As Excel.Workbook, strTmpDir As String, strTmpBookPath As String
'
'    Const strTmpBookBaseName As String = "TemporaryMacroBook"
'
'    strTmpDir = GetCurrentOfficeFileObject().Path & "\TemporaryBooks"
'
'    strTmpBookPath = strTmpDir & "\" & strTmpBookBaseName & ".xlsm"
'
'    Set objBook = GetWorkbookIfItExists(strTmpBookPath)
'
'    RemoveAllVBACodesWhenThisIsNotActiveApplication objBook
'End Sub
'
'
''***--------------------------------------------
''*** Other office application importing tests
''***--------------------------------------------
''''
'''' for Word test
''''
'Private Sub msubSanityTestToImportCodesIntoAWordDocument()
'
'    Dim objDocument As Object, objApplication As Object, strVBACodeDirectoryPath As String
'
'    If IsTheSpecifiedOfficeApplicationInstalledInThisComputer("Word") Then
'
'        Set objApplication = CreateObject("Word.Application")
'
'        Set objDocument = objApplication.Documents.Add()
'
'        objDocument.VBProject.Name = "AutoImportedTestVBA"
'
'        'strVBACodeDirectoryPath = GetRepositoryCodesRootDir() & "\CommonOfficeVBA"
'
'        strVBACodeDirectoryPath = GetRepositoryCodesRootDir() & "\CommonOfficeVBA\Clipboard"
'
'        LoadAllVBACodesFromDirectory strVBACodeDirectoryPath, objDocument
'
'        objApplication.Visible = True
'    Else
'        MsgBox "The Microsoft Word is not installed in this computer.", vbCritical Or vbOKOnly
'    End If
'End Sub
'
''''
'''' for PowerPoint test
''''
'Private Sub msubSanityTestToImportCodesIntoAPowerPointPresentation()
'
'    Dim objPresentation As Object, objApplication As Object, strVBACodeDirectoryPath As String
'
'    If IsTheSpecifiedOfficeApplicationInstalledInThisComputer("PowerPoint") Then
'
'        Set objApplication = CreateObject("PowerPoint.Application")
'
'        Set objApplicationObjects = VBA.CallByName(objApplication, "Presentations", VbGet)
'
'        Set objPresentation = objApplicationObjects.Add()
'
'        objPresentation.VBProject.Name = "AutoImportedTestVBA"
'
'        'strVBACodeDirectoryPath = GetRepositoryCodesRootDir() & "\CommonOfficeVBA"
'
'        strVBACodeDirectoryPath = GetRepositoryCodesRootDir() & "\CommonOfficeVBA\Clipboard"
'
'        LoadAllVBACodesFromDirectory strVBACodeDirectoryPath, objPresentation
'
'        objApplication.Visible = True
'    Else
'        MsgBox "The Microsoft PowerPoint is not installed in this computer.", vbCritical Or vbOKOnly
'    End If
'End Sub
'
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
''''
''''
''''
'Private Function GetTemporaryCodeDirectoryPath() As String
'
'    Dim objOfficeFile As Object, strTmpDir As String
'
'    Set objOfficeFile = mfobjGetCurrentOfficeFile()
'
'    strTmpDir = objOfficeFile.Path & "\" & mstrCommonVBAGenerationSubDirectoryName
'
'    With New Scripting.FileSystemObject
'
'        If Not .FolderExists(strTmpDir) Then
'
'            .CreateFolder strTmpDir
'        End If
'
'        strTmpDir = strTmpDir & "\" & mstrCommonTemporarySourceExportedDirectoryName
'
'        If Not .FolderExists(strTmpDir) Then
'
'            .CreateFolder strTmpDir
'        End If
'    End With
'
'    GetTemporaryCodeDirectoryPath = strTmpDir
'End Function
'
''''
'''' Attention! use duplicated codes for keeping this module independency
''''
'Private Function mfobjGetCurrentOfficeFile() As Object
'
'    Dim objOfficeObject As Object
'
'    Set objOfficeObject = Nothing
'
'    Select Case Trim(Replace(LCase(Application.Name), "microsoft", ""))
'
'        Case "excel"
'
'            On Error Resume Next
'
'            Set objOfficeObject = VBA.CallByName(Application, "ThisWorkbook", VbGet) ' Application.ThisWorkbook
'
'            On Error GoTo 0
'
'            If objOfficeObject Is Nothing Then
'
'                Set objOfficeObject = VBA.CallByName(Application, "ActiveWorkbook", VbGet)  ' Excel.Workbook
'            End If
'
'        Case "word"
'
'            On Error Resume Next
'
'            Set objOfficeObject = VBA.CallByName(Application, "ThisDocument", VbGet)
'
'            On Error GoTo 0
'
'            If objOfficeObject Is Nothing Then
'
'                Set objOfficeObject = VBA.CallByName(Application, "ActiveDocument", VbGet)  ' Word.Document
'            End If
'
'        Case "powerpoint"
'
'            Set objOfficeObject = VBA.CallByName(Application, "ActivePresentation", VbGet)  ' PowerPoint.Presentation
'    End Select
'
'    Set mfobjGetCurrentOfficeFile = objOfficeObject
'End Function
'
'''--VBA_Code_File--<CodeVBAReferenceSetter.bas>--
'Attribute VB_Name = "CodeVBAReferenceSetter"
''
''   set or remove the Active-X, OLE control references from the Office object which includes VBIDE.VBProject
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on VBIDE
''       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 - 2024 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Fri, 13/May/2022    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Directives
''///////////////////////////////////////////////
'#Const HAS_REF = False
'
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''**---------------------------------------------
''** Exchange references if it is installed
''**---------------------------------------------
'
'Public Sub ExchangeVBProjectWordReferenceEnabledState()
'
'    ExchangeVBProjectReferenceEnabledStateIfItInstalled GetCurrentOfficeFileObject().VBProject, "Word"
'End Sub
'
'Public Sub ExchangeVBProjectOutlookReferenceEnabledState()
'
'    ExchangeVBProjectReferenceEnabledStateIfItInstalled GetCurrentOfficeFileObject().VBProject, "Outlook"
'End Sub
'
'Public Sub ExchangeVBProjectPowerPointReferenceEnabledState()
'
'    ExchangeVBProjectReferenceEnabledStateIfItInstalled GetCurrentOfficeFileObject().VBProject, "PowerPoint"
'End Sub
'
'Public Sub ExchangeVBProjectMSAccessReferenceEnabledState()
'
'    ExchangeVBProjectReferenceEnabledStateIfItInstalled GetCurrentOfficeFileObject().VBProject, "Access"
'End Sub
'
'
''''
''''
''''
'Public Sub ExchangeVBProjectReferenceEnabledStateIfItInstalled(ByVal vobjVBProject As VBIDE.VBProject, ByVal vstrReferenceName As String)
'
'    Dim objReferenceNameToGUIDDic As Scripting.Dictionary, blnIsInstalled As Boolean
'
'    blnIsInstalled = True
'
'    Select Case vstrReferenceName
'
'        Case "PowerPoint", "Access", "Word", "Excel", "Outlook"
'
'            blnIsInstalled = IsTheSpecifiedOfficeApplicationInstalledInThisComputer(vstrReferenceName)
'    End Select
'
'    If blnIsInstalled Then
'
'        Set objReferenceNameToGUIDDic = GetVBProjectReferenceCOMComponentNameToGUIDDic(vobjVBProject)
'
'        If objReferenceNameToGUIDDic.Exists(vstrReferenceName) Then
'
'            ForceToRemoveReferences vobjVBProject, mfstrGetIntarnalKeywordFromRef(vstrReferenceName)
'
'            With GetVBProjectReferenceCOMComponentNameToGUIDDic(vobjVBProject)
'
'                If .Exists(vstrReferenceName) Then
'
'                    MsgBox mfstrGetLibraryApplicationNameFromRef(vstrReferenceName) & " reference removing has been failed.", vbOKOnly Or vbCritical
'                Else
'                    MsgBox mfstrGetLibraryApplicationNameFromRef(vstrReferenceName) & " reference has been removed.", vbOKOnly Or vbExclamation
'                End If
'            End With
'        Else
'            ForceToSetReferences vobjVBProject, mfstrGetIntarnalKeywordFromRef(vstrReferenceName)
'
'            MsgBox mfstrGetLibraryApplicationNameFromRef(vstrReferenceName) & " reference has been added.", vbOKOnly Or vbInformation
'        End If
'    Else
'        MsgBox mfstrGetLibraryApplicationNameFromRef(vstrReferenceName) & " is not installed.", vbOKOnly Or vbCritical
'    End If
'End Sub
'
''''
''''
''''
'Private Function mfstrGetLibraryApplicationNameFromRef(ByVal vstrReferenceName As String) As String
'
'    Dim strName As String
'
'    strName = ""
'
'    Select Case vstrReferenceName
'
'        Case "PowerPoint"
'
'            strName = "Microsoft PowerPoint"
'
'        Case "Access"
'
'            strName = "Microsoft Access"
'
'        Case "Word"
'
'            strName = "Microsoft Word"
'
'        Case "Excel"
'
'            strName = "Microsoft Excel"
'
'        Case "Outlook"
'
'            strName = "Microsoft Outlook"
'    End Select
'
'    mfstrGetLibraryApplicationNameFromRef = strName
'End Function
'
'
''''
''''
''''
'Private Function mfstrGetIntarnalKeywordFromRef(ByVal vstrReferenceName As String) As String
'
'    Dim strName As String
'
'    strName = ""
'
'    Select Case vstrReferenceName
'
'        Case "PowerPoint"
'
'            strName = "MSPowerPoint"
'
'        Case "Access"
'
'            strName = "MSAccess,DAO"
'
'        Case "Word"
'
'            strName = "MSWord"
'
'        Case "Excel"
'
'            strName = "MSExcel"
'
'        Case "Outlook"
'
'            strName = "MSOutlook"
'    End Select
'
'    mfstrGetIntarnalKeywordFromRef = strName
'End Function
'
'
''**---------------------------------------------
''** Add references if it is installed
''**---------------------------------------------
''''
''''
''''
'Public Sub ForceToSetFewestReferences(ByVal vobjVBProject As VBIDE.VBProject)
'
'    ForceToSetReferences vobjVBProject, "ComFewest"
'End Sub
'
''''
''''
''''
'Public Sub ForceToSetMSWordReference(ByVal vobjVBProject As VBIDE.VBProject)
'
'    ForceToSetReferences vobjVBProject, "MSWord"
'End Sub
'
'Public Sub ForceToSetMSAccessReference(ByVal vobjVBProject As VBIDE.VBProject)
'
'    ForceToSetReferences vobjVBProject, "MSAccess"
'
'End Sub
'
'Public Sub ForceToSetMSPowerPointReference(ByVal vobjVBProject As VBIDE.VBProject)
'
'    ForceToSetReferences vobjVBProject, "MSPowerPoint"
'End Sub
'
''''
''''
''''
'Public Sub ForceToSetReferences(ByVal vobjVBProject As VBIDE.VBProject, ByVal vstrSpecifiedGroup As String)
'
'    #If HAS_REF Then
'
'        Dim objReferences As VBIDE.References, objReference As VBIDE.Reference
'
'        Dim objNameToGUIDDic As Scripting.Dictionary, objFS As Scripting.FileSystemObject
'
'        Set objFS = New Scripting.FileSystemObject
'    #Else
'
'        Dim objReferences As Object, objReference As Object  ' VBIDE.Reference
'
'        Dim objNameToGUIDDic As Object, objFS As Object
'
'        Set objFS = CreateObject("Scripting.FileSystemObject")
'    #End If
'
'
'    Dim varCOMComponentName As Variant, strCOMGuidInfo As String, strElement() As String
'
'
'    Set objReferences = vobjVBProject.References
'
'    With GetRefCOMComponentNameToRefGUIDInfoDic(vstrSpecifiedGroup)
'
'        For Each varCOMComponentName In .Keys
'
'            On Error Resume Next
'
'            Set objReference = Nothing
'
'            Set objReference = objReferences.Item(CStr(varCOMComponentName))
'
'            If Err.Number <> 0 Then
'
'                If objReference Is Nothing Then
'
'                    strCOMGuidInfo = CStr(.Item(varCOMComponentName))
'
'                    strElement = Split(strCOMGuidInfo, ";")
'
'                    objReferences.AddFromGuid strElement(0), strElement(1), strElement(2)
'
'                    Debug.Print "The " & varCOMComponentName & " is added to " & objFS.GetFileName(vobjVBProject.fileName) & " References"
'                End If
'
'                Err.Clear
'            Else
'
'                'Debug.Print "The " & varCOMComponentName & " has already added to " & vobjMacroBook.Name & " References"
'            End If
'
'            Err.Clear
'
'            On Error GoTo 0
'        Next
'    End With
'End Sub
'
''**---------------------------------------------
''** remove references if it is installed
''**---------------------------------------------
'Public Sub ForceToRemoveFewestReferences(ByVal vobjVBProject As VBIDE.VBProject)
'
'    ForceToRemoveReferences vobjVBProject, "ComFewest"
'End Sub
'
''''
'''' remove Microsoft Word reference
''''
'Public Sub ForceToRemoveMSWordReference(ByVal vobjVBProject As VBIDE.VBProject)
'
'    ForceToRemoveReferences vobjVBProject, "MSWord"
'End Sub
'
''''
'''' remove Microsoft Access reference
''''
'Public Sub ForceToRemoveMSAccessReference(ByVal vobjVBProject As VBIDE.VBProject)
'
'    ForceToRemoveReferences vobjVBProject, "MSAccess"
'End Sub
'
''''
'''' remove Microsoft Access reference
''''
'Public Sub ForceToRemoveMSPowerPointReference(ByVal vobjVBProject As VBIDE.VBProject)
'
'    ForceToRemoveReferences vobjVBProject, "MSPowerPoint"
'End Sub
'
'
''''
''''
''''
'Public Sub ForceToRemoveReferences(ByVal vobjVBProject As VBIDE.VBProject, ByVal vstrSpecifiedGroup As String)
'
'    #If HAS_REF Then
'
'        Dim objReferences As VBIDE.References, objReference As VBIDE.Reference
'        Dim objFS As Scripting.FileSystemObject
'
'        Set objFS = New Scripting.FileSystemObject
'    #Else
'        Dim objReferences As Object, objReference As Object
'        Dim objFS As Object
'
'        Set objFS = CreateObject("Scripting.FileSystemObject")
'    #End If
'
'    Dim varCOMComponentName As Variant, strItem As String, strElement() As String
'
'
'    Set objReferences = vobjVBProject.References
'
'    With GetRefCOMComponentNameToRefGUIDInfoDic(vstrSpecifiedGroup)
'
'        For Each varCOMComponentName In .Keys
'
'            On Error Resume Next
'
'            Set objReference = Nothing
'
'            Set objReference = objReferences.Item(CStr(varCOMComponentName))
'
'            If Not objReference Is Nothing Then
'
'                objReferences.Remove objReference
'
'                If Err.Number <> 0 Then
'
'                    Debug.Print "The removing " & varCOMComponentName & " was failed from " & objFS.GetFileName(vobjVBProject.fileName) & " References"
'                Else
'                    Debug.Print "The " & varCOMComponentName & " is removed from " & objFS.GetFileName(vobjVBProject.fileName) & " References"
'                End If
'            End If
'
'            On Error GoTo 0
'        Next
'    End With
'End Sub
'
'
''**---------------------------------------------
''** Conversion between COM compenent name and this defined meta Reference group name
''**---------------------------------------------
''''
''''
''''
'Public Function GetCOMComponentNameToDescriptionDicFromRegGroupNames(ByVal vobjRefGroupNames As Collection) As Scripting.Dictionary
'
'    Dim varCOMComponentName As Variant, varRefGroupName As Variant, objNameToDescriptionDic As Scripting.Dictionary, strRefInfos() As String
'
'    Set objNameToDescriptionDic = New Scripting.Dictionary
'
'    For Each varRefGroupName In vobjRefGroupNames
'
'        With GetRefCOMComponentNameToRefGUIDInfoDic(varRefGroupName)
'
'            For Each varCOMComponentName In .Keys
'
'                strRefInfos = Split(.Item(varCOMComponentName), ";")
'
'                objNameToDescriptionDic.Add varCOMComponentName, strRefInfos(3)
'            Next
'        End With
'    Next
'
'    Set GetCOMComponentNameToDescriptionDicFromRegGroupNames = objNameToDescriptionDic
'End Function
'
''**---------------------------------------------
''** output references information as either Dictionary of Collection
''**---------------------------------------------
''''
''''
''''
'Public Function GetVBProjectReferenceCOMComponentNameToGUIDDic(ByVal vobjVBProject As VBIDE.VBProject) As Scripting.Dictionary
'
'#If HAS_REF Then
'
'    Dim objReference As VBIDE.Reference
'#Else
'    Dim objReference As Object
'#End If
'    Dim objDic As Scripting.Dictionary
'
'
'    Set objDic = New Scripting.Dictionary
'
'    For Each objReference In vobjVBProject.References
'
'        With objReference
'
'            objDic.Add .Name, .GUID
'        End With
'    Next
'
'    Set GetVBProjectReferenceCOMComponentNameToGUIDDic = objDic
'End Function
'
''''
''''
''''
'Public Function GetVBProjectReferenceTableCol(ByVal vobjVBProject As VBIDE.VBProject) As Collection
'
'#If HAS_REF Then
'
'    Dim objReference As VBIDE.Reference
'#Else
'    Dim objReference As Object
'#End If
'
'    Dim objDTCol As Collection, objRowCol As Collection
'
'
'    Set objDTCol = New Collection
'
'    For Each objReference In vobjVBProject.References
'
'        With objReference
'
'            Set objRowCol = New Collection
'
'            objRowCol.Add .Name
'
'            objRowCol.Add CStr(.Major) & "." & CStr(.Minor)
'
'            objRowCol.Add .Description
'
'            objRowCol.Add .GUID
'
'            'Debug.Print "Name := " & .Name & ", Type := " & .Type & ", Major := " & .Major & ", Minor := " & .Minor & ", GUID := " & .GUID
'        End With
'
'        objDTCol.Add objRowCol
'    Next
'
'    SortDataTableCollection objDTCol
'
'    Set GetVBProjectReferenceTableCol = objDTCol
'End Function
'
'
''**---------------------------------------------
''** output references information to immediate window
''**---------------------------------------------
''''
'''' Output the VBA references to the imidiate window (Debug.Print)
''''
'Public Sub DebugPringSpecifiedVBProjectReferenceTable(ByVal vobjVBProject As VBIDE.VBProject)
'
'#If HAS_REF Then
'
'    Dim objReference As VBIDE.Reference
'#Else
'    Dim objReference As Object
'#End If
'
'    For Each objReference In vobjVBProject.References
'
'        With objReference
'
'            Debug.Print "Name := " & .Name & ", Type := " & .Type & ", Major := " & .Major & ", Minor := " & .Minor & ", GUID := " & .GUID & ", Description := " & .Description
'        End With
'    Next
'
'    ' the generated code-string by the below are able to be used by GetFewestCOMComponetNameToRefGUIDInfoDic procedure
'    Debug.Print "For code-generation" & vbNewLine
'
'    For Each objReference In vobjVBProject.References
'
'        With objReference
'
'            Debug.Print ".Add """ & .Name & """, """ & .GUID & ";" & .Major & ";" & .Minor & """"
'        End With
'    Next
'
'    'Debug.Print TypeName(objProject.References.Item("VBIDE"))
'End Sub
'
''''
''''
''''
'Public Sub SetUpRecommendingVbaReferences(ByVal vobjOfficeObject As Object)
'
'#If HAS_REF Then
'
'    Dim objVBProject As VBIDE.VBProject
'#Else
'    Dim objVBProject As Object
'#End If
'
'    Set objVBProject = vobjOfficeObject.VBProject
'
'    With objVBProject.References
'
'        On Error Resume Next
'
'        ' About ComFewest
'
'        .AddFromGuid "{420B2830-E718-11CF-893D-00A0C9054228}", 1, 0  ' Scripting - Microsoft Scripting Runtime
'
'        .AddFromGuid "{3F4DACA7-160D-11D2-A8E9-00104B365C9F}", 5, 5  ' VBScript_RegExp_55 - Microsoft VBScript Regular Expressions 5.5
'
'        .AddFromGuid "{F935DC20-1CF0-11D0-ADB9-00C04FD58A0B}", 1, 0  ' IWshRuntimeLibrary -  Windows Script Host Object Model
'
'        .AddFromGuid "{50A7E9B0-70EF-11D1-B75A-00A0C90564FE}", 1, 0  ' Shell32 - Microsoft Shell Controls And Automation
'
'        .AddFromGuid "{EAB22AC0-30C1-11CF-A7EB-0000C05BAE0B}", 1, 1  ' SHDocVw - Microsoft Internet Controls
'
'        ' About VBIDE
'
'        .AddFromGuid "{0002E157-0000-0000-C000-000000000046}", 5, 3  ' VBIDE - Microsoft Visual Basic for Applications Extensibility 5.3
'
'        ' About Outlook
'
'        .AddFromGuid "{00062FFF-0000-0000-C000-000000000046}", 9, 6  ' Outlook - Microsoft Outlook 16.0 Object Library
'
'        ' About ADO
'
'        .AddFromGuid "{B691E011-1797-432E-907A-4D8C69339129}", 6, 1  ' ADODB - Microsoft ActiveX Data Objects 6.1 Library
'
'        .AddFromGuid "{00000300-0000-0010-8000-00AA006D2EA4}", 6, 0  ' ADOR - Microsoft ActiveX Data Objects Recordset 6.0 Library
'
'        .AddFromGuid "{00000600-0000-0010-8000-00AA006D2EA4}", 6, 0  ' ADOX - Microsoft ADO Ext. 6.0 for DDL and Security
'
'        ' About MSForms
'
'        .AddFromGuid "{0D452EE1-E08F-101A-852E-02608C4D0BB4}", 2, 0  ' MSForms - Microsoft Forms 2.0 Object Library
'
'        ' About MSComctlLib
'
'        .AddFromGuid "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}", 2, 2  ' MSComctlLib - Microsoft Windows Common Controls 6.0 (SP6)
'
'        ' About WMI
'
'        .AddFromGuid "{565783C6-CB41-11D1-8B02-00600806D9B6}", 1, 2  ' WMI - Microsoft WMI Scripting V1.2 Library
'
'        On Error GoTo 0
'    End With
'
'End Sub
'
'
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
''''
''''
''''
'Private Function GetFewestCOMComponetNameToRefGUIDInfoDic() As Object
'
'    Set GetFewestCOMComponetNameToRefGUIDInfoDic = GetRefCOMComponentNameToRefGUIDInfoDic("ComFewest")
'End Function
'
''''
''''
''''
'#If HAS_REF Then
'
'Private Function GetRefCOMComponentNameToRefGUIDInfoDic(ByVal vstrSpecifiedGroup As String) As Scripting.Dictionary
'
'    Dim objDic As Scripting.Dictionary: Set objDic = New Scripting.Dictionary
'#Else
'
'Private Function GetRefCOMComponentNameToRefGUIDInfoDic(ByVal vstrSpecifiedGroup As String) As Object
'
'    Dim objDic As Object: Set objDic = CreateObject("Scripting.Dictionary")
'#End If
'
'    Dim varGroupName As Variant
'
'    For Each varGroupName In Split(vstrSpecifiedGroup, ",")
'
'        With objDic
'
'            Select Case CStr(varGroupName)
'
'                Case "ComFewest"
'
'                    .Add "Scripting", "{420B2830-E718-11CF-893D-00A0C9054228};1;0;Microsoft Scripting Runtime"
'
'                    .Add "VBScript_RegExp_55", "{3F4DACA7-160D-11D2-A8E9-00104B365C9F};5;5;Microsoft VBScript Regular Expressions 5.5" ' RegExp (regular expression) object
'
'                    .Add "IWshRuntimeLibrary", "{F935DC20-1CF0-11D0-ADB9-00C04FD58A0B};1;0;Windows Script Host Object Model" ' IWshRuntimeLibrary, example, IWshRuntimeLibrary.WshShell
'
'                    .Add "Shell32", "{50A7E9B0-70EF-11D1-B75A-00A0C90564FE};1;0;Microsoft Shell Controls And Automation"    ' Shell32, example, Shell32.Shell (CreateObject("Shell.Application"))
'
'                    .Add "SHDocVw", "{EAB22AC0-30C1-11CF-A7EB-0000C05BAE0B};1;1;Microsoft Internet Controls"    ' Microsoft Internet Controls (using InternetExplorer class in SHDocVw.dll)
'
'                Case "VBIDE"
'
'                    .Add "VBIDE", "{0002E157-0000-0000-C000-000000000046};5;3;Microsoft Visual Basic for Applications Extensibility 5.3"
'
'                Case "MSExcel"
'
'                    If IsTheSpecifiedOfficeApplicationInstalledInThisComputer("Excel") Then
'
'                        .Add "Excel", "{00020813-0000-0000-C000-000000000046};1;9;Microsoft Excel 16.0 Object Library"
'                    End If
'
'                Case "MSWord"
'
'                    If IsTheSpecifiedOfficeApplicationInstalledInThisComputer("Word") Then
'
'                        .Add "Word", "{00020905-0000-0000-C000-000000000046};8;5;Microsoft Word 16.0 Object Library"
'                    End If
'
'                Case "MSAccess"
'
'                    If IsTheSpecifiedOfficeApplicationInstalledInThisComputer("Access") Then
'
'                        .Add "Access", "{4AFFC9A0-5F99-101B-AF4E-00AA003F0F07};9;0;Microsoft Access 16.0 Object Library"
'                    End If
'
'                Case "MSPowerPoint"
'
'                    If IsTheSpecifiedOfficeApplicationInstalledInThisComputer("PowerPoint") Then
'
'                        .Add "PowerPoint", "{91493440-5A91-11CF-8700-00AA0060263B};2;12;Microsoft PowerPoint 16.0 Object Library"
'                    End If
'
'                Case "MSOutlook"
'
'                    If IsTheSpecifiedOfficeApplicationInstalledInThisComputer("Outlook") Then
'
'                        .Add "Outlook", "{00062FFF-0000-0000-C000-000000000046};9;6;Microsoft Outlook 16.0 Object Library"
'                    End If
'
'                Case "ADO"
'
'                    .Add "ADODB", "{B691E011-1797-432E-907A-4D8C69339129};6;1;Microsoft ActiveX Data Objects 6.1 Library"
'                    .Add "ADOR", "{00000300-0000-0010-8000-00AA006D2EA4};6;0;Microsoft ActiveX Data Objects Recordset 6.0 Library"
'
'                Case "ADOX" ' for use ADOX.Catalog - Microsoft ADO Ext 6.0 for DDL and Security
'
'                    .Add "ADOX", "{00000600-0000-0010-8000-00AA006D2EA4};6;0;Microsoft ADO Ext. 6.0 for DDL and Security"
'
'                Case "DAO"
'
'                    ' DAO is able to use without purchasing Microsoft Access by downloading 'Microsoft 365 Access Runtime'
'#If Win64 Then
'                    .Add "DAO", "{4AC9E1DA-5BAD-4AC7-86E3-24F4CDCECA28};12;0;Microsoft Office 16.0 Access database engine Object Library"
'#Else
'                    ' Need for the Microsoft DAO 3.6 Object Library at 32 bit Windows
'#End If
'
'                Case "MSForms"  ' for example, UserForm, TextBox, ListBox, ComboBox
'
'                    .Add "MSForms", "{0D452EE1-E08F-101A-852E-02608C4D0BB4};2;0;Microsoft Forms 2.0 Object Library"
'
'                Case "MSComctlLib"  ' for example, ListView
'
'                    .Add "MSComctlLib", "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1};2;2;Microsoft Windows Common Controls 6.0 (SP6)"
'
'                Case "msWMI"    ' WMI
'
'                    .Add "WbemScripting", "{565783C6-CB41-11D1-8B02-00600806D9B6};1;2;Microsoft WMI Scripting V1.2 Library"
'            End Select
'        End With
'    Next
'
'    Set GetRefCOMComponentNameToRefGUIDInfoDic = objDic
'End Function
'
'
'
'
'
'
'
'''--VBA_Code_File--<UTfCodeVBAReferenceSetterForXl.bas>--
'Attribute VB_Name = "UTfCodeVBAReferenceSetterForXl"
''
''   Sanity test to set or remove the Active-X, OLE control references from the Office object which includes VBIDE.VBProject
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on both VBIDE and Excel
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2024 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Sun, 21/Jan/2024    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
'
''///////////////////////////////////////////////
''/// Directives
''///////////////////////////////////////////////
'#Const HAS_REF = False
'
'
''///////////////////////////////////////////////
''/// Sanity tests
''///////////////////////////////////////////////
''''
''''
''''
'Private Sub msubSanityTestToSetUpRecommendedVbaReferences()
'
'#If HAS_REF Then
'
'    Dim objBook As Excel.Workbook, objVBProject As VBProject
'#Else
'    Dim objBook As Object, objVBProject As Object
'#End If
'
'    Set objBook = ThisWorkbook.Application.Workbooks.Add()
'
'    SetUpRecommendingVbaReferences objBook
'
'    Set objVBProject = objBook.VBProject
'
'    objVBProject.Name = "AVbaRefSetTest" & Format(Now(), "yyyymmdd")
'End Sub
'
''''
'''' When this VBA module HAS_REF = False and the ThisWorkbook has not COM component references, this can be executed only when the VBIDE is permitted.
''''
'Private Sub msubSanityTestToSetUpRecommendingVbaReferencesWhenThisWorkbookHasNoVbaCOMComponentReferences()
'
'    SetUpRecommendingVbaReferences ThisWorkbook
'End Sub
'
'
'
'''--VBA_Code_File--<CodeBookPath.bas>--
'Attribute VB_Name = "CodeBookPath"
''
''   Solute generally the macro-book path to be imported source-files
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Wed,  4/Jan/2023    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
'
''///////////////////////////////////////////////
''/// Constants
''///////////////////////////////////////////////
'
'Private Const mstrNewMacroBookBaseName As String = "NewMacroTransferedBook"
'
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''''
''''
''''
'Public Function GetNewMacroBookPathUnderThisWorkbookSubDir(ByVal vstrNewMacroBookBaseName As String) As String
'
'    GetNewMacroBookPathUnderThisWorkbookSubDir = mfstrGetMacroBookPathWithSpecifiedSubDirAndCreatingDirectoryIfNeeded(GetParentDirectoryPathOfCurrentOfficeFile(), vstrNewMacroBookBaseName)
'End Function
'
'
'Public Function GetMacroBookPathUnderThisWorkbookSubDir(ByVal vstrSubDirName As String, ByVal vstrMacroBookFileName As String) As String
'
'    Dim strDir As String, strPath As String
'
'    strDir = GetParentDirectoryPathOfCurrentOfficeFile() & "\" & vstrSubDirName
'
'    ForceToCreateDirectory strDir
'
'    strPath = strDir & "\" & vstrMacroBookFileName
'
'    GetMacroBookPathUnderThisWorkbookSubDir = strPath
'End Function
'
'
''///////////////////////////////////////////////
''/// Internal function
''///////////////////////////////////////////////
''''
''''
''''
'Private Function mfstrGetMacroBookPathWithSpecifiedSubDirAndCreatingDirectoryIfNeeded(ByVal vstrDirectoryPath As String, ByVal vstrNewMacroBookBaseName As String) As String
'
'    mfstrGetMacroBookPathWithSpecifiedSubDirAndCreatingDirectoryIfNeeded = GetMacroBookPathWithCreatingDirectoryIfNeeded(vstrDirectoryPath, "GeneratedMacroBooks", vstrNewMacroBookBaseName)
'End Function
'
'
'Public Function GetMacroBookPathWithCreatingDirectoryIfNeeded(ByVal vstrDirectoryPath As String, ByVal vstrSubDirName As String, ByVal vstrNewMacroBookBaseName As String) As String
'
'    Dim strBookFileName As String, strDir As String
'
'    strDir = vstrDirectoryPath & "\" & vstrSubDirName
'
'    ForceToCreateDirectory strDir
'
'    strBookFileName = vstrNewMacroBookBaseName & ".xlsm"
'
'    GetMacroBookPathWithCreatingDirectoryIfNeeded = strDir & "\" & strBookFileName
'End Function
'''--VBA_Code_File--<ModifyExternalVBACode.bas>--
'Attribute VB_Name = "ModifyExternalVBACode"
''
''   Modify external VBA source code
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access, or ADO
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Fri,  3/Jun/2022    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
'
''///////////////////////////////////////////////
''/// Constants
''///////////////////////////////////////////////
'Private Const mstrModuleKey As String = "ModifyExternalVBACode"   ' 1st part of registry sub-key
'
'
''///////////////////////////////////////////////
''/// Declarations
''///////////////////////////////////////////////
''**---------------------------------------------
''** variables to detect a range of the header comments
''**---------------------------------------------
'Private Type HeaderCommentRangeDetectionState
'
'    objRegExp As VBScript_RegExp_55.RegExp
'
'    blnIsPreviousLineHeaderComment As Boolean
'
'    blnIsCurrentLineHeaderComment As Boolean
'
'
'    blnIsToBeRemoved As Boolean
'
'    blnIsFinishedSearching As Boolean
'End Type
'
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''''
''''
''''
'''' <Argument>rvarLines: Output</Argument>
'''' <Argument>vstrSourceFilePath: Input</Argument>
'Public Sub GetSourceCodeLinesArray(ByRef rvarLines As Variant, ByVal vstrSourceFilePath As String)
'
'    Dim intFileNumber As Integer
'    Dim bytBuf() As Byte, strAllLines As String, strOneLine(0) As String
'
'     ' High speed text loading
'    intFileNumber = FreeFile
'
'    Open vstrSourceFilePath For Binary As #intFileNumber
'
'    ReDim bytBuf(1 To LOF(intFileNumber))
'
'    Get #intFileNumber, , bytBuf
'
'    Close #intFileNumber
'
'
'    strAllLines = StrConv(bytBuf, vbUnicode)
'
'    If InStr(1, strAllLines, vbCrLf) > 0 Then
'
'        rvarLines = Split(strAllLines, vbCrLf)
'
'    ElseIf InStr(1, strAllLines, vbLf) > 0 Then
'
'        rvarLines = Split(strAllLines, vbLf)
'
'    ElseIf InStr(1, strAllLines, vbCr) > 0 Then
'
'        rvarLines = Split(strAllLines, vbCr)
'    Else
'        ' seems to be only one line
'        strOneLine(0) = strAllLines
'
'        rvarLines = strOneLine
'    End If
'End Sub
'
'
''''
''''
''''
'Public Sub RemoveHeaderBlockFromExternalVBACodeFile(ByVal vstrOrignalCodePath As String, ByVal vstrOutCodePath As String)
'
'    Dim varDataList As Variant, intRowMax As Long, strLine As String
'    Dim i As Long
'
'    Dim udtHeaderCommentRangeDetectionState As HeaderCommentRangeDetectionState
'
'    Dim strOutCodes As String, objTS As Scripting.TextStream
'
'
'    GetSourceCodeLinesArray varDataList, vstrOrignalCodePath
'
'    intRowMax = UBound(varDataList)
'
'    msubInitializeHeaderCommentRangeDetectionState udtHeaderCommentRangeDetectionState
'
'    strOutCodes = ""
'
'    For i = 1 To intRowMax
'
'        strLine = varDataList(i - 1)
'
'        With udtHeaderCommentRangeDetectionState
'
'            If i > 1 Then
'
'                If Not .blnIsToBeRemoved Then   ' If the previous line is to be removed,
'
'                    strOutCodes = strOutCodes & vbCrLf
'                End If
'            End If
'
'            If Not .blnIsFinishedSearching Then
'
'                msubReadHeaderCommentRangeDetectionState strLine, udtHeaderCommentRangeDetectionState
'            End If
'
'            If Not .blnIsToBeRemoved Then
'
'                strOutCodes = strOutCodes & strLine
'            End If
'        End With
'    Next
'
'    ' Output edited code
'
'    OutputEditedCodes strOutCodes, vstrOutCodePath
'End Sub
'
'
''''
''''
''''
'Public Sub OutputEditedCodes(ByVal vstrOutCodes As String, ByVal vstrOutCodePath As String)
'
'    With New Scripting.FileSystemObject
'
'        ForceToCreateDirectory .GetParentFolderName(vstrOutCodePath)
'
'        With .CreateTextFile(vstrOutCodePath, True)
'
'            .Write vstrOutCodes
'
'            .Close
'        End With
'    End With
'End Sub
'
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
''''
'''' initialize a variable of HeaderCommentRangeDetectionState type
''''
'Private Sub msubInitializeHeaderCommentRangeDetectionState(ByRef rudtHeaderCommentRangeDetectionState As HeaderCommentRangeDetectionState)
'
'    With rudtHeaderCommentRangeDetectionState
'
'        Set .objRegExp = New VBScript_RegExp_55.RegExp
'
'        With .objRegExp
'            .Global = False
'            '.Pattern = "' |'\t"
'            .Pattern = "'"
'        End With
'
'        .blnIsPreviousLineHeaderComment = False
'
'        .blnIsCurrentLineHeaderComment = False
'
'        .blnIsFinishedSearching = False
'
'        .blnIsToBeRemoved = False
'    End With
'
'End Sub
'
'
'
''''
'''' read code-line with a variable of HeaderCommentRangeDetectionState type
''''
'Private Sub msubReadHeaderCommentRangeDetectionState(ByVal vstrLine As String, ByRef rudtHeaderCommentRangeDetectionState As HeaderCommentRangeDetectionState)
'    Dim objMatches As VBScript_RegExp_55.MatchCollection, objMatch As VBScript_RegExp_55.Match
'
'    With rudtHeaderCommentRangeDetectionState
'
'        .blnIsToBeRemoved = False
'
'        If Not .blnIsFinishedSearching Then
'
'            .blnIsCurrentLineHeaderComment = False
'
'            If .objRegExp.Test(vstrLine) Then
'
'                Set objMatches = .objRegExp.Execute(vstrLine)
'
'                Set objMatch = objMatches.Item(0)
'
'                If objMatch.FirstIndex = 0 Then
'
'                    .blnIsCurrentLineHeaderComment = True
'
'                    .blnIsToBeRemoved = True
'                End If
'            End If
'
'            If .blnIsPreviousLineHeaderComment And Not .blnIsCurrentLineHeaderComment Then
'
'                .blnIsFinishedSearching = True
'            End If
'
'            ' copy to previous line state
'            .blnIsPreviousLineHeaderComment = .blnIsCurrentLineHeaderComment
'        End If
'    End With
'End Sub
'
'''--VBA_Code_File--<CodeVBAFunctions.bas>--
'Attribute VB_Name = "CodeVBAFunctions"
''
''   collect all procedures, functions, properties of this VBA project source codes
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on VBIDE
''       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Wed,  1/Jun/2022    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Directives
''///////////////////////////////////////////////
'#Const HAS_REF = True
'
''///////////////////////////////////////////////
''/// Constants
''///////////////////////////////////////////////
'Private Const VBAAnalysisAllCodeFunctionsBookBaseName = "VBACodeAllFunctionsInformation"
'
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''''
'''' make VBProject all procedures information from the specified VBA macro Office file
''''
'Public Sub MakeVBProjectAllComponentsInfoTableForSelectedVBMacroFileByFileDialog()
'
'    Dim strSelectedPath As String
'
'    strSelectedPath = GetMacroEnabledFilePathByApplicationFileDialog(GetTextOfStrKeyInterfaceCallForUiFileDialogTitleSelectMacroOfficeFileForListUpFunctions(), GetTextOfStrKeyInterfaceCallForUiTextMacroEnabledFile() & "(*.xlsm;*.xls;*.docm;*.doc;*.pptm;*.ppt),*.xlsm;*.xls;*.docm;*.doc;*.pptm;*.ppt", "MakeVBProjectAllProcedureInfo")
'
'    If strSelectedPath <> "" Then
'
'        ' prevent executing Workbook_Open event handler by setting True of vblnOpenWithPreventingEvents
'
'        MakeVBProjectAllProceduresTableToLog GetVBProjectFromFilePath(strSelectedPath, True, True, False), GenerateNewBookWithDeletingOldSheets
'    End If
'End Sub
'
'
''''
''''
''''
'Public Sub MakeVBProjectAllProceduresTableToLog(ByVal vobjVBProject As VBIDE.VBProject, _
'        Optional ByVal venmLogDTVisualizationType As LogDTVisualizationType = LogDTVisualizationType.GenerateNewBookWithDeletingOldSheets)
'
'
'    Dim objDTCol As Collection, objModuleNamesImplementedFunctionsCol As Collection
'
'    msubCollectAllVBProceduresInformation objDTCol, objModuleNamesImplementedFunctionsCol, vobjVBProject
'
'    Select Case venmLogDTVisualizationType
'
'        Case LogDTVisualizationType.GenerateCSVFile
'
'            msubOutputCollectedAllVBProceduresInformationToCSV objDTCol, objModuleNamesImplementedFunctionsCol, vobjVBProject
'
'        Case LogDTVisualizationType.GenerateNewBookWithDeletingOldSheets
'
'            ' Prevent occuring a compile error when the current VB project doesn't include some Excel book output codes
'            ' Note: If called outside function has have some VB syntax errors, the VBA interpreter may have been unstable. When you confirm the unstable state of Excel VBE, I reccomend to change the direct call with avoiding interface-call
'
'
'            'CallBackInterfaceByThreeArguments "OutputCollectedAllVBProceduresInformationToExcelBook", objDTCol, objModuleNamesImplementedFunctionsCol, vobjVBProject, vobjVBProject.FileName
'
'            OutputCollectedAllVBProceduresInformationToExcelBook objDTCol, objModuleNamesImplementedFunctionsCol, vobjVBProject
'    End Select
'End Sub
'
'
''''
''''
''''
'Public Function GetLoggingFileBaseNameForVBProjectAllProcedureInfo(ByVal vobjTargetVBProject As VBIDE.VBProject) As String
'
'    Dim strBaseName As String
'    Dim objCurrentFile As Object, objThisVBProject As VBIDE.VBProject    ' Excel.Workbook, Word.Document, PowerPoint.Presentation, Access.CurrentProject
'
'    strBaseName = VBAAnalysisAllCodeFunctionsBookBaseName
'
'    Set objCurrentFile = Nothing
'
'    Set objThisVBProject = Nothing
'
'    On Error Resume Next
'
'    Set objCurrentFile = GetCurrentOfficeFileObject()
'
'    If Not objCurrentFile Is Nothing Then
'
'        Set objThisVBProject = objCurrentFile.VBProject ' This is supported from Excel, Word, PowerPoint
'
'        If objThisVBProject Is Nothing Then
'
'            ' For microsoft Access, at the moment Access 2021, 2019 is forbidden to use VBIDE.VBProject
'            Set objThisVBProject = objCurrentFile.Application.VBE.ActiveVBProject
'        End If
'
'        If Not objThisVBProject Is Nothing Then
'
'            If Not objThisVBProject Is vobjTargetVBProject Then
'
'                With New Scripting.FileSystemObject
'
'                    strBaseName = strBaseName & "_" & .GetBaseName(vobjTargetVBProject.fileName)
'                End With
'            End If
'        End If
'    End If
'
'    On Error GoTo 0
'
'    GetLoggingFileBaseNameForVBProjectAllProcedureInfo = strBaseName
'End Function
'
'
''''
''''
''''
'Public Sub CollectAllVBProceduresBasicFromVBComponent(ByRef robjDataTableCol As Collection, _
'        ByRef robjFoundModuleNameDic As Scripting.Dictionary, _
'        ByVal vobjComponent As VBIDE.VBComponent, _
'        Optional ByVal vstrClassifyingKey01 As String = "OfficeCommonCore", _
'        Optional ByVal vstrClassifyingKey02 As String = "")
'
'
'    Dim intLine As Long, intLineMax As Long
'    Dim strModuleName As String
'    Dim strProcName As String, strProcNameDicKey As String
'    Dim enmReceivedProcKind As VBIDE.vbext_ProcKind, intProcBodyLine As Long, intProcStartLine As Long, intProcCountLines As Long
'    Dim objRowCol As Collection
'    Dim objProcNameToStartLineDic As Scripting.Dictionary
'    Dim blnInsertToClassifyingKeyInfo As Boolean
'
'    blnInsertToClassifyingKeyInfo = False
'
'    If vstrClassifyingKey01 <> "" Or vstrClassifyingKey02 <> "" Then
'
'        blnInsertToClassifyingKeyInfo = True
'    End If
'
'
'    Set objProcNameToStartLineDic = New Scripting.Dictionary
'
'    With vobjComponent
'
'        strModuleName = .Name & GetDotExtensionFromVBAComponentType(.Type)
'
'        With .CodeModule
'
'            intLineMax = .CountOfLines
'
'            intLine = .CountOfDeclarationLines + 1
'
'            While intLine <= intLineMax
'
'                strProcName = .ProcOfLine(intLine, enmReceivedProcKind)
'
'                strProcNameDicKey = strProcName & GetVBAProcedureKindSuffixKey(enmReceivedProcKind)
'
'                If strProcName <> "" And Not objProcNameToStartLineDic.Exists(strProcNameDicKey) Then
'
'                    objProcNameToStartLineDic.Add strProcNameDicKey, intLine
'
'                    intProcStartLine = .ProcStartLine(strProcName, enmReceivedProcKind)
'
'                    intProcBodyLine = .ProcBodyLine(strProcName, enmReceivedProcKind)
'
'                    intProcCountLines = .ProcCountLines(strProcName, enmReceivedProcKind)
'
'                    Set objRowCol = New Collection
'
'                    With objRowCol
'
'                        If blnInsertToClassifyingKeyInfo Then
'
'                            .Add vstrClassifyingKey01: .Add vstrClassifyingKey02
'                        End If
'
'                        .Add strModuleName  ' 1st column
'
'                        .Add strProcName    ' 2nd column
'
'                        .Add GetKindFullNameOfVBAProcedure(enmReceivedProcKind)  ' 3rd column
'
'                        .Add intLine        ' 4th column
'
'                        .Add intProcCountLines  ' 5th column
'
'                        .Add intProcBodyLine    ' 6th column
'
'                        .Add intProcStartLine   ' 7th column
'                    End With
'
'                    robjDataTableCol.Add objRowCol
'
'                    If robjFoundModuleNameDic Is Nothing Then Set robjFoundModuleNameDic = New Scripting.Dictionary
'
'                    With robjFoundModuleNameDic
'
'                        If Not .Exists(strModuleName) Then
'
'                            .Add strModuleName, 0
'                        End If
'                    End With
'
'                    intLine = intProcStartLine + intProcCountLines
'                Else
'                    intLine = intLine + 1
'                End If
'            Wend
'        End With
'    End With
'End Sub
'
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
''''
''''
''''
'Private Sub msubCollectAllVBProceduresInformation(ByRef robjDTCol As Collection, _
'        ByRef robjModuleNamesImplementedFunctionsCol As Collection, _
'        ByVal vobjVBProject As VBIDE.VBProject)
'
'    Dim objComponent As VBIDE.VBComponent, objFoundModuleNameDic As Scripting.Dictionary
'
'    If robjDTCol Is Nothing Then Set robjDTCol = New Collection
'
'    With vobjVBProject
'
'        For Each objComponent In .VBComponents
'
'            'Debug.Print "Name := " & objComponent.Name & ", Type := " & objComponent.Type & ", CountOfLine := " & objComponent.CodeModule.CountOfLines
'
'            CollectAllVBProceduresBasicFromVBComponent robjDTCol, objFoundModuleNameDic, objComponent, "", ""
'        Next
'    End With
'
'    If Not objFoundModuleNameDic Is Nothing Then
'
'        Set robjModuleNamesImplementedFunctionsCol = GetEnumeratorKeysColFromDic(objFoundModuleNameDic)
'    End If
'End Sub
'
'
''''
''''
''''
'Private Sub msubOutputCollectedAllVBProceduresInformationToCSV(ByVal vobjDTCol As Collection, _
'        ByVal vobjModuleNamesImplementedFunctionsCol As Collection, _
'        ByVal vobjVBProject As VBIDE.VBProject)
'
'    Dim strCSVFilePath As String
'
'    strCSVFilePath = GetRepositoryCodesRootDir() & "\" & GetLoggingFileBaseNameForVBProjectAllProcedureInfo(vobjVBProject) & ".csv"
'
'    OutputColToCSVFile vobjDTCol, GetFieldTitlesColForAllVBProceduresInformationLog(), strCSVFilePath
'
'    OpenTextBySomeSelectedTextEditorFromWshShell strCSVFilePath, True, True
'End Sub
'
'
'
'
'
'
'''--VBA_Code_File--<CompareVBProjects.bas>--
'Attribute VB_Name = "CompareVBProjects"
''
''   compare plural VBA projects
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on VBIDE
''       Dependent on OperateWinMerge.bas
''       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Tue, 24/May/2022    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''''
'''' use WinMerge
''''
'''' <Argument>vstrExecutionApplicationFilePath: Excel file, Word file, or PowerPoint file. Access cannot serve the VBIDE.VBProject. Outlook has no VBIDE.VBProject model </Argument>
'Public Sub ComparePluralVBProjectsAboutCountOfLinesOfEachVBComponent(ByVal vstrExecutionApplicationFilePath As String, _
'        ByVal vobjVBProjects As Collection, _
'        Optional ByVal venmLogDTVisualizationType As LogDTVisualizationType = LogDTVisualizationType.GenerateCSVFile, _
'        Optional ByVal vblnDisplayDifferentLinesByWinMerge As Boolean = True)
'
'
'    Dim objProjectNameToModuleNameDic As Scripting.Dictionary, objModuleNameToCountOfLinesDic As Scripting.Dictionary
'    Dim objCommonModuleNameToLinesInfoDic As Scripting.Dictionary, objProjectToIndependentModuleNameInfoDic As Scripting.Dictionary
'    Dim objPartiallyCommonModuleNameToLinesInfoDic As Scripting.Dictionary
'    Dim objDiffrentLinesCommonModuleNameToLinesInfoDic As Scripting.Dictionary, objModuleNameToMaxMinToVBProjectIndexDic As Scripting.Dictionary
'
'
'    Set objProjectNameToModuleNameDic = mfobjGetProjectsNameToCountOfLinesDictionary(vobjVBProjects)
'
'    msubGetCommonOrIndependentModuleInfo objCommonModuleNameToLinesInfoDic, objPartiallyCommonModuleNameToLinesInfoDic, objProjectToIndependentModuleNameInfoDic, objProjectNameToModuleNameDic, vobjVBProjects
'
'    Set objDiffrentLinesCommonModuleNameToLinesInfoDic = mfobjGetDiffrentLinesCommonModuleNameToLinesInfoDic(objCommonModuleNameToLinesInfoDic)
'
'    Set objModuleNameToMaxMinToVBProjectIndexDic = mfobjGetModuleNameToMaxMinToVBProjectIndexDic(objDiffrentLinesCommonModuleNameToLinesInfoDic)
'
'    If vblnDisplayDifferentLinesByWinMerge And IsWinMergeUInstalledInThisComputer() Then
'
'        ' It is not confirmed about all-texts for each VBComponents, the only count of lines are checked
'
'        msubOpenComparedVBACodesBetweenAutoSelectedTwoVBACodesForEachVBModuleName objModuleNameToMaxMinToVBProjectIndexDic, vobjVBProjects
'    End If
'
'    ' Output results
'
'    msubOutputComparedLinesOfPluralVBProjectResults vstrExecutionApplicationFilePath, vobjVBProjects, objCommonModuleNameToLinesInfoDic, objDiffrentLinesCommonModuleNameToLinesInfoDic, objModuleNameToMaxMinToVBProjectIndexDic, objPartiallyCommonModuleNameToLinesInfoDic, objProjectToIndependentModuleNameInfoDic, venmLogDTVisualizationType
'End Sub
'
'
''''
'''' use WinMergeU.exe
''''
'''' <vstrExecutionApplicationFilePath>Excel file, Word file, or PowerPoint file. Access cannot serve the VBIDE.VBProject. Outlook has no VBIDE.VBProject model </vstrExecutionApplicationFilePath>
'Public Sub CompareTwoVBProjectsAboutAllTextsByExportingForEachCommonModuleName(ByVal vstrExecutionApplicationFilePath As String, _
'        ByVal vobj1stVBProject As VBIDE.VBProject, _
'        ByVal vobj2ndVBProject As VBIDE.VBProject, _
'        Optional ByVal venmLogDTVisualizationType As LogDTVisualizationType = LogDTVisualizationType.GenerateCSVFile, _
'        Optional ByVal vblnDisplayDifferentTextsByWinMerge As Boolean = True)
'
'
'    Dim objVBProjects As Collection
'    Dim objProjectNameToModuleNameDic As Scripting.Dictionary, objModuleNameToCountOfLinesDic As Scripting.Dictionary
'    Dim objCommonModuleNameToLinesInfoDic As Scripting.Dictionary, objProjectToIndependentModuleNameInfoDic As Scripting.Dictionary
'    Dim objDummyPartiallyCommonModuleNameToLinesInfoDic As Scripting.Dictionary
'    Dim objCommonModuleNameToDifferenceCheckedAndLinesInfoDic As Scripting.Dictionary
'
'
'    If vobj1stVBProject Is vobj2ndVBProject Then
'
'        MsgBox mfstrGetMassageWhenTwoVBProjectsAreSame(vobj1stVBProject), vbExclamation Or vbOKOnly, "Comparing two VBProjects by exporting each codes"
'
'        Exit Sub
'    End If
'
'    Set objVBProjects = mfobjGetVBProjectsBySortingLastModifiedDate(vobj1stVBProject, vobj2ndVBProject)
'
'    Set objProjectNameToModuleNameDic = mfobjGetProjectsNameToCountOfLinesDictionary(objVBProjects)
'
'    ' When the count of objVBProjects is 2, then objDummyPartiallyCommonModuleNameToLinesInfoDic is sure to nothing
'    msubGetCommonOrIndependentModuleInfo objCommonModuleNameToLinesInfoDic, objDummyPartiallyCommonModuleNameToLinesInfoDic, objProjectToIndependentModuleNameInfoDic, objProjectNameToModuleNameDic, objVBProjects
'
'    Set objCommonModuleNameToDifferenceCheckedAndLinesInfoDic = mfobjGetAllTextsDifferenceCheckedAndLinesInfoDicByExporingVBComponentsAndCompareIts(objVBProjects, objCommonModuleNameToLinesInfoDic)
'
'    ' Output results
'    msubOutputComparedAllTextsOfTwoVBProjectResults vstrExecutionApplicationFilePath, objVBProjects, objCommonModuleNameToDifferenceCheckedAndLinesInfoDic, objProjectToIndependentModuleNameInfoDic, venmLogDTVisualizationType
'End Sub
'
''''
'''' use WinMergeU.exe
''''
'Private Function mfobjGetAllTextsDifferenceCheckedAndLinesInfoDicByExporingVBComponentsAndCompareIts(ByVal vobjVBProjects As Collection, _
'        ByVal vobjCommonModuleNameToLinesInfoDic As Scripting.Dictionary, _
'        Optional ByVal vblnDisplayDifferentTextsByWinMerge As Boolean = True) As Scripting.Dictionary
'
'    Dim objDic As Scripting.Dictionary, i As Long, varVBProject As Variant, objVBProject As VBIDE.VBProject
'    Dim varModuleFileName As Variant, objFS As Scripting.FileSystemObject
'    Dim strDir As String, strPath() As String
'    Dim objDifferentCommonModuleNameToCheckedAndLinesInfoDic As Scripting.Dictionary, objSameCommonModuleNameToCheckedAndLinesInfoDic As Scripting.Dictionary
'    Dim objCheckedAndLinesInfoCol As Collection, objLinesInfoCol As Collection
'    Dim stdLastModifiedDateArray(1 To 2) As Date, intCountOfLinesArray(1 To 2) As Long
'    Dim strLeftLineNumberInfo As String, strRightLineNumberInfo As String
'    Dim strWinMergeOption As String
'
'
'    Set objFS = New Scripting.FileSystemObject
'
'    i = 1
'    For Each varVBProject In vobjVBProjects
'
'        Set objVBProject = varVBProject
'
'        stdLastModifiedDateArray(i) = objFS.GetFile(objVBProject.fileName).DateLastModified
'
'        i = i + 1
'    Next
'
'
'    ReDim strPath(1 To vobjVBProjects.Count)    ' vobjVBProjects.Count is always two...
'
'    With vobjCommonModuleNameToLinesInfoDic
'
'        For Each varModuleFileName In .Keys
'
'            Set objLinesInfoCol = .Item(varModuleFileName)
'
'            i = 1
'
'            For Each varVBProject In vobjVBProjects
'
'                Set objVBProject = varVBProject
'
'                strDir = GetTmpCompareRootVBProjectDirectory(i, objFS)
'
'                strPath(i) = strDir & "\" & varModuleFileName
'
'                With objVBProject.VBComponents.Item(objFS.GetBaseName(varModuleFileName))
'
'                    .Export strPath(i)
'
'                    intCountOfLinesArray(i) = .CodeModule.CountOfLines
'                End With
'
'                i = i + 1
'            Next
'
'            ' compare all texts for two source codes
'            If IsDifferentBetweenTextFilesByFileSystemObject(strPath(1), strPath(2)) Then
'
'                If objDifferentCommonModuleNameToCheckedAndLinesInfoDic Is Nothing Then Set objDifferentCommonModuleNameToCheckedAndLinesInfoDic = New Scripting.Dictionary
'
'                With objDifferentCommonModuleNameToCheckedAndLinesInfoDic
'
'                    Set objCheckedAndLinesInfoCol = New Collection
'
'                    objCheckedAndLinesInfoCol.Add True
'
'                    UnionDoubleCollectionsToSingle objCheckedAndLinesInfoCol, objLinesInfoCol
'
'                    .Add varModuleFileName, objCheckedAndLinesInfoCol
'                End With
'
'                If vblnDisplayDifferentTextsByWinMerge And IsWinMergeUInstalledInThisComputer() Then
'
'                    ' use WinMergeU.exe
'
'                    strWinMergeOption = " -xq -e"
'
'                    If intCountOfLinesArray(1) > intCountOfLinesArray(2) Then
'
'                        strLeftLineNumberInfo = "Count of lines - greater (" & CStr(intCountOfLinesArray(1)) & ")"
'
'                        strRightLineNumberInfo = "Count of lines - lesser (" & CStr(intCountOfLinesArray(2)) & ")"
'
'                    ElseIf intCountOfLinesArray(1) = intCountOfLinesArray(2) Then
'
'                        strLeftLineNumberInfo = "Same count of lines of both (" & CStr(intCountOfLinesArray(1)) & ")"
'
'                        strRightLineNumberInfo = "Same count of lines of both (" & CStr(intCountOfLinesArray(2)) & ")"
'                    Else
'
'                        strLeftLineNumberInfo = "Count of lines - lesser (" & CStr(intCountOfLinesArray(1)) & ")"
'
'                        strRightLineNumberInfo = "Count of lines - greater (" & CStr(intCountOfLinesArray(2)) & ")"
'                    End If
'
'                    strWinMergeOption = strWinMergeOption & " -dl """ & varModuleFileName & " ,Latest update:[" & Format(stdLastModifiedDateArray(1), "yyyy/m/d hh:mm") & "] " & strLeftLineNumberInfo & """ -dr "" " & varModuleFileName & " ,Latest update:[" & Format(stdLastModifiedDateArray(2), "yyyy/m/d hh:mm") & "] " & strRightLineNumberInfo & " """
'
'                    CompareTwoSourceFilesByWinMerge strPath(1), strPath(2), strWinMergeOption
'
'                End If
'            Else
'                If objSameCommonModuleNameToCheckedAndLinesInfoDic Is Nothing Then Set objSameCommonModuleNameToCheckedAndLinesInfoDic = New Scripting.Dictionary
'
'                With objSameCommonModuleNameToCheckedAndLinesInfoDic
'
'                    Set objCheckedAndLinesInfoCol = New Collection
'
'                    objCheckedAndLinesInfoCol.Add False
'
'                    UnionDoubleCollectionsToSingle objCheckedAndLinesInfoCol, objLinesInfoCol
'
'                    .Add varModuleFileName, objCheckedAndLinesInfoCol
'                End With
'            End If
'        Next
'    End With
'
'
'    If Not objDifferentCommonModuleNameToCheckedAndLinesInfoDic Is Nothing And Not objSameCommonModuleNameToCheckedAndLinesInfoDic Is Nothing Then
'
'        UnionDoubleDictionariesToSingle objDifferentCommonModuleNameToCheckedAndLinesInfoDic, objSameCommonModuleNameToCheckedAndLinesInfoDic
'
'        Set objDic = objDifferentCommonModuleNameToCheckedAndLinesInfoDic
'    ElseIf Not objDifferentCommonModuleNameToCheckedAndLinesInfoDic Is Nothing Then
'
'        Set objDic = objDifferentCommonModuleNameToCheckedAndLinesInfoDic
'
'    ElseIf Not objSameCommonModuleNameToCheckedAndLinesInfoDic Is Nothing Then
'
'        Set objDic = objSameCommonModuleNameToCheckedAndLinesInfoDic
'    End If
'
'    Set mfobjGetAllTextsDifferenceCheckedAndLinesInfoDicByExporingVBComponentsAndCompareIts = objDic
'End Function
'
'
''''
'''' The latest file has the first index
''''
'Private Function mfobjGetVBProjectsBySortingLastModifiedDate(ByVal vobj1stVBProject As VBIDE.VBProject, vobj2ndVBProject As VBIDE.VBProject) As Collection
'
'    Dim std1stLastModifiedDate As Date, std2ndLastModifiedDate As Date, objCol As Collection
'
'    With New Scripting.FileSystemObject
'
'        std1stLastModifiedDate = .GetFile(vobj1stVBProject.fileName).DateLastModified
'
'        std2ndLastModifiedDate = .GetFile(vobj2ndVBProject.fileName).DateLastModified
'    End With
'
'    Set objCol = New Collection
'
'    With objCol
'
'        If std1stLastModifiedDate > std2ndLastModifiedDate Then
'
'            .Add vobj1stVBProject
'
'            .Add vobj2ndVBProject
'        Else
'            .Add vobj2ndVBProject
'
'            .Add vobj1stVBProject
'        End If
'    End With
'
'    Set mfobjGetVBProjectsBySortingLastModifiedDate = objCol
'End Function
'
'
'
'Private Function mfstrGetMassageWhenTwoVBProjectsAreSame(ByVal vobjVBProject As VBIDE.VBProject) As String
'
'    Dim strMassage As String
'
'    strMassage = "The specified two VBProject are same." & vbNewLine & vbNewLine & "Path: " & vobjVBProject.fileName
'
'    mfstrGetMassageWhenTwoVBProjectsAreSame = strMessage
'End Function
'
''''
''''
''''
'Public Sub OutputComparingLinesResultsForPluralVBProjectsByCSV(ByVal vstrOutputDir As String, _
'        ByVal vobjCommonModuleNameToLinesInfoDic As Scripting.Dictionary, _
'        ByVal vobjDiffrentLinesCommonModuleNameToLinesInfoDic As Scripting.Dictionary, _
'        ByVal vobjModuleNameToMaxMinToVBProjectIndexDic As Scripting.Dictionary, _
'        ByVal vobjPartiallyCommonModuleNameToLinesInfoDic As Scripting.Dictionary, _
'        ByVal vobjProjectToIndependentModuleNameInfoDic As Scripting.Dictionary, _
'        ByVal vobjVBProjects As Collection)
'
'
'    Dim objFieldTitlesCol As Collection, strCSVFilePath As String, varProjectPath As Variant, strProjectPath As String
'    Dim objIndependentModuleNameToCountOfLinesDic As Scripting.Dictionary, objFS As Scripting.FileSystemObject, i As Long
'
'
'    ForceToCreateDirectory vstrOutputDir
'
'    If Not vobjCommonModuleNameToLinesInfoDic Is Nothing Then
'
'        If vobjCommonModuleNameToLinesInfoDic.Count > 0 Then
'
'            strCSVFilePath = vstrOutputDir & "\CommonModulesInfo.csv"
'
'            Set objFieldTitlesCol = GetFieldTitlesColForComparingVBProjectsByCSV(vobjVBProjects)
'
'            OutputDicToCSVFile vobjCommonModuleNameToLinesInfoDic, objFieldTitlesCol, strCSVFilePath
'
'            OpenTextBySomeSelectedTextEditorFromWshShell strCSVFilePath, True
'        End If
'    End If
'
'    If Not vobjDiffrentLinesCommonModuleNameToLinesInfoDic Is Nothing Then
'
'        If vobjDiffrentLinesCommonModuleNameToLinesInfoDic.Count > 0 Then
'
'            strCSVFilePath = vstrOutputDir & "\DifferentLinesCommonModulesInfo.csv"
'
'            Set objFieldTitlesCol = GetFieldTitlesColForComparingVBProjectsByCSV(vobjVBProjects)
'
'            OutputDicToCSVFile vobjCommonModuleNameToLinesInfoDic, objFieldTitlesCol, strCSVFilePath
'
'            OpenTextBySomeSelectedTextEditorFromWshShell strCSVFilePath, True
'        End If
'    End If
'
'
'    If Not vobjPartiallyCommonModuleNameToLinesInfoDic Is Nothing Then
'
'        If vobjPartiallyCommonModuleNameToLinesInfoDic.Count > 0 Then
'
'            strCSVFilePath = vstrOutputDir & "\PartiallyCommonModulesInfo.csv"
'
'            If objFieldTitlesCol Is Nothing Then
'
'                Set objFieldTitlesCol = GetFieldTitlesColForComparingVBProjectsByCSV(vobjVBProjects)
'            End If
'
'            OutputDicToCSVFile vobjCommonModuleNameToLinesInfoDic, objFieldTitlesCol, strCSVFilePath
'
'            OpenTextBySomeSelectedTextEditorFromWshShell strCSVFilePath, True
'        End If
'    End If
'
'    msubOutputProjectToIndependentModuleNameInfoDicByCSV vstrOutputDir, vobjProjectToIndependentModuleNameInfoDic, vobjVBProjects
'End Sub
'
'
''''
''''
''''
'Public Sub OutputComparingAllTextsResultsForPluralVBProjectsByCSV(ByVal vstrOutputDir As String, _
'        ByVal vobjCommonModuleNameToDifferenceCheckedAndLinesInfoDic As Scripting.Dictionary, _
'        ByVal vobjProjectToIndependentModuleNameInfoDic As Scripting.Dictionary, _
'        ByVal vobjVBProjects As Collection)
'
'
'    ForceToCreateDirectory vstrOutputDir
'
'    msubOutputCommonModuleNameToDifferenceCheckedAndLinesInfoDicByCSV vstrOutputDir, vobjCommonModuleNameToDifferenceCheckedAndLinesInfoDic, vobjVBProjects
'
'    msubOutputProjectToIndependentModuleNameInfoDicByCSV vstrOutputDir, vobjProjectToIndependentModuleNameInfoDic, vobjVBProjects
'End Sub
'
'
'
'Private Sub msubOutputCommonModuleNameToDifferenceCheckedAndLinesInfoDicByCSV(ByVal vstrOutputDir As String, _
'        ByVal vobjProjectToIndependentModuleNameInfoDic As Scripting.Dictionary, _
'        ByVal vobjVBProjects As Collection)
'
'
'    Dim strCSVFilePath As String, objFieldTitlesCol As Collection
'    Dim i As Long, objFS As Scripting.FileSystemObject, varProjectPath As Variant, strProjectPath As String
'
'
'    If vobjProjectToIndependentModuleNameInfoDic.Count > 0 Then
'
'        strCSVFilePath = vstrOutputDir & "\CommonModulesInfoWithCodeDifferenceExistence.csv"
'
'        Set objFieldTitlesCol = GetFieldTitlesColForComparingTwoVBProjectsWithExportingCodesByCSV(vobjVBProjects)
'
'        OutputDicToCSVFile vobjProjectToIndependentModuleNameInfoDic, objFieldTitlesCol, strCSVFilePath
'    End If
'End Sub
'
'
'
'Private Sub msubOutputProjectToIndependentModuleNameInfoDicByCSV(ByVal vstrOutputDir As String, _
'        ByVal vobjProjectToIndependentModuleNameInfoDic As Scripting.Dictionary, _
'        ByVal vobjVBProjects As Collection)
'
'
'    Dim strCSVFilePath As String, objIndependentModuleNameToCountOfLinesDic As Scripting.Dictionary, objFieldTitlesCol As Collection
'    Dim i As Long, objFS As Scripting.FileSystemObject, varProjectPath As Variant, strProjectPath As String
'
'
'    With vobjProjectToIndependentModuleNameInfoDic
'
'        If .Count > 0 Then
'
'            Set objFS = New Scripting.FileSystemObject
'
'            i = 1
'
'            For Each varProjectPath In .Keys
'
'                strProjectPath = varProjectPath
'
'                Set objIndependentModuleNameToCountOfLinesDic = .Item(varProjectPath)
'
'                Set objFieldTitlesCol = GetColFromLineDelimitedChar("ModuleFileName,CountOfLines")
'
'                strCSVFilePath = vstrOutputDir & "\IndependentModules" & Format(i, "00") & "_" & objFS.GetBaseName(strProjectPath) & ".csv"
'
'                OutputDicToCSVFile objIndependentModuleNameToCountOfLinesDic, objFieldTitlesCol, strCSVFilePath
'
'                i = i + 1
'            Next
'        End If
'    End With
'End Sub
'
'
'
'
'Public Function GetFieldTitlesColForComparingVBProjectsByCSV(ByVal vobjVBProjects As Collection) As Collection
'
'    Set GetFieldTitlesColForComparingVBProjectsByCSV = GetFieldTitlesColForComparingVBProjects(vobjVBProjects)
'End Function
'
'
'Public Function GetFieldTitlesColForComparingVBProjectsByExcelBook(ByVal vobjVBProjects As Collection) As Collection
'
'    Set GetFieldTitlesColForComparingVBProjectsByExcelBook = GetFieldTitlesColForComparingVBProjects(vobjVBProjects, True)
'End Function
'
'
'Public Function GetFieldTitlesColForComparingTwoVBProjectsWithExportingCodesByCSV(ByVal vobjVBProjects As Collection) As Collection
'
'    Set GetFieldTitlesColForComparingTwoVBProjectsWithExportingCodesByCSV = GetFieldTitlesColForComparingVBProjects(vobjVBProjects, False, True)
'End Function
'
'Public Function GetFieldTitlesColForComparingTwoVBProjectsWithExportingCodesByExcelBook(ByVal vobjVBProjects As Collection) As Collection
'
'    Set GetFieldTitlesColForComparingTwoVBProjectsWithExportingCodesByExcelBook = GetFieldTitlesColForComparingVBProjects(vobjVBProjects, True, True)
'End Function
'
''''
''''
''''
'Private Function GetFieldTitlesColForComparingVBProjects(ByVal vobjVBProjects As Collection, Optional ByVal vblnUseLineFeed As Boolean = False, Optional ByVal vblnIncludesColumnThatIsDifferenceExists As Boolean = False) As Collection
'
'    Dim objCol As Collection, varVBProject As Variant, objVBProject As VBIDE.VBProject
'    Dim objFS As Scripting.FileSystemObject, i As Long, strTitle As String
'
'
'    Set objFS = New Scripting.FileSystemObject
'
'    Set objCol = New Collection
'
'    objCol.Add "ModuleFileName"
'
'    If vblnIncludesColumnThatIsDifferenceExists Then
'
'        objCol.Add "IsDifferenceExists"
'    End If
'
'    i = 1
'
'    For Each varVBProject In vobjVBProjects
'
'        Set objVBProject = varVBProject
'
'        strTitle = mfstrGetComparingFieldTitleOfVBProject(i, objVBProject, vblnUseLineFeed, objFS)
'
'        objCol.Add strTitle
'
'        i = i + 1
'    Next
'
'    Set GetFieldTitlesColForComparingVBProjects = objCol
'End Function
'
''''
''''
''''
'Public Function GetComparingFieldColumnIndexToFieldTitle(ByVal vobjVBProjects As Collection, Optional ByVal vblnUseLineFeed As Boolean = False) As Scripting.Dictionary
'
'    Dim objDic As Scripting.Dictionary
'    Dim varVBProject As Variant, objVBProject As VBIDE.VBProject, i As Long, strTitle As String
'
'
'    Set objDic = New Scripting.Dictionary
'
'    i = 1
'
'    For Each varVBProject In vobjVBProjects
'
'        Set objVBProject = varVBProject
'
'        strTitle = mfstrGetComparingFieldTitleOfVBProject(i, objVBProject, vblnUseLineFeed)
'
'        objDic.Add i, strTitle
'
'        i = i + 1
'    Next
'
'    Set GetComparingFieldColumnIndexToFieldTitle = objDic
'End Function
'
'
''''
''''
''''
'Private Function mfstrGetComparingFieldTitleOfVBProject(ByVal vintIndex As Long, ByVal vobjVBProject As VBIDE.VBProject, Optional ByVal vblnUseLineFeed As Boolean = False, Optional ByVal vobjFS As Scripting.FileSystemObject = Nothing) As String
'
'    Dim strTitle As String, objFS As Scripting.FileSystemObject
'
'    If vobjFS Is Nothing Then
'
'        Set objFS = New Scripting.FileSystemObject
'    Else
'        Set objFS = vobjFS
'    End If
'
'    With vobjVBProject
'
'        strTitle = CStr(vintIndex) & " / " & objFS.GetFileName(.fileName)
'
'        If vblnUseLineFeed Then
'
'            strTitle = strTitle & vbLf
'        Else
'            strTitle = strTitle & " : "
'        End If
'
'        strTitle = strTitle & objFS.GetParentFolderName(.fileName)
'    End With
'
'    mfstrGetComparingFieldTitleOfVBProject = strTitle
'End Function
'
'
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
''''
''''
''''
'''' <Argument>vobjVBProjects: Collection</Argument>
'''' <Return>Dictionary(Of Key[VB project path], Of Dictionary(Of Key[Module name], Of Value[Count of lines]))</Return>
'Private Function mfobjGetProjectsNameToCountOfLinesDictionary(ByVal vobjVBProjects As Collection) As Scripting.Dictionary
'
'    Dim varVBProject As Variant, objVBProject As VBIDE.VBProject
'    Dim objComponent As VBIDE.VBComponent
'    Dim strModuleFileName As String
'    Dim objProjectNameToModuleNameDic As Scripting.Dictionary, objModuleNameToCountOfLinesDic As Scripting.Dictionary
'
'    Set objProjectNameToModuleNameDic = New Scripting.Dictionary
'
'    For Each varVBProject In vobjVBProjects
'
'        Set objVBProject = varVBProject
'
'        If objProjectNameToModuleNameDic Is Nothing Then Set objProjectNameToModuleNameDic = New Scripting.Dictionary
'
'        Set objModuleNameToCountOfLinesDic = Nothing
'
'        For Each objComponent In objVBProject.VBComponents
'
'            With objComponent
'
'                If .Type <> vbext_ct_Document Then
'
'                    If objModuleNameToCountOfLinesDic Is Nothing Then Set objModuleNameToCountOfLinesDic = New Scripting.Dictionary
'
'                    strModuleFileName = .Name & GetDotExtensionFromVBAComponentType(.Type)
'
'                    objModuleNameToCountOfLinesDic.Add strModuleFileName, .CodeModule.CountOfLines
'                End If
'            End With
'        Next
'
'        With objProjectNameToModuleNameDic
'
'            If Not objModuleNameToCountOfLinesDic Is Nothing Then
'
'                .Add objVBProject.fileName, objModuleNameToCountOfLinesDic
'            Else
'
'                .Add objVBProject.fileName, Nothing
'            End If
'        End With
'    Next
'
'    Set mfobjGetProjectsNameToCountOfLinesDictionary = objProjectNameToModuleNameDic
'End Function
'
'
''''
'''' get common module name and independent module name
''''
'''' <Argument>robjCommonModuleNameToLinesInfoDic: Output - </Argument>
'Private Sub msubGetCommonOrIndependentModuleInfo(ByRef robjCommonModuleNameToLinesInfoDic As Scripting.Dictionary, _
'        ByRef robjPartiallyCommonModuleNameToLinesInfoDic As Scripting.Dictionary, _
'        ByRef robjProjectToIndependentModuleNameInfoDic As Scripting.Dictionary, _
'        ByVal vobjProjectNameToModuleNameDic As Scripting.Dictionary, _
'        ByVal vobjVBProjects As Collection)
'
'
'    ' temporary dictionary
'    Dim objProjectPathToCommonModuleNameDictionaries As Scripting.Dictionary, objProjectPathToPartialCommonModuleNameDictionaries As Scripting.Dictionary
'    Dim objProjectPaths As Collection, objColOfModuleNameToCountOfLineDic As Collection
'
'
'    Set robjCommonModuleNameToLinesInfoDic = Nothing
'
'    Set robjProjectToIndependentModuleNameInfoDic = Nothing
'
'    ' vobjProjectNameToModuleNameDic - Dictionary(Of Key[VB project path], Of Dictionary(Of Key[Module name], Of Value[Count of lines]))
'
'    GetDividedTwoCollectionsFromDictionary objProjectPaths, objColOfModuleNameToCountOfLineDic, vobjProjectNameToModuleNameDic
'
'    CompareKeysOfEachNestedDictionaries objProjectPathToCommonModuleNameDictionaries, objProjectPathToPartialCommonModuleNameDictionaries, robjProjectToIndependentModuleNameInfoDic, vobjProjectNameToModuleNameDic
'
'    Set robjCommonModuleNameToLinesInfoDic = GetCommonKeyToAlignedValuesDic(objProjectPathToCommonModuleNameDictionaries, objProjectPaths.Count, objProjectPaths, "1", 0)
'
'    Set robjPartiallyCommonModuleNameToLinesInfoDic = GetCommonKeyToAlignedValuesDic(objProjectPathToPartialCommonModuleNameDictionaries, objProjectPaths.Count, objProjectPaths, "1", 0)
'End Sub
'
'
''''
'''' Detect code defference among the count of lines of the codes
''''
'Private Function mfobjGetDiffrentLinesCommonModuleNameToLinesInfoDic(ByVal vobjCommonModuleNameToLinesInfoDic As Scripting.Dictionary) As Scripting.Dictionary
'
'    Dim objDic As Scripting.Dictionary, varFileName As Variant, strFileName As String, objCountOfLines As Collection
'    Dim intPreviousCountOfLines As Long, intNextCountOfLines As Long, i As Long, blnHasDifferenceAmongSomeCodes As Boolean
'    Dim varCountOfLines As Variant
'
'    Set objDic = New Scripting.Dictionary
'
'    If Not vobjCommonModuleNameToLinesInfoDic Is Nothing Then
'
'        With vobjCommonModuleNameToLinesInfoDic
'
'            For Each varFileName In .Keys
'
'                strFileName = varFileName
'
'                Set objCountOfLines = .Item(varFileName)
'
'                blnHasDifferenceAmongSomeCodes = False
'
'                If objCountOfLines.Count > 2 Then
'
'                    i = 1
'
'                    For Each varCountOfLines In objCountOfLines
'
'                        If i = 1 Then
'
'                            intPreviousCountOfLines = varCountOfLines
'                        Else
'                            intNextCountOfLines = varCountOfLines
'
'                            If intPreviousCountOfLines <> intNextCountOfLines Then
'
'                                blnHasDifferenceAmongSomeCodes = True
'
'                                Exit For
'                            End If
'                        End If
'
'                        i = i + 1
'                    Next
'                Else
'                    intPreviousCountOfLines = objCountOfLines.Item(1)
'
'                    intNextCountOfLines = objCountOfLines.Item(2)
'
'                    If intPreviousCountOfLines <> intNextCountOfLines Then
'
'                        blnHasDifferenceAmongSomeCodes = True
'                    End If
'                End If
'
'                If blnHasDifferenceAmongSomeCodes Then
'
'                    objDic.Add varFileName, objCountOfLines
'                End If
'            Next
'        End With
'    End If
'
'    Set mfobjGetDiffrentLinesCommonModuleNameToLinesInfoDic = objDic
'End Function
'
'
''''
'''' get Dictionary by querying the VBProject indexes about both Maximum and Minimum from CommonModuleNameToLinesDic restricted having count difference of lines
''''
'''' <Argument>Scripting.Dictionary(Of String[ModuleFileName], Of Collections(Of Long[Cont of lines of each module])</Argument>
'''' <Return>Scripting.Dictionary(Of String[ModuleFileName], Of Scripting.Dictionary(Of String[Max Or Min], Of Collection(Of Long[VBProjectIndex])))</Return>
'Private Function mfobjGetModuleNameToMaxMinToVBProjectIndexDic(ByVal vobjDiffrentLinesCommonModuleNameToLinesInfoDic As Scripting.Dictionary) As Scripting.Dictionary
'
'    Dim varModuleName As Variant, objCountOfLinesCol As Collection, i As Long, varCountOfLines As Variant
'    Dim intMaxCountOfLines As Long, intMinCountOfLines As Long, intCurrentCountOfLines As Long, intEnableCount As Long
'    Dim objDic As Scripting.Dictionary, objMaxMinToIndexCountsDic As Scripting.Dictionary, objMaxIndexCol As Collection, objMinIndexCol As Collection
'
'
'    Set objDic = Nothing
'
'    If Not vobjDiffrentLinesCommonModuleNameToLinesInfoDic Is Nothing Then
'
'        With vobjDiffrentLinesCommonModuleNameToLinesInfoDic
'
'            For Each varModuleName In .Keys
'
'                Set objCountOfLinesCol = .Item(varModuleName)
'
'                intMaxCountOfLines = 0
'
'                intMinCountOfLines = 0
'
'                i = 1
'
'                intEnableCount = 1
'
'                For Each varCountOfLines In objCountOfLinesCol
'
'                    intCurrentCountOfLines = varCountOfLines
'
'                    If intCurrentCountOfLines > 0 Then
'
'                        If intEnableCount = 1 Then
'
'                            intMaxCountOfLines = intCurrentCountOfLines
'
'                            intMinCountOfLines = intCurrentCountOfLines
'                        Else
'                            If intCurrentCountOfLines > intMaxCountOfLines Then
'
'                                intMaxCountOfLines = intCurrentCountOfLines
'
'                            ElseIf intCurrentCountOfLines < intMinCountOfLines Then
'
'                                intMinCountOfLines = intCurrentCountOfLines
'                            End If
'                        End If
'
'                        intEnableCount = intEnableCount + 1
'                    End If
'
'                    i = i + 1
'                Next
'
'                If intMinCountOfLines > 0 And intMaxCountOfLines > 0 Then
'
'                    If objDic Is Nothing Then Set objDic = New Scripting.Dictionary
'
'                    Set objMaxMinToIndexCountsDic = New Scripting.Dictionary
'
'                    Set objMaxIndexCol = New Collection
'
'                    Set objMinIndexCol = New Collection
'
'                    i = 1
'
'                    For Each varCountOfLines In objCountOfLinesCol
'
'                        intCurrentCountOfLines = varCountOfLines
'
'                        Select Case intCurrentCountOfLines
'
'                            Case intMaxCountOfLines
'
'                                objMaxIndexCol.Add i
'
'                            Case intMinCountOfLines
'
'                                objMinIndexCol.Add i
'                        End Select
'
'                        i = i + 1
'                    Next
'
'                    With objMaxMinToIndexCountsDic
'
'                        .Add "Max", objMaxIndexCol
'
'                        .Add "Min", objMinIndexCol
'                    End With
'
'                    objDic.Add varModuleName, objMaxMinToIndexCountsDic
'                End If
'            Next
'        End With
'    End If
'
'    Set mfobjGetModuleNameToMaxMinToVBProjectIndexDic = objDic
'End Function
'
'
''''
'''' use WinMergeU.exe
''''
'''' <Argument>vobjModuleNameToMaxMinToVBProjectIndexDic: Scripting.Dictionary(Of String[ModuleFileName], Of Scripting.Dictionary(Of String[Max Or Min], Of Collection(Of Long[VBProjectIndex])))</Argument>
'Private Sub msubOpenComparedVBACodesBetweenAutoSelectedTwoVBACodesForEachVBModuleName(ByVal vobjModuleNameToMaxMinToVBProjectIndexDic As Scripting.Dictionary, _
'        ByVal vobjVBProjects As Collection)
'
'
'    Dim varModuleName As Variant, objMaxMinToIndexCountsDic As Scripting.Dictionary, objMaxIndexCol As Collection, objMinIndexCol As Collection
'    Dim int1stVBProjectIndex As Long, int2ndVBProjectIndex As Long
'    Dim str1stCodePath As String, str2ndCodePath As String, stdLatestLastUpdateDate As Date, stdOldLastUpdateDate As Date
'    Dim int1stCountOfLines As Long, int2ndCountOfLines As Long
'    Dim strLeftLineNumberInfo As String, strRightLineNumberInfo As String
'    Dim strWinMergeOption As String, blnIsTwoProjects As Boolean
'
'
'    If Not vobjModuleNameToMaxMinToVBProjectIndexDic Is Nothing Then
'
'        If vobjModuleNameToMaxMinToVBProjectIndexDic.Count > 0 Then
'
'            blnIsTwoProjects = False
'
'            If vobjVBProjects.Count = 2 Then
'
'                blnIsTwoProjects = True
'
'                With New Scripting.FileSystemObject
'
'
'                End With
'
'                ' objFile.DateLastModified
'
'            End If
'
'
'            With vobjModuleNameToMaxMinToVBProjectIndexDic
'
'                For Each varModuleName In .Keys
'
'                    Set objMaxMinToIndexCountsDic = .Item(varModuleName)    ' varModuleName includes file extension
'
'                    strWinMergeOption = " -xq -e"
'
'                    If blnIsTwoProjects Then
'
'                        msubGetVBProjectIndexesByComparingFileLastModifiedDatesBetweenTwoFiles int1stVBProjectIndex, int2ndVBProjectIndex, stdLatestLastUpdateDate, stdOldLastUpdateDate, vobjVBProjects
'
'
'                        Set objMaxIndexCol = objMaxMinToIndexCountsDic.Item("Max")
'
'                        str1stCodePath = mfstrGetExportedPathAfterExportingSpecifiedModule(int1stCountOfLines, varModuleName, vobjVBProjects, int1stVBProjectIndex)
'
'                        str2ndCodePath = mfstrGetExportedPathAfterExportingSpecifiedModule(int2ndCountOfLines, varModuleName, vobjVBProjects, int2ndVBProjectIndex)
'
'                        ' use WinMerge
'                        If objMaxIndexCol.Item(1) = 1 Then
'
'                            strLeftLineNumberInfo = "Lesser"
'
'                            strRightLineNumberInfo = "Greater"
'                        Else
'                            strLeftLineNumberInfo = "Greater"
'
'                            strRightLineNumberInfo = "Lesser"
'                        End If
'
'                        strWinMergeOption = strWinMergeOption & " -dl """ & varModuleName & " ,Latest update:[" & Format(stdLatestLastUpdateDate, "yyyy/m/d hh:mm") & "] " & strLeftLineNumberInfo & " (" & CStr(int1stCountOfLines) & ") "" -dr "" " & varModuleName & " ,Latest update:[" & Format(stdOldLastUpdateDate, "yyyy/m/d hh:mm") & "] " & strRightLineNumberInfo & " (" & CStr(int2ndCountOfLines) & ") """
'
'                        CompareTwoSourceFilesByWinMerge str1stCodePath, str2ndCodePath, strWinMergeOption
'
'                    Else    ' more than two VBProjects
'                        Set objMaxIndexCol = objMaxMinToIndexCountsDic.Item("Max")
'
'                        Set objMinIndexCol = objMaxMinToIndexCountsDic.Item("Min")
'
'                        int1stVBProjectIndex = objMaxIndexCol.Item(1)
'
'                        int2ndVBProjectIndex = objMinIndexCol.Item(1)
'
'                        str1stCodePath = mfstrGetExportedPathAfterExportingSpecifiedModule(int1stCountOfLines, varModuleName, vobjVBProjects, int1stVBProjectIndex)
'
'                        str2ndCodePath = mfstrGetExportedPathAfterExportingSpecifiedModule(int2ndCountOfLines, varModuleName, vobjVBProjects, int2ndVBProjectIndex)
'
'                        ' use WinMerge
'                        strWinMergeOption = strWinMergeOption & " -dl """ & varModuleName & " , Count of lines: Greater (" & CStr(int1stCountOfLines) & ") "" -dr "" " & varModuleName & " , Count of lines: Lesser (" & CStr(int2ndCountOfLines) & ") """
'
'                        CompareTwoSourceFilesByWinMerge str1stCodePath, str2ndCodePath, strWinMergeOption
'                    End If
'                Next
'            End With
'        End If
'    End If
'End Sub
'
''''
''''
''''
'Private Sub msubGetVBProjectIndexesByComparingFileLastModifiedDatesBetweenTwoFiles(ByRef rintLeftVBProjectIndex As Long, _
'        ByRef rintRightVBProjectIndex As Long, _
'        ByRef rstdLatestLastModifiedDate As Date, _
'        ByRef rstdOldLastModifiedDate As Date, _
'        ByVal vobjTwoVBProjects As Collection)
'
'
'    Dim objVBProject As VBIDE.VBProject
'    Dim objFile As Scripting.File, stdLastModifiedDate01 As Date, stdLastModifiedDate02 As Date
'
'
'    With New Scripting.FileSystemObject
'
'        Set objVBProject = vobjTwoVBProjects.Item(1)
'
'        Set objFile = .GetFile(objVBProject.fileName)
'
'        stdLastModifiedDate01 = objFile.DateLastModified
'
'        Set objVBProject = vobjTwoVBProjects.Item(2)
'
'        Set objFile = .GetFile(objVBProject.fileName)
'
'        stdLastModifiedDate02 = objFile.DateLastModified
'
'        If stdLastModifiedDate01 > stdLastModifiedDate02 Then
'
'            ' The latest VBProject Index is 1st
'
'            rintLeftVBProjectIndex = 1
'
'            rintRightVBProjectIndex = 2
'
'            rstdLatestLastModifiedDate = stdLastModifiedDate01
'
'            rstdOldLastModifiedDate = stdLastModifiedDate02
'        Else
'
'            ' The latest VBProject Index is 2nd
'
'            rintLeftVBProjectIndex = 2
'
'            rintRightVBProjectIndex = 1
'
'            rstdLatestLastModifiedDate = stdLastModifiedDate02
'
'            rstdOldLastModifiedDate = stdLastModifiedDate01
'        End If
'    End With
'End Sub
'
'
'
''''
''''
''''
'Private Function mfstrGetExportedPathAfterExportingSpecifiedModule(ByRef rintOutputCountOfLines As Long, _
'        ByVal vstrModuleFileName As String, _
'        ByVal vobjVBProjects As Collection, _
'        ByVal vintVBProjectIndex As Long) As String
'
'
'    Dim strOutputPath As String, strDir As String, objVBProject As VBIDE.VBProject, objVBComponent As VBIDE.VBComponent
'
'
'    Set objVBProject = vobjVBProjects.Item(vintVBProjectIndex)
'
'    With New Scripting.FileSystemObject
'
'        Set objVBComponent = objVBProject.VBComponents.Item(.GetBaseName(vstrModuleFileName))
'
'        strDir = GetTmpCompareRootVBProjectDirectory(vintVBProjectIndex)
'
'        strOutputPath = strDir & "\" & vstrModuleFileName
'
'        ForceToCreateDirectory strDir
'    End With
'
'    objVBComponent.Export strOutputPath
'
'    ' Outputs
'    rintOutputCountOfLines = objVBComponent.CodeModule.CountOfLines
'
'    mfstrGetExportedPathAfterExportingSpecifiedModule = strOutputPath
'End Function
'
'
''''
''''
''''
'Private Function GetTmpCompareRootVBProjectDirectory(ByVal vintVBProjectIndex As Long, _
'        Optional ByVal vobjFS As Scripting.FileSystemObject = Nothing) As String
'
'
'    Dim strDir As String, objFS As Scripting.FileSystemObject
'
'    If vobjFS Is Nothing Then
'
'        Set objFS = New Scripting.FileSystemObject
'    Else
'        Set objFS = vobjFS
'    End If
'
'    strDir = GetTemporaryCodesBaseDir() & "\TmpCompareVBProject" & Format(vintVBProjectIndex, "00")
'
'    ForceToCreateDirectory strDir, objFS
'
'    GetTmpCompareRootVBProjectDirectory = strDir
'End Function
'
'
'
''''
'''' Output the compared VBComponent lines results
''''
'Private Sub msubOutputComparedLinesOfPluralVBProjectResults(ByVal vstrExecutionApplicationFilePath As String, _
'        ByVal vobjVBProjects As Collection, _
'        ByVal vobjCommonModuleNameToLinesInfoDic As Scripting.Dictionary, _
'        ByVal vobjDiffrentLinesCommonModuleNameToLinesInfoDic As Scripting.Dictionary, _
'        ByVal vobjModuleNameToMaxMinToVBProjectIndexDic As Scripting.Dictionary, _
'        ByVal vobjPartiallyCommonModuleNameToLinesInfoDic As Scripting.Dictionary, _
'        ByVal vobjProjectToIndependentModuleNameInfoDic As Scripting.Dictionary, _
'        Optional ByVal venmLogDTVisualizationType As LogDTVisualizationType = LogDTVisualizationType.GenerateCSVFile)
'
'
'    Dim strOutputDir As String
'
'    strOutputDir = mfstrGetVBProjectsComparedResultsDirectoryPath()
'
'    SortDictionaryAboutKeys vobjCommonModuleNameToLinesInfoDic
'
'    SortDictionaryAboutKeys vobjDiffrentLinesCommonModuleNameToLinesInfoDic
'
'    SortDictionaryAboutKeys vobjPartiallyCommonModuleNameToLinesInfoDic
'
'    SortDictionaryAboutValuesChildDictionaryKeys vobjProjectToIndependentModuleNameInfoDic
'
'    Select Case venmLogDTVisualizationType
'
'        Case LogDTVisualizationType.GenerateCSVFile
'
'            OutputComparingLinesResultsForPluralVBProjectsByCSV strOutputDir, vobjCommonModuleNameToLinesInfoDic, vobjDiffrentLinesCommonModuleNameToLinesInfoDic, vobjModuleNameToMaxMinToVBProjectIndexDic, vobjPartiallyCommonModuleNameToLinesInfoDic, vobjProjectToIndependentModuleNameInfoDic, vobjVBProjects
'
'        Case LogDTVisualizationType.GenerateNewBookWithDeletingOldSheets, LogDTVisualizationType.AddNewSheetToCurrentBook, LogDTVisualizationType.GenerateNewBookWithAddingSheet
'
'            ' Note: It is not used for ThisWorkbook keyword
'
'            ' Note: If called outside function has have some VB syntax errors, the VBA interpreter may have been unstable. When you confirm the unstable state of Excel VBE, I reccomend to change the direct call with avoiding interface-call
'
'            'CallBackInterfaceBySevenArguments "OutputComparingLinesResultsForComparingPluralVBProjectsByExcelBook", strOutputDir, vobjCommonModuleNameToLinesInfoDic, vobjDiffrentLinesCommonModuleNameToLinesInfoDic, vobjModuleNameToMaxMinToVBProjectIndexDic, vobjPartiallyCommonModuleNameToLinesInfoDic, vobjProjectToIndependentModuleNameInfoDic, vobjVBProjects, ThisWorkbook.FullName
'
'            OutputComparingLinesResultsForComparingPluralVBProjectsByExcelBook strOutputDir, vobjCommonModuleNameToLinesInfoDic, vobjDiffrentLinesCommonModuleNameToLinesInfoDic, vobjModuleNameToMaxMinToVBProjectIndexDic, vobjPartiallyCommonModuleNameToLinesInfoDic, vobjProjectToIndependentModuleNameInfoDic, vobjVBProjects
'    End Select
'
'End Sub
'
''''
'''' Output the compared VBComponent all-texts results
''''
'Private Sub msubOutputComparedAllTextsOfTwoVBProjectResults(ByVal vstrExecutionApplicationFilePath As String, _
'        ByVal vobjVBProjects As Collection, _
'        ByVal vobjCommonModuleNameToDifferenceCheckedAndLinesInfoDic As Scripting.Dictionary, _
'        ByVal vobjProjectToIndependentModuleNameInfoDic As Scripting.Dictionary, _
'        Optional ByVal venmLogDTVisualizationType As LogDTVisualizationType = LogDTVisualizationType.GenerateCSVFile)
'
'    Dim strOutputDir As String
'
'    strOutputDir = mfstrGetVBProjectsComparedResultsDirectoryPath()
'
'    Select Case venmLogDTVisualizationType
'
'        Case LogDTVisualizationType.GenerateCSVFile
'
'            OutputComparingAllTextsResultsForPluralVBProjectsByCSV strOutputDir, vobjCommonModuleNameToDifferenceCheckedAndLinesInfoDic, vobjProjectToIndependentModuleNameInfoDic, vobjVBProjects
'
'        Case LogDTVisualizationType.GenerateNewBookWithDeletingOldSheets, LogDTVisualizationType.AddNewSheetToCurrentBook, LogDTVisualizationType.GenerateNewBookWithAddingSheet
'
'            ' Note: It is not used for ThisWorkbook keyword
'            ' Note: If called outside function has have some VB syntax errors, the VBA interpreter may have been unstable. When you confirm the unstable state of Excel VBE, I reccomend to change the direct call with avoiding interface-call
'
'            'CallBackInterfaceByFourArguments "OutputComparingAllTextsResultsForComparingPluralVBProjectsByExcelBook", strOutputDir, vobjCommonModuleNameToDifferenceCheckedAndLinesInfoDic, vobjProjectToIndependentModuleNameInfoDic, vobjVBProjects, ThisWorkbook.FullName
'
'            OutputComparingAllTextsResultsForComparingPluralVBProjectsByExcelBook strOutputDir, vobjCommonModuleNameToDifferenceCheckedAndLinesInfoDic, vobjProjectToIndependentModuleNameInfoDic, vobjVBProjects
'    End Select
'End Sub
'
'
'Private Function mfstrGetVBProjectsComparedResultsDirectoryPath() As String
'
'    mfstrGetVBProjectsComparedResultsDirectoryPath = GetDevelopmentVBARootDir() & "\VBProjectsComparedResults"
'End Function
'
'
''''
''''
''''
'Private Sub msubGetFirstVBProjectPathAndCountOfLines(ByRef rstrVBProjectPath As String, _
'        ByRef rintCountOfLines As Long, _
'        ByVal vstrModuleFileName As String, _
'        ByVal vobjModuleNameToIncludedPaths As Scripting.Dictionary)
'
'
'    Dim objVBProjectPathToCountOfLinesDic As Scripting.Dictionary
'    Dim varVBProjectPath As Variant
'
'    Set objVBProjectPathToCountOfLinesDic = vobjModuleNameToIncludedPaths.Item(vstrModuleFileName)
'
'    For Each varVBProjectPath In objVBProjectPathToCountOfLinesDic.Keys
'
'        rstrVBProjectPath = varVBProjectPath
'
'        rintCountOfLines = objVBProjectPathToCountOfLinesDic.Item(varVBProjectPath)
'
'        Exit For
'    Next
'End Sub
'
'
''///////////////////////////////////////////////
''/// Sanity tests
''///////////////////////////////////////////////
''''
'''' get full-path of VBProject object
''''
'Private Sub msubSanityTestToVBProjectFileNameProperty()
'
'    With GetCurrentOfficeFileObject()
'
'        Debug.Print .VBProject.fileName
'    End With
'End Sub
'
'
'
'''--VBA_Code_File--<OperateVBIDEForProjectPass.bas>--
'Attribute VB_Name = "OperateVBIDEForProjectPass"
''
''   Set and remove the specified VBProject password by UI
''   Attension - If you are a VBA developer and have some customers, the password locking may prevent from some customers' wrong operations
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Dependency Abstract:
''       Dependent on both VBIDE and Windows OS
''       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Thu, 26/Mar/2015    Mr. bmp...              A part of idea has been disclosed at https://detail.chiebukuro.yahoo.co.jp/qa/question_detail/q12143520277
''       Tue, 30/May/2023    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
''///////////////////////////////////////////////
''/// Windows API private declarations
''///////////////////////////////////////////////
'#If VBA7 Then
'
'    Private Declare PtrSafe Sub keybd_event Lib "user32" (ByVal bVk As Byte, ByVal bScan As Byte, ByVal dwFlags As Long, ByVal dwExtraInfo As LongPtr)
'    Private Declare PtrSafe Sub Sleep Lib "kernel32" (ByVal ms As LongPtr)
'#Else
'    Private Declare Sub keybd_event Lib "user32" (ByVal bVk As Byte, ByVal bScan As Byte, ByVal dwFlags As Long, ByVal dwExtraInfo As Long)
'    Private Declare Sub Sleep Lib "kernel32" (ByVal ms As Long)
'#End If
'
'Private Const KEYEVENTF_EXTENDEDKEY = &H1
'
'Private Const KEYEVENTF_KEYUP = &H2
'
'
'Private Const VK_OEM_102 = &HE2 ' \ and _
'
''///////////////////////////////////////////////
''/// Constants
''///////////////////////////////////////////////
'Private Const mintShortMiliTime As Long = 150
'
'Private Const mintALittleLongMiliTime As Long = 400
'
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''''
'''' If you are a VBA developer and have some customers, the password locking may prevent from their wrong operations
''''
'Public Sub SetVBProjectLockPassword(ByVal vobjVBProject As VBIDE.VBProject, _
'        ByVal vstrPassword As String, _
'        Optional ByVal vblnLockVBProjectForViewing As Boolean = True)
'
'
'    SetWindowForegroundAboutVBE
'
'    With vobjVBProject
'
'        Set .VBE.ActiveVBProject = vobjVBProject
'    End With
'
'    Sleep mintShortMiliTime
'
'    msubLockPasswordVBProjectForActive vstrPassword, vblnLockVBProjectForViewing, True, True
'End Sub
'
'
''''
''''
''''
'Public Sub SendVirtualKeystrokeOfNumberAndAlphabetByWinAPI(ByRef rstrOneChar As String)
'
'    Select Case rstrOneChar
'
'        Case "a"
'
'            SendKeystrokeByWinAPI vbKeyA
'
'        Case "b"
'
'            SendKeystrokeByWinAPI vbKeyB
'
'        Case "c"
'
'            SendKeystrokeByWinAPI vbKeyC
'
'        Case "d"
'
'            SendKeystrokeByWinAPI vbKeyD
'
'        Case "e"
'
'            SendKeystrokeByWinAPI vbKeyE
'
'        Case "f"
'
'            SendKeystrokeByWinAPI vbKeyF
'
'        Case "g"
'
'            SendKeystrokeByWinAPI vbKeyG
'
'        Case "h"
'
'            SendKeystrokeByWinAPI vbKeyH
'
'        Case "i"
'
'            SendKeystrokeByWinAPI vbKeyI
'
'        Case "j"
'
'            SendKeystrokeByWinAPI vbKeyJ
'
'        Case "i"
'
'            SendKeystrokeByWinAPI vbKeyI
'
'        Case "k"
'
'            SendKeystrokeByWinAPI vbKeyK
'
'        Case "l"
'
'            SendKeystrokeByWinAPI vbKeyL
'
'        Case "m"
'
'            SendKeystrokeByWinAPI vbKeyM
'
'        Case "n"
'
'            SendKeystrokeByWinAPI vbKeyN
'
'        Case "o"
'
'            SendKeystrokeByWinAPI vbKeyO
'
'        Case "p"
'
'            SendKeystrokeByWinAPI vbKeyP
'
'        Case "q"
'
'            SendKeystrokeByWinAPI vbKeyQ
'
'        Case "r"
'
'            SendKeystrokeByWinAPI vbKeyR
'
'        Case "s"
'
'            SendKeystrokeByWinAPI vbKeyS
'
'        Case "t"
'
'            SendKeystrokeByWinAPI vbKeyT
'
'        Case "u"
'
'            SendKeystrokeByWinAPI vbKeyU
'
'        Case "v"
'
'            SendKeystrokeByWinAPI vbKeyV
'
'        Case "w"
'
'            SendKeystrokeByWinAPI vbKeyW
'
'        Case "x"
'
'            SendKeystrokeByWinAPI vbKeyX
'
'        Case "y"
'
'            SendKeystrokeByWinAPI vbKeyY
'
'        Case "z"
'
'            SendKeystrokeByWinAPI vbKeyZ
'
'        Case "A"
'
'            SendKeystrokeByWinAPI vbKeyShift, vbKeyA
'
'        Case "B"
'
'            SendKeystrokeByWinAPI vbKeyShift, vbKeyB
'
'        Case "C"
'
'            SendKeystrokeByWinAPI vbKeyShift, vbKeyC
'
'        Case "D"
'
'            SendKeystrokeByWinAPI vbKeyShift, vbKeyD
'
'        Case "E"
'
'            SendKeystrokeByWinAPI vbKeyShift, vbKeyE
'
'        Case "F"
'
'            SendKeystrokeByWinAPI vbKeyShift, vbKeyF
'
'        Case "G"
'
'            SendKeystrokeByWinAPI vbKeyShift, vbKeyG
'
'        Case "H"
'
'            SendKeystrokeByWinAPI vbKeyShift, vbKeyH
'
'        Case "I"
'
'            SendKeystrokeByWinAPI vbKeyShift, vbKeyI
'
'        Case "J"
'
'            SendKeystrokeByWinAPI vbKeyShift, vbKeyJ
'
'        Case "K"
'
'            SendKeystrokeByWinAPI vbKeyShift, vbKeyK
'
'        Case "L"
'
'            SendKeystrokeByWinAPI vbKeyShift, vbKeyL
'
'        Case "M"
'
'            SendKeystrokeByWinAPI vbKeyShift, vbKeyM
'
'        Case "N"
'
'            SendKeystrokeByWinAPI vbKeyShift, vbKeyN
'
'        Case "O"
'
'            SendKeystrokeByWinAPI vbKeyShift, vbKeyO
'
'        Case "P"
'
'            SendKeystrokeByWinAPI vbKeyShift, vbKeyP
'
'        Case "Q"
'
'            SendKeystrokeByWinAPI vbKeyShift, vbKeyQ
'
'        Case "R"
'
'            SendKeystrokeByWinAPI vbKeyShift, vbKeyR
'
'        Case "S"
'
'            SendKeystrokeByWinAPI vbKeyShift, vbKeyS
'
'        Case "T"
'
'            SendKeystrokeByWinAPI vbKeyShift, vbKeyT
'
'        Case "U"
'
'            SendKeystrokeByWinAPI vbKeyShift, vbKeyU
'
'        Case "V"
'
'            SendKeystrokeByWinAPI vbKeyShift, vbKeyV
'
'        Case "W"
'
'            SendKeystrokeByWinAPI vbKeyShift, vbKeyW
'
'        Case "X"
'
'            SendKeystrokeByWinAPI vbKeyShift, vbKeyX
'
'        Case "Y"
'
'            SendKeystrokeByWinAPI vbKeyShift, vbKeyY
'
'        Case "Z"
'
'            SendKeystrokeByWinAPI vbKeyShift, vbKeyZ
'
'        Case "0"
'
'            SendKeystrokeByWinAPI vbKey0
'
'        Case "1"
'
'            SendKeystrokeByWinAPI vbKey1
'
'        Case "2"
'
'            SendKeystrokeByWinAPI vbKey2
'
'        Case "3"
'
'            SendKeystrokeByWinAPI vbKey3
'
'        Case "4"
'
'            SendKeystrokeByWinAPI vbKey4
'
'        Case "5"
'
'            SendKeystrokeByWinAPI vbKey5
'
'        Case "6"
'
'            SendKeystrokeByWinAPI vbKey6
'
'        Case "7"
'
'            SendKeystrokeByWinAPI vbKey7
'
'        Case "8"
'
'            SendKeystrokeByWinAPI vbKey8
'
'        Case "9"
'
'            SendKeystrokeByWinAPI vbKey9
'
'        Case "_"
'
'            ' for example, Japanese 109 keyboard
'
'            SendKeystrokeByWinAPI vbKeyShift, VK_OEM_102
'
'        Case "."
'
'            SendKeystrokeByWinAPI vbKeyDecimal
'
'    End Select
'End Sub
'
'
''''
''''
''''
'Public Sub SendKeystrokeByWinAPI(ByRef rvarVirtualKey01 As Integer, Optional ByVal vvarVirtualKey02 As Variant)
'
'    Select Case True
'
'        Case IsMissing(vvarVirtualKey02)
'
'            keybd_event CByte(rvarVirtualKey01), 0, 0, 0
'
'            keybd_event CByte(rvarVirtualKey01), 0, KEYEVENTF_KEYUP, 0
'
'        Case Else
'
'            keybd_event CByte(rvarVirtualKey01), 0, 0, 0
'
'            keybd_event CByte(vvarVirtualKey02), 0, 0, 0
'
'            keybd_event CByte(vvarVirtualKey02), 0, KEYEVENTF_KEYUP, 0
'
'            keybd_event CByte(rvarVirtualKey01), 0, KEYEVENTF_KEYUP, 0
'    End Select
'End Sub
'
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
''''
''''
''''
'Private Sub msubLockPasswordVBProjectForActive(ByVal vstrPassword As String, _
'        Optional ByVal vblnLockVBProjectForViewing As Boolean = True, _
'        Optional ByVal vblnAllowToFinishSettingPassword As Boolean = False, _
'        Optional ByVal vblnAllowToCloseVBEAfterAllProcesses As Boolean = True)
'
'    msubOpenVBProjectPasswordSettingWindow vblnLockVBProjectForViewing
'
'    ' Next, the cursor is to be the first password text-box
'
'    SendKeystrokeByWinAPI vbKeyTab
'
'    msubSendKeysOfStringLiteral vstrPassword
'
'    ' Next, the cursor is to be the second password text-box
'
'    SendKeystrokeByWinAPI vbKeyTab
'
'    msubSendKeysOfStringLiteral vstrPassword
'
'    If vblnAllowToFinishSettingPassword Then
'
'        SendKeystrokeByWinAPI vbKeyReturn   ' enter key
'    End If
'
'    If vblnAllowToCloseVBEAfterAllProcesses Then
'
'        SendKeystrokeByWinAPI vbKeyMenu, vbKeyF4    ' close the Visual Basic Editor
'    End If
'End Sub
'
'
''''
'''' A step to open VBProject property page form
''''
'Private Sub msubOpenVBProjectPasswordSettingWindow(ByVal vblnLockVBProjectForViewing As Boolean)
'
'    Interaction.DoEvents
'
'    Sleep 250
'
'    Interaction.DoEvents
'
'    Interaction.AppActivate "Microsoft Visual Basic"
'
'    Sleep mintShortMiliTime
'
'    SendKeystrokeByWinAPI vbKeyMenu, vbKeyT
'
'    Sleep mintShortMiliTime
'
'    SendKeystrokeByWinAPI vbKeyE
'
'    Sleep mintALittleLongMiliTime
'
'    SendKeystrokeByWinAPI vbKeyControl, vbKeyTab
'
'    Sleep mintShortMiliTime
'
'    SendKeystrokeByWinAPI vbKeyTab
'
'    If vblnLockVBProjectForViewing Then
'
'        SendKeystrokeByWinAPI vbKeySpace  ' Japanese as 「プロジェクトを表示用にロック」
'
'        Sleep mintShortMiliTime
'    End If
'
'    SendKeystrokeByWinAPI vbKeyTab  ' next to 1st password text-box
'
'    Sleep mintShortMiliTime
'End Sub
'
''''
''''
''''
'Private Sub msubSendKeysOfStringLiteral(ByRef rstrText As String)
'
'    Dim i As Long, strSendChar As String
'
'    For i = 1 To Len(rstrText)
'
'        strSendChar = Mid(rstrText, i, 1)
'
'        SendVirtualKeystrokeOfNumberAndAlphabetByWinAPI strSendChar
'    Next
'End Sub
'
''///////////////////////////////////////////////
''/// Sanity tests
''///////////////////////////////////////////////
''''
''''
''''
'Private Sub msubSanityTestToSendKeystrokeByWinAPI()
'
'    ' input Under-bar
'
'    SendKeystrokeByWinAPI vbKeyShift, VK_OEM_102
'End Sub
'
'
''''
''''
''''
'Private Sub msubSanityTestToLockPasswordVBProjectForActive()
'
'    msubLockPasswordVBProjectForActive "TestA", False, False, False
'End Sub
'
'
'Private Sub msubSanityTestToLockPasswordVBProject()
'
'    msubOpenVBProjectPasswordSettingWindow True
'
'    ' Next, the cursor is to be the first password text-box
'
'    SendKeystrokeByWinAPI vbKeyTab
'
'    msubSendKeysOfStringLiteral "TestA"
'
'    ' Next, the cursor is to be the second password text-box
'
'    SendKeystrokeByWinAPI vbKeyTab
'
'    msubSendKeysOfStringLiteral "TestA"
'
'
'    SendKeystrokeByWinAPI vbKeyReturn   ' enter key
'
'    SendKeystrokeByWinAPI vbKeyMenu, vbKeyF4    ' close the Visual Basic Editor
'End Sub
'
'''--VBA_Code_File--<MetaDirectives.bas>--
'Attribute VB_Name = "MetaDirectives"
''
''   Parsing VBA directives
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Development Policy:
''       Some algorithms should have looked like either uncompleted or naive, if you are a IT architect, a computer scientist, or a mathematician.
''       It is not a open-source project purpose that the completed algorithms are adopted in all individual processes.
''       After I have confirmed both the users' needs and my sufficient funds, I will investigate to improve the concerned algorithms.
''
''   Dependency Abstract:
''       Dependent on VBIDE
''       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access, or ADO
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Tue, 21/Feb/2023    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
'
''///////////////////////////////////////////////
''/// Constants
''///////////////////////////////////////////////
'Private Const mstrExpressionPatternChars As String = "a-zA-Z0-9 \=<>\(\)_\."
'
'Private Const mstrVariablePatternChars As String = "a-zA-Z0-9_\."
'
''///////////////////////////////////////////////
''/// Enumerations
''///////////////////////////////////////////////
'Public Enum VBALangDirectivesType
'
'    NoVBADirectives = 0
'
'
'    UnknownAsVBADirectives = 1
'
'    ConstVBADirective = 2
'
'    IfBranchDirective = 3
'
'    ElseIfBranchDirective = 4
'
'    ElseBranchDirective = 5
'
'    EndIfBranchDirective = 6
'End Enum
'
'
'
''///////////////////////////////////////////////
''/// Declarations
''///////////////////////////////////////////////
'
'Private mobjVBADirectivesRegExp As VBScript_RegExp_55.RegExp
'
'Private mobjVBADirectiveAssignmentSyntaxRegExp As VBScript_RegExp_55.RegExp
'
'Private mobjVBADirectiveBooleanExpressionSyntaxRegExp As VBScript_RegExp_55.RegExp
'
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''**---------------------------------------------
''** Syntax VBA directives
''**---------------------------------------------
''''
''''
''''
'Public Function GetVBABooleanEvaluationEachTerms(ByRef rstrExpression As String, ByRef robjNameToValueDic As Scripting.Dictionary) As Boolean
'
'    Dim blnResult As Boolean, blnValue1 As Boolean, blnValue2 As Boolean, varBinaryOperator As Variant
'
'    blnResult = False
'
'    With GetVBADirectiveBooleanExpressionSyntaxRegExp()
'
'        If .Test(rstrExpression) Then
'
'            With .Execute(rstrExpression).Item(0).SubMatches
'
'                ' This is not a smart algorithm, or a temporary algorithm.
'
'                ' Attention - The simple algorithm is to be adoped as long as keeping usefulness, although it is naive.
'
'                If .Item(6) <> "" Then
'
'                    blnResult = mfblnGetBooleanValueFromIdentifier(.Item(6), robjNameToValueDic)
'
'                ElseIf .Item(5) <> "" Then
'
'                    blnResult = Not mfblnGetBooleanValueFromIdentifier(.Item(5), robjNameToValueDic)
'
'                ElseIf .Item(2) <> "" Then
'
'                    blnValue1 = mfblnGetBooleanValueFromIdentifier(.Item(1), robjNameToValueDic)
'
'                    blnValue2 = mfblnGetBooleanValueFromIdentifier(.Item(3), robjNameToValueDic)
'
'                    varBinaryOperator = .Item(2)
'
'                    Select Case varBinaryOperator
'
'                        Case "="
'
'                            blnResult = (blnValue1 = blnValue2)
'
'                        Case "And"
'
'                            blnResult = (blnValue1 And blnValue2)
'
'                        Case "Or"
'
'                            blnResult = (blnValue1 Or blnValue2)
'                    End Select
'                End If
'
'            End With
'        End If
'    End With
'
'    GetVBABooleanEvaluationEachTerms = blnResult
'End Function
'
''''
''''
''''
'Private Function mfblnGetBooleanValueFromIdentifier(ByRef rvarValue As Variant, ByRef robjNameToValueDic As Scripting.Dictionary) As Boolean
'
'    Dim blnResult As Boolean
'
'    Select Case rvarValue
'
'        Case "True", "False"
'
'            blnResult = CBool(rvarValue)
'
'        Case Else
'
'            If robjNameToValueDic.Exists(rvarValue) Then
'
'                blnResult = CBool(robjNameToValueDic.Item(rvarValue))
'            End If
'    End Select
'
'    mfblnGetBooleanValueFromIdentifier = blnResult
'End Function
'
'
'
''''
''''
''''
'Public Sub GetVBAAssignmentEachTerms(ByRef rstrExpression As String, ByRef rstrDirectiveConstantName As String, ByRef rvarValue As Variant)
'
'    With GetVBADirectiveAssignmentSyntaxRegExp
'
'        If .Test(rstrExpression) Then
'
'            With .Execute(rstrExpression).Item(0).SubMatches
'
'                rstrDirectiveConstantName = .Item(0)
'
'                ' Attention - The simple algorithm is to be adoped as long as keeping usefulness, although it is naive.
'
'                Select Case .Item(1)
'
'                    Case "True", "False"
'
'                        rvarValue = CBool(.Item(1))
'
'                    Case InStrRev(.Item(1), ".") > 0
'
'                        rvarValue = CDbl(.Item(1))
'                    Case Else
'
'                        rvarValue = CLng(.Item(1))
'                End Select
'
'            End With
'        Else
'            rstrDirectiveConstantName = ""
'            rvarValue = 0
'        End If
'    End With
'
'End Sub
'
'
''''
''''
''''
'Public Sub GetVBADirectiveInfo(ByRef rstrCodeToSyntax As String, ByRef renmVBALangDirectivesType As VBALangDirectivesType, ByRef rstrDirectiveExpression As String)
'
'    Dim objMatches As VBScript_RegExp_55.MatchCollection, objMatch As VBScript_RegExp_55.Match
'
'    renmVBALangDirectivesType = NoVBADirectives
'
'    With GetVBADerectivesRegExp()
'
'        If .Test(rstrCodeToSyntax) Then
'
'            Set objMatches = .Execute(rstrCodeToSyntax)
'
'            Set objMatch = objMatches.Item(0)
'
'            With objMatch.SubMatches
'
'                If StrComp(.Item(1), "#Const") = 0 Then
'
'                    renmVBALangDirectivesType = ConstVBADirective
'
'                    rstrDirectiveExpression = .Item(2)
'
'                ElseIf StrComp(.Item(4), "#If") = 0 Then
'
'                    renmVBALangDirectivesType = IfBranchDirective
'
'                    rstrDirectiveExpression = .Item(5)
'
'                ElseIf StrComp(.Item(7), "#ElseIf") = 0 Then
'
'                    renmVBALangDirectivesType = ElseIfBranchDirective
'
'                    rstrDirectiveExpression = .Item(8)
'
'                ElseIf StrComp(.Item(9), "#Else") = 0 Then
'
'                    renmVBALangDirectivesType = ElseBranchDirective
'
'                    rstrDirectiveExpression = ""
'
'                ElseIf StrComp(.Item(10), "#End If") = 0 Then
'
'                    renmVBALangDirectivesType = EndIfBranchDirective
'
'                    rstrDirectiveExpression = ""
'                Else
'
'                    If InStr(1, rstrCodeToSyntax, "#") = 1 Then
'
'                        renmVBALangDirectivesType = UnknownAsVBADirectives
'                    Else
'                        renmVBALangDirectivesType = NoVBADirectives
'                    End If
'
'                    rstrDirectiveExpression = ""
'
'                End If
'            End With
'        Else
'
'            rstrDirectiveExpression = ""
'        End If
'    End With
'End Sub
'
''''
''''
''''
'Public Function GetEnumTextOfVBALangDirectivesType(ByRef renmVBALangDirectivesType As VBALangDirectivesType) As String
'
'    Dim strText As String
'
'    strText = ""
'
'    Select Case renmVBALangDirectivesType
'
'        Case VBALangDirectivesType.NoVBADirectives
'
'            strText = "NoVBADirectives"
'
'        Case VBALangDirectivesType.UnknownAsVBADirectives
'
'            strText = "UnknownAsVBADirectives"
'
'        Case VBALangDirectivesType.ConstVBADirective
'
'            strText = "ConstVBADirective"
'
'        Case VBALangDirectivesType.IfBranchDirective
'
'            strText = "IfBranchDirective"
'
'        Case VBALangDirectivesType.ElseIfBranchDirective
'
'            strText = "ElseIfBranchDirective"
'
'        Case VBALangDirectivesType.ElseBranchDirective
'
'            strText = "ElseBranchDirective"
'
'        Case VBALangDirectivesType.EndIfBranchDirective
'
'            strText = "EndIfBranchDirective"
'
'    End Select
'
'    GetEnumTextOfVBALangDirectivesType = strText
'End Function
'
'
''**---------------------------------------------
''** Prepare RegExp objects for syntax
''**---------------------------------------------
'Public Function GetVBADerectivesRegExp() As VBScript_RegExp_55.RegExp
'
'    If mobjVBADirectivesRegExp Is Nothing Then
'
'        Set mobjVBADirectivesRegExp = New VBScript_RegExp_55.RegExp
'
'        With mobjVBADirectivesRegExp
'
'            .Pattern = "((#Const) ([" & mstrExpressionPatternChars & "]{1,}))|((#If) ([" & mstrExpressionPatternChars & "]{1,}) Then)|((#ElseIf) ([" & mstrExpressionPatternChars & "]{1,}) Then)|(#Else)|(#End If)"
'        End With
'    End If
'
'    Set GetVBADerectivesRegExp = mobjVBADirectivesRegExp
'End Function
'
'
'Public Function GetVBADirectiveAssignmentSyntaxRegExp() As VBScript_RegExp_55.RegExp
'
'    If mobjVBADirectiveAssignmentSyntaxRegExp Is Nothing Then
'
'        Set mobjVBADirectiveAssignmentSyntaxRegExp = New VBScript_RegExp_55.RegExp
'
'        With mobjVBADirectiveAssignmentSyntaxRegExp
'
'            .Pattern = "([" & mstrVariablePatternChars & "]{1,}) \= ([" & mstrVariablePatternChars & "]{1,})"
'        End With
'    End If
'
'    Set GetVBADirectiveAssignmentSyntaxRegExp = mobjVBADirectiveAssignmentSyntaxRegExp
'End Function
'
'
'Public Function GetVBADirectiveBooleanExpressionSyntaxRegExp() As VBScript_RegExp_55.RegExp
'
'    If mobjVBADirectiveBooleanExpressionSyntaxRegExp Is Nothing Then
'
'        Set mobjVBADirectiveBooleanExpressionSyntaxRegExp = New VBScript_RegExp_55.RegExp
'
'        With mobjVBADirectiveBooleanExpressionSyntaxRegExp
'
'            ' This is not a smart algorithm, or a temporary algorithm.
'
'            ' Attention - The simple algorithm is to be adoped as long as keeping usefulness, although it is naive.
'
'            .Pattern = "(([" & mstrVariablePatternChars & "]{1,}) (\=|And|Or) ([" & mstrVariablePatternChars & "]{1,}))|(Not ([" & mstrVariablePatternChars & "]{1,}))|([" & mstrVariablePatternChars & "]{1,})"
'        End With
'    End If
'
'    Set GetVBADirectiveBooleanExpressionSyntaxRegExp = mobjVBADirectiveBooleanExpressionSyntaxRegExp
'End Function
'
'
'
''**---------------------------------------------
''** Clear cache of RegExp objects
''**---------------------------------------------
''''
''''
''''
'Public Sub ClearRegExpObjectsAboutVBADirectives()
'
'    Set mobjVBADirectivesRegExp = Nothing
'
'    Set mobjVBADirectiveAssignmentSyntaxRegExp = Nothing
'
'    Set mobjVBADirectiveBooleanExpressionSyntaxRegExp = Nothing
'End Sub
'
'
'
'''--VBA_Code_File--<LTfMetaDirectives.bas>--
'Attribute VB_Name = "LTfMetaDirectives"
''
''   Log text out tests for syntax VBA directives
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Development Policy:
''       Some algorithms should have looked like either uncompleted or naive, if you are a IT architect, a computer scientist, or a mathematician.
''       It is not a open-source project purpose that the completed algorithms are adopted in all individual processes.
''       After I have confirmed both the users' needs and my sufficient funds, I will investigate to improve the concerned algorithms.
''
''   Dependency Abstract:
''       Dependent on VBIDE
''       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access, or ADO
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Tue, 21/Feb/2023    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
'
''///////////////////////////////////////////////
''/// Log text out tests
''///////////////////////////////////////////////
'Private Sub msubLogTextOutTestOfGetVBADirectiveInfo()
'
'    Dim objLines As Collection, strLog As String
'
'    ClearRegExpObjectsAboutVBADirectives
'
'
'    Set objLines = New Collection
'
'    With objLines
'
'        .Add "#Const HAS_REF = True"
'
'        .Add "#If HAS_REF Then"
'
'        .Add "#If Not HAS_REF Then"
'
'        .Add "#ElseIf VBA7 Then"
'
'        .Add "#If VBA7 And Win64 Then"
'
'        .Add "#Else"
'
'        .Add "#End If"
'    End With
'End Sub
'
'
''''
''''
''''
'Private Sub msubLogTextOutTestOfVBADirectiveBooleanExpressionSyntaxRegExp()
'
'    Dim objLines As Collection, strLog As String
'
'    ClearRegExpObjectsAboutVBADirectives
'
'    Set objLines = New Collection
'
'    With objLines
'
'        .Add "HAS_REF"
'
'        .Add "Not HAS_REF"
'
'        .Add "HAS_REF = True"
'
'        .Add "HAS_REF And VBA7"
'    End With
'
'    strLog = GetLogOfRegExpAndPluralLines(GetVBADirectiveBooleanExpressionSyntaxRegExp(), objLines)
'
'    DebugPrintToTextFile strLog, "DebugPrintVBADirectivesBooleanExpression.txt"
'End Sub
'
'
''''
''''
''''
'Private Sub msubLogTextOutTestOfSomeDirectives()
'
'    Dim objLines As Collection, strLog As String
'
'    ClearRegExpObjectsAboutVBADirectives
'
'
'    Set objLines = GetVariousVBADirectiveInputs()
''
''    With objLines
''
''        .Add "#Const HAS_REF = True"
''
''        .Add "#If HAS_REF Then"
''
''        .Add "#If Not HAS_REF Then"
''
''        .Add "#ElseIf VBA7 Then"
''
''        .Add "#Else"
''
''        .Add "#End If"
''    End With
'
'    strLog = GetLogOfRegExpAndPluralLines(GetVBADerectivesRegExp(), objLines)
'
'    DebugPrintToTextFile strLog, "DebugPrintVBADirectivesLineSyntax.txt"
'End Sub
'
'
'
'''--VBA_Code_File--<UTfMetaDirectives.bas>--
'Attribute VB_Name = "UTfMetaDirectives"
''
''   Unit tests for syntax VBA directives
''
''   Coding Conventions Note:
''       At the moment, this source code ordinarily adopts a special system-Hungarian notations.
''       Almost recent integrated development environment(IDE) applications of various programming languages have various
''       refactoring tools and various coding processing dashboards. These enriched functions emphasizes any
''       troubles of adopting system-Hungarian notations. But in the Visual Basic Editor of Microsoft Office software,
''       some features are different. First, this editor has no powerful refactoring functions.
''       Second, VBA can use Windows API, which library is written by a system-Hungarian notations.
''       In a limited VBA, using a similar system-Hungarian notations contributes to the uniformity improvement.
''       If the merit of adopting a system-Hungarian notation is small when the VB project size is smaller.
''       However, this VB project size is not small. Especially, when a VB project includes low-level numerical
''       calculation codes, the notation is to be often near the application-Hungarian notation.
''       If you would like to eliminate all system-Hungarian notations, it might be a necessity to use some outside other refactoring tools.
''       Although the lack of convenient refactoring tools of IDE is a weak point,
''       the light-weight IDE will be a strong benefit for all software developers as an interpreter system.
''
''   Development Policy:
''       Some algorithms should have looked like either uncompleted or naive, if you are a IT architect, a computer scientist, or a mathematician.
''       It is not a open-source project purpose that the completed algorithms are adopted in all individual processes.
''       After I have confirmed both the users' needs and my sufficient funds, I will investigate to improve the concerned algorithms.
''
''   Dependency Abstract:
''       Dependent on VBIDE
''       This is independent from Office Application References, such as Excel, Word, PowerPoint, or Access, or ADO
''
''   Author:
''       Kalmclaeyd M. Tarclanus
''
''   License disclosure:
''       Copyright (c) 2023 Kalmclaeyd M. Tarclanus
''       Released under the MIT license
''       https://opensource.org/licenses/mit-license.php
''
''   Modification History:
''       Tue, 21/Feb/2023    Kalmclaeyd Tarclanus    Initial Creation
''
'
'Option Explicit
'
'
''///////////////////////////////////////////////
''/// Unit tests
''///////////////////////////////////////////////
''''
''''
''''
'Private Sub UnitTestToGetVBADirectiveInfo()
'
'    Dim objSheet As Excel.Worksheet, objParameters As UnitTestWrappedParameters
'
'    Set objParameters = New UnitTestWrappedParameters
'
'    With objParameters
'
'        .ModuleNameOfTestingFunction = "MetaDirectives"
'
'        .SetUpTargetFunctionParameters "GetWrappedVBADirectiveInfo", "GetInputToRightPluralOutputsDicForGetVBADirectiveInfo", "GetVBADirectiveInfo", 2, 1
'
'        .SheetFormatSetting.SetUpIOColumnNameAndColumnWidth "SourceLine,20", "VBALangDirectivesType,13,DirectiveExpression,18"
'
'        .UnitTestResultBookBaseName = "UnitTestOfGetVBADirectiveInfo"
'
'        .AutoShapeUnitTestTheme = "Testing to GetVBADirectiveInfo of each module"
'    End With
'
'    Set objSheet = UnitTestBasedOnWrappedFormation(objParameters)
'
'End Sub
'
''///////////////////////////////////////////////
''/// Operations
''///////////////////////////////////////////////
''''
'''' For unit test definition. because the overhead process of the object creation should have been avoided.
''''
'Public Function GetWrappedVBADirectiveInfo(ByRef rstrCodeToSyntax As String) As Collection
'
'    Dim objCol As Collection
'    Dim enmVBALangDirectivesType As VBALangDirectivesType, strDirectiveExpression As String
'
'
'    GetVBADirectiveInfo rstrCodeToSyntax, enmVBALangDirectivesType, strDirectiveExpression
'
'    Set objCol = New Collection
'
'    With objCol
'
'        .Add GetEnumTextOfVBALangDirectivesType(enmVBALangDirectivesType)
'
'        .Add strDirectiveExpression
'    End With
'
'    Set GetWrappedVBADirectiveInfo = objCol
'End Function
'
''''
'''' called from LTfMetaDirectives.bas
''''
'Public Function GetVariousVBADirectiveInputs() As Collection
'
'    Dim objLines As Collection
'
'    Set objLines = GetEnumeratorKeysColFromDic(GetInputToRightPluralOutputsDicForGetVBADirectiveInfo())
'
'    Set GetVariousVBADirectiveInputs = objLines
'End Function
'
'
'
''///////////////////////////////////////////////
''/// Internal functions
''///////////////////////////////////////////////
''''
'''' get Input to right output dictionary for GetSnakeCaseFromCamelCase
''''
'Private Function GetInputToRightPluralOutputsDicForGetVBADirectiveInfo() As Scripting.Dictionary
'
'    Dim objInputToRightPluralOutputs As Scripting.Dictionary
'    Dim objOutputs As Collection, strInput As String
'
'    Set objInputToRightPluralOutputs = New Scripting.Dictionary
'
'    With objInputToRightPluralOutputs
'
'        ' 01 input-output
'        strInput = "#Const HAS_REF = True"
'
'        Set objOutputs = New Collection
'
'        With objOutputs
'
'            .Add "ConstVBADirective": .Add "HAS_REF = True"
'        End With
'
'        .Add strInput, objOutputs
'
'        ' 02 input-output
'        strInput = "#If HAS_REF Then"
'
'        Set objOutputs = New Collection
'
'        With objOutputs
'
'            .Add "IfBranchDirective": .Add "HAS_REF"
'        End With
'
'        .Add strInput, objOutputs
'
'        ' 03 input-output
'        strInput = "#If Not HAS_REF Then"
'
'        Set objOutputs = New Collection
'
'        With objOutputs
'
'            .Add "IfBranchDirective": .Add "Not HAS_REF"
'        End With
'
'        .Add strInput, objOutputs
'
'        ' 04 input-output
'        strInput = "#ElseIf VBA7 Then"
'
'        Set objOutputs = New Collection
'
'        With objOutputs
'
'            .Add "ElseIfBranchDirective": .Add "VBA7"
'        End With
'
'        .Add strInput, objOutputs
'
'        ' 05 input-output
'        strInput = "#If VBA7 And Win64 Then"
'
'        Set objOutputs = New Collection
'
'        With objOutputs
'
'            .Add "IfBranchDirective": .Add "VBA7 And Win64"
'        End With
'
'        .Add strInput, objOutputs
'
'        ' 06 input-output
'        strInput = "#Else"
'
'        Set objOutputs = New Collection
'
'        With objOutputs
'
'            .Add "ElseBranchDirective": .Add ""
'        End With
'
'        .Add strInput, objOutputs
'
'        ' 07 input-output
'        strInput = "#End If"
'
'        Set objOutputs = New Collection
'
'        With objOutputs
'
'            .Add "EndIfBranchDirective": .Add ""
'        End With
'
'        .Add strInput, objOutputs
'
'
'        ' 08 input-output
'        strInput = ""
'
'        Set objOutputs = New Collection
'
'        With objOutputs
'
'            .Add "NoVBADirectives": .Add ""
'        End With
'
'        ' 09 input-output
'        strInput = "#Define ABC_TAG = 1"
'
'        Set objOutputs = New Collection
'
'        With objOutputs
'
'            .Add "UnknownAsVBADirectives": .Add ""
'        End With
'    End With
'
'
'    Set GetInputToRightPluralOutputsDicForGetVBADirectiveInfo = objInputToRightPluralOutputs
'End Function
'
'
'

